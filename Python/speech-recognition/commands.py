from getpass import getuser
from os import startfile

def handle_speech(text: str) -> str:
    responses = {
        "hello": "Hello! How can I help you today?",
        "goodbye": "Goodbye!",
        "how are you": "I'm good, thank you.",
        "what's your name": "My name is Speech.",
        "what's my username": f"Your username is {getuser()}.",
        # add more commands to get response
    }

    start_page = {
        "linkedin": "https://www.linkedin.com/in/josepedrobertolatavares/",
        # add more commands to start page
    }

    print(f'Speech: You said "{text}".')

    if text.lower() in responses:
        return responses[text.lower()]

    if text.lower() in start_page:
        startfile(start_page[text.lower()])
        return f"Opening {start_page[text.lower()]}"

    return "Sorry, I don't understand."