import speech_recognition as sr
from commands import handle_speech

recognizer = sr.Recognizer()
microphone = sr.Microphone()

print('Speech: Say something...')

with microphone as source:
    recognizer.adjust_for_ambient_noise(source)
    while True:
        print('---------------------------------------------------------------------------------------')
        audio = recognizer.listen(source)

        try:

            text = recognizer.recognize_google(audio).lower()
            response = handle_speech(text)
            print(f"Speech: {response}")

            if "goodbye" in text:
                break

        except sr.UnknownValueError:
            print('Speech: Say something...')