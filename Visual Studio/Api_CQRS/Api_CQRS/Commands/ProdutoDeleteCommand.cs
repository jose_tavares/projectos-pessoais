﻿namespace Api_CQRS.Commands
{
    using Api_CQRS.Models;
    using MediatR;

    public class ProdutoDeleteCommand : IRequest<Produto>
    {
        public int Id { get; set; }
    }
}
