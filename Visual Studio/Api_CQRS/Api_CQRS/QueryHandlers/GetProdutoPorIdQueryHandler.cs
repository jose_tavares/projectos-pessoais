﻿namespace Api_CQRS.QueryHandlers
{
    using System.Threading;
    using System.Threading.Tasks;
    using Api_CQRS.Context;
    using Api_CQRS.Models;
    using Api_CQRS.Queries;
    using MediatR;
    using Microsoft.EntityFrameworkCore;

    public class GetProdutoPorIdQueryHandler : IRequestHandler<GetProdutoPorIdQuery, Produto>
    {
        private readonly AppDbContext context;

        public GetProdutoPorIdQueryHandler(AppDbContext context)
        {
            this.context = context;
        }

        public async Task<Produto> Handle(GetProdutoPorIdQuery request, CancellationToken cancellationToken)
        {
            return await context.Produtos.FirstOrDefaultAsync(produto => produto.Id == request.Id, cancellationToken);
        }
    }
}
