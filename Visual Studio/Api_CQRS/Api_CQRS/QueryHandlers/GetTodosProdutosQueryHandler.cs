﻿namespace Api_CQRS.QueryHandlers
{
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;
    using Api_CQRS.Context;
    using Api_CQRS.Models;
    using Api_CQRS.Queries;
    using MediatR;
    using Microsoft.EntityFrameworkCore;

    public class GetTodosProdutosQueryHandler : IRequestHandler<GetTodosProdutosQuery, IEnumerable<Produto>>
    {
        private readonly AppDbContext context;

        public GetTodosProdutosQueryHandler(AppDbContext context)
        {
            this.context = context;
        }

        public async Task<IEnumerable<Produto>> Handle(GetTodosProdutosQuery request, CancellationToken cancellationToken)
        {
            return await context.Produtos.ToListAsync();
        }
    }
}
