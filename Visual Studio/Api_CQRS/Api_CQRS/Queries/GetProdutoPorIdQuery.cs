﻿namespace Api_CQRS.Queries
{
    using Api_CQRS.Models;
    using MediatR;

    public class GetProdutoPorIdQuery : IRequest<Produto>
    {
        public int Id { get; set; }
    }
}
