﻿namespace Api_CQRS.CommandHandlers
{
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Api_CQRS.Commands;
    using Api_CQRS.Context;
    using Api_CQRS.Models;
    using MediatR;

    public class ProdutoUpdateCommandHandler : IRequestHandler<ProdutoUpdateCommand, Produto>
    {
        private readonly AppDbContext context;

        public ProdutoUpdateCommandHandler(AppDbContext context)
        {
            this.context = context;
        }

        public async Task<Produto> Handle(ProdutoUpdateCommand request, CancellationToken cancellationToken)
        {
            var produto = context.Produtos.Where(where => where.Id == request.Id).FirstOrDefault();

            if (produto == null)
            {
                return default;
            }
            else
            {
                produto.CodigoBarras = request.CodigoBarras;
                produto.Nome = request.Nome;
                produto.Preco = request.Preco;
                produto.Taxa = request.Taxa;
                produto.Descricao = request.Descricao;

                await context.SaveChangesAsync();
                return produto;
            }
        }
    }
}
