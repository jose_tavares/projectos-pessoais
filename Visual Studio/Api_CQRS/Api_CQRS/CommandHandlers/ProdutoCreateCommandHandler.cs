﻿namespace Api_CQRS.CommandHandlers
{
    using System.Threading;
    using System.Threading.Tasks;
    using Api_CQRS.Commands;
    using Api_CQRS.Context;
    using Api_CQRS.Models;
    using MediatR;

    public class ProdutoCreateCommandHandler : IRequestHandler<ProdutoCreateCommand, Produto>
    {
        private readonly AppDbContext context;

        public ProdutoCreateCommandHandler(AppDbContext context)
        {
            this.context = context;
        }

        public async Task<Produto> Handle(ProdutoCreateCommand request, CancellationToken cancellationToken)
        {
            var produto = new Produto
            {
                CodigoBarras = request.CodigoBarras,
                Nome = request.Nome,
                Preco = request.Preco,
                Taxa = request.Taxa,
                Descricao = request.Descricao
            };

            context.Produtos.Add(produto);
            await context.SaveChangesAsync();

            return produto;
        }
    }
}
