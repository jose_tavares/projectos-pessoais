﻿namespace Api_CQRS.CommandHandlers
{
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Api_CQRS.Commands;
    using Api_CQRS.Context;
    using Api_CQRS.Models;
    using MediatR;
    using Microsoft.EntityFrameworkCore;

    public class ProdutoDeleteCommandHandler : IRequestHandler<ProdutoDeleteCommand, Produto>
    {
        private readonly AppDbContext context;

        public ProdutoDeleteCommandHandler(AppDbContext context)
        {
            this.context = context;
        }

        public async Task<Produto> Handle(ProdutoDeleteCommand request, CancellationToken cancellationToken)
        {
            var produto = await context.Produtos.Where(where => where.Id == request.Id).FirstOrDefaultAsync();
            context.Remove(produto);

            await context.SaveChangesAsync();
            return produto;
        }
    }
}
