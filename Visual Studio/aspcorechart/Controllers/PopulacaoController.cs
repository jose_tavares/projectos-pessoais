﻿using aspcorechart.Services;
using Microsoft.AspNetCore.Mvc;
using static aspcorechart.Models.Enums;

namespace aspcorechart.Controllers
{
    public class PopulacaoController : Controller
    {
        public IActionResult Index(int tipoGrafico = (int)TipoGrafico.Coluna) => View(tipoGrafico);

        public JsonResult PopulacaoGrafico(int tipoGrafico) =>
            Json(new
            {
                data = PopulacaoService.GetPopulacaoPorEstado(),
                tipoGrafico
            });
    }
}
