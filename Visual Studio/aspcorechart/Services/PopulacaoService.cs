﻿using aspcorechart.Models;
using System.Collections.Generic;

namespace aspcorechart.Services
{
    public class PopulacaoService
    {
        public static List<PopulacaoModel> GetPopulacaoPorEstado()
        {
            return new List<PopulacaoModel>
            {
                new PopulacaoModel { Cidade="São Paulo", Populacao2017 = 45094 , Populacao2010 = 39585 },
                new PopulacaoModel { Cidade="Minas Gerais", Populacao2017 = 21119 , Populacao2010 = 16715 },
                new PopulacaoModel { Cidade="Rio de Janeiro", Populacao2017 = 16718 , Populacao2010 = 15464 },
                new PopulacaoModel { Cidade="Bahia", Populacao2017 = 15344 , Populacao2010 = 10120 },
                new PopulacaoModel { Cidade="Parana", Populacao2017 = 11320 , Populacao2010 = 8912 }
            };
        }
    }
}