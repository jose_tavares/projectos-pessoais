﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

google.charts.load('current', { packages: ['corechart', 'bar'] });
google.charts.setOnLoadCallback(CarregaDados);

function CarregaDados() {
    $.ajax({
        url: $('#chart_div').data("url"),
        dataType: "json",
        type: "GET",
        error: function (xhr, status, error) {
            var result = eval("(" + xhr.responseText + ")");
            toastr.error(result.message);
        },
        success: function (result) {
            GraficoPopulacional(result);
            return false;
        }
    });

    return false;
}

function GraficoPopulacional(result) {
    var dataArray = [
        ['Cidade', '2010', '2017']
    ];

    $.each(result.data, function (i, item) {
        dataArray.push([item.cidade, item.populacao2010, item.populacao2017])
    });

    var data = google.visualization.arrayToDataTable(dataArray);

    var options = {
        title: 'Brasil - População das 5 Cidades mais populosas',
        chartArea: {
            width: '50%'
        },
        colors: ['#b0120a', '#ffab91'],
        hAxis: {
            title: 'Populacao Total (em milhões)',
            minValue: 0
        },
        vAxis: {
            title: 'Cidade'
        }
    };

    var chart =
        (result.tipoGrafico == 1) ?
            new google.visualization.BarChart(document.getElementById('chart_div')) :
            (result.tipoGrafico == 2) ?
                new google.visualization.LineChart(document.getElementById('chart_div')) :
                new google.visualization.ColumnChart(document.getElementById('chart_div'));

    chart.draw(data, options);
    return false;
}