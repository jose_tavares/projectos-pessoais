﻿using System.Windows;
using System.Globalization;
using Wpf_CalcPagamentos.Model;

namespace Wpf_CalcPagamentos
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnCalcula_Click(object sender, RoutedEventArgs e)
        {
            // Obtém os dados dos parâmetros.
            decimal saldo = decimal.Parse(txtSaldoInicial.Text, NumberStyles.Any);
            decimal taxa_juros = decimal.Parse(txtTaxaJuros.Text.Replace("%", "")) / 100 / 12;
            decimal pagamento_percentual = decimal.Parse(txtPercentualPagamento.Text.Replace("%", "")) / 100;
            decimal pagamento_minimo = decimal.Parse(txtPagamentoMinimo.Text, NumberStyles.Any);

            lblTotalPagamentos.Content = null;
            decimal total_pagamentos = 0;

            // Exibe o saldo inicial
            lvwPagamentos.Items.Clear();
            Amortizacao data = new Amortizacao()
            {
                Periodo = "0",
                Pagamento = null,
                Juros = null,
                Saldo = saldo.ToString("c"),
            };

            lvwPagamentos.Items.Add(data);

            // Percorre ate o que o saldo seja zero (saldo == 0.)
            for (int i = 1; saldo > 0; i++)
            {
                // Calcula o Pagamento
                decimal pagamento = saldo * pagamento_percentual;
                if (pagamento < pagamento_minimo) pagamento = pagamento_minimo;

                // Calcula o juros
                decimal juro = saldo * taxa_juros;
                saldo += juro;

                // Verifica se podemos liquidar o saldo
                if (pagamento > saldo) pagamento = saldo;
                total_pagamentos += pagamento;
                saldo -= pagamento;

                // Exibe os resultados
                data = new Amortizacao()
                {
                    Periodo = i.ToString(),
                    Pagamento = pagamento.ToString("c"),
                    Juros = juro.ToString("c"),
                    Saldo = saldo.ToString("c"),
                };

                lvwPagamentos.Items.Add(data);
            }

            // Exibe o pagamento total
            lblTotalPagamentos.Content = total_pagamentos.ToString("c");
        }
    }
}
