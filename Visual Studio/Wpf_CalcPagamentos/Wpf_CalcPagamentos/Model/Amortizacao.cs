﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wpf_CalcPagamentos.Model
{
    public class Amortizacao
    {
        public string Periodo { get; set; }

        public string Pagamento { get; set; }

        public string Juros { get; set; }

        public string Saldo { get; set; }
    }
}
