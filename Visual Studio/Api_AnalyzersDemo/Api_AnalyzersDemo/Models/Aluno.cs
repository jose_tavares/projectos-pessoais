﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api_AnalyzersDemo.Models
{
    public class Aluno
    {
        public int AlunoId { get; set; }

        public string Nome { get; set; }

        public int Idade { get; set; }
    }
}
