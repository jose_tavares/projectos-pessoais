﻿using Microsoft.Owin;
using Owin;
using System;

[assembly: OwinStartup(typeof(OWIN_Katana.Startup1))]

namespace OWIN_Katana
{
    public class Startup1
    {
        public void Configuration(IAppBuilder app)
        {
            // New code: Add the error page middleware to the pipeline. 
            app.UseErrorPage();

            app.Run(context =>
            {
                // New code: Throw an exception for this URI path.
                if (context.Request.Path.Equals(new PathString("/fail")))
                {
                    throw new Exception("Random exception");
                }

                context.Response.ContentType = "text/plain";
                return context.Response.WriteAsync("Hello, world.");
            });
        }
    }
}
