﻿namespace FuncionariosWeb.Context
{
    using Microsoft.AspNetCore.Identity;

    public class ApplicationUser : IdentityUser
    {
        public string Cidade { get; set; }
    }
}