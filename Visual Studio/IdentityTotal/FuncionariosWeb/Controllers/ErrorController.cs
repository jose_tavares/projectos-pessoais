﻿namespace FuncionariosWeb.Controllers
{
    using FuncionariosWeb.ViewModels;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Diagnostics;
    using Microsoft.AspNetCore.Mvc;

    public class ErrorController : Controller
    {
        [AllowAnonymous]
        [Route("Error")]
        public IActionResult Error()
        {
            //// Retorna detalhes da exceção
            var exceptionHandlerPathFeature = HttpContext.Features.Get<IExceptionHandlerPathFeature>();

            return View("Error", new ErrorViewModel
            {
                ExceptionMessage = exceptionHandlerPathFeature.Error.Message,
                ExceptionPath = exceptionHandlerPathFeature.Path,
                StackTrace = exceptionHandlerPathFeature.Error.StackTrace
            });
        }
    }
}
