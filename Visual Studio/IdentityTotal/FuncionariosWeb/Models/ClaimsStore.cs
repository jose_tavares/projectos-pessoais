﻿namespace FuncionariosWeb.Models
{
    using System.Collections.Generic;
    using System.Security.Claims;

    public static class ClaimsStore
    {
        public static List<Claim> AllClaims { get; } = new List<Claim>()
        {
            new Claim("Cria Role", "Cria Role"),
            new Claim("Edita Role", "Edita Role"),
            new Claim("Apaga Role", "Apaga Role")
        };
    }
}
