﻿using System.ComponentModel.DataAnnotations;

namespace FuncionariosWeb.ViewModels
{
    public class LoginViewModel
    {
        [EmailAddress(ErrorMessage = "Email inválido")]
        [Required(ErrorMessage = "O email é obrigatório")]
        public string Email { get; set; }

        [DataType(DataType.Password)]
        [Required(ErrorMessage = "A senha é obrigatória")]
        public string Password { get; set; }

        [Display(Name = "Lembrar-me")]
        public bool RememberMe { get; set; }

        public string ReturnUrl { get; set; }
    }
}
