﻿namespace FuncionariosWeb.ViewModels
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class EditUserViewModel
    {
        public string Id { get; set; }

        [Required(ErrorMessage = "O nome do utilizador é obrigatório")]
        public string UserName { get; set; }

        [EmailAddress(ErrorMessage = "Email inválido")]
        [Required(ErrorMessage = "O nome email é obrigatório")]
        public string Email { get; set; }

        public string Cidade { get; set; }

        public IList<string> Claims { get; set; }

        public IList<string> Roles { get; set; }
    }
}
