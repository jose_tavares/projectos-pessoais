﻿namespace FuncionariosWeb.ViewModels
{
    using System.Collections.Generic;

    public class UserClaimsViewModel
    {
        public string UserId { get; set; }

        public List<UserClaim> Claims { get; } = new List<UserClaim>();
    }
}
