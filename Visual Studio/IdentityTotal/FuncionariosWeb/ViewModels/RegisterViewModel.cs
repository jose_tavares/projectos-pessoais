﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace FuncionariosWeb.ViewModels
{
    public class RegisterViewModel
    {
        [Required]
        [EmailAddress]
        [Remote(action: "IsEmailInUse", controller: "Account")]
        public string Email { get; set; }
        
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        
        [DataType(DataType.Password)]
        [Display(Name = "Confirme a palavra-chave")]
        [Compare("Password", ErrorMessage = "As palavras-chaves não conferem")]
        public string ConfirmPassword { get; set; }

        public string Cidade { get; set; }
    }
}
