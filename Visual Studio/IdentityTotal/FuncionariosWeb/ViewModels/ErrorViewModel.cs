﻿namespace FuncionariosWeb.ViewModels
{
    public class ErrorViewModel
    {
        public string ExceptionTitle { get; set; }

        public string ExceptionMessage { get; set; }

        public string ExceptionPath { get; set; }

        public string StackTrace { get; set; }
    }
}
