﻿namespace FuncionariosWeb.Validation
{
    using System.ComponentModel.DataAnnotations;

    public class ValidaDominioEmailAttribute : ValidationAttribute
    {
        private readonly string dominioPermitido;

        public ValidaDominioEmailAttribute(string dominioPermitido)
        {
            this.dominioPermitido = dominioPermitido;
        }

        public override bool IsValid(object value)
        {
            string[] strings = value.ToString().Split('@');
            return strings[1].ToUpper() == dominioPermitido.ToUpper();
        }
    }
}
