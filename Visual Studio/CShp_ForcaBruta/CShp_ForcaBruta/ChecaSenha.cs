﻿using System.Linq;

namespace CShp_ForcaBruta
{
    public class ChecaSenha
    {
        public bool Verifica(string senha)
        {
            //min 6 e max 15 caracteres
            if (senha.Length < 6 || senha.Length > 15)
            {
                return false;
            }

            //Sem espaços em branco
            if (senha.Contains(" "))
            {
                return false;
            }

            //Pelo menos um caracter caixa alta
            if (!senha.Any(char.IsUpper))
            {
                return false;
            }

            //Pelo menos um caracter caixa baixa
            if (!senha.Any(char.IsLower))
            {
                return false;
            }

            //Não permite dois caracteres iguais sequencialmente
            for (int i = 0; i < senha.Length - 1; i++)
            {
                if (senha[i] == senha[i + 1])
                {
                    return false;
                }
            }

            //Pelo menos um caractere especial
            string specialCharacters = @"%!@#$%^&*()?/>.<,:;'\|}]{[_~`+=-" + "\"";
            char[] specialCharactersArray = specialCharacters.ToCharArray();

            foreach (char c in specialCharactersArray)
            {
                if (senha.Contains(c))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
