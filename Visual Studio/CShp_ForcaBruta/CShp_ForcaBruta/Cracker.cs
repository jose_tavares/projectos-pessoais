﻿namespace CShp_ForcaBruta
{
    public class Cracker
    {
        public char Primeirochar { get; } = 'a';

        public char Ultimochar { get; } = 'z';

        public int TamanhoSenha { get; set; } = 8;

        public long Tentativas { get; private set; } = 0;

        public bool Concluido { get; private set; } = false;

        public string Password { get; set; } = "abc";

        public void QuebraSenha(string chaves)
        {
            if (chaves == Password)
            {
                Concluido = true;
            }

            if (chaves.Length == TamanhoSenha || Concluido == true)
            {
                return;
            }

            for (char c = Primeirochar; c <= Ultimochar; c++)
            {
                Tentativas++;
                QuebraSenha(chaves + c);
            }
        }
    }
}
