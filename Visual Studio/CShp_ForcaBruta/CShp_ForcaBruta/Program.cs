﻿using System;
using System.Diagnostics;

namespace CShp_ForcaBruta
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Teste de Força Bruta");
            Console.Write("\nInforme uma senha  > ");

            Cracker cracker = new Cracker
            {
                Password = Convert.ToString(Console.ReadLine()).ToLower()
            };

            Console.WriteLine("\nQuebrando a senha...");
            Stopwatch timer = Stopwatch.StartNew();

            cracker.TamanhoSenha = cracker.Password.Length;
            cracker.QuebraSenha(string.Empty);

            timer.Stop();
            long elapsedMs = timer.ElapsedMilliseconds;
            double tempoGasto = elapsedMs / 1000;

            if (tempoGasto > 0)
            {
                Console.WriteLine("\n\nA senha foi hackeada! Estatísticas:");
                Console.WriteLine("----------------------------------");
                Console.WriteLine("Password: {0} {1}", cracker.Password, new ChecaSenha().Verifica(cracker.Password));
                Console.WriteLine("Tamanho Senha: {0}", cracker.TamanhoSenha);
                Console.WriteLine("Tentativas: {0}", cracker.Tentativas);
                string plural = "segundos";

                if (tempoGasto == 1)
                {
                    plural = "segundo";
                }

                Console.WriteLine("Tempo gasto para hackear a senha: {0} {1}", tempoGasto, plural);
                Console.WriteLine("Senhas por segundo : {0}", (long)(cracker.Tentativas / tempoGasto));
            }
            else
            {
                Console.WriteLine("\n\nA senha foi hackeada ! Estatísticas:");
                Console.WriteLine("----------------------------------");
                Console.WriteLine("Senha: {0}", cracker.Password);
                Console.WriteLine("Tamnho da Senha: {0}", cracker.TamanhoSenha);
                Console.WriteLine("Tentativas: {0}", cracker.Tentativas);
                Console.WriteLine("Tempo para hackear : {0} segundos", tempoGasto);
            }

            Console.ReadKey();
        }
    }
}
