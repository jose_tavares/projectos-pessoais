﻿using System;
using System.Globalization;
using System.Collections.Generic;

namespace CShp_CultureStrings
{
    class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine($"O valor de CurrentCulture é {CultureInfo.CurrentCulture.Name}.");

            //var data = new DateTime(2020, 9, 1);
            //var ptbrData = data.ToString("d");
            //Console.WriteLine($"{ptbrData}");

            //var eslovenaCultura = CultureInfo.GetCultureInfo("sl-SI");
            //Console.WriteLine($"Cultura Eslovênia = {eslovenaCultura}");

            //var slsiData = data.ToString("d", eslovenaCultura);
            //Console.WriteLine($"{slsiData}");

            //CultureNumeros();

            //var eslovenaCulture = CultureInfo.GetCultureInfo("sl-SI");
            //var temperatura = 21.5;
            //var timestamp = new DateTime(2018, 9, 1, 16, 15, 30);
            //var formatString = "Temperatura: {0} °C em {1} ";
            //var formatopt_br = string.Format(formatString, temperatura, timestamp);
            //var formatosl_si = string.Format(eslovenaCulture, formatString, temperatura, timestamp);

            //Console.WriteLine($"cultura {CultureInfo.CurrentCulture.Name}");
            //Console.WriteLine(formatopt_br);
            //Console.WriteLine($"cultura {eslovenaCulture}");
            //Console.WriteLine(formatosl_si);

            //Console.WriteLine($"Cultura atual = {CultureInfo.CurrentCulture.Name}.");

            //var doubleS = "ss";
            //var eszett = "ß";   //O eszett representa o fonema s no Alemão
            //var equalOrdinal = string.Equals(doubleS, eszett); // = false
            //Console.WriteLine($"{equalOrdinal}");

            //// Mudando a cultura atual para Alemão
            //CultureInfo.CurrentCulture = new CultureInfo("de-DE", false);
            //Console.WriteLine($"Cultura atual = {CultureInfo.CurrentCulture.Name}.");

            //var equalCulture = string.Equals(doubleS, eszett, StringComparison.CurrentCulture); // = true
            //Console.WriteLine($"{equalCulture}");

            var czCulture = CultureInfo.GetCultureInfo("cs-CZ");
            var palavras = new[] { "channel", "double" };
            var ordinalSortedSet = new SortedSet<string>(palavras);
            Console.WriteLine(">>ordinal");
            
            foreach (var palavra in ordinalSortedSet)
            {
                Console.WriteLine(palavra);
            }

            var cultureSortedSet = new SortedSet<string>(palavras, StringComparer.Create(czCulture, ignoreCase: true));
            Console.WriteLine($"Cultura = {czCulture}");
            Console.WriteLine(">>ignoreCase");
            
            foreach (var palavra in cultureSortedSet)
            {
                Console.WriteLine(palavra);
            }
        }

        private static void CultureNumeros()
        {
            Console.WriteLine($"O valor de CurrentCulture é {CultureInfo.CurrentCulture.Name}.");

            var pi = 3.14;
            var ptbrPi = pi.ToString(); // = "3,14
            Console.WriteLine($"{ptbrPi}");

            var enusCulture = CultureInfo.GetCultureInfo("en-US");
            Console.WriteLine($"Cultura Americana = {enusCulture}");

            var enusPi = pi.ToString(enusCulture); // = "3.14
            Console.WriteLine($"{enusPi}");
        }
    }
}
