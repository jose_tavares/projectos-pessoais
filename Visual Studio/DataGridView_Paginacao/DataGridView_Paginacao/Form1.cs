﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Drawing;

namespace DataGridView_Paginacao
{
    public partial class Form1 : Form
    {
        private DataTable dt;
        private int regInicio;
        private int registros;
        private DataSet dsPaginado;
        private SqlDataAdapter daPaginacao;
        private int quantidadeRegistrosPaginar;

        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void txtQuantidadeRegistros_TextChanged(object sender, EventArgs e)
        {

        }

        private void dgvDados_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnCarregarDados_Click(object sender, EventArgs e)
        {
            try
            {
                quantidadeRegistrosPaginar = Convert.ToInt32(txtQuantidadeRegistros.Text);

                var connectionString = @"Data Source=np:\\.\pipe\LOCALDB#3A081617\tsql\query;Initial Catalog=NORTHWND;Integrated Security=True";
                var sql = "SELECT * FROM Products";
                var connection = new SqlConnection(connectionString);

                daPaginacao = new SqlDataAdapter(sql, connectionString);
                dsPaginado = new DataSet();
                dt = new DataTable();

                connection.Open();
                daPaginacao.Fill(dt);
                registros = dt.Rows.Count;
                daPaginacao.Fill(dsPaginado, regInicio, quantidadeRegistrosPaginar, "Products");
                connection.Close();

                dgvDados.DataSource = dsPaginado;
                dgvDados.DataMember = "Products";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnProximo_Click(object sender, EventArgs e)
        {
            regInicio += quantidadeRegistrosPaginar;

            if (regInicio >= registros)
            {
                regInicio = registros - quantidadeRegistrosPaginar;
            }

            dsPaginado.Clear();
            daPaginacao.Fill(dsPaginado, regInicio, quantidadeRegistrosPaginar, "Products");
        }

        private void btnAnterior_Click(object sender, EventArgs e)
        {
            regInicio -= quantidadeRegistrosPaginar;

            if (regInicio <= 0)
            {
                regInicio = 0;
            }

            dsPaginado.Clear();
            daPaginacao.Fill(dsPaginado, regInicio, quantidadeRegistrosPaginar, "Products");
        }
    }
}
