﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.Diagnostics;
using TraductionsTest.Services;
using System.Collections.Generic;
using TraductionsTest.ViewModels;

namespace TraductionsTest.Controllers
{
    public class HomeController : Controller
    {
        private readonly IService _service;

        public HomeController(IService service) =>
            _service = service;

        #region Public Methods
        public ActionResult Index()
        {
            var translationsResultsTimesViewModel = new TranslationsResultsTimesViewModel();
            var productsApi = _service.GetProducts().ToList();

            if ((productsApi != null) && productsApi.Any())
            {
                translationsResultsTimesViewModel.IsCreated = true;
                translationsResultsTimesViewModel.ProductsLength = productsApi.Count();

                translationsResultsTimesViewModel.CurrentElapsed = CurrentFunctionToTestSpeed(productsApi);
                CleanNameTranslated(productsApi);

                translationsResultsTimesViewModel.EfByColumnElapsed = EFByColumnFunctionToTestSpeed(productsApi);
                CleanNameTranslated(productsApi);

                translationsResultsTimesViewModel.DapperElapsed = DapperFunctionToTestSpeed(productsApi);
                CleanNameTranslated(productsApi);

                translationsResultsTimesViewModel.DapperByColumnElapsed = DapperByColumnFunctionToTestSpeed(productsApi);
                CleanNameTranslated(productsApi);
            }

            return View(translationsResultsTimesViewModel);
        }

        [HttpPost]
        public ActionResult Index(string submitButton, int totalProducts)
        {
            switch (submitButton)
            {
                case "Create":
                    if (!_service.Add(totalProducts))
                        throw new Exception();

                    break;

                case "Delete":
                    if (!_service.Delete())
                        throw new Exception();

                    break;

                default:
                    break;
            }

            return RedirectToAction("Index");
        }
        #endregion

        #region Private Methods To Test Speed
        private double CurrentFunctionToTestSpeed(List<ProductViewModel> products)
        {
            var watch = Stopwatch.StartNew();

            products.ForEach(each =>
                each.NameTranslated = _service.EFTranslationValue(each.Name, "Portuguese")
            );

            watch.Stop();

            return watch.Elapsed.TotalSeconds;
        }

        private double EFByColumnFunctionToTestSpeed(List<ProductViewModel> products)
        {
            var watch = Stopwatch.StartNew();

            products.ForEach(each =>
                each.NameTranslated = _service.EFTranslationByColumnNameValue(each.Name)
            );

            watch.Stop();

            return watch.Elapsed.TotalSeconds;
        }

        private double DapperFunctionToTestSpeed(List<ProductViewModel> products)
        {
            var watch = Stopwatch.StartNew();

            products.ForEach(each =>
                each.NameTranslated = _service.DapperTranslationValue(each.Name, "Portuguese")
            );

            watch.Stop();

            return watch.Elapsed.TotalSeconds;
        }

        private double DapperByColumnFunctionToTestSpeed(List<ProductViewModel> products)
        {
            var watch = Stopwatch.StartNew();

            products.ForEach(each =>
                each.NameTranslated = _service.DapperTranslationByColumnNameValue(each.Name)
            );

            watch.Stop();

            return watch.Elapsed.TotalSeconds;
        }
        #endregion

        private void CleanNameTranslated(List<ProductViewModel> products)
        {
            products.ForEach(each =>
                each.NameTranslated = string.Empty
            );
        }
    }
}