﻿using System;

namespace TraductionsTest.ViewModels
{
    public class ProductViewModel
    {
        public Guid ProductId { get; set; }

        public string Name { get; set; }

        public string NameTranslated { get; set; }
    }
}