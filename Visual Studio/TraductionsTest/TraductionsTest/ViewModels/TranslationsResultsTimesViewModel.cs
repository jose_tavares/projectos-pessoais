﻿namespace TraductionsTest.ViewModels
{
    public class TranslationsResultsTimesViewModel
    {
        public bool IsCreated { get; set; }

        public int ProductsLength { get; set; }

        public double CurrentElapsed { get; set; }

        public double EfByColumnElapsed { get; set; }

        public double DapperElapsed { get; set; }

        public double DapperByColumnElapsed { get; set; }
    }
}