﻿using System;
using System.Linq;
using System.Net.Http;
using System.Collections.Generic;
using TraductionsTest.ViewModels;

namespace TraductionsTest.Services
{
    public class Service : IService
    {
        private readonly HttpClient _client = new HttpClient
        {
            BaseAddress = new Uri("https://localhost:44376/api/")
        };

        public bool Add(int total)
        {
            _client.Timeout = TimeSpan.FromMinutes(30);

            var responseTask = _client.GetAsync("add?total=" + total);

            responseTask.Wait();

            var result = responseTask.Result;

            if (result.IsSuccessStatusCode)
            {
                return true;
            }

            return false;
        }

        public bool Delete()
        {
            var responseTask = _client.GetAsync("delete");

            responseTask.Wait();

            var result = responseTask.Result;

            if (result.IsSuccessStatusCode)
            {
                return true;
            }

            return false;
        }

        public IEnumerable<ProductViewModel> GetProducts()
        {
            var responseTask = _client.GetAsync("products");
            responseTask.Wait();

            var result = responseTask.Result;

            if (result.IsSuccessStatusCode)
            {
                var readTask = result.Content.ReadAsAsync<IEnumerable<ProductViewModel>>();
                readTask.Wait();

                return readTask.Result;
            }

            return Enumerable.Empty<ProductViewModel>();
        }

        public string EFTranslationValue(string fieldId, string language)
        {
            var responseTask = _client.GetAsync("translation/ef/" + fieldId + "/" + language);
            responseTask.Wait();

            var result = responseTask.Result;

            if (result.IsSuccessStatusCode)
            {
                var readTask = result.Content.ReadAsAsync<string>();
                readTask.Wait();

                return readTask.Result;
            }

            return null;
        }

        public string EFTranslationByColumnNameValue(string fieldId)
        {
            var responseTask = _client.GetAsync("translationByColumnName/ef/" + fieldId);
            responseTask.Wait();

            var result = responseTask.Result;

            if (result.IsSuccessStatusCode)
            {
                var readTask = result.Content.ReadAsAsync<string>();
                readTask.Wait();

                return readTask.Result;
            }

            return null;
        }

        public string DapperTranslationValue(string fieldId, string language)
        {
            var responseTask = _client.GetAsync("translation/dapper/" + fieldId + "/" + language);
            responseTask.Wait();

            var result = responseTask.Result;

            if (result.IsSuccessStatusCode)
            {
                var readTask = result.Content.ReadAsAsync<string>();
                readTask.Wait();

                return readTask.Result;
            }

            return null;
        }

        public string DapperTranslationByColumnNameValue(string fieldId)
        {
            var responseTask = _client.GetAsync("translationByColumnName/dapper/" + fieldId);
            responseTask.Wait();

            var result = responseTask.Result;

            if (result.IsSuccessStatusCode)
            {
                var readTask = result.Content.ReadAsAsync<string>();
                readTask.Wait();

                return readTask.Result;
            }

            return null;
        }
    }
}