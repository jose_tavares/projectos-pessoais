﻿using System.Collections.Generic;
using TraductionsTest.ViewModels;

namespace TraductionsTest.Services
{
    public interface IService
    {
        bool Add(int total);

        bool Delete();

        IEnumerable<ProductViewModel> GetProducts();

        string EFTranslationValue(string fieldId, string language);

        string EFTranslationByColumnNameValue(string fieldId);

        string DapperTranslationValue(string fieldId, string language);

        string DapperTranslationByColumnNameValue(string fieldId);
    }
}
