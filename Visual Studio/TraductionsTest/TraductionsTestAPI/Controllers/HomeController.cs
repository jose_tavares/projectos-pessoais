﻿using System.Web.Http;
using TraductionsTestAPI.Repositories;

namespace TraductionsTestAPI.Controllers
{
    public class HomeController : ApiController
    {
        private readonly IRepository _repository;

        public HomeController(IRepository repository) =>
            _repository = repository;

        [HttpGet]
        [Route("api/products")]
        public IHttpActionResult GetProducts() =>
            Ok(_repository.GetProducts());

        [HttpGet]
        [Route("api/translation/ef/{fieldId}/{language}")]
        public IHttpActionResult EFTranslation(string fieldId, string language) =>
            Ok(_repository.EFTranslationValue(fieldId, language));

        [HttpGet]
        [Route("api/translationByColumnName/ef/{fieldId}")]
        public IHttpActionResult EFTranslationByColumnName(string fieldId) =>
            Ok(_repository.EFTranslationByColumnNameValue(fieldId));

        [HttpGet]
        [Route("api/translation/dapper/{fieldId}/{language}")]
        public IHttpActionResult DapperTranslation(string fieldId, string language) =>
            Ok(_repository.DapperTranslationValue(fieldId, language));

        [HttpGet]
        [Route("api/translationByColumnName/dapper/{fieldId}")]
        public IHttpActionResult DapperTranslationByColumnName(string fieldId) =>
            Ok(_repository.DapperTranslationByColumnNameValue(fieldId));

        [HttpGet]
        [Route("api/add")]
        public IHttpActionResult Add(int total)
        {
            if (total < 1)
            {
                return BadRequest("Introduza pelo menos 1 produto.");
            }

            _repository.Add(total);

            var result = (total > 1) ?
                $"Foram criados {total} produtos com sucesso." :
                $"Foi criado {total} produto com sucesso.";

            return Ok(result);
        }

        [HttpGet]
        [Route("api/delete")]
        public IHttpActionResult Delete()
        {
            _repository.Delete();

            return Ok("Produtos e traduções apagados com sucesso.");
        }
    }
}
