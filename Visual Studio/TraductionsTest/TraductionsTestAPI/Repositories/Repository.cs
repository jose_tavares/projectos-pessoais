﻿using System;
using Dapper;
using System.Linq;
using System.Configuration;
using System.Data.SqlClient;
using TraductionsTestAPI.Models;
using System.Collections.Generic;

namespace TraductionsTestAPI.Repositories
{
    public class Repository : IRepository
    {
        private readonly TranslationsTestEntities _context = new TranslationsTestEntities();
        private readonly SqlConnection _dapperContext = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);

        public void Add(int total)
        {
            for (int i = 0; i < total; i++)
            {
                var newFieldId = Guid.NewGuid().ToString();
                AddTranslations(i, newFieldId);

                var product = new Products
                {
                    ProductId = Guid.NewGuid(),
                    Name = newFieldId
                };

                _context.Products.Add(product);

                AddTranslationsByColumnName(i, product);
            }

            _context.SaveChanges();
        }

        public IEnumerable<Products> GetProducts()
            => _context.Products.AsEnumerable();

        public string EFTranslationValue(string fieldId, string language)
            => _context.Translations.Single(single => (single.FieldId == fieldId) && (single.Language == language)).Value;

        public string EFTranslationByColumnNameValue(string fieldId)
            => _context.TranslationsByColumnName.Single(single => single.FieldId.ToString() == fieldId).Portuguese;

        public string DapperTranslationValue(string fieldId, string language) =>
            _dapperContext.Query<string>
                ("SELECT [Value] FROM [TranslationsTest].[dbo].[Translations] WHERE [FieldId]=@fieldId AND [Language]=@language",
                new { fieldId, language })
            .First();

        public string DapperTranslationByColumnNameValue(string fieldId) =>
            _dapperContext.Query<string>
                ("SELECT [Portuguese] FROM [TranslationsTest].[dbo].[TranslationsByColumnName] WHERE [FieldId]=@fieldId",
                new { fieldId })
            .First();

        public void Delete()
        {
            _context.Database.ExecuteSqlCommand("DELETE FROM [TranslationsTest].[dbo].[Products]");
            _context.Database.ExecuteSqlCommand("DELETE FROM [TranslationsTest].[dbo].[Translations]");
            _context.Database.ExecuteSqlCommand("DELETE FROM [TranslationsTest].[dbo].[TranslationsByColumnName]");

            _context.SaveChanges();
        }

        private void AddTranslations(int i, string newFieldId)
        {
            string languageName = string.Empty;

            for (int j = 0; j < 3; j++)
            {
                languageName = j == 0 ? "Portuguese" :
                                j == 1 ? "French" :
                                j == 2 ? "English" : string.Empty;

                var fieldId = new Translations
                {
                    TranslationsId = Guid.NewGuid(),
                    Language = languageName,
                    Value = "Produto " + (i + 1) + " - " + (j + 1),
                    FieldId = newFieldId
                };

                _context.Translations.Add(fieldId);
            }
        }

        private void AddTranslationsByColumnName(int i, Products product)
        {
            var fieldId = new TranslationsByColumnName
            {
                FieldId = Guid.Parse(product.Name),
                Portuguese = "Producto " + (i + 1) + " - 1",
                French = "Producto " + (i + 1) + " - 2",
                English = "Producto " + (i + 1) + " - 3",
            };

            _context.TranslationsByColumnName.Add(fieldId);
        }
    }
}