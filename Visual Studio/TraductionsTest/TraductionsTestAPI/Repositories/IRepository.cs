﻿using TraductionsTestAPI.Models;
using System.Collections.Generic;

namespace TraductionsTestAPI.Repositories
{
    public interface IRepository
    {
        void Add(int total);

        void Delete();

        IEnumerable<Products> GetProducts();

        string EFTranslationValue(string fieldId, string language);

        string EFTranslationByColumnNameValue(string fieldId);

        string DapperTranslationValue(string fieldId, string language);

        string DapperTranslationByColumnNameValue(string fieldId);
    }
}
