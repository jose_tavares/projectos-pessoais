﻿using BigPictureDemo.ViewModels;
using System.ComponentModel;
using Xamarin.Forms;

namespace BigPictureDemo.Views
{
    public partial class ItemDetailPage : ContentPage
    {
        public ItemDetailPage()
        {
            InitializeComponent();
            BindingContext = new ItemDetailViewModel();
        }
    }
}