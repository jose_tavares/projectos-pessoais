﻿namespace PadraoProjetoObserver
{
    public interface IObservador
    {
        void Atualizar(ISujeito sujeito);
    }
}
