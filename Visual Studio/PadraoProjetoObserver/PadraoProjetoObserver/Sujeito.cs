﻿namespace PadraoProjetoObserver
{
    public interface ISujeito
    {
        void NotificarObservadores();

        void RegistarObservador(IObservador o);

        void RemoverObservador(IObservador o);

        void AlterarEdicao();

        bool GetEdicao();
    }
}
