﻿using System;
using System.Collections.Generic;

namespace PadraoProjetoObserver
{
    public class EditoraConcreta : ISujeito
    {
        private bool _novaEdicao = false;
        private readonly List<IObservador> _observadores = new List<IObservador>();

        public void NotificarObservadores()
        {
            foreach (var o in _observadores)
            {
                o.Atualizar(this);
            }
        }

        public void RegistarObservador(IObservador o)
        {
            _observadores.Add(o);
        }

        public void RemoverObservador(IObservador o)
        {
            _observadores.Remove(o);
        }

        public void AlterarEdicao()
        {
            if (_novaEdicao)
            {
                _novaEdicao = false;
            }
            else
            {
                _novaEdicao = true;
            }

            NotificarObservadores();
        }

        public bool GetEdicao()
        {
            return _novaEdicao;
        }
    }
}
