﻿using System;

namespace PadraoProjetoObserver
{
    public class AssinanteConcreto : IObservador
    {
        private ISujeito _objetoObservado;

        public AssinanteConcreto(ISujeito objetoObervado)
        {
            _objetoObservado = objetoObervado;
            _objetoObservado.RegistarObservador(this);
        }
        public void Atualizar(ISujeito sujeito)
        {
            if (sujeito == _objetoObservado)
            {
                Console.WriteLine("[Aviso] - A editora alterou o seu estado para : " + _objetoObservado.GetEdicao());
            }
        }
    }
}
