﻿using System;
using System.Linq;
using EFCore_Consultas.Models;
using Microsoft.EntityFrameworkCore;

namespace EFCore_Consultas
{
    class Program
    {
        static void Main(string[] args)
        {
            #region AsNoTracking
            //using var _context = new NORTHWNDContext();
            //var produtos = _context.Products.ToList();

            //foreach (var p in produtos)
            //{
            //    Console.WriteLine($"{p.ProductId} \t {p.ProductName} \t\t {p.UnitPrice}");
            //}

            //using var _context = new NORTHWNDContext();
            //var produtos = _context.Products.AsNoTracking().ToList();

            //foreach (var p in produtos)
            //{
            //    Console.WriteLine($"{p.ProductId} \t {p.ProductName} \t\t {p.UnitPrice}");
            //}
            #endregion

            #region Eager Loading
            //using var _context = new NORTHWNDContext();
            //var customer = _context.Customers
            //                       .Include(c => c.Orders)
            //                       .SingleOrDefault(c => c.CustomerId == "ANTON");
            //Console.WriteLine($"{customer.CustomerId} {customer.ContactName}");

            //foreach (var order in customer.Orders)
            //{
            //    Console.WriteLine($"{order.OrderId} {order.OrderDate}");
            //}
            #endregion

            #region Lazy Loading
            //using var _context = new NORTHWNDContext();
            //var customer = _context.Customers
            //    .SingleOrDefault(single => single.CustomerId == "ANTON");

            //Console.WriteLine($"{customer.CustomerId} {customer.ContactName}");

            //foreach (var order in customer.Orders)
            //{
            //    Console.WriteLine($"{order.OrderId} {order.OrderDate}");
            //}
            #endregion

            #region FromSqlInterpolated
            //using var _context = new NORTHWNDContext();
            //var preco = 39.50m;

            //var produtos =
            //  _context.Products
            //  .FromSqlInterpolated($"SELECT * FROM dbo.Products Where UnitPrice > {preco}")
            //  .ToList();
            //foreach (var p in produtos)
            //{
            //    Console.WriteLine($"{p.ProductId} {p.ProductName} {p.UnitPrice}");
            //}
            #endregion

            #region FromSqlRaw
            using var _context = new NORTHWNDContext();
            var preco = 39.50m;

            var produtos =
              _context.Products
              .FromSqlRaw("SELECT * FROM dbo.Products where UnitPrice > {0}", preco)
              .ToList();
            foreach (var p in produtos)
            {
                Console.WriteLine($"{p.ProductId} {p.ProductName} {p.UnitPrice}");
            }
            #endregion
        }
    }
}
