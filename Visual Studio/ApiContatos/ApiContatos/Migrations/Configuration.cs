﻿namespace ApiContatos.Migrations
{
    using ApiContatos.Models;
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<AppDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(AppDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method
            //  to avoid creating duplicate seed data.

            var endereco1 = new Endereco { EnderecoId = 1, Local = "Rua Projetada 100", Cidade = "Xerem", Estado = "Rio de Janeiro" };
            var endereco2 = new Endereco { EnderecoId = 2, Local = "Av. México", Cidade = "Santos", Estado = "São Paulo" };
            var endereco3 = new Endereco { EnderecoId = 3, Local = "Pça XV Novembro 100", Cidade = "Campinas", Estado = "São Paulo" };

            context.Enderecos.AddOrUpdate(addOrUpdate => addOrUpdate.EnderecoId,
                endereco1,
                endereco2,
                endereco3
            );

            context.Contatos.AddOrUpdate(addOrUpdate => addOrUpdate.ContatoId,
                new Contato { ContatoId = 1, Nome = "Macoratti", Email = "macoratti@yahoo.com", Telefone = "21-9985-6633", Endereco = endereco1 },
                new Contato { ContatoId = 2, Nome = "Maria Bueno", Email = "maria@uol.com.br", Telefone = "11-9580-6633", Endereco = endereco2 },
                new Contato { ContatoId = 3, Nome = "Marcia Silveira", Email = "marcia@hotmail.com", Telefone = "21-9985-0011", Endereco = endereco3 }
            );
        }
    }
}
