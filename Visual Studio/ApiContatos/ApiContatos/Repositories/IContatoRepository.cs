﻿using ApiContatos.Models;
using System.Collections.Generic;

namespace ApiContatos.Repositories
{
    public interface IContatoRepository
    {
        void Add(ContatoEnderecoDTO contato);

        bool Delete(int id);

        bool Edit(Contato contato);

        IEnumerable<Contato> Get(bool incluirEndereco);

        Contato Get(int id);

        IEnumerable<Contato> Get(string name);
    }
}
