﻿using System;
using System.Linq;
using ApiContatos.Models;
using System.Data.Entity;
using System.Collections.Generic;

namespace ApiContatos.Repositories
{
    public class ContatoRepository : IContatoRepository
    {
        private readonly AppDbContext _context = new AppDbContext();

        public void Add(ContatoEnderecoDTO contato)
        {
            using (_context)
            {
                _context.Contatos.Add(new Contato
                {
                    Nome = contato.Nome,
                    Email = contato.Email,
                    Telefone = contato.Telefone,
                    Endereco = new Endereco
                    {
                        Local = contato.Local,
                        Cidade = contato.Cidade,
                        Estado = contato.Estado
                    }
                });

                _context.SaveChanges();
            }
        }

        public bool Delete(int id)
        {
            using (_context)
            {
                var contatoSelecionado = _context.Contatos.FirstOrDefault(first => first.ContatoId == id);

                if (contatoSelecionado != null)
                {
                    _context.Entry(contatoSelecionado).State = EntityState.Deleted;

                    var enderecoSelecionado = _context.Enderecos.FirstOrDefault(first => first.EnderecoId == contatoSelecionado.EnderecoId);

                    if (enderecoSelecionado != null)
                    {
                        _context.Entry(enderecoSelecionado).State = EntityState.Deleted;
                    }

                    _context.SaveChanges();

                    return true;
                }
            }

            return false;
        }

        public bool Edit(Contato contato)
        {
            using (_context)
            {
                var contatoSelecionado = _context.Contatos.FirstOrDefault(first => first.ContatoId == contato.ContatoId);

                if (contatoSelecionado != null)
                {
                    contatoSelecionado.Nome = contato.Nome;
                    contatoSelecionado.Email = contato.Email;
                    contatoSelecionado.Telefone = contato.Telefone;

                    _context.Entry(contatoSelecionado).State = EntityState.Modified;

                    var enderecoSelecionado = _context.Enderecos.FirstOrDefault(first => first.EnderecoId == contato.Endereco.EnderecoId);

                    if (enderecoSelecionado != null)
                    {
                        enderecoSelecionado.Local = contato.Endereco.Local;
                        enderecoSelecionado.Cidade = contato.Endereco.Cidade;
                        enderecoSelecionado.Estado = contato.Endereco.Estado;

                        _context.Entry(enderecoSelecionado).State = EntityState.Modified;
                    }

                    _context.SaveChanges();

                    return true;
                }
            }

            return false;
        }

        public IEnumerable<Contato> Get(bool incluirEndereco)
        {
            using (_context)
            {
                return _context.Contatos.Include("Endereco").ToList()
                    .Select(select => new Contato
                    {
                        ContatoId = select.ContatoId,
                        Nome = select.Nome,
                        Email = select.Email,
                        Telefone = select.Telefone,
                        Endereco = !incluirEndereco || (select.Endereco == null) ? null : new Endereco
                        {
                            EnderecoId = select.Endereco.EnderecoId,
                            Local = select.Endereco.Local,
                            Cidade = select.Endereco.Cidade,
                            Estado = select.Endereco.Estado
                        }
                    });
            }
        }

        public Contato Get(int id)
        {
            using (_context)
            {
                var findContato = _context.Contatos.Include("Endereco").ToList()
                    .FirstOrDefault(first => first.ContatoId == id);

                if (findContato != null)
                {
                    return new Contato
                    {
                        ContatoId = findContato.ContatoId,
                        Nome = findContato.Nome,
                        Email = findContato.Email,
                        Telefone = findContato.Telefone,
                        Endereco = (findContato.Endereco == null) ? null : new Endereco
                        {
                            EnderecoId = findContato.Endereco.EnderecoId,
                            Local = findContato.Endereco.Local,
                            Cidade = findContato.Endereco.Cidade,
                            Estado = findContato.Endereco.Estado
                        }
                    };
                }
            }

            return null;
        }

        public IEnumerable<Contato> Get(string name)
        {
            using (_context)
            {
                return _context.Contatos.Include("Endereco").ToList()
                    .Where(where => where.Nome.ToLower().StartsWith(name.ToLower()))
                    .Select(select => new Contato
                    {
                        ContatoId = select.ContatoId,
                        Nome = select.Nome,
                        Email = select.Email,
                        Telefone = select.Telefone,
                        Endereco = (select.Endereco == null) ? null : new Endereco
                        {
                            EnderecoId = select.Endereco.EnderecoId,
                            Local = select.Endereco.Local,
                            Cidade = select.Endereco.Cidade,
                            Estado = select.Endereco.Estado
                        }
                    });
            }
        }
    }
}