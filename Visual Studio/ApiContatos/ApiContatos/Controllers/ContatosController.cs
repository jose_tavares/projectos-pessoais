﻿using System.Linq;
using System.Web.Http;
using ApiContatos.Models;
using System.Data.Entity;
using ApiContatos.Repositories;
using System.Collections.Generic;

namespace ApiContatos.Controllers
{
    public class ContatosController : ApiController
    {
        private readonly IContatoRepository _contatoRepository;

        public ContatosController(IContatoRepository contatoRepository) =>
            _contatoRepository = contatoRepository;

        [HttpGet]
        public IHttpActionResult GetTodosContatos(bool incluirEndereco = false)
        {
            var contatos = _contatoRepository.Get(incluirEndereco);

            if (contatos.Count() == 0)
            {
                return NotFound();
            }

            return Ok(contatos);
        }

        [HttpGet]
        public IHttpActionResult GetContatoPorId(int? id)
        {
            if (!id.HasValue)
            {
                return BadRequest("O Id do contato é inválido.");
            }

            var contato = _contatoRepository.Get(id.Value);

            if (contato == null)
            {
                return NotFound();
            }

            return Ok(contato);
        }

        [HttpGet]
        public IHttpActionResult GetContatoPorNome(string nome)
        {
            if (string.IsNullOrEmpty(nome))
            {
                return BadRequest("O nome do contato é inválido.");
            }

            var contatos = _contatoRepository.Get(nome);

            if (contatos.Count() == 0)
            {
                return NotFound();
            }

            return Ok(contatos);
        }

        [HttpPost]
        public IHttpActionResult PostNovoContato(ContatoEnderecoDTO contato)
        {
            if (!ModelState.IsValid || (contato == null))
            {
                return BadRequest("Dados do contato inválidos.");
            }

            _contatoRepository.Add(contato);

            return Ok(contato);
        }

        [HttpPut]
        public IHttpActionResult Put(Contato contato)
        {
            if (!ModelState.IsValid || (contato == null))
            {
                return BadRequest("Dados do contato inválidos.");
            }

            if (!_contatoRepository.Edit(contato))
            {
                return NotFound();
            }

            return Ok($"Contato {contato.Nome} atualizado com sucesso.");
        }

        [HttpDelete]
        public IHttpActionResult Delete(int? id)
        {
            if (id == null)
            {
                return BadRequest("Dados inválidos");
            }

            if (!_contatoRepository.Delete(id.Value))
            {
                return NotFound();
            }

            return Ok($"Contato {id} foi apagagado com sucesso.");
        }
    }
}
