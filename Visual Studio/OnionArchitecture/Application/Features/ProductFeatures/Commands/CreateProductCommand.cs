﻿namespace Application.Features.ProductFeatures.Commands
{
    using MediatR;

    public class CreateProductCommand : IRequest<int>
    {
        public string Name { get; set; }

        public string Barcode { get; set; }

        public string Description { get; set; }

        public decimal Rate { get; set; }
    }
}
