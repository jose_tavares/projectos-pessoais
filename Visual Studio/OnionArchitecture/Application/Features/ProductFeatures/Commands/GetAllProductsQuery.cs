﻿namespace Application.Features.ProductFeatures.Commands
{
    using Domain.Entities;
    using MediatR;
    using System.Collections.Generic;

    public class GetAllProductsQuery : IRequest<IEnumerable<Product>> { }
}
