﻿using MediatR;
using Domain.Entities;

namespace Application.Features.ProductFeatures.Commands
{
    public class GetProductByIdQuery : IRequest<Product>
    {
        public int Id { get; set; }
    }
}
