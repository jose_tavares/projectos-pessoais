﻿using MediatR;
using System.Linq;
using Domain.Entities;
using System.Threading;
using Application.Interfaces;
using System.Threading.Tasks;
using Application.Features.ProductFeatures.Commands;

namespace Application.Features.ProductFeatures.CommandHandlers
{
    public class GetProductByIdQueryHandler : IRequestHandler<GetProductByIdQuery, Product>
    {
        private readonly IApplicationDbContext _context;

        public GetProductByIdQueryHandler(IApplicationDbContext context) =>
            _context = context;

        public async Task<Product> Handle(GetProductByIdQuery query, CancellationToken cancellationToken)
        {
            Product product = null;

            await Task.Run(() =>
            {
                product = _context.Products.Where(a => a.Id == query.Id).FirstOrDefault();
            });

            return product ?? null;
        }
    }
}
