﻿namespace Application.Features.ProductFeatures.CommandHandlers
{
    using Application.Features.ProductFeatures.Commands;
    using Application.Interfaces;
    using Domain.Entities;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;

    public class GetAllProductsQueryHandler : IRequestHandler<GetAllProductsQuery, IEnumerable<Product>>
    {
        private readonly IApplicationDbContext _context;

        public GetAllProductsQueryHandler(IApplicationDbContext context) =>
            _context = context;

        public async Task<IEnumerable<Product>> Handle(GetAllProductsQuery query, CancellationToken cancellationToken)
        {
            var productList = await _context.Products.ToListAsync();

            if (productList == null)
            {
                return null;
            }

            return productList.AsReadOnly();
        }
    }
}
