﻿using MediatR;
using System.Linq;
using System.Threading;
using Application.Interfaces;
using System.Threading.Tasks;
using Application.Features.ProductFeatures.Commands;

namespace Application.Features.ProductFeatures.CommandHandlers
{
    public class UpdateProductCommandHandler : IRequestHandler<UpdateProductCommand, int>
    {
        private readonly IApplicationDbContext _context;

        public UpdateProductCommandHandler(IApplicationDbContext context) =>
            _context = context;

        public async Task<int> Handle(UpdateProductCommand command, CancellationToken cancellationToken)
        {
            var product = _context.Products.Where(a => a.Id == command.Id).FirstOrDefault();

            if (product == null)
            {
                return default;
            }
            else
            {
                product.Barcode = command.Barcode;
                product.Name = command.Name;
                product.Rate = command.Rate;
                product.Description = command.Description;

                await _context.SaveChangesAsync();
                return product.Id;
            }
        }
    }
}
