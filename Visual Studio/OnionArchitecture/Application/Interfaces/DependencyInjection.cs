﻿using MediatR;
using System.Reflection;
using Microsoft.Extensions.DependencyInjection;

namespace Application.Interfaces
{
    public static class DependencyInjection
    {
        public static void AddApplication(this IServiceCollection services) =>
            services.AddMediatR(Assembly.GetExecutingAssembly());
    }
}
