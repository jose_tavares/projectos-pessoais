# Writing Testable Code
Sample code for my course on [Writing Testable Code](https://pluralsight.pxf.io/testable-code) in .NET Framework 4.5.

This sample application is intended to be an educational resource for learning testable coding practices. It incorporates these best practices in ways that are simple and easy to understand.

## Technologies
This demo application uses the following technologies:
- .NET Framework 4.5
- C# .NET 5.0
- NUnit 3.6
- Moq 4.7
- AutoMoq 2.0
- Ninject 3.2 
