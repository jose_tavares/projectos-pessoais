﻿namespace TestableCodeDemos.Module1.Easy
{
    public interface IPrinter
    {
        void WriteLine(string text);
    }
}
