﻿namespace TestableCodeDemos.Module1.Easy
{
    public interface ICalculator
    {
        decimal GetTotal(decimal parts, decimal service, decimal discount);
    }
}