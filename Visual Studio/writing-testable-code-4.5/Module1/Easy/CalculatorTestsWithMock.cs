﻿using Moq;
using NUnit.Framework;

namespace TestableCodeDemos.Module1.Easy
{
    [TestFixture]
    public class CalculatorTestsWithMock
    {        
        private Mock<IPrinter> mockPrinter;
        private Mock<ICalculator> mockCalculator;
        private PrintCalculatorCommand command;

        private const decimal parts = 1.00m;
        private const decimal service = 2.00m;
        private const decimal discount = 0.50m;
        private const decimal expectedResult = 2.50m;

        [SetUp]
        public void SetUp()
        {
            mockPrinter = new Mock<IPrinter>();
            mockCalculator = new Mock<ICalculator>();

            mockCalculator
                .Setup(setup => setup.GetTotal(parts, service, discount))
                .Returns(expectedResult);

            command = new PrintCalculatorCommand(mockPrinter.Object, mockCalculator.Object);
        }

        [Test]
        public void TestExecuteShouldPrintTotalPrice()
        {
            command.Execute(parts, service, discount);

            mockPrinter.Verify(verify => verify.WriteLine("Total Price: $2,50"), Times.Once);
        }

        [Test]
        public void TestExecuteShouldNotPrintTotalPrice()
        {
            command.Execute(parts, service, discount);

            mockPrinter.Verify(verify => verify.WriteLine("Total Price: $2.50"), Times.Never);
        }
    }
}
