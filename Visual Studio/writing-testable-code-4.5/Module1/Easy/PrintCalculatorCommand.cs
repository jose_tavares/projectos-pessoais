﻿namespace TestableCodeDemos.Module1.Easy
{
    public class PrintCalculatorCommand
    {
        private readonly IPrinter printer;
        private readonly ICalculator calculator;

        public PrintCalculatorCommand(
            IPrinter printer,
            ICalculator calculator)
        {
            this.printer = printer;
            this.calculator = calculator;
        }

        public void Execute(decimal parts, decimal service, decimal discount)
        {
            decimal total = calculator.GetTotal(parts, service, discount);

            printer.WriteLine("Total Price: $" + total);
        }
    }
}
