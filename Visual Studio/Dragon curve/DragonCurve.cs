﻿namespace Dragon_curve
{
    using System;
    using System.Drawing;
    using System.Windows.Forms;
    using System.Drawing.Drawing2D;
    using System.Collections.Generic;

    public partial class DragonCurve : Form
    {
        private readonly List<int> turns;
        private readonly double startingAngle, side;

        public DragonCurve()
        {
            InitializeComponent();
        }

        public DragonCurve(int iter)
        {
            Size = new Size(800, 600);
            StartPosition = FormStartPosition.CenterScreen;
            DoubleBuffered = true;
            BackColor = Color.White;

            startingAngle = -iter * (Math.PI / 4);
            side = 400 / Math.Pow(2, iter / 2.0);

            turns = GetSequence(iter);
        }

        private List<int> GetSequence(int iter)
        {
            List<int> turnSequence = new List<int>();

            for (int i = 0; i < iter; i++)
            {
                List<int> copy = new List<int>(turnSequence);
                copy.Reverse();
                turnSequence.Add(1);

                foreach (int turn in copy)
                {
                    turnSequence.Add(-turn);
                }
            }

            return turnSequence;
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;

            double angle = startingAngle;
            int x1 = 230, y1 = 350;
            int x2 = x1 + (int)(Math.Cos(angle) * side);
            int y2 = y1 + (int)(Math.Sin(angle) * side);

            e.Graphics.DrawLine(Pens.Black, x1, y1, x2, y2);
            x1 = x2;
            y1 = y2;

            foreach (int turn in turns)
            {
                angle += turn * (Math.PI / 2);
                x2 = x1 + (int)(Math.Cos(angle) * side);
                y2 = y1 + (int)(Math.Sin(angle) * side);
                e.Graphics.DrawLine(Pens.Black, x1, y1, x2, y2);
                x1 = x2;
                y1 = y2;
            }
        }
    }
}
