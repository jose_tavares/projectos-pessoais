	v4.push_back(-2);
	v4.push_back(7);
	v4.push_back(0);
	v4.push_back(1);

	sort(begin(v4), end(v4));
	//in C++ 20:  sort(v4);

	bool allpositive = std::all_of(begin(v4), end(v4), [](int elem) {return elem >= 0; });

	string s{ "Hello I am a sentence" };
	//find the first a
	auto letter = find(begin(s), end(s), 'a');

	auto caps = std::count_if(begin(s), end(s), [](char c) {return (c != ' ') && (toupper(c) == c); });
