﻿using NHibernate;
using System.Linq;
using System.Collections.Generic;
using CRUD_FluentNHibernate.Models;

namespace CRUD_FluentNHibernate.Repository
{
    public class AlunoRepository : IAlunoRepository
    {
        private readonly ISession _session = NHibernateHelper.OpenSession();

        public bool Add(Aluno aluno)
        {
            if (aluno == null)
            {
                return false;
            }

            var transaction = _session.BeginTransaction();

            _session.Save(aluno);
            transaction.Commit();

            return true;
        }

        public bool Delete(int id)
        {
            var aluno = Get(id);

            if (aluno == null)
            {
                return false;
            }

            var transaction = _session.BeginTransaction();

            _session.Delete(aluno);
            transaction.Commit();

            return true;
        }

        public bool Edit(Aluno aluno)
        {
            if (aluno == null)
            {
                return false;
            }

            var alunoAlterado = Get(aluno.Id);

            if (alunoAlterado == null)
            {
                return false;
            }

            alunoAlterado.Sexo = aluno.Sexo;
            alunoAlterado.Curso = aluno.Curso;
            alunoAlterado.Email = aluno.Email;
            alunoAlterado.Nome = aluno.Nome;

            var transaction = _session.BeginTransaction();

            _session.Save(alunoAlterado);
            transaction.Commit();

            return true;
        }

        public Aluno Get(int id)
        {
            return _session.Get<Aluno>(id);
        }

        public IEnumerable<Aluno> GetAll()
        {
            return _session.Query<Aluno>().ToList();
        }
    }
}