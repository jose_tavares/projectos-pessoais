﻿using System.Collections.Generic;
using CRUD_FluentNHibernate.Models;

namespace CRUD_FluentNHibernate.Repository
{
    public interface IAlunoRepository
    {
        bool Add(Aluno aluno);

        bool Delete(int id);

        bool Edit(Aluno aluno);

        Aluno Get(int id);

        IEnumerable<Aluno> GetAll();
    }
}
