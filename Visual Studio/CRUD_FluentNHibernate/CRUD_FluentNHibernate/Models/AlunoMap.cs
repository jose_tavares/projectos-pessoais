﻿using FluentNHibernate.Mapping;

namespace CRUD_FluentNHibernate.Models
{
    public class AlunoMap : ClassMap<Aluno>
    {
        public AlunoMap()
        {
            Id(id => id.Id);
            Map(map => map.Nome);
            Map(map => map.Email);
            Map(map => map.Curso);
            Map(map => map.Sexo);
            Table("Alunos");
        }
    }
}