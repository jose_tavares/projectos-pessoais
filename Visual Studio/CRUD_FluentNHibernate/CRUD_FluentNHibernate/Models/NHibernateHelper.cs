﻿using NHibernate;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate.Tool.hbm2ddl;

namespace CRUD_FluentNHibernate.Models
{
    public class NHibernateHelper
    {
        public static ISession OpenSession()
        {
            ISessionFactory sessionFactory = Fluently.Configure()
                .Database(
                    MsSqlConfiguration.MsSql2012
                        .ConnectionString
                            (@"Data Source=192.168.10.11\SQLDEV, 1533;Initial Catalog=Cadastro;user id=sa;password=_PWD4sa_;Integrated Security=False;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False")
                        .ShowSql())

                .Mappings(mappings =>
                    mappings.FluentMappings
                        .AddFromAssemblyOf<Aluno>())

                .ExposeConfiguration(config => new SchemaExport(config)
                    .Create(false, false))
                .BuildSessionFactory();

            return sessionFactory.OpenSession();
        }
    }
}