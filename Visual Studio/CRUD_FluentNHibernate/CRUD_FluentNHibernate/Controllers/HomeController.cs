﻿using NHibernate;
using System.Web.Mvc;
using CRUD_FluentNHibernate.Models;
using CRUD_FluentNHibernate.Repository;

namespace CRUD_FluentNHibernate.Controllers
{
    public class HomeController : Controller
    {
        private readonly IAlunoRepository _alunoRepository;

        public HomeController(IAlunoRepository alunoRepository) => _alunoRepository = alunoRepository;

        // GET: Home
        public ActionResult Index()
        {
            var alunos = _alunoRepository.GetAll();

            return View(alunos);
        }

        // GET: Home/Details/5
        public ActionResult Details(int id)
        {
            var aluno = _alunoRepository.Get(id);

            return View(aluno);
        }

        // GET: Home/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Home/Create
        [HttpPost]
        public ActionResult Create(Aluno aluno)
        {
            if (_alunoRepository.Add(aluno))
            {
                return RedirectToAction("Index");
            }

            return View();
        }

        // GET: Home/Edit/5
        public ActionResult Edit(int id)
        {
            var aluno = _alunoRepository.Get(id);

            return View(aluno);
        }

        // POST: Home/Edit/5
        [HttpPost]
        public ActionResult Edit(Aluno aluno)
        {
            if (_alunoRepository.Edit(aluno))
            {
                return RedirectToAction("Index");
            }

            return View();
        }

        // GET: Home/Delete/5
        public ActionResult Delete(int id)
        {
            var aluno = _alunoRepository.Get(id);

            return View(aluno);
        }

        // POST: Home/Delete/5
        [HttpPost]
        public ActionResult Delete(Aluno aluno)
        {
            if (_alunoRepository.Delete(aluno.Id))
            {
                return RedirectToAction("Index");
            }

            return View();
        }
    }
}
