﻿using System.Linq;
using System.Web.Mvc;
using Mvc_ActionLink.Models;
using System.Collections.Generic;

namespace Mvc_ActionLink.Controllers
{
    public class HomeController : Controller
    {
        private readonly NORTHWNDEntities ctx = new NORTHWNDEntities();

        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Index2()
        {
            return View();
        }

        public PartialViewResult Brazil()
        {
            var resultado = ctx.Customers.Where(where => where.Country == "Brazil").ToList();

            return PartialView("Pais", resultado);
        }

        public PartialViewResult Argentina()
        {
            var resultado = ctx.Customers.Where(where => where.Country == "Argentina").ToList();

            return PartialView("Pais", resultado);
        }

        public ActionResult Localizar(string criterio)
        {
            var produtos = new List<Product>();

            if (!string.IsNullOrWhiteSpace(criterio))
            {
                produtos = ctx.Products.Where(where => where.ProductName.Contains(criterio)).ToList();

                if (ctx != null)
                {
                    ctx.Dispose();
                }

                if (produtos == null)
                {
                    return HttpNotFound();
                }
            }

            return PartialView("Produtos", produtos);
        }
    }
}