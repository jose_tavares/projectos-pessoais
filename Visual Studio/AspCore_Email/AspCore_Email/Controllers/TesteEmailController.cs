﻿using AspCore_Email.Models;
using AspCore_Email.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Threading.Tasks;

namespace AspCore_Email.Controllers
{
    public class TesteEmailController : Controller
    {
        private readonly IEmailSender _emailSender;

        public TesteEmailController
            (IHostingEnvironment env,
            IEmailSender emailSender) =>
                _emailSender = emailSender;

        public IActionResult EnviaEmail() => View();

        [HttpPost]
        public IActionResult EnviaEmail(EmailModel email)
        {
            if (!ModelState.IsValid)
            {
                return View(email);
            }

            try
            {
                TesteEnvioEmail(email.Destino, email.Assunto, email.Mensagem).GetAwaiter();

                return RedirectToAction("EmailEnviado");
            }

            catch { return RedirectToAction("EmailFalhou"); }
        }

        public IActionResult EmailEnviado() => View();

        public IActionResult EmailFalhou() => View();

        private async Task TesteEnvioEmail(string email, string assunto, string mensagem)
        {
            try { await _emailSender.SendEmailAsync(assunto, mensagem, email); }

            catch (Exception ex) { throw ex; }
        }
    }
}