﻿using System.Threading.Tasks;

namespace AspCore_Email.Services
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string subject, string message, string email = null, string arquivo = null);
    }
}