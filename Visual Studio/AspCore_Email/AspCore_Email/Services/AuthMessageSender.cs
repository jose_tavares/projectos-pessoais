﻿using System;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;

namespace AspCore_Email.Services
{
    public class AuthMessageSender : IEmailSender
    {
        private readonly EmailSettings _emailSettings;

        public AuthMessageSender(IOptions<EmailSettings> emailSettings) => _emailSettings = emailSettings.Value;

        public Task SendEmailAsync(string subject, string message, string email = null, string arquivo = null)
        {
            try
            {
                Execute(subject, message, email, arquivo).Wait();
                return Task.FromResult(0);
            }

            catch (Exception ex) { throw ex; }
        }

        private async Task Execute(string subject, string message, string email = null, string arquivo = null)
        {
            try
            {
                var toEmail = string.IsNullOrEmpty(email) ? _emailSettings.ToEmail : email;

                var mail = new MailMessage
                {
                    From = new MailAddress(_emailSettings.UsernameEmail, _emailSettings.Username)
                };

                mail.To.Add(new MailAddress(toEmail));
                mail.CC.Add(new MailAddress(_emailSettings.CcEmail));

                mail.Subject = subject;
                mail.Body = message;
                mail.IsBodyHtml = true;
                mail.Priority = MailPriority.High;

                if (!string.IsNullOrEmpty(arquivo))
                {
                    mail.Attachments.Add(new Attachment(arquivo));
                }

                using (var smtp = new SmtpClient(_emailSettings.PrimaryDomain, _emailSettings.PrimaryPort))
                {
                    smtp.Credentials = new NetworkCredential(_emailSettings.UsernameEmail, _emailSettings.UsernamePassword);
                    smtp.EnableSsl = true;
                    await smtp.SendMailAsync(mail);
                }
            }

            catch (Exception ex) { throw ex; }
        }
    }
}
