﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.Text;
using System.Text;

[Generator]
public sealed class HelloWorldGenerator : ISourceGenerator
{
    public void Initialize(GeneratorInitializationContext context)
    {
        // Initialization logic, if needed
    }

    public void Execute(GeneratorExecutionContext context)
    {
        string source = @"using System;
        namespace HelloWorldGenerated
        {
            public static class HelloWorld
            {
                public static void SayHello() => Console.WriteLine(""Hello, World!"");
            }
        }";
        context.AddSource("helloWorldGenerator", SourceText.From(source, Encoding.UTF8));
    }
}