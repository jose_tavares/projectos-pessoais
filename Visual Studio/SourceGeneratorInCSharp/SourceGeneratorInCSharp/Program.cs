﻿namespace SourceGeneratorInCSharp
{
    using HelloWorldGenerated;

    internal class Program
    {
        static void Main(string[] args)
        {
            HelloWorld.SayHello();
        }
    }
}
