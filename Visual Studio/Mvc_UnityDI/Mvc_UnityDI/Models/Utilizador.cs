﻿namespace Mvc_UnityDI.Models
{
    public class Utilizador
    {
        public int Id { get; set; }

        public string Nome { get; set; }

        public string Senha { get; set; }

        public string Email { get; set; }
    }
}