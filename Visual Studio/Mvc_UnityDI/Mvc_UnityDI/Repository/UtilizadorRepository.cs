﻿using System;
using System.Linq;
using Mvc_UnityDI.Models;
using System.Collections.Generic;

namespace Mvc_UnityDI.Repository
{
    public class UtilizadorRepository : IUtilizadorRepository
    {
        private int _id = 1;
        private readonly List<Utilizador> _utilizadores = new List<Utilizador>();

        public UtilizadorRepository()
        {
            Add(new Utilizador { Id = 1, Nome = "Macoratti", Email = "macoratti@teste.com", Senha = "numsey@13" });
            Add(new Utilizador { Id = 2, Nome = "Jefferson", Email = "jeff@teste.com", Senha = "ytedg6543" });
            Add(new Utilizador { Id = 3, Nome = "Miriam", Email = "miriam3@teste.com", Senha = "#5496dskj" });
        }

        public Utilizador Add(Utilizador item)
        {
            if (item == null)
            {
                throw new ArgumentNullException(nameof(item));
            }

            item.Id = _id++;
            _utilizadores.Add(item);

            return item;
        }

        public bool Delete(Utilizador item)
        {
            _utilizadores.RemoveAll(removeAll => removeAll.Id == item.Id);

            return true;
        }

        public Utilizador Get(int id)
        {
            return _utilizadores.FirstOrDefault(first => first.Id == id);
        }

        public IEnumerable<Utilizador> GetAll()
        {
            return _utilizadores;
        }

        public bool Update(Utilizador item)
        {
            if (item == null)
            {
                throw new ArgumentNullException(nameof(item));
            }

            int index = _utilizadores.FindIndex(findIndex => findIndex.Id == item.Id);

            if (index == -1)
            {
                return false;
            }

            _utilizadores.RemoveAt(index);
            _utilizadores.Add(item);

            return true;
        }
    }
}