﻿using Mvc_UnityDI.Models;
using System.Collections.Generic;

namespace Mvc_UnityDI.Repository
{
    public interface IUtilizadorRepository
    {
        IEnumerable<Utilizador> GetAll();

        Utilizador Get(int id);

        Utilizador Add(Utilizador item);

        bool Update(Utilizador item);

        bool Delete(Utilizador item);
    }
}
