﻿using System.Web.Mvc;
using Mvc_UnityDI.Repository;

namespace Mvc_UnityDI.Controllers
{
    public class UtilizadorController : Controller
    {
        private readonly IUtilizadorRepository _utilizadorRepository;

        public UtilizadorController(IUtilizadorRepository utilizadorRepository)
        {
            _utilizadorRepository = utilizadorRepository;
        }

        // GET: Utilizador
        public ActionResult Index()
        {
            var data = _utilizadorRepository.GetAll();

            return View(data);
        }
    }
}