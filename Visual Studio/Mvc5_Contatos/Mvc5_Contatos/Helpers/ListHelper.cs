﻿using System.Linq;
using System.Collections.Generic;

namespace Mvc5_Contatos.Helpers
{
    public static class ListHelper
    {
        public static bool IsValid<T>(this IEnumerable<T> list) => (list != null) && list.Any();
    }
}