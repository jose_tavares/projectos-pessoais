﻿$(function () {
    $(".create").click(function () {
        $("#modal").load("/Contatos/Create", function () {
            $("#modal").modal();
        });
    });

    $(".delete").click(function () {
        var id = $(this).attr("data-id");

        $("#modal").load("/Contatos/DeleteGet?id=" + id, function () {
            $("#modal").modal();
        })
    });

    $(".edit").click(function () {
        var id = $(this).attr("data-id");

        $("#modal").load("/Contatos/Edit?id=" + id, function () {
            $("#modal").modal();
        })
    });

    $(".details").click(function () {
        var id = $(this).attr("data-id");

        $("#modal").load("/Contatos/Details?id=" + id, function () {
            $("#modal").modal();
        })
    });
});