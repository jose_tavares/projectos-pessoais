﻿using System;
using Rotativa;
using System.Net;
using System.Web.Mvc;
using System.Web.Helpers;
using Mvc5_Contatos.Models;
using Mvc5_Contatos.Helpers;
using Mvc5_Contatos.Services;

namespace Mvc5_Contatos.Controllers
{
    public class ContatosController : Controller
    {
        private readonly IContatoService _contatoRepository;

        public ContatosController(IContatoService contatoRepository) => _contatoRepository = contatoRepository;

        [HttpGet]
        // GET: Contatos
        public ActionResult Index(string nome, string submit, bool error = true, bool pdf = false)
        {
            var requestUri = "contatos" +
                (submit == "Procurar" ?
                    "?nome=" + nome :
                    string.Empty);

            var contatos = _contatoRepository.GetAll(requestUri);

            if (!contatos.IsValid() || !error)
            {
                ModelState.AddModelError(string.Empty, "Erro no servidor. Contate o Administrador.");
            }

            var grid = new WebGrid(contatos, rowsPerPage: 2);

            if (pdf)
            {
                return new ViewAsPdf
                {
                    ViewName = "Index",
                    IsGrayScale = true,
                    FileName = "RelatorioContatos.pdf",
                    Model = Tuple.Create(grid, nome, submit)
                };
            }

            return View(Tuple.Create(grid, nome, submit));
        }

        #region Create
        [HttpGet]
        public ActionResult Create()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult Create(ContatoEnderecoViewModel contato)
        {
            if (contato == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            return RedirectToAction("Index", new { error = _contatoRepository.Add(contato) });
        }
        #endregion

        #region Edit
        [HttpGet]
        public ActionResult Edit(int? id)
        {
            if (!id.HasValue)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            return PartialView(_contatoRepository.Get(id.Value));
        }

        [HttpPost]
        public ActionResult Edit(ContatoViewModel contato)
        {
            if (contato == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            return RedirectToAction("Index", new { error = _contatoRepository.Edit(contato) });
        }
        #endregion

        [HttpGet]
        public ActionResult DeleteGet(int? id)
        {
            if (!id.HasValue)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            return PartialView(_contatoRepository.Get(id.Value));
        }

        [HttpPost]
        public ActionResult DeletePost(int? id)
        {
            if (!id.HasValue)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            return RedirectToAction("Index", new { error = _contatoRepository.Delete(id.Value) });
        }

        [HttpGet]
        public ActionResult Details(int? id)
        {
            if (!id.HasValue)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            return PartialView(_contatoRepository.Get(id.Value));
        }
    }
}