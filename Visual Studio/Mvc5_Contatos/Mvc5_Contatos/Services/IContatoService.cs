﻿using Mvc5_Contatos.Models;
using System.Collections.Generic;

namespace Mvc5_Contatos.Services
{
    public interface IContatoService
    {
        bool Add(ContatoEnderecoViewModel contato);

        bool Delete(int id);

        bool Edit(ContatoViewModel contato);

        ContatoViewModel Get(int id);

        IEnumerable<ContatoViewModel> GetAll(string requestUri);
    }
}
