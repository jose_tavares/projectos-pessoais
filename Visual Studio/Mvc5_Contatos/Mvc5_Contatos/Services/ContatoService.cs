﻿using System;
using System.Linq;
using System.Net.Http;
using Mvc5_Contatos.Models;
using System.Collections.Generic;

namespace Mvc5_Contatos.Services
{
    public class ContatoService : IContatoService
    {
        private readonly HttpClient _client = new HttpClient();
        private readonly string _uriString = "https://localhost:44311/api/";

        public bool Add(ContatoEnderecoViewModel contato)
        {
            using (_client)
            {
                _client.BaseAddress = new Uri(_uriString);

                //HTTP POST
                var postTask = _client.PostAsJsonAsync("contatos", contato);

                postTask.Wait();

                var result = postTask.Result;

                if (result.IsSuccessStatusCode)
                {
                    return true;
                }
            }

            return false;
        }

        public bool Delete(int id)
        {
            using (_client)
            {
                _client.BaseAddress = new Uri(_uriString);

                //HTTP DELETE
                var deleteTask = _client.DeleteAsync("contatos/" + id.ToString());
                deleteTask.Wait();

                var result = deleteTask.Result;

                if (result.IsSuccessStatusCode)
                {
                    return true;
                }
            }

            return false;
        }

        public bool Edit(ContatoViewModel contato)
        {
            using (_client)
            {
                _client.BaseAddress = new Uri(_uriString);

                //HTTP PUT
                var putTask = _client.PutAsJsonAsync("contatos", contato);

                putTask.Wait();

                var result = putTask.Result;

                if (result.IsSuccessStatusCode)
                {
                    return true;
                }
            }

            return false;
        }

        public ContatoViewModel Get(int id)
        {
            using (_client)
            {
                _client.BaseAddress = new Uri(_uriString);

                //HTTP GET
                var responseTask = _client.GetAsync("contatos?id=" + id.ToString());
                responseTask.Wait();

                var result = responseTask.Result;

                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<ContatoViewModel>();
                    readTask.Wait();

                    return readTask.Result;
                }
            }

            return null;
        }

        public IEnumerable<ContatoViewModel> GetAll(string requestUri)
        {
            IEnumerable<ContatoViewModel> contatos = null;

            using (_client)
            {
                _client.BaseAddress = new Uri(_uriString);

                //HTTP GET
                var responseTask = _client.GetAsync(requestUri);
                responseTask.Wait();

                var result = responseTask.Result;

                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<IList<ContatoViewModel>>();
                    readTask.Wait();

                    contatos = readTask.Result;
                }
                else
                {
                    contatos = Enumerable.Empty<ContatoViewModel>();
                }
            }

            return contatos;
        }
    }
}