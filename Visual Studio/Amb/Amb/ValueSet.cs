﻿using System.Collections.Generic;

namespace Amb
{
    public class ValueSet<T> : IValueSet, IAmbValue<T>, IValueSetIterator
    {
        private IEnumerator<T> enumerator;
        
        public IEnumerable<T> Enumerable;

        public T Value => enumerator.Current;

        public IValueSetIterator CreateIterator()
        {
            enumerator = Enumerable.GetEnumerator();
            return this;
        }

        public void Dispose() => enumerator.Dispose();

        public bool MoveNext => enumerator.MoveNext();
    }
}