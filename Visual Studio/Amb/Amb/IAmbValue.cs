﻿namespace Amb
{
    public interface IAmbValue<T>
    {
        T Value { get; }
    }
}