﻿using System;

namespace Amb
{
    [Serializable]
    public class AmbException : Exception
    {
        public AmbException() : base("AMB is angry") { }
    }
}