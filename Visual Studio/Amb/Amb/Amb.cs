﻿using System;
using System.Collections.Generic;

namespace Amb
{
    public class Amb : IDisposable
    {
        private volatile bool stopped = false;
        private readonly List<IValueSet> streams = new List<IValueSet>();
        private readonly List<IAssertOrAction> assertsOrActions = new List<IAssertOrAction>();

        public IAmbValue<T> DefineValues<T>(params T[] values)
        {
            return DefineValueSet(values);
        }

        public IAmbValue<T> DefineValueSet<T>(T[] values)
        {
            ValueSet<T> stream = new ValueSet<T>
            {
                Enumerable = values
            };

            streams.Add(stream);
            return stream;
        }

        public Amb Assert(Func<bool> function)
        {
            assertsOrActions.Add(new AmbAssert()
            {
                Level = streams.Count,
                IsValidFunction = function
            });

            return this;
        }

        public Amb Perform(Action action)
        {
            assertsOrActions.Add(new AmbAction()
            {
                Level = streams.Count,
                Action = action
            });

            return this;
        }

        public void Stop()
        {
            stopped = true;
        }

        public void Dispose()
        {
            RunLevel(0, 0);

            if (!stopped)
            {
                throw new AmbException();
            }
        }

        private void RunLevel(int level, int actionIndex)
        {
            while (actionIndex < assertsOrActions.Count && assertsOrActions[actionIndex].Level <= level)
            {
                if (!assertsOrActions[actionIndex].Invoke() || stopped) return;
                actionIndex++;
            }

            if (level < streams.Count)
            {
                using (IValueSetIterator iterator = streams[level].CreateIterator())
                {
                    while (iterator.MoveNext)
                    {
                        RunLevel(level + 1, actionIndex);
                    }
                }
            }
        }
    }
}
