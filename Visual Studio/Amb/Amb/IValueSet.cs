﻿namespace Amb
{
    public interface IValueSet
    {
        IValueSetIterator CreateIterator();
    }
}