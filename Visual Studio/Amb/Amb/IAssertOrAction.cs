﻿namespace Amb
{
    public interface IAssertOrAction
    {
        int Level { get; }

        bool Invoke();
    }
}