﻿using System;

namespace Amb
{
    public interface IValueSetIterator : IDisposable
    {
        bool MoveNext { get; }
    }
}