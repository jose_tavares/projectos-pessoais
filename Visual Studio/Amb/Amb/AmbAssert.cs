﻿using System;

namespace Amb
{
    public class AmbAssert : IAssertOrAction
    {
        int IAssertOrAction.Level => Level;

        public int Level { get; set; }

        public Func<bool> IsValidFunction { get; set; }

        public bool Invoke()
        {
            return IsValidFunction();
        }
    }
}