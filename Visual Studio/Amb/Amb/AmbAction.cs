﻿using System;

namespace Amb
{
    public class AmbAction : IAssertOrAction
    {
        int IAssertOrAction.Level => Level;
        
        public int Level { get; set; }

        public Action Action { get; set; }

        public bool Invoke()
        {
            Action();
            return true;
        }
    }
}