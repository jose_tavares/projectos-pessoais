﻿using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using AspNet_MediatR1.Domain.Entity;

namespace AspNet_MediatR1.Repository
{
    public class ProdutoRepository : IRepository<Produto>
    {
        private static readonly Dictionary<int, Produto> _produtos = new Dictionary<int, Produto>();

        public ProdutoRepository()
        {
            _produtos.Add(1, new Produto { Id = 1, Nome = "Caneta", Preco = 3.45m });
            _produtos.Add(2, new Produto { Id = 2, Nome = "Caderno", Preco = 7.65m });
            _produtos.Add(3, new Produto { Id = 3, Nome = "Borracha", Preco = 1.20m });
        }

        public async Task Add(Produto item) =>
            await Task.Run(() => _produtos.Add(item.Id, item));

        public async Task Delete(int id) =>
            await Task.Run(() => _produtos.Remove(id));

        public async Task Edit(Produto item) =>
            await Task.Run(() =>
                {
                    _produtos.Remove(item.Id);
                    _produtos.Add(item.Id, item);
                });

        public async Task<Produto> Get(int id) =>
            await Task.Run(() => _produtos.GetValueOrDefault(id));

        public async Task<IEnumerable<Produto>> GetAll() =>
            await Task.Run(() => _produtos.Values.ToList());
    }
}
