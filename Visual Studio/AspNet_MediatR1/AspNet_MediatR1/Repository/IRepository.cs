﻿using System.Threading.Tasks;
using System.Collections.Generic;

namespace AspNet_MediatR1.Repository
{
    public interface IRepository<T> where T : class
    {
        Task Add(T item);

        Task Delete(int id);

        Task Edit(T item);

        Task<T> Get(int id);

        Task<IEnumerable<T>> GetAll();
    }
}
