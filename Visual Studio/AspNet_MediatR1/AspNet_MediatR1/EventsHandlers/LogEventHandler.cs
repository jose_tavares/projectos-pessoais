﻿using System;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using AspNet_MediatR1.Notifications;

namespace AspNet_MediatR1.EventsHandlers
{
    public class LogEventHandler :
                            INotificationHandler<ErroNotification>,
                            INotificationHandler<ProdutoCreateNotification>,
                            INotificationHandler<ProdutoDeleteNotification>,
                            INotificationHandler<ProdutoUpdateNotification>
    {
        public Task Handle(ErroNotification notification, CancellationToken cancellationToken) =>
            Task.Run(() =>
                Console.WriteLine($"ERRO: '{notification.Erro} \n {notification.PilhaErro}'")
            );

        public Task Handle(ProdutoCreateNotification notification, CancellationToken cancellationToken) =>
            Task.Run(() =>
                Console.WriteLine($"CRIACAO: '{notification.Id} " + $"- {notification.Nome} - {notification.Preco}'")
            );

        public Task Handle(ProdutoDeleteNotification notification, CancellationToken cancellationToken) =>
            Task.Run(() =>
                Console.WriteLine($"EXCLUSAO: '{notification.Id} " + $"- {notification.IsConcluido}'")
            );

        public Task Handle(ProdutoUpdateNotification notification, CancellationToken cancellationToken) =>
            Task.Run(() =>
                Console.WriteLine($"ALTERACAO: '{notification.Id} - {notification.Nome} " + $"- {notification.Preco} - {notification.IsConcluido}'")
            );
    }
}
