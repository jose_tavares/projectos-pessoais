﻿namespace DATA.Helpers
{
    using System;
    using DATA.Classes;
    using System.Net.Http;
    using DATA.Properties;
    using System.Diagnostics;
    using System.Net.Http.Headers;

    public static class ApiHelper
    {
        public static HttpClient HttpClient { get; set; }

        public static void InitializeClient()
        {
            string urlString = Debugger.IsAttached
                ? Settings.Default.ProdBaseURI
                : Settings.Default.ProdBaseURI;

            HttpClientHandler handler = new HttpClientHandler
            {
                ClientCertificateOptions = ClientCertificateOption.Manual,
                ServerCertificateCustomValidationCallback =
                (httpRequestMessage, cert, cetChain, policyErrors) =>
                {
                    return true;
                }
            };

            HttpClient = new HttpClient(handler)
            {
                BaseAddress = new Uri(urlString),
                Timeout = Settings.Default.APITimeout
            };

            HttpClient.DefaultRequestHeaders.Accept.Clear();
            HttpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpClient.DefaultRequestHeaders.Add("AppName", "ERTSERVICE");
        }

        public static void AddToken(Token Token)
        {
            HttpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Token.TokenType, Token.AccessToken);
        }
    }
}
