﻿using System;
using Newtonsoft.Json;

namespace DATA.Classes
{
    public class Token
    {
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }

        [JsonProperty("token_type")]
        public string TokenType { get; set; }

        [JsonProperty("expires_in")]
        public int ExpiresIn { get; set; }

        [JsonProperty(".issued")]
        public DateTime RequestedDate { get; set; }

        [JsonProperty(".expires")]
        public DateTime ExpireDate { get; set; }

        public decimal ROWID { get; set; }
    }
}
