﻿using System;
using Newtonsoft.Json;
using System.Runtime.Serialization;

namespace DATA.Classes
{
    [Serializable]
    public class TokenException : Exception
    {
        [JsonProperty("error")]
        public string ErrorType { get; set; }

        [JsonProperty("error_description")]
        public string ErrorDescription { get; set; }

        public TokenException(SerializationInfo info, StreamingContext context)
        {
            ErrorType = info.GetString("error");
            ErrorDescription = info.GetString("error_description");
        }

        public TokenException(string errorType, string errorDescription)
        {
            ErrorType = errorType;
            ErrorDescription = errorDescription;
        }

        public TokenException(TokenException ex)
        {
            if (ex != null)
            {
                ErrorType = ex.ErrorType;
                ErrorDescription = ex.ErrorDescription;
            }
        }
    }
}
