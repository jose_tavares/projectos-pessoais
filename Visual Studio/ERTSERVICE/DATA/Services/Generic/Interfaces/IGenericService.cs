﻿using MODELS.Helpers;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace DATA.Services.Generic.Interfaces
{
    public interface IGenericService<T>
    {
        Task<IEnumerable<T>> GetAll();

        Task<T> GetByID(decimal id);

        Task<DataResult> Insert(T model);

        Task<DataResult> Update(T model);

        DataResult IsValidInsert(T model);

        DataResult IsValidUpdate(T model);

        Task<DataResult> Delete(decimal id);
    }
}
