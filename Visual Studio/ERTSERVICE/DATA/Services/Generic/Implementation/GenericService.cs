﻿namespace DATA.Services.Generic.Implementation
{
    using System;
    using DATA.Helpers;
    using MODELS.Helpers;
    using System.Net.Http;
    using System.Threading.Tasks;
    using System.Collections.Generic;
    using DATA.Services.Generic.Interfaces;

    public class GenericService<T> : IGenericService<T> where T : class
    {
        public async Task<IEnumerable<T>> GetAll()
        {
            using (HttpResponseMessage response = await ApiHelper.HttpClient.GetAsync(typeof(T).Name))
            {
                return response.IsSuccessStatusCode
                    ? await response.Content.ReadAsAsync<IEnumerable<T>>()
                    : throw new Exception(await response.Content.ReadAsStringAsync());
            }
        }

        public async Task<T> GetByID(decimal id)
        {
            using (HttpResponseMessage response = await ApiHelper.HttpClient.GetAsync(typeof(T).Name + "/GetById?id=" + id))
            {
                return response.IsSuccessStatusCode
                    ? await response.Content.ReadAsAsync<T>()
                    : throw new Exception(await response.Content.ReadAsStringAsync());
            }
        }

        public async Task<DataResult> Insert(T model)
        {
            DataResult result = IsValidInsert(model);

            if (result.Success)
            {
                using (HttpResponseMessage response = await ApiHelper.HttpClient.PostAsJsonAsync(typeof(T).Name, model))
                {
                    return response.IsSuccessStatusCode
                        ? await response.Content.ReadAsAsync<DataResult>()
                        : throw new Exception(await response.Content.ReadAsStringAsync());
                }
            }

            return result;
        }

        public async Task<DataResult> Update(T model)
        {
            DataResult result = IsValidUpdate(model);

            if (result.Success)
            {
                using (HttpResponseMessage response = await ApiHelper.HttpClient.PutAsJsonAsync(typeof(T).Name, model))
                {
                    return response.IsSuccessStatusCode
                        ? await response.Content.ReadAsAsync<DataResult>()
                        : throw new Exception(await response.Content.ReadAsStringAsync());
                }
            }

            return result;
        }

        public DataResult IsValidInsert(T model)
        {
            DataResult result = model.IsClassValid();

            return result;
        }

        public DataResult IsValidUpdate(T model)
        {
            DataResult result = model.IsClassValid();

            return result;
        }

        public async Task<DataResult> Delete(decimal id)
        {
            using (HttpResponseMessage response = await ApiHelper.HttpClient.DeleteAsync(typeof(T).Name + "/" + id))
            {
                return response.IsSuccessStatusCode
                    ? await response.Content.ReadAsAsync<DataResult>()
                    : throw new Exception(await response.Content.ReadAsStringAsync());
            }
        }
    }
}
