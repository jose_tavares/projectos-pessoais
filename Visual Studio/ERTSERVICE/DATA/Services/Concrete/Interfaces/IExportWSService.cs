﻿using System.Threading.Tasks;
using MODELS.BusinessObject.ERTSOFT;
using DATA.Services.Generic.Interfaces;

namespace DATA.Services.Concrete.Interfaces
{
    public interface IExportWSService : IGenericService<EXPORTWS>
    {
        Task ExportPending();
    }
}
