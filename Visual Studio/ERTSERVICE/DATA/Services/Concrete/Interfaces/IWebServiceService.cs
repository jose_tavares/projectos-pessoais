﻿using System.Threading.Tasks;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT;
using DATA.Services.Generic.Interfaces;

namespace DATA.Services.Concrete.Interfaces
{
    public interface IWebServiceService : IGenericService<WEBSERVICE>
    {
        Task<IEnumerable<WEBSERVICE>> GetByImportFlg(int IMPORTFLG_0);
    }
}
