﻿namespace DATA.Services.Concrete.Interfaces
{
    using DATA.Classes;
    using MODELS.Helpers;
    using System.Threading.Tasks;
    using MODELS.BusinessObject.ERTSOFT;
    using DATA.Services.Generic.Interfaces;

    public interface IUserService : IGenericService<USER>
    {
        Task<USER> GetByUser(string user);

        Task<USER> GetByUsername(string username);

        Task<Token> Login(string username, string password);

        Task<DataResult> Logout();
    }
}
