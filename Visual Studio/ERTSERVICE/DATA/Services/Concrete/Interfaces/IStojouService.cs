﻿namespace DATA.Services.Concrete.Interfaces
{
    using MODELS.Helpers;
    using System.Threading.Tasks;
    using System.Collections.Generic;
    using MODELS.BusinessObject.ERTSOFT;
    using DATA.Services.Generic.Interfaces;

    public interface IStojouService : IGenericService<STOJOU>
    {
        Task<IEnumerable<STOJOU>> GetByWebService(string webService);

        Task<IEnumerable<STOJOU>> GetPending();

        Task<DataResult> ImportFirstPendingByWebService(string WEBSERVICE_0);
    }
}
