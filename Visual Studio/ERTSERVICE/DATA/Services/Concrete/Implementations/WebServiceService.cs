﻿using System;
using DATA.Helpers;
using System.Net.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT;
using DATA.Services.Concrete.Interfaces;
using DATA.Services.Generic.Implementation;

namespace DATA.Services.Concrete.Implementations
{
    public class WebServiceService : GenericService<WEBSERVICE>, IWebServiceService
    {
        public async Task<IEnumerable<WEBSERVICE>> GetByImportFlg(int IMPORTFLG_0)
        {
            using (HttpResponseMessage response = await ApiHelper.HttpClient.GetAsync(nameof(WEBSERVICE) + $"/GetByImportFlg?IMPORTFLG_0={IMPORTFLG_0}"))
            {
                if (response.IsSuccessStatusCode)
                    return await response.Content.ReadAsAsync<IEnumerable<WEBSERVICE>>();
                else
                    throw new Exception(await response.Content.ReadAsStringAsync());
            }
        }
    }
}
