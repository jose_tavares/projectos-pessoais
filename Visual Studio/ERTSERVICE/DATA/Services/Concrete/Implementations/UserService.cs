﻿namespace DATA.Services.Concrete.Implementations
{
    using System;
    using System.Net;
    using DATA.Classes;
    using DATA.Helpers;
    using MODELS.Helpers;
    using Newtonsoft.Json;
    using System.Net.Http;
    using System.Threading.Tasks;
    using System.Collections.Generic;
    using MODELS.BusinessObject.ERTSOFT;
    using DATA.Services.Concrete.Interfaces;
    using DATA.Services.Generic.Implementation;

    public class UserService : GenericService<USER>, IUserService
    {
        public async Task<USER> GetByUser(string user)
        {
            using (HttpResponseMessage response = await ApiHelper.HttpClient.GetAsync(nameof(USER) + "/GetByUser/" + user + "/"))
            {
                return response.IsSuccessStatusCode
                    ? await response.Content.ReadAsAsync<USER>()
                    : throw new Exception(await response.Content.ReadAsStringAsync());
            }
        }

        public async Task<USER> GetByUsername(string username)
        {
            using (HttpResponseMessage response = await ApiHelper.HttpClient.GetAsync(nameof(USER) + "/GetByUsername/" + username + "/"))
            {
                return response.IsSuccessStatusCode
                    ? await response.Content.ReadAsAsync<USER>()
                    : throw new Exception(await response.Content.ReadAsStringAsync());
            }
        }

        public async Task<Token> Login(string username, string password)
        {
            Dictionary<string, string> data = new Dictionary<string, string>
            {
                {"grant_type", "password"},
                {"username", username},
                {"password", password},
            };

            using (HttpResponseMessage response = await ApiHelper.HttpClient.PostAsync(nameof(USER) + "/Login", new FormUrlEncodedContent(data)))
            {
                if (response.IsSuccessStatusCode)
                {
                    return await response.Content.ReadAsAsync<Token>();
                }
                else
                {
                    string error = await response.Content.ReadAsStringAsync();

                    if (response.StatusCode == HttpStatusCode.BadRequest)
                    {
                        throw new TokenException(JsonConvert.DeserializeObject<TokenException>(error));
                    }

                    throw new Exception(error);
                }
            }
        }

        public async Task<DataResult> Logout()
        {
            using (HttpResponseMessage response = await ApiHelper.HttpClient.PutAsync(nameof(USER) + "/Logout/", null))
            {
                return response.IsSuccessStatusCode
                    ? await response.Content.ReadAsAsync<DataResult>()
                    : throw new Exception(await response.Content.ReadAsStringAsync());
            }
        }
    }
}
