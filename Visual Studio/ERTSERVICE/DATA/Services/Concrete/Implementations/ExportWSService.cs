﻿using System;
using DATA.Helpers;
using System.Net.Http;
using System.Threading.Tasks;
using MODELS.BusinessObject.ERTSOFT;
using DATA.Services.Concrete.Interfaces;
using DATA.Services.Generic.Implementation;

namespace DATA.Services.Concrete.Implementations
{
    public class ExportWSService : GenericService<EXPORTWS>, IExportWSService
    {
        public async Task ExportPending()
        {
            using (HttpResponseMessage response = await ApiHelper.HttpClient.GetAsync(nameof(EXPORTWS) + "/ExportPending"))
            {
                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception(await response.Content.ReadAsStringAsync());
                }
            }
        }
    }
}
