﻿namespace DATA.Services.Concrete.Implementations
{
    using System;
    using DATA.Helpers;
    using MODELS.Helpers;
    using System.Net.Http;
    using System.Threading.Tasks;
    using System.Collections.Generic;
    using MODELS.BusinessObject.ERTSOFT;
    using DATA.Services.Concrete.Interfaces;
    using DATA.Services.Generic.Implementation;

    public class StojouService : GenericService<STOJOU>, IStojouService
    {
        public async Task<IEnumerable<STOJOU>> GetByWebService(string webService)
        {
            using (HttpResponseMessage response = await ApiHelper.HttpClient.GetAsync(nameof(STOJOU) + "/GetByWebService/" + webService + "/"))
            {
                if (response.IsSuccessStatusCode)
                    return await response.Content.ReadAsAsync<IEnumerable<STOJOU>>();
                else
                    throw new Exception(await response.Content.ReadAsStringAsync());
            }
        }

        public async Task<IEnumerable<STOJOU>> GetPending()
        {
            using (HttpResponseMessage response = await ApiHelper.HttpClient.GetAsync(nameof(STOJOU) + "/GetPending/"))
            {
                if (response.IsSuccessStatusCode)
                    return await response.Content.ReadAsAsync<IEnumerable<STOJOU>>();
                else
                    throw new Exception(await response.Content.ReadAsStringAsync());
            }
        }

        public async Task<DataResult> ImportFirstPendingByWebService(string WEBSERVICE_0)
        {
            using (HttpResponseMessage response = await ApiHelper.HttpClient.GetAsync(nameof(STOJOU) + $"/ImportFirstPendingByWebService?WEBSERVICE_0={WEBSERVICE_0}"))
            {
                if (response.IsSuccessStatusCode)
                    return await response.Content.ReadAsAsync<DataResult>();
                else
                    throw new Exception(await response.Content.ReadAsStringAsync());
            }
        }
    }
}
