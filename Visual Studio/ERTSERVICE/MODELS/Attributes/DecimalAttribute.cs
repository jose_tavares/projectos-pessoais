﻿using System.ComponentModel.DataAnnotations;
using System.Data.SqlTypes;
using System.Runtime.CompilerServices;

namespace MODELS.Attributes
{
    public class DecimalAttribute : ValidationAttribute
    {
        public int Precision { get; set; }
        public int Scale { get; set; }

        public DecimalAttribute(int precision, int scale, [CallerMemberName] string propertyName = null) : base ($"{propertyName} deverá conter no máximo {precision} dígitos e {scale} casas decimais.")
        {
            Precision = precision;
            Scale = scale;
        }

        public override bool IsValid(object value)
        {
            if (value != null && decimal.TryParse(value.ToString(), out decimal decimalValue))
            {
                SqlDecimal sqlDecimal = new SqlDecimal(decimalValue);
                
                if (Precision >= sqlDecimal.Precision && Scale >= sqlDecimal.Scale)
                {
                    return true;
                }
            }
 
            return false;
        }
    }
}
