﻿using System;
using MODELS.Helpers;
using MODELS.Attributes;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MODELS.BusinessObject.ERTSOFT
{
    // EXPORTWS
    [Table("ERT.[EXPORTWS]")]
    public partial class EXPORTWS : DBRules
    {
        [MaxLength(100)]
        [StringLength(100)]
        [Required(AllowEmptyStrings = true)]
        public string EXPORTWS_0 { get; set; } // EXPORTWS_0 (length: 100)

        [MaxLength(8000)]
        [StringLength(8000)]
        [Required(AllowEmptyStrings = true)]
        public string DES_0 { get; set; } // DES_0 (length: 8000)

        [MaxLength(100)]
        [StringLength(100)]
        [Required(AllowEmptyStrings = true)]
        public string WSCODE_0 { get; set; } // WSCODE_0 (length: 100)

        [MaxLength(8000)]
        [StringLength(8000)]
        [Required(AllowEmptyStrings = true)]
        public string SELECTQUERY_0 { get; set; } // SELECTQUERY_0 (length: 8000)

        [MaxLength(8000)]
        [StringLength(8000)]
        [Required(AllowEmptyStrings = true)]
        public string DELETEQUERY_0 { get; set; } // DELETEQUERY_0 (length: 8000)

        [MaxLength(200)]
        [StringLength(200)]
        [Required(AllowEmptyStrings = true)]
        public string MAINTABLE_0 { get; set; } // MAINTABLE_0 (length: 200)

        [MaxLength(200)]
        [StringLength(200)]
        [Required(AllowEmptyStrings = true)]
        public string TEMPTABLE_0 { get; set; } // TEMPTABLE_0 (length: 200)

        [MaxLength(200)]
        [StringLength(200)]
        [Required(AllowEmptyStrings = true)]
        public string LOGTABLE_0 { get; set; } // LOGTABLE_0 (length: 200)

        [Required]
        [Range(0, int.MaxValue)]
        public int WRITEFREQUENCY_0 { get; set; } // WRITEFREQUENCY_0

        [Required]
        [Range(0, int.MaxValue)]
        public int REWRITEFREQUENCY_0 { get; set; } // REWRITEFREQUENCY_0

        [Required]
        [SqlDefaultValue(DefaultValue = "('1900-01-01')")]
        public DateTime LASTWRITEDATE_0 { get; set; } // LASTWRITEDATE_0

        [Required]
        [SqlDefaultValue(DefaultValue = "('1900-01-01')")]
        public DateTime NEXTWRITEDATE_0 { get; set; } // NEXTWRITEDATE_0

        [Required]
        [Range(1, 2)]
        public int REWRITEFLG_0 { get; set; } // REWRITEFLG_0

        [Required]
        [SqlDefaultValue(DefaultValue = "('1900-01-01')")]
        public DateTime LASTREWRITEDATE_0 { get; set; } // LASTREWRITEDATE_0

        [Required]
        [SqlDefaultValue(DefaultValue = "('1900-01-01')")]
        public DateTime NEXTREWRITEDATE_0 { get; set; } // NEXTREWRITEDATE_0

        [Required]
        [Range(1, 2)]
        [SqlDefaultValue(DefaultValue = "((2))")]
        public int ENABFLG_0 { get; set; } // ENABFLG_0

        public EXPORTWS()
        {
            InitializePartial();
        }

        partial void InitializePartial();
    }
}
