﻿using System;
using MODELS.Helpers;
using MODELS.Attributes;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MODELS.BusinessObject.ERTSOFT
{
    // USER
    [Table("ERT.[USER]")]
    public partial class USER : DBRules
    {
        [MaxLength(50)]
        [StringLength(50)]
        [Required(AllowEmptyStrings = true)]
        public string USR_0 { get; set; } // USR_0 (length: 50)

        [MaxLength(50)]
        [StringLength(50)]
        [Required(AllowEmptyStrings = true)]
        public string USERNAM_0 { get; set; } // USERNAM_0 (length: 50)

        [MaxLength(2000)]
        [StringLength(2000)]
        [Required(AllowEmptyStrings = true)]
        public string PASSWORD_0 { get; set; } // PASSWORD_0 (length: 2000)

        [MaxLength(200)]
        [StringLength(200)]
        [Required(AllowEmptyStrings = true)]
        public string FULLNAM_0 { get; set; } // FULLNAM_0 (length: 200)

        [MaxLength(100)]
        [StringLength(100)]
        [Required(AllowEmptyStrings = true)]
        public string FIRSTNAM_0 { get; set; } // FIRSTNAM_0 (length: 100)

        [MaxLength(100)]
        [StringLength(100)]
        [Required(AllowEmptyStrings = true)]
        public string LASTNAM_0 { get; set; } // LASTNAM_0 (length: 100)

        [MaxLength(400)]
        [StringLength(400)]
        [Required(AllowEmptyStrings = true)]
        public string EMAIL_0 { get; set; } // EMAIL_0 (length: 400)

        [MaxLength(200)]
        [StringLength(200)]
        [Required(AllowEmptyStrings = true)]
        public string FUNCTION_0 { get; set; } // FUNCTION_0 (length: 200)

        [MaxLength(100)]
        [StringLength(100)]
        [Required(AllowEmptyStrings = true)]
        public string MOBPHONE_0 { get; set; } // MOBPHONE_0 (length: 100)

        [MaxLength(100)]
        [StringLength(100)]
        [Required(AllowEmptyStrings = true)]
        public string PHONE_0 { get; set; } // PHONE_0 (length: 100)

        [Required]
        public DateTime BIRTHDAT_0 { get; set; } // BIRTHDAT_0

        [Required]
        public bool GENDER_0 { get; set; } // GENDER_0

        [MaxLength(100)]
        [StringLength(100)]
        [Required(AllowEmptyStrings = true)]
        public string PROFILE_0 { get; set; } // PROFILE_0 (length: 100)

        [Required]
        [Range(1, 2)]
        [SqlDefaultValue(DefaultValue = "((1))")]
        public int LOGGEDFLG_0 { get; set; } // LOGGEDFLG_0

        [Required]
        [Range(1, 2)]
        public int ADMINFLG_0 { get; set; } // ADMINFLG_0

        [Required]
        [Range(1, 2)]
        public int SPECIALFLG_0 { get; set; } // SPECIALFLG_0

        [Required]
        [Range(1, 2)]
        [SqlDefaultValue(DefaultValue = "((2))")]
        public int ENABFLG_0 { get; set; } // ENABFLG_0

        public USER()
        {
            InitializePartial();
        }

        partial void InitializePartial();
    }
}
