﻿using System;
using MODELS.Helpers;
using MODELS.Attributes;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MODELS.BusinessObject.ERTSOFT
{
    // STOJOU
    [Table("ERT.[STOJOU]")]
    public partial class STOJOU : DBRules
    {
        [MaxLength(50)]
        [StringLength(50)]
        [Required(AllowEmptyStrings = true)]
        public string WSTYP_0 { get; set; } // WSTYP_0 (length: 50)

        [MaxLength(50)]
        [StringLength(50)]
        [Required(AllowEmptyStrings = true)]
        [SqlDefaultValue(DefaultValue = "('')")]
        public string CPY_0 { get; set; } // CPY_0 (length: 50)

        [MaxLength(50)]
        [StringLength(50)]
        [Required(AllowEmptyStrings = true)]
        [SqlDefaultValue(DefaultValue = "('')")]
        public string STOFCY_0 { get; set; } // STOFCY_0 (length: 50)

        [MaxLength(50)]
        [StringLength(50)]
        [Required(AllowEmptyStrings = true)]
        [SqlDefaultValue(DefaultValue = "('')")]
        public string DESSTOFCY_0 { get; set; } // DESSTOFCY_0 (length: 50)

        [MaxLength(50)]
        [StringLength(50)]
        [Required(AllowEmptyStrings = true)]
        [SqlDefaultValue(DefaultValue = "('')")]
        public string MFGNUM_0 { get; set; } // MFGNUM_0 (length: 50)

        [MaxLength(50)]
        [StringLength(50)]
        [Required(AllowEmptyStrings = true)]
        [SqlDefaultValue(DefaultValue = "('')")]
        public string SLOORI_0 { get; set; } // SLOORI_0 (length: 50)

        [Required]
        [Range(0, int.MaxValue)]
        [SqlDefaultValue(DefaultValue = "('')")]
        public int MFGSTA_0 { get; set; } // MFGSTA_0

        [MaxLength(50)]
        [StringLength(50)]
        [Required(AllowEmptyStrings = true)]
        [SqlDefaultValue(DefaultValue = "('')")]
        public string MFGLIN_0 { get; set; } // MFGLIN_0 (length: 50)

        [MaxLength(50)]
        [StringLength(50)]
        [Required(AllowEmptyStrings = true)]
        [SqlDefaultValue(DefaultValue = "('')")]
        public string BOMALT_0 { get; set; } // BOMALT_0 (length: 50)

        [Required]
        [Decimal(28, 13)]
        [SqlDefaultValue(DefaultValue = "((0))")]
        public decimal BOMQTY_0 { get; set; } // BOMQTY_0

        [MaxLength(50)]
        [StringLength(50)]
        [Required(AllowEmptyStrings = true)]
        [SqlDefaultValue(DefaultValue = "((0))")]
        public string BOMSEQ_0 { get; set; } // BOMSEQ_0 (length: 50)

        [MaxLength(50)]
        [StringLength(50)]
        [Required(AllowEmptyStrings = true)]
        [SqlDefaultValue(DefaultValue = "('')")]
        public string BOMNUM_0 { get; set; } // BOMNUM_0 (length: 50)

        [MaxLength(50)]
        [StringLength(50)]
        [Required(AllowEmptyStrings = true)]
        [SqlDefaultValue(DefaultValue = "('')")]
        public string YTRANS_0 { get; set; } // YTRANS_0 (length: 50)

        [MaxLength(50)]
        [StringLength(50)]
        [Required(AllowEmptyStrings = true)]
        [SqlDefaultValue(DefaultValue = "('')")]
        public string YDAT_0 { get; set; } // YDAT_0 (length: 50)

        [MaxLength(50)]
        [StringLength(50)]
        [Required(AllowEmptyStrings = true)]
        [SqlDefaultValue(DefaultValue = "('')")]
        public string TRSTYP_0 { get; set; } // TRSTYP_0 (length: 50)

        [MaxLength(50)]
        [StringLength(50)]
        [Required(AllowEmptyStrings = true)]
        [SqlDefaultValue(DefaultValue = "('')")]
        public string VCRNUMORI_0 { get; set; } // VCRNUMORI_0 (length: 50)

        [MaxLength(50)]
        [StringLength(50)]
        [Required(AllowEmptyStrings = true)]
        [SqlDefaultValue(DefaultValue = "((0))")]
        public string VCRLIN_0 { get; set; } // VCRLIN_0 (length: 50)

        [MaxLength(50)]
        [StringLength(50)]
        [Required(AllowEmptyStrings = true)]
        [SqlDefaultValue(DefaultValue = "('')")]
        public string ITMREF_0 { get; set; } // ITMREF_0 (length: 50)

        [MaxLength(50)]
        [StringLength(50)]
        [Required(AllowEmptyStrings = true)]
        [SqlDefaultValue(DefaultValue = "('')")]
        public string LOT_0 { get; set; } // LOT_0 (length: 50)

        [MaxLength(50)]
        [StringLength(50)]
        [Required(AllowEmptyStrings = true)]
        [SqlDefaultValue(DefaultValue = "('')")]
        public string SLO_0 { get; set; } // SLO_0 (length: 50)

        [Required]
        [Decimal(28, 13)]
        [SqlDefaultValue(DefaultValue = "((0))")]
        public decimal STRQTY_0 { get; set; } // STRQTY_0

        [Required]
        [Decimal(28, 13)]
        [SqlDefaultValue(DefaultValue = "((0))")]
        public decimal USENETQTY_0 { get; set; } // USENETQTY_0

        [Required]
        [Decimal(28, 13)]
        [SqlDefaultValue(DefaultValue = "((0))")]
        public decimal USEGROSSQTY_0 { get; set; } // USEGROSSQTY_0

        [Required]
        [Range(0, int.MaxValue)]
        [SqlDefaultValue(DefaultValue = "((0))")]
        public int DEFECT_0 { get; set; } // DEFECT_0

        [Required]
        [Decimal(28, 13)]
        [SqlDefaultValue(DefaultValue = "((0))")]
        public decimal QTYPCU_0 { get; set; } // QTYPCU_0

        [MaxLength(50)]
        [StringLength(50)]
        [Required(AllowEmptyStrings = true)]
        [SqlDefaultValue(DefaultValue = "('')")]
        public string USELOC_0 { get; set; } // USELOC_0 (length: 50)

        [MaxLength(50)]
        [StringLength(50)]
        [Required(AllowEmptyStrings = true)]
        [SqlDefaultValue(DefaultValue = "('')")]
        public string DESLOC_0 { get; set; } // DESLOC_0 (length: 50)

        [MaxLength(50)]
        [StringLength(50)]
        [Required(AllowEmptyStrings = true)]
        [SqlDefaultValue(DefaultValue = "('')")]
        public string LOC_0 { get; set; } // LOC_0 (length: 50)

        [MaxLength(50)]
        [StringLength(50)]
        [Required(AllowEmptyStrings = true)]
        [SqlDefaultValue(DefaultValue = "('')")]
        public string USELOCTYP_0 { get; set; } // USELOCTYP_0 (length: 50)

        [MaxLength(50)]
        [StringLength(50)]
        [Required(AllowEmptyStrings = true)]
        [SqlDefaultValue(DefaultValue = "('')")]
        public string DESLOCTYP_0 { get; set; } // DESLOCTYP_0 (length: 50)

        [MaxLength(50)]
        [StringLength(50)]
        [Required(AllowEmptyStrings = true)]
        [SqlDefaultValue(DefaultValue = "('')")]
        public string LOCTYP_0 { get; set; } // LOCTYP_0 (length: 50)

        [MaxLength(50)]
        [StringLength(50)]
        [Required(AllowEmptyStrings = true)]
        [SqlDefaultValue(DefaultValue = "('')")]
        public string USESTA_0 { get; set; } // USESTA_0 (length: 50)

        [MaxLength(50)]
        [StringLength(50)]
        [Required(AllowEmptyStrings = true)]
        [SqlDefaultValue(DefaultValue = "('')")]
        public string DESSTA_0 { get; set; } // DESSTA_0 (length: 50)

        [MaxLength(50)]
        [StringLength(50)]
        [Required(AllowEmptyStrings = true)]
        [SqlDefaultValue(DefaultValue = "('')")]
        public string STA_0 { get; set; } // STA_0 (length: 50)

        [MaxLength(50)]
        [StringLength(50)]
        [Required(AllowEmptyStrings = true)]
        [SqlDefaultValue(DefaultValue = "('')")]
        public string USESTU_0 { get; set; } // USESTU_0 (length: 50)

        [MaxLength(50)]
        [StringLength(50)]
        [Required(AllowEmptyStrings = true)]
        [SqlDefaultValue(DefaultValue = "('')")]
        public string STU_0 { get; set; } // STU_0 (length: 50)

        [Required]
        [Range(0, int.MaxValue)]
        [SqlDefaultValue(DefaultValue = "((0))")]
        public int MACID_0 { get; set; } // MACID_0

        [Required]
        [Range(0, int.MaxValue)]
        [SqlDefaultValue(DefaultValue = "((0))")]
        public int OPERATION_0 { get; set; } // OPERATION_0

        [MaxLength(50)]
        [StringLength(50)]
        [Required(AllowEmptyStrings = true)]
        [SqlDefaultValue(DefaultValue = "('')")]
        public string ZCRONOMOV_0 { get; set; } // ZCRONOMOV_0 (length: 50)

        [Required]
        [Range(0, int.MaxValue)]
        [SqlDefaultValue(DefaultValue = "((0))")]
        public int BOXID_0 { get; set; } // BOXID_0

        [MaxLength(50)]
        [StringLength(50)]
        [Required(AllowEmptyStrings = true)]
        [SqlDefaultValue(DefaultValue = "('')")]
        public string CUSORDREF_0 { get; set; } // CUSORDREF_0 (length: 50)

        [Required]
        [Range(0, int.MaxValue)]
        [SqlDefaultValue(DefaultValue = "((1))")]
        public int PDA_0 { get; set; } // PDA_0

        [Required(AllowEmptyStrings = true)]
        [SqlDefaultValue(DefaultValue = "('')")]
        public string XML_0 { get; set; } // XML_0

        [Required(AllowEmptyStrings = true)]
        [SqlDefaultValue(DefaultValue = "('')")]
        public string OUTPUTWS_0 { get; set; } // OUTPUTWS_0

        [Required(AllowEmptyStrings = true)]
        [SqlDefaultValue(DefaultValue = "('')")]
        public string VCRNUM_0 { get; set; } // VCRNUM_0

        [Required]
        [Range(0, int.MaxValue)]
        [SqlDefaultValue(DefaultValue = "((0))")]
        public int ATTEMPTS_0 { get; set; } // ATTEMPTS_0

        [Required]
        [Range(0, int.MaxValue)]
        [SqlDefaultValue(DefaultValue = "((1))")]
        public int SUCCEEDED_0 { get; set; } // SUCCEEDED_0

        [Required]
        [Range(1, 2)]
        [SqlDefaultValue(DefaultValue = "((2))")]
        public int IMPORTFLG_0 { get; set; } // IMPORTFLG_0

        [Required]
        [Range(1, 2)]
        [SqlDefaultValue(DefaultValue = "((1))")]
        public int REGFLG_0 { get; set; } // REGFLG_0

        [MaxLength(255)]
        [StringLength(255)]
        [Required(AllowEmptyStrings = true)]
        [SqlDefaultValue(DefaultValue = "('')")]
        public string HOSTNAM_0 { get; set; } // HOSTNAM_0 (length: 255)

        [MaxLength(50)]
        [StringLength(50)]
        [Required(AllowEmptyStrings = true)]
        [SqlDefaultValue(DefaultValue = "('')")]
        public string WSUSR_0 { get; set; } // WSUSR_0 (length: 50)

        public STOJOU()
        {
            InitializePartial();
        }

        partial void InitializePartial();
    }
}
