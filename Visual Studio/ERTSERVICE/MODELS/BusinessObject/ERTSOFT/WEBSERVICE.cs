﻿using MODELS.Helpers;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MODELS.BusinessObject.ERTSOFT
{
    // WEBSERVICE
    [Table("ERT.[WEBSERVICE]")]
    public partial class WEBSERVICE : DBRules
    {
        [MaxLength(200)]
        [StringLength(200)]
        [Required(AllowEmptyStrings = true)]
        public string CODE_0 { get; set; } // CODE_0 (length: 200)

        [MaxLength(200)]
        [StringLength(200)]
        [Required(AllowEmptyStrings = true)]
        public string NAM_0 { get; set; } // NAM_0 (length: 200)

        [MaxLength(2000)]
        [StringLength(2000)]
        [Required(AllowEmptyStrings = true)]
        public string URL_0 { get; set; } // URL_0 (length: 2000)

        [MaxLength(200)]
        [StringLength(200)]
        [Required(AllowEmptyStrings = true)]
        public string POOL_0 { get; set; } // POOL_0 (length: 200)

        [MaxLength(200)]
        [StringLength(200)]
        [Required(AllowEmptyStrings = true)]
        public string CODELANG_0 { get; set; } // CODELANG_0 (length: 200)

        [MaxLength(8000)]
        [StringLength(8000)]
        [Required(AllowEmptyStrings = true)]
        public string PARAMBEGIN_0 { get; set; } // PARAMBEGIN_0 (length: 8000)

        [Required]
        [Range(0, int.MaxValue)]
        public int MAXROWS_0 { get; set; } // MAXROWS_0

        [MaxLength(200)]
        [StringLength(200)]
        [Required(AllowEmptyStrings = true)]
        public string KEYFLD_0 { get; set; } // KEYFLD_0 (length: 200)

        [MaxLength(200)]
        [StringLength(200)]
        [Required(AllowEmptyStrings = true)]
        public string DATEFLD_0 { get; set; } // DATEFLD_0 (length: 200)

        [MaxLength(200)]
        [StringLength(200)]
        [Required(AllowEmptyStrings = true)]
        public string USER_0 { get; set; } // USER_0 (length: 200)

        [MaxLength(200)]
        [StringLength(200)]
        [Required(AllowEmptyStrings = true)]
        public string PASS_0 { get; set; } // PASS_0 (length: 200)

        [MaxLength(8000)]
        [StringLength(8000)]
        [Required(AllowEmptyStrings = true)]
        public string XMLEXAMPLE_0 { get; set; } // XMLEXAMPLE_0 (length: 8000)

        [Required]
        [Range(0, int.MaxValue)]
        public int ATTEMPTS_0 { get; set; } // ATTEMPTS_0

        [Required]
        [Range(1, 2)]
        public int VCRNUMFLG_0 { get; set; } // VCRNUMFLG_0

        [MaxLength(40)]
        [StringLength(40)]
        [Required(AllowEmptyStrings = true)]
        public string VCRNUMSEARCH_0 { get; set; } // VCRNUMSEARCH_0 (length: 40)

        [Required]
        [Range(0, int.MaxValue)]
        public int VCRNUMSIZE_0 { get; set; } // VCRNUMSIZE_0

        [Required]
        [Range(1, 2)]
        public int IMPORTFLG_0 { get; set; } // IMPORTFLG_0

        [Required]
        [Range(1, 2)]
        public int ENABFLG_0 { get; set; } // ENABFLG_0
    }
}
