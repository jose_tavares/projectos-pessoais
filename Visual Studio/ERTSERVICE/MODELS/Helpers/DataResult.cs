﻿using System;
using System.Collections.Generic;

namespace MODELS.Helpers
{
    public class DataResult
    {
        #region Properties
        public bool Success { get; set; }

        public decimal InsertedId { get; set; }

        public int AffectedRows { get; set; }

        public object Obj { get; set; }

        public List<string> Messages { get; set; }
        #endregion

        #region Constructors
        public DataResult()
        {
            Messages = new List<string>();
            InsertedId = -1;
            AffectedRows = -1;
        }

        public DataResult(bool success)
        {
            Messages = new List<string>();
            Success = success;
        }
        #endregion

        #region Public Methods
        public bool IsInserted(decimal id, object obj, int affectedRows = 1)
        {
            InsertedId = id;
            AffectedRows = affectedRows;
            Obj = obj;

            if (id > 0)
                Inserted(id);
            else
                NotInserted();

            return Success;
        }

        public bool IsInserted(DataResult result)
        {
            InsertedId = result.InsertedId;
            Obj = result.Obj;

            if (InsertedId > 0)
                Inserted(InsertedId);
            else
                NotInserted();

            return Success;
        }

        public bool IsUpdated(DataResult result)
        {
            AffectedRows = result.AffectedRows;
            Obj = result.Obj;

            if (AffectedRows > 0)
                Updated();
            else
                NotUpdated();

            return Success;
        }

        public bool IsDeleted(DataResult result)
        {
            AffectedRows = result.AffectedRows;
            Obj = result.Obj;

            if (AffectedRows > 0)
                Deleted();
            else
                NotDeleted();

            return Success;
        }

        public bool IsUpdated(int affectedRows, object obj)
        {
            AffectedRows = affectedRows;
            Obj = obj;

            if (AffectedRows > 0)
                Updated();
            else
                NotUpdated();

            return Success;
        }

        public bool IsDeleted(int affectedRows)
        {
            AffectedRows = affectedRows;

            if (AffectedRows > 0)
                Deleted();
            else
                NotDeleted();

            return Success;
        }

        public void AddMessage(string message)
        {
            Messages.Add(message);
        }

        public void AddMessage(DataResult result)
        {
            Messages.AddRange(result.Messages);
        }

        public void AddValidMessage(string message)
        {
            Success = true;
            Messages.Add(message);
        }

        public void AddValidMessage(DataResult result)
        {
            Success = true;
            Messages.AddRange(result.Messages);
        }

        public void AddInvalidMessage(string message)
        {
            Success = false;
            Messages.Add(message);
        }

        public void AddInvalidMessage(DataResult result)
        {
            Success = false;
            Messages.AddRange(result.Messages);
        }

        public override string ToString()
        {
            string finalMessage = string.Empty;

            foreach (string message in Messages)
            {
                finalMessage += message + Environment.NewLine;
            }

            return finalMessage;
        }
        #endregion

        #region Private Methods
        private void Inserted(decimal id)
        {
            AddValidMessage("O recurso solicitado foi criado com sucesso com o ID " + id);
        }

        private void NotInserted()
        {
            AddInvalidMessage("Ocorreu um problema ao criar o recurso solicitado.");
        }

        private void Updated()
        {
            AddValidMessage("O recurso solicitado foi atualizado com sucesso.");
        }

        private void NotUpdated()
        {
            AddInvalidMessage("Ocorreu um problema ao atualizar o recurso solicitado.");
        }

        private void Deleted()
        {
            AddValidMessage("O recurso solicitado foi eliminado com sucesso.");
        }

        private void NotDeleted()
        {
            AddInvalidMessage("Ocorreu um problema ao eliminar o recurso solicitado.");
        }
        #endregion
    }
}