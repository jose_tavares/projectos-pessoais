﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DataAnnotationsExtensions;
using MODELS.Attributes;

namespace MODELS.Helpers
{
    public class DBRules
    {
        #region Properties
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [SqlDefaultValue(DefaultValue = "getdate()")]
        public DateTime CREDAT_0 { get; set; }

        [Required]
        [StringLength(50)]
        public string CREUSR_0 { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [SqlDefaultValue(DefaultValue = "getdate()")]
        public DateTime UPDDAT_0 { get; set; }

        [Required]
        [StringLength(50)]
        public string UPDUSR_0 { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [SqlDefaultValue(DefaultValue = "getdate()")]
        public DateTime CREDATTIM_0 { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [SqlDefaultValue(DefaultValue = "getdate()")]
        public DateTime UPDDATTIM_0 { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Decimal(38, 1)]
        [Min(1)]
        [Key]
        public decimal ROWID { get; set; }

        #endregion

        #region Constructors

        public DBRules()
        {
            ROWID = 0;
        }

        public DBRules(int ID)
        {
            ROWID = ID;
        }

        #endregion
    }
}
