﻿using System;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using MODELS.Attributes;
using System.ComponentModel.DataAnnotations.Schema;
using System.Threading.Tasks;

namespace MODELS.Helpers
{
    public static class ClassHelper
    {
        public static bool IsSimpleProperty(Type type)
        {
            return Type.GetTypeCode(type) != TypeCode.Object;
        }

        public static async Task<DataResult> IsClassValidAsync(this object obj)
        {
            return await Task.Run(() => obj.IsClassValid());
        }

        public static DataResult IsClassValid(this object obj)
        {
            List<ValidationResult> validationResults = new List<ValidationResult>();
            DataResult result = new DataResult()
            {
                Obj = obj,
                Success = Validator.TryValidateObject(obj, new ValidationContext(obj), validationResults, true)
            };

            foreach (ValidationResult validationResult in validationResults)
            {
                result.AddInvalidMessage(validationResult.ErrorMessage);
            }


            return result;
        }

        public static async Task<DataResult> IsPropertiesValidAsync(this object obj, params string[] propNames)
        {
            return await Task.Run(() => obj.IsPropertiesValid(propNames));
        }

        public static DataResult IsPropertiesValid(this object obj, params string[] propNames)
        {
            List<ValidationResult> validationResults = new List<ValidationResult>();
            DataResult result = new DataResult()
            {
                Obj = obj
            };

            foreach (string propName in propNames)
            {
                Validator.TryValidateProperty(obj.GetType().GetProperty(propName).GetValue(obj), new ValidationContext(obj) { MemberName = propName }, validationResults);
            }

            if (validationResults.Count == 0)
            {
                result.Success = true;
            }

            foreach (ValidationResult validationResult in validationResults)
            {
                result.AddInvalidMessage(validationResult.ErrorMessage);
            }

            return result;
        }

        public static DataResult IsValueValid<T>(this object value, string propName)
        {
            List<ValidationResult> validationResults = new List<ValidationResult>();
            IEnumerable<ValidationAttribute> attributes = typeof(T).GetProperty(propName).GetCustomAttributes(false).OfType<ValidationAttribute>().ToArray();
            DataResult result = new DataResult()
            {
                Success = Validator.TryValidateValue(value, new ValidationContext(value) { MemberName = propName }, validationResults, attributes)
            };

            foreach (ValidationResult validationResult in validationResults)
            {
                result.AddInvalidMessage(validationResult.ErrorMessage);
            }

            return result;
        }

        public static object GetPropValue(this object obj, string propName)
        {
            foreach (string name in propName.Split('.'))
            {
                if (obj == null)
                {
                    return null;
                }

                PropertyInfo info = obj.GetType().GetProperty(name);

                if (info == null)
                {
                    return null;
                }

                obj = info.GetValue(obj, null);
            }

            return obj;
        }

        public static IEnumerable<PropertyInfo> GetSimpleProperties<T>()
        {
            List<PropertyInfo> list = new List<PropertyInfo>();

            foreach (PropertyInfo prop in typeof(T).GetProperties())
            {
                if (IsSimpleProperty(prop.PropertyType))
                {
                    list.Add(prop);
                }
            }

            return list;
        }

        public static IEnumerable<PropertyInfo> GetSimpleFilteredProperties<T>(bool excludeSqlDefaultValue)
        {
            List<PropertyInfo> list = new List<PropertyInfo>();

            foreach (PropertyInfo prop in typeof(T).GetProperties())
            {
                if (IsSimpleProperty(prop.PropertyType))
                {
                    if (!prop.CustomAttributes.Where(x => x.AttributeType == typeof(SqlDefaultValueAttribute) && excludeSqlDefaultValue || x.AttributeType == typeof(DatabaseGeneratedAttribute) && excludeSqlDefaultValue).Any())
                    {
                        list.Add(prop);
                    }
                }
            }

            return list;
        }

        
    }
}