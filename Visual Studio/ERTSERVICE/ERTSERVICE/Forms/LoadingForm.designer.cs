﻿
namespace ERTSERVICE.Forms
{
    partial class LoadingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TPB_Loading = new ERTSERVICE.CustomControls.TextProgressBar();
            this.PN_Loading = new System.Windows.Forms.Panel();
            this.PB_Loading = new System.Windows.Forms.PictureBox();
            this.PN_Loading.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PB_Loading)).BeginInit();
            this.SuspendLayout();
            // 
            // TPB_Loading
            // 
            this.TPB_Loading.CustomText = "";
            this.TPB_Loading.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TPB_Loading.Location = new System.Drawing.Point(0, 0);
            this.TPB_Loading.Name = "TPB_Loading";
            this.TPB_Loading.ProgressColor = System.Drawing.Color.LightGreen;
            this.TPB_Loading.Size = new System.Drawing.Size(630, 67);
            this.TPB_Loading.TabIndex = 0;
            this.TPB_Loading.TextColor = System.Drawing.Color.Black;
            this.TPB_Loading.TextFont = new System.Drawing.Font("Times New Roman", 11F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.TPB_Loading.Visible = false;
            this.TPB_Loading.VisualMode = ERTSERVICE.CustomControls.ProgressBarDisplayMode.CurrProgress;
            // 
            // PN_Loading
            // 
            this.PN_Loading.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.PN_Loading.Controls.Add(this.TPB_Loading);
            this.PN_Loading.Location = new System.Drawing.Point(255, 476);
            this.PN_Loading.Name = "PN_Loading";
            this.PN_Loading.Size = new System.Drawing.Size(630, 67);
            this.PN_Loading.TabIndex = 1;
            // 
            // PB_Loading
            // 
            this.PB_Loading.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PB_Loading.Image = global::ERTSERVICE.Properties.Resources.LoadingGif;
            this.PB_Loading.InitialImage = null;
            this.PB_Loading.Location = new System.Drawing.Point(413, 131);
            this.PB_Loading.Name = "PB_Loading";
            this.PB_Loading.Size = new System.Drawing.Size(315, 319);
            this.PB_Loading.TabIndex = 2;
            this.PB_Loading.TabStop = false;
            // 
            // Loading
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1140, 580);
            this.ControlBox = false;
            this.Controls.Add(this.PB_Loading);
            this.Controls.Add(this.PN_Loading);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Loading";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Tag = "ResizeAll";
            this.Text = "Loading";
            this.PN_Loading.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PB_Loading)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ERTSERVICE.CustomControls.TextProgressBar TPB_Loading;
        private System.Windows.Forms.Panel PN_Loading;
        private System.Windows.Forms.PictureBox PB_Loading;
    }
}