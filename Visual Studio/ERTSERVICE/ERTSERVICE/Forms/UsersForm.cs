﻿namespace ERTSERVICE.Forms
{
    using System;
    using ERTSERVICE.Helpers;
    using System.Windows.Forms;
    using MODELS.BusinessObject.ERTSOFT;
    using ERTSERVICE.Controllers.Interfaces;
    using MODELS.Helpers;

    public partial class UsersForm : Form
    {
        private readonly IUsersController usersController;

        public UsersForm(IUsersController usersController)
        {
            InitializeComponent();
            this.usersController = usersController;
        }

        #region Control Events
        private void Users_Load(object sender, EventArgs e)
        {
            try
            {
                usersController.LoadTitle();
                usersController.SetEnabled();
                usersController.LoadUsersTable();
            }
            catch /*(Exception ex)*/
            {
                //LogHelper.LogError(ex);
            }
        }

        private async void DGV_Users_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                usersController.SelectionChanging = true;

                bool isItemSelected = false;

                if (DGV_Users.SelectedRows.Count == 1)
                {
                    USER user = await usersController.GetSelectedUser();

                    if (user != null)
                    {
                        usersController.ResetTabPagesTag();
                        usersController.LoadHeaderInfo(user);
                        usersController.LoadTabPage();

                        BTN_Supress.Enabled = true;

                        isItemSelected = true;
                    }
                }

                if (!isItemSelected)
                {
                    usersController.NoItemSelected();
                }
            }
            catch /*(Exception ex)*/
            {
                //LogHelper.LogError(ex);
            }
            finally
            {
                usersController.SelectionChanging = false;
            }
        }

        internal async void TC_DetailedData_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                usersController.SelectionChanging = true;

                TabPage tp = TC_DetailedData.SelectedTab;

                if ((string)tp.Tag == "0")
                {
                    USER user = await usersController.GetSelectedUser();

                    if (user != null)
                    {
                        if (TC_DetailedData.SelectedIndex == 0)
                        {
                            usersController.LoadFirstTab(user);
                        }

                        //Add here more tab indices to load in case there is more tabpages
                    }
                }
            }
            catch /*(Exception ex)*/
            {
                //LogHelper.LogError(ex);
            }
            finally
            {
                usersController.SelectionChanging = false;
            }
        }

        private void BTN_AbandonScreen_Click(object sender, EventArgs e)
        {
            try
            {
                Close();
            }
            catch /*(Exception ex)*/
            {
                //LogHelper.LogError(ex);
            }
        }

        private void BTN_New_Click(object sender, EventArgs e)
        {
            try
            {
                DGV_Users.ClearSelection();
                DGV_Users.Enabled = false;
                BTN_New.Enabled = false;
                BTN_Update.Enabled = false;
                BTN_Create.Enabled = true;
                BTN_Supress.Enabled = false;
                BTN_Abandon.Enabled = true;
                usersController.ClearControls();
                TXT_Username.Focus();
            }
            catch /*(Exception ex)*/
            {
                //LogHelper.LogError(ex);
            }
        }

        private async void BTN_Update_Click(object sender, EventArgs e)
        {
            try
            {
                if (DGV_Users.SelectedRows.Count == 1)
                {
                    if (TXT_Password.Text == TXT_RepeatPassword.Text)
                    {
                        USER user = await usersController.GetSelectedUser();

                        if (user != null)
                        {
                            user.USERNAM_0 = TXT_Username.Text;
                            user.PASSWORD_0 = TXT_Password.Text;
                            user.ENABFLG_0 = Convert.ToInt32(CB_IsActive.Checked) + 1;

                            DataResult result = await usersController.Update(user);

                            if (result.Success)
                            {
                                await usersController.LoadUsersTable(user.ROWID);
                            }

                            BTN_Abandon.PerformClick();

                            Module.ResultMessage(result);
                        }
                    }
                    else
                    {
                        Module.ErrorMessage("As duas passwords não correspondem.");
                    }
                }
            }
            catch /*(Exception ex)*/
            {
                //LogHelper.LogError(ex);
            }
        }

        private async void BTN_Create_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(TXT_Password.Text) && !string.IsNullOrWhiteSpace(TXT_RepeatPassword.Text) && TXT_Password.Text == TXT_RepeatPassword.Text)
                {
                    USER user = new USER
                    {
                        USERNAM_0 = TXT_Username.Text,
                        PASSWORD_0 = TXT_Password.Text,
                        ENABFLG_0 = CB_IsActive.Checked ? 2 : 1
                    };

                    DataResult result = await usersController.Insert(user);

                    if (result.Success)
                    {
                        await usersController.LoadUsersTable(result.AffectedRows);
                        BTN_Abandon.PerformClick();
                    }

                    Module.ResultMessage(result);
                }
                else
                {
                    Module.ErrorMessage("As duas passwords não correspondem.");
                }
            }
            catch /*(Exception ex)*/
            {
                //LogHelper.LogError(ex);
            }
        }

        private void BTN_Supress_Click(object sender, EventArgs e)
        {
            try
            {
                Module.ErrorMessage("Funcionalidade não implementada");
            }
            catch /*(Exception ex)*/
            {
                //LogHelper.LogError(ex);
            }
        }

        private void BTN_Abandon_Click(object sender, EventArgs e)
        {
            try
            {
                BTN_New.Enabled = true;
                BTN_Create.Enabled = false;
                BTN_Update.Enabled = false;
                BTN_Abandon.Enabled = false;
                DGV_Users.Enabled = true;

                if (e.ToString() != "System.EventArgs")
                {
                    DGV_Users_SelectionChanged(DGV_Users, EventArgs.Empty);
                }
            }
            catch /*(Exception ex)*/
            {
                //LogHelper.LogError(ex);
            }
        }

        private void TXT_Username_TextChanged(object sender, EventArgs e)
        {
            try
            {
                usersController.ChangingData();
            }
            catch /*(Exception ex)*/
            {
                //LogHelper.LogError(ex);
            }
        }

        private void TXT_Password_TextChanged(object sender, EventArgs e)
        {
            try
            {
                usersController.ChangingData();
            }
            catch /*(Exception ex)*/
            {
                //LogHelper.LogError(ex);
            }
        }

        private void CB_IsActive_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                usersController.ChangingData();
            }
            catch /*(Exception ex)*/
            {
                //LogHelper.LogError(ex);
            }
        }
        #endregion
    }
}
