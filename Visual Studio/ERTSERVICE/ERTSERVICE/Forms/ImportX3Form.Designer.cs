﻿
namespace ERTSERVICE.Forms
{
    partial class ImportX3Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DGV_Importacoes = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.DGV_Importacoes)).BeginInit();
            this.SuspendLayout();
            // 
            // DGV_Importacoes
            // 
            this.DGV_Importacoes.AllowUserToAddRows = false;
            this.DGV_Importacoes.AllowUserToDeleteRows = false;
            this.DGV_Importacoes.AllowUserToResizeRows = false;
            this.DGV_Importacoes.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(67)))), ((int)(((byte)(91)))));
            this.DGV_Importacoes.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.DGV_Importacoes.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.DGV_Importacoes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGV_Importacoes.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.DGV_Importacoes.Enabled = false;
            this.DGV_Importacoes.Location = new System.Drawing.Point(12, 70);
            this.DGV_Importacoes.Name = "DGV_Importacoes";
            this.DGV_Importacoes.RowHeadersVisible = false;
            this.DGV_Importacoes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGV_Importacoes.Size = new System.Drawing.Size(707, 406);
            this.DGV_Importacoes.TabIndex = 197;
            // 
            // ImportX3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1090, 580);
            this.Controls.Add(this.DGV_Importacoes);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ImportX3";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ImportX3";
            this.Load += new System.EventHandler(this.ImportX3_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DGV_Importacoes)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.DataGridView DGV_Importacoes;
    }
}