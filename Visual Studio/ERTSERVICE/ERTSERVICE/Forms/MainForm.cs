﻿using System;
using ERTSERVICE.Helpers;
using System.Windows.Forms;
using ERTSERVICE.Controllers.Interfaces;

namespace ERTSERVICE.Forms
{
    public partial class MainForm : Form
    {
        private readonly IMainController mainController;
        private readonly IUsersController usersController;

        public MainForm(IMainController mainController, IUsersController usersController)
        {
            InitializeComponent();
            this.mainController = mainController;
            this.usersController = usersController;
        }

        #region Control Events
        private void Main_Load(object sender, EventArgs e)
        {
            try
            {
                mainController.HideSubMenus();
                mainController.LoadToolStripsText();
                mainController.LoadFormProperties();
                mainController.CreateTimers();
            }
            catch (Exception)
            {

            }
        }

        private void Timer_Main_Tick(object sender, EventArgs e)
        {
            try
            {
                mainController.LoadToolStripsText();
            }
            catch (Exception)
            {

            }
        }

        public async void Timer_ImportSage_Tick(object sender, EventArgs e)
        {
            try
            {
                await mainController.ImportSage(sender);
            }
            catch (Exception)
            {

            }
        }

        private async void Timer_ExportSage_Tick(object sender, EventArgs e)
        {
            try
            {
                await mainController.ExportSage();
            }
            catch (Exception)
            {

            }
        }

        private void LBL_Welcome_MouseDown(object sender, MouseEventArgs e)
        {
            try
            {
                mainController.WelcomeMouseDown();
            }
            catch (Exception)
            {

            }
        }

        private void LBL_Welcome_MouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                mainController.WelcomeMouseMove();
            }
            catch (Exception)
            {

            }
        }

        private void LBL_Welcome_MouseUp(object sender, MouseEventArgs e)
        {
            try
            {
                mainController.Dragging = false;
            }
            catch (Exception)
            {

            }
        }

        private void BTN_UserData_Click(object sender, EventArgs e)
        {
            try
            {
                mainController.ShowSubMenu(PN_UserData);
            }
            catch (Exception)
            {

            }
        }

        private void BTN_Users_Click(object sender, EventArgs e)
        {
            try
            {
                mainController.HideSubMenus();
                mainController.OpenFormScreen(usersController.UsersForm());
            }
            catch (Exception)
            {

            }
        }

        private void BTN_SessionUsers_Click(object sender, EventArgs e)
        {
            try
            {
                mainController.HideSubMenus();
            }
            catch (Exception)
            {

            }
        }

        private void BTN_IntegrationData_Click(object sender, EventArgs e)
        {
            try
            {
                mainController.ShowSubMenu(PN_IntegrationData);
            }
            catch (Exception)
            {

            }
        }

        private void BTN_ImportX3_Click(object sender, EventArgs e)
        {
            try
            {
                mainController.HideSubMenus();
                ImportX3Form form = new ImportX3Form();
                mainController.OpenFormScreen(form);
            }
            catch (Exception)
            {

            }
        }

        private void BTN_ExportX3_Click(object sender, EventArgs e)
        {
            try
            {

            }
            catch (Exception)
            {

            }
        }

        private void BTN_StatusImportSage_Click(object sender, EventArgs e)
        {
            try
            {
                mainController.StatusServiceImport();
            }
            catch (Exception)
            {

            }
        }

        private void BTN_StatusExportSage_Click(object sender, EventArgs e)
        {
            try
            {
                mainController.StatusServiceExport();
            }
            catch (Exception)
            {

            }
        }

        private void TS_User_Click(object sender, EventArgs e)
        {
            try
            {
                if (Module.QuestionMessage("Deseja terminar sessão?") == DialogResult.Yes)
                {
                    Close();
                }
            }
            catch (Exception)
            {
            }
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                mainController.CloseForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Erro crítico de sistema, não foi possível fechar o MainForm de forma apropriada. A app irá ser desligada { ex.Message}");
                Application.Exit();
            }
        }
        #endregion
    }
}