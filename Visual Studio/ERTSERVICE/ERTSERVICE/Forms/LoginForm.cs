﻿namespace ERTSERVICE.Forms
{
    using System;
    using DATA.Classes;
    using ERTSERVICE.Helpers;
    using System.Windows.Forms;
    using ERTSERVICE.Controllers.Interfaces;

    public partial class LoginForm : Form
    {
        private readonly ILoginController loginController;

        public LoginForm(ILoginController loginController)
        {
            InitializeComponent();
            this.loginController = loginController;
        }

        private void BTN_Login_Click(object sender, EventArgs e)
        {
            try
            {
                loginController.Login();
                loginController.Logout();
            }
            catch (TokenException ex)
            {
                Module.ErrorMessage(ex.ErrorDescription);
            }
            catch (Exception ex)
            {
                //log exception
                MessageBox.Show(ex.Message);
            }
        }
    }
}
