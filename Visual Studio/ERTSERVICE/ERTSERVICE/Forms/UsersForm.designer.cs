﻿
namespace ERTSERVICE.Forms
{
    partial class UsersForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.PN_Content = new System.Windows.Forms.Panel();
            this.PN_DetailedData = new System.Windows.Forms.Panel();
            this.TC_DetailedData = new System.Windows.Forms.TabControl();
            this.TP_DetailInfo1 = new System.Windows.Forms.TabPage();
            this.TXT_RepeatPassword = new System.Windows.Forms.TextBox();
            this.LBL_RepeatPassword = new System.Windows.Forms.Label();
            this.TXT_Password = new System.Windows.Forms.TextBox();
            this.LBL_Password = new System.Windows.Forms.Label();
            this.TXT_Username = new System.Windows.Forms.TextBox();
            this.LBL_Username = new System.Windows.Forms.Label();
            this.CB_IsActive = new System.Windows.Forms.CheckBox();
            this.PN_DetailedHeaderSeparator = new System.Windows.Forms.Panel();
            this.PN_HeaderData = new System.Windows.Forms.Panel();
            this.CB_IsLogged = new System.Windows.Forms.CheckBox();
            this.TXT_ID = new System.Windows.Forms.TextBox();
            this.LBL_ID = new System.Windows.Forms.Label();
            this.PN_MainTitle = new System.Windows.Forms.Panel();
            this.LBL_MainTitle = new System.Windows.Forms.Label();
            this.PN_ButtonActions = new System.Windows.Forms.Panel();
            this.BTN_Abandon = new System.Windows.Forms.Button();
            this.BTN_Supress = new System.Windows.Forms.Button();
            this.BTN_Create = new System.Windows.Forms.Button();
            this.BTN_Update = new System.Windows.Forms.Button();
            this.BTN_New = new System.Windows.Forms.Button();
            this.BTN_AbandonScreen = new System.Windows.Forms.Button();
            this.PN_MainTable = new System.Windows.Forms.Panel();
            this.PN_MainTableData = new System.Windows.Forms.Panel();
            this.PN_Users = new System.Windows.Forms.Panel();
            this.DGV_Users = new System.Windows.Forms.DataGridView();
            this.PN_UpperLeftCorner = new System.Windows.Forms.Panel();
            this.PN_Pagination = new System.Windows.Forms.Panel();
            this.LBL_Users = new System.Windows.Forms.Label();
            this.PN_SeparatorPagingTable = new System.Windows.Forms.Panel();
            this.PN_SecundaryTitle = new System.Windows.Forms.Panel();
            this.LBL_SecondaryTitle = new System.Windows.Forms.Label();
            this.PN_RightSeparator = new System.Windows.Forms.Panel();
            this.PN_LeftSeparator = new System.Windows.Forms.Panel();
            this.PN_Content.SuspendLayout();
            this.PN_DetailedData.SuspendLayout();
            this.TC_DetailedData.SuspendLayout();
            this.TP_DetailInfo1.SuspendLayout();
            this.PN_HeaderData.SuspendLayout();
            this.PN_MainTitle.SuspendLayout();
            this.PN_ButtonActions.SuspendLayout();
            this.PN_MainTable.SuspendLayout();
            this.PN_MainTableData.SuspendLayout();
            this.PN_Users.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGV_Users)).BeginInit();
            this.PN_UpperLeftCorner.SuspendLayout();
            this.PN_Pagination.SuspendLayout();
            this.PN_SecundaryTitle.SuspendLayout();
            this.SuspendLayout();
            // 
            // PN_Content
            // 
            this.PN_Content.BackColor = System.Drawing.Color.White;
            this.PN_Content.Controls.Add(this.PN_DetailedData);
            this.PN_Content.Controls.Add(this.PN_DetailedHeaderSeparator);
            this.PN_Content.Controls.Add(this.PN_HeaderData);
            this.PN_Content.Controls.Add(this.PN_MainTitle);
            this.PN_Content.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PN_Content.Location = new System.Drawing.Point(249, 0);
            this.PN_Content.Margin = new System.Windows.Forms.Padding(0);
            this.PN_Content.Name = "PN_Content";
            this.PN_Content.Size = new System.Drawing.Size(677, 580);
            this.PN_Content.TabIndex = 29;
            // 
            // PN_DetailedData
            // 
            this.PN_DetailedData.Controls.Add(this.TC_DetailedData);
            this.PN_DetailedData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PN_DetailedData.Location = new System.Drawing.Point(0, 151);
            this.PN_DetailedData.Margin = new System.Windows.Forms.Padding(0);
            this.PN_DetailedData.Name = "PN_DetailedData";
            this.PN_DetailedData.Size = new System.Drawing.Size(677, 429);
            this.PN_DetailedData.TabIndex = 23;
            // 
            // TC_DetailedData
            // 
            this.TC_DetailedData.Controls.Add(this.TP_DetailInfo1);
            this.TC_DetailedData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TC_DetailedData.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TC_DetailedData.Location = new System.Drawing.Point(0, 0);
            this.TC_DetailedData.Margin = new System.Windows.Forms.Padding(0);
            this.TC_DetailedData.Name = "TC_DetailedData";
            this.TC_DetailedData.SelectedIndex = 0;
            this.TC_DetailedData.Size = new System.Drawing.Size(677, 429);
            this.TC_DetailedData.TabIndex = 0;
            this.TC_DetailedData.SelectedIndexChanged += new System.EventHandler(this.TC_DetailedData_SelectedIndexChanged);
            // 
            // TP_DetailInfo1
            // 
            this.TP_DetailInfo1.Controls.Add(this.TXT_RepeatPassword);
            this.TP_DetailInfo1.Controls.Add(this.LBL_RepeatPassword);
            this.TP_DetailInfo1.Controls.Add(this.TXT_Password);
            this.TP_DetailInfo1.Controls.Add(this.LBL_Password);
            this.TP_DetailInfo1.Controls.Add(this.TXT_Username);
            this.TP_DetailInfo1.Controls.Add(this.LBL_Username);
            this.TP_DetailInfo1.Controls.Add(this.CB_IsActive);
            this.TP_DetailInfo1.Location = new System.Drawing.Point(4, 26);
            this.TP_DetailInfo1.Margin = new System.Windows.Forms.Padding(0);
            this.TP_DetailInfo1.Name = "TP_DetailInfo1";
            this.TP_DetailInfo1.Size = new System.Drawing.Size(669, 399);
            this.TP_DetailInfo1.TabIndex = 0;
            this.TP_DetailInfo1.Text = "Identificação";
            this.TP_DetailInfo1.UseVisualStyleBackColor = true;
            // 
            // TXT_RepeatPassword
            // 
            this.TXT_RepeatPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXT_RepeatPassword.Location = new System.Drawing.Point(20, 178);
            this.TXT_RepeatPassword.Margin = new System.Windows.Forms.Padding(0);
            this.TXT_RepeatPassword.Name = "TXT_RepeatPassword";
            this.TXT_RepeatPassword.PasswordChar = '*';
            this.TXT_RepeatPassword.ShortcutsEnabled = false;
            this.TXT_RepeatPassword.Size = new System.Drawing.Size(225, 23);
            this.TXT_RepeatPassword.TabIndex = 11;
            this.TXT_RepeatPassword.Tag = "";
            // 
            // LBL_RepeatPassword
            // 
            this.LBL_RepeatPassword.AutoSize = true;
            this.LBL_RepeatPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LBL_RepeatPassword.Location = new System.Drawing.Point(13, 151);
            this.LBL_RepeatPassword.Margin = new System.Windows.Forms.Padding(0);
            this.LBL_RepeatPassword.Name = "LBL_RepeatPassword";
            this.LBL_RepeatPassword.Size = new System.Drawing.Size(119, 17);
            this.LBL_RepeatPassword.TabIndex = 18;
            this.LBL_RepeatPassword.Text = "Repetir Password";
            // 
            // TXT_Password
            // 
            this.TXT_Password.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXT_Password.Location = new System.Drawing.Point(20, 108);
            this.TXT_Password.Margin = new System.Windows.Forms.Padding(0);
            this.TXT_Password.Name = "TXT_Password";
            this.TXT_Password.PasswordChar = '*';
            this.TXT_Password.ShortcutsEnabled = false;
            this.TXT_Password.Size = new System.Drawing.Size(225, 23);
            this.TXT_Password.TabIndex = 10;
            this.TXT_Password.Tag = "";
            this.TXT_Password.TextChanged += new System.EventHandler(this.TXT_Password_TextChanged);
            // 
            // LBL_Password
            // 
            this.LBL_Password.AutoSize = true;
            this.LBL_Password.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LBL_Password.Location = new System.Drawing.Point(13, 81);
            this.LBL_Password.Margin = new System.Windows.Forms.Padding(0);
            this.LBL_Password.Name = "LBL_Password";
            this.LBL_Password.Size = new System.Drawing.Size(69, 17);
            this.LBL_Password.TabIndex = 16;
            this.LBL_Password.Text = "Password";
            // 
            // TXT_Username
            // 
            this.TXT_Username.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXT_Username.Location = new System.Drawing.Point(20, 39);
            this.TXT_Username.Margin = new System.Windows.Forms.Padding(0);
            this.TXT_Username.Name = "TXT_Username";
            this.TXT_Username.Size = new System.Drawing.Size(225, 23);
            this.TXT_Username.TabIndex = 9;
            this.TXT_Username.Tag = "";
            this.TXT_Username.TextChanged += new System.EventHandler(this.TXT_Username_TextChanged);
            // 
            // LBL_Username
            // 
            this.LBL_Username.AutoSize = true;
            this.LBL_Username.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LBL_Username.Location = new System.Drawing.Point(13, 12);
            this.LBL_Username.Margin = new System.Windows.Forms.Padding(0);
            this.LBL_Username.Name = "LBL_Username";
            this.LBL_Username.Size = new System.Drawing.Size(73, 17);
            this.LBL_Username.TabIndex = 8;
            this.LBL_Username.Text = "Username";
            // 
            // CB_IsActive
            // 
            this.CB_IsActive.AutoSize = true;
            this.CB_IsActive.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CB_IsActive.Location = new System.Drawing.Point(325, 36);
            this.CB_IsActive.Margin = new System.Windows.Forms.Padding(0);
            this.CB_IsActive.Name = "CB_IsActive";
            this.CB_IsActive.Size = new System.Drawing.Size(58, 21);
            this.CB_IsActive.TabIndex = 12;
            this.CB_IsActive.Tag = "";
            this.CB_IsActive.Text = "Ativo";
            this.CB_IsActive.UseVisualStyleBackColor = true;
            this.CB_IsActive.CheckedChanged += new System.EventHandler(this.CB_IsActive_CheckedChanged);
            // 
            // PN_DetailedHeaderSeparator
            // 
            this.PN_DetailedHeaderSeparator.BackColor = System.Drawing.Color.White;
            this.PN_DetailedHeaderSeparator.Dock = System.Windows.Forms.DockStyle.Top;
            this.PN_DetailedHeaderSeparator.Location = new System.Drawing.Point(0, 133);
            this.PN_DetailedHeaderSeparator.Margin = new System.Windows.Forms.Padding(0);
            this.PN_DetailedHeaderSeparator.Name = "PN_DetailedHeaderSeparator";
            this.PN_DetailedHeaderSeparator.Size = new System.Drawing.Size(677, 18);
            this.PN_DetailedHeaderSeparator.TabIndex = 24;
            // 
            // PN_HeaderData
            // 
            this.PN_HeaderData.BackColor = System.Drawing.Color.WhiteSmoke;
            this.PN_HeaderData.Controls.Add(this.CB_IsLogged);
            this.PN_HeaderData.Controls.Add(this.TXT_ID);
            this.PN_HeaderData.Controls.Add(this.LBL_ID);
            this.PN_HeaderData.Dock = System.Windows.Forms.DockStyle.Top;
            this.PN_HeaderData.Location = new System.Drawing.Point(0, 62);
            this.PN_HeaderData.Margin = new System.Windows.Forms.Padding(0);
            this.PN_HeaderData.Name = "PN_HeaderData";
            this.PN_HeaderData.Size = new System.Drawing.Size(677, 71);
            this.PN_HeaderData.TabIndex = 22;
            // 
            // CB_IsLogged
            // 
            this.CB_IsLogged.AutoSize = true;
            this.CB_IsLogged.Enabled = false;
            this.CB_IsLogged.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CB_IsLogged.Location = new System.Drawing.Point(203, 27);
            this.CB_IsLogged.Margin = new System.Windows.Forms.Padding(0);
            this.CB_IsLogged.Name = "CB_IsLogged";
            this.CB_IsLogged.Size = new System.Drawing.Size(70, 21);
            this.CB_IsLogged.TabIndex = 16;
            this.CB_IsLogged.Tag = "";
            this.CB_IsLogged.Text = "Ligado";
            this.CB_IsLogged.UseVisualStyleBackColor = true;
            // 
            // TXT_ID
            // 
            this.TXT_ID.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXT_ID.Location = new System.Drawing.Point(54, 24);
            this.TXT_ID.Margin = new System.Windows.Forms.Padding(0);
            this.TXT_ID.Name = "TXT_ID";
            this.TXT_ID.ReadOnly = true;
            this.TXT_ID.Size = new System.Drawing.Size(94, 23);
            this.TXT_ID.TabIndex = 11;
            this.TXT_ID.Tag = "APP";
            // 
            // LBL_ID
            // 
            this.LBL_ID.AutoSize = true;
            this.LBL_ID.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LBL_ID.Location = new System.Drawing.Point(20, 27);
            this.LBL_ID.Margin = new System.Windows.Forms.Padding(0);
            this.LBL_ID.Name = "LBL_ID";
            this.LBL_ID.Size = new System.Drawing.Size(21, 17);
            this.LBL_ID.TabIndex = 10;
            this.LBL_ID.Text = "ID";
            // 
            // PN_MainTitle
            // 
            this.PN_MainTitle.BackColor = System.Drawing.Color.White;
            this.PN_MainTitle.Controls.Add(this.LBL_MainTitle);
            this.PN_MainTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.PN_MainTitle.Location = new System.Drawing.Point(0, 0);
            this.PN_MainTitle.Margin = new System.Windows.Forms.Padding(0);
            this.PN_MainTitle.Name = "PN_MainTitle";
            this.PN_MainTitle.Size = new System.Drawing.Size(677, 62);
            this.PN_MainTitle.TabIndex = 21;
            // 
            // LBL_MainTitle
            // 
            this.LBL_MainTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LBL_MainTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LBL_MainTitle.Location = new System.Drawing.Point(0, 0);
            this.LBL_MainTitle.Margin = new System.Windows.Forms.Padding(0);
            this.LBL_MainTitle.Name = "LBL_MainTitle";
            this.LBL_MainTitle.Size = new System.Drawing.Size(677, 62);
            this.LBL_MainTitle.TabIndex = 16;
            this.LBL_MainTitle.Text = "TITLE";
            // 
            // PN_ButtonActions
            // 
            this.PN_ButtonActions.Controls.Add(this.BTN_Abandon);
            this.PN_ButtonActions.Controls.Add(this.BTN_Supress);
            this.PN_ButtonActions.Controls.Add(this.BTN_Create);
            this.PN_ButtonActions.Controls.Add(this.BTN_Update);
            this.PN_ButtonActions.Controls.Add(this.BTN_New);
            this.PN_ButtonActions.Controls.Add(this.BTN_AbandonScreen);
            this.PN_ButtonActions.Dock = System.Windows.Forms.DockStyle.Right;
            this.PN_ButtonActions.Location = new System.Drawing.Point(951, 0);
            this.PN_ButtonActions.Margin = new System.Windows.Forms.Padding(0);
            this.PN_ButtonActions.Name = "PN_ButtonActions";
            this.PN_ButtonActions.Size = new System.Drawing.Size(139, 580);
            this.PN_ButtonActions.TabIndex = 28;
            // 
            // BTN_Abandon
            // 
            this.BTN_Abandon.BackColor = System.Drawing.Color.SteelBlue;
            this.BTN_Abandon.Dock = System.Windows.Forms.DockStyle.Top;
            this.BTN_Abandon.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTN_Abandon.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_Abandon.ForeColor = System.Drawing.Color.White;
            this.BTN_Abandon.Location = new System.Drawing.Point(0, 225);
            this.BTN_Abandon.Margin = new System.Windows.Forms.Padding(0);
            this.BTN_Abandon.Name = "BTN_Abandon";
            this.BTN_Abandon.Size = new System.Drawing.Size(139, 45);
            this.BTN_Abandon.TabIndex = 45;
            this.BTN_Abandon.Text = "Abandonar";
            this.BTN_Abandon.UseVisualStyleBackColor = false;
            this.BTN_Abandon.Click += new System.EventHandler(this.BTN_Abandon_Click);
            // 
            // BTN_Supress
            // 
            this.BTN_Supress.BackColor = System.Drawing.Color.SteelBlue;
            this.BTN_Supress.Dock = System.Windows.Forms.DockStyle.Top;
            this.BTN_Supress.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTN_Supress.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_Supress.ForeColor = System.Drawing.Color.White;
            this.BTN_Supress.Location = new System.Drawing.Point(0, 180);
            this.BTN_Supress.Margin = new System.Windows.Forms.Padding(0);
            this.BTN_Supress.Name = "BTN_Supress";
            this.BTN_Supress.Size = new System.Drawing.Size(139, 45);
            this.BTN_Supress.TabIndex = 44;
            this.BTN_Supress.Text = "Suprimir";
            this.BTN_Supress.UseVisualStyleBackColor = false;
            this.BTN_Supress.Click += new System.EventHandler(this.BTN_Supress_Click);
            // 
            // BTN_Create
            // 
            this.BTN_Create.BackColor = System.Drawing.Color.SteelBlue;
            this.BTN_Create.Dock = System.Windows.Forms.DockStyle.Top;
            this.BTN_Create.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTN_Create.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_Create.ForeColor = System.Drawing.Color.White;
            this.BTN_Create.Location = new System.Drawing.Point(0, 135);
            this.BTN_Create.Margin = new System.Windows.Forms.Padding(0);
            this.BTN_Create.Name = "BTN_Create";
            this.BTN_Create.Size = new System.Drawing.Size(139, 45);
            this.BTN_Create.TabIndex = 43;
            this.BTN_Create.Text = "Criar";
            this.BTN_Create.UseVisualStyleBackColor = false;
            this.BTN_Create.Click += new System.EventHandler(this.BTN_Create_Click);
            // 
            // BTN_Update
            // 
            this.BTN_Update.BackColor = System.Drawing.Color.SteelBlue;
            this.BTN_Update.Dock = System.Windows.Forms.DockStyle.Top;
            this.BTN_Update.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTN_Update.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_Update.ForeColor = System.Drawing.Color.White;
            this.BTN_Update.Location = new System.Drawing.Point(0, 90);
            this.BTN_Update.Margin = new System.Windows.Forms.Padding(0);
            this.BTN_Update.Name = "BTN_Update";
            this.BTN_Update.Size = new System.Drawing.Size(139, 45);
            this.BTN_Update.TabIndex = 42;
            this.BTN_Update.Text = "Atualizar";
            this.BTN_Update.UseVisualStyleBackColor = false;
            this.BTN_Update.Click += new System.EventHandler(this.BTN_Update_Click);
            // 
            // BTN_New
            // 
            this.BTN_New.BackColor = System.Drawing.Color.SteelBlue;
            this.BTN_New.Dock = System.Windows.Forms.DockStyle.Top;
            this.BTN_New.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTN_New.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_New.ForeColor = System.Drawing.Color.White;
            this.BTN_New.Location = new System.Drawing.Point(0, 45);
            this.BTN_New.Margin = new System.Windows.Forms.Padding(0);
            this.BTN_New.Name = "BTN_New";
            this.BTN_New.Size = new System.Drawing.Size(139, 45);
            this.BTN_New.TabIndex = 41;
            this.BTN_New.Text = "Novo";
            this.BTN_New.UseVisualStyleBackColor = false;
            this.BTN_New.Click += new System.EventHandler(this.BTN_New_Click);
            // 
            // BTN_AbandonScreen
            // 
            this.BTN_AbandonScreen.BackColor = System.Drawing.Color.SteelBlue;
            this.BTN_AbandonScreen.Dock = System.Windows.Forms.DockStyle.Top;
            this.BTN_AbandonScreen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTN_AbandonScreen.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_AbandonScreen.ForeColor = System.Drawing.Color.White;
            this.BTN_AbandonScreen.Location = new System.Drawing.Point(0, 0);
            this.BTN_AbandonScreen.Margin = new System.Windows.Forms.Padding(0);
            this.BTN_AbandonScreen.Name = "BTN_AbandonScreen";
            this.BTN_AbandonScreen.Size = new System.Drawing.Size(139, 45);
            this.BTN_AbandonScreen.TabIndex = 40;
            this.BTN_AbandonScreen.Text = "Abandonar Ecrã";
            this.BTN_AbandonScreen.UseVisualStyleBackColor = false;
            this.BTN_AbandonScreen.Click += new System.EventHandler(this.BTN_AbandonScreen_Click);
            // 
            // PN_MainTable
            // 
            this.PN_MainTable.Controls.Add(this.PN_MainTableData);
            this.PN_MainTable.Controls.Add(this.PN_UpperLeftCorner);
            this.PN_MainTable.Dock = System.Windows.Forms.DockStyle.Left;
            this.PN_MainTable.Location = new System.Drawing.Point(0, 0);
            this.PN_MainTable.Margin = new System.Windows.Forms.Padding(0);
            this.PN_MainTable.Name = "PN_MainTable";
            this.PN_MainTable.Size = new System.Drawing.Size(224, 580);
            this.PN_MainTable.TabIndex = 27;
            // 
            // PN_MainTableData
            // 
            this.PN_MainTableData.Controls.Add(this.PN_Users);
            this.PN_MainTableData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PN_MainTableData.Location = new System.Drawing.Point(0, 62);
            this.PN_MainTableData.Margin = new System.Windows.Forms.Padding(0);
            this.PN_MainTableData.Name = "PN_MainTableData";
            this.PN_MainTableData.Size = new System.Drawing.Size(224, 518);
            this.PN_MainTableData.TabIndex = 17;
            // 
            // PN_Users
            // 
            this.PN_Users.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PN_Users.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.PN_Users.Controls.Add(this.DGV_Users);
            this.PN_Users.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PN_Users.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PN_Users.Location = new System.Drawing.Point(0, 0);
            this.PN_Users.Margin = new System.Windows.Forms.Padding(0);
            this.PN_Users.Name = "PN_Users";
            this.PN_Users.Size = new System.Drawing.Size(224, 518);
            this.PN_Users.TabIndex = 82;
            // 
            // DGV_Users
            // 
            this.DGV_Users.AllowUserToAddRows = false;
            this.DGV_Users.AllowUserToDeleteRows = false;
            this.DGV_Users.AllowUserToResizeColumns = false;
            this.DGV_Users.AllowUserToResizeRows = false;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(248)))), ((int)(((byte)(249)))));
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            this.DGV_Users.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle7;
            this.DGV_Users.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(236)))), ((int)(((byte)(242)))));
            this.DGV_Users.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.DGV_Users.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.DGV_Users.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(97)))), ((int)(((byte)(147)))), ((int)(((byte)(172)))));
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(97)))), ((int)(((byte)(147)))), ((int)(((byte)(172)))));
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGV_Users.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.DGV_Users.ColumnHeadersHeight = 29;
            this.DGV_Users.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(236)))), ((int)(((byte)(242)))));
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGV_Users.DefaultCellStyle = dataGridViewCellStyle9;
            this.DGV_Users.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DGV_Users.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.DGV_Users.EnableHeadersVisualStyles = false;
            this.DGV_Users.Location = new System.Drawing.Point(0, 0);
            this.DGV_Users.Margin = new System.Windows.Forms.Padding(0);
            this.DGV_Users.MultiSelect = false;
            this.DGV_Users.Name = "DGV_Users";
            this.DGV_Users.ReadOnly = true;
            this.DGV_Users.RowHeadersVisible = false;
            this.DGV_Users.RowHeadersWidth = 23;
            this.DGV_Users.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGV_Users.Size = new System.Drawing.Size(220, 514);
            this.DGV_Users.TabIndex = 200;
            this.DGV_Users.SelectionChanged += new System.EventHandler(this.DGV_Users_SelectionChanged);
            // 
            // PN_UpperLeftCorner
            // 
            this.PN_UpperLeftCorner.Controls.Add(this.PN_Pagination);
            this.PN_UpperLeftCorner.Controls.Add(this.PN_SecundaryTitle);
            this.PN_UpperLeftCorner.Dock = System.Windows.Forms.DockStyle.Top;
            this.PN_UpperLeftCorner.Location = new System.Drawing.Point(0, 0);
            this.PN_UpperLeftCorner.Margin = new System.Windows.Forms.Padding(0);
            this.PN_UpperLeftCorner.Name = "PN_UpperLeftCorner";
            this.PN_UpperLeftCorner.Size = new System.Drawing.Size(224, 62);
            this.PN_UpperLeftCorner.TabIndex = 16;
            // 
            // PN_Pagination
            // 
            this.PN_Pagination.Controls.Add(this.LBL_Users);
            this.PN_Pagination.Controls.Add(this.PN_SeparatorPagingTable);
            this.PN_Pagination.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PN_Pagination.Location = new System.Drawing.Point(0, 28);
            this.PN_Pagination.Margin = new System.Windows.Forms.Padding(0);
            this.PN_Pagination.Name = "PN_Pagination";
            this.PN_Pagination.Size = new System.Drawing.Size(224, 34);
            this.PN_Pagination.TabIndex = 22;
            // 
            // LBL_Users
            // 
            this.LBL_Users.BackColor = System.Drawing.Color.SteelBlue;
            this.LBL_Users.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LBL_Users.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LBL_Users.ForeColor = System.Drawing.Color.White;
            this.LBL_Users.Location = new System.Drawing.Point(0, 0);
            this.LBL_Users.Margin = new System.Windows.Forms.Padding(0);
            this.LBL_Users.Name = "LBL_Users";
            this.LBL_Users.Size = new System.Drawing.Size(224, 24);
            this.LBL_Users.TabIndex = 70;
            this.LBL_Users.Text = "Utilizadores: 0";
            this.LBL_Users.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // PN_SeparatorPagingTable
            // 
            this.PN_SeparatorPagingTable.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.PN_SeparatorPagingTable.Location = new System.Drawing.Point(0, 24);
            this.PN_SeparatorPagingTable.Name = "PN_SeparatorPagingTable";
            this.PN_SeparatorPagingTable.Size = new System.Drawing.Size(224, 10);
            this.PN_SeparatorPagingTable.TabIndex = 0;
            // 
            // PN_SecundaryTitle
            // 
            this.PN_SecundaryTitle.Controls.Add(this.LBL_SecondaryTitle);
            this.PN_SecundaryTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.PN_SecundaryTitle.Location = new System.Drawing.Point(0, 0);
            this.PN_SecundaryTitle.Margin = new System.Windows.Forms.Padding(4);
            this.PN_SecundaryTitle.Name = "PN_SecundaryTitle";
            this.PN_SecundaryTitle.Size = new System.Drawing.Size(224, 28);
            this.PN_SecundaryTitle.TabIndex = 21;
            // 
            // LBL_SecondaryTitle
            // 
            this.LBL_SecondaryTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LBL_SecondaryTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LBL_SecondaryTitle.Location = new System.Drawing.Point(0, 0);
            this.LBL_SecondaryTitle.Margin = new System.Windows.Forms.Padding(0);
            this.LBL_SecondaryTitle.Name = "LBL_SecondaryTitle";
            this.LBL_SecondaryTitle.Size = new System.Drawing.Size(224, 28);
            this.LBL_SecondaryTitle.TabIndex = 18;
            this.LBL_SecondaryTitle.Text = "TITLE";
            // 
            // PN_RightSeparator
            // 
            this.PN_RightSeparator.BackColor = System.Drawing.Color.White;
            this.PN_RightSeparator.Dock = System.Windows.Forms.DockStyle.Right;
            this.PN_RightSeparator.Location = new System.Drawing.Point(926, 0);
            this.PN_RightSeparator.Margin = new System.Windows.Forms.Padding(0);
            this.PN_RightSeparator.Name = "PN_RightSeparator";
            this.PN_RightSeparator.Size = new System.Drawing.Size(25, 580);
            this.PN_RightSeparator.TabIndex = 31;
            // 
            // PN_LeftSeparator
            // 
            this.PN_LeftSeparator.BackColor = System.Drawing.Color.White;
            this.PN_LeftSeparator.Dock = System.Windows.Forms.DockStyle.Left;
            this.PN_LeftSeparator.Location = new System.Drawing.Point(224, 0);
            this.PN_LeftSeparator.Margin = new System.Windows.Forms.Padding(0);
            this.PN_LeftSeparator.Name = "PN_LeftSeparator";
            this.PN_LeftSeparator.Size = new System.Drawing.Size(25, 580);
            this.PN_LeftSeparator.TabIndex = 30;
            // 
            // UsersForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1090, 580);
            this.Controls.Add(this.PN_Content);
            this.Controls.Add(this.PN_RightSeparator);
            this.Controls.Add(this.PN_LeftSeparator);
            this.Controls.Add(this.PN_ButtonActions);
            this.Controls.Add(this.PN_MainTable);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "UsersForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Utilizadores";
            this.Load += new System.EventHandler(this.Users_Load);
            this.PN_Content.ResumeLayout(false);
            this.PN_DetailedData.ResumeLayout(false);
            this.TC_DetailedData.ResumeLayout(false);
            this.TP_DetailInfo1.ResumeLayout(false);
            this.TP_DetailInfo1.PerformLayout();
            this.PN_HeaderData.ResumeLayout(false);
            this.PN_HeaderData.PerformLayout();
            this.PN_MainTitle.ResumeLayout(false);
            this.PN_ButtonActions.ResumeLayout(false);
            this.PN_MainTable.ResumeLayout(false);
            this.PN_MainTableData.ResumeLayout(false);
            this.PN_Users.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DGV_Users)).EndInit();
            this.PN_UpperLeftCorner.ResumeLayout(false);
            this.PN_Pagination.ResumeLayout(false);
            this.PN_SecundaryTitle.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Panel PN_Content;
        internal System.Windows.Forms.Panel PN_DetailedData;
        internal System.Windows.Forms.Panel PN_DetailedHeaderSeparator;
        internal System.Windows.Forms.Panel PN_HeaderData;
        internal System.Windows.Forms.Panel PN_MainTitle;
        internal System.Windows.Forms.Label LBL_MainTitle;
        internal System.Windows.Forms.Panel PN_ButtonActions;
        internal System.Windows.Forms.Panel PN_MainTable;
        internal System.Windows.Forms.Panel PN_MainTableData;
        internal System.Windows.Forms.Panel PN_UpperLeftCorner;
        internal System.Windows.Forms.Panel PN_Pagination;
        internal System.Windows.Forms.Panel PN_SecundaryTitle;
        internal System.Windows.Forms.Label LBL_SecondaryTitle;
        internal System.Windows.Forms.CheckBox CB_IsLogged;
        internal System.Windows.Forms.TextBox TXT_ID;
        internal System.Windows.Forms.Label LBL_ID;
        internal System.Windows.Forms.Panel PN_RightSeparator;
        internal System.Windows.Forms.Panel PN_LeftSeparator;
        private System.Windows.Forms.TabPage TP_DetailInfo1;
        internal System.Windows.Forms.TextBox TXT_Password;
        internal System.Windows.Forms.Label LBL_Password;
        internal System.Windows.Forms.TextBox TXT_Username;
        internal System.Windows.Forms.Label LBL_Username;
        internal System.Windows.Forms.CheckBox CB_IsActive;
        internal System.Windows.Forms.TextBox TXT_RepeatPassword;
        internal System.Windows.Forms.Label LBL_RepeatPassword;
        internal System.Windows.Forms.Panel PN_Users;
        internal System.Windows.Forms.DataGridView DGV_Users;
        internal System.Windows.Forms.Label LBL_Users;
        private System.Windows.Forms.Panel PN_SeparatorPagingTable;
        internal System.Windows.Forms.Button BTN_AbandonScreen;
        internal System.Windows.Forms.Button BTN_Abandon;
        internal System.Windows.Forms.Button BTN_Supress;
        internal System.Windows.Forms.Button BTN_Create;
        internal System.Windows.Forms.Button BTN_Update;
        internal System.Windows.Forms.Button BTN_New;
        internal System.Windows.Forms.TabControl TC_DetailedData;
    }
}