﻿namespace ERTSERVICE.Forms
{
    partial class LoginForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LBL_Username = new System.Windows.Forms.Label();
            this.TXT_Username = new System.Windows.Forms.TextBox();
            this.LBL_Password = new System.Windows.Forms.Label();
            this.TXT_Password = new System.Windows.Forms.TextBox();
            this.BTN_Login = new System.Windows.Forms.Button();
            this.PN_Titulo = new System.Windows.Forms.Panel();
            this.LBL_IniciarSessao = new System.Windows.Forms.Label();
            this.PN_UserInformation = new System.Windows.Forms.Panel();
            this.PB_Login = new System.Windows.Forms.PictureBox();
            this.PN_Login = new System.Windows.Forms.Panel();
            this.PN_Titulo.SuspendLayout();
            this.PN_UserInformation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PB_Login)).BeginInit();
            this.PN_Login.SuspendLayout();
            this.SuspendLayout();
            // 
            // LBL_Username
            // 
            this.LBL_Username.AutoSize = true;
            this.LBL_Username.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LBL_Username.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.LBL_Username.Location = new System.Drawing.Point(18, 15);
            this.LBL_Username.Margin = new System.Windows.Forms.Padding(0);
            this.LBL_Username.Name = "LBL_Username";
            this.LBL_Username.Size = new System.Drawing.Size(200, 25);
            this.LBL_Username.TabIndex = 1;
            this.LBL_Username.Text = "Nome de Utilizador:";
            // 
            // TXT_Username
            // 
            this.TXT_Username.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXT_Username.Location = new System.Drawing.Point(22, 53);
            this.TXT_Username.Margin = new System.Windows.Forms.Padding(0);
            this.TXT_Username.Name = "TXT_Username";
            this.TXT_Username.Size = new System.Drawing.Size(315, 26);
            this.TXT_Username.TabIndex = 2;
            // 
            // LBL_Password
            // 
            this.LBL_Password.AutoSize = true;
            this.LBL_Password.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LBL_Password.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.LBL_Password.Location = new System.Drawing.Point(18, 88);
            this.LBL_Password.Margin = new System.Windows.Forms.Padding(0);
            this.LBL_Password.Name = "LBL_Password";
            this.LBL_Password.Size = new System.Drawing.Size(112, 25);
            this.LBL_Password.TabIndex = 3;
            this.LBL_Password.Text = "Password:";
            // 
            // TXT_Password
            // 
            this.TXT_Password.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXT_Password.Location = new System.Drawing.Point(22, 128);
            this.TXT_Password.Margin = new System.Windows.Forms.Padding(0);
            this.TXT_Password.Name = "TXT_Password";
            this.TXT_Password.PasswordChar = '*';
            this.TXT_Password.ShortcutsEnabled = false;
            this.TXT_Password.Size = new System.Drawing.Size(315, 26);
            this.TXT_Password.TabIndex = 4;
            // 
            // BTN_Login
            // 
            this.BTN_Login.BackColor = System.Drawing.Color.SteelBlue;
            this.BTN_Login.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BTN_Login.FlatAppearance.BorderSize = 0;
            this.BTN_Login.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_Login.ForeColor = System.Drawing.Color.White;
            this.BTN_Login.Location = new System.Drawing.Point(0, 0);
            this.BTN_Login.Margin = new System.Windows.Forms.Padding(0);
            this.BTN_Login.Name = "BTN_Login";
            this.BTN_Login.Size = new System.Drawing.Size(358, 72);
            this.BTN_Login.TabIndex = 5;
            this.BTN_Login.Text = "Login";
            this.BTN_Login.UseVisualStyleBackColor = false;
            this.BTN_Login.Click += new System.EventHandler(this.BTN_Login_Click);
            // 
            // PN_Titulo
            // 
            this.PN_Titulo.Controls.Add(this.LBL_IniciarSessao);
            this.PN_Titulo.Dock = System.Windows.Forms.DockStyle.Top;
            this.PN_Titulo.Location = new System.Drawing.Point(0, 0);
            this.PN_Titulo.Margin = new System.Windows.Forms.Padding(0);
            this.PN_Titulo.Name = "PN_Titulo";
            this.PN_Titulo.Size = new System.Drawing.Size(358, 72);
            this.PN_Titulo.TabIndex = 6;
            // 
            // LBL_IniciarSessao
            // 
            this.LBL_IniciarSessao.AutoEllipsis = true;
            this.LBL_IniciarSessao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LBL_IniciarSessao.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LBL_IniciarSessao.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(28)))), ((int)(((byte)(86)))));
            this.LBL_IniciarSessao.Location = new System.Drawing.Point(0, 0);
            this.LBL_IniciarSessao.Margin = new System.Windows.Forms.Padding(0);
            this.LBL_IniciarSessao.Name = "LBL_IniciarSessao";
            this.LBL_IniciarSessao.Size = new System.Drawing.Size(358, 72);
            this.LBL_IniciarSessao.TabIndex = 0;
            this.LBL_IniciarSessao.Text = "Iniciar Sessão";
            this.LBL_IniciarSessao.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PN_UserInformation
            // 
            this.PN_UserInformation.Controls.Add(this.PB_Login);
            this.PN_UserInformation.Controls.Add(this.TXT_Username);
            this.PN_UserInformation.Controls.Add(this.LBL_Username);
            this.PN_UserInformation.Controls.Add(this.LBL_Password);
            this.PN_UserInformation.Controls.Add(this.TXT_Password);
            this.PN_UserInformation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PN_UserInformation.Location = new System.Drawing.Point(0, 72);
            this.PN_UserInformation.Margin = new System.Windows.Forms.Padding(0);
            this.PN_UserInformation.Name = "PN_UserInformation";
            this.PN_UserInformation.Size = new System.Drawing.Size(358, 455);
            this.PN_UserInformation.TabIndex = 7;
            // 
            // PB_Login
            // 
            this.PB_Login.BackColor = System.Drawing.Color.Transparent;
            this.PB_Login.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.PB_Login.Image = global::ERTSERVICE.Properties.Resources.Login;
            this.PB_Login.InitialImage = null;
            this.PB_Login.Location = new System.Drawing.Point(71, 154);
            this.PB_Login.Margin = new System.Windows.Forms.Padding(2);
            this.PB_Login.Name = "PB_Login";
            this.PB_Login.Size = new System.Drawing.Size(216, 254);
            this.PB_Login.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PB_Login.TabIndex = 6;
            this.PB_Login.TabStop = false;
            // 
            // PN_Login
            // 
            this.PN_Login.Controls.Add(this.BTN_Login);
            this.PN_Login.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.PN_Login.Location = new System.Drawing.Point(0, 455);
            this.PN_Login.Margin = new System.Windows.Forms.Padding(0);
            this.PN_Login.Name = "PN_Login";
            this.PN_Login.Size = new System.Drawing.Size(358, 72);
            this.PN_Login.TabIndex = 8;
            // 
            // LoginForm
            // 
            this.AcceptButton = this.BTN_Login;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(358, 527);
            this.Controls.Add(this.PN_Login);
            this.Controls.Add(this.PN_UserInformation);
            this.Controls.Add(this.PN_Titulo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LoginForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Login";
            this.PN_Titulo.ResumeLayout(false);
            this.PN_UserInformation.ResumeLayout(false);
            this.PN_UserInformation.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PB_Login)).EndInit();
            this.PN_Login.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label LBL_Username;
        private System.Windows.Forms.Label LBL_Password;
        private System.Windows.Forms.Button BTN_Login;
        private System.Windows.Forms.Panel PN_Titulo;
        private System.Windows.Forms.Panel PN_Login;
        private System.Windows.Forms.Label LBL_IniciarSessao;
        private System.Windows.Forms.PictureBox PB_Login;
        internal System.Windows.Forms.TextBox TXT_Username;
        internal System.Windows.Forms.TextBox TXT_Password;
        internal System.Windows.Forms.Panel PN_UserInformation;
    }
}