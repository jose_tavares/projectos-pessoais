﻿using System;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;
using ERTSERVICE.Helpers;

namespace ERTSERVICE.Forms
{
    public partial class LoadingForm : Form
    {
        private bool closeForm = false;

        public LoadingForm()
        {
            InitializeComponent();
        }

        private void Loading_Load(object sender, EventArgs e)
        {
            try
            {
                //Descobrir form principal e o controlo onde ficam os ecras. Ao encontrar defino este ecra e coloco com o mesmo tamanho e posição.
                foreach (Form Form in Module.GetOpenForms())
                {
                    if (Form is MainForm Main)
                    {
                        foreach (Control Control in Main.PN_Main.Controls)
                        {
                            if (Control.Tag != null && Control.Tag.ToString() == "PN_Screens")
                            {
                                base.BackColor = Control.BackColor;
                                Width = Control.Width;
                                Height = Control.Height;
                                Location = new Point(Control.Left + Form.Left, Control.Top + Form.Top);
                            }
                        }
                    }
                }

                Module.LoadingForm = this;
            }
            catch (Exception /*ex*/)
            {
                //LogHelper.LogError(ex);
            }
        }

        private void Loading_Shown(object sender, EventArgs e)
        {
            try
            {
                CheckForIllegalCrossThreadCalls = false;
                CheckForIllegalCrossThreadCalls = true;
                TopLevel = true;
            }
            catch (Exception /*ex*/)
            {
                //LogHelper.LogError(ex);
            }
        }

        private void Loading_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                if (!closeForm)
                {
                    e.Cancel = true;
                }
            }
            catch (Exception /*ex*/)
            {
                //LogHelper.LogError(ex);
            }
        }

        private void Loading_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                Module.LoadingForm = null;
            }
            catch (Exception /*ex*/)
            {
                //LogHelper.LogError(ex);
            }
        }

        public void UpdateProgressBar(int value, int maximum = 100)
        {
            Task t = Task.Run(() =>
            {
                if (value > maximum)
                {
                    value = maximum;
                }

                if (TPB_Loading.InvokeRequired)
                {
                    TPB_Loading.BeginInvoke(new Action(() => TPB_Loading.Visible = true));
                    TPB_Loading.BeginInvoke(new Action(() => TPB_Loading.Maximum = maximum));
                    TPB_Loading.BeginInvoke(new Action(() => TPB_Loading.Value = value));
                }
            });
        }

        public new void Hide()
        {
            closeForm = true;
            BeginInvoke((Action)delegate { Close(); });
        }
    }
}
