﻿
namespace ERTSERVICE.Forms
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PN_Main = new System.Windows.Forms.Panel();
            this.PN_Screens = new System.Windows.Forms.Panel();
            this.SS_Footer = new System.Windows.Forms.StatusStrip();
            this.TS_User = new System.Windows.Forms.ToolStripStatusLabel();
            this.TS_Computer = new System.Windows.Forms.ToolStripStatusLabel();
            this.TS_OnlineTime = new System.Windows.Forms.ToolStripStatusLabel();
            this.TS_Version = new System.Windows.Forms.ToolStripStatusLabel();
            this.PN_Header = new System.Windows.Forms.Panel();
            this.BTN_StatusImportSage = new System.Windows.Forms.Button();
            this.BTN_StatusExportSage = new System.Windows.Forms.Button();
            this.LBL_Welcome = new System.Windows.Forms.Label();
            this.PN_Modules = new System.Windows.Forms.Panel();
            this.PN_IntegrationData = new System.Windows.Forms.Panel();
            this.BTN_ExportX3 = new System.Windows.Forms.Button();
            this.BTN_ImportX3 = new System.Windows.Forms.Button();
            this.BTN_IntegrationData = new System.Windows.Forms.Button();
            this.PN_UserData = new System.Windows.Forms.Panel();
            this.BTN_SessionUsers = new System.Windows.Forms.Button();
            this.BTN_Users = new System.Windows.Forms.Button();
            this.BTN_UserData = new System.Windows.Forms.Button();
            this.PN_AppInfo = new System.Windows.Forms.Panel();
            this.PN_Logo = new System.Windows.Forms.PictureBox();
            this.Timer_Main = new System.Windows.Forms.Timer(this.components);
            this.Timer_ExportSage = new System.Windows.Forms.Timer(this.components);
            this.PN_Main.SuspendLayout();
            this.SS_Footer.SuspendLayout();
            this.PN_Header.SuspendLayout();
            this.PN_Modules.SuspendLayout();
            this.PN_IntegrationData.SuspendLayout();
            this.PN_UserData.SuspendLayout();
            this.PN_AppInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PN_Logo)).BeginInit();
            this.SuspendLayout();
            // 
            // PN_Main
            // 
            this.PN_Main.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(30)))), ((int)(((byte)(45)))));
            this.PN_Main.Controls.Add(this.PN_Screens);
            this.PN_Main.Controls.Add(this.SS_Footer);
            this.PN_Main.Controls.Add(this.PN_Header);
            this.PN_Main.Controls.Add(this.PN_Modules);
            this.PN_Main.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PN_Main.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PN_Main.Location = new System.Drawing.Point(0, 0);
            this.PN_Main.Margin = new System.Windows.Forms.Padding(0);
            this.PN_Main.Name = "PN_Main";
            this.PN_Main.Size = new System.Drawing.Size(1340, 740);
            this.PN_Main.TabIndex = 126;
            this.PN_Main.Tag = "Principal";
            // 
            // PN_Screens
            // 
            this.PN_Screens.BackColor = System.Drawing.Color.White;
            this.PN_Screens.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PN_Screens.Location = new System.Drawing.Point(200, 80);
            this.PN_Screens.Name = "PN_Screens";
            this.PN_Screens.Size = new System.Drawing.Size(1140, 580);
            this.PN_Screens.TabIndex = 128;
            this.PN_Screens.Tag = "PN_Screens";
            // 
            // SS_Footer
            // 
            this.SS_Footer.AutoSize = false;
            this.SS_Footer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(67)))), ((int)(((byte)(91)))));
            this.SS_Footer.Font = new System.Drawing.Font("Century Gothic", 7.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SS_Footer.GripMargin = new System.Windows.Forms.Padding(0);
            this.SS_Footer.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.SS_Footer.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TS_User,
            this.TS_Computer,
            this.TS_OnlineTime,
            this.TS_Version});
            this.SS_Footer.Location = new System.Drawing.Point(200, 660);
            this.SS_Footer.Name = "SS_Footer";
            this.SS_Footer.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.SS_Footer.Size = new System.Drawing.Size(1140, 80);
            this.SS_Footer.SizingGrip = false;
            this.SS_Footer.TabIndex = 126;
            this.SS_Footer.Text = "StatusStrip";
            // 
            // TS_User
            // 
            this.TS_User.AutoSize = false;
            this.TS_User.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.TS_User.Font = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TS_User.ForeColor = System.Drawing.Color.White;
            this.TS_User.Name = "TS_User";
            this.TS_User.Size = new System.Drawing.Size(281, 75);
            this.TS_User.Spring = true;
            this.TS_User.Text = "User";
            this.TS_User.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.TS_User.Click += new System.EventHandler(this.TS_User_Click);
            // 
            // TS_Computer
            // 
            this.TS_Computer.AutoSize = false;
            this.TS_Computer.BackColor = System.Drawing.SystemColors.Control;
            this.TS_Computer.Font = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TS_Computer.ForeColor = System.Drawing.Color.White;
            this.TS_Computer.Name = "TS_Computer";
            this.TS_Computer.Size = new System.Drawing.Size(281, 75);
            this.TS_Computer.Spring = true;
            this.TS_Computer.Text = "Computer";
            this.TS_Computer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // TS_OnlineTime
            // 
            this.TS_OnlineTime.AutoSize = false;
            this.TS_OnlineTime.BackColor = System.Drawing.SystemColors.Control;
            this.TS_OnlineTime.Font = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TS_OnlineTime.ForeColor = System.Drawing.Color.White;
            this.TS_OnlineTime.Name = "TS_OnlineTime";
            this.TS_OnlineTime.Size = new System.Drawing.Size(281, 75);
            this.TS_OnlineTime.Spring = true;
            this.TS_OnlineTime.Text = "Online: 0D 0H 0M";
            this.TS_OnlineTime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // TS_Version
            // 
            this.TS_Version.AutoSize = false;
            this.TS_Version.BackColor = System.Drawing.SystemColors.Control;
            this.TS_Version.Font = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TS_Version.ForeColor = System.Drawing.Color.White;
            this.TS_Version.Name = "TS_Version";
            this.TS_Version.Size = new System.Drawing.Size(281, 75);
            this.TS_Version.Spring = true;
            this.TS_Version.Text = "Version";
            this.TS_Version.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // PN_Header
            // 
            this.PN_Header.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.PN_Header.Controls.Add(this.BTN_StatusImportSage);
            this.PN_Header.Controls.Add(this.BTN_StatusExportSage);
            this.PN_Header.Controls.Add(this.LBL_Welcome);
            this.PN_Header.Dock = System.Windows.Forms.DockStyle.Top;
            this.PN_Header.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PN_Header.Location = new System.Drawing.Point(200, 0);
            this.PN_Header.Name = "PN_Header";
            this.PN_Header.Size = new System.Drawing.Size(1140, 80);
            this.PN_Header.TabIndex = 125;
            // 
            // BTN_StatusImportSage
            // 
            this.BTN_StatusImportSage.BackColor = System.Drawing.Color.SteelBlue;
            this.BTN_StatusImportSage.Dock = System.Windows.Forms.DockStyle.Right;
            this.BTN_StatusImportSage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTN_StatusImportSage.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_StatusImportSage.ForeColor = System.Drawing.Color.LightGray;
            this.BTN_StatusImportSage.Location = new System.Drawing.Point(862, 0);
            this.BTN_StatusImportSage.Name = "BTN_StatusImportSage";
            this.BTN_StatusImportSage.Size = new System.Drawing.Size(139, 80);
            this.BTN_StatusImportSage.TabIndex = 19;
            this.BTN_StatusImportSage.Text = "Status importação: Inativo";
            this.BTN_StatusImportSage.UseVisualStyleBackColor = false;
            this.BTN_StatusImportSage.Click += new System.EventHandler(this.BTN_StatusImportSage_Click);
            // 
            // BTN_StatusExportSage
            // 
            this.BTN_StatusExportSage.BackColor = System.Drawing.Color.SteelBlue;
            this.BTN_StatusExportSage.Dock = System.Windows.Forms.DockStyle.Right;
            this.BTN_StatusExportSage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTN_StatusExportSage.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_StatusExportSage.ForeColor = System.Drawing.Color.LightGray;
            this.BTN_StatusExportSage.Location = new System.Drawing.Point(1001, 0);
            this.BTN_StatusExportSage.Name = "BTN_StatusExportSage";
            this.BTN_StatusExportSage.Size = new System.Drawing.Size(139, 80);
            this.BTN_StatusExportSage.TabIndex = 18;
            this.BTN_StatusExportSage.Text = "Status exportação: Inativo";
            this.BTN_StatusExportSage.UseVisualStyleBackColor = false;
            this.BTN_StatusExportSage.Click += new System.EventHandler(this.BTN_StatusExportSage_Click);
            // 
            // LBL_Welcome
            // 
            this.LBL_Welcome.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(67)))), ((int)(((byte)(91)))));
            this.LBL_Welcome.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LBL_Welcome.Font = new System.Drawing.Font("Century Gothic", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LBL_Welcome.ForeColor = System.Drawing.Color.White;
            this.LBL_Welcome.Location = new System.Drawing.Point(0, 0);
            this.LBL_Welcome.Name = "LBL_Welcome";
            this.LBL_Welcome.Size = new System.Drawing.Size(1140, 80);
            this.LBL_Welcome.TabIndex = 5;
            this.LBL_Welcome.Text = "BEM VINDO AO ERTSERVICE";
            this.LBL_Welcome.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.LBL_Welcome.MouseDown += new System.Windows.Forms.MouseEventHandler(this.LBL_Welcome_MouseDown);
            this.LBL_Welcome.MouseMove += new System.Windows.Forms.MouseEventHandler(this.LBL_Welcome_MouseMove);
            this.LBL_Welcome.MouseUp += new System.Windows.Forms.MouseEventHandler(this.LBL_Welcome_MouseUp);
            // 
            // PN_Modules
            // 
            this.PN_Modules.AutoScroll = true;
            this.PN_Modules.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(67)))), ((int)(((byte)(91)))));
            this.PN_Modules.Controls.Add(this.PN_IntegrationData);
            this.PN_Modules.Controls.Add(this.BTN_IntegrationData);
            this.PN_Modules.Controls.Add(this.PN_UserData);
            this.PN_Modules.Controls.Add(this.BTN_UserData);
            this.PN_Modules.Controls.Add(this.PN_AppInfo);
            this.PN_Modules.Dock = System.Windows.Forms.DockStyle.Left;
            this.PN_Modules.Location = new System.Drawing.Point(0, 0);
            this.PN_Modules.Name = "PN_Modules";
            this.PN_Modules.Size = new System.Drawing.Size(200, 740);
            this.PN_Modules.TabIndex = 127;
            // 
            // PN_IntegrationData
            // 
            this.PN_IntegrationData.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(32)))), ((int)(((byte)(39)))));
            this.PN_IntegrationData.Controls.Add(this.BTN_ExportX3);
            this.PN_IntegrationData.Controls.Add(this.BTN_ImportX3);
            this.PN_IntegrationData.Dock = System.Windows.Forms.DockStyle.Top;
            this.PN_IntegrationData.Location = new System.Drawing.Point(0, 280);
            this.PN_IntegrationData.Name = "PN_IntegrationData";
            this.PN_IntegrationData.Size = new System.Drawing.Size(200, 80);
            this.PN_IntegrationData.TabIndex = 19;
            // 
            // BTN_ExportX3
            // 
            this.BTN_ExportX3.BackColor = System.Drawing.Color.SteelBlue;
            this.BTN_ExportX3.Dock = System.Windows.Forms.DockStyle.Top;
            this.BTN_ExportX3.FlatAppearance.BorderSize = 0;
            this.BTN_ExportX3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTN_ExportX3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_ExportX3.ForeColor = System.Drawing.Color.LightGray;
            this.BTN_ExportX3.Location = new System.Drawing.Point(0, 40);
            this.BTN_ExportX3.Name = "BTN_ExportX3";
            this.BTN_ExportX3.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.BTN_ExportX3.Size = new System.Drawing.Size(200, 40);
            this.BTN_ExportX3.TabIndex = 18;
            this.BTN_ExportX3.Text = "Exportações";
            this.BTN_ExportX3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BTN_ExportX3.UseVisualStyleBackColor = false;
            this.BTN_ExportX3.Click += new System.EventHandler(this.BTN_ExportX3_Click);
            // 
            // BTN_ImportX3
            // 
            this.BTN_ImportX3.BackColor = System.Drawing.Color.SteelBlue;
            this.BTN_ImportX3.Dock = System.Windows.Forms.DockStyle.Top;
            this.BTN_ImportX3.FlatAppearance.BorderSize = 0;
            this.BTN_ImportX3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTN_ImportX3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_ImportX3.ForeColor = System.Drawing.Color.LightGray;
            this.BTN_ImportX3.Location = new System.Drawing.Point(0, 0);
            this.BTN_ImportX3.Name = "BTN_ImportX3";
            this.BTN_ImportX3.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.BTN_ImportX3.Size = new System.Drawing.Size(200, 40);
            this.BTN_ImportX3.TabIndex = 17;
            this.BTN_ImportX3.Text = "Importações";
            this.BTN_ImportX3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BTN_ImportX3.UseVisualStyleBackColor = false;
            this.BTN_ImportX3.Click += new System.EventHandler(this.BTN_ImportX3_Click);
            // 
            // BTN_IntegrationData
            // 
            this.BTN_IntegrationData.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(67)))), ((int)(((byte)(91)))));
            this.BTN_IntegrationData.Dock = System.Windows.Forms.DockStyle.Top;
            this.BTN_IntegrationData.FlatAppearance.BorderSize = 0;
            this.BTN_IntegrationData.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTN_IntegrationData.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_IntegrationData.ForeColor = System.Drawing.Color.Gainsboro;
            this.BTN_IntegrationData.Location = new System.Drawing.Point(0, 220);
            this.BTN_IntegrationData.Name = "BTN_IntegrationData";
            this.BTN_IntegrationData.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.BTN_IntegrationData.Size = new System.Drawing.Size(200, 60);
            this.BTN_IntegrationData.TabIndex = 18;
            this.BTN_IntegrationData.Text = "Gestão Integrações";
            this.BTN_IntegrationData.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BTN_IntegrationData.UseVisualStyleBackColor = false;
            this.BTN_IntegrationData.Click += new System.EventHandler(this.BTN_IntegrationData_Click);
            // 
            // PN_UserData
            // 
            this.PN_UserData.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(32)))), ((int)(((byte)(39)))));
            this.PN_UserData.Controls.Add(this.BTN_SessionUsers);
            this.PN_UserData.Controls.Add(this.BTN_Users);
            this.PN_UserData.Dock = System.Windows.Forms.DockStyle.Top;
            this.PN_UserData.Location = new System.Drawing.Point(0, 140);
            this.PN_UserData.Name = "PN_UserData";
            this.PN_UserData.Size = new System.Drawing.Size(200, 80);
            this.PN_UserData.TabIndex = 17;
            // 
            // BTN_SessionUsers
            // 
            this.BTN_SessionUsers.BackColor = System.Drawing.Color.SteelBlue;
            this.BTN_SessionUsers.Dock = System.Windows.Forms.DockStyle.Top;
            this.BTN_SessionUsers.FlatAppearance.BorderSize = 0;
            this.BTN_SessionUsers.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTN_SessionUsers.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_SessionUsers.ForeColor = System.Drawing.Color.LightGray;
            this.BTN_SessionUsers.Location = new System.Drawing.Point(0, 40);
            this.BTN_SessionUsers.Name = "BTN_SessionUsers";
            this.BTN_SessionUsers.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.BTN_SessionUsers.Size = new System.Drawing.Size(200, 40);
            this.BTN_SessionUsers.TabIndex = 18;
            this.BTN_SessionUsers.Text = "Sessões";
            this.BTN_SessionUsers.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BTN_SessionUsers.UseVisualStyleBackColor = false;
            this.BTN_SessionUsers.Click += new System.EventHandler(this.BTN_SessionUsers_Click);
            // 
            // BTN_Users
            // 
            this.BTN_Users.BackColor = System.Drawing.Color.SteelBlue;
            this.BTN_Users.Dock = System.Windows.Forms.DockStyle.Top;
            this.BTN_Users.FlatAppearance.BorderSize = 0;
            this.BTN_Users.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTN_Users.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_Users.ForeColor = System.Drawing.Color.LightGray;
            this.BTN_Users.Location = new System.Drawing.Point(0, 0);
            this.BTN_Users.Name = "BTN_Users";
            this.BTN_Users.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.BTN_Users.Size = new System.Drawing.Size(200, 40);
            this.BTN_Users.TabIndex = 17;
            this.BTN_Users.Text = "Utilizadores";
            this.BTN_Users.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BTN_Users.UseVisualStyleBackColor = false;
            this.BTN_Users.Click += new System.EventHandler(this.BTN_Users_Click);
            // 
            // BTN_UserData
            // 
            this.BTN_UserData.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(67)))), ((int)(((byte)(91)))));
            this.BTN_UserData.Dock = System.Windows.Forms.DockStyle.Top;
            this.BTN_UserData.FlatAppearance.BorderSize = 0;
            this.BTN_UserData.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTN_UserData.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_UserData.ForeColor = System.Drawing.Color.Gainsboro;
            this.BTN_UserData.Location = new System.Drawing.Point(0, 80);
            this.BTN_UserData.Name = "BTN_UserData";
            this.BTN_UserData.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.BTN_UserData.Size = new System.Drawing.Size(200, 60);
            this.BTN_UserData.TabIndex = 16;
            this.BTN_UserData.Text = "Gestão Utilizadores";
            this.BTN_UserData.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BTN_UserData.UseVisualStyleBackColor = false;
            this.BTN_UserData.Click += new System.EventHandler(this.BTN_UserData_Click);
            // 
            // PN_AppInfo
            // 
            this.PN_AppInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(67)))), ((int)(((byte)(91)))));
            this.PN_AppInfo.Controls.Add(this.PN_Logo);
            this.PN_AppInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.PN_AppInfo.Location = new System.Drawing.Point(0, 0);
            this.PN_AppInfo.Name = "PN_AppInfo";
            this.PN_AppInfo.Size = new System.Drawing.Size(200, 80);
            this.PN_AppInfo.TabIndex = 15;
            // 
            // PN_Logo
            // 
            this.PN_Logo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PN_Logo.Dock = System.Windows.Forms.DockStyle.Left;
            this.PN_Logo.ErrorImage = null;
            this.PN_Logo.InitialImage = null;
            this.PN_Logo.Location = new System.Drawing.Point(0, 0);
            this.PN_Logo.Name = "PN_Logo";
            this.PN_Logo.Size = new System.Drawing.Size(88, 80);
            this.PN_Logo.TabIndex = 14;
            this.PN_Logo.TabStop = false;
            // 
            // Timer_Main
            // 
            this.Timer_Main.Enabled = true;
            this.Timer_Main.Interval = 60000;
            this.Timer_Main.Tick += new System.EventHandler(this.Timer_Main_Tick);
            // 
            // Timer_ExportSage
            // 
            this.Timer_ExportSage.Enabled = true;
            this.Timer_ExportSage.Interval = 3000;
            this.Timer_ExportSage.Tick += new System.EventHandler(this.Timer_ExportSage_Tick);
            // 
            // MainForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1340, 740);
            this.ControlBox = false;
            this.Controls.Add(this.PN_Main);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MinimumSize = new System.Drawing.Size(1340, 726);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "ResizeAll";
            this.Text = "Main";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.Load += new System.EventHandler(this.Main_Load);
            this.PN_Main.ResumeLayout(false);
            this.SS_Footer.ResumeLayout(false);
            this.SS_Footer.PerformLayout();
            this.PN_Header.ResumeLayout(false);
            this.PN_Modules.ResumeLayout(false);
            this.PN_IntegrationData.ResumeLayout(false);
            this.PN_UserData.ResumeLayout(false);
            this.PN_AppInfo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PN_Logo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Panel PN_Main;
        private System.Windows.Forms.Panel PN_Header;
        internal System.Windows.Forms.Label LBL_Welcome;
        private System.Windows.Forms.Panel PN_Modules;
        private System.Windows.Forms.Button BTN_SessionUsers;
        private System.Windows.Forms.Button BTN_Users;
        private System.Windows.Forms.Button BTN_UserData;
        internal System.Windows.Forms.Panel PN_AppInfo;
        internal System.Windows.Forms.PictureBox PN_Logo;
        internal System.Windows.Forms.StatusStrip SS_Footer;
        internal System.Windows.Forms.ToolStripStatusLabel TS_User;
        internal System.Windows.Forms.ToolStripStatusLabel TS_Computer;
        internal System.Windows.Forms.ToolStripStatusLabel TS_OnlineTime;
        internal System.Windows.Forms.ToolStripStatusLabel TS_Version;
        internal System.Windows.Forms.Timer Timer_Main;
        private System.Windows.Forms.Button BTN_IntegrationData;
        private System.Windows.Forms.Button BTN_ExportX3;
        private System.Windows.Forms.Button BTN_ImportX3;
        internal System.Windows.Forms.Panel PN_Screens;
        internal System.Windows.Forms.Panel PN_UserData;
        internal System.Windows.Forms.Panel PN_IntegrationData;
        internal System.Windows.Forms.Timer Timer_ExportSage;
        internal System.Windows.Forms.Button BTN_StatusExportSage;
        internal System.Windows.Forms.Button BTN_StatusImportSage;
    }
}