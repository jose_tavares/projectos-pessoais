﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Threading.Tasks;
using MODELS.BusinessObject.ERTSOFT;
using MODELS.Helpers;
using ERTSERVICE.Forms;
using DATA.Classes;
using DATA.Services.Concrete.Interfaces;
using ERTSERVICE.Ninject;

namespace ERTSERVICE.Helpers
{
    public static class Module
    {
        #region Private Variables
        private static readonly IUserService userService = NinjectProgram.GetInstance<IUserService>();
        private static USER _User;
        #endregion

        #region Public Properties
        public static LoadingForm LoadingForm { get; set; }

        public static USER User
        {
            get
            {
                return _User;
            }
            set
            {
                _User = value;
            }
        }

        public static int ComputerWidth
        {
            get
            {
                return Screen.PrimaryScreen.WorkingArea.Width;
            }
        }

        public static int ComputerHeight
        {
            get
            {
                return Screen.PrimaryScreen.WorkingArea.Height;
            }
        }

        public static Version AppVersion
        {
            get
            {
                if (System.Deployment.Application.ApplicationDeployment.IsNetworkDeployed)
                    return System.Deployment.Application.ApplicationDeployment.CurrentDeployment.CurrentVersion;
                else
                    return Assembly.GetExecutingAssembly().GetName().Version;
            }
        }
        #endregion

        #region Public Methods
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        static extern bool EnumThreadWindows(int dwThreadId, EnumWindowProc lpEnumFunc, IntPtr lParam);
        delegate void EnumWindowProc(IntPtr hWnd, IntPtr parameter);

        public static IEnumerable<Form> GetOpenForms()
        {
            foreach (ProcessThread thread in Process.GetCurrentProcess().Threads)
            {
                List<IntPtr> hWnds = new List<IntPtr>();
                EnumThreadWindows(thread.Id, (hWnd, param) => { hWnds.Add(hWnd); }, IntPtr.Zero);
                foreach (IntPtr hWnd in hWnds)
                {
                    if (Control.FromHandle(hWnd) is Form form)
                    {
                        yield return form;
                    }
                }
            }
        }

        public static bool IsDebugMode()
        {
            return Debugger.IsAttached;
        }

        public async static Task SetUser(Token Token)
        {
            User = await userService.GetByID(Token.ROWID);
        }

        public static void ClearUser()
        {
            User = null;
        }

        public static DialogResult QuestionMessage(string Message)
        {
            return MessageBox.Show(Message, "Questão", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        }

        public static void ErrorMessage(string Message)
        {
            MessageBox.Show(Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public static void WarningMessage(string Message)
        {
            MessageBox.Show(Message, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        public static void InformationMessage(string Message)
        {
            MessageBox.Show(Message, "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public static void ResultMessage(DataResult Result)
        {
            if (Result.Success)
                InformationMessage(Result.ToString());
            else
                ErrorMessage(Result.ToString());
        }

        public static void OpenLoading()
        {
            if (LoadingForm == null)
            {
                System.Threading.Thread t = new System.Threading.Thread(OpenLoadingForm);
                t.Start();
            }
        }

        private static void OpenLoadingForm()
        {
            LoadingForm Loading = new LoadingForm();
            LoadingForm = Loading;
            Loading.ShowDialog();
            //Application.Run(Loading);
        }

        public static void UpdateProgressBar(int Value, int Maximum = 100)
        {
            if (LoadingForm != null)
            {
                LoadingForm.UpdateProgressBar(Value, Maximum);
            }
        }

        public static void CloseLoading()
        {
            LoadingForm.Hide();
            LoadingForm = null;
        }
        #endregion

        #region Extensions
        public static void ResizeFont(this Control Control, decimal WidthRatio, float FontSize = 0)
        {
            if (FontSize > 0)
                Control.Font = new Font(Control.Font.FontFamily, (float)Math.Floor(Convert.ToDecimal((decimal)FontSize * WidthRatio)), Control.Font.Style);
            else
                Control.Font = new Font(Control.Font.FontFamily, (float)Math.Floor(Convert.ToDecimal((decimal)Control.Font.Size * WidthRatio)), Control.Font.Style);
        }

        public static void DefaultFont(this Control Control, decimal WidthRatio)
        {
            Control.Font = new Font("Microsoft Sans Serif", (float)Math.Floor(Convert.ToDecimal((decimal)8.25 * WidthRatio)), FontStyle.Regular);
        }

        public static void ResizeFont(this ToolStripItem TSI, decimal WidthRatio, float FontSize = 0)
        {
            if (FontSize > 0)
                TSI.Font = new Font(TSI.Font.FontFamily, (float)Math.Floor(Convert.ToDecimal((decimal)FontSize * WidthRatio)), TSI.Font.Style);
            else
                TSI.Font = new Font(TSI.Font.FontFamily, (float)Math.Floor(Convert.ToDecimal((decimal)TSI.Font.Size * WidthRatio)), TSI.Font.Style);
        }

        public static void Resize(this Control Control, decimal WidthRatio, decimal HeightRatio)
        {
            Control.Width = (int)(Control.Width * WidthRatio);
            Control.Height = (int)(Control.Height * HeightRatio);
            Control.Location = new Point((int)(Control.Location.X * WidthRatio), (int)(Control.Location.Y * HeightRatio));
        }

        public static bool IsLengthValid(this string Text, int Min, int Max = int.MaxValue)
        {
            if (Text.Length >= Min && Text.Length <= Max)
                return true;

            return false;
        }
        #endregion
    }
}
