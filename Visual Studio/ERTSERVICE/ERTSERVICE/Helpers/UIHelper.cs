﻿using System.Windows.Forms;
using MODELS.Helpers;

namespace ERTSERVICE.Helpers
{
    public static class UIHelper
    {
        public static void DefineColumnSize(this DataGridView DGV)
        {
            int Sum = 0;

            foreach (DataGridViewColumn column in DGV.Columns)
            {
                if (column.Visible)
                {
                    column.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;

                    if (column.ValueType == typeof(int) | column.ValueType == typeof(decimal) || column.ValueType == typeof(double) || column.ValueType == typeof(float))
                        column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    else
                        column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

                    column.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

                    Sum += column.Width;
                }
            }

            if (DGV.Columns.Count > 0 && DGV.Width - Sum > 0)
                DGV.Columns[DGV.Columns.GetLastColumn(DataGridViewElementStates.Visible, DataGridViewElementStates.None).Index].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }

        public static void SelectRowByID(this DataGridView DGV, decimal id)
        {
            DGV.ClearSelection();

            foreach (DataGridViewRow row in DGV.Rows)
            {
                if ((decimal)row.Cells[nameof(DBRules.ROWID)].Value == id)
                {
                    row.Selected = true;
                    break;
                }
            }
        }

        public static void CLB_CheckItems(this CheckedListBox CLB, bool Check)
        {
            for (int i = 0; i <= CLB.Items.Count - 1; i++)
                CLB.SetItemChecked(i, Check);
        }

        public static void ClearControls(this Control ParentControl)
        {
            if (ParentControl is NumericUpDown NUD)
            {
                if (NUD.Maximum >= 0)
                    NUD.Value = 0;
                else
                    NUD.Value = NUD.Minimum;
            }
            else if (ParentControl is ComboBox Combobox)
            {
                if (Combobox.DropDownStyle == ComboBoxStyle.DropDownList)
                {
                    if (Combobox.Items.Count > 0)
                        Combobox.SelectedIndex = 0;
                }
                else
                    Combobox.Text = string.Empty;
            }
            else if (ParentControl is RadioButton RB)
            {
                RB.Checked = false;
            }
            else if (ParentControl is CheckBox CB)
            {
                CB.Checked = false;
            }
            else if (ParentControl is CheckedListBox CLB)
            {
                CLB.ClearSelected();

                CLB_CheckItems(CLB, false);
            }
            else if (ParentControl is DateTimePicker DTP)
            {
                DTP.Value = DTP.MinDate;
            }
            else if (ParentControl is ProgressBar ProgressBar)
            {
                ProgressBar.Value = ProgressBar.Minimum;
            }
            else if (!(ParentControl is Button))
            {
                if (!(ParentControl is Label))
                {
                    if (!(ParentControl is GroupBox))
                    {
                        if (!(ParentControl is TabControl))
                        {
                            if (!(ParentControl is TabPage))
                            {
                                ParentControl.ResetText();
                            }
                        }
                    }
                }
            }

            if (ParentControl.HasChildren)
            {
                foreach (Control controlo in ParentControl.Controls)
                    ClearControls(controlo);
            }
        }
    }
}
