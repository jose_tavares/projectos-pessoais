﻿namespace ERTSERVICE
{
    using System;
    using DATA.Helpers;
    using ERTSERVICE.Ninject;
    using System.Windows.Forms;
    using ERTSERVICE.Controllers.Interfaces;

    internal static class Program
    {
        /// <summary>
        /// Ponto de entrada principal para o aplicativo.
        /// </summary>
        [STAThread]
        static void Main()
        {
            NinjectProgram.Load(new ApplicationModule());
            ApiHelper.InitializeClient();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(NinjectProgram.GetInstance<ILoginController>().LoginForm);
        }
    }
}
