﻿using Ninject.Modules;

namespace ERTSERVICE.Ninject
{
    using DATA.Services.Concrete.Interfaces;
    using ERTSERVICE.Controllers.Interfaces;
    using DATA.Services.Concrete.Implementations;
    using ERTSERVICE.Controllers.Implementations;

    public class ApplicationModule : NinjectModule
    {
        public override void Load()
        {
            #region Services
            Bind<IUserService>().To<UserService>();
            Bind<IStojouService>().To<StojouService>();
            Bind<IExportWSService>().To<ExportWSService>();
            Bind<IWebServiceService>().To<WebServiceService>();
            #endregion

            #region Controllers
            Bind<IMainController>().To<MainController>();
            Bind<ILoginController>().To<LoginController>();
            Bind<IUsersController>().To<UsersController>();
            #endregion
        }
    }
}