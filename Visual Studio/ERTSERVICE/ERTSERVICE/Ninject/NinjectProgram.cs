﻿using Ninject;
using Ninject.Modules;
using Ninject.Parameters;

namespace ERTSERVICE.Ninject
{
    public class NinjectProgram
    {
        private static IKernel _ninjectKernel;

        public static void Load(INinjectModule module)
        {
            _ninjectKernel = new StandardKernel(module);
        }

        public static T GetInstance<T>()
        {
            return _ninjectKernel.Get<T>();
        }

        public static T GetInstance<T>(params IParameter[] parameters)
        {
            return _ninjectKernel.Get<T>(parameters);
        }
    }
}
