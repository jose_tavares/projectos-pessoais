﻿namespace ERTSERVICE.Controllers.Interfaces
{
    using ERTSERVICE.Forms;

    public interface ILoginController
    {
        LoginForm LoginForm { get; }

        void Login();

        void Logout();
    }
}
