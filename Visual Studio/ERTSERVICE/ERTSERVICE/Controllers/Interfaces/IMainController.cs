﻿using MODELS.Helpers;
using System.Windows.Forms;
using System.Threading.Tasks;

namespace ERTSERVICE.Controllers.Interfaces
{
    public interface IMainController
    {
        bool Dragging { set; }

        #region Methods
        void ShowDialog();

        void LoadFormProperties();

        void LoadToolStripsText();

        Task CreateTimers();

        void ShowSubMenu(Panel SubMenu);

        void HideSubMenus();

        void OpenFormScreen(Form form);

        Task<DataResult> ImportSage(object sender);

        Task ExportSage();

        void WelcomeMouseDown();

        void WelcomeMouseMove();

        void StatusServiceImport();

        void StatusServiceExport();

        void CloseForm();
        #endregion
    }
}
