﻿namespace ERTSERVICE.Controllers.Interfaces
{
    using ERTSERVICE.Forms;
    using System.Threading.Tasks;
    using MODELS.BusinessObject.ERTSOFT;
    using MODELS.Helpers;

    public interface IUsersController
    {
        bool SelectionChanging { set; }

        UsersForm UsersForm();

        Task LoadUsersTable(decimal selectedID = -1);

        void LoadHeaderInfo(USER user);

        void LoadFirstTab(USER user);

        Task<USER> GetSelectedUser();

        void LoadTitle();

        void SetEnabled();

        void ResetTabPagesTag();

        void LoadTabPage();

        void NoItemSelected();

        void ChangingData();

        void ClearControls();

        Task<DataResult> Update(USER user);

        Task<DataResult> Insert(USER user);
    }
}
