﻿namespace ERTSERVICE.Controllers.Implementations
{
    using System;
    using System.Net;
    using MODELS.Helpers;
    using System.Drawing;
    using ERTSERVICE.Forms;
    using ERTSERVICE.Helpers;
    using System.Windows.Forms;
    using System.Threading.Tasks;
    using System.Collections.Generic;
    using MODELS.BusinessObject.ERTSOFT;
    using DATA.Services.Concrete.Interfaces;
    using ERTSERVICE.Controllers.Interfaces;

    public class MainController : IMainController
    {
        #region Private Variables
        private readonly MainForm mainForm;
        private readonly IStojouService stojouService;
        private readonly IExportWSService exportService;
        private readonly IWebServiceService webServiceService;
        
        private Point dragFormPoint;
        private Point dragCursorPoint;
        private bool stopImportService = true;
        private bool stopExportService = true;
        private readonly DateTime SessionDate = DateTime.Now;
        private static readonly List<Timer> timerList = new List<Timer>();
        #endregion

        public bool Dragging { private get; set; } = false;

        public MainController(IStojouService stojouService, IWebServiceService webServiceService, IExportWSService exportService, IUsersController usersController)
        {
            this.stojouService = stojouService;
            this.exportService = exportService;
            this.webServiceService = webServiceService;
            mainForm = new MainForm(this, usersController);
        }

        #region Public Methods
        public void LoadFormProperties()
        {
            //Uncomment if form should be full screen.
            //Left = 0;
            //Top = 0;
            //AutoScaleMode = AutoScaleMode.None;
            //Width = Screen.PrimaryScreen.WorkingArea.Width;
            //Height = Screen.PrimaryScreen.WorkingArea.Height;
        }

        public void LoadToolStripsText()
        {
            TimeSpan DateDiff = DateTime.Now - SessionDate;
            mainForm.TS_OnlineTime.Text = "Online: " + DateDiff.Days + "D " + DateDiff.Hours + "H " + DateDiff.Minutes + "M";
            mainForm.SS_Footer.ForeColor = Color.White;
            mainForm.TS_User.Text = Module.User?.USERNAM_0 + " (Logout)";
            mainForm.TS_Computer.Text = Dns.GetHostName();
            //TS_Version.Text = "App Version: " + Module.AppVersion;
        }

        public async Task CreateTimers()
        {
            IEnumerable<WEBSERVICE> webServiceList = await webServiceService.GetByImportFlg(2);

            foreach (WEBSERVICE webServiceItem in webServiceList)
            {
                Timer importSageTimer = new Timer()
                {
                    Tag = webServiceItem.CODE_0,
                    Enabled = true,
                    Interval = 1000
                };

                timerList.Add(importSageTimer);

                importSageTimer.Tick += new EventHandler(mainForm.Timer_ImportSage_Tick);
            }
        }

        public void DisposeTimers()
        {
            foreach (Timer timerItem in timerList)
            {
                timerItem.Tick -= mainForm.Timer_ImportSage_Tick;
                timerItem.Dispose();
            }

            timerList.Clear();
        }

        public void ShowSubMenu(Panel subMenu)
        {
            if (!subMenu.Visible)
            {
                HideSubMenus();
                subMenu.Visible = true;
            }
            else
            {
                subMenu.Visible = false;
            }
        }

        public void OpenFormScreen(Form form)
        {
            //Module.OpenLoading();
            SetScreenAttributes(form, mainForm.PN_Screens);
            mainForm.PN_Screens.Controls.Clear();
            mainForm.PN_Screens.Controls.Add(form);
            form.Show();
            //Module.CloseLoading();
        }

        public void HideSubMenus()
        {
            mainForm.PN_UserData.Visible = false;
            mainForm.PN_IntegrationData.Visible = false;
        }

        public async Task<DataResult> ImportSage(object sender)
        {
            DataResult result = new DataResult();

            if (sender is Timer timer)
            {
                try
                {
                    timer.Enabled = false;

                    if (!stopImportService)
                    {
                        result = await stojouService.ImportFirstPendingByWebService(timer.Tag.ToString());
                    }
                    else
                    {
                        result.AddInvalidMessage("Serviço importação parado.");
                    }
                }
                catch (Exception)
                {

                }
                finally
                {
                    timer.Enabled = true;
                }
            }
            else
            {
                result.AddInvalidMessage("O objeto associado ao evento não é um timer.");
            }

            return result;
        }

        public async Task ExportSage()
        {
            try
            {
                mainForm.Timer_ExportSage.Enabled = false;

                if (!stopExportService)
                {
                    await exportService.ExportPending();
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                mainForm.Timer_ExportSage.Enabled = true;
            }
        }

        public void WelcomeMouseDown()
        {
            Dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = mainForm.Location;
        }

        public void WelcomeMouseMove()
        {
            if (Dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                mainForm.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        public void StatusServiceImport()
        {
            stopImportService = !stopImportService;

            UpdateStatusImportText();
        }

        public void StatusServiceExport()
        {
            stopExportService = !stopExportService;

            UpdateStatusExportText();
        }

        public void ShowDialog()
        {
            mainForm.ShowDialog();
        }

        public void CloseForm()
        {
            stopImportService = true;
            stopExportService = true;
            UpdateStatusImportText();
            UpdateStatusExportText();
            DisposeTimers();
        }
        #endregion

        private void SetScreenAttributes(Form form, Panel pn_Screens)
        {
            form.Tag = form.Name;
            form.TopLevel = false;
            form.Dock = DockStyle.Fill;
            form.BackColor = pn_Screens.BackColor;
            form.IsMdiContainer = false;
            form.AutoScaleMode = AutoScaleMode.None;
        }

        private void UpdateStatusImportText()
        {
            if (!stopImportService)
            {
                mainForm.BTN_StatusImportSage.Text = "Status importação: Ativo";
            }
            else
            {
                mainForm.BTN_StatusImportSage.Text = "Status importação: Inativo";
            }
        }

        private void UpdateStatusExportText()
        {
            if (!stopExportService)
            {
                mainForm.BTN_StatusExportSage.Text = "Status exportação: Ativo";
            }
            else
            {
                mainForm.BTN_StatusExportSage.Text = "Status exportação: Inativo";
            }
        }

    }
}
