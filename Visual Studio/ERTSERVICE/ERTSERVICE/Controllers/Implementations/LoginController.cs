﻿namespace ERTSERVICE.Controllers.Implementations
{
    using DATA.Classes;
    using DATA.Helpers;
    using MODELS.Helpers;
    using ERTSERVICE.Forms;
    using ERTSERVICE.Helpers;
    using DATA.Services.Concrete.Interfaces;
    using ERTSERVICE.Controllers.Interfaces;

    public class LoginController : ILoginController
    {
        private readonly LoginForm loginForm;
        private readonly IUserService userService;
        private readonly IMainController mainController;

        public LoginForm LoginForm => loginForm;

        public LoginController(IUserService userService, IMainController mainController)
        {
            this.userService = userService;
            this.mainController = mainController;

            loginForm = new LoginForm(this);
        }

        public void Login()
        {
            Token token = AsyncHelper.RunSync(() => userService.Login(loginForm.TXT_Username.Text, loginForm.TXT_Password.Text));
            ApiHelper.AddToken(token);
            AsyncHelper.RunSync(() => Module.SetUser(token));

            loginForm.Hide();
            UIHelper.ClearControls(loginForm.PN_UserInformation);

            mainController.ShowDialog();
        }

        public void Logout()
        {
            loginForm.Show();
            AsyncHelper.RunSync(() => userService.Logout());
            Module.ClearUser();
        }
    }
}
