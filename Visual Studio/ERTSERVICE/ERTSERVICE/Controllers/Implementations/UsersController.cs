﻿namespace ERTSERVICE.Controllers.Implementations
{
    using System;
    using ERTSERVICE.Forms;
    using MODELS.BusinessObject.ERTSOFT;
    using ERTSERVICE.Controllers.Interfaces;
    using DATA.Services.Concrete.Interfaces;
    using System.Windows.Forms;
    using ERTSERVICE.Helpers;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using System.Linq;
    using MODELS.Helpers;

    public class UsersController : IUsersController
    {
        private UsersForm usersForm;
        private readonly IUserService userService;

        #region Public Properties
        public bool SelectionChanging { private get; set; }
        #endregion

        public UsersController(IUserService userService)
        {
            this.userService = userService;
        }

        #region Public Methods
        #region LoadInformation
        public async Task LoadUsersTable(decimal selectedID = -1)
        {
            IEnumerable<USER> users = await userService.GetAll();

            usersForm.DGV_Users.Enabled = true;
            usersForm.DGV_Users.DataSource = users;
            CostumizeUsersTable();
            usersForm.LBL_Users.Text = "Utilizadores: " + users.Count();

            if (selectedID > -1)
            {
                UIHelper.SelectRowByID(usersForm.DGV_Users, selectedID);
            }
        }

        public void LoadHeaderInfo(USER user)
        {
            usersForm.TXT_ID.Text = user.ROWID.ToString();
            usersForm.CB_IsLogged.Checked = Convert.ToBoolean(user.LOGGEDFLG_0 - 1);
        }

        public void LoadFirstTab(USER user)
        {
            usersForm.TXT_Username.Text = user.USERNAM_0;
            //TXT_Password.Text = User.GetDefaultPassword();
            usersForm.TXT_RepeatPassword.Text = usersForm.TXT_Password.Text;
            usersForm.CB_IsActive.Checked = Convert.ToBoolean(user.ENABFLG_0 - 1);
        }
        #endregion

        #region GetInformation
        public async Task<USER> GetSelectedUser()
        {
            if (usersForm.DGV_Users.SelectedRows.Count == 1)
            {
                decimal id = (decimal)usersForm.DGV_Users.SelectedRows[0].Cells["ROWID"].Value;

                return await userService.GetByID(id);
            }

            return null;
        }
        #endregion

        public void LoadTitle()
        {
            usersForm.LBL_MainTitle.Text = usersForm.Text;
            usersForm.LBL_SecondaryTitle.Text = usersForm.Text;
        }

        public void SetEnabled()
        {
            usersForm.BTN_AbandonScreen.Enabled = true;
            usersForm.BTN_New.Enabled = true;
            usersForm.BTN_Update.Enabled = false;
            usersForm.BTN_Create.Enabled = false;
            usersForm.BTN_Supress.Enabled = false;
            usersForm.BTN_Abandon.Enabled = false;
        }

        public void ResetTabPagesTag()
        {
            foreach (TabPage TP in usersForm.TC_DetailedData.TabPages)
            {
                TP.Tag = "0";
            }
        }

        public void LoadTabPage()
        {
            usersForm.TC_DetailedData_SelectedIndexChanged(usersForm.TC_DetailedData, EventArgs.Empty);
        }

        public void NoItemSelected()
        {
            usersForm.BTN_Supress.Enabled = false;

            ClearControls();
        }

        public void ChangingData()
        {
            if (!SelectionChanging && usersForm.DGV_Users.SelectedRows.Count == 1 && !usersForm.BTN_Create.Enabled)
            {
                usersForm.DGV_Users.Enabled = false;
                usersForm.BTN_New.Enabled = false;
                usersForm.BTN_Create.Enabled = false;
                usersForm.BTN_Supress.Enabled = false;
                usersForm.BTN_Update.Enabled = true;
                usersForm.BTN_Abandon.Enabled = true;
            }
        }

        public void ClearControls()
        {
            usersForm.PN_HeaderData.ClearControls();
            usersForm.PN_DetailedData.ClearControls();
        }

        public async Task<DataResult> Update(USER user)
        {
            DataResult result = userService.IsValidUpdate(user);

            if (result.Success)
            {
                return await userService.Update(user);
            }

            return result;
        }

        public async Task<DataResult> Insert(USER user)
        {
            DataResult result = userService.IsValidInsert(user);

            if (result.Success)
            {
                return await userService.Insert(user);
            }

            return result;
        }

        public UsersForm UsersForm()
        {
            usersForm = new UsersForm(this);

            return usersForm;
        }
        #endregion

        #region Private Methods
        private void CostumizeUsersTable()
        {
            if (usersForm.DGV_Users.Columns.Count > 0)
            {
                //Initializing list with all column names that must be visible
                List<string> VisibleColumns = new List<string>
                {
                    nameof(USER.USERNAM_0), nameof(USER.PASSWORD_0)
                };

                foreach (DataGridViewColumn dataGridViewColumn in usersForm.DGV_Users.Columns)
                {
                    if (VisibleColumns.Contains(dataGridViewColumn.DataPropertyName))
                    {
                        dataGridViewColumn.Visible = true;
                    }
                    else
                    {
                        dataGridViewColumn.Visible = false;
                    }
                }

                UIHelper.DefineColumnSize(usersForm.DGV_Users);
            }
        }
        #endregion
    }
}
