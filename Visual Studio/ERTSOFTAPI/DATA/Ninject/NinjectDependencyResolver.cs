﻿using Ninject;
using System.Web.Http.Dependencies;

namespace DATA.Ninject
{
    public class NinjectDependencyResolver : NinjectDependencyScope, IDependencyResolver
    {
        private readonly IKernel kernel;

        public NinjectDependencyResolver(IKernel kernel) : base(kernel) => this.kernel = kernel;

        public IDependencyScope BeginScope() => new NinjectDependencyScope(kernel.BeginBlock());
    }
}
