using System;
using Ninject;
using System.Web;
using DATA.Ninject;
using System.Web.Http;
using Ninject.Parameters;
using Ninject.Web.Common;
using Ninject.Web.Common.WebHost;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using DATA.LayeredArquiteture.BusinessLogic.Implementation;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;
using DATA.LayeredArquiteture.Repositories.Interfaces.Concrete;
using DATA.LayeredArquiteture.Repositories.Implementation.Concrete;
using DATA.LayeredArquiteture.Repositories.UnitOfWork;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethod(typeof(NinjectWebCommon), "Stop")]

namespace DATA.Ninject
{
    public static class NinjectWebCommon
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application.
        /// </summary>
        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }

        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }

        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            StandardKernel kernel = new StandardKernel();

            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();
                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            #region BLL
            kernel.Bind<IAppBLL>().To<AppBLL>();
            kernel.Bind<IUserBLL>().To<UserBLL>();
            kernel.Bind<IStockBLL>().To<StockBLL>();
            kernel.Bind<IModuleBLL>().To<ModuleBLL>();
            kernel.Bind<IStojouBLL>().To<StojouBLL>();
            kernel.Bind<IProfileBLL>().To<ProfileBLL>();
            kernel.Bind<IWsFieldConfigBLL>().To<WsFieldConfigBLL>();
            kernel.Bind<IStojouLogBLL>().To<StojouLogBLL>();
            kernel.Bind<IWebServiceBLL>().To<WebServiceBLL>();
            kernel.Bind<IExportWSBLL>().To<ExportWSBLL>();
            kernel.Bind<IExportWSLogBLL>().To<ExportWSLogBLL>();
            kernel.Bind<IImportWSBLL>().To<ImportWSBLL>();
            kernel.Bind<IImportWSDataBLL>().To<ImportWSDataBLL>();
            kernel.Bind<IImportWSLogBLL>().To<ImportWSLogBLL>();
            kernel.Bind<IItmMasterBLL>().To<ItmMasterBLL>();
            kernel.Bind<IMfgHeadBLL>().To<MfgHeadBLL>();
            kernel.Bind<IMfgItmBLL>().To<MfgItmBLL>();
            kernel.Bind<IMfgMatBLL>().To<MfgMatBLL>();
            kernel.Bind<IStolotBLL>().To<StolotBLL>();
            kernel.Bind<IFacilityBLL>().To<FacilityBLL>();
            kernel.Bind<IConfigValueBLL>().To<ConfigValueBLL>();
            kernel.Bind<IItmLockBLL>().To<ItmLockBLL>();
            kernel.Bind<IAtabdivBLL>().To<AtabdivBLL>();
            kernel.Bind<IAtextraBLL>().To<AtextraBLL>();
            kernel.Bind<IStolocBLL>().To<StolocBLL>();
            kernel.Bind<ITabstastoBLL>().To<TabstastoBLL>();
            kernel.Bind<ITabUnitBLL>().To<TabUnitBLL>();
            kernel.Bind<IItmFacilitBLL>().To<ItmFacilitBLL>();
            kernel.Bind<IStoDefectBLL>().To<StoDefectBLL>();
            kernel.Bind<IDocAllBLL>().To<DocAllBLL>();
            kernel.Bind<IApiRequestBLL>().To<ApiRequestBLL>();
            kernel.Bind<IMfgItmTrkBLL>().To<MfgItmTrkBLL>();
            kernel.Bind<IMfgMatTrkBLL>().To<MfgMatTrkBLL>();
            kernel.Bind<IProductionBLL>().To<ProductionBLL>();
            kernel.Bind<IConsumptionBLL>().To<ConsumptionBLL>();
            kernel.Bind<IMfgnumZmultiBLL>().To<MfgnumZmultiBLL>();
            #endregion

            #region Repositories
            kernel.Bind<IUnitOfWork>().To<UnitOfWork>();
            kernel.Bind<IAppRepository>().To<AppRepository>();
            kernel.Bind<IUserRepository>().To<UserRepository>();
            kernel.Bind<IStockRepository>().To<StockRepository>();
            kernel.Bind<IModuleRepository>().To<ModuleRepository>();
            kernel.Bind<IStojouRepository>().To<StojouRepository>();
            kernel.Bind<IProfileRepository>().To<ProfileRepository>();
            kernel.Bind<IWsFieldConfigRepository>().To<WsFieldConfigRepository>();
            kernel.Bind<IMfgItmTrkRepository>().To<MfgItmTrkRepository>();
            kernel.Bind<IMfgMatTrkRepository>().To<MfgMatTrkRepository>();
            kernel.Bind<IStojouLogRepository>().To<StojouLogRepository>();
            kernel.Bind<IWebServiceRepository>().To<WebServiceRepository>();
            kernel.Bind<IExportWSRepository>().To<ExportWSRepository>();
            kernel.Bind<IExportWSLogRepository>().To<ExportWSLogRepository>();
            kernel.Bind<IImportWSRepository>().To<ImportWSRepository>();
            kernel.Bind<IImportWSDataRepository>().To<ImportWSDataRepository>();
            kernel.Bind<IImportWSLogRepository>().To<ImportWSLogRepository>();
            kernel.Bind<IItmMasterRepository>().To<ItmMasterRepository>();
            kernel.Bind<IMfgHeadRepository>().To<MfgHeadRepository>();
            kernel.Bind<IMfgItmRepository>().To<MfgItmRepository>();
            kernel.Bind<IMfgMatRepository>().To<MfgMatRepository>();
            kernel.Bind<IStolotRepository>().To<StolotRepository>();
            kernel.Bind<IFacilityRepository>().To<FacilityRepository>();
            kernel.Bind<IConfigValueRepository>().To<ConfigValueRepository>();
            kernel.Bind<IItmLockRepository>().To<ItmLockRepository>();
            kernel.Bind<IAtabdivRepository>().To<AtabdivRepository>();
            kernel.Bind<IAtextraRepository>().To<AtextraRepository>();
            kernel.Bind<IStolocRepository>().To<StolocRepository>();
            kernel.Bind<ITabstastoRepository>().To<TabstastoRepository>();
            kernel.Bind<ITabUnitRepository>().To<TabUnitRepository>();
            kernel.Bind<IItmFacilitRepository>().To<ItmFacilitRepository>();
            kernel.Bind<IStoDefectRepository>().To<StoDefectRepository>();
            kernel.Bind<IDocAllRepository>().To<DocAllRepository>();
            kernel.Bind<IApiRequestRepository>().To<ApiRequestRepository>();
            kernel.Bind<IProductionRepository>().To<ProductionRepository>();
            kernel.Bind<IConsumptionRepository>().To<ConsumptionRepository>();
            kernel.Bind<IMfgnumZmultiRepository>().To<MfgnumZmultiRepository>();
            #endregion
        }

        public static void RegisterNinject(HttpConfiguration configuration)
        {
            // Set Web API Resolver
            configuration.DependencyResolver = new NinjectDependencyResolver(bootstrapper.Kernel);
        }

        /// <summary>
        /// Gets an instance of the specified service.
        /// </summary>
        /// <typeparam name="T">The service to resolve.</typeparam>
        /// <returns>An instance of the service.</returns>
        public static T GetInstance<T>()
        {
            return bootstrapper.Kernel.Get<T>();
        }

        /// <summary>
        /// Gets an instance of the specified service.
        /// </summary>
        /// <typeparam name="T">The service to resolve.</typeparam>
        /// <param name="parameters">The parameters to pass to the request.</param>
        /// <returns>An instance of the service.</returns>
        public static T GetInstance<T>(params IParameter[] parameters)
        {
            return bootstrapper.Kernel.Get<T>(parameters);
        }
    }
}