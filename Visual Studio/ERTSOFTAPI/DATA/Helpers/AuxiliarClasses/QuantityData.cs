﻿namespace DATA.Helpers.AuxiliarClasses
{
    public class QuantityData
    {
        public decimal QTYPCUORI_0 { get; set; }

        public decimal USENETQTY_0 { get; set; }

        public decimal USEGROSSQTY_0 { get; set; }

        public int DEFECT_0 { get; set; }

        public QuantityData()
        {

        }
    }
}
