﻿namespace DATA.Helpers.AuxiliarClasses
{
    public class ProductionData
    {
        public string HOSTNAM_0 { get; set; }

        public string MACHINE_0 { get; set; }

        public string OPERATION_0 { get; set; }

        public string MFGNUM_0 { get; set; }

        public decimal PRODQTY_0 { get; set; }

        public bool PRODOK_0 { get; set; }

        public string VCRNUMDES_0 { get; set; }
    }
}
