﻿using MODELS.BusinessObject.ERTSOFT.Database;

namespace DATA.Helpers.AuxiliarClasses
{
    public class ImportData
    {
        public WEBSERVICE WebService { get; set; }

        public dynamic Data { get; set; }    
    }
}
