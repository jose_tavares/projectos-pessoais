﻿using System.Data.SqlClient;

namespace DATA.Helpers.AuxiliarClasses
{
    public class ExportData
    {
        public SqlCommand InsertCommand { get; set; }

        public SqlCommand UpdateCommand { get; set; }

        public decimal LogId { get; set; }

        public string KeyValue { get; set; }

        public string DateValue { get; set; }

        public ExportData()
        {

        }

        public ExportData(SqlCommand insertCommand, SqlCommand updateCommand, decimal logId, string keyValue, string dateValue)
        {
            LogId = logId;
            InsertCommand = insertCommand;
            UpdateCommand = updateCommand;
            KeyValue = keyValue;
            DateValue = dateValue;
        }
    }
}
