﻿using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;
using MODELS.BusinessObject.SAGEX3.Database;

namespace DATA.Helpers.AuxiliarClasses
{
    public class StojouData
    {
        public ProductionData ProductionData { get; set; }

        public QuantityData QuantityData { get; set; }

        public WEBSERVICE WsConsumption { get; set; }

        public WEBSERVICE WsProduction { get; set; }

        public MFGHEAD MfgHead { get; set; }

        public MFGITM MfgItm { get; set; }

        public IEnumerable<MFGMAT> MfgMatList { get; set; }

        public IEnumerable<MFGMAT> MfgMatOperationList { get; set; }

        public ITMFACILIT ItmFacilit { get; set; }

        public FACILITY Facility { get; set; }

        public STOLOC Stoloc { get; set; }

        public TABSTASTO TabStaSto { get; set; }

        public ATEXTRA MacNam { get; set; }

        public ATABDIV OperationInfo { get; set; }

        public ATABDIV ConsumptionInfo { get; set; }

        public ATABDIV ProductionInfo { get; set; }

        public TABUNIT TabUnit { get; set; }

        public ITMMASTER ItmMaster { get; set; }
    }
}
