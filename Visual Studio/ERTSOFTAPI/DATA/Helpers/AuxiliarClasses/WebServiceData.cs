﻿using MODELS.BusinessObject.ERTSOFT.Database;

namespace DATA.Helpers.AuxiliarClasses
{
    public class WebServiceData
    {
        public WebServiceData()
        {

        }

        public WebServiceData(WEBSERVICE webService, string xml, string document, string outputws, string resultxml)
        {
            WebService = webService;
            XML = xml;
            Document = document;
            OUTPUTWS_0 = outputws;
            ResultXML = resultxml;
        }

        public WEBSERVICE WebService { get; set; }

        public string XML { get; set; }

        public string OUTPUTWS_0 { get; set; }

        public string ResultXML { get; set; }

        public string Document { get; set; }
    }
}
