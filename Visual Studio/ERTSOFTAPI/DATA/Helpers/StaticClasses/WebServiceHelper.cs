﻿using System;
using System.Net;
using System.Linq;
using MODELS.Helpers;
using DATA.WebServiceX3;
using System.Net.Security;
using DATA.Helpers.AuxiliarClasses;
using System.Threading.Tasks;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace DATA.Helpers.StaticClasses
{
    public static class WebServiceHelper
    {
        #region Public Methods
        public static string GetXML(STOJOU stojou, IEnumerable<WSFIELDCONFIG> wsFieldConfigList)
        {
            string XML = string.Empty;

            foreach (WSFIELDCONFIG wsField in wsFieldConfigList.OrderBy(w => w.POSITION_0))
            {
                if (wsField.FLDFLG_0 == 2 && wsField.VALUEFLG_0 == 2)
                {
                    XML += "<FLD NAME=" + wsField.FLD_0 + ">" + wsField.VALUE_0 + "</FLD> ";
                }
                else if (wsField.FLDFLG_0 == 2 && wsField.VALUEFLG_0 == 1)
                {
                    XML += "<FLD NAME=" + wsField.FLD_0 + ">" + stojou.GetPropValue(wsField.VALUE_0) + "</FLD> ";
                }
                else if (wsField.FLDFLG_0 == 1 && wsField.VALUEFLG_0 == 2)
                {
                    XML += wsField.VALUE_0;
                }
            }

            return XML;
        }

        public static string GetXMLByDynamic(dynamic data, IEnumerable<WSFIELDCONFIG> wsFieldConfigList)
        {
            string XML = string.Empty;

            foreach (WSFIELDCONFIG wsField in wsFieldConfigList.OrderBy(w => w.POSITION_0))
            {
                if (wsField.ROWID == 58)
                {
                    XML += "";
                }
                
                if (wsField.FLDFLG_0 == 2 && wsField.VALUEFLG_0 == 2)
                {
                    XML += "<FLD NAME=" + wsField.FLD_0 + ">" + wsField.VALUE_0 + "</FLD> ";
                }
                else if (wsField.FLDFLG_0 == 2 && wsField.VALUEFLG_0 == 1)
                {
                    var dictData = data as IDictionary<string, object>;

                    XML += "<FLD NAME=" + wsField.FLD_0 + ">" + dictData[wsField.VALUE_0] + "</FLD> ";
                }
                else if (wsField.FLDFLG_0 == 1 && wsField.VALUEFLG_0 == 2)
                {
                    XML += wsField.VALUE_0;
                }
            }

            return XML;
        }

        public static async Task<Result<WebServiceData>> RunWebServiceAsync(WEBSERVICE webService, string XML)
        {
            Result<WebServiceData> result = GetWebServiceData(webService, null, XML);

            try
            {
                CAdxCallContext cadxContext = GetContext(webService);
                CadxWebServiceHelper cadxWService = GetWebServiceX3(webService);
                CAdxResultXml cadxResult = await Task.Run(() => cadxWService.run(cadxContext, webService.NAM_0, XML));

                result = GetWebServiceData(webService, cadxResult, XML);
            }
            catch (Exception ex)
            {
                result.AddInvalidMessage(ex.Message);
            }

            return result;
        }
        #endregion

        #region Private Methods
        private static Result<WebServiceData> GetWebServiceData(WEBSERVICE webService, CAdxResultXml cadxResult, string XML)
        {
            Result<WebServiceData> result = GetResult(cadxResult);

            string document = GetDocument(webService, result);
            result.Obj = new WebServiceData(webService, XML, document, result.ToString(), cadxResult?.resultXml);

            return result;
        }

        private static string GetDocument(WEBSERVICE webService, Result<WebServiceData> result)
        {
            string document = string.Empty;

            if (result.Success && webService.VCRNUMFLG_0 == 2)
            {
                if (result.Messages.Count > 0)
                {
                    string finalMessage = string.Empty;

                    foreach (string message in result.Messages)
                    {
                        finalMessage += message;
                    }

                    if (webService.VCRNUMSEARCH_0 != string.Empty && webService.VCRNUMSIZE_0 > 0)
                    {
                        int index = finalMessage.IndexOf(webService.VCRNUMSEARCH_0);

                        if (index > -1 && finalMessage.Length - index >= webService.VCRNUMSIZE_0)
                        {
                            document = finalMessage.Substring(index, webService.VCRNUMSIZE_0);
                        }
                    }
                    else
                    {
                        document = finalMessage;
                    }
                }
            }

            return document;
        }

        private static CAdxCallContext GetContext(WEBSERVICE webService)
        {
            CAdxCallContext cadxContext = new CAdxCallContext
            {
                codeLang = webService.CODELANG_0,
                poolAlias = webService.POOL_0
            };

            return cadxContext;
        }

        private static CadxWebServiceHelper GetWebServiceX3(WEBSERVICE webService)
        {
            CadxWebServiceHelper cadxWService = new CadxWebServiceHelper
            {
                Url = webService.URL_0,
                Credentials = new NetworkCredential(webService.USER_0, webService.PASS_0),
                BasicAuth = true,
                PreAuthenticate = true
            };

            ServicePointManager.ServerCertificateValidationCallback = AcceptCertificates;

            return cadxWService;
        }

        private static bool AcceptCertificates(object sender, X509Certificate certification, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        private static Result<WebServiceData> GetResult(CAdxResultXml cadxResult)
        {
            Result<WebServiceData> result = new Result<WebServiceData>();

            if (cadxResult != null)
            {
                if (cadxResult.messages != null)
                {
                    foreach (CAdxMessage cadxMessage in cadxResult.messages)
                    {
                        result.AddMessage(cadxMessage.message);
                    }
                }

                if (cadxResult.status == 1)
                {
                    result.Success = true;
                }
            }

            return result;
        }
        #endregion
    }

    public class CadxWebServiceHelper : CAdxWebServiceXmlCCService
    {
        public bool BasicAuth { get; set; }

        public string AccessToken { get; set; }

        protected override WebRequest GetWebRequest(Uri uri)
        {
            HttpWebRequest WebRequest = (HttpWebRequest)base.GetWebRequest(uri);
            NetworkCredential Credentials = (NetworkCredential)WebRequest.Credentials;

            if (BasicAuth)
            {
                if (Credentials != null)
                {
                    string AuthInfo;

                    if (Credentials.Domain != null && Credentials.Domain.Length > 0)
                    {
                        AuthInfo = Credentials.Domain + @"\" + Credentials.UserName + ":" + Credentials.Password;
                    }
                    else
                    {
                        AuthInfo = "" + Credentials.UserName + ":" + Credentials.Password;
                    }

                    AuthInfo = Convert.ToBase64String(Encoding.Default.GetBytes(AuthInfo));
                    WebRequest.Headers["Authorization"] = "Basic " + AuthInfo;
                }
            }
            else
            {
                WebRequest.Headers["Authorization"] = "Bearer " + AccessToken;
            }

            return WebRequest;
        }
    }
}
