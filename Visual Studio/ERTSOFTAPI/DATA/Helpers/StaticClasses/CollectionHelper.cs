﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace DATA.Helpers.StaticClasses
{
    public static class CollectionHelper
    {
        public static T[] Add<T>(this T[] array, T element)
        {
            int i;
            T[] newArray = new T[array.Length + 1];

            for (i = 0; i < array.Length; i++)
            {
                newArray[i] = array[i];
            }

            newArray[i] = element;

            return newArray;
        }

        /// <summary>
        /// Validates any IEnumerable or class that implements it based on condition you can send, if null no condition is applied.
        /// Returns true when list is not null and contains at least one element, false otherwise.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumerable"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static bool IsValid<T>(this IEnumerable<T> enumerable, Func<T, bool> predicate = null)
        {
            if (enumerable == null)
            {
                return false;
            }

            if (predicate == null)
            {
                return enumerable.Any();
            }

            return enumerable.Any(predicate);
        }

        public static IEnumerable<IEnumerable<T>> SplitIntoSets<T>(this IEnumerable<T> source, int itemsPerSet)
        {
            var sourceList = source as List<T> ?? source.ToList();

            for (var index = 0; index < sourceList.Count; index += itemsPerSet)
            {
                yield return sourceList.Skip(index).Take(itemsPerSet);
            }
        }
    }
}
