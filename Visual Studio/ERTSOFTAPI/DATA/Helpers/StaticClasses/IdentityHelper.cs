﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;
using System.Web;

namespace DATA.Helpers.StaticClasses
{
    public static class IdentityHelper
    {
        public static decimal GetId(this IIdentity identity)
        {
            string id = identity.GetField(nameof(USER.ROWID));

            return id != null ? Convert.ToDecimal(id) : default;
        }

        public static string GetField(this IIdentity identity, string field)
        {
            if (identity.IsAuthenticated)
            {
                IEnumerable<Claim> claims = ((ClaimsIdentity)identity).Claims;
                Claim claimType = claims.SingleOrDefault(c => c.Type == field);

                return claimType.Value;
            }

            return null;
        }

        public static USER GetAuthenticatedUser()
        {
            return new USER
            {
                USR_0 = HttpContext.Current?.User?.Identity?.GetField(nameof(USER.USR_0)) ?? "N/D",
                PROFILE_0 = HttpContext.Current?.User?.Identity?.GetField(nameof(USER.PROFILE_0)) ?? "N/D"
            };
        }
    }
}