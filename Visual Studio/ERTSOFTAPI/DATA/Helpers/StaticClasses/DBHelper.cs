﻿using System;
using System.Linq;
using System.Data;
using MODELS.Helpers;
using MODELS.Attributes;
using System.Reflection;
using System.Diagnostics;
using System.Transactions;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DATA.Helpers.StaticClasses
{
    internal static class DBHelper
    {
        #region QueryLibrary
        private static Dictionary<string, SqlDbType> SQLTypeDictionary { get; } = new Dictionary<string, SqlDbType>()
        {
            { "bigint", SqlDbType.BigInt },
            { "binary", SqlDbType.Binary },
            { "bit", SqlDbType.Bit },
            { "char", SqlDbType.Char },
            { "datetime", SqlDbType.DateTime },
            { "decimal", SqlDbType.Decimal },
            { "float", SqlDbType.Float },
            { "image", SqlDbType.Image },
            { "int", SqlDbType.Int },
            { "money", SqlDbType.Money },
            { "nchar", SqlDbType.NChar },
            { "ntext", SqlDbType.NText },
            { "nvarchar", SqlDbType.NVarChar },
            { "real", SqlDbType.Real },
            { "uniqueidentifier", SqlDbType.UniqueIdentifier },
            { "smalldatetime", SqlDbType.SmallDateTime },
            { "smallint", SqlDbType.SmallInt },
            { "smallmoney", SqlDbType.SmallMoney },
            { "text", SqlDbType.Text },
            { "timestamp", SqlDbType.Timestamp },
            { "tinyint", SqlDbType.TinyInt },
            { "varbinary", SqlDbType.VarBinary },
            { "varchar", SqlDbType.VarChar },
            { "variant", SqlDbType.Variant },
            { "xml", SqlDbType.Xml },
            { "udt", SqlDbType.Udt },
            { "structured", SqlDbType.Structured },
            { "date", SqlDbType.Date },
            { "time", SqlDbType.Time },
            { "datetime2", SqlDbType.DateTime2 },
            { "datetimeoffset", SqlDbType.DateTimeOffset }
        };

        public static Dictionary<string, SqlParameter> GetSQLParameters(this string tableName, bool includeFieldsWithoutDefaultValue, string connName)
        {
            Dictionary<string, SqlParameter> parameters = new Dictionary<string, SqlParameter>();
            string query = @"SELECT COLUMN_NAME AS 'Column', DATA_TYPE AS 'DataType', CHARACTER_MAXIMUM_LENGTH AS 'MaxTextSize',
            NUMERIC_PRECISION AS 'MaxNumberSize', NUMERIC_SCALE AS 'DecimalPlaces', DATETIME_PRECISION AS 'DatePrecision' FROM
            INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = @SCHEMA AND TABLE_NAME = @TABLE";

            if (includeFieldsWithoutDefaultValue)
            {
                query += " AND COLUMN_DEFAULT IS NULL";
            }

            query += " ORDER BY ORDINAL_POSITION";

            using (SqlCommand command = new SqlCommand(query, NewConnection(connName, false)))
            {
                command.Connection.ConnectionString += "Enlist=false;";

                string schema = tableName.Substring(0, tableName.IndexOf("."));
                string table = tableName.Substring(tableName.IndexOf(".") + 1);

                command.Parameters.Add("@SCHEMA", SqlDbType.VarChar).Value = schema;
                command.Parameters.Add("@TABLE", SqlDbType.VarChar).Value = table;

                using (SqlDataReader dr = NewDataReader(command))
                {
                    while (dr.Read())
                    {
                        SqlParameter parameter = new SqlParameter()
                        {
                            ParameterName = "@" + dr["Column"],
                            SqlDbType = SQLTypeDictionary[Convert.ToString(dr["DataType"])]
                        };

                        if (!DBNull.Value.Equals(dr["MaxTextSize"]))
                        {
                            parameter.Size = Convert.ToInt32(dr["MaxTextSize"]);
                        }

                        if (!DBNull.Value.Equals(dr["DatePrecision"]))
                        {
                            parameter.Size = Convert.ToInt32(dr["DatePrecision"]);
                        }

                        if (!DBNull.Value.Equals(dr["MaxNumberSize"]))
                        {
                            parameter.Precision = Convert.ToByte(dr["MaxNumberSize"]);
                        }

                        if (!DBNull.Value.Equals(dr["DecimalPlaces"]))
                        {
                            parameter.Scale = Convert.ToByte(dr["DecimalPlaces"]);
                        }

                        parameters.Add(Convert.ToString(dr["Column"]), parameter);
                    }
                }
            }

            return parameters;
        }
        #endregion

        #region ADO.NET
        public static SqlConnection NewConnection(string connName, bool openConn = true)
        {
            try
            {
                string environment = Debugger.IsAttached ? "Dev" : "Prod";
                //environment = Debugger.IsAttached ? "Prod" : "Prod"; //Uncomment temporarily if you wish to test in production using Visual Studio
                string calculatedConnName = environment + connName + "Connection";
                string connString = Properties.Settings.Default[calculatedConnName].ToString();

                SqlConnection connection = new SqlConnection(connString);

                if (openConn)
                {
                    connection.Open();
                }

                return connection;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static TransactionScope NewTransaction(System.Transactions.IsolationLevel iso = System.Transactions.IsolationLevel.Serializable, TransactionScopeOption tso = TransactionScopeOption.Required, TimeSpan timeout = default)
        {
            try
            {
                if (timeout == default)
                {
                    timeout = TransactionManager.DefaultTimeout;
                }

                if (Transaction.Current == null || tso == TransactionScopeOption.RequiresNew)
                    return new TransactionScope(tso, new TransactionOptions() { IsolationLevel = iso, Timeout = timeout });
                else
                    return new TransactionScope(tso, new TransactionOptions() { IsolationLevel = Transaction.Current.IsolationLevel, Timeout = timeout });
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static TransactionScope NewTransaction(TransactionScopeAsyncFlowOption tsaf, System.Transactions.IsolationLevel iso = System.Transactions.IsolationLevel.Serializable, TransactionScopeOption tso = TransactionScopeOption.Required, TimeSpan timeout = default)
        {
            try
            {
                if (timeout == default)
                {
                    timeout = TransactionManager.DefaultTimeout;
                }

                if (Transaction.Current == null || tso == TransactionScopeOption.RequiresNew)
                {
                    return new TransactionScope(tso, new TransactionOptions() { IsolationLevel = iso, Timeout = timeout }, tsaf);
                }
                else
                {
                    return new TransactionScope(tso, new TransactionOptions()
                    {
                        IsolationLevel = Transaction.Current.IsolationLevel,
                        Timeout = timeout
                    }, tsaf);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static SqlDataReader NewDataReader(this SqlCommand command, CommandBehavior commandBehavior = CommandBehavior.Default)
        {
            try
            {
                if (command.Connection.State != ConnectionState.Closed)
                {
                    command.Connection.Close();
                }

                command.Connection.Open();

                return command.ExecuteReader(commandBehavior);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static SqlCommand NewCommand(this SqlConnection connection, string query)
        {
            SqlCommand dbCommand = connection.CreateCommand();
            dbCommand.CommandText = query;

            return dbCommand;
        }

        public static bool IsEnlisted(this SqlConnection sqlConnection)
        {
            object innerConnection = typeof(SqlConnection).GetField("_innerConnection", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.FlattenHierarchy).GetValue(sqlConnection);

            FieldInfo enlistedTransactionField = EnumerateInheritanceChain(innerConnection.GetType())
                .Select(t => t.GetField("_enlistedTransaction", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.FlattenHierarchy))
                .Where(fi => fi != null)
                .First();

            object enlistedTransaction = enlistedTransactionField.GetValue(innerConnection);

            return enlistedTransaction != null;
        }

        private static IEnumerable<Type> EnumerateInheritanceChain(Type root)
        {
            for (Type current = root; current != null; current = current.BaseType)
            {
                yield return current;
            }
        }
        #endregion

        #region Validators
        public static async Task<Result<T>> IsModelOnInsertValidAsync<T>(this T obj) where T : class
        {
            return await Task.Run(() => obj.IsModelOnInsertValid());
        }

        public static Result<T> IsModelOnInsertValid<T>(this T obj) where T : class
        {
            Result<T> result = new Result<T>
            {
                Obj = obj
            };

            if (obj == null)
            {
                result.AddInvalidMessage($"O modelo '{typeof(T).Name}' não pode ser null.");

                return result;
            }

            List<string> propNameList = new List<string>();
            
            foreach (PropertyInfo property in ClassHelper.GetSimpleProperties<T>())
            {
                if (!DBRules.ValidPropsOnCreate.Contains(property.Name))
                {
                    if (!property.CustomAttributes.Where(x => x.AttributeType == typeof(SqlDefaultValueAttribute) || x.AttributeType == typeof(DatabaseGeneratedAttribute)).Any())
                    {
                        propNameList.Add(property.Name);
                    }
                }
            }

            return ClassHelper.IsPropertiesValid(obj, propNameList.ToArray());
        }

        public static async Task<Result<T>> IsModelOnUpdateValidAsync<T>(this T obj) where T : class
        {
            return await Task.Run(() => obj.IsModelOnUpdateValid());
        }

        public static Result<T> IsModelOnUpdateValid<T>(this T obj) where T : class
        {
            Result<T> result = new Result<T>
            {
                Obj = obj
            };

            if (obj == null)
            {
                result.AddInvalidMessage($"O modelo '{typeof(T).Name}' não pode ser null.");

                return result;
            }

            List<string> propNameList = new List<string>();

            foreach (PropertyInfo property in ClassHelper.GetSimpleProperties<T>())
            {
                if (!DBRules.ValidPropsOnUpdate.Contains(property.Name))
                {
                    propNameList.Add(property.Name);
                }
            }

            return ClassHelper.IsPropertiesValid(obj, propNameList.ToArray());
        }

        public static Result<T> ChangeModelByModifiableFields<T>(T oldModel, T newModel, params string[] modifiableFields) where T : class
        {
            Result<T> resultT = new Result<T>(true)
            {
                Obj = newModel
            };

            if (modifiableFields.Length == 0)
            {
                return resultT;
            }

            List<string> propertyList = new List<string>();

            foreach (PropertyInfo property in ClassHelper.GetSimpleProperties<T>())
            {
                if (!modifiableFields.Contains(property.Name) && typeof(DBRules).GetProperty(property.Name) == null)
                {
                    propertyList.Add(property.Name);
                }
            }

            string unmodifiableFields = string.Empty;

            foreach (string prop in propertyList)
            {
                object oldValue = oldModel.GetPropValue(prop);
                object newValue = newModel.GetPropValue(prop);

                if (!oldValue.Equals(newValue))
                {
                    newModel.GetType().GetProperty(prop).SetValue(newModel, oldValue, null);
                    unmodifiableFields += $"{prop}, ";

                    resultT.AddMessage($"Valor '{newValue}' foi assumido como '{oldValue}'.");
                }
            }

            if (unmodifiableFields != string.Empty)
            {
                unmodifiableFields = unmodifiableFields.Remove(unmodifiableFields.Length - 2);

                resultT.Messages.Insert(0, $"Os campos [{unmodifiableFields}] não são passíveis de modificação.");
            }

            return resultT;
        }

        public static Result<T> ChangeModelByUnModifiableFields<T>(T oldModel, T newModel, params string[] unmodifiableFields) where T : class
        {
            Result<T> resultT = new Result<T>(true)
            {
                Obj = newModel
            };

            if (unmodifiableFields.Length == 0)
            {
                return resultT;
            }

            List<string> propertyList = new List<string>();

            foreach (PropertyInfo property in ClassHelper.GetSimpleProperties<T>())
            {
                if (unmodifiableFields.Contains(property.Name) && typeof(DBRules).GetProperty(property.Name) != null)
                {
                    propertyList.Add(property.Name);
                }
            }

            string modifiableFields = string.Empty;

            foreach (string prop in propertyList)
            {
                object oldValue = oldModel.GetPropValue(prop);
                object newValue = newModel.GetPropValue(prop);

                if (!oldValue.Equals(newValue))
                {
                    newModel.GetType().GetProperty(prop).SetValue(newModel, oldValue, null);
                    modifiableFields += $"{prop}, ";

                    resultT.AddMessage($"Valor '{newValue}' foi assumido como '{oldValue}'.");
                }
            }

            if (modifiableFields != string.Empty)
            {
                modifiableFields = modifiableFields.Remove(modifiableFields.Length - 2);

                resultT.Messages.Insert(0, $"Os campos [{modifiableFields}] não são passíveis de modificação.");
            }

            return resultT;
        }
        #endregion
    }

    public static class QueryStringHelper
    {
        public static string TableName<T>(string aliasName = "")
        {
            Type type = typeof(T);
            string aliasStr = string.Empty;

            if (!string.IsNullOrWhiteSpace(aliasName))
                aliasStr = $" {aliasName}";

            foreach (object atributo in type.GetCustomAttributes(false))
            {
                if (atributo is TableAttribute Table)
                    return Table.Name + aliasStr;
            }

            return string.Empty;
        }

        public static string SelectFromProps<T>(this string str, string aliasName = "", bool includeFrom = false)
        {
            if (string.IsNullOrWhiteSpace(aliasName))
            {
                foreach (PropertyInfo prop in ClassHelper.GetSimpleProperties<T>())
                {
                    str += prop.Name + ", ";
                }
            }
            else
            {
                foreach (PropertyInfo prop in ClassHelper.GetSimpleProperties<T>())
                {
                    str += aliasName + "." + prop.Name + ", ";
                }
            }

            str = str.Remove(str.Length - 2);

            if (includeFrom)
            {
                str += $" FROM {TableName<T>(aliasName)}";
            }

            return str;
        }

        public static string SelectAll<T>(string aliasName = "")
        {
            return "SELECT ".SelectFromProps<T>(aliasName, true);
        }

        public static string SelectTopAll<T>(int top, string aliasName = "")
        {
            string str = $"SELECT TOP {top} ".SelectFromProps<T>(aliasName, true);

            return str;
        }

        public static string SelectById<T>()
        {
            return SelectAll<T>() + $" WHERE {nameof(DBRules.ROWID)} = @{nameof(DBRules.ROWID)}";
        }

        public static string SelectByField<T>(string field)
        {
            return SelectAll<T>() + $" WHERE {field} = @{field}";
        }

        public static string SelectByFields<T>(params string[] fields)
        {
            string table = TableName<T>();
            string query = "SELECT ";

            foreach (PropertyInfo prop in ClassHelper.GetSimpleProperties<T>())
            {
                query += $"{prop.Name}, ";
            }

            query = query.Remove(query.Length - 2);
            query += $" FROM {table} WHERE ";

            foreach (string field in fields)
            {
                query += $"{field} = @{field} AND ";
            }

            query = query.Remove(query.Length - 5);

            return query;
        }

        public static string SelectByConditions<T>(params string[] conditions)
        {
            string table = TableName<T>();
            string query = "SELECT ";

            foreach (PropertyInfo prop in ClassHelper.GetSimpleProperties<T>())
            {
                query += prop.Name + ", ";
            }

            query = query.Remove(query.Length - 2);
            query += $" FROM {table} WHERE ";

            foreach (string condition in conditions)
            {
                query += $"{condition} AND ";
            }

            query = query.Remove(query.Length - 5);

            return query;
        }

        public static string InnerJoin<T>(this string str, string aliasName, string condition, bool includeSelect = true)
        {
            if (includeSelect)
            {
                int index = str.IndexOf(" FROM ");

                str = str.Insert(index, ", ".SelectFromProps<T>(aliasName));
            }

            return str += $" INNER JOIN {TableName<T>(aliasName)} ON {condition}";
        }

        public static string LeftJoin<T>(this string str, string aliasName, string condition, bool includeSelect = true)
        {
            if (includeSelect)
            {
                int index = str.IndexOf(" FROM ");

                str = str.Insert(index, ", " + SelectFromProps<T>(str, aliasName));
            }

            return str += $" LEFT JOIN {TableName<T>(aliasName)} ON {condition}";
        }

        public static string RightJoin<T>(this string str, string aliasName, string condition, bool includeSelect = true)
        {
            if (includeSelect)
            {
                int index = str.IndexOf(" FROM ");

                str = str.Insert(index, ", " + SelectFromProps<T>(str, aliasName));
            }

            return str += $" RIGHT JOIN {TableName<T>(aliasName)} ON {condition}";
        }

        public static string Where(this string str, string condition)
        {
            return str += $" WHERE {condition}";
        }

        public static string And(this string str, string condition)
        {
            return str += $" AND {condition}";
        }

        public static string Or(this string str, string condition)
        {
            return str += $" OR {condition}";
        }

        public static string OrderBy(this string str, string orderBy)
        {
            return str += $" ORDER BY {orderBy}";
        }

        public static string GroupBy(this string str, string groupBy)
        {
            return str += $" GROUP BY {groupBy}";
        }

        public static string Insert<T>()
        {
            string table = TableName<T>();
            string query = $"INSERT INTO {table}";
            string fieldsStr = " (";
            string parametersStr = " (";

            foreach (PropertyInfo property in ClassHelper.GetSimpleProperties<T>())
            {
                if (!property.CustomAttributes.Where(p => p.AttributeType == typeof(SqlDefaultValueAttribute) || p.AttributeType == typeof(DatabaseGeneratedAttribute)).Any())
                {
                    fieldsStr += $"{property.Name}, ";
                    parametersStr += $"@{property.Name}, ";
                } 
            }

            fieldsStr = $"{fieldsStr.Remove(fieldsStr.Length - 2)})";
            parametersStr = $"{parametersStr.Remove(parametersStr.Length - 2)})";

            query += $"{fieldsStr} OUTPUT INSERTED.{nameof(DBRules.ROWID)} VALUES{parametersStr};";

            return query;
        }

        public static string Update<T>()
        {
            string table = TableName<T>();
            string query = "UPDATE " + table + " SET ";

            foreach (PropertyInfo property in ClassHelper.GetSimpleProperties<T>())
            {
                if (property.Name == nameof(DBRules.UPDDAT_0) || property.Name == nameof(DBRules.UPDDATTIM_0))
                {
                    SqlDefaultValueAttribute defaultValueAttr = (SqlDefaultValueAttribute)property.GetCustomAttribute(typeof(SqlDefaultValueAttribute));

                    if (defaultValueAttr == null)
                        query += $"{property.Name} = getdate(), ";
                    else
                        query += $"{property.Name} = {defaultValueAttr.DefaultValue}, ";

                    continue;
                }
                
                if (!DBRules.IgnorePropsOnUpdate.Contains(property.Name))
                {
                    query += $"{property.Name} = @{property.Name}, ";
                }
            }

            query = query.Remove(query.Length - 2);
            query += $" WHERE {nameof(DBRules.ROWID)} = @{nameof(DBRules.ROWID)}";

            return query;
        }

        public static string Delete<T>()
        {
            string table = TableName<T>();
            string query = $"DELETE FROM {table} WHERE {nameof(DBRules.ROWID)} = @{nameof(DBRules.ROWID)}";

            return query;
        }
    }
}