﻿using Ninject.Parameters;
using DATA.Ninject;
using System.Transactions;
using System;
using DATA.LayeredArquiteture.Repositories.UnitOfWork;

namespace DATA.Helpers.StaticClasses
{
    public static class NinjectHelper
    {
        public static IUnitOfWork UowInstance(TransactionScopeAsyncFlowOption? tsaf = null, IsolationLevel iso = IsolationLevel.Serializable,
            TransactionScopeOption tso = TransactionScopeOption.Required, TimeSpan timeout = default)
        {
            IParameter[] parameters = UowParams(tsaf, iso, tso, timeout);

            return NinjectWebCommon.GetInstance<IUnitOfWork>(parameters);
        }

        private static IParameter[] UowParams(TransactionScopeAsyncFlowOption? tsaf, IsolationLevel iso, TransactionScopeOption tso, TimeSpan timeout = default)
        {
            ConstructorArgument tsafParam = new ConstructorArgument("tsaf", tsaf);
            ConstructorArgument isoParam = new ConstructorArgument("iso", iso);
            ConstructorArgument tsoParam = new ConstructorArgument("tso", tso);
            ConstructorArgument timeoutParam = new ConstructorArgument("timeout", timeout);

            IParameter[] constructorParams = new ConstructorArgument[4];
            constructorParams[0] = tsafParam;
            constructorParams[1] = isoParam;
            constructorParams[2] = tsoParam;
            constructorParams[3] = timeoutParam;

            return constructorParams;
        }


        public static T RepoInstance<T>(string connName)
        {
            ConstructorArgument connNameParam = new ConstructorArgument("connName", connName);

            return NinjectWebCommon.GetInstance<T>(connNameParam);
        }
    }
}
