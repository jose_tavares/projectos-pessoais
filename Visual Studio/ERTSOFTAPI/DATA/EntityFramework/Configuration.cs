﻿using System;
using System.Data.Entity.SqlServer;
using MODELS.BusinessObject.ERTSOFT.Database;
using System.Data.Entity.Migrations;
using System.Data.Entity.Migrations.Model;
using System.Data.Entity.Infrastructure.Annotations;

namespace DATA.EntityFramework
{
    internal sealed class Configuration : DbMigrationsConfiguration<ERTSOFTContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
            SetSqlGenerator("System.Data.SqlClient", new CustomSqlServerMigrationSqlGenerator());
            ContextKey = "MODELS.BusinessObject.ERTSOFT";
            CommandTimeout = 1500;
        }

        protected override void Seed(ERTSOFTContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method
            //  to avoid creating duplicate seed data.

            context.USER.AddOrUpdate(
                new USER
                {
                    ROWID = 1,
                    USR_0 = "ADMIN",
                    USERNAM_0 = "admin",
                    PASSWORD_0 = "admin",
                    FULLNAM_0 = "Admin Admin",
                    FIRSTNAM_0 = "Admin",
                    LASTNAM_0 = "Admin",
                    EMAIL_0 = "it@ertgrupo.com",
                    FUNCTION_0 = "Admin",
                    MOBPHONE_0 = "",
                    PHONE_0 = "",
                    BIRTHDAT_0 = new DateTime(1900, 1, 1),
                    PROFILE_0 = "ADMIN",
                    LOGGEDFLG_0 = 1,
                    ADMINFLG_0 = 2,
                    SPECIALFLG_0 = 2,
                    ENABFLG_0 = 2,
                    CREUSR_0 = "ADMIN",
                    UPDUSR_0 = "ADMIN"
                }
            );

            context.PROFILE.AddOrUpdate(
                new PROFILE
                {
                    ROWID = 1,
                    PROFILE_0 = "ADMIN",
                    NAM_0 = "Administrador",
                    ENABFLG_0 = 2,
                    CREUSR_0 = "ADMIN",
                    UPDUSR_0 = "ADMIN"
                }
            );
        }
    }

    internal class CustomSqlServerMigrationSqlGenerator : SqlServerMigrationSqlGenerator
    {
        protected override void Generate(AddColumnOperation addColumnOperation)
        {
            SetAnnotatedColumn(addColumnOperation.Column);

            base.Generate(addColumnOperation);
        }

        protected override void Generate(CreateTableOperation createTableOperation)
        {
            foreach (ColumnModel col in createTableOperation.Columns)
            {
                SetAnnotatedColumn(col);
            }

            base.Generate(createTableOperation);
        }

        private void SetAnnotatedColumn(ColumnModel col)
        {
            if (col.Annotations.TryGetValue("SqlDefaultValue", out AnnotationValues values))
            {
                col.DefaultValueSql = (string)values.NewValue;
            }
        }
    }
}
