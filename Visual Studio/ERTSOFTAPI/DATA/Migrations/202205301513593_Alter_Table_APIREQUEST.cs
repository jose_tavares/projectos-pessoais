﻿namespace DATA.EntityFramework
{
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;

    public partial class Alter_Table_APIREQUEST : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "ERT.MESSAGE",
                c => new
                    {
                        ROWID = c.Decimal(nullable: false, precision: 38, scale: 0, identity: true),
                        CODE_0 = c.Int(nullable: false),
                        CAPTION_0 = c.String(nullable: false, maxLength: 255, unicode: false),
                        MSG_0 = c.String(nullable: false, maxLength: 8000, unicode: false),
                        BUTTONS_0 = c.Int(nullable: false),
                        DEFAULTBTN_0 = c.Int(nullable: false),
                        ICON_0 = c.Int(nullable: false),
                        CREDAT_0 = c.DateTime(nullable: false, storeType: "date",
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        CREUSR_0 = c.String(nullable: false, maxLength: 5, unicode: false),
                        UPDDAT_0 = c.DateTime(nullable: false, storeType: "date",
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        UPDUSR_0 = c.String(nullable: false, maxLength: 5, unicode: false),
                        CREDATTIM_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        UPDDATTIM_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.ROWID);
            
            AddColumn("ERT.APIREQUEST", "STATUSDES_0", c => c.String(nullable: false, maxLength: 200, unicode: false));
            AlterColumn("ERT.APIREQUEST", "STATUSCODE_0", c => c.Int(nullable: false));
            AlterColumn("ERT.APIREQUEST", "TOKEN_0", c => c.String(nullable: false, maxLength: 1000, unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("ERT.APIREQUEST", "TOKEN_0", c => c.String(nullable: false, maxLength: 500, unicode: false));
            AlterColumn("ERT.APIREQUEST", "STATUSCODE_0", c => c.String(nullable: false, maxLength: 20, unicode: false));
            DropColumn("ERT.APIREQUEST", "STATUSDES_0");
            DropTable("ERT.MESSAGE",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CREDAT_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "CREDATTIM_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "UPDDAT_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "UPDDATTIM_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                });
        }
    }
}
