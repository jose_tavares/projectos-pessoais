﻿namespace DATA.EntityFramework
{
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;

    public partial class Add_Table_APIREQUEST : DbMigration
    {
        public override void Up()
        {
            DropIndex("ERT.USER", "IX_USR");
            CreateTable(
                "ERT.APIREQUEST",
                c => new
                    {
                        ROWID = c.Decimal(nullable: false, precision: 38, scale: 0, identity: true),
                        CLIENTAPP_0 = c.String(nullable: false, maxLength: 50, unicode: false),
                        USER_0 = c.String(nullable: false, maxLength: 5, unicode: false),
                        USERNAM_0 = c.String(nullable: false, maxLength: 50, unicode: false),
                        STATUSCODE_0 = c.String(nullable: false, maxLength: 20, unicode: false),
                        URI_0 = c.String(nullable: false, maxLength: 8000, unicode: false),
                        METHOD_0 = c.String(nullable: false, maxLength: 100, unicode: false),
                        CONTROLLER_0 = c.String(nullable: false, maxLength: 100, unicode: false),
                        REQUESTBODY_0 = c.String(nullable: false, unicode: false),
                        RESPONSEBODY_0 = c.String(nullable: false, unicode: false),
                        TOKEN_0 = c.String(nullable: false, maxLength: 500, unicode: false),
                        EXECUTIONTIME_0 = c.Long(nullable: false),
                        CREDAT_0 = c.DateTime(nullable: false, storeType: "date",
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        CREUSR_0 = c.String(nullable: false, maxLength: 5, unicode: false),
                        UPDDAT_0 = c.DateTime(nullable: false, storeType: "date",
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        UPDUSR_0 = c.String(nullable: false, maxLength: 5, unicode: false),
                        CREDATTIM_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        UPDDATTIM_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.ROWID);
            
            AlterColumn("ERT.USER", "USR_0", c => c.String(nullable: false, maxLength: 5, unicode: false));
            CreateIndex("ERT.USER", "USR_0", unique: true, name: "IX_USR");
        }
        
        public override void Down()
        {
            DropIndex("ERT.USER", "IX_USR");
            AlterColumn("ERT.USER", "USR_0", c => c.String(nullable: false, maxLength: 50, unicode: false));
            DropTable("ERT.APIREQUEST",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CREDAT_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "CREDATTIM_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "UPDDAT_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "UPDDATTIM_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                });
            CreateIndex("ERT.USER", "USR_0", unique: true, name: "IX_USR");
        }
    }
}
