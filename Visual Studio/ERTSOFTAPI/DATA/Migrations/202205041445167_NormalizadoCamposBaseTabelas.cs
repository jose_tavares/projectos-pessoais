﻿namespace DATA.EntityFramework
{
    using System.Data.Entity.Migrations;

    public partial class NormalizadoCamposBaseTabelas : DbMigration
    {
        public override void Up()
        {
            DropIndex("ERT.WEBSERVICE", "IX_WEBSERVICE");
            AlterColumn("ERT.EXPORTWS", "CREDAT_0", c => c.DateTime(nullable: false, storeType: "date"));
            AlterColumn("ERT.EXPORTWS", "UPDDAT_0", c => c.DateTime(nullable: false, storeType: "date"));
            AlterColumn("ERT.EXPORTWSLOG", "WSCODE_0", c => c.String(nullable: false, maxLength: 100, unicode: false));
            AlterColumn("ERT.EXPORTWSLOG", "CREDAT_0", c => c.DateTime(nullable: false, storeType: "date"));
            AlterColumn("ERT.EXPORTWSLOG", "CREUSR_0", c => c.String(nullable: false, maxLength: 5, unicode: false));
            AlterColumn("ERT.EXPORTWSLOG", "UPDDAT_0", c => c.DateTime(nullable: false, storeType: "date"));
            AlterColumn("ERT.EXPORTWSLOG", "UPDUSR_0", c => c.String(nullable: false, maxLength: 5, unicode: false));
            AlterColumn("ERT.IMPORTWS", "CREDAT_0", c => c.DateTime(nullable: false, storeType: "date"));
            AlterColumn("ERT.IMPORTWS", "CREUSR_0", c => c.String(nullable: false, maxLength: 5, unicode: false));
            AlterColumn("ERT.IMPORTWS", "UPDDAT_0", c => c.DateTime(nullable: false, storeType: "date"));
            AlterColumn("ERT.IMPORTWS", "UPDUSR_0", c => c.String(nullable: false, maxLength: 5, unicode: false));
            AlterColumn("ERT.IMPORTWSLOG", "WSCODE_0", c => c.String(nullable: false, maxLength: 100, unicode: false));
            AlterColumn("ERT.IMPORTWSLOG", "CREDAT_0", c => c.DateTime(nullable: false, storeType: "date"));
            AlterColumn("ERT.IMPORTWSLOG", "CREUSR_0", c => c.String(nullable: false, maxLength: 5, unicode: false));
            AlterColumn("ERT.IMPORTWSLOG", "UPDDAT_0", c => c.DateTime(nullable: false, storeType: "date"));
            AlterColumn("ERT.IMPORTWSLOG", "UPDUSR_0", c => c.String(nullable: false, maxLength: 5, unicode: false));
            AlterColumn("ERT.STODEFECT", "CREDAT_0", c => c.DateTime(nullable: false, storeType: "date"));
            AlterColumn("ERT.STODEFECT", "UPDDAT_0", c => c.DateTime(nullable: false, storeType: "date"));
            AlterColumn("ERT.STOJOU", "WSCODE_0", c => c.String(nullable: false, maxLength: 100, unicode: false));
            AlterColumn("ERT.STOJOU", "CREDAT_0", c => c.DateTime(nullable: false, storeType: "date"));
            AlterColumn("ERT.STOJOU", "UPDDAT_0", c => c.DateTime(nullable: false, storeType: "date"));
            AlterColumn("ERT.STOJOULOG", "WSCODE_0", c => c.String(nullable: false, maxLength: 100, unicode: false));
            AlterColumn("ERT.STOJOULOG", "CREDAT_0", c => c.DateTime(nullable: false, storeType: "date"));
            AlterColumn("ERT.STOJOULOG", "UPDDAT_0", c => c.DateTime(nullable: false, storeType: "date"));
            AlterColumn("ERT.WEBSERVICE", "CODE_0", c => c.String(nullable: false, maxLength: 100, unicode: false));
            CreateIndex("ERT.WEBSERVICE", "CODE_0", name: "IX_WEBSERVICE");
        }
        
        public override void Down()
        {
            DropIndex("ERT.WEBSERVICE", "IX_WEBSERVICE");
            AlterColumn("ERT.WEBSERVICE", "CODE_0", c => c.String(nullable: false, maxLength: 200, unicode: false));
            AlterColumn("ERT.STOJOULOG", "UPDDAT_0", c => c.DateTime(nullable: false));
            AlterColumn("ERT.STOJOULOG", "CREDAT_0", c => c.DateTime(nullable: false));
            AlterColumn("ERT.STOJOULOG", "WSCODE_0", c => c.String(nullable: false, maxLength: 50, unicode: false));
            AlterColumn("ERT.STOJOU", "UPDDAT_0", c => c.DateTime(nullable: false));
            AlterColumn("ERT.STOJOU", "CREDAT_0", c => c.DateTime(nullable: false));
            AlterColumn("ERT.STOJOU", "WSCODE_0", c => c.String(nullable: false, maxLength: 50, unicode: false));
            AlterColumn("ERT.STODEFECT", "UPDDAT_0", c => c.DateTime(nullable: false));
            AlterColumn("ERT.STODEFECT", "CREDAT_0", c => c.DateTime(nullable: false));
            AlterColumn("ERT.IMPORTWSLOG", "UPDUSR_0", c => c.String(nullable: false, maxLength: 100, unicode: false));
            AlterColumn("ERT.IMPORTWSLOG", "UPDDAT_0", c => c.DateTime(nullable: false));
            AlterColumn("ERT.IMPORTWSLOG", "CREUSR_0", c => c.String(nullable: false, maxLength: 100, unicode: false));
            AlterColumn("ERT.IMPORTWSLOG", "CREDAT_0", c => c.DateTime(nullable: false));
            AlterColumn("ERT.IMPORTWSLOG", "WSCODE_0", c => c.String(nullable: false, maxLength: 200, unicode: false));
            AlterColumn("ERT.IMPORTWS", "UPDUSR_0", c => c.String(nullable: false, maxLength: 100, unicode: false));
            AlterColumn("ERT.IMPORTWS", "UPDDAT_0", c => c.DateTime(nullable: false));
            AlterColumn("ERT.IMPORTWS", "CREUSR_0", c => c.String(nullable: false, maxLength: 100, unicode: false));
            AlterColumn("ERT.IMPORTWS", "CREDAT_0", c => c.DateTime(nullable: false));
            AlterColumn("ERT.EXPORTWSLOG", "UPDUSR_0", c => c.String(nullable: false, maxLength: 100, unicode: false));
            AlterColumn("ERT.EXPORTWSLOG", "UPDDAT_0", c => c.DateTime(nullable: false));
            AlterColumn("ERT.EXPORTWSLOG", "CREUSR_0", c => c.String(nullable: false, maxLength: 100, unicode: false));
            AlterColumn("ERT.EXPORTWSLOG", "CREDAT_0", c => c.DateTime(nullable: false));
            AlterColumn("ERT.EXPORTWSLOG", "WSCODE_0", c => c.String(nullable: false, maxLength: 200, unicode: false));
            AlterColumn("ERT.EXPORTWS", "UPDDAT_0", c => c.DateTime(nullable: false));
            AlterColumn("ERT.EXPORTWS", "CREDAT_0", c => c.DateTime(nullable: false));
            CreateIndex("ERT.WEBSERVICE", "CODE_0", name: "IX_WEBSERVICE");
        }
    }
}
