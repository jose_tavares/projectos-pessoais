﻿// <auto-generated />
namespace DATA.EntityFramework
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.4")]
    public sealed partial class SeedDB : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(SeedDB));
        
        string IMigrationMetadata.Id
        {
            get { return "202205021721245_SeedDB"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
