﻿namespace DATA.EntityFramework
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class IMPORTWS : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "ERT.WSPARAM", newName: "WSFIELDCONFIG");
            DropIndex("ERT.DOCALL", "IX_DOCALL_2");
            DropIndex("ERT.DOCALL", "IX_DOCALL_1");
            DropIndex("ERT.EXPORTWS", "IX_EXPORTWS");
            DropIndex("ERT.IMPORTWS", "IX_IMPORTWS");
            DropIndex("ERT.STOJOULOG", "IX_STOJOULOG");
            DropIndex("ERT.WSFIELDCONFIG", "IX_WSPARAM");
            DropPrimaryKey("ERT.APIREQUEST");
            DropPrimaryKey("ERT.APP");
            DropPrimaryKey("ERT.CONFIGDES");
            DropPrimaryKey("ERT.CONFIGKEY");
            DropPrimaryKey("ERT.CONFIGVALUE");
            DropPrimaryKey("ERT.DOCALL");
            DropPrimaryKey("ERT.EXPORTWS");
            DropPrimaryKey("ERT.EXPORTWSLOG");
            DropPrimaryKey("ERT.IMPORTWS");
            DropPrimaryKey("ERT.IMPORTWSLOG");
            DropPrimaryKey("ERT.ITMLOCK");
            DropPrimaryKey("ERT.MESSAGE");
            DropPrimaryKey("ERT.MODULE");
            DropPrimaryKey("ERT.PROFILE");
            DropPrimaryKey("ERT.SCREEN");
            DropPrimaryKey("ERT.STODEFECT");
            DropPrimaryKey("ERT.STOJOU");
            DropPrimaryKey("ERT.STOJOULOG");
            DropPrimaryKey("ERT.USER");
            DropPrimaryKey("ERT.WEBSERVICE");
            DropPrimaryKey("ERT.WSFIELDCONFIG");
            CreateTable(
                "ERT.CONSUMPTION",
                c => new
                    {
                        ROWID = c.Int(nullable: false, identity: true),
                        CPY_0 = c.String(nullable: false, maxLength: 5, unicode: false),
                        STOFCY_0 = c.String(nullable: false, maxLength: 5, unicode: false),
                        MFGNUM_0 = c.String(nullable: false, maxLength: 20, unicode: false),
                        MFGTYP_0 = c.String(nullable: false, maxLength: 20, unicode: false),
                        MFGLIN_0 = c.Int(nullable: false),
                        BOMALT_0 = c.Short(nullable: false),
                        BOMQTY_0 = c.Decimal(nullable: false, precision: 28, scale: 13),
                        BOMSEQ_0 = c.Short(nullable: false),
                        BOMNUM_0 = c.String(nullable: false, maxLength: 50, unicode: false),
                        ITMREF_0 = c.String(nullable: false, maxLength: 20, unicode: false),
                        LOT_0 = c.String(nullable: false, maxLength: 15, unicode: false),
                        SLO_0 = c.String(nullable: false, maxLength: 5, unicode: false),
                        USENETQTY_0 = c.Decimal(nullable: false, precision: 28, scale: 13),
                        USELOC_0 = c.String(nullable: false, maxLength: 10, unicode: false),
                        USELOCTYP_0 = c.String(nullable: false, maxLength: 5, unicode: false),
                        USESTA_0 = c.String(nullable: false, maxLength: 3, unicode: false),
                        USESTU_0 = c.String(nullable: false, maxLength: 3, unicode: false),
                        BOXID_0 = c.Int(nullable: false),
                        SLOORI_0 = c.String(nullable: false, maxLength: 50, unicode: false),
                        VCRNUMDES_0 = c.String(nullable: false, maxLength: 200, unicode: false),
                        SCANCODE_0 = c.Int(nullable: false),
                        MACHINE_0 = c.String(nullable: false, maxLength: 20, unicode: false),
                        MACNAM_0 = c.String(nullable: false, maxLength: 200, unicode: false),
                        OPERATION_0 = c.String(nullable: false, maxLength: 20, unicode: false),
                        HOSTNAM_0 = c.String(nullable: false, maxLength: 255, unicode: false),
                        REGFLG_0 = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "((1))")
                                },
                            }),
                        CREDAT_0 = c.DateTime(nullable: false, storeType: "date",
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        CREUSR_0 = c.String(nullable: false, maxLength: 50, unicode: false),
                        UPDDAT_0 = c.DateTime(nullable: false, storeType: "date",
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        UPDUSR_0 = c.String(nullable: false, maxLength: 50, unicode: false),
                        CREDATTIM_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        UPDDATTIM_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.ROWID)
                .Index(t => new { t.MFGNUM_0, t.OPERATION_0, t.REGFLG_0 }, name: "IX_CONSUMPTION")
                .Index(t => t.SLOORI_0, name: "IX_CONSUMPTION_1");
            
            CreateTable(
                "ERT.IMPORTWSDATA",
                c => new
                    {
                        ROWID = c.Int(nullable: false, identity: true),
                        WSCODE_0 = c.String(nullable: false, maxLength: 100, unicode: false),
                        ID = c.Int(nullable: false),
                        XML_0 = c.String(nullable: false, maxLength: 4000, unicode: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "('')")
                                },
                            }),
                        OUTPUT_0 = c.String(nullable: false, maxLength: 4000, unicode: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "('')")
                                },
                            }),
                        OUTPUTWS_0 = c.String(nullable: false, maxLength: 4000, unicode: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "('')")
                                },
                            }),
                        VCRNUM_0 = c.String(nullable: false, maxLength: 4000, unicode: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "('')")
                                },
                            }),
                        WSUSR_0 = c.String(nullable: false, maxLength: 5, unicode: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "('')")
                                },
                            }),
                        ATTEMPTS_0 = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "((0))")
                                },
                            }),
                        SUCCESSFLG_0 = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "((1))")
                                },
                            }),
                        IMPORTFLG_0 = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "((2))")
                                },
                            }),
                        REGFLG_0 = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "((1))")
                                },
                            }),
                        FAILFLG_0 = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "((1))")
                                },
                            }),
                        CREDAT_0 = c.DateTime(nullable: false, storeType: "date",
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        CREUSR_0 = c.String(nullable: false, maxLength: 5, unicode: false),
                        UPDDAT_0 = c.DateTime(nullable: false, storeType: "date",
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        UPDUSR_0 = c.String(nullable: false, maxLength: 5, unicode: false),
                        CREDATTIM_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        UPDDATTIM_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.ROWID)
                .Index(t => new { t.WSCODE_0, t.ID }, name: "IX_IMPORTWSDATA");
            
            CreateTable(
                "ERT.MFGNUMZMULTI",
                c => new
                    {
                        ROWID = c.Int(nullable: false, identity: true),
                        STOFCY_0 = c.String(nullable: false, maxLength: 5, unicode: false),
                        OPERATION_0 = c.Int(nullable: false),
                        MFGNUMORI_0 = c.String(nullable: false, maxLength: 50, unicode: false),
                        MFGNUM_0 = c.String(nullable: false, maxLength: 50, unicode: false),
                        REGFLG_0 = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "((1))")
                                },
                            }),
                        CREDAT_0 = c.DateTime(nullable: false, storeType: "date",
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        CREUSR_0 = c.String(nullable: false, maxLength: 5, unicode: false),
                        UPDDAT_0 = c.DateTime(nullable: false, storeType: "date",
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        UPDUSR_0 = c.String(nullable: false, maxLength: 5, unicode: false),
                        CREDATTIM_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        UPDDATTIM_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.ROWID)
                .Index(t => new { t.MFGNUMORI_0, t.MFGNUM_0 }, unique: true, name: "IX_MFGNUMZMULTI");
            
            CreateTable(
                "ERT.PRODUCTION",
                c => new
                    {
                        ROWID = c.Int(nullable: false, identity: true),
                        CPY_0 = c.String(nullable: false, maxLength: 5, unicode: false),
                        STOFCY_0 = c.String(nullable: false, maxLength: 5, unicode: false),
                        MFGNUM_0 = c.String(nullable: false, maxLength: 20, unicode: false),
                        MFGTYP_0 = c.String(nullable: false, maxLength: 20, unicode: false),
                        ITMREF_0 = c.String(nullable: false, maxLength: 20, unicode: false),
                        LOT_0 = c.String(nullable: false, maxLength: 15, unicode: false),
                        SLO_0 = c.String(nullable: false, maxLength: 5, unicode: false),
                        QTYPCUORI_0 = c.Decimal(nullable: false, precision: 28, scale: 13),
                        USENETQTY_0 = c.Decimal(nullable: false, precision: 28, scale: 13),
                        USEGROSSQTY_0 = c.Decimal(nullable: false, precision: 28, scale: 13),
                        DEFECT_0 = c.Int(nullable: false),
                        USELOC_0 = c.String(nullable: false, maxLength: 10, unicode: false),
                        USELOCTYP_0 = c.String(nullable: false, maxLength: 5, unicode: false),
                        USESTA_0 = c.String(nullable: false, maxLength: 3, unicode: false),
                        USESTU_0 = c.String(nullable: false, maxLength: 3, unicode: false),
                        CUSORDREF_0 = c.String(nullable: false, maxLength: 50, unicode: false),
                        VCRNUMDES_0 = c.String(nullable: false, maxLength: 200, unicode: false),
                        MACHINE_0 = c.String(nullable: false, maxLength: 20, unicode: false),
                        MACNAM_0 = c.String(nullable: false, maxLength: 200, unicode: false),
                        OPERATION_0 = c.String(nullable: false, maxLength: 20, unicode: false),
                        HOSTNAM_0 = c.String(nullable: false, maxLength: 255, unicode: false),
                        REGFLG_0 = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "((1))")
                                },
                            }),
                        CREDAT_0 = c.DateTime(nullable: false, storeType: "date",
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        CREUSR_0 = c.String(nullable: false, maxLength: 50, unicode: false),
                        UPDDAT_0 = c.DateTime(nullable: false, storeType: "date",
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        UPDUSR_0 = c.String(nullable: false, maxLength: 50, unicode: false),
                        CREDATTIM_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        UPDDATTIM_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.ROWID)
                .Index(t => new { t.MFGNUM_0, t.OPERATION_0, t.REGFLG_0 }, name: "IX_PRODUCTION")
                .Index(t => t.SLO_0, name: "IX_PRODUCTION_1");
            
            AddColumn("ERT.DOCALL", "PRODUCTIONID", c => c.Int(nullable: false));
            AddColumn("ERT.DOCALL", "AVAILFLG_0", c => c.Int(nullable: false));
            AddColumn("ERT.EXPORTWS", "PARAMBEGIN_0", c => c.String(nullable: false, maxLength: 1000, unicode: false));
            AddColumn("ERT.EXPORTWS", "KEYFLD_0", c => c.String(nullable: false, maxLength: 200, unicode: false));
            AddColumn("ERT.EXPORTWS", "DATEFLD_0", c => c.String(nullable: false, maxLength: 200, unicode: false));
            AddColumn("ERT.EXPORTWS", "MAXROWS_0", c => c.Int(nullable: false));
            AddColumn("ERT.IMPORTWS", "SQLTABLE_0", c => c.String(nullable: false, maxLength: 100, unicode: false));
            AddColumn("ERT.IMPORTWS", "MAXATTEMPTS_0", c => c.Int(nullable: false));
            AddColumn("ERT.IMPORTWS", "ENABFLG_0", c => c.Int(nullable: false));
            AddColumn("ERT.IMPORTWSLOG", "ID", c => c.Int(nullable: false));
            AddColumn("ERT.IMPORTWSLOG", "VCRNUM_0", c => c.String(nullable: false, maxLength: 4000, unicode: false));
            AddColumn("ERT.IMPORTWSLOG", "WSUSR_0", c => c.String(nullable: false, maxLength: 5, unicode: false));
            AddColumn("ERT.IMPORTWSLOG", "ATTEMPTS_0", c => c.Int(nullable: false));
            AddColumn("ERT.IMPORTWSLOG", "REGFLG_0", c => c.Int(nullable: false));
            AddColumn("ERT.IMPORTWSLOG", "FAILFLG_0", c => c.Int(nullable: false));
            AddColumn("ERT.IMPORTWSLOG", "IMPORTID", c => c.Int(nullable: false));
            AddColumn("ERT.STOJOU", "VCRNUMDES_0", c => c.String(nullable: false, maxLength: 200, unicode: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "(' ')")
                    },
                }));
            AddColumn("ERT.STOJOU", "SCANCODE_0", c => c.Int(nullable: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "((0))")
                    },
                }));
            AddColumn("ERT.STOJOU", "FAILFLG_0", c => c.Int(nullable: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "((1))")
                    },
                }));
            AddColumn("ERT.STOJOULOG", "VCRNUMDES_0", c => c.String(nullable: false, maxLength: 200, unicode: false));
            AddColumn("ERT.STOJOULOG", "SCANCODE_0", c => c.Int(nullable: false));
            AddColumn("ERT.STOJOULOG", "FAILFLG_0", c => c.Int(nullable: false));
            AddColumn("ERT.WEBSERVICE", "EXPORTFLG_0", c => c.Int(nullable: false));
            AddColumn("ERT.WSFIELDCONFIG", "FLDFLG_0", c => c.Int(nullable: false));
            AddColumn("ERT.WSFIELDCONFIG", "VALUEFLG_0", c => c.Int(nullable: false));
            AlterColumn("ERT.APIREQUEST", "ROWID", c => c.Int(nullable: false, identity: true));
            AlterColumn("ERT.APP", "ROWID", c => c.Int(nullable: false, identity: true));
            AlterColumn("ERT.CONFIGDES", "ROWID", c => c.Int(nullable: false, identity: true));
            AlterColumn("ERT.CONFIGKEY", "ROWID", c => c.Int(nullable: false, identity: true));
            AlterColumn("ERT.CONFIGVALUE", "ROWID", c => c.Int(nullable: false, identity: true));
            AlterColumn("ERT.DOCALL", "ROWID", c => c.Int(nullable: false, identity: true));
            AlterColumn("ERT.EXPORTWS", "ROWID", c => c.Int(nullable: false, identity: true));
            AlterColumn("ERT.EXPORTWSLOG", "ROWID", c => c.Int(nullable: false, identity: true));
            AlterColumn("ERT.IMPORTWS", "ROWID", c => c.Int(nullable: false, identity: true));
            AlterColumn("ERT.IMPORTWSLOG", "ROWID", c => c.Int(nullable: false, identity: true));
            AlterColumn("ERT.IMPORTWSLOG", "SUCCESSFLG_0", c => c.Int(nullable: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: "((1))", newValue: null)
                    },
                }));
            AlterColumn("ERT.ITMLOCK", "ROWID", c => c.Int(nullable: false, identity: true));
            AlterColumn("ERT.MESSAGE", "ROWID", c => c.Int(nullable: false, identity: true));
            AlterColumn("ERT.MODULE", "ROWID", c => c.Int(nullable: false, identity: true));
            AlterColumn("ERT.PROFILE", "ROWID", c => c.Int(nullable: false, identity: true));
            AlterColumn("ERT.SCREEN", "ROWID", c => c.Int(nullable: false, identity: true));
            AlterColumn("ERT.STODEFECT", "ROWID", c => c.Int(nullable: false, identity: true));
            AlterColumn("ERT.STOJOU", "ROWID", c => c.Int(nullable: false, identity: true));
            AlterColumn("ERT.STOJOULOG", "ROWID", c => c.Int(nullable: false, identity: true));
            AlterColumn("ERT.STOJOULOG", "XML_0", c => c.String(nullable: false, unicode: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: "('')", newValue: null)
                    },
                }));
            AlterColumn("ERT.STOJOULOG", "OUTPUTWS_0", c => c.String(nullable: false, unicode: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: "('')", newValue: null)
                    },
                }));
            AlterColumn("ERT.STOJOULOG", "VCRNUM_0", c => c.String(nullable: false, unicode: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: "('')", newValue: null)
                    },
                }));
            AlterColumn("ERT.STOJOULOG", "WSUSR_0", c => c.String(nullable: false, maxLength: 5, unicode: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: "('')", newValue: null)
                    },
                }));
            AlterColumn("ERT.STOJOULOG", "ATTEMPTS_0", c => c.Int(nullable: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: "((0))", newValue: null)
                    },
                }));
            AlterColumn("ERT.STOJOULOG", "SUCCESSFLG_0", c => c.Int(nullable: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: "((1))", newValue: null)
                    },
                }));
            AlterColumn("ERT.STOJOULOG", "IMPORTFLG_0", c => c.Int(nullable: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: "((2))", newValue: null)
                    },
                }));
            AlterColumn("ERT.STOJOULOG", "REGFLG_0", c => c.Int(nullable: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: "((1))", newValue: null)
                    },
                }));
            AlterColumn("ERT.STOJOULOG", "QTYPCU_0", c => c.Decimal(nullable: false, precision: 28, scale: 13,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: "((0))", newValue: null)
                    },
                }));
            AlterColumn("ERT.STOJOULOG", "LOC_0", c => c.String(nullable: false, maxLength: 10, unicode: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: "('')", newValue: null)
                    },
                }));
            AlterColumn("ERT.STOJOULOG", "LOCTYP_0", c => c.String(nullable: false, maxLength: 5, unicode: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: "('')", newValue: null)
                    },
                }));
            AlterColumn("ERT.STOJOULOG", "STA_0", c => c.String(nullable: false, maxLength: 3, unicode: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: "('')", newValue: null)
                    },
                }));
            AlterColumn("ERT.STOJOULOG", "STU_0", c => c.String(nullable: false, maxLength: 3, unicode: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: "('')", newValue: null)
                    },
                }));
            AlterColumn("ERT.STOJOULOG", "OBSERVATION_0", c => c.String(nullable: false, maxLength: 8000, unicode: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: "('')", newValue: null)
                    },
                }));
            AlterColumn("ERT.STOJOULOG", "STOJOUID", c => c.Int(nullable: false));
            AlterColumn("ERT.USER", "ROWID", c => c.Int(nullable: false, identity: true));
            AlterColumn("ERT.WEBSERVICE", "ROWID", c => c.Int(nullable: false, identity: true));
            AlterColumn("ERT.WSFIELDCONFIG", "ROWID", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("ERT.APIREQUEST", "ROWID");
            AddPrimaryKey("ERT.APP", "ROWID");
            AddPrimaryKey("ERT.CONFIGDES", "ROWID");
            AddPrimaryKey("ERT.CONFIGKEY", "ROWID");
            AddPrimaryKey("ERT.CONFIGVALUE", "ROWID");
            AddPrimaryKey("ERT.DOCALL", "ROWID");
            AddPrimaryKey("ERT.EXPORTWS", "ROWID");
            AddPrimaryKey("ERT.EXPORTWSLOG", "ROWID");
            AddPrimaryKey("ERT.IMPORTWS", "ROWID");
            AddPrimaryKey("ERT.IMPORTWSLOG", "ROWID");
            AddPrimaryKey("ERT.ITMLOCK", "ROWID");
            AddPrimaryKey("ERT.MESSAGE", "ROWID");
            AddPrimaryKey("ERT.MODULE", "ROWID");
            AddPrimaryKey("ERT.PROFILE", "ROWID");
            AddPrimaryKey("ERT.SCREEN", "ROWID");
            AddPrimaryKey("ERT.STODEFECT", "ROWID");
            AddPrimaryKey("ERT.STOJOU", "ROWID");
            AddPrimaryKey("ERT.STOJOULOG", "ROWID");
            AddPrimaryKey("ERT.USER", "ROWID");
            AddPrimaryKey("ERT.WEBSERVICE", "ROWID");
            AddPrimaryKey("ERT.WSFIELDCONFIG", "ROWID");
            CreateIndex("ERT.DOCALL", new[] { "DOCUMENT_0", "STOFCY_0", "ITMREF_0", "QTYPCU_0", "AVAILFLG_0" }, name: "IX_DOCALL_2");
            CreateIndex("ERT.DOCALL", new[] { "DOCUMENT_0", "AVAILFLG_0" }, name: "IX_DOCALL_1");
            CreateIndex("ERT.EXPORTWS", "WSCODE_0", unique: true, name: "IX_EXPORTWS");
            CreateIndex("ERT.IMPORTWS", "WSCODE_0", name: "IX_IMPORTWS");
            CreateIndex("ERT.STOJOU", new[] { "STOFCY_0", "ITMREF_0", "LOT_0", "SLO_0", "USELOC_0", "USESTA_0", "SUCCESSFLG_0", "IMPORTFLG_0", "FAILFLG_0" }, name: "IX_STOJOU");
            CreateIndex("ERT.STOJOU", "MFGNUM_0", name: "IX_STOJOU_1");
            CreateIndex("ERT.STOJOULOG", "STOJOUID", name: "IX_STOJOULOG");
            CreateIndex("ERT.WSFIELDCONFIG", new[] { "WSCODE_0", "POSITION_0" }, unique: true, name: "IX_WSPARAM");
            DropColumn("ERT.DOCALL", "USEFLG_0");
            DropColumn("ERT.EXPORTWS", "EXPORTWS_0");
            DropColumn("ERT.EXPORTWS", "TEMPTABLE_0");
            DropColumn("ERT.EXPORTWSLOG", "EXPORTWS_0");
            DropColumn("ERT.IMPORTWS", "IMPORTWS_0");
            DropColumn("ERT.IMPORTWS", "TABLE_0");
            DropColumn("ERT.IMPORTWSLOG", "IMPORTWS_0");
            DropColumn("ERT.IMPORTWSLOG", "WSNAM_0");
            DropColumn("ERT.IMPORTWSLOG", "REWRITEFLG_0");
            DropColumn("ERT.IMPORTWSLOG", "EXECUTIONTIME_0");
            DropColumn("ERT.USER", "GENDER_0");
            DropColumn("ERT.WEBSERVICE", "PARAMBEGIN_0");
            DropColumn("ERT.WEBSERVICE", "MAXROWS_0");
            DropColumn("ERT.WEBSERVICE", "KEYFLD_0");
            DropColumn("ERT.WEBSERVICE", "DATEFLD_0");
            DropColumn("ERT.WEBSERVICE", "ATTEMPTS_0");
            DropColumn("ERT.WSFIELDCONFIG", "TEXT_0");
        }
        
        public override void Down()
        {
            AddColumn("ERT.WSFIELDCONFIG", "TEXT_0", c => c.Int(nullable: false));
            AddColumn("ERT.WEBSERVICE", "ATTEMPTS_0", c => c.Int(nullable: false));
            AddColumn("ERT.WEBSERVICE", "DATEFLD_0", c => c.String(nullable: false, maxLength: 200, unicode: false));
            AddColumn("ERT.WEBSERVICE", "KEYFLD_0", c => c.String(nullable: false, maxLength: 200, unicode: false));
            AddColumn("ERT.WEBSERVICE", "MAXROWS_0", c => c.Int(nullable: false));
            AddColumn("ERT.WEBSERVICE", "PARAMBEGIN_0", c => c.String(nullable: false, maxLength: 8000, unicode: false));
            AddColumn("ERT.USER", "GENDER_0", c => c.Boolean(nullable: false));
            AddColumn("ERT.IMPORTWSLOG", "EXECUTIONTIME_0", c => c.Int(nullable: false));
            AddColumn("ERT.IMPORTWSLOG", "REWRITEFLG_0", c => c.Int(nullable: false));
            AddColumn("ERT.IMPORTWSLOG", "WSNAM_0", c => c.String(nullable: false, maxLength: 200, unicode: false));
            AddColumn("ERT.IMPORTWSLOG", "IMPORTWS_0", c => c.String(nullable: false, maxLength: 200, unicode: false));
            AddColumn("ERT.IMPORTWS", "TABLE_0", c => c.String(nullable: false, maxLength: 100, unicode: false));
            AddColumn("ERT.IMPORTWS", "IMPORTWS_0", c => c.String(nullable: false, maxLength: 100, unicode: false));
            AddColumn("ERT.EXPORTWSLOG", "EXPORTWS_0", c => c.String(nullable: false, maxLength: 200, unicode: false));
            AddColumn("ERT.EXPORTWS", "TEMPTABLE_0", c => c.String(nullable: false, maxLength: 200, unicode: false));
            AddColumn("ERT.EXPORTWS", "EXPORTWS_0", c => c.String(nullable: false, maxLength: 100, unicode: false));
            AddColumn("ERT.DOCALL", "USEFLG_0", c => c.Int(nullable: false));
            DropIndex("ERT.WSFIELDCONFIG", "IX_WSPARAM");
            DropIndex("ERT.STOJOULOG", "IX_STOJOULOG");
            DropIndex("ERT.STOJOU", "IX_STOJOU_1");
            DropIndex("ERT.STOJOU", "IX_STOJOU");
            DropIndex("ERT.PRODUCTION", "IX_PRODUCTION_1");
            DropIndex("ERT.PRODUCTION", "IX_PRODUCTION");
            DropIndex("ERT.MFGNUMZMULTI", "IX_MFGNUMZMULTI");
            DropIndex("ERT.IMPORTWSDATA", "IX_IMPORTWSDATA");
            DropIndex("ERT.IMPORTWS", "IX_IMPORTWS");
            DropIndex("ERT.EXPORTWS", "IX_EXPORTWS");
            DropIndex("ERT.DOCALL", "IX_DOCALL_1");
            DropIndex("ERT.DOCALL", "IX_DOCALL_2");
            DropIndex("ERT.CONSUMPTION", "IX_CONSUMPTION_1");
            DropIndex("ERT.CONSUMPTION", "IX_CONSUMPTION");
            DropPrimaryKey("ERT.WSFIELDCONFIG");
            DropPrimaryKey("ERT.WEBSERVICE");
            DropPrimaryKey("ERT.USER");
            DropPrimaryKey("ERT.STOJOULOG");
            DropPrimaryKey("ERT.STOJOU");
            DropPrimaryKey("ERT.STODEFECT");
            DropPrimaryKey("ERT.SCREEN");
            DropPrimaryKey("ERT.PROFILE");
            DropPrimaryKey("ERT.MODULE");
            DropPrimaryKey("ERT.MESSAGE");
            DropPrimaryKey("ERT.ITMLOCK");
            DropPrimaryKey("ERT.IMPORTWSLOG");
            DropPrimaryKey("ERT.IMPORTWS");
            DropPrimaryKey("ERT.EXPORTWSLOG");
            DropPrimaryKey("ERT.EXPORTWS");
            DropPrimaryKey("ERT.DOCALL");
            DropPrimaryKey("ERT.CONFIGVALUE");
            DropPrimaryKey("ERT.CONFIGKEY");
            DropPrimaryKey("ERT.CONFIGDES");
            DropPrimaryKey("ERT.APP");
            DropPrimaryKey("ERT.APIREQUEST");
            AlterColumn("ERT.WSFIELDCONFIG", "ROWID", c => c.Decimal(nullable: false, precision: 38, scale: 0, identity: true));
            AlterColumn("ERT.WEBSERVICE", "ROWID", c => c.Decimal(nullable: false, precision: 38, scale: 0, identity: true));
            AlterColumn("ERT.USER", "ROWID", c => c.Decimal(nullable: false, precision: 38, scale: 0, identity: true));
            AlterColumn("ERT.STOJOULOG", "STOJOUID", c => c.Decimal(nullable: false, precision: 38, scale: 0));
            AlterColumn("ERT.STOJOULOG", "OBSERVATION_0", c => c.String(nullable: false, maxLength: 8000, unicode: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "('')")
                    },
                }));
            AlterColumn("ERT.STOJOULOG", "STU_0", c => c.String(nullable: false, maxLength: 3, unicode: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "('')")
                    },
                }));
            AlterColumn("ERT.STOJOULOG", "STA_0", c => c.String(nullable: false, maxLength: 3, unicode: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "('')")
                    },
                }));
            AlterColumn("ERT.STOJOULOG", "LOCTYP_0", c => c.String(nullable: false, maxLength: 5, unicode: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "('')")
                    },
                }));
            AlterColumn("ERT.STOJOULOG", "LOC_0", c => c.String(nullable: false, maxLength: 10, unicode: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "('')")
                    },
                }));
            AlterColumn("ERT.STOJOULOG", "QTYPCU_0", c => c.Decimal(nullable: false, precision: 28, scale: 13,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "((0))")
                    },
                }));
            AlterColumn("ERT.STOJOULOG", "REGFLG_0", c => c.Int(nullable: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "((1))")
                    },
                }));
            AlterColumn("ERT.STOJOULOG", "IMPORTFLG_0", c => c.Int(nullable: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "((2))")
                    },
                }));
            AlterColumn("ERT.STOJOULOG", "SUCCESSFLG_0", c => c.Int(nullable: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "((1))")
                    },
                }));
            AlterColumn("ERT.STOJOULOG", "ATTEMPTS_0", c => c.Int(nullable: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "((0))")
                    },
                }));
            AlterColumn("ERT.STOJOULOG", "WSUSR_0", c => c.String(nullable: false, maxLength: 5, unicode: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "('')")
                    },
                }));
            AlterColumn("ERT.STOJOULOG", "VCRNUM_0", c => c.String(nullable: false, unicode: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "('')")
                    },
                }));
            AlterColumn("ERT.STOJOULOG", "OUTPUTWS_0", c => c.String(nullable: false, unicode: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "('')")
                    },
                }));
            AlterColumn("ERT.STOJOULOG", "XML_0", c => c.String(nullable: false, unicode: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "('')")
                    },
                }));
            AlterColumn("ERT.STOJOULOG", "ROWID", c => c.Decimal(nullable: false, precision: 38, scale: 0, identity: true));
            AlterColumn("ERT.STOJOU", "ROWID", c => c.Decimal(nullable: false, precision: 38, scale: 0, identity: true));
            AlterColumn("ERT.STODEFECT", "ROWID", c => c.Decimal(nullable: false, precision: 38, scale: 0, identity: true));
            AlterColumn("ERT.SCREEN", "ROWID", c => c.Decimal(nullable: false, precision: 38, scale: 0, identity: true));
            AlterColumn("ERT.PROFILE", "ROWID", c => c.Decimal(nullable: false, precision: 38, scale: 0, identity: true));
            AlterColumn("ERT.MODULE", "ROWID", c => c.Decimal(nullable: false, precision: 38, scale: 0, identity: true));
            AlterColumn("ERT.MESSAGE", "ROWID", c => c.Decimal(nullable: false, precision: 38, scale: 0, identity: true));
            AlterColumn("ERT.ITMLOCK", "ROWID", c => c.Decimal(nullable: false, precision: 38, scale: 0, identity: true));
            AlterColumn("ERT.IMPORTWSLOG", "SUCCESSFLG_0", c => c.Int(nullable: false,
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SqlDefaultValue",
                        new AnnotationValues(oldValue: null, newValue: "((1))")
                    },
                }));
            AlterColumn("ERT.IMPORTWSLOG", "ROWID", c => c.Decimal(nullable: false, precision: 38, scale: 0, identity: true));
            AlterColumn("ERT.IMPORTWS", "ROWID", c => c.Decimal(nullable: false, precision: 38, scale: 0, identity: true));
            AlterColumn("ERT.EXPORTWSLOG", "ROWID", c => c.Decimal(nullable: false, precision: 38, scale: 0, identity: true));
            AlterColumn("ERT.EXPORTWS", "ROWID", c => c.Decimal(nullable: false, precision: 38, scale: 0, identity: true));
            AlterColumn("ERT.DOCALL", "ROWID", c => c.Decimal(nullable: false, precision: 38, scale: 0, identity: true));
            AlterColumn("ERT.CONFIGVALUE", "ROWID", c => c.Decimal(nullable: false, precision: 38, scale: 0, identity: true));
            AlterColumn("ERT.CONFIGKEY", "ROWID", c => c.Decimal(nullable: false, precision: 38, scale: 0, identity: true));
            AlterColumn("ERT.CONFIGDES", "ROWID", c => c.Decimal(nullable: false, precision: 38, scale: 0, identity: true));
            AlterColumn("ERT.APP", "ROWID", c => c.Decimal(nullable: false, precision: 38, scale: 0, identity: true));
            AlterColumn("ERT.APIREQUEST", "ROWID", c => c.Decimal(nullable: false, precision: 38, scale: 0, identity: true));
            DropColumn("ERT.WSFIELDCONFIG", "VALUEFLG_0");
            DropColumn("ERT.WSFIELDCONFIG", "FLDFLG_0");
            DropColumn("ERT.WEBSERVICE", "EXPORTFLG_0");
            DropColumn("ERT.STOJOULOG", "FAILFLG_0");
            DropColumn("ERT.STOJOULOG", "SCANCODE_0");
            DropColumn("ERT.STOJOULOG", "VCRNUMDES_0");
            DropColumn("ERT.STOJOU", "FAILFLG_0",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SqlDefaultValue", "((1))" },
                });
            DropColumn("ERT.STOJOU", "SCANCODE_0",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SqlDefaultValue", "((0))" },
                });
            DropColumn("ERT.STOJOU", "VCRNUMDES_0",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SqlDefaultValue", "(' ')" },
                });
            DropColumn("ERT.IMPORTWSLOG", "IMPORTID");
            DropColumn("ERT.IMPORTWSLOG", "FAILFLG_0");
            DropColumn("ERT.IMPORTWSLOG", "REGFLG_0");
            DropColumn("ERT.IMPORTWSLOG", "ATTEMPTS_0");
            DropColumn("ERT.IMPORTWSLOG", "WSUSR_0");
            DropColumn("ERT.IMPORTWSLOG", "VCRNUM_0");
            DropColumn("ERT.IMPORTWSLOG", "ID");
            DropColumn("ERT.IMPORTWS", "ENABFLG_0");
            DropColumn("ERT.IMPORTWS", "MAXATTEMPTS_0");
            DropColumn("ERT.IMPORTWS", "SQLTABLE_0");
            DropColumn("ERT.EXPORTWS", "MAXROWS_0");
            DropColumn("ERT.EXPORTWS", "DATEFLD_0");
            DropColumn("ERT.EXPORTWS", "KEYFLD_0");
            DropColumn("ERT.EXPORTWS", "PARAMBEGIN_0");
            DropColumn("ERT.DOCALL", "AVAILFLG_0");
            DropColumn("ERT.DOCALL", "PRODUCTIONID");
            DropTable("ERT.PRODUCTION",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CREDAT_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "CREDATTIM_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "REGFLG_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "((1))" },
                        }
                    },
                    {
                        "UPDDAT_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "UPDDATTIM_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                });
            DropTable("ERT.MFGNUMZMULTI",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CREDAT_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "CREDATTIM_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "REGFLG_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "((1))" },
                        }
                    },
                    {
                        "UPDDAT_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "UPDDATTIM_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                });
            DropTable("ERT.IMPORTWSDATA",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "ATTEMPTS_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "((0))" },
                        }
                    },
                    {
                        "CREDAT_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "CREDATTIM_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "FAILFLG_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "((1))" },
                        }
                    },
                    {
                        "IMPORTFLG_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "((2))" },
                        }
                    },
                    {
                        "OUTPUT_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "('')" },
                        }
                    },
                    {
                        "OUTPUTWS_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "('')" },
                        }
                    },
                    {
                        "REGFLG_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "((1))" },
                        }
                    },
                    {
                        "SUCCESSFLG_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "((1))" },
                        }
                    },
                    {
                        "UPDDAT_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "UPDDATTIM_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "VCRNUM_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "('')" },
                        }
                    },
                    {
                        "WSUSR_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "('')" },
                        }
                    },
                    {
                        "XML_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "('')" },
                        }
                    },
                });
            DropTable("ERT.CONSUMPTION",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CREDAT_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "CREDATTIM_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "REGFLG_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "((1))" },
                        }
                    },
                    {
                        "UPDDAT_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "UPDDATTIM_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                });
            AddPrimaryKey("ERT.WSFIELDCONFIG", "ROWID");
            AddPrimaryKey("ERT.WEBSERVICE", "ROWID");
            AddPrimaryKey("ERT.USER", "ROWID");
            AddPrimaryKey("ERT.STOJOULOG", "ROWID");
            AddPrimaryKey("ERT.STOJOU", "ROWID");
            AddPrimaryKey("ERT.STODEFECT", "ROWID");
            AddPrimaryKey("ERT.SCREEN", "ROWID");
            AddPrimaryKey("ERT.PROFILE", "ROWID");
            AddPrimaryKey("ERT.MODULE", "ROWID");
            AddPrimaryKey("ERT.MESSAGE", "ROWID");
            AddPrimaryKey("ERT.ITMLOCK", "ROWID");
            AddPrimaryKey("ERT.IMPORTWSLOG", "ROWID");
            AddPrimaryKey("ERT.IMPORTWS", "ROWID");
            AddPrimaryKey("ERT.EXPORTWSLOG", "ROWID");
            AddPrimaryKey("ERT.EXPORTWS", "ROWID");
            AddPrimaryKey("ERT.DOCALL", "ROWID");
            AddPrimaryKey("ERT.CONFIGVALUE", "ROWID");
            AddPrimaryKey("ERT.CONFIGKEY", "ROWID");
            AddPrimaryKey("ERT.CONFIGDES", "ROWID");
            AddPrimaryKey("ERT.APP", "ROWID");
            AddPrimaryKey("ERT.APIREQUEST", "ROWID");
            CreateIndex("ERT.WSFIELDCONFIG", "WSCODE_0", name: "IX_WSPARAM");
            CreateIndex("ERT.STOJOULOG", "STOJOUID", name: "IX_STOJOULOG");
            CreateIndex("ERT.IMPORTWS", "IMPORTWS_0", unique: true, name: "IX_IMPORTWS");
            CreateIndex("ERT.EXPORTWS", "EXPORTWS_0", unique: true, name: "IX_EXPORTWS");
            CreateIndex("ERT.DOCALL", new[] { "DOCUMENT_0", "USEFLG_0" }, name: "IX_DOCALL_1");
            CreateIndex("ERT.DOCALL", new[] { "DOCUMENT_0", "STOFCY_0", "ITMREF_0", "QTYPCU_0", "USEFLG_0" }, name: "IX_DOCALL_2");
            RenameTable(name: "ERT.WSFIELDCONFIG", newName: "WSPARAM");
        }
    }
}
