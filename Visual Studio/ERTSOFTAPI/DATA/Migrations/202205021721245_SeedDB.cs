﻿namespace DATA.EntityFramework
{
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;

    public partial class SeedDB : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "ERT.APP",
                c => new
                    {
                        ROWID = c.Decimal(nullable: false, precision: 38, scale: 0, identity: true),
                        APP_0 = c.String(nullable: false, maxLength: 50, unicode: false),
                        DES_0 = c.String(nullable: false, maxLength: 1000, unicode: false),
                        DEVLANG_0 = c.String(nullable: false, maxLength: 400, unicode: false),
                        TYP_0 = c.String(nullable: false, maxLength: 400, unicode: false),
                        VRS_0 = c.String(nullable: false, maxLength: 50, unicode: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "('1.0.0')")
                                },
                            }),
                        ENABFLG_0 = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "((2))")
                                },
                            }),
                        CREDAT_0 = c.DateTime(nullable: false, storeType: "date",
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        CREUSR_0 = c.String(nullable: false, maxLength: 5, unicode: false),
                        UPDDAT_0 = c.DateTime(nullable: false, storeType: "date",
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        UPDUSR_0 = c.String(nullable: false, maxLength: 5, unicode: false),
                        CREDATTIM_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        UPDDATTIM_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.ROWID)
                .Index(t => t.APP_0, unique: true, name: "IX_APP");
            
            CreateTable(
                "ERT.CONFIGDES",
                c => new
                    {
                        ROWID = c.Decimal(nullable: false, precision: 38, scale: 0, identity: true),
                        CONFIGTYP_0 = c.String(nullable: false, maxLength: 100, unicode: false),
                        VALUE_0 = c.String(nullable: false, maxLength: 100, unicode: false),
                        VALUE_1 = c.String(nullable: false, maxLength: 100, unicode: false),
                        VALUE_2 = c.String(nullable: false, maxLength: 100, unicode: false),
                        VALUE_3 = c.String(nullable: false, maxLength: 100, unicode: false),
                        DESCRIPTION_0 = c.String(nullable: false, maxLength: 200, unicode: false),
                        CREDAT_0 = c.DateTime(nullable: false, storeType: "date",
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        CREUSR_0 = c.String(nullable: false, maxLength: 5, unicode: false),
                        UPDDAT_0 = c.DateTime(nullable: false, storeType: "date",
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        UPDUSR_0 = c.String(nullable: false, maxLength: 5, unicode: false),
                        CREDATTIM_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        UPDDATTIM_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.ROWID);
            
            CreateTable(
                "ERT.CONFIGKEY",
                c => new
                    {
                        ROWID = c.Decimal(nullable: false, precision: 38, scale: 0, identity: true),
                        CONFIGTYP_0 = c.String(nullable: false, maxLength: 100, unicode: false),
                        KEYFLG_0 = c.Int(nullable: false),
                        KEYFLG_1 = c.Int(nullable: false),
                        KEYFLG_2 = c.Int(nullable: false),
                        KEYFLG_3 = c.Int(nullable: false),
                        CREDAT_0 = c.DateTime(nullable: false, storeType: "date",
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        CREUSR_0 = c.String(nullable: false, maxLength: 5, unicode: false),
                        UPDDAT_0 = c.DateTime(nullable: false, storeType: "date",
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        UPDUSR_0 = c.String(nullable: false, maxLength: 5, unicode: false),
                        CREDATTIM_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        UPDDATTIM_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.ROWID);
            
            CreateTable(
                "ERT.CONFIGVALUE",
                c => new
                    {
                        ROWID = c.Decimal(nullable: false, precision: 38, scale: 0, identity: true),
                        CONFIGTYP_0 = c.String(nullable: false, maxLength: 100, unicode: false),
                        VALUE_0 = c.String(nullable: false, maxLength: 100, unicode: false),
                        VALUE_1 = c.String(nullable: false, maxLength: 100, unicode: false),
                        VALUE_2 = c.String(nullable: false, maxLength: 100, unicode: false),
                        VALUE_3 = c.String(nullable: false, maxLength: 100, unicode: false),
                        ENABFLG_0 = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "((2))")
                                },
                            }),
                        CREDAT_0 = c.DateTime(nullable: false, storeType: "date",
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        CREUSR_0 = c.String(nullable: false, maxLength: 5, unicode: false),
                        UPDDAT_0 = c.DateTime(nullable: false, storeType: "date",
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        UPDUSR_0 = c.String(nullable: false, maxLength: 5, unicode: false),
                        CREDATTIM_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        UPDDATTIM_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.ROWID);
            
            CreateTable(
                "ERT.DOCALL",
                c => new
                    {
                        ROWID = c.Decimal(nullable: false, precision: 38, scale: 0, identity: true),
                        STOFCY_0 = c.String(nullable: false, maxLength: 5, unicode: false),
                        DOCUMENT_0 = c.String(nullable: false, maxLength: 20, unicode: false),
                        ITMREF_0 = c.String(nullable: false, maxLength: 20, unicode: false),
                        LOT_0 = c.String(nullable: false, maxLength: 50, unicode: false),
                        SLO_0 = c.String(nullable: false, maxLength: 5, unicode: false),
                        LOC_0 = c.String(nullable: false, maxLength: 10, unicode: false),
                        STA_0 = c.String(nullable: false, maxLength: 3, unicode: false),
                        QTYPCUORI_0 = c.Decimal(nullable: false, precision: 28, scale: 13),
                        QTYPCU_0 = c.Decimal(nullable: false, precision: 28, scale: 13),
                        USEFLG_0 = c.Int(nullable: false),
                        CREDAT_0 = c.DateTime(nullable: false, storeType: "date",
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        CREUSR_0 = c.String(nullable: false, maxLength: 5, unicode: false),
                        UPDDAT_0 = c.DateTime(nullable: false, storeType: "date",
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        UPDUSR_0 = c.String(nullable: false, maxLength: 5, unicode: false),
                        CREDATTIM_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        UPDDATTIM_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.ROWID)
                .Index(t => new { t.DOCUMENT_0, t.STOFCY_0, t.ITMREF_0, t.QTYPCU_0, t.USEFLG_0 }, name: "IX_DOCALL_2")
                .Index(t => new { t.DOCUMENT_0, t.USEFLG_0 }, name: "IX_DOCALL_1");
            
            CreateTable(
                "ERT.EXPORTWS",
                c => new
                    {
                        ROWID = c.Decimal(nullable: false, precision: 38, scale: 0, identity: true),
                        EXPORTWS_0 = c.String(nullable: false, maxLength: 100, unicode: false),
                        DES_0 = c.String(nullable: false, maxLength: 8000, unicode: false),
                        WSCODE_0 = c.String(nullable: false, maxLength: 100, unicode: false),
                        SELECTQUERY_0 = c.String(nullable: false, maxLength: 8000, unicode: false),
                        DELETEQUERY_0 = c.String(nullable: false, maxLength: 8000, unicode: false),
                        MAINTABLE_0 = c.String(nullable: false, maxLength: 200, unicode: false),
                        TEMPTABLE_0 = c.String(nullable: false, maxLength: 200, unicode: false),
                        LOGTABLE_0 = c.String(nullable: false, maxLength: 200, unicode: false),
                        WRITEFREQUENCY_0 = c.Int(nullable: false),
                        REWRITEFREQUENCY_0 = c.Int(nullable: false),
                        LASTWRITEDATE_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "('1900-01-01')")
                                },
                            }),
                        NEXTWRITEDATE_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "('1900-01-01')")
                                },
                            }),
                        REWRITEFLG_0 = c.Int(nullable: false),
                        LASTREWRITEDATE_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "('1900-01-01')")
                                },
                            }),
                        NEXTREWRITEDATE_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "('1900-01-01')")
                                },
                            }),
                        ENABFLG_0 = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "((2))")
                                },
                            }),
                        CREDAT_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        CREUSR_0 = c.String(nullable: false, maxLength: 5, unicode: false),
                        UPDDAT_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        UPDUSR_0 = c.String(nullable: false, maxLength: 5, unicode: false),
                        CREDATTIM_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        UPDDATTIM_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.ROWID)
                .Index(t => t.EXPORTWS_0, unique: true, name: "IX_EXPORTWS");
            
            CreateTable(
                "ERT.EXPORTWSLOG",
                c => new
                    {
                        ROWID = c.Decimal(nullable: false, precision: 38, scale: 0, identity: true),
                        EXPORTWS_0 = c.String(nullable: false, maxLength: 200, unicode: false),
                        WSCODE_0 = c.String(nullable: false, maxLength: 200, unicode: false),
                        WSNAM_0 = c.String(nullable: false, maxLength: 200, unicode: false),
                        REWRITEFLG_0 = c.Int(nullable: false),
                        OUTPUT_0 = c.String(nullable: false, maxLength: 4000, unicode: false),
                        OUTPUTWS_0 = c.String(nullable: false, maxLength: 4000, unicode: false),
                        XML_0 = c.String(nullable: false, maxLength: 4000, unicode: false),
                        INSERTEDROWS_0 = c.Int(nullable: false),
                        UPDATEDROWS_0 = c.Int(nullable: false),
                        DELETEDROWS_0 = c.Int(nullable: false),
                        EXECUTIONTIME_0 = c.Int(nullable: false),
                        SUCCESSFLG_0 = c.Int(nullable: false),
                        CREDAT_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        CREUSR_0 = c.String(nullable: false, maxLength: 100, unicode: false),
                        UPDDAT_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        UPDUSR_0 = c.String(nullable: false, maxLength: 100, unicode: false),
                        CREDATTIM_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        UPDDATTIM_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.ROWID);
            
            CreateTable(
                "ERT.IMPORTWS",
                c => new
                    {
                        ROWID = c.Decimal(nullable: false, precision: 38, scale: 0, identity: true),
                        IMPORTWS_0 = c.String(nullable: false, maxLength: 100, unicode: false),
                        DES_0 = c.String(nullable: false, maxLength: 8000, unicode: false),
                        WSCODE_0 = c.String(nullable: false, maxLength: 100, unicode: false),
                        TABLE_0 = c.String(nullable: false, maxLength: 100, unicode: false),
                        CREDAT_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        CREUSR_0 = c.String(nullable: false, maxLength: 100, unicode: false),
                        UPDDAT_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        UPDUSR_0 = c.String(nullable: false, maxLength: 100, unicode: false),
                        CREDATTIM_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        UPDDATTIM_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.ROWID)
                .Index(t => t.IMPORTWS_0, unique: true, name: "IX_IMPORTWS");
            
            CreateTable(
                "ERT.IMPORTWSLOG",
                c => new
                    {
                        ROWID = c.Decimal(nullable: false, precision: 38, scale: 0, identity: true),
                        IMPORTWS_0 = c.String(nullable: false, maxLength: 200, unicode: false),
                        WSCODE_0 = c.String(nullable: false, maxLength: 200, unicode: false),
                        WSNAM_0 = c.String(nullable: false, maxLength: 200, unicode: false),
                        REWRITEFLG_0 = c.Int(nullable: false),
                        OUTPUT_0 = c.String(nullable: false, maxLength: 4000, unicode: false),
                        OUTPUTWS_0 = c.String(nullable: false, maxLength: 4000, unicode: false),
                        XML_0 = c.String(nullable: false, maxLength: 4000, unicode: false),
                        EXECUTIONTIME_0 = c.Int(nullable: false),
                        SUCCESSFLG_0 = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "((1))")
                                },
                            }),
                        IMPORTFLG_0 = c.Int(nullable: false),
                        CREDAT_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        CREUSR_0 = c.String(nullable: false, maxLength: 100, unicode: false),
                        UPDDAT_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        UPDUSR_0 = c.String(nullable: false, maxLength: 100, unicode: false),
                        CREDATTIM_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        UPDDATTIM_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.ROWID);
            
            CreateTable(
                "ERT.ITMLOCK",
                c => new
                    {
                        ROWID = c.Decimal(nullable: false, precision: 38, scale: 0, identity: true),
                        ITMREF_0 = c.String(nullable: false, maxLength: 20, unicode: false),
                        CREDAT_0 = c.DateTime(nullable: false, storeType: "date",
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        CREUSR_0 = c.String(nullable: false, maxLength: 5, unicode: false),
                        UPDDAT_0 = c.DateTime(nullable: false, storeType: "date",
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        UPDUSR_0 = c.String(nullable: false, maxLength: 5, unicode: false),
                        CREDATTIM_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        UPDDATTIM_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.ROWID)
                .Index(t => t.ITMREF_0, unique: true, name: "IX_ITMLOCK");
            
            CreateTable(
                "ERT.MODULE",
                c => new
                    {
                        ROWID = c.Decimal(nullable: false, precision: 38, scale: 0, identity: true),
                        APP_0 = c.String(nullable: false, maxLength: 50, unicode: false),
                        MODULE_0 = c.String(nullable: false, maxLength: 50, unicode: false),
                        NAM_0 = c.String(nullable: false, maxLength: 200, unicode: false),
                        DES_0 = c.String(nullable: false, maxLength: 1000, unicode: false),
                        ENABFLG_0 = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "((2))")
                                },
                            }),
                        CREDAT_0 = c.DateTime(nullable: false, storeType: "date",
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        CREUSR_0 = c.String(nullable: false, maxLength: 5, unicode: false),
                        UPDDAT_0 = c.DateTime(nullable: false, storeType: "date",
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        UPDUSR_0 = c.String(nullable: false, maxLength: 5, unicode: false),
                        CREDATTIM_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        UPDDATTIM_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.ROWID)
                .Index(t => new { t.APP_0, t.MODULE_0 }, unique: true, name: "IX_MODULE");
            
            CreateTable(
                "ERT.PROFILE",
                c => new
                    {
                        ROWID = c.Decimal(nullable: false, precision: 38, scale: 0, identity: true),
                        PROFILE_0 = c.String(nullable: false, maxLength: 50, unicode: false),
                        NAM_0 = c.String(nullable: false, maxLength: 50, unicode: false),
                        ENABFLG_0 = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "((2))")
                                },
                            }),
                        CREDAT_0 = c.DateTime(nullable: false, storeType: "date",
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        CREUSR_0 = c.String(nullable: false, maxLength: 5, unicode: false),
                        UPDDAT_0 = c.DateTime(nullable: false, storeType: "date",
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        UPDUSR_0 = c.String(nullable: false, maxLength: 5, unicode: false),
                        CREDATTIM_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        UPDDATTIM_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.ROWID)
                .Index(t => t.PROFILE_0, unique: true, name: "IX_PROFILE");
            
            CreateTable(
                "ERT.SCREEN",
                c => new
                    {
                        ROWID = c.Decimal(nullable: false, precision: 38, scale: 0, identity: true),
                        APP_0 = c.String(nullable: false, maxLength: 50, unicode: false),
                        MODULE_0 = c.String(nullable: false, maxLength: 50, unicode: false),
                        SCREEN_0 = c.String(nullable: false, maxLength: 50, unicode: false),
                        DISPLAY_0 = c.String(nullable: false, maxLength: 40, unicode: false),
                        DES_0 = c.String(nullable: false, maxLength: 400, unicode: false),
                        TECDOCUMENT_0 = c.String(nullable: false, maxLength: 1000, unicode: false),
                        FUNCDOCUMENT_0 = c.String(nullable: false, maxLength: 1000, unicode: false),
                        MAINSHOWFLG_0 = c.Int(nullable: false),
                        ENABFLG_0 = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "((2))")
                                },
                            }),
                        CREDAT_0 = c.DateTime(nullable: false, storeType: "date",
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        CREUSR_0 = c.String(nullable: false, maxLength: 5, unicode: false),
                        UPDDAT_0 = c.DateTime(nullable: false, storeType: "date",
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        UPDUSR_0 = c.String(nullable: false, maxLength: 5, unicode: false),
                        CREDATTIM_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        UPDDATTIM_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.ROWID)
                .Index(t => new { t.APP_0, t.MODULE_0, t.SCREEN_0 }, unique: true, name: "IX_SCREEN");
            
            CreateTable(
                "ERT.STODEFECT",
                c => new
                    {
                        ROWID = c.Decimal(nullable: false, precision: 38, scale: 0, identity: true),
                        MFGNUM_0 = c.String(nullable: false, maxLength: 20, unicode: false),
                        ITMREF_0 = c.String(nullable: false, maxLength: 20, unicode: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "('')")
                                },
                            }),
                        LOT_0 = c.String(nullable: false, maxLength: 10, unicode: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "('')")
                                },
                            }),
                        SLO_0 = c.String(nullable: false, maxLength: 5, unicode: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "('')")
                                },
                            }),
                        DEFECT_0 = c.String(nullable: false, maxLength: 3, unicode: false),
                        DEFECTTYP_0 = c.String(nullable: false, maxLength: 50, unicode: false),
                        QTYPCU_0 = c.Decimal(nullable: false, precision: 28, scale: 2),
                        BONUSABLEFLG_0 = c.Int(nullable: false),
                        MACHINE_0 = c.String(nullable: false, maxLength: 20, unicode: false),
                        OPERATION_0 = c.String(nullable: false, maxLength: 20, unicode: false),
                        CREDAT_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        CREUSR_0 = c.String(nullable: false, maxLength: 5, unicode: false),
                        UPDDAT_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        UPDUSR_0 = c.String(nullable: false, maxLength: 5, unicode: false),
                        CREDATTIM_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        UPDDATTIM_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.ROWID)
                .Index(t => new { t.MFGNUM_0, t.ITMREF_0, t.LOT_0, t.SLO_0, t.MACHINE_0, t.OPERATION_0 }, name: "IX_STODEFECT");
            
            CreateTable(
                "ERT.STOJOU",
                c => new
                    {
                        ROWID = c.Decimal(nullable: false, precision: 38, scale: 0, identity: true),
                        WSCODE_0 = c.String(nullable: false, maxLength: 50, unicode: false),
                        CPY_0 = c.String(nullable: false, maxLength: 5, unicode: false),
                        STOFCY_0 = c.String(nullable: false, maxLength: 5, unicode: false),
                        DESSTOFCY_0 = c.String(nullable: false, maxLength: 5, unicode: false),
                        MFGNUM_0 = c.String(nullable: false, maxLength: 20, unicode: false),
                        MFGTYP_0 = c.String(nullable: false, maxLength: 20, unicode: false),
                        MFGSTA_0 = c.Int(nullable: false),
                        MFGLIN_0 = c.Int(nullable: false),
                        BOMALT_0 = c.Short(nullable: false),
                        BOMQTY_0 = c.Decimal(nullable: false, precision: 28, scale: 13),
                        BOMSEQ_0 = c.Short(nullable: false),
                        BOMNUM_0 = c.String(nullable: false, maxLength: 50, unicode: false),
                        ITMREF_0 = c.String(nullable: false, maxLength: 20, unicode: false),
                        LOT_0 = c.String(nullable: false, maxLength: 15, unicode: false),
                        SLO_0 = c.String(nullable: false, maxLength: 5, unicode: false),
                        QTYPCUORI_0 = c.Decimal(nullable: false, precision: 28, scale: 13),
                        USENETQTY_0 = c.Decimal(nullable: false, precision: 28, scale: 13),
                        USEGROSSQTY_0 = c.Decimal(nullable: false, precision: 28, scale: 13),
                        DEFECT_0 = c.Int(nullable: false),
                        USELOC_0 = c.String(nullable: false, maxLength: 10, unicode: false),
                        USELOCTYP_0 = c.String(nullable: false, maxLength: 5, unicode: false),
                        DESLOC_0 = c.String(nullable: false, maxLength: 10, unicode: false),
                        DESLOCTYP_0 = c.String(nullable: false, maxLength: 5, unicode: false),
                        USESTA_0 = c.String(nullable: false, maxLength: 3, unicode: false),
                        DESSTA_0 = c.String(nullable: false, maxLength: 3, unicode: false),
                        USESTU_0 = c.String(nullable: false, maxLength: 3, unicode: false),
                        BOXID_0 = c.Int(nullable: false),
                        CUSORDREF_0 = c.String(nullable: false, maxLength: 50, unicode: false),
                        YTRANS_0 = c.String(nullable: false, maxLength: 50, unicode: false),
                        YDAT_0 = c.String(nullable: false, maxLength: 50, unicode: false),
                        TRSTYP_0 = c.String(nullable: false, maxLength: 50, unicode: false),
                        SLOORI_0 = c.String(nullable: false, maxLength: 50, unicode: false),
                        VCRNUMORI_0 = c.String(nullable: false, maxLength: 50, unicode: false),
                        MACHINE_0 = c.String(nullable: false, maxLength: 20, unicode: false),
                        MACNAM_0 = c.String(nullable: false, maxLength: 200, unicode: false),
                        OPERATION_0 = c.String(nullable: false, maxLength: 20, unicode: false),
                        HOSTNAM_0 = c.String(nullable: false, maxLength: 255, unicode: false),
                        XML_0 = c.String(nullable: false, unicode: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "('')")
                                },
                            }),
                        OUTPUTWS_0 = c.String(nullable: false, unicode: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "('')")
                                },
                            }),
                        VCRNUM_0 = c.String(nullable: false, unicode: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "('')")
                                },
                            }),
                        WSUSR_0 = c.String(nullable: false, maxLength: 5, unicode: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "('')")
                                },
                            }),
                        QTYPCU_0 = c.Decimal(nullable: false, precision: 28, scale: 13,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "((0))")
                                },
                            }),
                        ATTEMPTS_0 = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "((0))")
                                },
                            }),
                        SUCCESSFLG_0 = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "((1))")
                                },
                            }),
                        IMPORTFLG_0 = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "((2))")
                                },
                            }),
                        REGFLG_0 = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "((1))")
                                },
                            }),
                        LOC_0 = c.String(nullable: false, maxLength: 10, unicode: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "('')")
                                },
                            }),
                        LOCTYP_0 = c.String(nullable: false, maxLength: 5, unicode: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "('')")
                                },
                            }),
                        STA_0 = c.String(nullable: false, maxLength: 3, unicode: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "('')")
                                },
                            }),
                        STU_0 = c.String(nullable: false, maxLength: 3, unicode: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "('')")
                                },
                            }),
                        OBSERVATION_0 = c.String(nullable: false, maxLength: 8000, unicode: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "('')")
                                },
                            }),
                        CREDAT_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        CREUSR_0 = c.String(nullable: false, maxLength: 50, unicode: false),
                        UPDDAT_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        UPDUSR_0 = c.String(nullable: false, maxLength: 50, unicode: false),
                        CREDATTIM_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        UPDDATTIM_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.ROWID);
            
            CreateTable(
                "ERT.STOJOULOG",
                c => new
                    {
                        ROWID = c.Decimal(nullable: false, precision: 38, scale: 0, identity: true),
                        WSCODE_0 = c.String(nullable: false, maxLength: 50, unicode: false),
                        CPY_0 = c.String(nullable: false, maxLength: 5, unicode: false),
                        STOFCY_0 = c.String(nullable: false, maxLength: 5, unicode: false),
                        DESSTOFCY_0 = c.String(nullable: false, maxLength: 5, unicode: false),
                        MFGNUM_0 = c.String(nullable: false, maxLength: 20, unicode: false),
                        MFGTYP_0 = c.String(nullable: false, maxLength: 20, unicode: false),
                        MFGSTA_0 = c.Int(nullable: false),
                        MFGLIN_0 = c.Int(nullable: false),
                        BOMALT_0 = c.Short(nullable: false),
                        BOMQTY_0 = c.Decimal(nullable: false, precision: 28, scale: 13),
                        BOMSEQ_0 = c.Short(nullable: false),
                        BOMNUM_0 = c.String(nullable: false, maxLength: 50, unicode: false),
                        ITMREF_0 = c.String(nullable: false, maxLength: 20, unicode: false),
                        LOT_0 = c.String(nullable: false, maxLength: 15, unicode: false),
                        SLO_0 = c.String(nullable: false, maxLength: 5, unicode: false),
                        QTYPCUORI_0 = c.Decimal(nullable: false, precision: 28, scale: 13),
                        USENETQTY_0 = c.Decimal(nullable: false, precision: 28, scale: 13),
                        USEGROSSQTY_0 = c.Decimal(nullable: false, precision: 28, scale: 13),
                        DEFECT_0 = c.Int(nullable: false),
                        USELOC_0 = c.String(nullable: false, maxLength: 10, unicode: false),
                        USELOCTYP_0 = c.String(nullable: false, maxLength: 5, unicode: false),
                        DESLOC_0 = c.String(nullable: false, maxLength: 10, unicode: false),
                        DESLOCTYP_0 = c.String(nullable: false, maxLength: 5, unicode: false),
                        USESTA_0 = c.String(nullable: false, maxLength: 3, unicode: false),
                        DESSTA_0 = c.String(nullable: false, maxLength: 3, unicode: false),
                        USESTU_0 = c.String(nullable: false, maxLength: 3, unicode: false),
                        BOXID_0 = c.Int(nullable: false),
                        CUSORDREF_0 = c.String(nullable: false, maxLength: 50, unicode: false),
                        YTRANS_0 = c.String(nullable: false, maxLength: 50, unicode: false),
                        YDAT_0 = c.String(nullable: false, maxLength: 50, unicode: false),
                        TRSTYP_0 = c.String(nullable: false, maxLength: 50, unicode: false),
                        SLOORI_0 = c.String(nullable: false, maxLength: 50, unicode: false),
                        VCRNUMORI_0 = c.String(nullable: false, maxLength: 50, unicode: false),
                        MACHINE_0 = c.String(nullable: false, maxLength: 20, unicode: false),
                        MACNAM_0 = c.String(nullable: false, maxLength: 200, unicode: false),
                        OPERATION_0 = c.String(nullable: false, maxLength: 20, unicode: false),
                        HOSTNAM_0 = c.String(nullable: false, maxLength: 255, unicode: false),
                        XML_0 = c.String(nullable: false, unicode: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "('')")
                                },
                            }),
                        OUTPUTWS_0 = c.String(nullable: false, unicode: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "('')")
                                },
                            }),
                        VCRNUM_0 = c.String(nullable: false, unicode: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "('')")
                                },
                            }),
                        WSUSR_0 = c.String(nullable: false, maxLength: 5, unicode: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "('')")
                                },
                            }),
                        QTYPCU_0 = c.Decimal(nullable: false, precision: 28, scale: 13,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "((0))")
                                },
                            }),
                        ATTEMPTS_0 = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "((0))")
                                },
                            }),
                        SUCCESSFLG_0 = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "((1))")
                                },
                            }),
                        IMPORTFLG_0 = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "((2))")
                                },
                            }),
                        REGFLG_0 = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "((1))")
                                },
                            }),
                        LOC_0 = c.String(nullable: false, maxLength: 10, unicode: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "('')")
                                },
                            }),
                        LOCTYP_0 = c.String(nullable: false, maxLength: 5, unicode: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "('')")
                                },
                            }),
                        STA_0 = c.String(nullable: false, maxLength: 3, unicode: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "('')")
                                },
                            }),
                        STU_0 = c.String(nullable: false, maxLength: 3, unicode: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "('')")
                                },
                            }),
                        OBSERVATION_0 = c.String(nullable: false, maxLength: 8000, unicode: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "('')")
                                },
                            }),
                        STOJOUID = c.Decimal(nullable: false, precision: 38, scale: 0),
                        CREDAT_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        CREUSR_0 = c.String(nullable: false, maxLength: 50, unicode: false),
                        UPDDAT_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        UPDUSR_0 = c.String(nullable: false, maxLength: 50, unicode: false),
                        CREDATTIM_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        UPDDATTIM_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.ROWID)
                .Index(t => t.STOJOUID, name: "IX_STOJOULOG");
            
            CreateTable(
                "ERT.USER",
                c => new
                    {
                        ROWID = c.Decimal(nullable: false, precision: 38, scale: 0, identity: true),
                        USR_0 = c.String(nullable: false, maxLength: 50, unicode: false),
                        USERNAM_0 = c.String(nullable: false, maxLength: 50, unicode: false),
                        PASSWORD_0 = c.String(nullable: false, maxLength: 2000, unicode: false),
                        FULLNAM_0 = c.String(nullable: false, maxLength: 200, unicode: false),
                        FIRSTNAM_0 = c.String(nullable: false, maxLength: 100, unicode: false),
                        LASTNAM_0 = c.String(nullable: false, maxLength: 100, unicode: false),
                        EMAIL_0 = c.String(nullable: false, maxLength: 400, unicode: false),
                        FUNCTION_0 = c.String(nullable: false, maxLength: 200, unicode: false),
                        MOBPHONE_0 = c.String(nullable: false, maxLength: 100, unicode: false),
                        PHONE_0 = c.String(nullable: false, maxLength: 100, unicode: false),
                        BIRTHDAT_0 = c.DateTime(nullable: false, storeType: "date"),
                        GENDER_0 = c.Boolean(nullable: false),
                        PROFILE_0 = c.String(nullable: false, maxLength: 50, unicode: false),
                        LOGGEDFLG_0 = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "((1))")
                                },
                            }),
                        ADMINFLG_0 = c.Int(nullable: false),
                        SPECIALFLG_0 = c.Int(nullable: false),
                        ENABFLG_0 = c.Int(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "((2))")
                                },
                            }),
                        CREDAT_0 = c.DateTime(nullable: false, storeType: "date",
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        CREUSR_0 = c.String(nullable: false, maxLength: 5, unicode: false),
                        UPDDAT_0 = c.DateTime(nullable: false, storeType: "date",
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        UPDUSR_0 = c.String(nullable: false, maxLength: 5, unicode: false),
                        CREDATTIM_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        UPDDATTIM_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.ROWID)
                .Index(t => t.USR_0, unique: true, name: "IX_USR")
                .Index(t => t.USERNAM_0, unique: true, name: "IX_LOGIN");
            
            CreateTable(
                "ERT.WEBSERVICE",
                c => new
                    {
                        ROWID = c.Decimal(nullable: false, precision: 38, scale: 0, identity: true),
                        CODE_0 = c.String(nullable: false, maxLength: 200, unicode: false),
                        NAM_0 = c.String(nullable: false, maxLength: 200, unicode: false),
                        URL_0 = c.String(nullable: false, maxLength: 2000, unicode: false),
                        POOL_0 = c.String(nullable: false, maxLength: 200, unicode: false),
                        CODELANG_0 = c.String(nullable: false, maxLength: 200, unicode: false),
                        PARAMBEGIN_0 = c.String(nullable: false, maxLength: 8000, unicode: false),
                        MAXROWS_0 = c.Int(nullable: false),
                        KEYFLD_0 = c.String(nullable: false, maxLength: 200, unicode: false),
                        DATEFLD_0 = c.String(nullable: false, maxLength: 200, unicode: false),
                        USER_0 = c.String(nullable: false, maxLength: 200, unicode: false),
                        PASS_0 = c.String(nullable: false, maxLength: 200, unicode: false),
                        XMLEXAMPLE_0 = c.String(nullable: false, maxLength: 8000, unicode: false),
                        ATTEMPTS_0 = c.Int(nullable: false),
                        VCRNUMFLG_0 = c.Int(nullable: false),
                        VCRNUMSEARCH_0 = c.String(nullable: false, maxLength: 40, unicode: false),
                        VCRNUMSIZE_0 = c.Int(nullable: false),
                        IMPORTFLG_0 = c.Int(nullable: false),
                        ENABFLG_0 = c.Int(nullable: false),
                        CREDAT_0 = c.DateTime(nullable: false, storeType: "date",
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        CREUSR_0 = c.String(nullable: false, maxLength: 5, unicode: false),
                        UPDDAT_0 = c.DateTime(nullable: false, storeType: "date",
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        UPDUSR_0 = c.String(nullable: false, maxLength: 5, unicode: false),
                        CREDATTIM_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        UPDDATTIM_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.ROWID)
                .Index(t => t.CODE_0, name: "IX_WEBSERVICE");
            
            CreateTable(
                "ERT.WSPARAM",
                c => new
                    {
                        ROWID = c.Decimal(nullable: false, precision: 38, scale: 0, identity: true),
                        WSCODE_0 = c.String(nullable: false, maxLength: 100, unicode: false),
                        FLD_0 = c.String(nullable: false, maxLength: 100, unicode: false),
                        VALUE_0 = c.String(nullable: false, maxLength: 100, unicode: false),
                        TEXT_0 = c.Int(nullable: false),
                        POSITION_0 = c.Int(nullable: false),
                        CREDAT_0 = c.DateTime(nullable: false, storeType: "date",
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        CREUSR_0 = c.String(nullable: false, maxLength: 5, unicode: false),
                        UPDDAT_0 = c.DateTime(nullable: false, storeType: "date",
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        UPDUSR_0 = c.String(nullable: false, maxLength: 5, unicode: false),
                        CREDATTIM_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                        UPDDATTIM_0 = c.DateTime(nullable: false,
                            annotations: new Dictionary<string, AnnotationValues>
                            {
                                { 
                                    "SqlDefaultValue",
                                    new AnnotationValues(oldValue: null, newValue: "getdate()")
                                },
                            }),
                    })
                .PrimaryKey(t => t.ROWID)
                .Index(t => t.WSCODE_0, name: "IX_WSPARAM");
            
        }
        
        public override void Down()
        {
            DropIndex("ERT.WSPARAM", "IX_WSPARAM");
            DropIndex("ERT.WEBSERVICE", "IX_WEBSERVICE");
            DropIndex("ERT.USER", "IX_LOGIN");
            DropIndex("ERT.USER", "IX_USR");
            DropIndex("ERT.STOJOULOG", "IX_STOJOULOG");
            DropIndex("ERT.STODEFECT", "IX_STODEFECT");
            DropIndex("ERT.SCREEN", "IX_SCREEN");
            DropIndex("ERT.PROFILE", "IX_PROFILE");
            DropIndex("ERT.MODULE", "IX_MODULE");
            DropIndex("ERT.ITMLOCK", "IX_ITMLOCK");
            DropIndex("ERT.IMPORTWS", "IX_IMPORTWS");
            DropIndex("ERT.EXPORTWS", "IX_EXPORTWS");
            DropIndex("ERT.DOCALL", "IX_DOCALL_1");
            DropIndex("ERT.DOCALL", "IX_DOCALL_2");
            DropIndex("ERT.APP", "IX_APP");
            DropTable("ERT.WSPARAM",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CREDAT_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "CREDATTIM_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "UPDDAT_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "UPDDATTIM_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                });
            DropTable("ERT.WEBSERVICE",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CREDAT_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "CREDATTIM_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "UPDDAT_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "UPDDATTIM_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                });
            DropTable("ERT.USER",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CREDAT_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "CREDATTIM_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "ENABFLG_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "((2))" },
                        }
                    },
                    {
                        "LOGGEDFLG_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "((1))" },
                        }
                    },
                    {
                        "UPDDAT_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "UPDDATTIM_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                });
            DropTable("ERT.STOJOULOG",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "ATTEMPTS_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "((0))" },
                        }
                    },
                    {
                        "CREDAT_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "CREDATTIM_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "IMPORTFLG_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "((2))" },
                        }
                    },
                    {
                        "LOC_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "('')" },
                        }
                    },
                    {
                        "LOCTYP_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "('')" },
                        }
                    },
                    {
                        "OBSERVATION_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "('')" },
                        }
                    },
                    {
                        "OUTPUTWS_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "('')" },
                        }
                    },
                    {
                        "QTYPCU_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "((0))" },
                        }
                    },
                    {
                        "REGFLG_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "((1))" },
                        }
                    },
                    {
                        "STA_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "('')" },
                        }
                    },
                    {
                        "STU_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "('')" },
                        }
                    },
                    {
                        "SUCCESSFLG_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "((1))" },
                        }
                    },
                    {
                        "UPDDAT_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "UPDDATTIM_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "VCRNUM_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "('')" },
                        }
                    },
                    {
                        "WSUSR_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "('')" },
                        }
                    },
                    {
                        "XML_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "('')" },
                        }
                    },
                });
            DropTable("ERT.STOJOU",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "ATTEMPTS_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "((0))" },
                        }
                    },
                    {
                        "CREDAT_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "CREDATTIM_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "IMPORTFLG_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "((2))" },
                        }
                    },
                    {
                        "LOC_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "('')" },
                        }
                    },
                    {
                        "LOCTYP_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "('')" },
                        }
                    },
                    {
                        "OBSERVATION_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "('')" },
                        }
                    },
                    {
                        "OUTPUTWS_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "('')" },
                        }
                    },
                    {
                        "QTYPCU_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "((0))" },
                        }
                    },
                    {
                        "REGFLG_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "((1))" },
                        }
                    },
                    {
                        "STA_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "('')" },
                        }
                    },
                    {
                        "STU_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "('')" },
                        }
                    },
                    {
                        "SUCCESSFLG_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "((1))" },
                        }
                    },
                    {
                        "UPDDAT_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "UPDDATTIM_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "VCRNUM_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "('')" },
                        }
                    },
                    {
                        "WSUSR_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "('')" },
                        }
                    },
                    {
                        "XML_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "('')" },
                        }
                    },
                });
            DropTable("ERT.STODEFECT",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CREDAT_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "CREDATTIM_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "ITMREF_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "('')" },
                        }
                    },
                    {
                        "LOT_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "('')" },
                        }
                    },
                    {
                        "SLO_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "('')" },
                        }
                    },
                    {
                        "UPDDAT_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "UPDDATTIM_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                });
            DropTable("ERT.SCREEN",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CREDAT_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "CREDATTIM_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "ENABFLG_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "((2))" },
                        }
                    },
                    {
                        "UPDDAT_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "UPDDATTIM_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                });
            DropTable("ERT.PROFILE",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CREDAT_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "CREDATTIM_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "ENABFLG_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "((2))" },
                        }
                    },
                    {
                        "UPDDAT_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "UPDDATTIM_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                });
            DropTable("ERT.MODULE",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CREDAT_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "CREDATTIM_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "ENABFLG_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "((2))" },
                        }
                    },
                    {
                        "UPDDAT_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "UPDDATTIM_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                });
            DropTable("ERT.ITMLOCK",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CREDAT_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "CREDATTIM_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "UPDDAT_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "UPDDATTIM_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                });
            DropTable("ERT.IMPORTWSLOG",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CREDAT_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "CREDATTIM_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "SUCCESSFLG_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "((1))" },
                        }
                    },
                    {
                        "UPDDAT_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "UPDDATTIM_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                });
            DropTable("ERT.IMPORTWS",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CREDAT_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "CREDATTIM_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "UPDDAT_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "UPDDATTIM_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                });
            DropTable("ERT.EXPORTWSLOG",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CREDAT_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "CREDATTIM_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "UPDDAT_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "UPDDATTIM_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                });
            DropTable("ERT.EXPORTWS",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CREDAT_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "CREDATTIM_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "ENABFLG_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "((2))" },
                        }
                    },
                    {
                        "LASTREWRITEDATE_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "('1900-01-01')" },
                        }
                    },
                    {
                        "LASTWRITEDATE_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "('1900-01-01')" },
                        }
                    },
                    {
                        "NEXTREWRITEDATE_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "('1900-01-01')" },
                        }
                    },
                    {
                        "NEXTWRITEDATE_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "('1900-01-01')" },
                        }
                    },
                    {
                        "UPDDAT_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "UPDDATTIM_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                });
            DropTable("ERT.DOCALL",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CREDAT_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "CREDATTIM_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "UPDDAT_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "UPDDATTIM_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                });
            DropTable("ERT.CONFIGVALUE",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CREDAT_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "CREDATTIM_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "ENABFLG_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "((2))" },
                        }
                    },
                    {
                        "UPDDAT_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "UPDDATTIM_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                });
            DropTable("ERT.CONFIGKEY",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CREDAT_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "CREDATTIM_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "UPDDAT_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "UPDDATTIM_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                });
            DropTable("ERT.CONFIGDES",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CREDAT_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "CREDATTIM_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "UPDDAT_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "UPDDATTIM_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                });
            DropTable("ERT.APP",
                removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
                {
                    {
                        "CREDAT_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "CREDATTIM_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "ENABFLG_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "((2))" },
                        }
                    },
                    {
                        "UPDDAT_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "UPDDATTIM_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "getdate()" },
                        }
                    },
                    {
                        "VRS_0",
                        new Dictionary<string, object>
                        {
                            { "SqlDefaultValue", "('1.0.0')" },
                        }
                    },
                });
        }
    }
}
