﻿using MODELS.Helpers;
using System.Threading.Tasks;

namespace DATA.LayeredArquiteture.BusinessLogic.Interfaces.Generic
{
    public interface ICreateBLL<T> : IReadBLL<T> where T : class
    {
        Task<Result<T>> Insert(T model);

        Task<Result<T>> IsValidInsert(T model);
    }
}