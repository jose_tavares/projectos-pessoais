﻿using MODELS.Helpers;
using System.Threading.Tasks;

namespace DATA.LayeredArquiteture.BusinessLogic.Interfaces.Generic
{
    public interface IUpdateBLL<T> : ICreateBLL<T> where T : class
    {
        Task<Result<T>> Update(T model);

        Task<Result<T>> IsValidUpdate(T model);
    }
}