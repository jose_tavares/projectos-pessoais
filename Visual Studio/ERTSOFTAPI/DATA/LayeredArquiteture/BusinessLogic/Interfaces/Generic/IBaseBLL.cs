﻿using MODELS.BusinessObject.ERTSOFT.Database;

namespace DATA.LayeredArquiteture.BusinessLogic.Interfaces.Generic
{
    public interface IBaseBLL
    {
        USER AuthenticatedUser { get; set; }
    }
}
