﻿using MODELS.Helpers;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace DATA.LayeredArquiteture.BusinessLogic.Interfaces.Generic
{
    public interface IReadBLL<T> : IBaseBLL where T : class
    {
        Task<Result<IEnumerable<T>>> GetAll();

        Task<Result<T>> GetById(int id);
    }
}
