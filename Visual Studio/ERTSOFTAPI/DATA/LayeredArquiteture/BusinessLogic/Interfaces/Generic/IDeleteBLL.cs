﻿using MODELS.Helpers;
using System.Threading.Tasks;

namespace DATA.LayeredArquiteture.BusinessLogic.Interfaces.Generic
{
    public interface IDeleteBLL<T> : IUpdateBLL<T> where T : class
    {
        Task<Result<T>> Delete(int id);

        Task<Result<T>> IsValidDelete(int id);
    }
}