﻿using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Generic;
using MODELS.BusinessObject.SAGEX3.Database;

namespace DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete
{
    public interface IStolotBLL : IReadBLL<STOLOT>
    {

    }
}
