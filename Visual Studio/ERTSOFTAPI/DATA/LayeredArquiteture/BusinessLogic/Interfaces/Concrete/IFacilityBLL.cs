﻿using MODELS.Helpers;
using System.Threading.Tasks;
using MODELS.BusinessObject.SAGEX3.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Generic;

namespace DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete
{
    public interface IFacilityBLL : IReadBLL<FACILITY>
    {
        Task<Result<FACILITY>> GetByUniqueField(string FCY_0);
    }
}
