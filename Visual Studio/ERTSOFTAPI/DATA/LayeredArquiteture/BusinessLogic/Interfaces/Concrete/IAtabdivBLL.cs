﻿using MODELS.Helpers;
using System.Threading.Tasks;
using System.Collections.Generic;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Generic;
using MODELS.BusinessObject.SAGEX3.Database;

namespace DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete
{
    public interface IAtabdivBLL : IReadBLL<ATABDIV>
    {
        Task<Result<IEnumerable<ATABDIV>>> GetByNumtab(short NUMTAB_0);

        Task<Result<IEnumerable<ATABDIV>>> GetByFields(ATABDIV atabdiv, params string[] filterFields);

        Task<Result<ATABDIV>> GetByUniqueFields(short NUMTAB_0, string CODE_0);
    }
}
