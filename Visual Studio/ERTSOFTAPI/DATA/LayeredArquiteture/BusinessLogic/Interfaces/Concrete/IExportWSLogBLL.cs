﻿using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;

namespace DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete
{
    public interface IExportWSLogBLL : IReadBLL<EXPORTWSLOG>
    {

    }
}
