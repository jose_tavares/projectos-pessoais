﻿using MODELS.Helpers;
using System.Threading.Tasks;
using MODELS.BusinessObject.SAGEX3.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Generic;

namespace DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete
{
    public interface IItmFacilitBLL : IReadBLL<ITMFACILIT>
    {
        Task<Result<ITMFACILIT>> GetByUniqueFields(string STOFCY_0, string ITMREF_0);
    }
}
