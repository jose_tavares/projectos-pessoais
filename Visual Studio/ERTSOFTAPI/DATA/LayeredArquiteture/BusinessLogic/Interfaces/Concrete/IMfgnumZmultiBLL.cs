﻿using MODELS.Helpers;
using System.Threading.Tasks;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Generic;

namespace DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete
{
    public interface IMfgnumZmultiBLL : IUpdateBLL<MFGNUMZMULTI>
    {
        Task<Result<IEnumerable<MFGNUMZMULTI>>> GetByMfgnumori(string MFGNUMORI_0);

        Task<Result<IEnumerable<MFGNUMZMULTI>>> GetByMfgnum(string MFGNUM_0);

        Task<Result<MFGNUMZMULTI>> GetByUniqueFields(string MFGNUMORI_0, string MFGNUM_0);
    }
}
