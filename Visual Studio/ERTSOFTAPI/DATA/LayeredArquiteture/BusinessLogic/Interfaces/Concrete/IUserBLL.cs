﻿using MODELS.Helpers;
using System.Threading.Tasks;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Generic;

namespace DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete
{
    public interface IUserBLL : IUpdateBLL<USER>
    {
        Task<Result<IEnumerable<USER>>> GetByProfile(string PROFILE_0);

        Task<Result<USER>> GetByUser(string USER_0);

        Task<Result<USER>> GetByUsername(string USERNAM_0);

        Task<Result<USER>> Login(string USERNAM_0, string PASSWORD_0);

        Task<Result<USER>> Logout(int id);
    }
}
