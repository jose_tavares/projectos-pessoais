﻿using MODELS.Helpers;
using System.Threading.Tasks;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Generic;

namespace DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete
{
    public interface IWebServiceBLL : IUpdateBLL<WEBSERVICE>
    {
        Task<Result<IEnumerable<WEBSERVICE>>> GetByImportFlg(int IMPORTFLG_0);

        Task<Result<WEBSERVICE>> GetByUniqueField(string CODE_0);

        Task<Result<WEBSERVICE>> IsValidImport(string CODE_0);

        Task<Result<WEBSERVICE>> IsValidExport(string CODE_0);
    }
}
