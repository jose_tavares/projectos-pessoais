﻿using MODELS.Helpers;
using System.Threading.Tasks;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Generic;
using MODELS.BusinessObject.SAGEX3.Database;

namespace DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete
{
    public interface IAtextraBLL : IReadBLL<ATEXTRA>
    {
        Task<Result<ATEXTRA>> GetByUniqueFields(string CODFIC_0, string ZONE_0, string LANGUE_0, string IDENT_1, string IDENT_2);
    }
}
