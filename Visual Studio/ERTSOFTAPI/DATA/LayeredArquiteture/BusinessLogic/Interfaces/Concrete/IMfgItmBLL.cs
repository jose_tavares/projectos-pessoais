﻿using MODELS.Helpers;
using System.Threading.Tasks;
using MODELS.BusinessObject.SAGEX3.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Generic;

namespace DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete
{
    public interface IMfgItmBLL : IReadBLL<MFGITM>
    {
        Task<Result<MFGITM>> GetByUniqueField(string MFGNUM_0);
    }
}
