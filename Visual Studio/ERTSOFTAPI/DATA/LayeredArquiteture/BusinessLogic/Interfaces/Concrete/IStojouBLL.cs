﻿using MODELS.Helpers;
using System.Threading.Tasks;
using System.Collections.Generic;
using DATA.Helpers.AuxiliarClasses;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Generic;

namespace DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete
{
    public interface IStojouBLL : IUpdateBLL<STOJOU>
    {
        Task<IEnumerable<STOJOU>> GetByWSCode(string WSCODE_0);

        Task<IEnumerable<STOJOU>> GetPending();

        Task<IEnumerable<STOJOU>> GetByFields(STOJOU stojou);

        Task<Result<STOJOU>> ImportFirstPendingByWebService(string WEBSERVICE_0);

        Task<STOJOU> GetFirstPendingByWebService(string WEBSERVICE_0);

        Task<STOJOU> GetByIdWithWebService(int id);

        Task<STOJOU> GetByIdWithStojouLogList(int id);

        Task<string> GetNextProductionSlo(string MFGNUM_0);

        Task<Result<STOJOU>> InsertProduction(ProductionData productionData);
    }
}
