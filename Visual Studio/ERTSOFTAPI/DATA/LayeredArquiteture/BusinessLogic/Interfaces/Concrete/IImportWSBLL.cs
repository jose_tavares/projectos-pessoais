﻿using MODELS.Helpers;
using System.Threading.Tasks;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Generic;

namespace DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete
{
    public interface IImportWSBLL : ICreateBLL<IMPORTWS>
    {
        Task<Result<IMPORTWS>> GetByUniqueField(string WSCODE_0);

        Task<Result<dynamic>> GetDynamicById(int id, string SQLTABLE_0, IEnumerable<string> fields);

        Task<(Result<dynamic>, IMPORTWS importWs)> GetFirstPendingByWsCode(string WSCODE_0);

        Task<Result<IMPORTWS>> ImportFirstPendingByWsCode(string WEBSERVICE_0);
    }
}
