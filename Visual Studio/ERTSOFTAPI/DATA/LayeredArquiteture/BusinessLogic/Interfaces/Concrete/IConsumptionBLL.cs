﻿using MODELS.Helpers;
using System.Threading.Tasks;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Generic;

namespace DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete
{
    public interface IConsumptionBLL : IReadBLL<CONSUMPTION>
    {
        Task<Result<IEnumerable<CONSUMPTION>>> GetByMfgnum(string MFGNUM_0, int REGFLG_0);

        Task<Result<IEnumerable<CONSUMPTION>>> GetByMfgnumItmref(string MFGNUM_0, string ITMREF_0, int REGFLG_0);

        Task<Result<IEnumerable<CONSUMPTION>>> GetByMfgnumSloori(string MFGNUM_0, string SLOORI_0, int REGFLG_0);
    }
}
