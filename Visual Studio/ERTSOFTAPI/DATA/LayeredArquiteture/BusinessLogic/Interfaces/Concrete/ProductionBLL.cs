﻿using MODELS.Helpers;
using System.Transactions;
using System.Threading.Tasks;
using DATA.Helpers.StaticClasses;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.Repositories.UnitOfWork;

namespace DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete
{
    public class ProductionBLL : IProductionBLL
    {
        public USER AuthenticatedUser { get; set; }

        public ProductionBLL()
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();
        }

        public async Task<IEnumerable<PRODUCTION>> GetAll()
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
            {
                IEnumerable<PRODUCTION> productionList = await uow.ProductionRepository.GetAll();

                uow.Commit();

                return productionList;
            }
        }

        public async Task<PRODUCTION> GetByID(decimal id)
        {
            Result<PRODUCTION> result = id.IsValueValid<PRODUCTION>(nameof(PRODUCTION.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                {
                    PRODUCTION production = await uow.ProductionRepository.GetByID(id);

                    uow.Commit();

                    return production;
                }
            }

            return null;
        }

        public async Task<Result<PRODUCTION>> Insert(PRODUCTION production)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
            {
                Result<PRODUCTION> result = await IsValidInsert(production);

                if (result.Success)
                {
                    production.CREUSR_0 = AuthenticatedUser.USR_0;
                    production.UPDUSR_0 = AuthenticatedUser.USR_0;

                    result = await uow.ProductionRepository.Insert(production);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<PRODUCTION>> IsValidInsert(PRODUCTION production)
        {
            Result<PRODUCTION> result = await production.IsClassValidAsync(true);

            return result;
        }
    }
}
