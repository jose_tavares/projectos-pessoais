﻿using MODELS.Helpers;
using System.Threading.Tasks;
using MODELS.BusinessObject.SAGEX3.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Generic;

namespace DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete
{
    public interface IStolocBLL : IReadBLL<STOLOC>
    {
        Task<Result<STOLOC>> GetByUniqueFields(string STOFCY_0, string LOC_0);
    }
}
