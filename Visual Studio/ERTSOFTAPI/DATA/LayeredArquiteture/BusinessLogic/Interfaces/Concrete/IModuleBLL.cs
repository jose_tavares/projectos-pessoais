﻿using MODELS.Helpers;
using System.Threading.Tasks;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Generic;

namespace DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete
{
    public interface IModuleBLL : IUpdateBLL<MODULE>
    {
        Task<Result<IEnumerable<MODULE>>> GetByApp(string APP_0);

        Task<Result<IEnumerable<MODULE>>> GetByModule(string MODULE_0);

        Task<Result<MODULE>> GetByUniqueFields(string APP_0, string MODULE_0);

        Task<Result<MODULE>> GetByUniqueFieldsWithScreenList(string APP_0, string MODULE_0);
    }
}
