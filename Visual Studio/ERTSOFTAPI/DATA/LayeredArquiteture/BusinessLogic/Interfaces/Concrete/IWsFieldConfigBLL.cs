﻿using MODELS.Helpers;
using System.Threading.Tasks;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Generic;

namespace DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete
{
    public interface IWsFieldConfigBLL : IUpdateBLL<WSFIELDCONFIG>
    {
        /// <summary>
        /// Uses properties to filter data, ignore properties with null values.
        /// Properties that can be used: WSCODE_0, FLD_0, VALUE_0, TEXT_0
        /// </summary>
        /// <param name="wsParam"></param>
        /// <returns>Configuration associated </returns>
        Task<Result<IEnumerable<WSFIELDCONFIG>>> GetByFields(string WSCODE_0, string FLD_0, string VALUE_0, int FLGFLG_0, int VALUEFLG_0);

        /// <summary>
        /// Gets 
        /// </summary>
        /// <param name="WSCODE_0"></param>
        /// <returns></returns>
        Task<Result<IEnumerable<WSFIELDCONFIG>>> GetByWsCode(string WSCODE_0);

        Task<Result<WSFIELDCONFIG>> GetByUniqueFields(string WSCODE_0, int POSITION_0);
    }
}
