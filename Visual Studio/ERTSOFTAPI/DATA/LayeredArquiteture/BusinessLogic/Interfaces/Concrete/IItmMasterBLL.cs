﻿using MODELS.Helpers;
using System.Threading.Tasks;
using MODELS.BusinessObject.SAGEX3.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Generic;

namespace DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete
{
    public interface IItmMasterBLL : IReadBLL<ITMMASTER>
    {
        Task<Result<ITMMASTER>> GetByUniqueField(string ITMREF_0);
    }
}
