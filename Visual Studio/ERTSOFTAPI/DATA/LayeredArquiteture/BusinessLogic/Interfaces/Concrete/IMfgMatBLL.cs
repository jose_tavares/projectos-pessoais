﻿using MODELS.Helpers;
using System.Collections.Generic;
using System.Threading.Tasks;
using MODELS.BusinessObject.SAGEX3.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Generic;

namespace DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete
{
    public interface IMfgMatBLL : IReadBLL<MFGMAT>
    {
        /// <summary>
        /// Gets List of MFGMAT filtered by MFGNUM_0.
        /// If parameter STOMGTCOD_0 is true conditions applied are ITMMASTER.STOMGTCOD_0 <> 1 and MATSTA_0 < 4, otherwise nothing is applied.
        /// </summary>
        /// <param name="MFGNUM_0"></param>
        /// <param name="STOMGTCOD_0"></param>
        /// <returns></returns>
        Task<Result<IEnumerable<MFGMAT>>> GetByMfgnum(string MFGNUM_0, bool STOMGTCOD_0);

        /// <summary>
        /// Gets List of MFGMAT filtered by MFGNUM_0, ZOPERCON_0 and (ZCONSPRDOK_0 or ZCONSPRDNOK_0)
        /// Conditions applied are ITMMASTER.STOMGTCOD_0 <> 1 and MATSTA_0 < 4.
        /// Take in consideration that if PRODOK_0 parameter is true, it will filter ZCONSPRDOK_0 = 2, otherwise ZCONSPRDNOK_0 = 2
        /// </summary>
        /// <param name="MFGNUM_0"></param>
        /// <param name="ZOPERCON_0"></param>
        /// <param name="PRODOK_0"></param>
        /// <returns></returns>
        Task<Result<IEnumerable<MFGMAT>>> GetByMfgnumAndOperationAndProdType(string MFGNUM_0, string ZOPERCON_0, bool PRODOK);
    }
}
