﻿using MODELS.Helpers;
using System.Threading.Tasks;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Generic;
using MODELS.BusinessObject.SAGEX3.Database;

namespace DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete
{
    public interface IMfgHeadBLL : IReadBLL<MFGHEAD>
    {
        Task<Result<MFGHEAD>> GetByUniqueField(string MFGNUM_0);
    }
}
