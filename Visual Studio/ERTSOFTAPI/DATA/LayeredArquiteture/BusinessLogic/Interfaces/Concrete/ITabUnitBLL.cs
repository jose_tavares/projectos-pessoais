﻿using MODELS.Helpers;
using System.Threading.Tasks;
using MODELS.BusinessObject.SAGEX3.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Generic;

namespace DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete
{
    public interface ITabUnitBLL : IReadBLL<TABUNIT>
    {
        Task<Result<TABUNIT>> GetByUniqueField(string UOM_0);
    }
}
