﻿using MODELS.Helpers;
using System.Threading.Tasks;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Generic;

namespace DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete
{
    public interface IItmLockBLL : IReadBLL<ITMLOCK>
    {
        Task<Result<ITMLOCK>> GetByUniqueField(string ITMREF_0);
    }
}