﻿using MODELS.Helpers;
using System.Threading.Tasks;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Generic;

namespace DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete
{
    public interface IConfigValueBLL : IUpdateBLL<CONFIGVALUE>
    {
        Task<Result<IEnumerable<CONFIGVALUE>>> GetByFields(CONFIGVALUE configValue, params string[] filterFields);
    }
}
