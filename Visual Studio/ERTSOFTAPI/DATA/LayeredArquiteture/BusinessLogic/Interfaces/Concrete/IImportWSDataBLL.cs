﻿using MODELS.Helpers;
using System.Threading.Tasks;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Generic;

namespace DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete
{
    public interface IImportWSDataBLL : IUpdateBLL<IMPORTWSDATA>
    {
        Task<Result<IMPORTWSDATA>> GetByUniqueFields(string WSCODE_0, int ID);

        Task<Result<IMPORTWSDATA>> GetFirstPendingByWsCode(string WSCODE_0);
    }
}
