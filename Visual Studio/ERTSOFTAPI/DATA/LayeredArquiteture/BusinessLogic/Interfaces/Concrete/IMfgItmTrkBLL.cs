﻿using MODELS.Helpers;
using System.Threading.Tasks;
using System.Collections.Generic;
using MODELS.BusinessObject.SAGEX3.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Generic;

namespace DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete
{
    public interface IMfgItmTrkBLL : IReadBLL<MFGITMTRK>
    {
        Task<Result<IEnumerable<MFGITMTRK>>> GetByZESSTROWID(string ZESSTROWID);
    }
}
