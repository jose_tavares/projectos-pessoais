﻿using MODELS.Helpers;
using System.Threading.Tasks;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Generic;

namespace DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete
{
    public interface IExportWSBLL : IUpdateBLL<EXPORTWS>
    {
        Task<Result<EXPORTWS>> GetByUniqueField(string EXPORTWS_0);

        Task<IEnumerable<EXPORTWS>> GetPending();

        Task<Result<EXPORTWS>> ExportPending();
    }
}
