﻿using System.Threading.Tasks;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Generic;

namespace DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete
{
    public interface IWSParamBLL : IUpdateBLL<WSPARAM>
    {
        /// <summary>
        /// Uses properties to filter data, ignore properties with null values.
        /// Properties that can be used: WSCODE_0, FLD_0, VALUE_0, TEXT_0
        /// </summary>
        /// <param name="wsParam"></param>
        /// <returns>Configuration associated </returns>
        Task<IEnumerable<WSPARAM>> GetByFields(WSPARAM wsParam);

        /// <summary>
        /// Gets 
        /// </summary>
        /// <param name="WSCODE_0"></param>
        /// <returns></returns>
        Task<IEnumerable<WSPARAM>> GetByWsCode(string WSCODE_0);
    }
}
