﻿using MODELS.Helpers;
using System.Threading.Tasks;
using System.Collections.Generic;
using MODELS.BusinessObject.SAGEX3.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Generic;

namespace DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete
{
    public interface IMfgMatTrkBLL : IReadBLL<MFGMATTRK>
    {
        Task<Result<IEnumerable<MFGMATTRK>>> GetByZESSTROWID(string ZESSTROWID);
    }
}