﻿using MODELS.Helpers;
using System.Threading.Tasks;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Generic;

namespace DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete
{
    public interface IAppBLL : IUpdateBLL<APP>
    {
        Task<Result<APP>> GetByUniqueFieldWithModuleList(string APP_0);

        Task<Result<APP>> GetByUniqueField(string APP_0);
    }
}
