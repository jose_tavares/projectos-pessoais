﻿using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Generic;

namespace DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete
{
    public interface IStojouLogBLL : IReadBLL<STOJOULOG> 
    {
    
    }
}
