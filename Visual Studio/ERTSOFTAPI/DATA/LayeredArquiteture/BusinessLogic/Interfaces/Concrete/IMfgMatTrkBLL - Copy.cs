﻿using MODELS.BusinessObject.SAGEX3;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Generic;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete
{
    public interface IMfgItmTrkBLL : IReadBLL<MFGITMTRK>
    {
        Task<IEnumerable<MFGITMTRK>> GetByZESSTROWID(decimal ZESSTROWID);
    }
}
