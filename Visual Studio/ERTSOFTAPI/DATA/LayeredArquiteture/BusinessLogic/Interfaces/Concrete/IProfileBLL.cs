﻿using MODELS.Helpers;
using System.Threading.Tasks;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Generic;

namespace DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete
{
    public interface IProfileBLL : IUpdateBLL<PROFILE>
    {
        Task<Result<PROFILE>> GetByUniqueField(string PROFILE_0);

        Task<Result<PROFILE>> GetByUniqueFieldWithUserList(string PROFILE_0);
    }
}
