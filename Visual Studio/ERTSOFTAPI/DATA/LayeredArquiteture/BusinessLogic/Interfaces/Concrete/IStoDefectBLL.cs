﻿using MODELS.Helpers;
using System.Threading.Tasks;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Generic;

namespace DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete
{
    public interface IStoDefectBLL : IUpdateBLL<STODEFECT>
    {
        Task<Result<IEnumerable<STODEFECT>>> GetByStoDefect(STODEFECT stoDefect);
    }
}
