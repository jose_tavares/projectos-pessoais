﻿using MODELS.Helpers;
using System.Threading.Tasks;
using System.Collections.Generic;
using MODELS.BusinessObject.SAGEX3.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Generic;

namespace DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete
{
    public interface IStockBLL : IReadBLL<STOCK>
    {
        /// <summary>
        /// Uses properties to filter data, ignore properties with null values.
        /// Properties that can be used: STOFCY_0, ITMREF_0, LOT_0, SLO_0, LOC_0, STA_0, LOCTYP_0, PCU_0
        /// </summary>
        /// <param name="stock"></param>
        /// <returns>Stock available in Sage</returns>
        Task<Result<IEnumerable<STOCK>>> GetByFields(STOCK stock, params string[] filterFields);

        Task<Result<IEnumerable<STOCK>>> GetByFieldsWithStolot(STOCK stock, params string[] filterFields);
    }
}
