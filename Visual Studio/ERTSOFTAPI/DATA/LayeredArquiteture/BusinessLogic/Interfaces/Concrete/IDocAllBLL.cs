﻿using MODELS.Helpers;
using System.Threading.Tasks;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Generic;

namespace DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete
{
    public interface IDocAllBLL : IUpdateBLL<DOCALL>
    {
        Task<Result<IEnumerable<DOCALL>>> GetDocAllAvailable(string DOCUMENT_0);

        Task<Result<IEnumerable<DOCALL>>> GetDocItmAllAvailable(string STOFCY_0, string DOCUMENT_0, string ITMREF_0);
    }
}
