﻿using DATA.Helpers.StaticClasses;
using System.Collections.Generic;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;
using System.Threading.Tasks;
using System.Transactions;
using MODELS.Helpers;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.Repositories.UnitOfWork;
using MODELS.BusinessObject.SAGEX3.Database;

namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{
    public class MfgHeadBLL : IMfgHeadBLL
    {
        public USER AuthenticatedUser { get; set; }

        public MfgHeadBLL()
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();
        }

        public async Task<IEnumerable<MFGHEAD>> GetAll()
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
            {
                IEnumerable<MFGHEAD> mfgHeadList = await uow.MfgHeadRepository.GetAll();

                uow.Commit();

                return mfgHeadList;
            }
        }

        public async Task<MFGHEAD> GetByUniqueKey(string MFGNUM_0)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
            {
                MFGHEAD mfgHead = await uow.MfgHeadRepository.GetByUniqueField(nameof(MFGHEAD.MFGNUM_0), MFGNUM_0);

                uow.Commit();

                return mfgHead;
            }
        }

        public async Task<MFGHEAD> GetByID(decimal id)
        {
            Result<MFGHEAD> result = id.IsValueValid<MFGHEAD>(nameof(MFGHEAD.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
                {
                    MFGHEAD mfgHead = await uow.MfgHeadRepository.GetByID(id);

                    uow.Commit();

                    return mfgHead;
                }
            }

            return null;
        }
    }
}
