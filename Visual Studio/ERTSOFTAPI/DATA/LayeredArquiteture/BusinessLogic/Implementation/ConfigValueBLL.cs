﻿using System.Linq;
using MODELS.Helpers;
using System.Transactions;
using System.Threading.Tasks;
using DATA.Helpers.StaticClasses;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.Repositories.UnitOfWork;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;

namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{
    public class ConfigValueBLL : IConfigValueBLL
    {
        public USER AuthenticatedUser { get; set; }

        public ConfigValueBLL()
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();
        }

        public async Task<IEnumerable<CONFIGVALUE>> GetAll()
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Snapshot))
            {
                IEnumerable<CONFIGVALUE> configValueList = await uow.ConfigValueRepository.GetAll();

                uow.Commit();

                return configValueList;
            }
        }

        public async Task<IEnumerable<CONFIGVALUE>> GetByFields(CONFIGVALUE configValue)
        {
            Dictionary<string, object> fields = new Dictionary<string, object>();
            Dictionary<string, object> fieldsTemp = new Dictionary<string, object>
            {
                { nameof(CONFIGVALUE.CONFIGTYP_0), configValue.CONFIGTYP_0},
                { nameof(CONFIGVALUE.VALUE_0), configValue.VALUE_0 },
                { nameof(CONFIGVALUE.VALUE_1), configValue.VALUE_1 },
                { nameof(CONFIGVALUE.VALUE_2), configValue.VALUE_2 },
                { nameof(CONFIGVALUE.VALUE_3), configValue.VALUE_3 }
            };

            foreach (KeyValuePair<string, object> field in fieldsTemp)
            {
                if (field.Value != null)
                {
                    fields.Add(field.Key, field.Value);
                }
            }

            if (fields.Count > 0)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
                {
                    IEnumerable<CONFIGVALUE> configValueList = await uow.ConfigValueRepository.GetByFields(fields);

                    uow.Commit();

                    return configValueList;
                }
            }

            return Enumerable.Empty<CONFIGVALUE>();
        }

        public async Task<CONFIGVALUE> GetByID(decimal id)
        {
            Result<CONFIGVALUE> result = id.IsValueValid<CONFIGVALUE>(nameof(CONFIGVALUE.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                {
                    CONFIGVALUE configValue = await uow.ConfigValueRepository.GetByID(id);

                    uow.Commit();

                    return configValue;
                }
            }

            return null;
        }

        public async Task<CONFIGVALUE> GetByUniqueFields(CONFIGVALUE configValue)
        {
            Dictionary<string, object> fields = new Dictionary<string, object>();
            Dictionary<string, object> fieldsTemp = new Dictionary<string, object>
            {
                { nameof(CONFIGVALUE.CONFIGTYP_0), configValue.CONFIGTYP_0},
                { nameof(CONFIGVALUE.VALUE_0), configValue.VALUE_0 },
                { nameof(CONFIGVALUE.VALUE_1), configValue.VALUE_1 },
                { nameof(CONFIGVALUE.VALUE_2), configValue.VALUE_2 },
                { nameof(CONFIGVALUE.VALUE_3), configValue.VALUE_3 }
            };

            foreach (KeyValuePair<string, object> field in fieldsTemp)
            {
                if (field.Value != null)
                {
                    fields.Add(field.Key, field.Value);
                }
            }

            if (fields.Count > 0)
            {
                Result<CONFIGVALUE> result = configValue.IsPropertiesValid(fields.Keys.ToArray());

                if (result.Success)
                {
                    using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
                    {
                        configValue = await uow.ConfigValueRepository.GetByUniqueFields(fields);

                        uow.Commit();

                        return configValue;
                    }
                }
            }

            return null;
        }

        public async Task<Result<CONFIGVALUE>> Insert(CONFIGVALUE configValue)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
            {
                Result<CONFIGVALUE> result = await IsValidInsert(configValue);

                if (result.Success)
                {
                    configValue.CREUSR_0 = AuthenticatedUser.USR_0;
                    configValue.UPDUSR_0 = AuthenticatedUser.USR_0;

                    result = await uow.ConfigValueRepository.Insert(configValue);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<CONFIGVALUE>> Update(CONFIGVALUE app)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
            {
                Result<CONFIGVALUE> result = await IsValidUpdate(app);

                if (result.Success)
                {
                    app.UPDUSR_0 = AuthenticatedUser.USR_0;

                    result = await uow.ConfigValueRepository.Update(app);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<CONFIGVALUE>> IsValidInsert(CONFIGVALUE app)
        {
            // Add more validations if needed
            Result<CONFIGVALUE> result = await app.IsClassValidAsync();

            return result;
        }

        public async Task<Result<CONFIGVALUE>> IsValidUpdate(CONFIGVALUE app)
        {
            // Add more validations if needed
            Result<CONFIGVALUE> result = await app.IsClassValidAsync();

            return result;
        }
    }
}
