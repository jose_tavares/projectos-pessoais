﻿using System.Linq;
using MODELS.Helpers;
using System.Transactions;
using System.Threading.Tasks;
using DATA.Helpers.StaticClasses;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;
using DATA.LayeredArquiteture.Repositories.UnitOfWork;
using MODELS.BusinessObject.SAGEX3.Database;

namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{
    public class StockBLL : IStockBLL
    {
        public USER AuthenticatedUser { get; set; }

        public StockBLL()
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();
        }

        public async Task<IEnumerable<STOCK>> GetAll()
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
            {
                IEnumerable<STOCK> stockList = await uow.StockRepository.GetAll();

                uow.Commit();

                return stockList;
            }
        }

        public async Task<IEnumerable<STOCK>> GetByITMREF(string ITMREF_0)
        {
            Result<STOCK> result = ITMREF_0.IsValueValid<STOCK>(nameof(STOCK.ITMREF_0));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
                {
                    IEnumerable<STOCK> stockList = await uow.StockRepository.GetByField(nameof(STOCK.ITMREF_0), ITMREF_0);

                    uow.Commit();

                    return stockList;
                }
            }

            return Enumerable.Empty<STOCK>();
        }

        public async Task<IEnumerable<STOCK>> GetByFields(STOCK stock)
        {
            Dictionary<string, object> fields = new Dictionary<string, object>();
            Dictionary<string, object> fieldsTemp = new Dictionary<string, object>
            {
                { nameof(STOCK.STOFCY_0), stock.STOFCY_0},
                { nameof(STOCK.ITMREF_0), stock.ITMREF_0 },
                { nameof(STOCK.LOT_0), stock.LOT_0 },
                { nameof(STOCK.SLO_0), stock.SLO_0 },
                { nameof(STOCK.LOC_0), stock.LOC_0 },
                { nameof(STOCK.STA_0), stock.STA_0 },
                { nameof(STOCK.LOCTYP_0), stock.LOCTYP_0 },
                { nameof(STOCK.PCU_0), stock.PCU_0 }
            };

            foreach (KeyValuePair<string, object> field in fieldsTemp)
            {
                if (field.Value != null)
                {
                    fields.Add(field.Key, field.Value);
                }
            }

            if (fields.Count > 0)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
                {
                    IEnumerable<STOCK> stockList = await uow.StockRepository.GetByFields(fields);

                    uow.Commit();

                    return stockList;
                }
            }

            return Enumerable.Empty<STOCK>();
        }

        public async Task<IEnumerable<STOCK>> GetByFieldsWithStolot(STOCK stock)
        {
            Dictionary<string, object> fields = new Dictionary<string, object>();
            Dictionary<string, object> fieldsTemp = new Dictionary<string, object>
            {
                { nameof(STOCK.STOFCY_0), stock.STOFCY_0},
                { nameof(STOCK.ITMREF_0), stock.ITMREF_0 },
                { nameof(STOCK.LOT_0), stock.LOT_0 },
                { nameof(STOCK.SLO_0), stock.SLO_0 },
                { nameof(STOCK.LOC_0), stock.LOC_0 },
                { nameof(STOCK.STA_0), stock.STA_0 },
                { nameof(STOCK.LOCTYP_0), stock.LOCTYP_0 }
            };

            foreach (KeyValuePair<string, object> field in fieldsTemp)
            {
                if (field.Value != null)
                {
                    fields.Add(field.Key, field.Value);
                }
            }

            if (fields.Count > 0)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
                {
                    IEnumerable<STOCK> stockList = await uow.StockRepository.GetByFieldsWithStolot(fields);

                    uow.Commit();

                    return stockList;
                }
            }

            return Enumerable.Empty<STOCK>();
        }

        public async Task<STOCK> GetByID(decimal id)
        {
            Result<STOCK> result = id.IsValueValid<STOCK>(nameof(STOCK.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
                {
                    STOCK stock = await uow.StockRepository.GetByID(id);

                    uow.Commit();

                    return stock;
                }
            }

            return null;
        }
    }
}
