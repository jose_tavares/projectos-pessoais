﻿using MODELS.Helpers;
using System.Transactions;
using System.Threading.Tasks;
using DATA.Helpers.StaticClasses;
using System.Collections.Generic;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.Repositories.UnitOfWork;
using MODELS.BusinessObject.SAGEX3.Database;

namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{
    public class MfgMatBLL : IMfgMatBLL
    {
        public USER AuthenticatedUser { get; set; }

        public MfgMatBLL()
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();
        }

        public async Task<IEnumerable<MFGMAT>> GetAll()
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
            {
                IEnumerable<MFGMAT> mfgMatList = await uow.MfgMatRepository.GetAll();

                uow.Commit();

                return mfgMatList;
            }
        }

        public async Task<IEnumerable<MFGMAT>> GetByMfgnum(string MFGNUM_0, bool STOMGTCOD_0)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
            {
                IEnumerable<MFGMAT> mfgMatList = null;

                if (STOMGTCOD_0)
                {
                    mfgMatList = await uow.MfgMatRepository.GetByMfgnum(MFGNUM_0);
                }
                else
                {
                    mfgMatList = await uow.MfgMatRepository.GetByField(nameof(MFGMAT.MFGNUM_0), MFGNUM_0);
                }

                uow.Commit();

                return mfgMatList;
            }
        }

        public async Task<IEnumerable<MFGMAT>> GetByMfgnumAndOperationAndProdType(string MFGNUM_0, string ZOPERCON_0, bool PRODOK_0)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
            {
                IEnumerable<MFGMAT> mfgMatList = await uow.MfgMatRepository.GetByMfgnumAndOperationAndProdType(MFGNUM_0, ZOPERCON_0, PRODOK_0);

                uow.Commit();

                return mfgMatList;
            }
        }

        public async Task<MFGMAT> GetByID(decimal id)
        {
            Result<MFGMAT> result = id.IsValueValid<MFGMAT>(nameof(MFGMAT.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
                {
                    MFGMAT mfgMat = await uow.MfgMatRepository.GetByID(id);

                    uow.Commit();

                    return mfgMat;
                }
            }

            return null;
        }
    }
}
