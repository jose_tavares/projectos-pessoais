﻿using System.Linq;
using MODELS.Helpers;
using System.Transactions;
using System.Threading.Tasks;
using DATA.Helpers.StaticClasses;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;
using DATA.LayeredArquiteture.Repositories.UnitOfWork;

namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{
    public class StoDefectBLL : IStoDefectBLL
    {
        public USER AuthenticatedUser { get; set; }

        public StoDefectBLL()
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();
        }

        public async Task<IEnumerable<STODEFECT>> GetAll()
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
            {
                IEnumerable<STODEFECT> stoDefectList = await uow.StoDefectRepository.GetAll();

                uow.Commit();

                return stoDefectList;
            }
        }

        public async Task<IEnumerable<STODEFECT>> GetByFields(STODEFECT stoDefect)
        {
            Dictionary<string, object> fields = new Dictionary<string, object>();
            Dictionary<string, object> fieldsTemp = new Dictionary<string, object>
            {
                { nameof(STODEFECT.MFGNUM_0), stoDefect.MFGNUM_0},
                { nameof(STODEFECT.ITMREF_0), stoDefect.ITMREF_0 },
                { nameof(STODEFECT.LOT_0), stoDefect.LOT_0 },
                { nameof(STODEFECT.SLO_0), stoDefect.SLO_0 },
                { nameof(STODEFECT.DEFECT_0), stoDefect.DEFECT_0 },
                { nameof(STODEFECT.MACHINE_0), stoDefect.MACHINE_0 },
                { nameof(STODEFECT.OPERATION_0), stoDefect.OPERATION_0 }
            };

            foreach (KeyValuePair<string, object> field in fieldsTemp)
            {
                if (field.Value != null)
                {
                    fields.Add(field.Key, field.Value);
                }
            }

            if (fields.Count > 0)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
                {
                    IEnumerable<STODEFECT> stoDefectList = await uow.StoDefectRepository.GetByFields(fields);

                    uow.Commit();

                    return stoDefectList;
                }
            }

            return Enumerable.Empty<STODEFECT>();
        }

        public async Task<STODEFECT> GetByID(decimal id)
        {
            Result<STODEFECT> result = id.IsValueValid<STODEFECT>(nameof(STODEFECT.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
                {
                    STODEFECT stoDefect = await uow.StoDefectRepository.GetByID(id);

                    uow.Commit();

                    return stoDefect;
                }
            }

            return null;
        }

        public async Task<Result<STODEFECT>> Insert(STODEFECT stoDefect)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
            {
                Result<STODEFECT> result = await IsValidInsert(stoDefect);

                if (result.Success)
                {
                    stoDefect.CREUSR_0 = AuthenticatedUser.USR_0;
                    stoDefect.UPDUSR_0 = AuthenticatedUser.USR_0;

                    result = await uow.StoDefectRepository.Insert(stoDefect);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<STODEFECT>> Update(STODEFECT stoDefect)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
            {
                Result<STODEFECT> result = await IsValidUpdate(stoDefect);

                if (result.Success)
                {
                    stoDefect.CREUSR_0 = AuthenticatedUser.USR_0;

                    result = await uow.StoDefectRepository.Update(stoDefect);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<STODEFECT>> IsValidInsert(STODEFECT stoDefect)
        {
            // Add more validations if needed
            Result<STODEFECT> result = await stoDefect.IsClassValidAsync();

            return result;
        }

        public async Task<Result<STODEFECT>> IsValidUpdate(STODEFECT stoDefect)
        {
            // Add more validations if needed
            Result<STODEFECT> result = await stoDefect.IsClassValidAsync();

            if (result.Success)
            {
                STODEFECT stoDefectTemp = await GetByID(stoDefect.ROWID);

                if (stoDefectTemp == null)
                {
                    result.AddInvalidMessage($"Não foi possível encontrar o defeito com o ID {stoDefect.ROWID}.");
                }
            }

            return result;
        }
    }
}
