﻿using MODELS.Helpers;
using System.Transactions;
using System.Threading.Tasks;
using DATA.Helpers.StaticClasses;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;
using System.Linq;
using DATA.LayeredArquiteture.Repositories.UnitOfWork;

namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{
    public class WebServiceBLL : IWebServiceBLL
    {
        public USER AuthenticatedUser { get; set; }

        public WebServiceBLL()
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();
        }

        public async Task<IEnumerable<WEBSERVICE>> GetAll()
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Snapshot))
            {
                IEnumerable<WEBSERVICE> webServiceList = await uow.WebServiceRepository.GetAll();

                uow.Commit();

                return webServiceList;
            }
        }

        public async Task<IEnumerable<WEBSERVICE>> GetByImportFlg(int IMPORTFLG_0)
        {
            Result<WEBSERVICE> result = IMPORTFLG_0.IsValueValid<WEBSERVICE>(nameof(STOJOU.IMPORTFLG_0));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Snapshot))
                {
                    IEnumerable<WEBSERVICE> webServiceList = await uow.WebServiceRepository.GetByField(nameof(WEBSERVICE.IMPORTFLG_0), IMPORTFLG_0);

                    uow.Commit();

                    return webServiceList;
                }
            }

            return Enumerable.Empty<WEBSERVICE>();
        }

        public async Task<WEBSERVICE> GetByID(decimal id)
        {
            Result<WEBSERVICE> result = id.IsValueValid<WEBSERVICE>(nameof(STOJOU.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                {
                    WEBSERVICE webService = await uow.WebServiceRepository.GetByID(id);

                    uow.Commit();

                    return webService;
                }
            }

            return null;
        }

        public async Task<WEBSERVICE> GetByUniqueKey(string WEBSERVICE_0)
        {
            Result<WEBSERVICE> result = WEBSERVICE_0.IsValueValid<WEBSERVICE>(nameof(WEBSERVICE.CODE_0));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                {
                    WEBSERVICE webServiceModel = await uow.WebServiceRepository.GetByUniqueField(nameof(WEBSERVICE.CODE_0), WEBSERVICE_0);

                    uow.Commit();

                    return webServiceModel;
                }
            }

            return null;
        }

        public async Task<Result<WEBSERVICE>> Insert(WEBSERVICE webService)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
            {
                Result<WEBSERVICE> result = await IsValidInsert(webService);

                if (result.Success)
                {
                    webService.CREUSR_0 = AuthenticatedUser.USR_0;
                    webService.UPDUSR_0 = AuthenticatedUser.USR_0;

                    result = await uow.WebServiceRepository.Insert(webService);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<WEBSERVICE>> Update(WEBSERVICE webService)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
            {
                Result<WEBSERVICE> result = await IsValidUpdate(webService);

                if (result.Success)
                {
                    webService.UPDUSR_0 = AuthenticatedUser.USR_0;

                    result = await uow.WebServiceRepository.Update(webService);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<WEBSERVICE>> IsValidInsert(WEBSERVICE webService)
        {
            // Add more validations if needed
            Result<WEBSERVICE> result = await webService.IsClassValidAsync();

            if (result.Success && (await GetByUniqueKey(webService.CODE_0) != null))
            {
                result.AddInvalidMessage($"Já existe um webservice com o nome {webService.CODE_0}");
            }

            return result;
        }

        public async Task<Result<WEBSERVICE>> IsValidUpdate(WEBSERVICE webService)
        {
            // Add more validations if needed
            Result<WEBSERVICE> result = await webService.IsClassValidAsync();

            if (result.Success)
            {
                WEBSERVICE webServiceTemp = await GetByID(webService.ROWID);

                if (webServiceTemp == null)
                {
                    result.AddInvalidMessage($"Não foi possível encontrar o webservice com o ID {webService.ROWID}.");
                }
                else
                {   
                    if (webService.CODE_0 != webServiceTemp.CODE_0)
                    {
                        result.AddInvalidMessage($"Não é permitido alterar o campo {nameof(webService.CODE_0)}");
                    }
                }
            }

            return result;
        }
    }
}
