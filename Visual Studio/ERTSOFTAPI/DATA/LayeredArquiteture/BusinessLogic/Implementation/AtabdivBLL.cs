﻿namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{
    using System.Linq;
    using MODELS.Helpers;
    using System.Transactions;
    using System.Threading.Tasks;
    using DATA.Helpers.StaticClasses;
    using System.Collections.Generic;
    using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;
    using MODELS.BusinessObject.ERTSOFT.Database;
    using DATA.LayeredArquiteture.Repositories.UnitOfWork;
    using MODELS.BusinessObject.SAGEX3.Database;

    public class AtabdivBLL : IAtabdivBLL
    {
        #region Public Properties
        public USER AuthenticatedUser { get; set; }
        #endregion

        #region Public Methods
        public AtabdivBLL()
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();
        }

        public async Task<IEnumerable<ATABDIV>> GetAll()
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
            {
                IEnumerable<ATABDIV> atabDivList = await uow.AtadivRepository.GetAll();

                uow.Commit();

                return atabDivList;
            }
        }

        public async Task<IEnumerable<ATABDIV>> GetByFields(ATABDIV atabdiv)
        {
            Dictionary<string, object> fields = new Dictionary<string, object>();
            Dictionary<string, object> fieldsTemp = new Dictionary<string, object>
            {
                { nameof(ATABDIV.NUMTAB_0), atabdiv.NUMTAB_0},
                { nameof(ATABDIV.CODE_0), atabdiv.CODE_0 },
                { nameof(ATABDIV.A1_0), atabdiv.A1_0 },
                { nameof(ATABDIV.A2_0), atabdiv.A2_0 },
                { nameof(ATABDIV.A3_0), atabdiv.A3_0 },
                { nameof(ATABDIV.A4_0), atabdiv.A4_0 },
                { nameof(ATABDIV.A5_0), atabdiv.A5_0 },
                { nameof(ATABDIV.A6_0), atabdiv.A6_0 }
            };

            foreach (KeyValuePair<string, object> field in fieldsTemp)
            {
                if (field.Value != null)
                {
                    fields.Add(field.Key, field.Value);
                }
            }

            if (fields.Count > 0)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
                {
                    IEnumerable<ATABDIV> atabdivList = await uow.AtadivRepository.GetByFields(fields);

                    uow.Commit();

                    return atabdivList;
                }
            }

            return Enumerable.Empty<ATABDIV>();
        }

        public async Task<IEnumerable<ATABDIV>> GetByNumtab(short NUMTAB_0)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
            {
                IEnumerable<ATABDIV> atabDivList = await uow.AtadivRepository.GetByField(nameof(ATABDIV.NUMTAB_0), NUMTAB_0);

                uow.Commit();

                return atabDivList;
            }
        }

        public async Task<ATABDIV> GetByID(decimal id)
        {
            Result<ATABDIV> result = id.IsValueValid<ATABDIV>(nameof(ATABDIV.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
                {
                    ATABDIV atabDiv = await uow.AtadivRepository.GetByID(id);

                    uow.Commit();

                    return atabDiv;
                }
            }

            return null;
        }

        public async Task<ATABDIV> GetByUniqueFields(short NUMTAB_0, string CODE_0)
        {
            ATABDIV atextraModel = new ATABDIV
            {
                NUMTAB_0 = NUMTAB_0,
                CODE_0 = CODE_0
            };
            Result<ATABDIV> result = atextraModel.IsPropertiesValid(nameof(ATABDIV.NUMTAB_0), nameof(ATABDIV.CODE_0));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
                {
                    Dictionary<string, object> fields = new Dictionary<string, object>
                    {
                        { nameof(ATABDIV.NUMTAB_0), NUMTAB_0 },
                        { nameof(ATABDIV.CODE_0), CODE_0}
                    };

                    atextraModel = await uow.AtadivRepository.GetByUniqueFields(fields);

                    uow.Commit();

                    return atextraModel;
                }
            }

            return null;
        }
        #endregion
    }
}
