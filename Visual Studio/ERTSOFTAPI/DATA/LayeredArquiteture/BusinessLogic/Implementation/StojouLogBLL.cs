﻿using MODELS.Helpers;
using System.Transactions;
using System.Threading.Tasks;
using DATA.Helpers.StaticClasses;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;
using DATA.LayeredArquiteture.Repositories.UnitOfWork;

namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{
    public class StojouLogBLL : IStojouLogBLL
    {
        public USER AuthenticatedUser { get; set; }

        public StojouLogBLL()
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();
        }

        public async Task<IEnumerable<STOJOULOG>> GetAll()
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Snapshot))
            {
                IEnumerable<STOJOULOG> stojouLogList = await uow.StojouLogRepository.GetAll();

                uow.Commit();

                return stojouLogList;
            }
        }

        public async Task<STOJOULOG> GetByID(decimal id)
        {
            Result<STOJOULOG> result = id.IsValueValid<STOJOULOG>(nameof(STOJOU.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Snapshot))
                {
                    STOJOULOG stojouLog = await uow.StojouLogRepository.GetByID(id);

                    uow.Commit();

                    return stojouLog;
                }
            }

            return null;
        }

        public async Task<Result<STOJOULOG>> Insert(STOJOULOG stojouLog)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Snapshot))
            {
                Result<STOJOULOG> result = await IsValidInsert(stojouLog);

                if (result.Success)
                {
                    stojouLog.CREUSR_0 = AuthenticatedUser.USR_0;
                    stojouLog.UPDUSR_0 = AuthenticatedUser.USR_0;

                    result = await uow.StojouLogRepository.Insert(stojouLog, false);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<STOJOULOG>> IsValidInsert(STOJOULOG stojouLog)
        {
            // Add more validations if needed
            Result<STOJOULOG> result = await stojouLog.IsClassValidAsync();

            return result;
        }
    }
}
