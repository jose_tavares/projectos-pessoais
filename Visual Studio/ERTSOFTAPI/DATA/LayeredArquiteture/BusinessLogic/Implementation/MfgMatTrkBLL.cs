﻿using MODELS.Helpers;
using System.Transactions;
using System.Threading.Tasks;
using DATA.Helpers.StaticClasses;
using System.Collections.Generic;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.Repositories.UnitOfWork;
using MODELS.BusinessObject.SAGEX3.Database;

namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{
    public class MfgMatTrkBLL : IMfgMatTrkBLL
    {
        public USER AuthenticatedUser { get; set; }

        public MfgMatTrkBLL()
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();
        }

        public async Task<IEnumerable<MFGMATTRK>> GetAll()
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
            {
                IEnumerable<MFGMATTRK> mfgMatTrkList = await uow.MfgMatTrkRepository.GetAll();

                uow.Commit();

                return mfgMatTrkList;
            }
        }

        public async Task<IEnumerable<MFGMATTRK>> GetByZESSTROWID(string ZESSTROWID)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
            {
                IEnumerable<MFGMATTRK> mfgMatTrkList = await uow.MfgMatTrkRepository.GetByField(nameof(MFGMATTRK.ZESSTROWID_0), ZESSTROWID);

                uow.Commit();

                return mfgMatTrkList;
            }
        }

        public async Task<MFGMATTRK> GetByID(decimal id)
        {
            Result<MFGMATTRK> result = id.IsValueValid<MFGMATTRK>(nameof(MFGMATTRK.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
                {
                    MFGMATTRK mfgMatTrk = await uow.MfgMatTrkRepository.GetByID(id);

                    uow.Commit();

                    return mfgMatTrk;
                }
            }

            return null;
        }
    }
}
