﻿using MODELS.Helpers;
using System.Transactions;
using System.Threading.Tasks;
using DATA.Helpers.StaticClasses;
using System.Collections.Generic;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.Repositories.UnitOfWork;
using MODELS.BusinessObject.SAGEX3.Database;

namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{
    public class MfgMatTrkBLL : IMfgMatTrkBLL
    {
        public USER AuthenticatedUser { get; set; }

        public MfgMatTrkBLL()
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();
        }

        #region Public Methods

        public async Task<Result<IEnumerable<MFGMATTRK>>> GetAll()
        {
            Result<IEnumerable<MFGMATTRK>> result = new Result<IEnumerable<MFGMATTRK>>(true);

            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
            {
                result.Obj = await uow.MfgMatTrkRepository.GetAll();

                uow.Commit();
            }

            if (!result.Obj.IsValid())
            {
                result.NotFound();
            }

            return result;
        }

        public async Task<Result<IEnumerable<MFGMATTRK>>> GetByZESSTROWID(string ZESSTROWID)
        {
            Result<IEnumerable<MFGMATTRK>> result = ZESSTROWID.IsValueValid<IEnumerable<MFGMATTRK>>(nameof(MFGMATTRK.ZESSTROWID_0));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
                {
                    result.Obj = await uow.MfgMatTrkRepository.GetByField(nameof(MFGMATTRK.ZESSTROWID_0), ZESSTROWID);

                    uow.Commit();
                }

                if (!result.Obj.IsValid())
                {
                    result.NotFound(nameof(MFGMATTRK.ZESSTROWID_0), ZESSTROWID);
                }
            }

            return result;
        }

        public async Task<Result<MFGMATTRK>> GetById(int id)
        {
            Result<MFGMATTRK> result = id.IsValueValid<MFGMATTRK>(nameof(MFGMATTRK.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
                {
                    result.Obj = await uow.MfgMatTrkRepository.GetById(id);

                    uow.Commit();
                }

                if (result.Obj == null)
                {
                    result.NotFound(id);
                }
            }

            return result;
        }
        #endregion
    }
}
