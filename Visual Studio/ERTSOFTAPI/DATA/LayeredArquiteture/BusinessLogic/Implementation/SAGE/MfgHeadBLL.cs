﻿using DATA.Helpers.StaticClasses;
using System.Collections.Generic;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;
using System.Threading.Tasks;
using System.Transactions;
using MODELS.Helpers;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.Repositories.UnitOfWork;
using MODELS.BusinessObject.SAGEX3.Database;

namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{
    public class MfgHeadBLL : IMfgHeadBLL
    {
        public USER AuthenticatedUser { get; set; }

        public MfgHeadBLL()
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();
        }

        #region Public Methods

        public async Task<Result<IEnumerable<MFGHEAD>>> GetAll()
        {
            Result<IEnumerable<MFGHEAD>> result = new Result<IEnumerable<MFGHEAD>>(true);

            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
            {
                result.Obj = await uow.MfgHeadRepository.GetAll();

                uow.Commit();
            }

            if (!result.Obj.IsValid())
            {
                result.NotFound();
            }

            return result;
        }

        public async Task<Result<MFGHEAD>> GetById(int id)
        {
            Result<MFGHEAD> result = id.IsValueValid<MFGHEAD>(nameof(MFGHEAD.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
                {
                    result.Obj = await uow.MfgHeadRepository.GetById(id);

                    uow.Commit();
                }

                if (result.Obj == null)
                {
                    result.NotFound(id);
                }
            }

            return result;
        }

        public async Task<Result<MFGHEAD>> GetByUniqueField(string MFGNUM_0)
        {
            Result<MFGHEAD> result = MFGNUM_0.IsValueValid<MFGHEAD>(nameof(MFGHEAD.MFGNUM_0));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
                {
                    result.Obj = await uow.MfgHeadRepository.GetByUniqueField(nameof(MFGHEAD.MFGNUM_0), MFGNUM_0);

                    uow.Commit();
                }

                if (result.Obj == null)
                {
                    result.NotFound(nameof(MFGHEAD.MFGNUM_0), MFGNUM_0);
                }
            }

            return result;
        }

        #endregion
    }
}
