﻿using MODELS.Helpers;
using System.Transactions;
using System.Threading.Tasks;
using System.Collections.Generic;
using DATA.Helpers.StaticClasses;
using MODELS.BusinessObject.ERTSOFT.Database;
using MODELS.BusinessObject.SAGEX3.Database;
using DATA.LayeredArquiteture.Repositories.UnitOfWork;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;

namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{
    public class MfgItmBLL : IMfgItmBLL
    {
        public USER AuthenticatedUser { get; set; }

        public MfgItmBLL()
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();
        }

        #region Public Methods

        public async Task<Result<IEnumerable<MFGITM>>> GetAll()
        {
            Result<IEnumerable<MFGITM>> result = new Result<IEnumerable<MFGITM>>(true);

            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
            {
                result.Obj = await uow.MfgItmRepository.GetAll();

                uow.Commit();
            }

            if (!result.Obj.IsValid())
            {
                result.NotFound();
            }

            return result;
        }

        public async Task<Result<MFGITM>> GetById(int id)
        {
            Result<MFGITM> result = id.IsValueValid<MFGITM>(nameof(MFGITM.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
                {
                    result.Obj = await uow.MfgItmRepository.GetById(id);

                    uow.Commit();
                }

                if (result.Obj == null)
                {
                    result.NotFound(id);
                }
            }

            return result;
        }

        public async Task<Result<MFGITM>> GetByUniqueField(string MFGNUM_0)
        {
            Result<MFGITM> result = MFGNUM_0.IsValueValid<MFGITM>(nameof(MFGITM.MFGNUM_0));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
                {
                    result.Obj = await uow.MfgItmRepository.GetByUniqueField(nameof(MFGITM.MFGNUM_0), MFGNUM_0);

                    uow.Commit();
                }

                if (result.Obj == null)
                {
                    result.NotFound(nameof(MFGITM.MFGNUM_0), MFGNUM_0);
                }
            }

            return result;
        }
        #endregion
    }
}
