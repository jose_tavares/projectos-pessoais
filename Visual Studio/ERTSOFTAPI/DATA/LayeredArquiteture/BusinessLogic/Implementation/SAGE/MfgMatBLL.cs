﻿using System;
using MODELS.Helpers;
using System.Transactions;
using System.Threading.Tasks;
using DATA.Helpers.StaticClasses;
using System.Collections.Generic;
using MODELS.BusinessObject.SAGEX3.Database;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.Repositories.UnitOfWork;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;

namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{
    public class MfgMatBLL : IMfgMatBLL
    {
        public USER AuthenticatedUser { get; set; }

        public MfgMatBLL()
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();
        }

        #region Public Methods

        public async Task<Result<IEnumerable<MFGMAT>>> GetAll()
        {
            Result<IEnumerable<MFGMAT>> result = new Result<IEnumerable<MFGMAT>>(true);

            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
            {
                result.Obj = await uow.MfgMatRepository.GetAll();

                uow.Commit();
            }

            if (!result.Obj.IsValid())
            {
                result.NotFound();
            }

            return result;
        }

        public async Task<Result<IEnumerable<MFGMAT>>> GetByMfgnum(string MFGNUM_0, bool STOMGTCOD_0)
        {
            Result<IEnumerable<MFGMAT>> result = MFGNUM_0.IsValueValid<IEnumerable<MFGMAT>>(nameof(MFGMAT.MFGNUM_0));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
                {
                    if (STOMGTCOD_0)
                    {
                        result.Obj = await uow.MfgMatRepository.GetByMfgnum(MFGNUM_0);
                    }
                    else
                    {
                        result.Obj = await uow.MfgMatRepository.GetByField(nameof(MFGMAT.MFGNUM_0), MFGNUM_0);
                    }

                    uow.Commit();
                }

                if (!result.Obj.IsValid())
                {
                    result.NotFound(nameof(MFGMAT.MFGNUM_0), MFGNUM_0);
                }
            }

            return result;
        }

        public async Task<Result<IEnumerable<MFGMAT>>> GetByMfgnumAndOperationAndProdType(string MFGNUM_0, string ZOPERCON_0, bool PRODOK_0)
        {
            MFGMAT mfgmat = new MFGMAT
            {
                MFGNUM_0 = MFGNUM_0,
                ZOPERCON_0 = ZOPERCON_0
            };

            if (PRODOK_0)
            {
                mfgmat.ZCONSPRDOK_0 = 2;
                mfgmat.ZCONSPRDNOK_0 = 1;
            }
            else
            {
                mfgmat.ZCONSPRDOK_0 = 1;
                mfgmat.ZCONSPRDNOK_0 = 2;
            }

            Result<Dictionary<string, object>> resultFields = ClassHelper.GetDictionaryFieldsFromObject(mfgmat, nameof(MFGMAT.MFGNUM_0), nameof(MFGMAT.ZOPERCON_0), nameof(MFGMAT.ZOPERCON_0), nameof(MFGMAT.ZCONSPRDOK_0), nameof(MFGMAT.ZCONSPRDOK_0));
            Result<IEnumerable<MFGMAT>> result = Result<IEnumerable<MFGMAT>>.ConvertAndGetFromResult(resultFields);

            if (result.Success)
            {
                Dictionary<string, object> fields = resultFields.Obj;

                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
                {
                    result.Obj = await uow.MfgMatRepository.GetByMfgnumAndOperationAndProdType(MFGNUM_0, ZOPERCON_0, PRODOK_0);

                    uow.Commit();
                }

                if (!result.Obj.IsValid())
                {
                    result.NotFound(fields);
                }
            }

            return result;      
        }

        public async Task<Result<MFGMAT>> GetById(int id)
        {
            Result<MFGMAT> result = id.IsValueValid<MFGMAT>(nameof(MFGMAT.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
                {
                    result.Obj = await uow.MfgMatRepository.GetById(id);

                    uow.Commit();
                }

                if (result.Obj == null)
                {
                    result.NotFound(id);
                }
            }

            return result;
        }

        #endregion
    }
}
