﻿using MODELS.Helpers;
using System.Transactions;
using System.Threading.Tasks;
using DATA.Helpers.StaticClasses;
using System.Collections.Generic;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.Repositories.UnitOfWork;
using MODELS.BusinessObject.SAGEX3.Database;

namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{
    public class TabUnitBLL : ITabUnitBLL
    {
        public USER AuthenticatedUser { get; set; }

        public TabUnitBLL()
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();
        }

        #region Public Methods

        public async Task<Result<IEnumerable<TABUNIT>>> GetAll()
        {
            Result<IEnumerable<TABUNIT>> result = new Result<IEnumerable<TABUNIT>>(true);

            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
            {
                result.Obj = await uow.TabUnitRepository.GetAll();

                uow.Commit();
            }

            if (!result.Obj.IsValid())
            {
                result.NotFound();
            }

            return result;
        }

        public async Task<Result<TABUNIT>> GetById(int id)
        {
            Result<TABUNIT> result = id.IsValueValid<TABUNIT>(nameof(TABUNIT.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
                {
                    result.Obj = await uow.TabUnitRepository.GetById(id);

                    uow.Commit();
                }

                if (result.Obj == null)
                {
                    result.NotFound(id);
                }
            }

            return result;
        }

        public async Task<Result<TABUNIT>> GetByUniqueField(string UOM_0)
        {
            Result<TABUNIT> result = UOM_0.IsValueValid<TABUNIT>(nameof(TABUNIT.UOM_0));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
                {
                    result.Obj = await uow.TabUnitRepository.GetByUniqueField(nameof(TABUNIT.UOM_0), UOM_0);

                    uow.Commit();
                }

                if (result.Obj == null)
                {
                    result.NotFound(nameof(TABUNIT.UOM_0), UOM_0);
                }
            }

            return result;
        }
        #endregion
    }
}
