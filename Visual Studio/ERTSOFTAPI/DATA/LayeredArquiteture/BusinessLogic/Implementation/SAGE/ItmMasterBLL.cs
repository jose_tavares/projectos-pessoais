﻿using MODELS.Helpers;
using System.Transactions;
using System.Threading.Tasks;
using DATA.Helpers.StaticClasses;
using System.Collections.Generic;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.Repositories.UnitOfWork;
using MODELS.BusinessObject.SAGEX3.Database;

namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{
    public class ItmMasterBLL : IItmMasterBLL
    {
        public USER AuthenticatedUser { get; set; }

        public ItmMasterBLL()
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();
        }

        #region Public Methods

        public async Task<Result<IEnumerable<ITMMASTER>>> GetAll()
        {
            Result<IEnumerable<ITMMASTER>> result = new Result<IEnumerable<ITMMASTER>>(true);

            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
            {
                result.Obj = await uow.ItmMasterRepository.GetAll();

                uow.Commit();
            }

            if (!result.Obj.IsValid())
            {
                result.NotFound();
            }

            return result;
        }

        public async Task<Result<ITMMASTER>> GetById(int id)
        {
            Result<ITMMASTER> result = id.IsValueValid<ITMMASTER>(nameof(ITMMASTER.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
                {
                    result.Obj = await uow.ItmMasterRepository.GetById(id);

                    uow.Commit();
                }

                if (result.Obj == null)
                {
                    result.NotFound(id);
                }
            }

            return result;
        }

        public async Task<Result<ITMMASTER>> GetByUniqueField(string ITMREF_0)
        {
            Result<ITMMASTER> result = ITMREF_0.IsValueValid<ITMMASTER>(nameof(ITMMASTER.ITMREF_0));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
                {
                    result.Obj = await uow.ItmMasterRepository.GetByUniqueField(nameof(ITMMASTER.ITMREF_0), ITMREF_0);

                    uow.Commit();
                }

                if (result.Obj == null)
                {
                    result.NotFound(nameof(ITMMASTER.ITMREF_0), ITMREF_0);
                }
            }

            return result;
        }
        #endregion
    }
}
