﻿namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{
    using MODELS.Helpers;
    using System.Transactions;
    using System.Threading.Tasks;
    using DATA.Helpers.StaticClasses;
    using System.Collections.Generic;
    using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;
    using MODELS.BusinessObject.ERTSOFT.Database;
    using DATA.LayeredArquiteture.Repositories.UnitOfWork;
    using MODELS.BusinessObject.SAGEX3.Database;

    public class AtextraBLL : IAtextraBLL
    {
        public USER AuthenticatedUser { get; set; }

        public AtextraBLL()
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();
        }

        #region Public Methods
        public async Task<Result<IEnumerable<ATEXTRA>>> GetAll()
        {
            Result<IEnumerable<ATEXTRA>> result = new Result<IEnumerable<ATEXTRA>>(true);

            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
            {
                result.Obj = await uow.AtextraRepository.GetAll();

                uow.Commit();
            }

            if (!result.Obj.IsValid())
            {
                result.NotFound();
            }

            return result;
        }

        public async Task<Result<ATEXTRA>> GetById(int id)
        {
            Result<ATEXTRA> result = id.IsValueValid<ATEXTRA>(nameof(ATEXTRA.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
                {
                    result.Obj = await uow.AtextraRepository.GetById(id);

                    uow.Commit();
                }

                if (result.Obj == null)
                {
                    result.NotFound(id);
                }
            }

            return result;
        }

        public async Task<Result<ATEXTRA>> GetByUniqueFields(string CODFIC_0, string ZONE_0, string LANGUE_0, string IDENT_1, string IDENT_2)
        {
            Result<ATEXTRA> result = new Result<ATEXTRA>(true);
            Result<Dictionary<string, object>> resultFields = ClassHelper.GetDictionaryFieldsFromObject(new ATEXTRA()
            {
                CODFIC_0 = CODFIC_0,
                ZONE_0 = ZONE_0,
                LANGUE_0 = LANGUE_0,
                IDENT1_0 = IDENT_1,
                IDENT2_0 = IDENT_2,
            }, nameof(ATEXTRA.CODFIC_0), nameof(ATEXTRA.ZONE_0), nameof(ATEXTRA.LANGUE_0), nameof(ATEXTRA.IDENT1_0), nameof(ATEXTRA.IDENT2_0));

            result.ConvertFromResult(resultFields);

            if (!resultFields.Success)
            {
                return result;
            }

            Dictionary<string, object> fields = resultFields.Obj;

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
                {
                    result.Obj = await uow.AtextraRepository.GetByUniqueFields(fields);

                    uow.Commit();
                }

                if (result.Obj == null)
                {
                    result.NotFound(fields);
                }
            }

            return result;
        }
        #endregion
    }
}
