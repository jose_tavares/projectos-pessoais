﻿using System.Linq;
using MODELS.Helpers;
using System.Transactions;
using System.Threading.Tasks;
using DATA.Helpers.StaticClasses;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;
using DATA.LayeredArquiteture.Repositories.UnitOfWork;
using MODELS.BusinessObject.SAGEX3.Database;

namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{
    public class StockBLL : IStockBLL
    {
        public USER AuthenticatedUser { get; set; }

        public StockBLL()
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();
        }

        #region Public Methods

        public async Task<Result<IEnumerable<STOCK>>> GetAll()
        {
            Result<IEnumerable<STOCK>> result = new Result<IEnumerable<STOCK>>(true);

            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
            {
                result.Obj = await uow.StockRepository.GetAll();

                uow.Commit();
            }

            if (!result.Obj.IsValid())
            {
                result.NotFound();
            }

            return result;
        }

        public async Task<Result<IEnumerable<STOCK>>> GetByITMREF(string ITMREF_0)
        {
            Result<IEnumerable<STOCK>> result = ITMREF_0.IsValueValid<IEnumerable<STOCK>>(nameof(STOCK.ITMREF_0));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
                {
                    result.Obj = await uow.StockRepository.GetByField(nameof(STOCK.ITMREF_0), ITMREF_0);

                    uow.Commit();
                }

                if (!result.Obj.IsValid())
                {
                    result.NotFound(nameof(STOCK.ITMREF_0), ITMREF_0);
                }
            }

            return result;
        }

        public async Task<Result<IEnumerable<STOCK>>> GetByFields(STOCK stock, params string[] filterFields)
        {
            Result<Dictionary<string, object>> resultFields = ClassHelper.GetDictionaryFieldsFromObject(stock, filterFields);
            Result<IEnumerable<STOCK>> result = Result<IEnumerable<STOCK>>.ConvertAndGetFromResult(resultFields);

            if (result.Success)
            {
                Dictionary<string, object> fields = resultFields.Obj;

                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
                {
                    result.Obj = await uow.StockRepository.GetByFields(fields);

                    uow.Commit();
                }

                if (!result.Obj.IsValid())
                {
                    result.NotFound(fields);
                }
            }

            return result;
        }

        public async Task<Result<IEnumerable<STOCK>>> GetByFieldsWithStolot(STOCK stock, params string[] filterFields)
        {
            Result<Dictionary<string, object>> resultFields = ClassHelper.GetDictionaryFieldsFromObject(stock, filterFields);
            Result<IEnumerable<STOCK>> result = Result<IEnumerable<STOCK>>.ConvertAndGetFromResult(resultFields);

            if (result.Success)
            {
                Dictionary<string, object> fields = resultFields.Obj;

                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
                {
                    result.Obj = await uow.StockRepository.GetByFieldsWithStolot(fields);

                    uow.Commit();
                }

                if (!result.Obj.IsValid())
                {
                    result.NotFound(fields);
                }
            }

            return result;
        }

        public async Task<Result<STOCK>> GetById(int id)
        {
            Result<STOCK> result = id.IsValueValid<STOCK>(nameof(STOCK.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
                {
                    result.Obj = await uow.StockRepository.GetById(id);

                    uow.Commit();
                }

                if (result.Obj == null)
                {
                    result.NotFound(id);
                }
            }

            return result;
        }
        #endregion
    }
}
