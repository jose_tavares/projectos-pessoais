﻿using MODELS.Helpers;
using System.Transactions;
using System.Threading.Tasks;
using DATA.Helpers.StaticClasses;
using System.Collections.Generic;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.Repositories.UnitOfWork;
using MODELS.BusinessObject.SAGEX3.Database;

namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{
    public class MfgItmTrkBLL : IMfgItmTrkBLL
    {
        public USER AuthenticatedUser { get; set; }

        public MfgItmTrkBLL()
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();
        }

        #region Public Methods

        public async Task<Result<IEnumerable<MFGITMTRK>>> GetAll()
        {
            Result<IEnumerable<MFGITMTRK>> result = new Result<IEnumerable<MFGITMTRK>>(true);

            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
            {
                result.Obj = await uow.MfgItmTrkRepository.GetAll();

                uow.Commit();
            }

            if (!result.Obj.IsValid())
            {
                result.NotFound();
            }

            return result;
        }

        public async Task<Result<MFGITMTRK>> GetById(int id)
        {
            Result<MFGITMTRK> result = id.IsValueValid<MFGITMTRK>(nameof(MFGITMTRK.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
                {
                    result.Obj = await uow.MfgItmTrkRepository.GetById(id);

                    uow.Commit();
                }

                if (result.Obj == null)
                {
                    result.NotFound(id);
                }
            }

            return result;
        }

        public async Task<Result<IEnumerable<MFGITMTRK>>> GetByZESSTROWID(string ZESSTROWID)
        {
            Result<IEnumerable<MFGITMTRK>> result = ZESSTROWID.IsValueValid<IEnumerable<MFGITMTRK>>(nameof(ATABDIV.NUMTAB_0));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
                {
                    result.Obj = await uow.MfgItmTrkRepository.GetByField(nameof(MFGITMTRK.ZESSTROWID_0), ZESSTROWID);

                    uow.Commit();
                }

                if (!result.Obj.IsValid())
                {
                    result.NotFound(nameof(MFGITMTRK.ZESSTROWID_0), ZESSTROWID);
                }
            }

            return result;
        }
        #endregion
    }
}
