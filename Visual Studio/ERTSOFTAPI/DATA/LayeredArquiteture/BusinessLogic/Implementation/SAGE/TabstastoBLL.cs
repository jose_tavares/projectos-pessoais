﻿using MODELS.Helpers;
using System.Transactions;
using System.Threading.Tasks;
using DATA.Helpers.StaticClasses;
using System.Collections.Generic;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.Repositories.UnitOfWork;
using MODELS.BusinessObject.SAGEX3.Database;

namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{
    public class TabstastoBLL : ITabstastoBLL
    {
        public USER AuthenticatedUser { get; set; }

        public TabstastoBLL()
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();
        }

        #region Public Methods

        public async Task<Result<IEnumerable<TABSTASTO>>> GetAll()
        {
            Result<IEnumerable<TABSTASTO>> result = new Result<IEnumerable<TABSTASTO>>(true);

            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
            {
                result.Obj = await uow.TabstastoRepository.GetAll();

                uow.Commit();
            }

            if (!result.Obj.IsValid())
            {
                result.NotFound();
            }

            return result;
        }

        public async Task<Result<TABSTASTO>> GetById(int id)
        {
            Result<TABSTASTO> result = id.IsValueValid<TABSTASTO>(nameof(TABSTASTO.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
                {
                    result.Obj = await uow.TabstastoRepository.GetById(id);

                    uow.Commit();
                }

                if (result.Obj == null)
                {
                    result.NotFound(id);
                }
            }

            return result;
        }

        public async Task<Result<TABSTASTO>> GetByUniqueField(string STA_0)
        {
            Result<TABSTASTO> result = STA_0.IsValueValid<TABSTASTO>(nameof(TABSTASTO.STASTO_0));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
                {
                    result.Obj = await uow.TabstastoRepository.GetByUniqueField(nameof(TABSTASTO.STASTO_0), STA_0);

                    uow.Commit();
                }

                if (result.Obj == null)
                {
                    result.NotFound(nameof(TABSTASTO.STASTO_0), STA_0);
                }
            }

            return result;
        }
        #endregion
    }
}
