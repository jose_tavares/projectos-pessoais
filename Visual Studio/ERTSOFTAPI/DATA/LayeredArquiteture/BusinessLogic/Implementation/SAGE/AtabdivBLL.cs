﻿namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{
    using MODELS.Helpers;
    using System.Transactions;
    using System.Threading.Tasks;
    using DATA.Helpers.StaticClasses;
    using System.Collections.Generic;
    using MODELS.BusinessObject.SAGEX3.Database;
    using MODELS.BusinessObject.ERTSOFT.Database;
    using DATA.LayeredArquiteture.Repositories.UnitOfWork;
    using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;

    public class AtabdivBLL : IAtabdivBLL
    {
        public USER AuthenticatedUser { get; set; }

        public AtabdivBLL()
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();
        }

        #region Public Methods
        public async Task<Result<IEnumerable<ATABDIV>>> GetAll()
        {
            Result<IEnumerable<ATABDIV>> result = new Result<IEnumerable<ATABDIV>>(true);

            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
            {
                result.Obj = await uow.AtabdivRepository.GetAll();

                uow.Commit();
            }

            if (!result.Obj.IsValid())
            {
                result.NotFound();
            }

            return result;
        }

        public async Task<Result<IEnumerable<ATABDIV>>> GetByFields(ATABDIV atabdiv, params string[] filterFields)
        {
            Result<Dictionary<string, object>> resultFields = ClassHelper.GetDictionaryFieldsFromObject(atabdiv, filterFields);
            Result<IEnumerable<ATABDIV>> result = Result<IEnumerable<ATABDIV>>.ConvertAndGetFromResult(resultFields);

            if (result.Success)
            {
                Dictionary<string, object> fields = resultFields.Obj;

                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
                {
                    result.Obj = await uow.AtabdivRepository.GetByFields(fields);

                    uow.Commit();
                }

                if (!result.Obj.IsValid())
                {
                    result.NotFound(fields);
                }
            }

            return result;
        }

        public async Task<Result<IEnumerable<ATABDIV>>> GetByNumtab(short NUMTAB_0)
        {
            Result<IEnumerable<ATABDIV>> result = NUMTAB_0.IsValueValid<IEnumerable<ATABDIV>>(nameof(ATABDIV.NUMTAB_0));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
                {
                    result.Obj = await uow.AtabdivRepository.GetByField(nameof(ATABDIV.NUMTAB_0), NUMTAB_0);

                    uow.Commit();
                }

                if (!result.Obj.IsValid())
                {
                    result.NotFound(nameof(ATABDIV.NUMTAB_0), NUMTAB_0);
                }
            }

            return result;
        }

        public async Task<Result<ATABDIV>> GetById(int id)
        {
            Result<ATABDIV> result = id.IsValueValid<ATABDIV>(nameof(ATABDIV.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
                {
                    result.Obj = await uow.AtabdivRepository.GetById(id);

                    uow.Commit();
                }

                if (result.Obj == null)
                {
                    result.NotFound(id);
                }
            }

            return result;
        }

        public async Task<Result<ATABDIV>> GetByUniqueFields(short NUMTAB_0, string CODE_0)
        {
            Result<Dictionary<string, object>> resultFields = ClassHelper.GetDictionaryFieldsFromObject(new ATABDIV()
            {
                NUMTAB_0 = NUMTAB_0,
                CODE_0 = CODE_0
            }, nameof(ATABDIV.NUMTAB_0), nameof(ATABDIV.CODE_0));
            Result<ATABDIV> result = Result<ATABDIV>.ConvertAndGetFromResult(resultFields);

            if (result.Success)
            {
                Dictionary<string, object> fields = resultFields.Obj;

                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
                {
                    result.Obj = await uow.AtabdivRepository.GetByUniqueFields(fields);

                    uow.Commit();
                }

                if (result.Obj == null)
                {
                    result.NotFound(fields);
                }
            }

            return result;
        }
        #endregion
    }
}
