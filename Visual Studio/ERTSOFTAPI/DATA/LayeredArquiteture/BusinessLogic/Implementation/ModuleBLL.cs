﻿using MODELS.Helpers;
using System.Transactions;
using System.Threading.Tasks;
using DATA.Helpers.StaticClasses;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;
using DATA.LayeredArquiteture.Repositories.UnitOfWork;

namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{
    public class ModuleBLL : IModuleBLL
    {
        public USER AuthenticatedUser { get; set; }

        public ModuleBLL()
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();
        }

        public async Task<IEnumerable<MODULE>> GetAll()
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Snapshot))
            {
                IEnumerable<MODULE> moduleList = await uow.ModuleRepository.GetAll();

                uow.Commit();

                return moduleList;
            }
        }

        public async Task<IEnumerable<MODULE>> GetByApp(string APP_0)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
            {
                IEnumerable<MODULE> moduleList = await uow.ModuleRepository.GetByField(nameof(MODULE.APP_0), APP_0);

                uow.Commit();

                return moduleList;
            }
        }

        public async Task<IEnumerable<MODULE>> GetByModule(string MODULE_0)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
            {
                IEnumerable<MODULE> moduleList = await uow.ModuleRepository.GetByField(nameof(MODULE.MODULE_0), MODULE_0);

                uow.Commit();

                return moduleList;
            }
        }

        public async Task<MODULE> GetByID(decimal id)
        {
            Result<MODULE> result = id.IsValueValid<MODULE>(nameof(MODULE.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                {
                    MODULE module = await uow.ModuleRepository.GetByID(id);

                    uow.Commit();

                    return module;
                }
            }

            return null;
        }

        public async Task<MODULE> GetByUniqueFields(string APP_0, string MODULE_0)
        {
            MODULE moduleModel = new MODULE
            {
                APP_0 = APP_0,
                MODULE_0 = MODULE_0
            };

            Result<MODULE> result = moduleModel.IsPropertiesValid(nameof(MODULE.APP_0), nameof(MODULE.MODULE_0));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                {
                    moduleModel = await uow.ModuleRepository.GetByUniqueFields(new Dictionary<string, object>
                    {
                        { nameof(MODULE.APP_0), APP_0 },
                        { nameof(MODULE.MODULE_0), MODULE_0 }
                    });

                    uow.Commit();

                    return moduleModel;
                }
            }

            return null;
        }

        public async Task<MODULE> GetByUniqueFieldsWithScreenList(string APP_0, string MODULE_0)
        {
            MODULE moduleModel = new MODULE
            {
                APP_0 = APP_0,
                MODULE_0 = MODULE_0
            };

            Result<MODULE> result = moduleModel.IsPropertiesValid(nameof(MODULE.APP_0), nameof(MODULE.MODULE_0));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                {
                    moduleModel = await uow.ModuleRepository.GetByUniqueFieldsWithScreenList(APP_0, MODULE_0);

                    uow.Commit();

                    return moduleModel;
                }
            }

            return null;
        }

        public async Task<Result<MODULE>> Insert(MODULE module)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
            {
                Result<MODULE> result = await IsValidInsert(module);

                if (result.Success)
                {
                    module.CREUSR_0 = AuthenticatedUser.USR_0;
                    module.UPDUSR_0 = AuthenticatedUser.USR_0;

                    result = await uow.ModuleRepository.Insert(module);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<MODULE>> Update(MODULE module)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
            {
                Result<MODULE> result = await IsValidUpdate(module);

                if (result.Success)
                {
                    module.UPDUSR_0 = AuthenticatedUser.USR_0;

                    result = await uow.ModuleRepository.Update(module);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<MODULE>> IsValidInsert(MODULE module)
        {
            // Add more validations if needed
            Result<MODULE> result = await module.IsClassValidAsync();

            if (result.Success)
            {
                MODULE moduleTemp = await GetByUniqueFields(module.APP_0, module.MODULE_0);

                if (moduleTemp != null)
                {
                    result.AddInvalidMessage($"Já existe um módulo com o nome {module.MODULE_0} associado à aplicação {module.APP_0}");
                }
            }

            return result;
        }

        public async Task<Result<MODULE>> IsValidUpdate(MODULE module)
        {
            // Add more validations if needed
            Result<MODULE> result = await module.IsClassValidAsync();

            if (result.Success)
            {
                MODULE moduleTemp = await GetByID(module.ROWID);

                if (moduleTemp == null)
                {
                    result.AddInvalidMessage($"Não foi possível encontrar o modulo com o ID {module.ROWID}.");
                }
                else
                {
                    if (module.MODULE_0 != moduleTemp.MODULE_0)
                    {
                        result.AddInvalidMessage($"Não é permitido alterar o campo {nameof(module.MODULE_0)}");
                    }

                    if (module.APP_0 != moduleTemp.APP_0) //Se a app está para ser alterada verifico se a combinação app + modulo já existem
                    {
                        moduleTemp = await GetByUniqueFields(module.APP_0, module.MODULE_0);

                        if (moduleTemp != null)
                        {
                            result.AddInvalidMessage($"Já existe um módulo com o nome {module.MODULE_0} associado à aplicação {module.APP_0}");
                        }
                    }
                }
            }

            return result;
        }
    }
}
