﻿using MODELS.Helpers;
using System.Transactions;
using System.Threading.Tasks;
using DATA.Helpers.StaticClasses;
using System.Collections.Generic;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.Repositories.UnitOfWork;
using MODELS.BusinessObject.SAGEX3.Database;

namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{
    public class ItmMasterBLL : IItmMasterBLL
    {
        public USER AuthenticatedUser { get; set; }

        public ItmMasterBLL()
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();
        }

        public async Task<IEnumerable<ITMMASTER>> GetAll()
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Snapshot))
            {
                IEnumerable<ITMMASTER> itmMasterList = await uow.ItmMasterRepository.GetAll();

                uow.Commit();

                return itmMasterList;
            }
        }

        public async Task<ITMMASTER> GetByUniqueKey(string ITMREF_0)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
            {
                ITMMASTER itmMaster = await uow.ItmMasterRepository.GetByUniqueField(nameof(ITMMASTER.ITMREF_0), ITMREF_0);

                uow.Commit();

                return itmMaster;
            }
        }

        public async Task<ITMMASTER> GetByID(decimal id)
        {
            Result<ITMMASTER> result = id.IsValueValid<ITMMASTER>(nameof(ITMMASTER.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                {
                    ITMMASTER itmMaster = await uow.ItmMasterRepository.GetByID(id);

                    uow.Commit();

                    return itmMaster;
                }
            }

            return null;
        }
    }
}
