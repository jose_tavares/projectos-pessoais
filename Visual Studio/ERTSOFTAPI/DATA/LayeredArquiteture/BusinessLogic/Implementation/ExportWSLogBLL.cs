﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Transactions;
using DATA.Helpers.StaticClasses;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;
using DATA.LayeredArquiteture.Repositories.UnitOfWork;
using MODELS.BusinessObject.ERTSOFT.Database;
using MODELS.Helpers;

namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{
    public class ExportWSLogBLL : IExportWSLogBLL
    {
        public USER AuthenticatedUser { get; set; }

        public ExportWSLogBLL()
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();
        }

        public async Task<IEnumerable<EXPORTWSLOG>> GetAll()
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Snapshot))
            {
                IEnumerable<EXPORTWSLOG> EXPORTWSLOGList = await uow.ExportWSLogRepository.GetAll();

                uow.Commit();

                return EXPORTWSLOGList;
            }
        }

        public async Task<EXPORTWSLOG> GetByID(decimal id)
        {
            Result<EXPORTWSLOG> result = id.IsValueValid<EXPORTWSLOG>(nameof(EXPORTWSLOG.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                {
                    EXPORTWSLOG EXPORTWSLOG = await uow.ExportWSLogRepository.GetByID(id);

                    uow.Commit();

                    return EXPORTWSLOG;
                }
            }

            return null;
        }

        public async Task<Result<EXPORTWSLOG>> Insert(EXPORTWSLOG exportWSLog)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
            {
                Result<EXPORTWSLOG> result = await IsValidInsert(exportWSLog);

                if (result.Success)
                {
                    exportWSLog.CREUSR_0 = AuthenticatedUser.USR_0;
                    exportWSLog.UPDUSR_0 = AuthenticatedUser.USR_0;

                    result = await uow.ExportWSLogRepository.Insert(exportWSLog);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<EXPORTWSLOG>> Update(EXPORTWSLOG exportWSLog)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
            {
                Result<EXPORTWSLOG> result = await IsValidUpdate(exportWSLog);

                if (result.Success)
                {
                    exportWSLog.UPDUSR_0 = AuthenticatedUser.USR_0;

                    result = await uow.ExportWSLogRepository.Update(exportWSLog);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<EXPORTWSLOG>> IsValidInsert(EXPORTWSLOG EXPORTWSLOG)
        {
            // Add more validations if needed
            Result<EXPORTWSLOG> result = await EXPORTWSLOG.IsClassValidAsync();

            return result;
        }

        public async Task<Result<EXPORTWSLOG>> IsValidUpdate(EXPORTWSLOG EXPORTWSLOG)
        {
            // Add more validations if needed
            Result<EXPORTWSLOG> result = await EXPORTWSLOG.IsClassValidAsync();

            return result;
        }
    }
}
