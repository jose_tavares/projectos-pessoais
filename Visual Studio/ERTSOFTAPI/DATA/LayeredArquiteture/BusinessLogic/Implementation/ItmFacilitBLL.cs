﻿using System.Linq;
using MODELS.Helpers;
using System.Transactions;
using System.Threading.Tasks;
using DATA.Helpers.StaticClasses;
using System.Collections.Generic;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.Repositories.UnitOfWork;
using MODELS.BusinessObject.SAGEX3.Database;

namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{
    public class ItmFacilitBLL : IItmFacilitBLL
    {
        public USER AuthenticatedUser { get; set; }

        public ItmFacilitBLL()
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();
        }

        public async Task<IEnumerable<ITMFACILIT>> GetAll()
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
            {
                IEnumerable<ITMFACILIT> itmFacilitList = await uow.ItmFacilitRepository.GetAll();

                uow.Commit();

                return itmFacilitList;
            }
        }

        public async Task<IEnumerable<ITMFACILIT>> GetByFields(ITMFACILIT itmFacilit)
        {
            Dictionary<string, object> fields = new Dictionary<string, object>();
            Dictionary<string, object> fieldsTemp = new Dictionary<string, object>
            {
                { nameof(ITMFACILIT.STOFCY_0), itmFacilit.STOFCY_0},
                { nameof(ITMFACILIT.ITMREF_0), itmFacilit.ITMREF_0}
            };

            foreach (KeyValuePair<string, object> field in fieldsTemp)
            {
                if (field.Value != null)
                {
                    fields.Add(field.Key, field.Value);
                }
            }

            if (fields.Count > 0)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
                {
                    IEnumerable<ITMFACILIT> itmFacilitList = await uow.ItmFacilitRepository.GetByFields(fields);

                    uow.Commit();

                    return itmFacilitList;
                }
            }

            return Enumerable.Empty<ITMFACILIT>();
        }

        public async Task<ITMFACILIT> GetByID(decimal id)
        {
            Result<ITMFACILIT> result = id.IsValueValid<ITMFACILIT>(nameof(ITMFACILIT.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
                {
                    ITMFACILIT itmFacilit = await uow.ItmFacilitRepository.GetByID(id);

                    uow.Commit();

                    return itmFacilit;
                }
            }

            return null;
        }

        public async Task<ITMFACILIT> GetByUniqueFields(string STOFCY_0, string ITMREF_0)
        {
            Dictionary<string, object> fields = new Dictionary<string, object>();
            Dictionary<string, object> fieldsTemp = new Dictionary<string, object>
            {
                { nameof(ITMFACILIT.STOFCY_0), STOFCY_0},
                { nameof(ITMFACILIT.ITMREF_0), ITMREF_0}
            };

            foreach (KeyValuePair<string, object> field in fieldsTemp)
            {
                if (field.Value != null)
                {
                    fields.Add(field.Key, field.Value);
                }
            }

            if (fields.Count > 0)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
                {
                    ITMFACILIT itmFacilit = await uow.ItmFacilitRepository.GetByUniqueFields(fields);

                    uow.Commit();

                    return itmFacilit;
                }
            }

            return null;
        }
    }
}
