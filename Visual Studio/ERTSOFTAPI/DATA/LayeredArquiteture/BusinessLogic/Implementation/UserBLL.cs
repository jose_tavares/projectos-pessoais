﻿using MODELS.Helpers;
using System.Transactions;
using System.Threading.Tasks;
using System.Collections.Generic;
using DATA.Helpers.StaticClasses;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;
using System.Linq;
using DATA.LayeredArquiteture.Repositories.UnitOfWork;

namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{
    public class UserBLL : IUserBLL
    {
        public USER AuthenticatedUser { get; set; }

        public UserBLL()
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();
        }

        public async Task<IEnumerable<USER>> GetAll()
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Snapshot))
            {
                IEnumerable<USER> userList = await uow.UserRepository.GetAll();

                uow.Commit();

                return userList;
            }
        }

        public async Task<IEnumerable<USER>> GetByProfile(string PROFILE_0)
        {
            Result<USER> result = PROFILE_0.IsValueValid<USER>(nameof(USER.PROFILE_0));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Snapshot))
                {
                    IEnumerable<USER> userList = await uow.UserRepository.GetByField(nameof(USER.PROFILE_0), PROFILE_0);

                    uow.Commit();

                    return userList;
                }
            }

            return Enumerable.Empty<USER>();
        }

        public async Task<USER> GetByID(decimal id)
        {
            Result<USER> result = id.IsValueValid<USER>(nameof(USER.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                {
                    USER user = await uow.UserRepository.GetByID(id);

                    uow.Commit();

                    return user;
                }
            }

            return null;
        }

        public async Task<Result<USER>> GetById(decimal id)
        {
            Result<USER> result = id.IsValueValid<USER>(nameof(USER.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                {
                    result.Obj = await uow.UserRepository.GetByID(id);

                    uow.Commit();
                }
            }

            if (result.Obj == null)
            {
                result.AddInvalidMessage($"Recurso não encontrado para o id {id}");
            }

            return result;
        }

        public async Task<USER> GetByUniqueKeyUser(string USER_0)
        {
            Result<USER> result = USER_0.IsValueValid<USER>(nameof(USER.USR_0));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                {
                    USER userModel = await uow.UserRepository.GetByUniqueField(nameof(USER.USR_0), USER_0);

                    uow.Commit();

                    return userModel;
                }
            }

            return null;
        }

        public async Task<USER> GetByUniqueKeyUsername(string USERNAM_0)
        {
            Result<USER> result = USERNAM_0.IsValueValid<USER>(nameof(USER.USERNAM_0));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                {
                    USER user = await uow.UserRepository.GetByUniqueField(nameof(USER.USERNAM_0), USERNAM_0);

                    uow.Commit();

                    return user;
                }
            }

            return null;
        }

        public async Task<Result<USER>> Insert(USER user)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
            {
                Result<USER> result = await IsValidInsert(user);

                if (result.Success)
                {
                    user.CREUSR_0 = AuthenticatedUser.USR_0;
                    user.UPDUSR_0 = AuthenticatedUser.USR_0;

                    result = await uow.UserRepository.Insert(user);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<USER>> Update(USER user)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
            {
                Result<USER> result = await IsValidUpdate(user);

                if (result.Success)
                {
                    user.UPDUSR_0 = AuthenticatedUser.USR_0;

                    result = await uow.UserRepository.Update(user);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<USER>> Login(string USERNAM_0, string PASSWORD_0)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
            {
                Result<USER> result = await IsValidLogin(USERNAM_0, PASSWORD_0);

                if (result.Success)
                {
                    USER user = result.Obj;
                    result = await Update(user);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<USER>> Logout(decimal id)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
            {
                Result<USER> result = await IsValidLogout(id);

                if (result.Success)
                {
                    USER user = result.Obj;

                    result = await Update(user);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<USER>> IsValidInsert(USER user)
        {
            // Add more validations if needed
            Result<USER> result = await user.IsClassValidAsync();

            if (result.Success)
            {
                if (await GetByUniqueKeyUser(user.USR_0) != null)
                {
                    result.AddInvalidMessage("O user já existe.");
                }

                if (await GetByUniqueKeyUsername(user.USERNAM_0) != null)
                {
                    result.AddInvalidMessage("O username já existe.");
                }
            }

            return result;
        }

        public async Task<Result<USER>> IsValidUpdate(USER user)
        {
            // Add more validations if needed
            Result<USER> result = await user.IsClassValidAsync();

            if (result.Success)
            {
                USER userTemp = await GetByID(user.ROWID);

                if (userTemp == null)
                {
                    result.AddInvalidMessage($"Não foi possível encontrar um utilizador com o ID {user.ROWID}.");
                }
                else
                {
                    if (user.USR_0 != userTemp.USR_0)
                    {
                        result.AddInvalidMessage($"Não é permitido alterar o campo {nameof(user.USR_0)}");
                    }

                    if (user.USERNAM_0 != userTemp.USERNAM_0)
                    {
                        if (await GetByUniqueKeyUsername(user.USERNAM_0) != null)
                        {
                            result.AddInvalidMessage("O username já existe.");
                        }
                    }
                }
            }

            return result;
        }

        private async Task<Result<USER>> IsValidLogin(string USERNAM_0, string PASSWORD_0)
        {
            USER validateUser = new USER 
            { 
                USERNAM_0 = USERNAM_0, 
                PASSWORD_0 = PASSWORD_0 
            };
            Result<USER> result = validateUser.IsPropertiesValid(nameof(USER.USERNAM_0), nameof(USER.PASSWORD_0));

            if (result.Success)
            {
                USER user = await GetByUniqueKeyUsername(USERNAM_0);

                if (user == null)
                {
                    result.AddInvalidMessage($"Não foi possível encontrar um utilizador com o username {USERNAM_0}.");
                }
                else
                {
                    if (user.PASSWORD_0 != PASSWORD_0)
                    {
                        result.AddInvalidMessage("A password encontra-se incorreta.");
                    }
                    else
                    {
                        if (user.ENABFLG_0 != 2)
                        {
                            result.AddInvalidMessage("O utilizador não se encontra ativo.");
                        }
                    }

                    if (result.Success)
                    {
                        user.LOGGEDFLG_0 = 2;
                    }
                }

                result.Obj = user;
            }

            return result;
        }

        private async Task<Result<USER>> IsValidLogout(decimal id)
        {
            Result<USER> result = id.IsValueValid<USER>(nameof(USER.ROWID));

            if (result.Success)
            {
                USER user = await GetByID(id);

                if (user == null)
                {
                    result.AddInvalidMessage($"Não foi possível encontrar um utilizador com o ID {id}.");
                }

                user.LOGGEDFLG_0 = 1;

                result.Obj = user;
            }

            return result;
        }
    }
}
