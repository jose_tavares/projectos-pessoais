﻿namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{
    using MODELS.Helpers;
    using System.Transactions;
    using System.Threading.Tasks;
    using DATA.Helpers.StaticClasses;
    using System.Collections.Generic;
    using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;
    using MODELS.BusinessObject.ERTSOFT.Database;
    using DATA.LayeredArquiteture.Repositories.UnitOfWork;
    using MODELS.BusinessObject.SAGEX3.Database;

    public class AtextraBLL : IAtextraBLL
    {
        #region Public Properties
        public USER AuthenticatedUser { get; set; }
        #endregion

        #region Public Methods
        public AtextraBLL()
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();
        }

        public async Task<IEnumerable<ATEXTRA>> GetAll()
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
            {
                IEnumerable<ATEXTRA> atextraList = await uow.AtextraRepository.GetAll();

                uow.Commit();

                return atextraList;
            }
        }

        public async Task<ATEXTRA> GetByID(decimal id)
        {
            Result<ATEXTRA> result = id.IsValueValid<ATEXTRA>(nameof(ATEXTRA.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
                {
                    ATEXTRA atextra = await uow.AtextraRepository.GetByID(id);

                    uow.Commit();

                    return atextra;
                }
            }

            return null;
        }

        public async Task<ATEXTRA> GetByUniqueFields(string CODFIC_0, string ZONE_0, string LANGUE_0, string IDENT_1, string IDENT_2)
        {
            ATEXTRA atextraModel = new ATEXTRA
            {
                CODFIC_0 = CODFIC_0,
                ZONE_0 = ZONE_0,
                LANGUE_0 = LANGUE_0,
                IDENT1_0 = IDENT_1,
                IDENT2_0 = IDENT_2
            };

            Result<ATEXTRA> result = atextraModel.IsPropertiesValid(nameof(ATEXTRA.CODFIC_0), nameof(ATEXTRA.ZONE_0), nameof(ATEXTRA.LANGUE_0), nameof(ATEXTRA.IDENT1_0), nameof(ATEXTRA.IDENT2_0));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
                {
                    Dictionary<string, object> fields = new Dictionary<string, object>
                    {
                        { nameof(ATEXTRA.CODFIC_0), CODFIC_0 },
                        { nameof(ATEXTRA.ZONE_0), ZONE_0},
                        { nameof(ATEXTRA.LANGUE_0), LANGUE_0},
                        { nameof(ATEXTRA.IDENT1_0), IDENT_1},
                        { nameof(ATEXTRA.IDENT2_0), IDENT_2}
                    };

                    atextraModel = await uow.AtextraRepository.GetByUniqueFields(fields);

                    uow.Commit();

                    return atextraModel;
                }
            }

            return null;
        }
        #endregion
    }
}
