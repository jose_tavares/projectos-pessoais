﻿using MODELS.Helpers;
using System.Transactions;
using System.Threading.Tasks;
using DATA.Helpers.StaticClasses;
using System.Collections.Generic;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.Repositories.UnitOfWork;
using MODELS.BusinessObject.SAGEX3.Database;

namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{
    public class StolotBLL : IStolotBLL
    {
        public USER AuthenticatedUser { get; set; }

        public StolotBLL()
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();
        }

        public async Task<IEnumerable<STOLOT>> GetAll()
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
            {
                IEnumerable<STOLOT> stolotList = await uow.StolotRepository.GetAll();

                uow.Commit();

                return stolotList;
            }
        }

        public async Task<STOLOT> GetByID(decimal id)
        {
            Result<STOLOT> result = id.IsValueValid<STOLOT>(nameof(STOLOT.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
                {
                    STOLOT stolot = await uow.StolotRepository.GetByID(id);

                    uow.Commit();

                    return stolot;
                }
            }

            return null;
        }
    }
}
