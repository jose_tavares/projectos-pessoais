﻿using MODELS.Helpers;
using System.Transactions;
using System.Threading.Tasks;
using DATA.Helpers.StaticClasses;
using System.Collections.Generic;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.Repositories.UnitOfWork;
using MODELS.BusinessObject.SAGEX3.Database;

namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{
    public class StolocBLL : IStolocBLL
    {
        public USER AuthenticatedUser { get; set; }

        public StolocBLL()
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();
        }

        public async Task<IEnumerable<STOLOC>> GetAll()
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
            {
                IEnumerable<STOLOC> stolocList = await uow.StolocRepository.GetAll();

                uow.Commit();

                return stolocList;
            }
        }

        public async Task<STOLOC> GetByID(decimal id)
        {
            Result<STOLOC> result = id.IsValueValid<STOLOC>(nameof(STOLOC.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
                {
                    STOLOC stoloc = await uow.StolocRepository.GetByID(id);

                    uow.Commit();

                    return stoloc;
                }
            }

            return null;
        }

        public async Task<STOLOC> GetByUniqueFields(string STOFCY_0, string LOC_0)
        {
            STOLOC stolocModel = new STOLOC
            {
                STOFCY_0 = STOFCY_0,
                LOC_0 = LOC_0
            };
            Result<STOLOC> result = stolocModel.IsPropertiesValid(nameof(STOLOC.STOFCY_0), nameof(STOLOC.LOC_0));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
                {
                    Dictionary<string, object> fields = new Dictionary<string, object>
                    {
                        { nameof(STOLOC.STOFCY_0), STOFCY_0 },
                        { nameof(STOLOC.LOC_0), LOC_0 }
                    };

                    stolocModel = await uow.StolocRepository.GetByUniqueFields(fields);

                    uow.Commit();

                    return stolocModel;
                }
            }

            return null;
        }
    }
}
