﻿using MODELS.Helpers;
using System.Transactions;
using System.Threading.Tasks;
using DATA.Helpers.StaticClasses;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;
using DATA.LayeredArquiteture.Repositories.UnitOfWork;

namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{
    public class ConsumptionBLL : IConsumptionBLL
    {
        public USER AuthenticatedUser { get; set; }

        public ConsumptionBLL()
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();
        }

        public async Task<IEnumerable<CONSUMPTION>> GetAll()
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
            {
                IEnumerable<CONSUMPTION> consumptionList = await uow.ConsumptionRepository.GetAll();

                uow.Commit();

                return consumptionList;
            }
        }

        public async Task<CONSUMPTION> GetByID(decimal id)
        {
            Result<CONSUMPTION> result = id.IsValueValid<CONSUMPTION>(nameof(CONSUMPTION.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                {
                    CONSUMPTION consumption = await uow.ConsumptionRepository.GetByID(id);

                    uow.Commit();

                    return consumption;
                }
            }

            return null;
        }

        public async Task<Result<CONSUMPTION>> Insert(CONSUMPTION consumption)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
            {
                Result<CONSUMPTION> result = await IsValidInsert(consumption);

                if (result.Success)
                {
                    consumption.CREUSR_0 = AuthenticatedUser.USR_0;
                    consumption.UPDUSR_0 = AuthenticatedUser.USR_0;

                    result = await uow.ConsumptionRepository.Insert(consumption);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<CONSUMPTION>> IsValidInsert(CONSUMPTION consumption)
        {
            Result<CONSUMPTION> result = await consumption.IsClassValidAsync(true);

            return result;
        }
    }
}
