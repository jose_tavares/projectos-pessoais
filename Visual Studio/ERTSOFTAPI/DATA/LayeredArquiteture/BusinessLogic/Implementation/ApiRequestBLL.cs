﻿using MODELS.Helpers;
using System.Transactions;
using System.Threading.Tasks;
using DATA.Helpers.StaticClasses;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.Repositories.UnitOfWork;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;

namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{
    public class ApiRequestBLL : IApiRequestBLL
    {
        public USER AuthenticatedUser { get; set; }

        public ApiRequestBLL()
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();
        }

        public async Task<IEnumerable<APIREQUEST>> GetAll()
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
            {
                IEnumerable<APIREQUEST> apiRequestList = await uow.ApiRequestRepository.GetAll();

                uow.Commit();

                return apiRequestList;
            }
        }

        public async Task<APIREQUEST> GetByID(decimal id)
        {
            Result<APIREQUEST> result = id.IsValueValid<APIREQUEST>(nameof(APIREQUEST.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                {
                    APIREQUEST apiRequest = await uow.ApiRequestRepository.GetByID(id);

                    uow.Commit();

                    return apiRequest;
                }
            }

            return null;
        }

        public async Task<Result<APIREQUEST>> Insert(APIREQUEST apiRequest)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
            {
                Result<APIREQUEST> result = await IsValidInsert(apiRequest);

                if (result.Success)
                {
                    apiRequest.CREUSR_0 = AuthenticatedUser.USR_0;
                    apiRequest.UPDUSR_0 = AuthenticatedUser.USR_0;

                    result = await uow.ApiRequestRepository.Insert(apiRequest);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<APIREQUEST>> IsValidInsert(APIREQUEST apiRequest)
        {
            Result<APIREQUEST> result = await apiRequest.IsClassValidAsync(true);

            return result;
        }
    }
}
