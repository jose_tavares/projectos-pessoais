﻿using MODELS.Helpers;
using System.Transactions;
using System.Threading.Tasks;
using DATA.Helpers.StaticClasses;
using System.Collections.Generic;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.Repositories.UnitOfWork;
using MODELS.BusinessObject.SAGEX3.Database;

namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{
    public class FacilityBLL : IFacilityBLL
    {
        public USER AuthenticatedUser { get; set; }

        public FacilityBLL()
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();
        }

        public async Task<IEnumerable<FACILITY>> GetAll()
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
            {
                IEnumerable<FACILITY> facilityList = await uow.FacilityRepository.GetAll();

                uow.Commit();

                return facilityList;
            }
        }

        public async Task<FACILITY> GetByID(decimal id)
        {
            Result<FACILITY> result = id.IsValueValid<FACILITY>(nameof(FACILITY.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
                {
                    FACILITY facility = await uow.FacilityRepository.GetByID(id);

                    uow.Commit();

                    return facility;
                }
            }

            return null;
        }

        public async Task<FACILITY> GetByUniqueField(string FCY_0)
        {
            Result<FACILITY> result = FCY_0.IsValueValid<FACILITY>(nameof(FACILITY.FCY_0));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
                {
                    FACILITY facility = await uow.FacilityRepository.GetByUniqueField(nameof(FACILITY.FCY_0), FCY_0);

                    uow.Commit();

                    return facility;
                }
            }

            return null;
        }
    }
}
