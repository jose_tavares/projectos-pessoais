﻿using MODELS.Helpers;
using System.Transactions;
using System.Threading.Tasks;
using DATA.Helpers.StaticClasses;
using System.Collections.Generic;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.Repositories.UnitOfWork;
using MODELS.BusinessObject.SAGEX3.Database;

namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{
    public class TabUnitBLL : ITabUnitBLL
    {
        public USER AuthenticatedUser { get; set; }

        public TabUnitBLL()
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();
        }

        public async Task<IEnumerable<TABUNIT>> GetAll()
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
            {
                IEnumerable<TABUNIT> tabUnitList = await uow.TabUnitRepository.GetAll();

                uow.Commit();

                return tabUnitList;
            }
        }

        public async Task<TABUNIT> GetByID(decimal id)
        {
            Result<TABUNIT> result = id.IsValueValid<TABUNIT>(nameof(TABUNIT.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
                {
                    TABUNIT tabUnit = await uow.TabUnitRepository.GetByID(id);

                    uow.Commit();

                    return tabUnit;
                }
            }

            return null;
        }

        public async Task<TABUNIT> GetByUniqueKey(string UOM_0)
        {
            Result<TABUNIT> result = UOM_0.IsValueValid<TABUNIT>(nameof(TABUNIT.UOM_0));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
                {
                    TABUNIT tabUnit = await uow.TabUnitRepository.GetByUniqueField(nameof(TABUNIT.UOM_0), UOM_0);

                    uow.Commit();

                    return tabUnit;
                }
            }

            return null;
        }
    }
}
