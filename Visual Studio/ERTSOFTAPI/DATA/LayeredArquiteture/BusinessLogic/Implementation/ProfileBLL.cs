﻿using MODELS.Helpers;
using System.Transactions;
using System.Threading.Tasks;
using DATA.Helpers.StaticClasses;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;
using DATA.LayeredArquiteture.Repositories.UnitOfWork;

namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{
    public class ProfileBLL : IProfileBLL
    {
        public USER AuthenticatedUser { get; set; }

        public ProfileBLL()
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();
        }

        public async Task<IEnumerable<PROFILE>> GetAll()
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Snapshot))
            {
                IEnumerable<PROFILE> profileList = await uow.ProfileRepository.GetAll();

                uow.Commit();

                return profileList;
            }
        }

        public async Task<PROFILE> GetByID(decimal id)
        {
            Result<PROFILE> result = id.IsValueValid<PROFILE>(nameof(PROFILE.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                {
                    PROFILE profile = await uow.ProfileRepository.GetByID(id);

                    uow.Commit();

                    return profile;
                }
            }

            return null;
        }

        public async Task<PROFILE> GetByName(string PROFILE_0)
        {
            Result<PROFILE> result = PROFILE_0.IsValueValid<PROFILE>(nameof(PROFILE.PROFILE_0));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                {
                    PROFILE profile = await uow.ProfileRepository.GetByUniqueField(nameof(PROFILE.PROFILE_0), PROFILE_0);

                    uow.Commit();

                    return profile;
                }
            }

            return null;
        }

        public async Task<PROFILE> GetByUniqueFieldWithUserList(string PROFILE_0)
        {
            Result<PROFILE> result = PROFILE_0.IsValueValid<PROFILE>(nameof(PROFILE.PROFILE_0));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                {
                    PROFILE profile = await uow.ProfileRepository.GetByUniqueFieldWithUserList(PROFILE_0);

                    uow.Commit();

                    return profile;
                }
            }

            return null;
        }

        public async Task<Result<PROFILE>> Insert(PROFILE model)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
            {
                Result<PROFILE> result = await IsValidInsert(model);

                if (result.Success)
                {
                    model.CREUSR_0 = AuthenticatedUser.USR_0;
                    model.UPDUSR_0 = AuthenticatedUser.USR_0;

                    result = await uow.ProfileRepository.Insert(model);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<PROFILE>> Update(PROFILE profile)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
            {
                Result<PROFILE> result = await IsValidUpdate(profile);

                if (result.Success)
                {
                    profile.UPDUSR_0 = AuthenticatedUser.USR_0;

                    result = await uow.ProfileRepository.Update(profile);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<PROFILE>> IsValidInsert(PROFILE profile)
        {
            // Add more validations if needed
            Result<PROFILE> result = await profile.IsClassValidAsync();

            if (result.Success)
            {
                var profileTemp = await GetByName(profile.PROFILE_0);

                if (profileTemp != null)
                {
                    result.AddInvalidMessage($"Já existe um perfil com o nome {profile.PROFILE_0}");
                }
            }

            return result;
        }

        public async Task<Result<PROFILE>> IsValidUpdate(PROFILE profile)
        {
            // Add more validations if needed
            Result<PROFILE> result = await profile.IsClassValidAsync();

            if (result.Success)
            {
                PROFILE profileTemp = await GetByID(profile.ROWID);

                if (profileTemp == null)
                {
                    result.AddInvalidMessage($"Não foi possível encontrar o perfil com o ID {profile.ROWID}.");
                }
                else if (profile.PROFILE_0 != profileTemp.PROFILE_0)
                {
                    result.AddInvalidMessage($"Não é permitido alterar o campo {nameof(profile.PROFILE_0)}");
                }
            }

            return result;
        }

        public async Task<Result<PROFILE>> IsValidDelete(decimal id)
        {
            // Add more validations if needed
            Result<PROFILE> result = id.IsValueValid<PROFILE>(nameof(PROFILE.ROWID));

            if (result.Success)
            {
                PROFILE validateProfile = await GetByID(id);

                if (validateProfile == null)
                {
                    result.AddInvalidMessage($"Não foi possível encontrar um perfil com o ID {id}.");
                }
            }

            return result;
        }
    }
}
