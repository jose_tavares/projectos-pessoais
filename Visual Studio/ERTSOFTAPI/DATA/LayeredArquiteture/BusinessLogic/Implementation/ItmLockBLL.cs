﻿using MODELS.Helpers;
using System.Transactions;
using System.Threading.Tasks;
using DATA.Helpers.StaticClasses;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;
using DATA.LayeredArquiteture.Repositories.UnitOfWork;

namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{
    public class ItmLockBLL : IItmLockBLL
    {
        public USER AuthenticatedUser { get; set; }

        public ItmLockBLL()
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();
        }

        public async Task<IEnumerable<ITMLOCK>> GetAll()
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
            {
                IEnumerable<ITMLOCK> itmLockList = await uow.ItmLockRepository.GetAll();

                uow.Commit();

                return itmLockList;
            }
        }

        public async Task<ITMLOCK> GetByID(decimal id)
        {
            Result<ITMLOCK> result = id.IsValueValid<ITMLOCK>(nameof(ITMLOCK.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
                {
                    ITMLOCK itmLock = await uow.ItmLockRepository.GetByID(id);

                    uow.Commit();

                    return itmLock;
                }
            }

            return null;
        }

        public async Task<ITMLOCK> GetByUniqueField(string ITMREF_0)
        {
            Result<ITMLOCK> result = ITMREF_0.IsValueValid<ITMLOCK>(nameof(ITMLOCK.ITMREF_0));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
                {
                    ITMLOCK itmLockModel = await uow.ItmLockRepository.GetByUniqueField(nameof(ITMLOCK.ITMREF_0), ITMREF_0);

                    uow.Commit();

                    return itmLockModel;
                }
            }

            return null;
        }

        public async Task<Result<ITMLOCK>> Insert(ITMLOCK itmLock)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
            {
                Result<ITMLOCK> result = await IsValidInsert(itmLock);

                if (result.Success)
                {
                    itmLock.CREUSR_0 = AuthenticatedUser.USR_0;
                    itmLock.UPDUSR_0 = AuthenticatedUser.USR_0;

                    result = await uow.ItmLockRepository.Insert(itmLock);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<ITMLOCK>> Update(ITMLOCK itmLock)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
            {
                Result<ITMLOCK> result = await IsValidUpdate(itmLock);

                if (result.Success)
                {
                    itmLock.UPDUSR_0 = AuthenticatedUser.USR_0;

                    result = await uow.ItmLockRepository.Update(itmLock);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<ITMLOCK>> Delete(decimal id)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
            {
                Result<ITMLOCK> result = await IsValidDelete(id);

                if (result.Success)
                {
                    result = await uow.ItmLockRepository.Delete(id);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<ITMLOCK>> IsValidInsert(ITMLOCK itmLock)
        {
            // Add more validations if needed
            Result<ITMLOCK> result = await itmLock.IsClassValidAsync(true);

            //if (result.Success)
            //{
            //    ITMLOCK itmLockTemp = await GetByItmref(itmLock.ITMREF_0);

            //    if (itmLockTemp != null)
            //    {
            //        result.AddInvalidMessage($"Já existe um artigo na tabela ITMLOCK com a chave {itmLock.ITMREF_0}");
            //    }
            //}

            return result;
        }

        public async Task<Result<ITMLOCK>> IsValidUpdate(ITMLOCK itmLock)
        {
            // Add more validations if needed
            Result<ITMLOCK> result = await itmLock.IsClassValidAsync();

            if (result.Success)
            {
                ITMLOCK itmLockTemp = await GetByID(itmLock.ROWID);

                if (itmLockTemp == null)
                {
                    result.AddInvalidMessage($"Não foi possível encontrar o artigo na ITMLOCK com a chave {itmLock.ITMREF_0}.");
                }
                else if (itmLock.ITMREF_0 != itmLockTemp.ITMREF_0)
                {
                    result.AddInvalidMessage($"Não é permitido alterar o campo {nameof(itmLock.ITMREF_0)}");
                }
            }

            return result;
        }

        public async Task<Result<ITMLOCK>> IsValidDelete(decimal id)
        {
            Result<ITMLOCK> result = await id.IsValuesValidAsync<ITMLOCK>(nameof(ITMLOCK.ROWID));

            return result;
        }
    }
}
