﻿using MODELS.Helpers;
using System.Transactions;
using System.Threading.Tasks;
using DATA.Helpers.StaticClasses;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.Repositories.UnitOfWork;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;

namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{
    public class ApiRequestBLL : IApiRequestBLL
    {
        public USER AuthenticatedUser { get; set; }

        public ApiRequestBLL()
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();
        }

        #region Public Methods
        public async Task<Result<IEnumerable<APIREQUEST>>> GetAll()
        {
            Result<IEnumerable<APIREQUEST>> result = new Result<IEnumerable<APIREQUEST>>(true);

            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Snapshot))
            {
                result.Obj = await uow.ApiRequestRepository.GetAll();

                uow.Commit();
            }

            if (!result.Obj.IsValid())
            {
                result.NotFound();
            }

            return result;
        }

        public async Task<Result<APIREQUEST>> GetById(int id)
        {
            Result<APIREQUEST> result = id.IsValueValid<APIREQUEST>(nameof(APIREQUEST.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                {
                    result.Obj = await uow.ApiRequestRepository.GetById(id);

                    uow.Commit();
                }

                if (result.Obj == null)
                {
                    result.NotFound(id);
                }
            }

            return result;
        }

        public async Task<Result<APIREQUEST>> Insert(APIREQUEST apiRequest)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
            {
                Result<APIREQUEST> result = await IsValidInsert(apiRequest);

                if (result.Success)
                {
                    apiRequest = result.Obj;
                    apiRequest.CREUSR_0 = AuthenticatedUser.USR_0;
                    apiRequest.UPDUSR_0 = AuthenticatedUser.USR_0;

                    result = await uow.ApiRequestRepository.Insert(apiRequest);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<APIREQUEST>> IsValidInsert(APIREQUEST apiRequest)
        {
            Result<APIREQUEST> result = await apiRequest.IsModelOnInsertValidAsync();

            return result;
        }
        #endregion
    }
}
