﻿using MODELS.Helpers;
using System.Transactions;
using System.Threading.Tasks;
using DATA.Helpers.StaticClasses;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;
using DATA.LayeredArquiteture.Repositories.UnitOfWork;

namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{
    public class ItmLockBLL : IItmLockBLL
    {
        public USER AuthenticatedUser { get; set; }

        public ItmLockBLL()
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();
        }

        #region Pubblic Methods

        public async Task<Result<IEnumerable<ITMLOCK>>> GetAll()
        {
            Result<IEnumerable<ITMLOCK>> result = new Result<IEnumerable<ITMLOCK>>(true);

            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
            {
                result.Obj = await uow.ItmLockRepository.GetAll();

                uow.Commit();
            }

            if (!result.Obj.IsValid())
            {
                result.NotFound();
            }

            return result;
        }

        public async Task<Result<ITMLOCK>> GetById(int id)
        {
            Result<ITMLOCK> result = id.IsValueValid<ITMLOCK>(nameof(ITMLOCK.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
                {
                    result.Obj = await uow.ItmLockRepository.GetById(id);

                    uow.Commit();
                }

                if (result.Obj == null)
                {
                    result.NotFound(id);
                }
            }

            return result;
        }

        public async Task<Result<ITMLOCK>> GetByUniqueField(string ITMREF_0)
        {
            Result<ITMLOCK> result = ITMREF_0.IsValueValid<ITMLOCK>(nameof(ITMLOCK.ITMREF_0));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
                {
                    result.Obj = await uow.ItmLockRepository.GetByUniqueField(nameof(ITMLOCK.ITMREF_0), ITMREF_0);

                    uow.Commit();
                }

                if (result.Obj == null)
                {
                    result.NotFound(nameof(ITMLOCK.ITMREF_0), ITMREF_0);
                }
            }

            return result;
        }
        #endregion
    }
}
