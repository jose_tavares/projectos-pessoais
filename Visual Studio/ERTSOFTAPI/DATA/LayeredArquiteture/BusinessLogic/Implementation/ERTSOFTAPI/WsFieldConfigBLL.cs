﻿using System.Linq;
using MODELS.Helpers;
using System.Transactions;
using System.Threading.Tasks;
using DATA.Helpers.StaticClasses;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;
using DATA.LayeredArquiteture.Repositories.UnitOfWork;

namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{
    public class WsFieldConfigBLL : IWsFieldConfigBLL
    {
        public USER AuthenticatedUser { get; set; }

        public WsFieldConfigBLL()
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();
        }

        #region Public Methods

        public async Task<Result<IEnumerable<WSFIELDCONFIG>>> GetAll()
        {
            Result<IEnumerable<WSFIELDCONFIG>> result = new Result<IEnumerable<WSFIELDCONFIG>>(true);

            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Snapshot))
            {
                result.Obj = await uow.WsFieldConfigRepository.GetAll();

                uow.Commit();
            }

            if (!result.Obj.IsValid())
            {
                result.NotFound();
            }

            return result;
        }

        public async Task<Result<IEnumerable<WSFIELDCONFIG>>> GetByFields(string WSCODE_0, string FLD_0, string VALUE_0, int FLDFLG_0, int VALUEFLG_0)
        {
            Result<Dictionary<string, object>> resultFields = ClassHelper.GetDictionaryFieldsFromObject(new WSFIELDCONFIG
            {
                WSCODE_0 = WSCODE_0,
                FLD_0 = FLD_0,
                VALUE_0 = VALUE_0,
                FLDFLG_0 = FLDFLG_0,
                VALUEFLG_0 = VALUEFLG_0
            }, nameof(WSFIELDCONFIG.WSCODE_0), nameof(WSFIELDCONFIG.FLD_0), nameof(WSFIELDCONFIG.VALUE_0), nameof(WSFIELDCONFIG.FLDFLG_0), nameof(WSFIELDCONFIG.VALUEFLG_0));
            Result<IEnumerable<WSFIELDCONFIG>> result = Result<IEnumerable<WSFIELDCONFIG>>.ConvertAndGetFromResult(resultFields);

            if (result.Success)
            {
                Dictionary<string, object> fields = resultFields.Obj;

                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Snapshot))
                {
                    result.Obj = await uow.WsFieldConfigRepository.GetByFields(fields);

                    uow.Commit();
                }

                if (!result.Obj.IsValid())
                {
                    result.NotFound(fields);
                }
            }

            return result;
        }

        public async Task<Result<IEnumerable<WSFIELDCONFIG>>> GetByWsCode(string WSCODE_0)
        {
            Result<IEnumerable<WSFIELDCONFIG>> result = WSCODE_0.IsValueValid<IEnumerable<WSFIELDCONFIG>>(nameof(WSFIELDCONFIG.WSCODE_0));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Snapshot))
                {
                    result.Obj = await uow.WsFieldConfigRepository.GetByField(nameof(WSFIELDCONFIG.WSCODE_0), WSCODE_0);

                    uow.Commit();
                }

                if (!result.Obj.IsValid())
                {
                    result.NotFound(nameof(WSFIELDCONFIG.WSCODE_0), WSCODE_0);
                }
            }

            return result;
        }

        public async Task<Result<WSFIELDCONFIG>> GetById(int id)
        {
            Result<WSFIELDCONFIG> result = id.IsValueValid<WSFIELDCONFIG>(nameof(WSFIELDCONFIG.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Snapshot))
                {
                    result.Obj = await uow.WsFieldConfigRepository.GetById(id);

                    uow.Commit();
                }

                if (result.Obj == null)
                {
                    result.NotFound(id);
                }
            }

            return result;
        }

        public async Task<Result<WSFIELDCONFIG>> GetByUniqueFields(string WSCODE_0, int POSITION_0)
        {
            Result<Dictionary<string, object>> resultFields = ClassHelper.GetDictionaryFieldsFromObject(new WSFIELDCONFIG()
            {
                WSCODE_0 = WSCODE_0,
                POSITION_0 = POSITION_0
            }, nameof(WSFIELDCONFIG.WSCODE_0), nameof(WSFIELDCONFIG.POSITION_0));
            Result<WSFIELDCONFIG> result = Result<WSFIELDCONFIG>.ConvertAndGetFromResult(resultFields);

            if (result.Success)
            {
                Dictionary<string, object> fields = resultFields.Obj;

                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Snapshot))
                {
                    result.Obj = await uow.WsFieldConfigRepository.GetByUniqueFields(fields);

                    uow.Commit();
                }

                if (result.Obj == null)
                {
                    result.NotFound(fields);
                }
            }

            return result;
        }

        public async Task<Result<WSFIELDCONFIG>> Insert(WSFIELDCONFIG wsFieldConfig)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
            {
                Result<WSFIELDCONFIG> result = await IsValidInsert(wsFieldConfig);

                if (result.Success)
                {
                    wsFieldConfig.CREUSR_0 = AuthenticatedUser.USR_0;
                    wsFieldConfig.UPDUSR_0 = AuthenticatedUser.USR_0;

                    result = await uow.WsFieldConfigRepository.Insert(wsFieldConfig);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<WSFIELDCONFIG>> Update(WSFIELDCONFIG wsFieldConfig)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
            {
                Result<WSFIELDCONFIG> result = await IsValidUpdate(wsFieldConfig);

                if (result.Success)
                {
                    wsFieldConfig = result.Obj;
                    wsFieldConfig.UPDUSR_0 = AuthenticatedUser.USR_0;

                    Result<WSFIELDCONFIG> resultUpdate = await uow.WsFieldConfigRepository.Update(wsFieldConfig);
                    result.ConvertFromResult(resultUpdate, true);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<WSFIELDCONFIG>> IsValidInsert(WSFIELDCONFIG wsFieldConfig)
        {
            Result<WSFIELDCONFIG> result = await wsFieldConfig.IsModelOnInsertValidAsync();

            if (result.Success)
            {
                Result<WSFIELDCONFIG> wsFieldConfigResult = await GetByUniqueFields(wsFieldConfig.WSCODE_0, wsFieldConfig.POSITION_0);

                if (wsFieldConfigResult.Obj != null)
                {
                    result.AddInvalidMessage($"Já existe um parametro do Webservice com o código '{wsFieldConfig.WSCODE_0}' na posição '{wsFieldConfig.POSITION_0}'");
                }

                //Validar se o WSCODE_0 existe na tabela WEBSERVICE
                //TEXTFLG_0 se = 1 então verificar se o VALUE_0 é uma coluna que realmente existe na tabela onde irá ler
                //POSITION_0 não pode já existir para o webservice em questão e tem que ser imediatamente a próxima posição
                //Add validations here
            }

            return result;
        }

        public async Task<Result<WSFIELDCONFIG>> IsValidUpdate(WSFIELDCONFIG wsFieldConfig)
        {
            Result<WSFIELDCONFIG> result = await wsFieldConfig.IsModelOnUpdateValidAsync();

            if (result.Success)
            {
                result = await GetById(wsFieldConfig.ROWID);

                if (result.Success)
                {
                    WSFIELDCONFIG wsFieldConfigTemp = result.Obj;

                    result = DBHelper.ChangeModelByUnModifiableFields(wsFieldConfigTemp, wsFieldConfig, nameof(WSFIELDCONFIG.WSCODE_0));

                    //Add validations here
                }
            }

            return result;
        }
        #endregion
    }
}
