﻿using System.Linq;
using MODELS.Helpers;
using System.Transactions;
using System.Threading.Tasks;
using DATA.Helpers.StaticClasses;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.Repositories.UnitOfWork;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;

namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{
    public class ConfigValueBLL : IConfigValueBLL
    {
        public USER AuthenticatedUser { get; set; }

        public ConfigValueBLL()
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();
        }

        #region Public Methods
        public async Task<Result<IEnumerable<CONFIGVALUE>>> GetAll()
        {
            Result<IEnumerable<CONFIGVALUE>> result = new Result<IEnumerable<CONFIGVALUE>>(true);

            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Snapshot))
            {
                result.Obj = await uow.ConfigValueRepository.GetAll();

                uow.Commit();
            }

            if (!result.Obj.IsValid())
            {
                result.NotFound();
            }

            return result;
        }

        public async Task<Result<IEnumerable<CONFIGVALUE>>> GetByFields(CONFIGVALUE configValue, params string[] filterFields)
        {
            Result<Dictionary<string, object>> resultFields = ClassHelper.GetDictionaryFieldsFromObject(configValue, filterFields);
            Result<IEnumerable<CONFIGVALUE>> result = Result<IEnumerable<CONFIGVALUE>>.ConvertAndGetFromResult(resultFields);

            if (result.Success)
            {
                Dictionary<string, object> fields = resultFields.Obj;

                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                {
                    result.Obj = await uow.ConfigValueRepository.GetByFields(fields);

                    uow.Commit();
                }

                if (!result.Obj.IsValid())
                {
                    result.NotFound(fields);
                }
            }

            return result;
        }

        public async Task<Result<CONFIGVALUE>> GetById(int id)
        {
            Result<CONFIGVALUE> result = id.IsValueValid<CONFIGVALUE>(nameof(CONFIGVALUE.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                {
                    result.Obj = await uow.ConfigValueRepository.GetById(id);

                    uow.Commit();
                }

                if (result.Obj == null)
                {
                    result.NotFound(id);
                }
            }

            return result;
        }

        public async Task<Result<CONFIGVALUE>> Insert(CONFIGVALUE configValue)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
            {
                Result<CONFIGVALUE> result = await IsValidInsert(configValue);

                if (result.Success)
                {
                    configValue = result.Obj;
                    configValue.CREUSR_0 = AuthenticatedUser.USR_0;
                    configValue.UPDUSR_0 = AuthenticatedUser.USR_0;

                    result = await uow.ConfigValueRepository.Insert(configValue);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<CONFIGVALUE>> Update(CONFIGVALUE configValue)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
            {
                Result<CONFIGVALUE> result = await IsValidUpdate(configValue);

                if (result.Success)
                {
                    configValue = result.Obj;
                    configValue.UPDUSR_0 = AuthenticatedUser.USR_0;

                    Result<CONFIGVALUE> resultUpdate = await uow.ConfigValueRepository.Update(configValue);
                    result.ConvertFromResult(resultUpdate, true);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<CONFIGVALUE>> IsValidInsert(CONFIGVALUE configValue)
        {
            // Add more validations if needed
            Result<CONFIGVALUE> result = await configValue.IsModelOnInsertValidAsync();

            //Aqui verificar se posso adicionar o valor validando se as chaves são repetidas pela tabela ERT.CONFIGKEY

            return result;
        }

        public async Task<Result<CONFIGVALUE>> IsValidUpdate(CONFIGVALUE configValue)
        {
            Result<CONFIGVALUE> result = await configValue.IsModelOnUpdateValidAsync();

            if (result.Success)
            {
                result = await GetById(configValue.ROWID);

                if (result.Success)
                {
                    CONFIGVALUE configvalueTemp = result.Obj;

                    result = DBHelper.ChangeModelByUnModifiableFields(configvalueTemp, configValue, nameof(CONFIGVALUE.CONFIGTYP_0));

                    //Add validations here
                }
            }

            return result;
        }
        #endregion
    }
}
