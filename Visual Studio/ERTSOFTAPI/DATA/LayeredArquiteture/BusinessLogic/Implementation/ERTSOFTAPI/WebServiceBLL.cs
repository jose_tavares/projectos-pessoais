﻿using DATA.Ninject;
using MODELS.Helpers;
using System.Transactions;
using System.Threading.Tasks;
using System.Collections.Generic;
using DATA.Helpers.StaticClasses;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.Repositories.UnitOfWork;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;

namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{
    public class WebServiceBLL : IWebServiceBLL
    {
        private readonly IWsFieldConfigBLL wsFieldConfigBLL;

        public USER AuthenticatedUser { get; set; }

        public WebServiceBLL(IWsFieldConfigBLL wsFieldConfigBLL)
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();

            this.wsFieldConfigBLL = wsFieldConfigBLL;
        }

        #region Public Methods

        public async Task<Result<IEnumerable<WEBSERVICE>>> GetAll()
        {
            Result<IEnumerable<WEBSERVICE>> result = new Result<IEnumerable<WEBSERVICE>>(true);

            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Snapshot))
            {
                result.Obj = await uow.WebServiceRepository.GetAll();

                uow.Commit();
            }

            if (!result.Obj.IsValid())
            {
                result.NotFound();
            }

            return result;
        }

        public async Task<Result<IEnumerable<WEBSERVICE>>> GetByImportFlg(int IMPORTFLG_0)
        {
            Result<IEnumerable<WEBSERVICE>> result = IMPORTFLG_0.IsValueValid<IEnumerable<WEBSERVICE>>(nameof(WEBSERVICE.IMPORTFLG_0));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Snapshot))
                {
                    result.Obj = await uow.WebServiceRepository.GetByField(nameof(WEBSERVICE.IMPORTFLG_0), IMPORTFLG_0);

                    uow.Commit();
                }

                if (!result.Obj.IsValid())
                {
                    result.NotFound(nameof(WEBSERVICE.IMPORTFLG_0), IMPORTFLG_0);
                }
            }

            return result;
        }

        public async Task<Result<WEBSERVICE>> GetById(int id)
        {
            Result<WEBSERVICE> result = id.IsValueValid<WEBSERVICE>(nameof(STOJOU.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Snapshot))
                {
                    result.Obj = await uow.WebServiceRepository.GetById(id);

                    uow.Commit();
                }

                if (result.Obj == null)
                {
                    result.NotFound(id);
                }
            }

            return result;
        }

        public async Task<Result<WEBSERVICE>> GetByUniqueField(string CODE_0)
        {
            Result<WEBSERVICE> result = CODE_0.IsValueValid<WEBSERVICE>(nameof(WEBSERVICE.CODE_0));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Snapshot))
                {
                    result.Obj = await uow.WebServiceRepository.GetByUniqueField(nameof(WEBSERVICE.CODE_0), CODE_0);

                    uow.Commit();
                }

                if (result.Obj == null)
                {
                    result.NotFound(nameof(WEBSERVICE.CODE_0), CODE_0);
                }
            }

            return result;
        }

        public async Task<Result<WEBSERVICE>> Insert(WEBSERVICE webservice)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
            {
                Result<WEBSERVICE> result = await IsValidInsert(webservice);

                if (result.Success)
                {
                    webservice.CREUSR_0 = AuthenticatedUser.USR_0;
                    webservice.UPDUSR_0 = AuthenticatedUser.USR_0;

                    result = await uow.WebServiceRepository.Insert(webservice);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<WEBSERVICE>> Update(WEBSERVICE webservice)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
            {
                Result<WEBSERVICE> result = await IsValidUpdate(webservice);

                if (result.Success)
                {
                    webservice = result.Obj;
                    webservice.UPDUSR_0 = AuthenticatedUser.USR_0;

                    Result<WEBSERVICE> resultUpdate = await uow.WebServiceRepository.Update(webservice);
                    result.ConvertFromResult(resultUpdate, true);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<WEBSERVICE>> IsValidInsert(WEBSERVICE webservice)
        {
            Result<WEBSERVICE> result = await webservice.IsModelOnInsertValidAsync();

            if (result.Success)
            {
                Result<WEBSERVICE> resultWebservice = await GetByUniqueField(webservice.CODE_0);

                if (resultWebservice.Obj != null) //Se encontrou o webservice
                {
                    result.AddInvalidMessage($"Já existe um webservice com o nome {webservice.CODE_0}");
                }

                //Verificar se o CODELANG_0 existe no Sage, Attempts tem que ser maior que 0 se IMPORTFLG = 2, VCRNUMSIZE tem que ser >= 0,
                //Add validations here
            }

            return result;
        }

        public async Task<Result<WEBSERVICE>> IsValidUpdate(WEBSERVICE webservice)
        {
            Result<WEBSERVICE> result = await webservice.IsModelOnUpdateValidAsync();

            if (result.Success)
            {
                result = await GetById(webservice.ROWID);

                if (result.Success)
                {
                    WEBSERVICE webserviceTemp = result.Obj;

                    result = DBHelper.ChangeModelByUnModifiableFields(webserviceTemp, webservice, nameof(WEBSERVICE.CODE_0));

                    //Add validations here
                }
            }

            return result;
        }

        public async Task<Result<WEBSERVICE>> IsValidImport(string CODE_0)
        {
            Result<WEBSERVICE> result = await GetByUniqueField(CODE_0);
            WEBSERVICE webService = result.Obj;

            if (result.Success)
            {
                if (webService == null)
                {
                    result.AddInvalidMessage($"Não foi possível obter informação do WEBSERVICE para o código '{CODE_0}'");

                    return result;
                }

                if (webService.ENABFLG_0 != 2)
                {
                    result.AddInvalidMessage($"'{CODE_0}' enontra-se inativo de utilização.");

                    return result;
                }

                if (webService.IMPORTFLG_0 != 2)
                {
                    result.AddInvalidMessage($"'{CODE_0}' não se encontra definido como webservice de importação de dados.");

                    return result;
                }

                Result<IMPORTWS> importWs = await NinjectWebCommon.GetInstance<IImportWSBLL>().GetByUniqueField(CODE_0);
                webService.ImportWsModel = importWs.Obj;

                if (webService.ImportWsModel == null)
                {
                    result.AddInvalidMessage($"Não existe uma linha no IMPORTWS configurada para o webservice '{CODE_0}'");

                    return result;
                }

                Result<IEnumerable<WSFIELDCONFIG>> wsFieldConfigListResult = await wsFieldConfigBLL.GetByWsCode(CODE_0);
                webService.WsFieldConfigList = wsFieldConfigListResult.Obj;

                if (!webService.WsFieldConfigList.IsValid())
                {
                    result.AddInvalidMessage($"Não existem parametros configurados para o webservice '{CODE_0}'");

                    return result;
                }

                result.Obj = webService;
            }

            return result;
        }

        public async Task<Result<WEBSERVICE>> IsValidExport(string CODE_0)
        {
            Result<WEBSERVICE> result = CODE_0.IsValueValid<WEBSERVICE>(nameof(WEBSERVICE.CODE_0));

            if (result.Success)
            {
                Result<WEBSERVICE> resultWebservice = await GetByUniqueField(CODE_0);
                WEBSERVICE webService = resultWebservice.Obj;

                if (webService == null)
                {
                    result.AddInvalidMessage($"Não foi possível obter informação para o código '{CODE_0}'");

                    return result;
                }

                if (webService.ENABFLG_0 != 2)
                {
                    result.AddInvalidMessage($"'{CODE_0}' enontra-se inativo.");

                    return result;
                }

                if (webService.EXPORTFLG_0 != 2)
                {
                    result.AddInvalidMessage($"'{CODE_0}' não se encontra definido como webservice de exportação de dados.");

                    return result;
                }

                result.Obj = webService;
            }

            return result;
        }
        #endregion
    }
}
