﻿using MODELS.Helpers;
using System.Transactions;
using System.Threading.Tasks;
using DATA.Helpers.StaticClasses;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;
using DATA.LayeredArquiteture.Repositories.UnitOfWork;

namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{
    public class ImportWSLogBLL : IImportWSLogBLL
    {
        public USER AuthenticatedUser { get; set; }

        public ImportWSLogBLL()
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();
        }

        #region Public Methods

        public async Task<Result<IEnumerable<IMPORTWSLOG>>> GetAll()
        {
            Result<IEnumerable<IMPORTWSLOG>> result = new Result<IEnumerable<IMPORTWSLOG>>(true);

            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Snapshot))
            {
                result.Obj = await uow.ImportWSLogRepository.GetAll();

                uow.Commit();
            }

            if (!result.Obj.IsValid())
            {
                result.NotFound();
            }

            return result;
        }

        public async Task<Result<IMPORTWSLOG>> GetById(int id)
        {
            Result<IMPORTWSLOG> result = id.IsValueValid<IMPORTWSLOG>(nameof(IMPORTWSLOG.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                {
                    result.Obj = await uow.ImportWSLogRepository.GetById(id);

                    uow.Commit();
                }

                if (result.Obj == null)
                {
                    result.NotFound(id);
                }
            }

            return result;
        }

        public async Task<Result<IMPORTWSLOG>> Insert(IMPORTWSLOG importWSLog)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
            {
                Result<IMPORTWSLOG> result = await IsValidInsert(importWSLog);

                if (result.Success)
                {
                    importWSLog.CREUSR_0 = AuthenticatedUser.USR_0;
                    importWSLog.UPDUSR_0 = AuthenticatedUser.USR_0;

                    result = await uow.ImportWSLogRepository.Insert(importWSLog);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<IMPORTWSLOG>> Update(IMPORTWSLOG ImportWS)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
            {
                Result<IMPORTWSLOG> result = await IsValidUpdate(ImportWS);

                if (result.Success)
                {
                    ImportWS.UPDUSR_0 = AuthenticatedUser.USR_0;

                    result = await uow.ImportWSLogRepository.Update(ImportWS);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<IMPORTWSLOG>> IsValidInsert(IMPORTWSLOG ImportWS)
        {
            Result<IMPORTWSLOG> result = await ImportWS.IsModelOnInsertValidAsync();

            return result;
        }

        public async Task<Result<IMPORTWSLOG>> IsValidUpdate(IMPORTWSLOG ImportWS)
        {
            Result<IMPORTWSLOG> result = await ImportWS.IsModelOnUpdateValidAsync();

            return result;
        }

        #endregion
    }
}
