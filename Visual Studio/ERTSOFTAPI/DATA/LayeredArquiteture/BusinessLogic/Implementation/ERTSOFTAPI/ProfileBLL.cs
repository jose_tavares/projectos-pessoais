﻿using MODELS.Helpers;
using System.Transactions;
using System.Threading.Tasks;
using DATA.Helpers.StaticClasses;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;
using DATA.LayeredArquiteture.Repositories.UnitOfWork;

namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{
    public class ProfileBLL : IProfileBLL
    {
        public USER AuthenticatedUser { get; set; }

        public ProfileBLL()
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();
        }

        #region Public Methods
        public async Task<Result<IEnumerable<PROFILE>>> GetAll()
        {
            Result<IEnumerable<PROFILE>> result = new Result<IEnumerable<PROFILE>>(true);

            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Snapshot))
            {
                result.Obj = await uow.ProfileRepository.GetAll();

                uow.Commit();
            }

            if (!result.Obj.IsValid())
            {
                result.NotFound();
            }

            return result;
        }

        public async Task<Result<PROFILE>> GetById(int id)
        {
            Result<PROFILE> result = id.IsValueValid<PROFILE>(nameof(PROFILE.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                {
                    result.Obj = await uow.ProfileRepository.GetById(id);

                    uow.Commit();
                }

                if (result.Obj == null)
                {
                    result.NotFound(id);
                }
            }

            return result;
        }

        public async Task<Result<PROFILE>> GetByUniqueField(string PROFILE_0)
        {
            Result<PROFILE> result = PROFILE_0.IsValueValid<PROFILE>(nameof(PROFILE.PROFILE_0));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                {
                    result.Obj = await uow.ProfileRepository.GetByUniqueField(nameof(PROFILE.PROFILE_0), PROFILE_0);

                    uow.Commit();
                }

                if (result.Obj == null)
                {
                    result.NotFound(nameof(PROFILE.PROFILE_0), PROFILE_0);
                }
            }

            return result;
        }

        public async Task<Result<PROFILE>> GetByUniqueFieldWithUserList(string PROFILE_0)
        {
            Result<PROFILE> result = PROFILE_0.IsValueValid<PROFILE>(nameof(PROFILE.PROFILE_0));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                {
                    result.Obj = await uow.ProfileRepository.GetByUniqueFieldWithUserList(PROFILE_0);

                    uow.Commit();
                }

                if (result.Obj == null)
                {
                    result.NotFound(nameof(PROFILE.PROFILE_0), PROFILE_0);
                }
            }

            return result;
        }

        public async Task<Result<PROFILE>> Insert(PROFILE model)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
            {
                Result<PROFILE> result = await IsValidInsert(model);

                if (result.Success)
                {
                    model.CREUSR_0 = AuthenticatedUser.USR_0;
                    model.UPDUSR_0 = AuthenticatedUser.USR_0;

                    result = await uow.ProfileRepository.Insert(model);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<PROFILE>> Update(PROFILE profile)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
            {
                Result<PROFILE> result = await IsValidUpdate(profile);

                if (result.Success)
                {
                    profile = result.Obj;
                    profile.UPDUSR_0 = AuthenticatedUser.USR_0;

                    Result<PROFILE> resultUpdate = await uow.ProfileRepository.Update(profile);
                    result.ConvertFromResult(resultUpdate, true);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<PROFILE>> IsValidInsert(PROFILE profile)
        {
            // Add more validations if needed
            Result<PROFILE> result = await profile.IsModelOnInsertValidAsync();

            if (result.Success)
            {
                Result<PROFILE> resultProfile = await GetByUniqueField(profile.PROFILE_0);

                if (resultProfile.Obj != null)
                {
                    result.AddInvalidMessage($"Já existe um perfil com o nome {profile.PROFILE_0}");
                }
            }

            return result;
        }

        public async Task<Result<PROFILE>> IsValidUpdate(PROFILE profile)
        {
            Result<PROFILE> result = await profile.IsModelOnUpdateValidAsync();

            if (result.Success)
            {
                result = await GetById(profile.ROWID);

                if (result.Success)
                {
                    PROFILE profileTemp = result.Obj;

                    result = DBHelper.ChangeModelByUnModifiableFields(profileTemp, profile, nameof(PROFILE.PROFILE_0));

                    //Add validations here
                }
            }

            return result;
        }
        #endregion
    }
}
