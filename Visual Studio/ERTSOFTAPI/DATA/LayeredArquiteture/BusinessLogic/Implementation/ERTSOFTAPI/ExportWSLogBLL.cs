﻿using MODELS.Helpers;
using System.Threading.Tasks;
using System.Transactions;
using System.Collections.Generic;
using DATA.Helpers.StaticClasses;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.Repositories.UnitOfWork;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;

namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{
    public class ExportWSLogBLL : IExportWSLogBLL
    {
        public USER AuthenticatedUser { get; set; }

        public ExportWSLogBLL()
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();
        }

        #region Public Methods
        public async Task<Result<IEnumerable<EXPORTWSLOG>>> GetAll()
        {
            Result<IEnumerable<EXPORTWSLOG>> result = new Result<IEnumerable<EXPORTWSLOG>>(true);

            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Snapshot))
            {
                result.Obj = await uow.ExportWSLogRepository.GetAll();

                uow.Commit();
            }

            if (!result.Obj.IsValid())
            {
                result.NotFound();
            }

            return result;
        }

        public async Task<Result<EXPORTWSLOG>> GetById(int id)
        {
            Result<EXPORTWSLOG> result = id.IsValueValid<EXPORTWSLOG>(nameof(EXPORTWSLOG.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                {
                    result.Obj = await uow.ExportWSLogRepository.GetById(id);

                    uow.Commit();
                }

                if (result.Obj == null)
                {
                    result.NotFound(id);
                }
            }

            return result;
        }
        #endregion
    }
}
