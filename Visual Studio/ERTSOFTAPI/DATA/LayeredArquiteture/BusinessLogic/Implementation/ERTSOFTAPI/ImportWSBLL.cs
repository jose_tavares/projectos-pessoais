﻿using MODELS.Helpers;
using System.Transactions;
using System.Threading.Tasks;
using DATA.Helpers.StaticClasses;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;
using System.Linq;
using System;
using DATA.Helpers.AuxiliarClasses;
using DATA.LayeredArquiteture.Repositories.UnitOfWork;
using Newtonsoft.Json;

namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{
    public class ImportWSBLL : IImportWSBLL
    {
        private readonly IImportWSDataBLL importWsDataBLL;

        public USER AuthenticatedUser { get; set; }

        public ImportWSBLL(IImportWSDataBLL importWsDataBLL)
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();

            this.importWsDataBLL = importWsDataBLL;
        }

        #region Public Methods
        public async Task<Result<IEnumerable<IMPORTWS>>> GetAll()
        {
            Result<IEnumerable<IMPORTWS>> result = new Result<IEnumerable<IMPORTWS>>(true);

            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Snapshot))
            {
                result.Obj = await uow.ImportWSRepository.GetAll();

                uow.Commit();
            }

            if (!result.Obj.IsValid())
            {
                result.NotFound();
            }

            return result;
        }

        public async Task<Result<IMPORTWS>> GetById(int id)
        {
            Result<IMPORTWS> result = id.IsValueValid<IMPORTWS>(nameof(IMPORTWS.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                {
                    result.Obj = await uow.ImportWSRepository.GetById(id);

                    uow.Commit();
                }

                if (result.Obj == null)
                {
                    result.NotFound(id);
                }
            }

            return result;
        }

        public async Task<Result<dynamic>> GetDynamicById(int id, string SQLTABLE_0, IEnumerable<string> fields)
        {
            Result<dynamic> result = new Result<dynamic>(true);
            Result<IMPORTWS> importWsResult = id.IsValueValid<IMPORTWS>(nameof(IMPORTWS.ROWID));

            if (!importWsResult.Success)
            {
                result.ConvertFromResult(importWsResult);

                return result;
            }

            importWsResult = SQLTABLE_0.IsValueValid<IMPORTWS>(nameof(IMPORTWS.SQLTABLE_0));

            if (!importWsResult.Success)
            {
                result.ConvertFromResult(importWsResult);

                return result;
            }

            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
            {
                result.Obj = await uow.ImportWSRepository.GetDynamicById(id, SQLTABLE_0, fields);

                uow.Commit();
            }   

            if (result.Obj == null)
            {
                result.NotFound(new Dictionary<string, object>
                {
                    { nameof(DBRules.ROWID), id },
                    { nameof(IMPORTWS.SQLTABLE_0), SQLTABLE_0 }
                });
            }

            return result;
        }

        public async Task<Result<IMPORTWS>> GetByUniqueField(string WSCODE_0)
        {
            Result<IMPORTWS> result = WSCODE_0.IsValueValid<IMPORTWS>(nameof(IMPORTWS.WSCODE_0));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                {
                    result.Obj = await uow.ImportWSRepository.GetByUniqueField(nameof(IMPORTWS.WSCODE_0), WSCODE_0);

                    uow.Commit();
                }

                if (result.Obj == null)
                {
                    result.NotFound(nameof(IMPORTWS.WSCODE_0), WSCODE_0);
                }
            }

            return result;
        }

        public async Task<(Result<dynamic>, IMPORTWS importWs)> GetFirstPendingByWsCode(string WSCODE_0)
        {
            Result<dynamic> result = new Result<dynamic>(true);
            Result<IMPORTWSDATA> importWsDataResult = await importWsDataBLL.GetFirstPendingByWsCode(WSCODE_0);
            result.ConvertFromResult(importWsDataResult);

            if (result.Success)
            {
                IMPORTWSDATA importWsData = importWsDataResult.Obj;
                List<string> fields = new List<string>();

                foreach (WSFIELDCONFIG wsField in importWsData.ImportWsModel.WebServiceModel.WsFieldConfigList)
                {
                    if (wsField.FLDFLG_0 == 2 && wsField.VALUEFLG_0 == 1)
                    {
                        fields.Add(wsField.VALUE_0);
                    }
                }

                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
                {
                    result = await GetDynamicById(importWsData.ID, importWsData.ImportWsModel.SQLTABLE_0, fields);

                    uow.Commit();
                }
            }

            return (result, importWsDataResult.Obj?.ImportWsModel);
        }

        public async Task<Result<IMPORTWS>> ImportFirstPendingByWsCode(string WSCODE_0)
        {
            Result<IMPORTWS> result = new Result<IMPORTWS>();

            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable, TransactionScopeOption.RequiresNew, TimeSpan.FromMinutes(5)))
            {
                (Result<dynamic> dataResult, IMPORTWS importWs) = await IsValidImport(WSCODE_0);
                result.ConvertFromResult(dataResult);

                if (dataResult.Success)
                {
                    dynamic import = dataResult.Obj;
                    string XML = WebServiceHelper.GetXMLByDynamic(import, importWs.WebServiceModel.WsFieldConfigList);

                    Result<WebServiceData> webServiceDataResult = await WebServiceHelper.RunWebServiceAsync(importWs.WebServiceModel, XML);

                    result.ConvertFromResult(webServiceDataResult);
                    result.Messages.Insert(0, $"A linha {import.ROWID} da tabela {importWs.SQLTABLE_0} {(result.Success ? string.Empty : " não")} foi importada com sucesso.");

                    Result<IMPORTWS> processResult = await ProcessWebServiceDataResult(uow, webServiceDataResult, importWs, result.Messages.First());

                    if (processResult.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<IMPORTWS>> Insert(IMPORTWS importWS)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
            {
                Result<IMPORTWS> result = await IsValidInsert(importWS);

                if (result.Success)
                {
                    importWS.CREUSR_0 = AuthenticatedUser.USR_0;
                    importWS.UPDUSR_0 = AuthenticatedUser.USR_0;

                    result = await uow.ImportWSRepository.Insert(importWS);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<IMPORTWS>> IsValidInsert(IMPORTWS importWs)
        {
            Result<IMPORTWS> result = await importWs.IsModelOnInsertValidAsync();

            //Validar se já existe com a chave IMPORTWS_0
            //Validar se já existe para o WSCODE_0
            //Ver se o SQLTABLE_0 é uma tabela válida no SQL
            //MAXATTEMPTS_0 > 0
            //na tabela WEBSERVICE, o webservice tem que ter o IMPORTFLG_0 = 2
            //O Enabflg_0 só pode ficar a 2 se na tabela webservice o ENABFLG_0 for = 2
            //No webservice quando é feito update para desativar, tem que desativar aqui também

            return result;
        }

        public async Task<(Result<dynamic> resultImport, IMPORTWS importWs)> IsValidImport(string WSCODE_0)
        {
            (Result<dynamic> dataResult, IMPORTWS importWs) result = await GetFirstPendingByWsCode(WSCODE_0);

            return result;
        }
        #endregion

        #region Private Methods
        private async Task<Result<IMPORTWS>> ProcessWebServiceDataResult(IUnitOfWork uow, Result<WebServiceData> webServiceDataResult, IMPORTWS importWs, string OUTPUT_0)
        {
            Result<IMPORTWS> result = new Result<IMPORTWS>();

            if (webServiceDataResult == null)
            {
                result.AddInvalidMessage("Parametro webServiceDataResult encontra-se null.");

                return result;
            }

            if (importWs == null)
            {
                result.AddInvalidMessage("Parametro importWs encontra-se null.");

                return result;
            }

            if (webServiceDataResult.Obj == null)
            {
                result.AddInvalidMessage("O objeto WebServiceData do parametro webServiceDataResult encontra-se null.");

                return result;
            }

            WebServiceData webServiceData = webServiceDataResult.Obj;

            importWs.ImportWsDataModel.XML_0 = webServiceData.XML;
            importWs.ImportWsDataModel.OUTPUT_0 = OUTPUT_0;
            importWs.ImportWsDataModel.OUTPUTWS_0 = webServiceData.OUTPUTWS_0;
            importWs.ImportWsDataModel.VCRNUM_0 = webServiceData.Document;
            importWs.ImportWsDataModel.WSUSR_0 = importWs.WebServiceModel.USER_0;
            importWs.ImportWsDataModel.ATTEMPTS_0 += 1;
            importWs.ImportWsDataModel.SUCCESSFLG_0 = Convert.ToInt32(result.Success) + 1;

            Result<IMPORTWSDATA> importWsDataResult = await uow.ImportWSDataRepository.Update(importWs.ImportWsDataModel);
            result.ConvertFromResult(importWsDataResult);

            if (result.Success)
            {
                string importWsDataJson = JsonConvert.SerializeObject(importWs.ImportWsDataModel);
                IMPORTWSLOG importWsLog = JsonConvert.DeserializeObject<IMPORTWSLOG>(importWsDataJson);
                importWsLog.IMPORTID = importWs.ImportWsDataModel.ROWID;

                Result<IMPORTWSLOG> importWsLogResult = await uow.ImportWSLogRepository.Insert(importWsLog);
                result.ConvertFromResult(importWsLogResult);
            }

            return result;
        }
        #endregion
    }
}
