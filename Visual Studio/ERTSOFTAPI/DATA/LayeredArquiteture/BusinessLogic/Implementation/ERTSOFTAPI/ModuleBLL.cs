﻿using MODELS.Helpers;
using System.Transactions;
using System.Threading.Tasks;
using DATA.Helpers.StaticClasses;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;
using DATA.LayeredArquiteture.Repositories.UnitOfWork;

namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{
    public class ModuleBLL : IModuleBLL
    {
        public USER AuthenticatedUser { get; set; }

        public ModuleBLL()
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();
        }

        public async Task<Result<IEnumerable<MODULE>>> GetAll()
        {
            Result<IEnumerable<MODULE>> result = new Result<IEnumerable<MODULE>>(true);

            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Snapshot))
            {
                result.Obj = await uow.ModuleRepository.GetAll();

                uow.Commit();
            }

            if (!result.Obj.IsValid())
            {
                result.NotFound();
            }

            return result;
        }

        public async Task<Result<IEnumerable<MODULE>>> GetByApp(string APP_0)
        {
            Result<IEnumerable<MODULE>> result = APP_0.IsValueValid<IEnumerable<MODULE>>(nameof(MODULE.APP_0));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                {
                    result.Obj = await uow.ModuleRepository.GetByField(nameof(MODULE.APP_0), APP_0);

                    uow.Commit();
                }

                if (!result.Obj.IsValid())
                {
                    result.NotFound(nameof(MODULE.APP_0), APP_0);
                }
            }

            return result;
        }

        public async Task<Result<IEnumerable<MODULE>>> GetByModule(string MODULE_0)
        {
            Result<IEnumerable<MODULE>> result = MODULE_0.IsValueValid<IEnumerable<MODULE>>(nameof(MODULE.MODULE_0));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                {
                    result.Obj = await uow.ModuleRepository.GetByField(nameof(MODULE.MODULE_0), MODULE_0);

                    uow.Commit();
                }

                if (!result.Obj.IsValid())
                {
                    result.NotFound(nameof(MODULE.MODULE_0), MODULE_0);
                }
            }

            return result;
        }

        public async Task<Result<MODULE>> GetById(int id)
        {
            Result<MODULE> result = id.IsValueValid<MODULE>(nameof(MODULE.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                {
                    result.Obj = await uow.ModuleRepository.GetById(id);

                    uow.Commit();
                }

                if (result.Obj == null)
                {
                    result.NotFound(id);
                }
            }

            return result;
        }

        public async Task<Result<MODULE>> GetByUniqueFields(string APP_0, string MODULE_0)
        {
            Result<Dictionary<string, object>> resultFields = ClassHelper.GetDictionaryFieldsFromObject(new MODULE()
            {
                APP_0 = APP_0,
                MODULE_0 = MODULE_0
            }, nameof(MODULE.APP_0), nameof(MODULE.MODULE_0));
            Result<MODULE> result = Result<MODULE>.ConvertAndGetFromResult(resultFields);

            if (result.Success)
            {
                Dictionary<string, object> fields = resultFields.Obj;

                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                {
                    result.Obj = await uow.ModuleRepository.GetByUniqueFields(fields);

                    uow.Commit();
                }

                if (result.Obj == null)
                {
                    result.NotFound(fields);
                }
            }

            return result;
        }

        public async Task<Result<MODULE>> GetByUniqueFieldsWithScreenList(string APP_0, string MODULE_0)
        {
            Result<Dictionary<string, object>> resultFields = ClassHelper.GetDictionaryFieldsFromObject(new MODULE()
            {
                APP_0 = APP_0,
                MODULE_0 = MODULE_0
            }, nameof(MODULE.APP_0), nameof(MODULE.MODULE_0));
            Result<MODULE> result = Result<MODULE>.ConvertAndGetFromResult(resultFields);

            if (result.Success)
            {
                Dictionary<string, object> fields = resultFields.Obj;

                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                {
                    result.Obj = await uow.ModuleRepository.GetByUniqueFieldsWithScreenList(APP_0, MODULE_0);

                    uow.Commit();
                }

                if (result.Obj == null)
                {
                    result.NotFound(fields);
                }
            }

            return result;
        }

        public async Task<Result<MODULE>> Insert(MODULE module)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
            {
                Result<MODULE> result = await IsValidInsert(module);

                if (result.Success)
                {
                    module.CREUSR_0 = AuthenticatedUser.USR_0;
                    module.UPDUSR_0 = AuthenticatedUser.USR_0;

                    result = await uow.ModuleRepository.Insert(module);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<MODULE>> Update(MODULE module)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
            {
                Result<MODULE> result = await IsValidUpdate(module);

                if (result.Success)
                {
                    module = result.Obj;
                    module.UPDUSR_0 = AuthenticatedUser.USR_0;

                    Result<MODULE> resultUpdate = await uow.ModuleRepository.Update(module);
                    result.ConvertFromResult(resultUpdate, true);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<MODULE>> IsValidInsert(MODULE module)
        {
            Result<MODULE> result = await module.IsModelOnInsertValidAsync();

            if (result.Success)
            {
                //Verificar se a APP existe antes de inserir

                Result<MODULE> resultModule = await GetByUniqueFields(module.APP_0, module.MODULE_0);

                if (resultModule.Obj != null)
                {
                    result.AddInvalidMessage($"Já existe um módulo com o nome '{module.MODULE_0}' associado à aplicação '{module.APP_0}'");
                }

                //Add validations here
            }

            return result;
        }

        public async Task<Result<MODULE>> IsValidUpdate(MODULE module)
        {
            Result<MODULE> result = await module.IsModelOnUpdateValidAsync();

            if (result.Success)
            {
                result = await GetById(module.ROWID);

                if (result.Success)
                {
                    MODULE moduleTemp = result.Obj;

                    result = DBHelper.ChangeModelByUnModifiableFields(moduleTemp, module, nameof(MODULE.APP_0), nameof(MODULE.MODULE_0));

                    //Add validations here
                }         
            }

            return result;
        }
    }
}
