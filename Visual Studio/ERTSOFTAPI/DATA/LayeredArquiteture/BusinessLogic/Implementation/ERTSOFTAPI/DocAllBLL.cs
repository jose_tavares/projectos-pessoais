﻿using System.Linq;
using MODELS.Helpers;
using System.Transactions;
using System.Threading.Tasks;
using DATA.Helpers.StaticClasses;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;
using DATA.LayeredArquiteture.Repositories.UnitOfWork;

namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{
    class DocAllBLL : IDocAllBLL
    {
        public USER AuthenticatedUser { get; set; }

        public DocAllBLL()
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();
        }

        #region Public Methods
        public async Task<Result<IEnumerable<DOCALL>>> GetAll()
        {
            Result<IEnumerable<DOCALL>> result = new Result<IEnumerable<DOCALL>>(true);

            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Snapshot))
            {
                result.Obj = await uow.DocAllRepository.GetAll();

                uow.Commit();
            }

            if (!result.Obj.IsValid())
            {
                result.NotFound();
            }

            return result;
        }

        public async Task<Result<IEnumerable<DOCALL>>> GetDocAllAvailable(string DOCUMENT_0)
        {
            Result<IEnumerable<DOCALL>> result = DOCUMENT_0.IsValueValid<IEnumerable<DOCALL>>(nameof(DOCALL.DOCUMENT_0));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
                {
                    IEnumerable<DOCALL> docAllList = await uow.DocAllRepository.GetAvailableByDocument(DOCUMENT_0);

                    uow.Commit();
                }

                if (!result.Obj.IsValid())
                {
                    result.NotFound(nameof(DOCALL.DOCUMENT_0), DOCUMENT_0);
                }
            }

            return result;
        }

        public async Task<Result<IEnumerable<DOCALL>>> GetDocItmAllAvailable(string STOFCY_0, string DOCUMENT_0, string ITMREF_0)
        {
            Dictionary<string, object> fields = new Dictionary<string, object>
            {
                { nameof(DOCALL.STOFCY_0), STOFCY_0 },
                { nameof(DOCALL.DOCUMENT_0), DOCUMENT_0 },
                { nameof(DOCALL.ITMREF_0), ITMREF_0 }
            };

            Result<IEnumerable<DOCALL>> result = ClassHelper.IsPropertiesValid<IEnumerable<DOCALL>>(fields);

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
                {
                    IEnumerable<DOCALL> docAllList = await uow.DocAllRepository.GetAvailableByStofcyDocumentItmref(STOFCY_0, DOCUMENT_0, ITMREF_0);

                    uow.Commit();
                }

                if (!result.Obj.IsValid())
                {
                    result.NotFound(nameof(DOCALL.DOCUMENT_0), DOCUMENT_0);
                }
            }

            return result;
        }

        public async Task<Result<DOCALL>> GetById(int id)
        {
            Result<DOCALL> result = id.IsValueValid<DOCALL>(nameof(DOCALL.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
                {
                    result.Obj = await uow.DocAllRepository.GetById(id);

                    uow.Commit();
                }

                if (result.Obj == null)
                {
                    result.NotFound(id);
                }
            }

            return result;
        }

        public async Task<Result<DOCALL>> Insert(DOCALL docAll)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
            {
                Result<DOCALL> result = await IsValidInsert(docAll);

                if (result.Success)
                {
                    docAll = result.Obj;
                    docAll.CREUSR_0 = AuthenticatedUser.USR_0;
                    docAll.UPDUSR_0 = AuthenticatedUser.USR_0;

                    result = await uow.DocAllRepository.Insert(docAll);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<DOCALL>> Update(DOCALL docAll)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
            {
                Result<DOCALL> result = await IsValidUpdate(docAll);

                if (result.Success)
                {
                    docAll = result.Obj;
                    docAll.UPDUSR_0 = AuthenticatedUser.USR_0;

                    Result<DOCALL> resultUpdate = await uow.DocAllRepository.Update(docAll);
                    result.ConvertFromResult(resultUpdate, true);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<DOCALL>> IsValidInsert(DOCALL docAll)
        {
            // Add more validations if needed
            Result<DOCALL> result = await docAll.IsModelOnInsertValidAsync();

            //Validar se documento é válido. (por agora se a OF existe e está aberta).
            //Artigo Lote e sublote e qtd e localizacao e status existem em stock descontndo a diferença na stojou

            return result;
        }

        public async Task<Result<DOCALL>> IsValidUpdate(DOCALL docAll)
        {
            Result<DOCALL> result = await docAll.IsModelOnUpdateValidAsync();

            if (result.Success)
            {
                result = await GetById(docAll.ROWID);

                if (result.Success)
                {
                    DOCALL docAllTemp = result.Obj;

                    result = DBHelper.ChangeModelByModifiableFields(docAllTemp, docAll, nameof(DOCALL.QTYPCU_0), nameof(DOCALL.AVAILFLG_0));

                    //Add validations here
                }
            }

            return result;
        }
        #endregion
    }
}
