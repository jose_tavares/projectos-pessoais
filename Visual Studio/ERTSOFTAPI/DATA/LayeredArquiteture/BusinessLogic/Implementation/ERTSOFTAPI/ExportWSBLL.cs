﻿namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{
    using System;
    using System.Xml;
    using MODELS.Helpers;
    using System.Diagnostics;
    using System.Transactions;
    using System.Data.SqlClient;
    using System.Threading.Tasks;
    using DATA.Helpers.StaticClasses;
    using System.Collections.Generic;
    using DATA.Helpers.AuxiliarClasses;
    using MODELS.BusinessObject.ERTSOFT.Database;
    using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;
    using System.Linq;
    using DATA.LayeredArquiteture.Repositories.UnitOfWork;

    public class ExportWSBLL : IExportWSBLL
    {
        public USER AuthenticatedUser { get; set; }

        public ExportWSBLL()
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();
        }

        #region Public Methods
        public async Task<Result<IEnumerable<EXPORTWS>>> GetAll()
        {
            Result<IEnumerable<EXPORTWS>> result = new Result<IEnumerable<EXPORTWS>>(true);

            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Snapshot))
            {
                result.Obj = await uow.ExportWSRepository.GetAll();

                uow.Commit();
            }

            if (!result.Obj.IsValid())
            {
                result.NotFound();
            }

            return result;
        }

        public async Task<IEnumerable<EXPORTWS>> GetPending()
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
            {
                IEnumerable<EXPORTWS> exportWSList = await uow.ExportWSRepository.GetPending();

                uow.Commit();

                return exportWSList;
            }
        }

        public async Task<Result<EXPORTWS>> GetById(int id)
        {
            Result<EXPORTWS> result = id.IsValueValid<EXPORTWS>(nameof(EXPORTWS.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                {
                    result.Obj = await uow.ExportWSRepository.GetById(id);

                    uow.Commit();
                }

                if (result.Obj == null)
                {
                    result.NotFound(id);
                }
            }

            return result;
        }

        public async Task<Result<EXPORTWS>> GetByUniqueField(string WSCODE_0)
        {
            Result<EXPORTWS> result = WSCODE_0.IsValueValid<EXPORTWS>(nameof(EXPORTWS.WSCODE_0));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                {
                    result.Obj = await uow.ExportWSRepository.GetByUniqueField(nameof(EXPORTWS.WSCODE_0), WSCODE_0);

                    uow.Commit();
                }

                if (result.Obj == null)
                {
                    result.NotFound(nameof(EXPORTWS.WSCODE_0), WSCODE_0);
                }
            }

            return result;
        }

        public async Task<Result<EXPORTWS>> ExportPending()
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable, TransactionScopeOption.Required, TransactionManager.MaximumTimeout))
            {
                EXPORTWS export = await GetFirstPending();
                Result<EXPORTWS> result = new Result<EXPORTWS>(true);

                if (export == null)
                {
                    result.AddValidMessage("Não existe nenhum registo pendente de exportação.");

                    return result;
                }

                if (export.WebServiceModel == null)
                {
                    result.AddInvalidMessage($"Webservice da exportação não encontrado. Code a procurar: {export.WSCODE_0}");

                    return result;
                }

                Stopwatch sw = Stopwatch.StartNew();

                if (export.NEXTREWRITEDATE_0 <= DateTime.UtcNow)
                {
                    export.REWRITEFLG_0 = 2;
                }

                Dictionary<string, SqlParameter> parameters = uow.ExportWSRepository.GetParameters(export.MAINTABLE_0);
                EXPORTWSLOG exportLog = GetExportLogModel(export);

                using (IUnitOfWork noTransactionUow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
                {
                    Result<EXPORTWSLOG> exportLogResult = await noTransactionUow.ExportWSLogRepository.Insert(exportLog);
                    result.ConvertFromResult(exportLogResult);
                    exportLog.ROWID = result.InsertedId;
                }

                if (result.Success)
                {
                    (Result<WebServiceData> result, EXPORTWS export, EXPORTWSLOG exportLog, DateTime initialDate) data = (default, default, default, default);

                    try
                    {
                        if (export.REWRITEFLG_0 == 1) //Aqui vou apenas escrever a diferença através de webservice
                        {
                            data = await ProcessByWebService(uow, export, exportLog, parameters, export.NEXTWRITEDATE_0);
                        }
                        else //Aqui vou reescrever tudo através da query configurada no EXPORTWS
                        {
                            data = await ProcessByQuery(uow, export, exportLog, parameters, export.NEXTREWRITEDATE_0);
                        }

                        result.ConvertFromResult(data.result);

                        if (result.Success)
                        {
                            export.REWRITEFLG_0 = 1;
                            export.LASTWRITEDATE_0 = data.initialDate;

                            result = await uow.ExportWSRepository.Update(export);
                        }         
                    }
                    catch (Exception e)
                    {
                        data.result.Success = false;
                        data.exportLog.OUTPUT_0 = e.Message;
                        result.Messages.Clear();
                        result.AddInvalidMessage(e.Message);
                    }

                    using (IUnitOfWork noTransactionUow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
                    {
                        Result<EXPORTWSLOG> exportLogResult = await UpdateExportLog(uow, data.export, data.result, data.exportLog, Convert.ToInt32(sw.ElapsedMilliseconds));

                        if (result.Success && exportLogResult.Success)
                        {
                            uow.Commit();
                        }
                    }
                }

                return result;
            }
        }

        public async Task<Result<EXPORTWS>> Insert(EXPORTWS exportWS)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
            {
                Result<EXPORTWS> result = await IsValidInsert(exportWS);

                if (result.Success)
                {
                    exportWS = result.Obj;
                    exportWS.CREUSR_0 = AuthenticatedUser.USR_0;
                    exportWS.UPDUSR_0 = AuthenticatedUser.USR_0;

                    result = await uow.ExportWSRepository.Insert(exportWS);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<EXPORTWS>> Update(EXPORTWS exportWS)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
            {
                Result<EXPORTWS> result = await IsValidUpdate(exportWS);

                if (result.Success)
                {
                    exportWS = result.Obj;
                    exportWS.UPDUSR_0 = AuthenticatedUser.USR_0;

                    Result<EXPORTWS> resultUpdate = await uow.ExportWSRepository.Update(exportWS);
                    result.ConvertFromResult(resultUpdate, true);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<EXPORTWS>> IsValidInsert(EXPORTWS exportWS)
        {
            // Add more validations if needed
            Result<EXPORTWS> result = await exportWS.IsModelOnInsertValidAsync();

            if (result.Success)
            {
                Result<EXPORTWS> resultExportWs = await GetByUniqueField(exportWS.WSCODE_0);

                if (resultExportWs.Success) //Se encontrou o exportWs
                {
                    result.AddInvalidMessage($"Já existe uma exportação com o código de webservice '{exportWS.WSCODE_0}'");

                    return result;
                }
            }

            //Verificar se o webservice a atualizar existe na Tabela WEBSERVICE
            //Verificar se a MAINTABLE, TEMPTABLE, LOGTABLE existe como tabela no SQL
            //Verificar se a SELECTQUERY_0 e DELETEQUERY_0 não dá erro a executar
            //RewriteFrequency tem de ser superior ao writefrequency

            return result;
        }

        public async Task<Result<EXPORTWS>> IsValidUpdate(EXPORTWS exportWS)
        {
            Result<EXPORTWS> result = await exportWS.IsModelOnUpdateValidAsync();

            if (result.Success)
            {
                result = await GetById(exportWS.ROWID);

                if (result.Success)
                {
                    EXPORTWS exportWsTemp = result.Obj;

                    result = DBHelper.ChangeModelByUnModifiableFields(exportWsTemp, exportWS, nameof(EXPORTWS.WSCODE_0));

                    //Verificar se o webservice a atualizar existe na Tabela WEBSERVICE
                    //Verificar se a MAINTABLE, TEMPTABLE, LOGTABLE existe como tabela no SQL
                    //Verificar se a SELECTQUERY_0 e DELETEQUERY_0 não dá erro a executar
                    //RewriteFrequency tem de ser superior ao writefrequency
                    //Add validations here
                }
            }

            return result;
        }
        #endregion

        #region Private Methods
        private async Task<EXPORTWS> GetFirstPending()
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
            {
                EXPORTWS exportWS = await uow.ExportWSRepository.GetFirstPending();

                if (exportWS != null)
                {
                    exportWS.WebServiceModel = await uow.WebServiceRepository.GetByUniqueField(nameof(WEBSERVICE.CODE_0), exportWS.WSCODE_0);
                }

                uow.Commit();

                return exportWS;
            }
        }

        private EXPORTWSLOG GetExportLogModel(EXPORTWS export)
        {
            EXPORTWSLOG exportLog = new EXPORTWSLOG
            {
                WSCODE_0 = export.WSCODE_0,
                WSNAM_0 = export.WebServiceModel.NAM_0,
                OUTPUTWS_0 = string.Empty,
                OUTPUT_0 = string.Empty,
                XML_0 = string.Empty,
                SUCCESSFLG_0 = 1,
                REWRITEFLG_0 = export.REWRITEFLG_0,
                CREUSR_0 = AuthenticatedUser.USR_0,
                UPDUSR_0 = AuthenticatedUser.USR_0
            };

            return exportLog;
        }

        private async Task<(Result<WebServiceData> result, EXPORTWS export, EXPORTWSLOG exportLog, DateTime initialDate)> ProcessByWebService(IUnitOfWork uow, EXPORTWS export, EXPORTWSLOG exportLog, Dictionary<string, SqlParameter> parameters, DateTime initialDate)
        {
            Result<WebServiceData> result = new Result<WebServiceData>(true)
            {
                Obj = new WebServiceData
                {
                    Document = string.Empty,
                    XML = string.Empty,
                    WebService = export.WebServiceModel
                }
            };

            try
            {
                string XML = export.PARAMBEGIN_0.Replace("@DATE", export.LASTWRITEDATE_0.ToString("yyyyMMdd HH:mm:ss"));
                result = await WebServiceHelper.RunWebServiceAsync(export.WebServiceModel, XML);       

                if (!result.Success)
                {
                    exportLog.OUTPUT_0 = $"Ocorreu algum problema na invocação do Webservice com o código '{export.WebServiceModel.CODE_0}'";
                    export.NEXTWRITEDATE_0 = initialDate;

                    return (result, export, exportLog, initialDate);
                }

                string lastRowDate = string.Empty;
                WebServiceData webServiceData = result.Obj;
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.LoadXml(webServiceData.ResultXML);
                XmlNodeList nodesList = xmlDocument.SelectNodes("RESULT/TAB/LIN");
                List<ExportData> exportDataList = new List<ExportData>();

                foreach (XmlNode node in nodesList)
                {
                    exportDataList.Add(uow.ExportWSRepository.GetExportDataByXMLNode(export, node, parameters, exportLog.ROWID));
                }

                Result<EXPORTWSLOG> exportWsLogResult = await uow.ExportWSRepository.InsertFromExportDataList(exportDataList, export, exportLog);
                result.ConvertFromResult(exportWsLogResult);

                if (result.Success)
                {
                    exportLog = exportWsLogResult.Obj;

                    if (exportDataList.IsValid())
                    {
                        lastRowDate = exportDataList.Last().DateValue;
                    }

                    if (nodesList.Count == export.MAXROWS_0)
                    {
                        initialDate = Convert.ToDateTime(lastRowDate);

                        if (export.LASTWRITEDATE_0 == initialDate)
                        {
                            initialDate = initialDate.AddSeconds(1);
                        }

                        export.NEXTWRITEDATE_0 = initialDate;
                    }
                    else
                    {
                        export.NEXTWRITEDATE_0 = initialDate.AddSeconds(export.WRITEFREQUENCY_0);
                    }

                    exportLog.OUTPUT_0 = "Exportação efetuada com sucesso.";
                }        
            }
            catch (Exception e)
            {
                result.Success = false;
                exportLog.OUTPUT_0 = e.Message;
            }

            return (result, export, exportLog, initialDate);
        }

        private async Task<(Result<WebServiceData> result, EXPORTWS export, EXPORTWSLOG exportLog, DateTime initialDate)> ProcessByQuery(IUnitOfWork uow, EXPORTWS export, EXPORTWSLOG exportLog, Dictionary<string, SqlParameter> parameters, DateTime initialDate)
        {
            Result<WebServiceData> result = new Result<WebServiceData>(true)
            {
                Obj = new WebServiceData
                {
                    Document = string.Empty,
                    XML = string.Empty,
                    WebService = export.WebServiceModel
                }
            };

            try
            {
                exportLog.DELETEDROWS_0 = await uow.ExportWSRepository.DeleteFromQuery(export.DELETEQUERY_0);
                Result<EXPORTWSLOG> exportWsLogResult = await uow.ExportWSRepository.InsertFromSelectQuery(export, exportLog, parameters);
                result.ConvertFromResult(exportWsLogResult);

                if (result.Success)
                {
                    exportLog = exportWsLogResult.Obj;

                    export.LASTREWRITEDATE_0 = initialDate;
                    export.NEXTREWRITEDATE_0 = initialDate.AddSeconds(export.REWRITEFREQUENCY_0);
                }             
            }
            catch (Exception e)
            {
                result.Success = false;
                exportLog.OUTPUT_0 = e.Message;
            }

            return (result, export, exportLog, initialDate);
        }

        private async Task<Result<EXPORTWSLOG>> UpdateExportLog(IUnitOfWork uow, EXPORTWS export, Result<WebServiceData> webServiceDataResult, EXPORTWSLOG exportLog, int executionTime)
        {
            Result<EXPORTWSLOG> result = new Result<EXPORTWSLOG>(true);
            EXPORTWSLOG newExportLog = await uow.ExportWSLogRepository.GetById(exportLog.ROWID);

            if (newExportLog == null)
            {
                result.AddInvalidMessage($"Não foi possível encontrar no sistema o log de exportação para o Id {exportLog.ROWID}");

                return result;
            }

            newExportLog.WSNAM_0 = export.WebServiceModel.NAM_0;
            newExportLog.OUTPUT_0 = exportLog.OUTPUT_0;
            newExportLog.OUTPUTWS_0 = webServiceDataResult.ToString();
            newExportLog.XML_0 = webServiceDataResult.Obj.XML;
            newExportLog.INSERTEDROWS_0 = exportLog.INSERTEDROWS_0;
            newExportLog.UPDATEDROWS_0 = exportLog.UPDATEDROWS_0;
            newExportLog.DELETEDROWS_0 = exportLog.DELETEDROWS_0;
            newExportLog.EXECUTIONTIME_0 = executionTime;
            newExportLog.SUCCESSFLG_0 = Convert.ToInt32(webServiceDataResult.Success) + 1;

            result = await uow.ExportWSLogRepository.Update(newExportLog);

            return result;
        }
        #endregion
    }
}
