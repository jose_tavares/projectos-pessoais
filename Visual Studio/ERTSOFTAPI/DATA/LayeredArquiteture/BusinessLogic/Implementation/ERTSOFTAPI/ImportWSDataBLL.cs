﻿using MODELS.Helpers;
using System.Transactions;
using System.Threading.Tasks;
using DATA.Helpers.StaticClasses;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.Repositories.UnitOfWork;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;

namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{
    public class ImportWSDataBLL : IImportWSDataBLL
    {
        private readonly IWebServiceBLL webServiceBLL;

        public USER AuthenticatedUser { get; set; }

        public ImportWSDataBLL(IWebServiceBLL webServiceBLL)
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();

            this.webServiceBLL = webServiceBLL;
        }

        #region Public Methods
        public async Task<Result<IEnumerable<IMPORTWSDATA>>> GetAll()
        {
            Result<IEnumerable<IMPORTWSDATA>> result = new Result<IEnumerable<IMPORTWSDATA>>(true);

            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Snapshot))
            {
                result.Obj = await uow.ImportWSDataRepository.GetAll();

                uow.Commit();
            }

            if (!result.Obj.IsValid())
            {
                result.NotFound();
            }

            return result;
        }

        public async Task<Result<IMPORTWSDATA>> GetById(int id)
        {
            Result<IMPORTWSDATA> result = id.IsValueValid<IMPORTWSDATA>(nameof(IMPORTWSDATA.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                {
                    result.Obj = await uow.ImportWSDataRepository.GetById(id);

                    uow.Commit();
                }

                if (result.Obj == null)
                {
                    result.NotFound(id);
                }
            }

            return result;
        }

        public async Task<Result<IMPORTWSDATA>> GetByUniqueFields(string WSCODE_0, int ID)
        {
            Result<Dictionary<string, object>> resultFields = ClassHelper.GetDictionaryFieldsFromObject(new IMPORTWSDATA
            {
                WSCODE_0 = WSCODE_0,
                ID = ID
            }, nameof(IMPORTWSDATA.WSCODE_0), nameof(IMPORTWSDATA.ID));
            Result<IMPORTWSDATA> result = Result<IMPORTWSDATA>.ConvertAndGetFromResult(resultFields);

            if (result.Success)
            {
                Dictionary<string, object> fields = resultFields.Obj;

                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                {
                    result.Obj = await uow.ImportWSDataRepository.GetByUniqueFields(fields);

                    uow.Commit();
                }

                if (result.Obj == null)
                {
                    result.NotFound(fields);
                }
            }

            return result;
        }

        public async Task<Result<IMPORTWSDATA>> GetFirstPendingByWsCode(string WSCODE_0)
        {
            Result<IMPORTWSDATA> result = WSCODE_0.IsValueValid<IMPORTWSDATA>(nameof(IMPORTWSDATA.WSCODE_0));

            if (result.Success)
            {
                Result<WEBSERVICE> webServiceResult = await webServiceBLL.IsValidImport(WSCODE_0);
                result.ConvertFromResult(webServiceResult);

                if (result.Success)
                {
                    WEBSERVICE webService = webServiceResult.Obj;

                    if (webService == null || webService.ImportWsModel == null)
                    {
                        result.AddInvalidMessage($"O valor do webservice encontra-se a null.");

                        return result;
                    }

                    using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                    {
                        result.Obj = await uow.ImportWSDataRepository.GetFirstPendingByWsCode(WSCODE_0, webService.ImportWsModel.MAXATTEMPTS_0);

                        uow.Commit();
                    }

                    if (result.Obj == null)
                    {
                        result.AddInvalidMessage($"Não existe nada pendente para importação com o Sage para o código de webservice '{WSCODE_0}'");
                    }
                    else
                    {
                        IMPORTWSDATA importWsData = result.Obj;

                        importWsData.ImportWsModel = webService.ImportWsModel;
                        importWsData.ImportWsModel.WebServiceModel = webService;
                        importWsData.ImportWsModel.ImportWsDataModel = importWsData;

                        result.Obj = importWsData;
                    }
                }     
            }

            return result;
        }

        public async Task<Result<IMPORTWSDATA>> Insert(IMPORTWSDATA importWSData)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
            {
                Result<IMPORTWSDATA> result = await IsValidInsert(importWSData);

                if (result.Success)
                {
                    importWSData = result.Obj;
                    importWSData.CREUSR_0 = AuthenticatedUser.USR_0;
                    importWSData.UPDUSR_0 = AuthenticatedUser.USR_0;

                    result = await uow.ImportWSDataRepository.Insert(importWSData);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<IMPORTWSDATA>> Update(IMPORTWSDATA importWSData)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
            {
                Result<IMPORTWSDATA> result = await IsValidUpdate(importWSData);

                if (result.Success)
                {
                    importWSData = result.Obj;
                    importWSData.UPDUSR_0 = AuthenticatedUser.USR_0;

                    Result<IMPORTWSDATA> resultUpdate = await uow.ImportWSDataRepository.Update(importWSData);
                    result.ConvertFromResult(resultUpdate, true);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<IMPORTWSDATA>> IsValidInsert(IMPORTWSDATA importWSData)
        {
            Result<IMPORTWSDATA> result = await importWSData.IsModelOnInsertValidAsync();

            return result;
        }

        public async Task<Result<IMPORTWSDATA>> IsValidUpdate(IMPORTWSDATA importWSData)
        {
            Result<IMPORTWSDATA> result = await importWSData.IsModelOnUpdateValidAsync();

            if (result.Success)
            {
                result = await GetById(importWSData.ROWID);

                if (result.Success)
                {
                    IMPORTWSDATA importWsDataTemp = result.Obj;

                    result = DBHelper.ChangeModelByUnModifiableFields(importWsDataTemp, importWSData, nameof(IMPORTWSDATA.WSCODE_0), nameof(IMPORTWSDATA.ID));

                    //Add validations here
                }
            }

            return result;
        }
        #endregion
    }
}
