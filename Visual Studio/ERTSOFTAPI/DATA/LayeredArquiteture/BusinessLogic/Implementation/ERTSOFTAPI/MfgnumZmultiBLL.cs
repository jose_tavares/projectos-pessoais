﻿using MODELS.Helpers;
using System.Transactions;
using System.Threading.Tasks;
using DATA.Helpers.StaticClasses;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.Repositories.UnitOfWork;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;

namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{
    public class MfgnumZmultiBLL : IMfgnumZmultiBLL
    {
        public USER AuthenticatedUser { get; set; }

        public MfgnumZmultiBLL()
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();
        }

        #region Public Methods

        public async Task<Result<IEnumerable<MFGNUMZMULTI>>> GetAll()
        {
            Result<IEnumerable<MFGNUMZMULTI>> result = new Result<IEnumerable<MFGNUMZMULTI>>(true);

            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Snapshot))
            {
                result.Obj = await uow.MfgnumZmultiRepository.GetAll();

                uow.Commit();
            }

            if (!result.Obj.IsValid())
            {
                result.NotFound();
            }

            return result;
        }

        public async Task<Result<IEnumerable<MFGNUMZMULTI>>> GetByMfgnumori(string MFGNUMORI_0)
        {
            Result<IEnumerable<MFGNUMZMULTI>> result = MFGNUMORI_0.IsValueValid<IEnumerable<MFGNUMZMULTI>>(nameof(MFGNUMZMULTI.MFGNUMORI_0));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
                {
                    result.Obj = await uow.MfgnumZmultiRepository.GetByField(nameof(MFGNUMZMULTI.MFGNUMORI_0), MFGNUMORI_0);

                    uow.Commit();
                }

                if (!result.Obj.IsValid())
                {
                    result.NotFound(nameof(MFGNUMZMULTI.MFGNUMORI_0), MFGNUMORI_0);
                }
            }

            return result;
        }

        public async Task<Result<IEnumerable<MFGNUMZMULTI>>> GetByMfgnum(string MFGNUM_0)
        {
            Result<IEnumerable<MFGNUMZMULTI>> result = MFGNUM_0.IsValueValid<IEnumerable<MFGNUMZMULTI>>(nameof(MFGNUMZMULTI.MFGNUM_0));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
                {
                    result.Obj = await uow.MfgnumZmultiRepository.GetByField(nameof(MFGNUMZMULTI.MFGNUM_0), MFGNUM_0);

                    uow.Commit();
                }

                if (!result.Obj.IsValid())
                {
                    result.NotFound(nameof(MFGNUMZMULTI.MFGNUM_0), MFGNUM_0);
                }
            }

            return result;
        }

        public async Task<Result<MFGNUMZMULTI>> GetById(int id)
        {
            Result<MFGNUMZMULTI> result = id.IsValueValid<MFGNUMZMULTI>(nameof(MFGNUMZMULTI.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
                {
                    result.Obj = await uow.MfgnumZmultiRepository.GetById(id);

                    uow.Commit();
                }

                if (result.Obj == null)
                {
                    result.NotFound(id);
                }
            }

            return result;
        }

        public async Task<Result<MFGNUMZMULTI>> GetByUniqueFields(string MFGNUMORI_0, string MFGNUM_0)
        {
            Result<Dictionary<string, object>> resultFields = ClassHelper.GetDictionaryFieldsFromObject(new MFGNUMZMULTI()
            {
                MFGNUMORI_0 = MFGNUMORI_0,
                MFGNUM_0 = MFGNUM_0
            }, nameof(MFGNUMZMULTI.MFGNUMORI_0), nameof(MFGNUMZMULTI.MFGNUM_0));
            Result<MFGNUMZMULTI> result = Result<MFGNUMZMULTI>.ConvertAndGetFromResult(resultFields);

            if (result.Success)
            {
                Dictionary<string, object> fields = resultFields.Obj;

                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
                {
                    result.Obj = await uow.MfgnumZmultiRepository.GetByUniqueFields(fields);

                    uow.Commit();
                }

                if (result.Obj == null)
                {
                    result.NotFound(fields);
                }
            }

            return result;
        }

        public async Task<Result<MFGNUMZMULTI>> Insert(MFGNUMZMULTI mfgnumZmulti)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
            {
                Result<MFGNUMZMULTI> result = await IsValidInsert(mfgnumZmulti);

                if (result.Success)
                {
                    mfgnumZmulti.CREUSR_0 = AuthenticatedUser.USR_0;
                    mfgnumZmulti.UPDUSR_0 = AuthenticatedUser.USR_0;

                    result = await uow.MfgnumZmultiRepository.Insert(mfgnumZmulti);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<MFGNUMZMULTI>> Update(MFGNUMZMULTI mfgnumZmulti)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
            {
                Result<MFGNUMZMULTI> result = await IsValidUpdate(mfgnumZmulti);

                if (result.Success)
                {
                    mfgnumZmulti = result.Obj;
                    mfgnumZmulti.UPDUSR_0 = AuthenticatedUser.USR_0;

                    Result<MFGNUMZMULTI> resultUpdate = await uow.MfgnumZmultiRepository.Update(mfgnumZmulti);
                    result.ConvertFromResult(resultUpdate, true);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<MFGNUMZMULTI>> IsValidInsert(MFGNUMZMULTI mfgnumZmulti)
        {
            Result<MFGNUMZMULTI> result = await mfgnumZmulti.IsModelOnInsertValidAsync();

            if (result.Success)
            {
                Result<MFGNUMZMULTI> resultMfgnumZmulti = await GetByUniqueFields(mfgnumZmulti.MFGNUMORI_0, mfgnumZmulti.MFGNUM_0);

                if (resultMfgnumZmulti.Obj != null)
                {
                    result.AddInvalidMessage($"Já existe um registo com a OF original {mfgnumZmulti.MFGNUMORI_0}");
                }
            }

            return result;
        }

        public async Task<Result<MFGNUMZMULTI>> IsValidUpdate(MFGNUMZMULTI mfgnumZmulti)
        {
            Result<MFGNUMZMULTI> result = await mfgnumZmulti.IsModelOnUpdateValidAsync();

            if (result.Success)
            {
                result = await GetById(mfgnumZmulti.ROWID);

                if (result.Success)
                {
                    MFGNUMZMULTI mfgnumZmultiTemp = result.Obj;

                    result = DBHelper.ChangeModelByModifiableFields(mfgnumZmultiTemp, mfgnumZmulti, nameof(MFGNUMZMULTI.REGFLG_0));

                    //Add validations here
                }
            }

            return result;
        }
        #endregion
    }
}
