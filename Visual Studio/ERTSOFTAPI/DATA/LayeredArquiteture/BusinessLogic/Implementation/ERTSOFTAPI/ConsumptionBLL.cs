﻿using MODELS.Helpers;
using System.Transactions;
using System.Threading.Tasks;
using DATA.Helpers.StaticClasses;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.Repositories.UnitOfWork;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;

namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{
    public class ConsumptionBLL : IConsumptionBLL
    {
        public USER AuthenticatedUser { get; set; }

        public ConsumptionBLL()
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();
        }

        #region Public Methods
        public async Task<Result<IEnumerable<CONSUMPTION>>> GetAll()
        {
            Result<IEnumerable<CONSUMPTION>> result = new Result<IEnumerable<CONSUMPTION>>(true);

            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
            {
                result.Obj = await uow.ConsumptionRepository.GetAll();

                uow.Commit();
            }

            if (!result.Obj.IsValid())
            {
                result.NotFound();
            }

            return result;
        }

        public async Task<Result<IEnumerable<CONSUMPTION>>> GetByMfgnum(string MFGNUM_0, int REGFLG_0)
        {
            Result<Dictionary<string, object>> resultFields = ClassHelper.GetDictionaryFieldsFromObject(new CONSUMPTION()
            {
                MFGNUM_0 = MFGNUM_0,
                REGFLG_0 = REGFLG_0
            }, nameof(CONSUMPTION.MFGNUM_0), nameof(CONSUMPTION.REGFLG_0));
            Result<IEnumerable<CONSUMPTION>> result = Result<IEnumerable<CONSUMPTION>>.ConvertAndGetFromResult(resultFields);

            if (result.Success)
            {
                Dictionary<string, object> fields = resultFields.Obj;

                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                {
                    result.Obj = await uow.ConsumptionRepository.GetByFields(fields);

                    uow.Commit();
                }

                if (!result.Obj.IsValid())
                {
                    result.NotFound(nameof(CONSUMPTION.MFGNUM_0), MFGNUM_0);
                }
            }

            return result;
        }

        public async Task<Result<IEnumerable<CONSUMPTION>>> GetByMfgnumItmref(string MFGNUM_0, string ITMREF_0, int REGFLG_0)
        {
            Result<Dictionary<string, object>> resultFields = ClassHelper.GetDictionaryFieldsFromObject(new CONSUMPTION()
            {
                MFGNUM_0 = MFGNUM_0,
                ITMREF_0 = ITMREF_0,
                REGFLG_0 = REGFLG_0
            }, nameof(CONSUMPTION.MFGNUM_0), nameof(CONSUMPTION.ITMREF_0), nameof(CONSUMPTION.REGFLG_0));
            Result<IEnumerable<CONSUMPTION>> result = Result<IEnumerable<CONSUMPTION>>.ConvertAndGetFromResult(resultFields);

            if (result.Success)
            {
                Dictionary<string, object> fields = resultFields.Obj;

                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                {
                    result.Obj = await uow.ConsumptionRepository.GetByFields(fields);

                    uow.Commit();
                }

                if (!result.Obj.IsValid())
                {
                    result.NotFound(nameof(CONSUMPTION.MFGNUM_0), MFGNUM_0);
                }
            }

            return result;
        }

        public async Task<Result<IEnumerable<CONSUMPTION>>> GetByMfgnumSloori(string MFGNUM_0, string SLOORI_0, int REGFLG_0)
        {
            Result<Dictionary<string, object>> resultFields = ClassHelper.GetDictionaryFieldsFromObject(new CONSUMPTION()
            {
                MFGNUM_0 = MFGNUM_0,
                SLOORI_0 = SLOORI_0,
                REGFLG_0 = REGFLG_0
            }, nameof(CONSUMPTION.MFGNUM_0), nameof(CONSUMPTION.SLOORI_0), nameof(CONSUMPTION.REGFLG_0));
            Result<IEnumerable<CONSUMPTION>> result = Result<IEnumerable<CONSUMPTION>>.ConvertAndGetFromResult(resultFields);

            if (result.Success)
            {
                Dictionary<string, object> fields = resultFields.Obj;

                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                {
                    result.Obj = await uow.ConsumptionRepository.GetByFields(fields);

                    uow.Commit();
                }

                if (!result.Obj.IsValid())
                {
                    result.NotFound(nameof(CONSUMPTION.MFGNUM_0), MFGNUM_0);
                }
            }

            return result;
        }

        public async Task<Result<CONSUMPTION>> GetById(int id)
        {
            Result<CONSUMPTION> result = id.IsValueValid<CONSUMPTION>(nameof(CONSUMPTION.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                {
                    result.Obj = await uow.ConsumptionRepository.GetById(id);

                    uow.Commit();
                }

                if (result.Obj == null)
                {
                    result.NotFound(id);
                }
            }

            return result;
        }
        #endregion
    }
}
