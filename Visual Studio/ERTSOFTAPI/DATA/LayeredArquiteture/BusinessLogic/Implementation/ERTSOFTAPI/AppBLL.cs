﻿namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{

    using DATA.Helpers.StaticClasses;
    using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;
    using DATA.LayeredArquiteture.Repositories.UnitOfWork;
    using MODELS.BusinessObject.ERTSOFT.Database;
    using MODELS.Helpers;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using System.Transactions;

    public class AppBLL : IAppBLL
    {
        public USER AuthenticatedUser { get; set; }

        public AppBLL()
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();
        }

        #region Public Methods

        public async Task<Result<IEnumerable<APP>>> GetAll()
        {
            Result<IEnumerable<APP>> result = new Result<IEnumerable<APP>>(true);

            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Snapshot))
            {
                result.Obj = await uow.AppRepository.GetAll();

                uow.Commit();
            }

            if (!result.Obj.IsValid())
            {
                result.NotFound();
            }

            return result;
        }

        public async Task<Result<APP>> GetById(int id)
        {
            Result<APP> result = id.IsValueValid<APP>(nameof(APP.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                {
                    result.Obj = await uow.AppRepository.GetById(id);

                    uow.Commit();
                }

                if (result.Obj == null)
                {
                    result.NotFound(id);
                }
            }

            return result;
        }

        public async Task<Result<APP>> GetByUniqueField(string APP_0)
        {
            Result<APP> result = APP_0.IsValueValid<APP>(nameof(APP.APP_0));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                {
                    result.Obj = await uow.AppRepository.GetByUniqueField(nameof(APP.APP_0), APP_0);

                    uow.Commit();
                }

                if (result.Obj == null)
                {
                    result.NotFound(nameof(APP.APP_0), APP_0);
                }
            }

            return result;
        }

        public async Task<Result<APP>> GetByUniqueFieldWithModuleList(string APP_0)
        {
            Result<APP> result = APP_0.IsValueValid<APP>(nameof(APP.APP_0));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                {
                    result.Obj = await uow.AppRepository.GetByUniqueFieldWithModuleList(APP_0);

                    uow.Commit();
                }

                if (result.Obj == null)
                {
                    result.NotFound(nameof(APP.APP_0), APP_0);
                }
            }

            return result;
        }

        public async Task<Result<APP>> Insert(APP app)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
            {
                Result<APP> result = await IsValidInsert(app);

                if (result.Success)
                {
                    app.CREUSR_0 = AuthenticatedUser.USR_0;
                    app.UPDUSR_0 = AuthenticatedUser.USR_0;

                    result = await uow.AppRepository.Insert(app);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<APP>> Update(APP app)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
            {
                Result<APP> result = await IsValidUpdate(app);

                if (result.Success)
                {
                    app = result.Obj;
                    app.UPDUSR_0 = AuthenticatedUser.USR_0;

                    Result<APP> resultUpdate = await uow.AppRepository.Update(app);
                    result.ConvertFromResult(resultUpdate, true);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<APP>> IsValidInsert(APP app)
        {
            Result<APP> result = await app.IsModelOnInsertValidAsync();

            if (result.Success)
            {
                APP appTemp = ClassHelper.GetObjectFromResult(await GetByUniqueField(app.APP_0));

                if (appTemp != null) //Se encontrou a app
                {
                    result.AddInvalidMessage($"Já existe uma aplicação com o nome '{app.APP_0}'");
                }

                //Add validations here
            }

            return result;
        }

        public async Task<Result<APP>> IsValidUpdate(APP app)
        {
            Result<APP> result = await app.IsModelOnUpdateValidAsync();

            if (result.Success)
            {
                result = await GetById(app.ROWID);

                if (result.Success)
                {
                    APP appTemp = result.Obj;

                    result = DBHelper.ChangeModelByModifiableFields(appTemp, app, nameof(APP.DES_0), nameof(APP.DEVLANG_0), nameof(APP.TYP_0), nameof(APP.VRS_0), nameof(APP.ENABFLG_0));

                    //Add validations here
                }
            }

            return result;
        }
        #endregion
    }
}
