﻿using System.Linq;
using MODELS.Helpers;
using System.Transactions;
using System.Threading.Tasks;
using DATA.Helpers.StaticClasses;
using System.Collections.Generic;
using DATA.Helpers.AuxiliarClasses;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;
using DATA.LayeredArquiteture.Repositories.UnitOfWork;

namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{
    public class StoDefectBLL : IStoDefectBLL
    {
        public USER AuthenticatedUser { get; set; }

        public StoDefectBLL()
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();
        }

        #region Public Methods

        public async Task<Result<IEnumerable<STODEFECT>>> GetAll()
        {
            Result<IEnumerable<STODEFECT>> result = new Result<IEnumerable<STODEFECT>>(true);

            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Snapshot))
            {
                result.Obj = await uow.StoDefectRepository.GetAll();

                uow.Commit();
            }

            if (!result.Obj.IsValid())
            {
                result.NotFound();
            }

            return result;
        }

        public async Task<Result<IEnumerable<STODEFECT>>> GetByStoDefect(STODEFECT stoDefect)
        {
            Result<Dictionary<string, object>> resultFields = ClassHelper.GetDictionaryFieldsFromObject(stoDefect, nameof(STODEFECT.MFGNUM_0), 
                nameof(STODEFECT.ITMREF_0), nameof(STODEFECT.LOT_0), nameof(STODEFECT.SLO_0), nameof(STODEFECT.MACHINE_0), nameof(STODEFECT.OPERATION_0));
            Result<IEnumerable<STODEFECT>> result = Result<IEnumerable<STODEFECT>>.ConvertAndGetFromResult(resultFields);

            if (result.Success)
            {
                Dictionary<string, object> fields = resultFields.Obj;

                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                {
                    result.Obj = await uow.StoDefectRepository.GetByFields(fields);

                    uow.Commit();
                }

                if (!result.Obj.IsValid())
                {
                    result.NotFound(fields);
                }
            }

            return result;
        }

        public async Task<Result<STODEFECT>> GetById(int id)
        {
            Result<STODEFECT> result = id.IsValueValid<STODEFECT>(nameof(STODEFECT.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                {
                    result.Obj = await uow.StoDefectRepository.GetById(id);

                    uow.Commit();
                }

                if (result.Obj == null)
                {
                    result.NotFound(id);
                }
            }

            return result;
        }

        public async Task<Result<STODEFECT>> Insert(STODEFECT stoDefect)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
            {
                Result<STODEFECT> result = await IsValidInsert(stoDefect);

                if (result.Success)
                {
                    stoDefect.CREUSR_0 = AuthenticatedUser.USR_0;
                    stoDefect.UPDUSR_0 = AuthenticatedUser.USR_0;

                    result = await uow.StoDefectRepository.Insert(stoDefect);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<STODEFECT>> Update(STODEFECT stoDefect)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
            {
                Result<STODEFECT> result = await IsValidUpdate(stoDefect);

                if (result.Success)
                {
                    stoDefect = result.Obj;
                    stoDefect.UPDUSR_0 = AuthenticatedUser.USR_0;

                    Result<STODEFECT> resultUpdate = await uow.StoDefectRepository.Update(stoDefect);
                    result.ConvertFromResult(resultUpdate, true);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<STODEFECT>> IsValidInsert(STODEFECT stoDefect)
        {
            Result<STODEFECT> result = await stoDefect.IsModelOnInsertValidAsync();

            //Validar se a OF é válida, obter artigo, verificar se a máquina e operação são válidas e se o perfil tem acesso à maquina
            //Calcular bonusableflg_0 = 2 se cliente ou artigo (nao sei, é no Sage) estiver configurado para tal
            //UM DIA QUANDO HOUVER TABELA EM QUE SE DEFINE OS CODIGOS DOS DEFEITOS, VALIDAR SE O CODIGO É VÁLIDO

            return result;
        }

        public async Task<Result<STODEFECT>> IsValidUpdate(STODEFECT stoDefect)
        {
            Result<STODEFECT> result = await stoDefect.IsModelOnUpdateValidAsync();

            if (result.Success)
            {
                result = await GetById(stoDefect.ROWID);

                if (result.Success)
                {
                    //Verificar se o sublote é válido e o sublote existe na PRODUCTION

                    STODEFECT stoDefectTemp = result.Obj;

                    result = DBHelper.ChangeModelByModifiableFields(stoDefectTemp, stoDefect, nameof(STODEFECT.SLO_0));

                    //Add validations here
                }
            }

            return result;
        }

        #endregion
    }
}
