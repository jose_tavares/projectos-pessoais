﻿using MODELS.Helpers;
using System.Transactions;
using System.Threading.Tasks;
using System.Collections.Generic;
using DATA.Helpers.StaticClasses;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;
using System.Linq;
using DATA.LayeredArquiteture.Repositories.UnitOfWork;

namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{
    public class UserBLL : IUserBLL
    {
        public USER AuthenticatedUser { get; set; }

        public UserBLL()
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();
        }

        #region Public Methods

        public async Task<Result<IEnumerable<USER>>> GetAll()
        {
            Result<IEnumerable<USER>> result = new Result<IEnumerable<USER>>(true);

            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Snapshot))
            {
                result.Obj = await uow.UserRepository.GetAll();

                uow.Commit();
            }

            if (!result.Obj.IsValid())
            {
                result.NotFound();
            }

            return result;
        }

        public async Task<Result<IEnumerable<USER>>> GetByProfile(string PROFILE_0)
        {
            Result<IEnumerable<USER>> result = PROFILE_0.IsValueValid<IEnumerable<USER>>(nameof(USER.PROFILE_0));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                {
                    result.Obj = await uow.UserRepository.GetByField(nameof(USER.PROFILE_0), PROFILE_0);

                    uow.Commit();
                }

                if (!result.Obj.IsValid())
                {
                    result.NotFound(nameof(USER.PROFILE_0), PROFILE_0);
                }
            }

            return result;
        }

        public async Task<Result<USER>> GetById(int id)
        {
            Result<USER> result = id.IsValueValid<USER>(nameof(USER.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
                {
                    result.Obj = await uow.UserRepository.GetById(id);

                    uow.Commit();
                }

                if (result.Obj == null)
                {
                    result.NotFound(id);
                }
            }

            return result;
        }

        public async Task<Result<USER>> GetByUser(string USER_0)
        {
            Result<USER> result = USER_0.IsValueValid<USER>(nameof(USER.USR_0));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
                {
                    result.Obj = await uow.UserRepository.GetByUniqueField(nameof(USER.USR_0), USER_0);

                    uow.Commit();
                }

                if (result.Obj == null)
                {
                    result.NotFound(nameof(USER.USR_0), USER_0);
                }
            }

            return result;
        }

        public async Task<Result<USER>> GetByUsername(string USERNAM_0)
        {
            Result<USER> result = USERNAM_0.IsValueValid<USER>(nameof(USER.USERNAM_0));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
                {
                    result.Obj = await uow.UserRepository.GetByUniqueField(nameof(USER.USERNAM_0), USERNAM_0);

                    uow.Commit();
                }

                if (result.Obj == null)
                {
                    result.NotFound(nameof(USER.USERNAM_0), USERNAM_0);
                }
            }

            return result;
        }

        public async Task<Result<USER>> Insert(USER user)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
            {
                Result<USER> result = await IsValidInsert(user);

                if (result.Success)
                {
                    user.CREUSR_0 = AuthenticatedUser.USR_0;
                    user.UPDUSR_0 = AuthenticatedUser.USR_0;

                    result = await uow.UserRepository.Insert(user);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<USER>> Update(USER user)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
            {
                Result<USER> result = await IsValidUpdate(user);

                if (result.Success)
                {
                    user = result.Obj;
                    user.UPDUSR_0 = AuthenticatedUser.USR_0;

                    Result<USER> resultUpdate = await uow.UserRepository.Update(user);
                    result.ConvertFromResult(resultUpdate, true);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<USER>> Login(string USERNAM_0, string PASSWORD_0)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
            {
                Result<USER> result = await IsValidLogin(USERNAM_0, PASSWORD_0);

                if (result.Success)
                {
                    USER user = result.Obj;
                    result = await uow.UserRepository.Update(user);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<USER>> Logout(int id)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
            {
                Result<USER> result = await IsValidLogout(id);

                if (result.Success)
                {
                    USER user = result.Obj;
                    result = await uow.UserRepository.Update(user);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<USER>> IsValidInsert(USER user)
        {
            Result<USER> result = await user.IsModelOnInsertValidAsync();

            if (result.Success)
            {
                Result<USER> resultApp = await GetByUser(user.USR_0);

                if (resultApp.Obj != null) //Se encontrou o user
                {
                    result.AddInvalidMessage($"Já existe um utilizador com o código '{user.USR_0}'");
                }
                else
                {
                    resultApp = await GetByUsername(user.USERNAM_0);

                    if (resultApp.Obj != null) //Se encontrou o user
                    {
                        result.AddInvalidMessage($"Já existe um utilizador com o username '{user.USERNAM_0}'");
                    }
                }

                //Add validations here
            }

            return result;
        }

        public async Task<Result<USER>> IsValidUpdate(USER user)
        {
            Result<USER> result = await user.IsModelOnUpdateValidAsync();

            if (result.Success)
            {
                result = await GetById(user.ROWID);

                if (result.Success)
                {
                    USER userTemp = result.Obj;

                    result = DBHelper.ChangeModelByUnModifiableFields(userTemp, user, nameof(USER.USR_0), nameof(USER.USERNAM_0));

                    //Add validations here
                }
            }

            return result;
        }

        private async Task<Result<USER>> IsValidLogin(string USERNAM_0, string PASSWORD_0)
        {
            Dictionary<string, object> fields = new Dictionary<string, object>
            {
                { nameof(USER.USERNAM_0), USERNAM_0 },
                { nameof(USER.PASSWORD_0), PASSWORD_0 }
            };

            Result<USER> result = ClassHelper.IsPropertiesValid<USER>(fields);

            if (result.Success)
            {
                Result<USER> resultUser = await GetByUsername(USERNAM_0);
                USER user = resultUser.Obj;

                if (user == null)
                {
                    result.AddInvalidMessage($"Não foi possível encontrar um utilizador com o username {USERNAM_0}.");

                    return result;
                }

                if (user.PASSWORD_0 != PASSWORD_0)
                {
                    result.AddInvalidMessage("A password encontra-se incorreta.");

                    return result;
                }

                if (user.ENABFLG_0 != 2)
                {
                    result.AddInvalidMessage("O utilizador não se encontra ativo.");

                    return result;
                }

                user.LOGGEDFLG_0 = 2;

                result.Obj = user;
            }

            return result;
        }

        private async Task<Result<USER>> IsValidLogout(int id)
        {
            Result<USER> result = id.IsValueValid<USER>(nameof(USER.ROWID));

            if (result.Success)
            {
                result = await GetById(id);

                if (result.Success)
                {
                    USER user = result.Obj;

                    user.LOGGEDFLG_0 = 1;
                    result.Obj = user;
                }
            }

            return result;
        }
        #endregion
    }
}
