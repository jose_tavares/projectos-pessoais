﻿namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{

    using MODELS.Helpers;
    using System.Transactions;
    using System.Threading.Tasks;
    using DATA.Helpers.StaticClasses;
    using System.Collections.Generic;
    using MODELS.BusinessObject.ERTSOFT.Database;
    using DATA.LayeredArquiteture.Repositories.UnitOfWork;
    using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;

    public class AppBLL : IAppBLL
    {
        #region Public Properties
        public USER AuthenticatedUser { get; set; }
        #endregion

        #region Public Methods

        public AppBLL()
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();
        }

        public async Task<IEnumerable<APP>> GetAll()
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Snapshot))
            {
                IEnumerable<APP> appList = await uow.AppRepository.GetAll();

                uow.Commit();

                return appList;
            }
        }

        public async Task<APP> GetByID(decimal id)
        {
            Result<APP> result = id.IsValueValid<APP>(nameof(APP.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                {
                    APP app = await uow.AppRepository.GetByID(id);

                    uow.Commit();

                    return app;
                }
            }

            return null;
        }

        public async Task<APP> GetByUniqueField(string APP_0)
        {
            Result<APP> result = APP_0.IsValueValid<APP>(nameof(APP.APP_0));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                {
                    APP app = await uow.AppRepository.GetByUniqueField(nameof(APP.APP_0), APP_0);

                    uow.Commit();

                    return app;
                }
            }

            return null;
        }

        public async Task<APP> GetByUniqueFieldWithModuleList(string APP_0)
        {
            Result<APP> result = APP_0.IsValueValid<APP>(nameof(APP.APP_0));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                {
                    APP app = await uow.AppRepository.GetByUniqueFieldWithModuleList(APP_0);

                    uow.Commit();

                    return app;
                }
            }

            return null;
        }

        public async Task<Result<APP>> Insert(APP app)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
            {
                Result<APP> result = await IsValidInsert(app);

                if (result.Success)
                {
                    app.CREUSR_0 = AuthenticatedUser.USR_0;
                    app.UPDUSR_0 = AuthenticatedUser.USR_0;

                    result = await uow.AppRepository.Insert(app);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<APP>> Update(APP app)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
            {
                Result<APP> result = await IsValidUpdate(app);

                if (result.Success)
                {
                    app.UPDUSR_0 = AuthenticatedUser.USR_0;

                    result = await uow.AppRepository.Update(app);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<APP>> IsValidInsert(APP app)
        {
            // Add more validations if needed
            Result<APP> result = await app.IsClassValidAsync();

            if (result.Success)
            {
                APP appTemp = await GetByUniqueField(app.APP_0);

                if (appTemp != null)
                {
                    result.AddInvalidMessage($"Já existe uma aplicação com o nome {app.APP_0}");
                }
            }

            return result;
        }

        public async Task<Result<APP>> IsValidUpdate(APP app)
        {
            // Add more validations if needed
            Result<APP> result = await app.IsClassValidAsync();

            if (result.Success)
            {
                APP appTemp = await GetByID(app.ROWID);

                if (appTemp == null)
                {
                    result.AddInvalidMessage($"Não foi possível encontrar o perfil com o ID {app.ROWID}.");
                }
                else if (app.APP_0 != appTemp.APP_0)
                {
                    result.AddInvalidMessage($"Não é permitido alterar o campo {nameof(app.APP_0)}");
                }
            }

            return result;
        }
        #endregion
    }
}
