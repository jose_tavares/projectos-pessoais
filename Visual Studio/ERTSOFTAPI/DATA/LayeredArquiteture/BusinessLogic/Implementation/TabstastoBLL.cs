﻿using MODELS.Helpers;
using System.Transactions;
using System.Threading.Tasks;
using DATA.Helpers.StaticClasses;
using System.Collections.Generic;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.Repositories.UnitOfWork;
using MODELS.BusinessObject.SAGEX3.Database;

namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{
    public class TabstastoBLL : ITabstastoBLL
    {
        public USER AuthenticatedUser { get; set; }

        public TabstastoBLL()
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();
        }

        public async Task<IEnumerable<TABSTASTO>> GetAll()
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
            {
                IEnumerable<TABSTASTO> tabstastoList = await uow.TabstastoRepository.GetAll();

                uow.Commit();

                return tabstastoList;
            }
        }

        public async Task<TABSTASTO> GetByID(decimal id)
        {
            Result<TABSTASTO> result = id.IsValueValid<TABSTASTO>(nameof(TABSTASTO.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
                {
                    TABSTASTO tabstasto = await uow.TabstastoRepository.GetByID(id);

                    uow.Commit();

                    return tabstasto;
                }
            }

            return null;
        }

        public async Task<TABSTASTO> GetByUniqueField(string STA)
        {
            Result<TABSTASTO> result = STA.IsValueValid<TABSTASTO>(nameof(TABSTASTO.STASTO_0));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
                {
                    TABSTASTO tabstasto = await uow.TabstastoRepository.GetByUniqueField(nameof(TABSTASTO.STASTO_0), STA);

                    uow.Commit();

                    return tabstasto;
                }
            }

            return null;
        }
    }
}
