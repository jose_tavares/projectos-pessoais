﻿using System.Linq;
using MODELS.Helpers;
using System.Transactions;
using System.Threading.Tasks;
using DATA.Helpers.StaticClasses;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;
using DATA.LayeredArquiteture.Repositories.UnitOfWork;

namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{
    public class WSParamBLL : IWSParamBLL
    {
        public USER AuthenticatedUser { get; set; }

        public WSParamBLL()
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();
        }

        public async Task<IEnumerable<WSPARAM>> GetAll()
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Snapshot))
            {
                IEnumerable<WSPARAM> wsParamList = await uow.WSParamRepository.GetAll();

                uow.Commit();

                return wsParamList;
            }
        }

        public async Task<IEnumerable<WSPARAM>> GetByFields(WSPARAM wsParam)
        {
            Dictionary<string, object> fields = new Dictionary<string, object>();
            Dictionary<string, object> fieldsTemp = new Dictionary<string, object>
            {
                { nameof(WSPARAM.WSCODE_0), wsParam.WSCODE_0},
                { nameof(WSPARAM.FLD_0), wsParam.FLD_0 },
                { nameof(WSPARAM.VALUE_0), wsParam.VALUE_0 },
                { nameof(WSPARAM.TEXT_0), wsParam.TEXT_0 }
            };

            foreach (KeyValuePair<string, object> field in fieldsTemp)
            {
                if (field.Value != null)
                {
                    fields.Add(field.Key, field.Value);
                }
            }

            if (fields.Count > 0)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted, TransactionScopeOption.Required))
                {
                    IEnumerable<WSPARAM> wsParamList = await uow.WSParamRepository.GetByFields(fields);

                    uow.Commit();

                    return wsParamList;
                }
            }

            return Enumerable.Empty<WSPARAM>();
        }

        public async Task<IEnumerable<WSPARAM>> GetByWsCode(string WEBSERVICE_0)
        {
            Result<WSPARAM> result = WEBSERVICE_0.IsValueValid<WSPARAM>(nameof(WSPARAM.WSCODE_0));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                {
                    IEnumerable<WSPARAM> wsParamList = await uow.WSParamRepository.GetByField(nameof(WSPARAM.WSCODE_0), WEBSERVICE_0);

                    uow.Commit();

                    return wsParamList;
                }
            }

            return Enumerable.Empty<WSPARAM>();
        }

        public async Task<WSPARAM> GetByID(decimal id)
        {
            Result<WSPARAM> result = id.IsValueValid<WSPARAM>(nameof(WSPARAM.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                {
                    WSPARAM wsParam = await uow.WSParamRepository.GetByID(id);

                    uow.Commit();

                    return wsParam;
                }
            }

            return null;
        }

        public async Task<Result<WSPARAM>> Insert(WSPARAM wsParam)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
            {
                Result<WSPARAM> result = await IsValidInsert(wsParam);

                if (result.Success)
                {
                    wsParam.CREUSR_0 = AuthenticatedUser.USR_0;
                    wsParam.UPDUSR_0 = AuthenticatedUser.USR_0;

                    result = await uow.WSParamRepository.Insert(wsParam);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<WSPARAM>> Update(WSPARAM wsParam)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
            {
                Result<WSPARAM> result = await IsValidUpdate(wsParam);

                if (result.Success)
                {
                    wsParam.UPDUSR_0 = AuthenticatedUser.USR_0;

                    result = await uow.WSParamRepository.Update(wsParam);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<WSPARAM>> IsValidInsert(WSPARAM wsParam)
        {
            // Add more validations if needed
            Result<WSPARAM> result = await wsParam.IsClassValidAsync();

            return result;
        }

        public async Task<Result<WSPARAM>> IsValidUpdate(WSPARAM wsParam)
        {
            // Add more validations if needed
            Result<WSPARAM> result = await wsParam.IsClassValidAsync();

            return result;
        }
    }
}
