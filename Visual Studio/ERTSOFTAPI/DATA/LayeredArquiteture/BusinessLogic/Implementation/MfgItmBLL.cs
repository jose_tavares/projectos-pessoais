﻿using MODELS.Helpers;
using System.Transactions;
using System.Threading.Tasks;
using System.Collections.Generic;
using DATA.Helpers.StaticClasses;
using MODELS.BusinessObject.ERTSOFT.Database;
using MODELS.BusinessObject.SAGEX3.Database;
using DATA.LayeredArquiteture.Repositories.UnitOfWork;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;

namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{
    public class MfgItmBLL : IMfgItmBLL
    {
        public USER AuthenticatedUser { get; set; }

        public MfgItmBLL()
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();
        }

        public async Task<IEnumerable<MFGITM>> GetAll()
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
            {
                IEnumerable<MFGITM> mfgItmList = await uow.MfgItmRepository.GetAll();

                uow.Commit();

                return mfgItmList;
            }
        }

        public async Task<MFGITM> GetByMfgnum(string MFGNUM_0)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
            {
                MFGITM mfgItm = await uow.MfgItmRepository.GetByUniqueField(nameof(MFGITM.MFGNUM_0), MFGNUM_0);

                uow.Commit();

                return mfgItm;
            }
        }

        public async Task<MFGITM> GetByID(decimal id)
        {
            Result<MFGITM> result = id.IsValueValid<MFGITM>(nameof(MFGITM.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
                {
                    MFGITM mfgItm = await uow.MfgItmRepository.GetByID(id);

                    uow.Commit();

                    return mfgItm;
                }
            }

            return null;
        }
    }
}
