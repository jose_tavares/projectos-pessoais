﻿using MODELS.Helpers;
using System.Transactions;
using System.Threading.Tasks;
using DATA.Helpers.StaticClasses;
using System.Collections.Generic;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.Repositories.UnitOfWork;
using MODELS.BusinessObject.SAGEX3.Database;

namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{
    public class MfgItmTrkBLL : IMfgItmTrkBLL
    {
        public USER AuthenticatedUser { get; set; }

        public MfgItmTrkBLL()
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();
        }

        public async Task<IEnumerable<MFGITMTRK>> GetAll()
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
            {
                IEnumerable<MFGITMTRK> mfgItmTrkList = await uow.MfgItmTrkRepository.GetAll();

                uow.Commit();

                return mfgItmTrkList;
            }
        }

        public async Task<IEnumerable<MFGITMTRK>> GetByZESSTROWID(string ZESSTROWID)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
            {
                IEnumerable<MFGITMTRK> mfgItmTrkList = await uow.MfgItmTrkRepository.GetByField(nameof(MFGITMTRK.ZESSTROWID_0), ZESSTROWID);

                uow.Commit();

                return mfgItmTrkList;
            }
        }

        public async Task<MFGITMTRK> GetByID(decimal id)
        {
            Result<MFGITMTRK> result = id.IsValueValid<MFGITMTRK>(nameof(MFGITMTRK.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
                {
                    MFGITMTRK mfgItmTrk = await uow.MfgItmTrkRepository.GetByID(id);

                    uow.Commit();

                    return mfgItmTrk;
                }
            }

            return null;
        }
    }
}
