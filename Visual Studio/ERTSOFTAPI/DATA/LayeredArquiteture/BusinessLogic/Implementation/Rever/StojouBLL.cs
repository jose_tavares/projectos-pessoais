﻿namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{
    using System;
    using System.Linq;
    using MODELS.Helpers;
    using Newtonsoft.Json;
    using System.Transactions;
    using System.Threading.Tasks;
    using DATA.Helpers.StaticClasses;
    using System.Collections.Generic;
    using DATA.Helpers.AuxiliarClasses;
    using MODELS.BusinessObject.SAGEX3.Database;
    using MODELS.BusinessObject.ERTSOFT.Database;
    using DATA.LayeredArquiteture.Repositories.UnitOfWork;
    using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;

    public class StojouBLL : IStojouBLL
    {
        #region Private Variables
        private readonly IStockBLL stockBLL;
        private readonly IDocAllBLL docAllBLL;
        private readonly IMfgItmBLL mfgItmBLL;
        private readonly IMfgMatBLL mfgMatBLL;
        private readonly IStolocBLL stolocBLL;
        private readonly IAtabdivBLL atabdivBLL;
        private readonly IAtextraBLL atextraBLL;
        private readonly IMfgHeadBLL mfgHeadBLL;
        private readonly IWsFieldConfigBLL wsFieldConfigBLL;
        private readonly ITabUnitBLL tabUnitBLL;
        private readonly IFacilityBLL facilityBLL;
        private readonly IItmMasterBLL itmMasterBLL;
        private readonly IMfgItmTrkBLL mfgItmTrkBLL;
        private readonly IMfgMatTrkBLL mfgMatTrkBLL;
        private readonly IStojouLogBLL stojouLogBLL;
        private readonly IStoDefectBLL stoDefectBLL;
        private readonly ITabstastoBLL tabstastoBLL;
        private readonly IItmFacilitBLL itmFacilitBLL;
        private readonly IWebServiceBLL webServiceBLL;
        private readonly IConfigValueBLL configValueBLL;
        #endregion

        #region Public Properties
        public USER AuthenticatedUser { get; set; }
        #endregion

        #region Public Constructors
        public StojouBLL(IStockBLL stockBLL, IDocAllBLL docAllBLL, IMfgItmBLL mfgItmBLL, IMfgMatBLL mfgMatBLL, IStolocBLL stolocBLL, IAtabdivBLL atabdivBLL, 
            IAtextraBLL atextraBLL, IMfgHeadBLL mfgHeadBLL, ITabUnitBLL tabUnitBLL, IWsFieldConfigBLL wsFieldConfigBLL, IFacilityBLL facilityBLL, IItmMasterBLL itmMasterBLL, 
            IMfgMatTrkBLL mfgMatTrkBLL, IMfgItmTrkBLL mfgItmTrkBLL, IStojouLogBLL stojouLogBLL, IStoDefectBLL stoDefectBLL, ITabstastoBLL tabstastoBLL, 
            IItmFacilitBLL itmFacilitBLL, IWebServiceBLL webServiceBLL, IConfigValueBLL configValueBLL)
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();

            this.stockBLL = stockBLL;
            this.docAllBLL = docAllBLL;
            this.mfgItmBLL = mfgItmBLL;
            this.mfgMatBLL = mfgMatBLL;
            this.stolocBLL = stolocBLL;
            this.atabdivBLL = atabdivBLL;
            this.atextraBLL = atextraBLL;
            this.mfgHeadBLL = mfgHeadBLL;
            this.tabUnitBLL = tabUnitBLL;
            this.wsFieldConfigBLL = wsFieldConfigBLL;
            this.facilityBLL = facilityBLL;
            this.itmMasterBLL = itmMasterBLL;
            this.mfgItmTrkBLL = mfgItmTrkBLL;
            this.mfgMatTrkBLL = mfgMatTrkBLL;
            this.stojouLogBLL = stojouLogBLL;
            this.stoDefectBLL = stoDefectBLL;
            this.tabstastoBLL = tabstastoBLL;
            this.itmFacilitBLL = itmFacilitBLL;
            this.webServiceBLL = webServiceBLL;
            this.configValueBLL = configValueBLL;
        }
        #endregion

        #region Public Methods
        public async Task<Result<IEnumerable<STOJOU>>> GetAll()
        {
            Result<IEnumerable<STOJOU>> result = new Result<IEnumerable<STOJOU>>(true);

            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Snapshot))
            {
                result.Obj = await uow.StojouRepository.GetAll();

                uow.Commit();  
            }

            if (!result.Obj.IsValid())
            {
                result.NotFound();
            }

            return result;
        }

        public async Task<IEnumerable<STOJOU>> GetByWSCode(string WSCODE_0)
        {
            Result<STOJOU> result = WSCODE_0.IsValueValid<STOJOU>(nameof(STOJOU.WSCODE_0));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                {
                    IEnumerable<STOJOU> stojouList = await uow.StojouRepository.GetByField(nameof(STOJOU.WSCODE_0), WSCODE_0);

                    uow.Commit();

                    return stojouList;
                }
            }

            return Enumerable.Empty<STOJOU>();
        }

        public async Task<IEnumerable<STOJOU>> GetPending()
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
            {
                IEnumerable<STOJOU> stojouList = await uow.StojouRepository.GetPending();

                uow.Commit();

                return stojouList;
            }
        }

        public async Task<IEnumerable<STOJOU>> GetByFields(STOJOU stojou)
        {
            Dictionary<string, object> fields = new Dictionary<string, object>();
            Dictionary<string, object> fieldsTemp = new Dictionary<string, object>
            {
                { nameof(STOJOU.WSCODE_0), stojou.WSCODE_0 },
                { nameof(STOJOU.STOFCY_0), stojou.STOFCY_0 },
                { nameof(STOJOU.ITMREF_0), stojou.ITMREF_0 },
                { nameof(STOJOU.LOT_0), stojou.LOT_0 },
                { nameof(STOJOU.SLO_0), stojou.SLO_0 },
                { nameof(STOJOU.USELOC_0), stojou.USELOC_0 },
                { nameof(STOJOU.USESTA_0), stojou.USESTA_0 },
                { nameof(STOJOU.SUCCESSFLG_0), stojou.SUCCESSFLG_0 },
                { nameof(STOJOU.REGFLG_0), stojou.REGFLG_0 },
                { nameof(STOJOU.CREUSR_0), stojou.CREUSR_0 }
            };

            foreach (KeyValuePair<string, object> field in fieldsTemp)
            {
                if (field.Value != null)
                {
                    fields.Add(field.Key, field.Value);
                }
            }

            if (fields.Count > 0)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
                {
                    IEnumerable<STOJOU> stojouList = await uow.StojouRepository.GetByFields(fields);

                    uow.Commit();

                    return stojouList;
                }
            }

            return Enumerable.Empty<STOJOU>();
        }

        public async Task<Result<STOJOU>> GetById(int id)
        {
            Result<STOJOU> result = id.IsValueValid<STOJOU>(nameof(STOJOU.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
                {
                    STOJOU stojou = await uow.StojouRepository.GetById(id);

                    uow.Commit();
                }

                if (result.Obj == null)
                {
                    result.NotFound(id);
                }
            }

            return result;
        }

        public async Task<STOJOU> GetFirstPendingByWebService(string WEBSERVICE_0)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable, TransactionScopeOption.Suppress))
            {
                STOJOU stojou = await uow.StojouRepository.GetFirstPendingByWebService(WEBSERVICE_0);

                uow.Commit();

                return stojou;
            }
        }

        public async Task<STOJOU> GetByIdWithWebService(int id)
        {
            Result<STOJOU> result = id.IsValueValid<STOJOU>(nameof(STOJOU.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
                {
                    STOJOU stojou = await uow.StojouRepository.GetByIdWithWebService(id);

                    uow.Commit();

                    return stojou;
                }
            }

            return null;
        }

        public async Task<STOJOU> GetByIdWithStojouLogList(int id)
        {
            Result<STOJOU> result = id.IsValueValid<STOJOU>(nameof(STOJOU.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
                {
                    STOJOU stojou = await uow.StojouRepository.GetByIdWithStojouLogList(id);

                    uow.Commit();

                    return stojou;
                }
            }

            return null;
        }

        public async Task<string> GetNextProductionSlo(string MFGNUM_0)
        {
            Result<STOJOU> result = MFGNUM_0.IsValueValid<STOJOU>(nameof(STOJOU.MFGNUM_0));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
                {
                    string maxSlo = await uow.StojouRepository.GetNextProductionSlo(MFGNUM_0);

                    uow.Commit();

                    return maxSlo;
                }
            }

            return null;
        }

        public async Task<Result<STOJOU>> ImportFirstPendingByWebService(string WEBSERVICE_0)
        {
            Result<STOJOU> result = await IsValidImport(WEBSERVICE_0);

            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable, TransactionScopeOption.RequiresNew, TimeSpan.FromMinutes(5)))
            {
                if (result.Success)
                {
                    STOJOU stojou = result.Obj;

                    if (stojou.SUCCESSFLG_0 == 1)
                    {
                        string XML = WebServiceHelper.GetXML(stojou, stojou.WebServiceModel.WsFieldConfigList);

                        Result<WebServiceData> webServiceDataResult = await WebServiceHelper.RunWebServiceAsync(stojou.WebServiceModel, XML);
                        result.ConvertFromResult(webServiceDataResult);
                        result.AddMessage($"Linha {stojou.ROWID + (result.Success ? string.Empty : " não")} foi processada com sucesso.");

                        Result<STOJOU> resultStojou = await ProcessWebServiceData(uow, webServiceDataResult, stojou);
                        result.Success = resultStojou.Success;

                        if (!result.Success)
                        {
                            result.Messages.AddRange(resultStojou.Messages);
                        }

                        result.Obj = stojou;
                    }
                    else
                    {
                        await UpdateImportStojou(uow, stojou);
                    }

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<STOJOU>> Insert(STOJOU stojou)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
            {
                Result<STOJOU> result = await IsValidInsert(stojou);

                if (result.Success)
                {
                    stojou.CREUSR_0 = AuthenticatedUser.USR_0;
                    stojou.UPDUSR_0 = AuthenticatedUser.USR_0;

                    result = await uow.StojouRepository.Insert(stojou);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<STOJOU>> Update(STOJOU stojou)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
            {
                Result<STOJOU> result = await IsValidUpdate(stojou);

                if (result.Success)
                {
                    stojou.UPDUSR_0 = AuthenticatedUser.USR_0;

                    result = await uow.StojouRepository.Update(stojou);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<STOJOU>> InsertProduction(ProductionData productionData)
        {
            Result<StojouData> resultStojouData = await IsValidInsertProduction(productionData);
            Result<STOJOU> resultStojou = Result<STOJOU>.ConvertAndGetFromResult(resultStojouData);

            if (!resultStojouData.Success)
            {
                return resultStojou;
            }

            StojouData data = resultStojouData.Obj;

            using (IUnitOfWork uowItmLock = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable)) //Começo uma transação SQL em serializable, nota que aqui não pretendo fazer commit, apenas bloquear na ITMLOCK os Artigos a trabalhar
            {
                await LockMfgMatList(uowItmLock, data.MfgMatOperationList);

                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable, TransactionScopeOption.RequiresNew)) //Começo uma transação SQL em serializable, aqui irei dar commit se tudo correr bem.
                {
                    List<STOJOU> stojouInsertList = new List<STOJOU>(); //Crio lista em memória que irá conter toda a info necessária para registar as linhas na STOJOU
                    Result<IEnumerable<STOJOU>> resultStojouList = await GetConsumptionStojou(data);
                    resultStojou.ConvertFromResult(resultStojouList);

                    if (!resultStojouList.Success)
                    {
                        return resultStojou;
                    }

                    stojouInsertList.AddRange(resultStojouList.Obj);

                    resultStojou = await GetProductionStojou(stojouInsertList, data);

                    if (!resultStojou.Success)
                    {
                        return resultStojou;
                    }

                    stojouInsertList.Add(resultStojou.Obj);

                    resultStojou = await InsertProductionList(uow, stojouInsertList);

                    if (!resultStojou.Success)
                    {
                        return resultStojou;
                    }

                    uow.Commit();
                }
            }

            return resultStojou;
        }

        public async Task<Result<STOJOU>> IsValidInsert(STOJOU stojou)
        {
            // Add more validations if needed
            Result<STOJOU> result = await stojou.IsModelOnInsertValidAsync();

            return result;
        }

        public async Task<Result<STOJOU>> IsValidUpdate(STOJOU stojou)
        {
            Result<STOJOU> result = await stojou.IsModelOnUpdateValidAsync();

            return result;
        }

        private async Task<Result<STOJOU>> IsValidImport(string WEBSERVICE_0)
        {
            Result<WEBSERVICE> resultWebService = await IsImportWSValid(WEBSERVICE_0);
            Result<STOJOU> resultStojou = Result<STOJOU>.ConvertAndGetFromResult(resultWebService);

            if (resultStojou.Success)
            {
                STOJOU stojou = await GetFirstPendingByWebService(WEBSERVICE_0);

                if (stojou != null)
                {
                    //resultStojou.Obj = await GetByID(stojou.ROWID);
                    resultStojou.Obj = stojou;

                    Random rnd = new Random();
                    int randomNumber = rnd.Next(3, 60);

                    System.Threading.Thread.Sleep(randomNumber);

                    if (resultStojou.Obj != null)
                    {
                        if (resultStojou.Obj.SUCCESSFLG_0 == 2)
                        {
                            resultStojou.AddInvalidMessage($"Stojou já se encontra processada. Possivelmente existem pedidos em paralelo a correr e um anteriormente já integrou a STOJOU com o ID {stojou.ROWID}");
                        }
                        else
                        {
                            resultStojou.Obj.WebServiceModel = resultWebService.Obj;
                            Result<IEnumerable<WSFIELDCONFIG>> wsFieldConfigListResult = await wsFieldConfigBLL.GetByWsCode(WEBSERVICE_0);
                            resultStojou.Obj.WebServiceModel.WsFieldConfigList = wsFieldConfigListResult.Obj;

                            if (resultStojou.Obj.WebServiceModel != null && resultStojou.Obj.WebServiceModel.WsFieldConfigList.IsValid())
                            {
                                resultStojou.Obj.WSUSR_0 = resultStojou.Obj.WebServiceModel.USER_0;
                            }
                            else
                            {
                                resultStojou.AddInvalidMessage($"Não foi possível encontrar as configurações associadas ao webservice {WEBSERVICE_0}.");
                            }
                        }
                    }
                    else
                    {
                        resultStojou.AddInvalidMessage($"Não foi possível obter informação da STOJOU para o ID {stojou.ROWID}");
                    }
                }
                else
                {
                    resultStojou.AddInvalidMessage($"Não existe nenhum registo pendente de importação para o webservice {WEBSERVICE_0}");
                }

            }

            return resultStojou;
        }

        public async Task<Result<StojouData>> IsValidInsertProduction(ProductionData productionData)
        {
            Result<StojouData> result = new Result<StojouData>(true)
            {
                Obj = new StojouData()
                {
                    ProductionData = productionData
                }
            };

            result = await IsWsConsumptionValid(result);

            if (!result.Success)
            {
                return result;
            }

            result = await IsWsProductionValid(result);

            if (!result.Success)
            {
                return result;
            }

            result = await IsMfgHeadValid(result, productionData.MFGNUM_0);

            if (!result.Success)
            {
                return result;
            }

            result = await IsMfgItmValid(result, productionData.MFGNUM_0);

            if (!result.Success)
            {
                return result;
            }

            result = await IsMfgItmOperationValid(result, productionData.MFGNUM_0, result.Obj.MfgItm?.ZOPERRPA_0);

            if (!result.Success)
            {
                return result;
            }

            result = await IsMfgMatValid(result, productionData.MFGNUM_0);

            if (!result.Success)
            {
                return result;
            }

            result = await IsItmFacilityValid(result, result.Obj.MfgHead?.MFGFCY_0, result.Obj.MfgItm?.ITMREF_0);

            if (!result.Success)
            {
                return result;
            }

            result = await IsOperationValid(result, productionData.OPERATION_0);

            if (!result.Success)
            {
                return result;
            }

            result = await IsMachineValid(result, productionData.MACHINE_0);

            if (!result.Success)
            {
                return result;
            }

            result = await IsProfileMachineValid(result, result.Obj.MfgHead?.MFGFCY_0, productionData.OPERATION_0, productionData.MACHINE_0, result.Obj.MacNam?.TEXTE_0);

            if (!result.Success)
            {
                return result;
            }

            result = await IsConsumptionInfoValid(result, result.Obj.MfgHead?.ZMFGTYP_0, result.Obj.MfgHead?.MFGFCY_0, productionData.MACHINE_0);

            if (!result.Success)
            {
                return result;
            }

            result = await IsProductionInfoValid(result, result.Obj.MfgHead?.ZMFGTYP_0, result.Obj.MfgHead?.MFGFCY_0, productionData.OPERATION_0);

            if (!result.Success)
            {
                return result;
            }

            result = await IsFacilityValid(result, result.Obj.MfgHead?.MFGFCY_0);

            if (!result.Success)
            {
                return result;
            }

            result = await IsStolocValid(result, result.Obj.MfgHead?.MFGFCY_0, result.Obj.ConsumptionInfo?.A4_0);

            if (!result.Success)
            {
                return result;
            }

            result = await IsTabStaStoValid(result, result.Obj.ConsumptionInfo?.A5_0);

            if (!result.Success)
            {
                return result;
            }

            result = await IsTabUnitValid(result, result.Obj.MfgItm?.STU_0);

            if (!result.Success)
            {
                return result;
            }

            result = await IsItmMasterValid(result, result.Obj.MfgItm?.ITMREF_0, result.Obj.MfgItm?.STU_0);

            if (!result.Success)
            {
                return result;
            }

            result = IsQtyValid(result, result.Obj.MfgItm, result.Obj.TabUnit, productionData.PRODQTY_0);

            if (!result.Success)
            {
                return result;
            }

            result.Obj.MfgMatOperationList = GetMfgMatOperationList(result.Obj.MfgMatList, productionData.OPERATION_0, productionData.PRODOK_0);

            return result;
        }
        #endregion

        #region Private Methods
        //if (operationInfo.A2_0 == "2")  //Verifico se a operação atual está configurada para controlar qtd produzida em relação aos consumos anteriores
        //{
        //    decimal necessity = 0;

        //    foreach (MFGMAT mfgMatItem in mfgMatList)
        //    {
        //        if (mfgMatItem.ZOPERCON_0 != OPERATION_0) //Apenas irei controlar os consumos sobre as operações que não correspondem
        //        {
        //            if (mfgMatItem.ZMODOCONS_0 == "100") //Modo consumo: Proporcional OF
        //            {
        //                necessity = mfgMatItem.BOMSTUCOE_0 * mfgMatItem.BOMQTY_0 * PRODQTY_0; //Cálculo da BOM sobre a qtd produzida
        //            }
        //            else if (mfgMatItem.ZMODOCONS_0 == "101" || mfgMatItem.ZMODOCONS_0 == "102") //Modo consumo: Por Sublote / Lote
        //            {
        //                necessity = mfgMatItem.ZQTYMC_0; //Quantidade correspondente ao modo consumo
        //            }
        //            else //Modo consumo não previsto
        //            {
        //                result.AddInvalidMessage($"Não está previsto o tipo de consumo com o código {mfgMatItem.ZMODOCONS_0}. O mesmo encontra-se associado ao Artigo: {mfgMatItem.ITMREF_0}");
        //                break;
        //            }


        //            //Tenho que obter total qtd produzida na STOJOU e total qtd consumida por Artigo na STOJOU
        //            //Comparo qtd teorica consumo para o total produzido + qtd produção atual com qtd consumida das outras operações
        //        }
        //    }
        //}

        #region Production & Consumption
        private async Task<Result<StojouData>> IsWsConsumptionValid(Result<StojouData> result) //Verifico se o webservice de consumos existe na tabela WEBSERVICE
        {
            Result<WEBSERVICE> resultWebservice = await webServiceBLL.GetByUniqueField("CONSUMPTION");
            WEBSERVICE wsConsumption = resultWebservice.Obj;

            if (wsConsumption == null)
            {
                result.AddInvalidMessage($"Não foi possível encontrar a configuração do webservice de consumos MP.");
                result.AddInvalidMessage($"Tabela: {nameof(WEBSERVICE)} / {nameof(WEBSERVICE.CODE_0)}: CONSUMPTION");

                return result;
            }

            result.Obj.WsConsumption = wsConsumption;
            return result;
        }

        private async Task<Result<StojouData>> IsWsProductionValid(Result<StojouData> result) //Verifico se o webservice de reporte produção existe na tabela WEBSERVICE
        {
            Result<WEBSERVICE> resultWebservice = await webServiceBLL.GetByUniqueField("PRODUCTION");
            WEBSERVICE wsProduction = resultWebservice.Obj;

            if (wsProduction == null)
            {
                result.AddInvalidMessage($"Não foi possível encontrar a configuração do webservice de reporte PA.");
                result.AddInvalidMessage($"Tabela: {nameof(WEBSERVICE)} / {nameof(WEBSERVICE.CODE_0)}: PRODUCTION");

                return result;
            }

            result.Obj.WsProduction = wsProduction;
            return result;
        }

        private async Task<Result<StojouData>> IsMfgHeadValid(Result<StojouData> result, string MFGNUM_0) //Verifico se a OF enviada por parametro existe na tabela MFGHEAD
        {
            Result<MFGHEAD> resultMfghead = await mfgHeadBLL.GetByUniqueField(MFGNUM_0);
            MFGHEAD mfgHead = resultMfghead.Obj;

            if (mfgHead == null)
            {
                result.AddInvalidMessage($"Não foi possível encontrar o cabeçalho da OF '{MFGNUM_0}'.");
                result.AddInvalidMessage($"Tabela: {nameof(MFGHEAD)} / {nameof(MFGHEAD.MFGNUM_0)}: {MFGNUM_0}");

                return result;
            }

            if (mfgHead.MFGSTA_0 == 4)
            {
                result.AddInvalidMessage($"Tabela: {nameof(MFGHEAD)} / {nameof(MFGHEAD.MFGSTA_0)}: {mfgHead.MFGSTA_0}");
                result.AddInvalidMessage($"A ordem de produção '{MFGNUM_0}' encontra-se fechada.");

                return result;
            }

            result.Obj.MfgHead = mfgHead;
            return result;
        }

        private async Task<Result<StojouData>> IsMfgItmValid(Result<StojouData> result, string MFGNUM_0) //Verifico se o Artigo associado à OF existe na MFGITM
        {
            Result<MFGITM> resultMfgitm = await mfgItmBLL.GetByUniqueField(MFGNUM_0);
            MFGITM mfgItm = resultMfgitm.Obj;

            if (mfgItm == null)
            {
                result.AddInvalidMessage($"Não foi possível encontrar info do artigo a produzir para a OF '{MFGNUM_0}'");
                result.AddInvalidMessage($"Tabela: {nameof(MFGITM)} / {nameof(MFGITM.MFGNUM_0)}: {MFGNUM_0}");

                return result;
            }

            result.Obj.MfgItm = mfgItm;
            return result;
        }

        private async Task<Result<StojouData>> IsMfgItmOperationValid(Result<StojouData> result, string MFGNUM_0, string OPERATION_0) //Verifico se a operação onde é feito o reporte de produção existe na tabela ATABDIV com o NUMTAB 6700
        {
            Result<ATABDIV> resultAtabdiv = await atabdivBLL.GetByUniqueFields(6700, OPERATION_0);
            ATABDIV atabdiv = resultAtabdiv.Obj;

            if (atabdiv == null)
            {
                result.AddInvalidMessage($"A operação '{OPERATION_0}' associada ao Artigo final da OF '{MFGNUM_0}' não se encontra configurada no sistema.");
                result.AddInvalidMessage($"Tabela: {nameof(ATABDIV)} / {nameof(ATABDIV.NUMTAB_0)}: 6700 / {nameof(ATABDIV.CODE_0)}: {OPERATION_0}");

                return result;
            }

            result.Obj.MfgItm.OperationModel = atabdiv;
            return result;
        }

        private async Task<Result<StojouData>> IsMfgMatValid(Result<StojouData> result, string MFGNUM_0) //Verifico se existem MP's associadas à OF na tabela MFGMAT
        {
            Result<IEnumerable<MFGMAT>> resultMfgMatList = await mfgMatBLL.GetByMfgnum(MFGNUM_0, true);
            IEnumerable<MFGMAT> mfgMatList = resultMfgMatList.Obj;

            if (!mfgMatList.IsValid())
            {
                result.AddInvalidMessage($"Não foi possível encontrar info das matérias primas de consumo para a OF '{MFGNUM_0}'");
                result.AddInvalidMessage($"Tabela: {nameof(MFGMAT)} / {nameof(MFGMAT.MFGNUM_0)}: {MFGNUM_0}");

                return result;
            }

            foreach (MFGMAT mfgMatItem in mfgMatList)
            {
                Result<ATABDIV> resultAtabdiv = await atabdivBLL.GetByUniqueFields(6700, mfgMatItem.ZOPERCON_0);
                ATABDIV operationInfo = resultAtabdiv.Obj;

                if (operationInfo == null) //Verifico se a operação associada à MP de consumo existe na tabela ATABDIV com o NUMTAB 6700
                {
                    result.AddInvalidMessage($"A operação '{mfgMatItem.ZOPERCON_0}' associada à matéria prima '{mfgMatItem.ITMREF_0}' não se encontra configurada em sistema ou a mesma é inválida.");

                    return result;
                }

                resultAtabdiv = await atabdivBLL.GetByUniqueFields(6701, mfgMatItem.ZMODOCONS_0);
                ATABDIV consumptionModeInfo = resultAtabdiv.Obj;

                if (consumptionModeInfo == null) //Verifico se o modo de consumo associado à MP existe na tabela ATABDIV com o NUMTAB 6701
                {
                    result.AddInvalidMessage($"O modo de consumo '{mfgMatItem.ZMODOCONS_0}' associado à matéria prima '{mfgMatItem.ITMREF_0}' não se encontra configurado em sistema ou o mesmo é inválido.");
                    result.AddInvalidMessage($"Tabela: {nameof(ATABDIV)} / {nameof(ATABDIV.NUMTAB_0)}: 6701 / {nameof(ATABDIV.CODE_0)}: {mfgMatItem.ZOPERCON_0}");

                    return result;
                }
            }

            result.Obj.MfgMatList = mfgMatList;
            return result;
        }

        private async Task<Result<StojouData>> IsItmFacilityValid(Result<StojouData> result, string STOFCY_0, string ITMREF_0) //Verifico se a localização de reporte de produção para o estabelecimento produção da OF e Artigo final da OF existe na tabela ITMFACILIT
        {
            var resultItmFacilit = await itmFacilitBLL.GetByUniqueFields(STOFCY_0, ITMREF_0);

            ITMFACILIT itmFacilit = resultItmFacilit.Obj;
            result.Obj.ItmFacilit = itmFacilit;

            if (itmFacilit == null)
            {
                result.AddInvalidMessage($"Não foi possível encontrar info da localização de reporte produção para o estabelecimento '{STOFCY_0}' e artigo de produção '{ITMREF_0}'");

                result.AddInvalidMessage($"Tabela: {nameof(ITMFACILIT)} / {nameof(ITMFACILIT.STOFCY_0)}: {STOFCY_0} / {nameof(ITMFACILIT.ITMREF_0)}: {ITMREF_0}");

                return result;
            }

            if (string.IsNullOrEmpty(itmFacilit.DEFLOC_0) || string.IsNullOrEmpty(itmFacilit.DEFLOC_4)) //verifico se as localizações por defeito encontram-se preenchidas
            {
                result.AddInvalidMessage($"As localizações de reporte de produção para o estabelecimento '{STOFCY_0}' e artigo de produção '{ITMREF_0}' não se encontram definidas em sistema.");

                result.AddInvalidMessage($"Tabela: {nameof(ITMFACILIT)} / {nameof(ITMFACILIT.STOFCY_0)}: {STOFCY_0} / {nameof(ITMFACILIT.ITMREF_0)}: {ITMREF_0}");

                return result;
            }

            return result;
        }

        private async Task<Result<StojouData>> IsOperationValid(Result<StojouData> result, string OPERATION_0) //Verifico se a operação enviada por parametro existe na tabela ATABDIV com o NUMTAB 6700
        {
            Result<ATABDIV> resultAtabdiv = await atabdivBLL.GetByUniqueFields(6700, OPERATION_0);
            ATABDIV atabdiv = resultAtabdiv.Obj;

            if (atabdiv == null)
            {
                result.AddInvalidMessage($"A operação '{OPERATION_0}' não se encontra configurada em sistema.");
                result.AddInvalidMessage($"Tabela: {nameof(ATABDIV)} / {nameof(ATABDIV.NUMTAB_0)}: 6700 / {nameof(ATABDIV.CODE_0)}: {OPERATION_0}");

                return result;
            }

            result.Obj.OperationInfo = atabdiv;
            return result;
        }

        private async Task<Result<StojouData>> IsMachineValid(Result<StojouData> result, string MACHINE_0) //Verifico se a máquina enviada por parametro existe na tabela ATABDIV com o NUMTAB 6705
        {
            Result<ATABDIV> resultAtabdiv = await atabdivBLL.GetByUniqueFields(6705, MACHINE_0);
            ATABDIV machineInfo = resultAtabdiv.Obj;

            if (machineInfo == null)
            {
                result.AddInvalidMessage($"A máquina '{MACHINE_0}' não se encontra configurada em sistema.");

                result.AddInvalidMessage($"Tabela: {nameof(ATABDIV)} / {nameof(ATABDIV.NUMTAB_0)}: 6705 / {nameof(ATABDIV.CODE_0)}: {MACHINE_0}");

                return result;
            }

            Result<ATEXTRA> resultAtextra = await atextraBLL.GetByUniqueFields("ATABDIV", "LNGDES", "POR", machineInfo.NUMTAB_0.ToString(), MACHINE_0); //Obter descrição maquina

            ATEXTRA atextra = resultAtextra.Obj;

            if (atextra == null) //Verifico se foi possível obter a descrição da máquina na tabela ATEXTRA
            {
                result.AddInvalidMessage($"Não foi possível encontrar o nome da máquina com o código '{MACHINE_0}'.");

                result.AddInvalidMessage($"Tabela: {nameof(ATEXTRA)} / {nameof(ATEXTRA.CODFIC_0)}: ATABDIV / {nameof(ATEXTRA.ZONE_0)}: LINGDES / {nameof(ATEXTRA.LANGUE_0)}: POR " +
                    $"/ {nameof(ATEXTRA.IDENT1_0)}: {machineInfo.NUMTAB_0} / {nameof(ATEXTRA.IDENT2_0)}: {MACHINE_0}");

                return result;
            }

            result.Obj.MacNam = atextra;
            return result;
        }

        private async Task<Result<StojouData>> IsProfileMachineValid(Result<StojouData> result, string STOFCY_0, string OPERATION_0, string MACHINE_0, string MACNAM_0) //Verifico se o Perfil do utilizador tem acesso à máquina + operação + estabelecimento na tabela CONFIGVALUE com o CONFIGTYP PROFILEMAC
        {
            Result<IEnumerable<CONFIGVALUE>> configValueListResult = await configValueBLL.GetByFields(new CONFIGVALUE
            {
                CONFIGTYP_0 = "PROFILEMAC",
                VALUE_0 = AuthenticatedUser.PROFILE_0,
                VALUE_1 = STOFCY_0,
                VALUE_2 = OPERATION_0,
                VALUE_3 = MACHINE_0
            }, nameof(CONFIGVALUE.CONFIGTYP_0), nameof(CONFIGVALUE.VALUE_0), nameof(CONFIGVALUE.VALUE_1), nameof(CONFIGVALUE.VALUE_2), nameof(CONFIGVALUE.VALUE_3));

            if (!configValueListResult.Success)
            {
                result.ConvertFromResult(configValueListResult);

                return result;
            }

            IEnumerable<CONFIGVALUE> configValueList = configValueListResult.Obj;

            int count = configValueList.Count();

            if (count == 0)
            {
                result.AddInvalidMessage($"A máquina com o código '{MACHINE_0}' e nome '{MACNAM_0}' não se encontra associada ao Perfil '{AuthenticatedUser.PROFILE_0}' / Estabelecimento '{STOFCY_0}' / Operação '{OPERATION_0}'");

                result.AddInvalidMessage($"Tabela: {nameof(CONFIGVALUE)} / {nameof(CONFIGVALUE.CONFIGTYP_0)}: PROFILEMAC / {nameof(CONFIGVALUE.VALUE_0)}: {AuthenticatedUser.PROFILE_0} " +
                    $"/ {nameof(CONFIGVALUE.VALUE_1)}: {STOFCY_0} / {nameof(CONFIGVALUE.VALUE_2)}: {OPERATION_0} / {nameof(CONFIGVALUE.VALUE_3)}: {MACHINE_0}");

                return result;
            }

            if (count > 1)
            {
                result.AddInvalidMessage($"Existem {count} linhas configuradas de máquina associada para o Perfil '{AuthenticatedUser.PROFILE_0}' / Estabelecimento '{STOFCY_0}' / Operação '{OPERATION_0}' e código máquina '{MACHINE_0}'.");

                result.AddInvalidMessage($"Tabela: {nameof(CONFIGVALUE)} / {nameof(CONFIGVALUE.CONFIGTYP_0)}: PROFILEMAC / {nameof(CONFIGVALUE.VALUE_0)}: {AuthenticatedUser.PROFILE_0} " +
                    $"/ {nameof(CONFIGVALUE.VALUE_1)}: {STOFCY_0} / {nameof(CONFIGVALUE.VALUE_2)}: {OPERATION_0} / {nameof(CONFIGVALUE.VALUE_3)}: {MACHINE_0}");

                return result;
            }

            return result;
        }

        private async Task<Result<StojouData>> IsConsumptionInfoValid(Result<StojouData> result, string ZMFGTYP_0, string MFGFCY_0, string MACHINE_0) //Verifico que para o tipo de OF + estabelecimento produçao da OF + codigo maquina enviado por parametro existe configuraçao da localizaçao e status de consumo. Apenas pode existir uma linha
        {
            Result<IEnumerable<ATABDIV>> resultAtabdivList = await atabdivBLL.GetByFields(new ATABDIV
            {
                NUMTAB_0 = 6703,
                A1_0 = ZMFGTYP_0,
                A2_0 = MFGFCY_0,
                A3_0 = MACHINE_0
            });

            IEnumerable<ATABDIV> consumptionList = resultAtabdivList.Obj;

            int count = consumptionList.Count();

            if (count == 0)
            {
                result.AddInvalidMessage($"Localização e status de consumo não encontrados para o tipo da OF '{ZMFGTYP_0}' / estabelecimento '{MFGFCY_0}' e código máquina '{MACHINE_0}'");

                result.AddInvalidMessage($"Tabela: {nameof(ATABDIV)} / {nameof(ATABDIV.NUMTAB_0)}: 6703 / {nameof(ATABDIV.A1_0)}: {ZMFGTYP_0} / {nameof(ATABDIV.A2_0)}: {MFGFCY_0} " +
                    $" / {nameof(ATABDIV.A3_0)}: {MACHINE_0}");

                return result;
            }

            if (count > 1)
            {
                result.AddInvalidMessage($"Existem {count} linhas configuradas de localização e status de consumo para o tipo da OF '{ZMFGTYP_0}' / estabelecimento '{MFGFCY_0}' e código máquina '{MACHINE_0}'.");

                result.AddInvalidMessage($"Tabela: {nameof(ATABDIV)} / {nameof(ATABDIV.NUMTAB_0)}: 6703 / {nameof(ATABDIV.A1_0)}: {ZMFGTYP_0} / {nameof(ATABDIV.A2_0)}: {MFGFCY_0} " +
                    $" / {nameof(ATABDIV.A3_0)}: {MACHINE_0}");

                return result;
            }

            result.Obj.ConsumptionInfo = consumptionList.Single();
            return result;
        }

        private async Task<Result<StojouData>> IsProductionInfoValid(Result<StojouData> result, string ZMFGTYP_0, string MFGFCY_0, string OPERATION_0) //Verifico que para o tipo de OF + estabelecimento produçao da OF + operação existe configuraçao dos status por tipologia de reporte de produção. Apenas pode existir uma linha
        {
            var resultAtabdivList = await atabdivBLL.GetByFields(new ATABDIV
            {
                NUMTAB_0 = 6704,
                A1_0 = ZMFGTYP_0,
                A2_0 = MFGFCY_0,
                A3_0 = OPERATION_0
            });

            IEnumerable<ATABDIV> productionList = resultAtabdivList.Obj;

            int count = productionList.Count();

            if (count == 0)
            {
                result.AddInvalidMessage($"Status de reporte PA não encontrados para o tipo da OF '{ZMFGTYP_0}' / estabelecimento '{MFGFCY_0}' e operação '{OPERATION_0}'.");

                result.AddInvalidMessage($"Tabela: {nameof(ATABDIV)} / {nameof(ATABDIV.NUMTAB_0)}: 6704 / {nameof(ATABDIV.A1_0)}: {ZMFGTYP_0} / {nameof(ATABDIV.A2_0)}: {MFGFCY_0} " +
                    $" / {nameof(ATABDIV.A3_0)}: {OPERATION_0}");

                return result;
            }

            if (count > 1)
            {
                result.AddInvalidMessage($"Existem {count} linhas configuradas de localização e status de consumo para o tipo da OF '{ZMFGTYP_0}' / estabelecimento '{MFGFCY_0}' e operação '{OPERATION_0}'.");

                result.AddInvalidMessage($"Tabela: {nameof(ATABDIV)} / {nameof(ATABDIV.NUMTAB_0)}: 6704 / {nameof(ATABDIV.A1_0)}: {ZMFGTYP_0} / {nameof(ATABDIV.A2_0)}: {MFGFCY_0} " +
                    $" / {nameof(ATABDIV.A3_0)}: {OPERATION_0}");

                return result;
            }

            result.Obj.ProductionInfo = productionList.Single();
            return result;
        }

        private async Task<Result<StojouData>> IsFacilityValid(Result<StojouData> result, string STOFCY_0) //Verifico se existe uma empressa associada ao estabelecimento de produção da OF na tabela FACILITY
        {
            Result<FACILITY> resultFacility = await facilityBLL.GetByUniqueField(STOFCY_0);
            FACILITY facility = resultFacility.Obj;

            if (facility == null)
            {
                result.AddInvalidMessage($"Não foi possível encontrar info da empresa para o estabelecimento '{STOFCY_0}'");
                result.AddInvalidMessage($"Tabela: {nameof(FACILITY)} / {nameof(FACILITY.FCY_0)}: {STOFCY_0}");

                return result;
            }

            result.Obj.Facility = facility;
            return result;
        }

        private async Task<Result<StojouData>> IsStolocValid(Result<StojouData> result, string STOFCY_0, string LOC_0) //Verifico se a localização de consumo para o estabelecimento produção da OF existe na tabela STOLOC
        {
            Result<STOLOC> resultStoloc = await stolocBLL.GetByUniqueFields(STOFCY_0, LOC_0);
            STOLOC stoloc = resultStoloc.Obj;

            if (stoloc == null)
            {
                result.AddInvalidMessage($"Não foi possível encontrar info da localização para o estabelecimento '{STOFCY_0}' e localização '{LOC_0}'");

                result.AddInvalidMessage($"Tabela: {nameof(STOLOC)} / {nameof(STOLOC.STOFCY_0)}: {STOFCY_0} / {nameof(STOLOC.LOCCAT_0)}: {LOC_0}");

                return result;
            }

            result.Obj.Stoloc = stoloc;
            return result;
        }

        private async Task<Result<StojouData>> IsTabStaStoValid(Result<StojouData> result, string STA_0) //Verifico se o status de consumo existe na tabela TABSTASTO
        {
            Result<TABSTASTO> resultTabstasto = await tabstastoBLL.GetByUniqueField(STA_0);
            TABSTASTO tabstasto = resultTabstasto.Obj;

            if (tabstasto == null)
            {
                result.AddInvalidMessage($"Não foi possível encontrar info do status '{STA_0}'");
                result.AddInvalidMessage($"Tabela: {nameof(TABSTASTO)} / {nameof(TABSTASTO.STASTO_0)}: {STA_0}");

                return result;
            }

            result.Obj.TabStaSto = tabstasto;
            return result;
        }

        private async Task<Result<StojouData>> IsTabUnitValid(Result<StojouData> result, string STU_0) //Verifico se existe carras decimais associadas à unidade de stock na tabela TABUNIT
        {
            Result<TABUNIT> resultTabUnit = await tabUnitBLL.GetByUniqueField(STU_0);
            TABUNIT tabUnit = resultTabUnit.Obj;

            if (tabUnit == null)
            {
                result.AddInvalidMessage($"Não foi possível encontrar a configuração do número de casas decimais para a unidade de stock '{STU_0}'");
                result.AddInvalidMessage($"Tabela: {nameof(TABUNIT)} / {nameof(TABUNIT.UOM_0)}: {STU_0}");

                return result;
            }

            result.Obj.TabUnit = tabUnit;
            return result;
        }

        private Result<StojouData> IsQtyValid(Result<StojouData> result, MFGITM mfgItm, TABUNIT tabUnit, decimal PRODQTY_0)
        {
            if (PRODQTY_0 <= 0) //Verifico se a quantidade produção não é positiva
            {
                result.AddInvalidMessage($"Apenas é permitida a produção de valores positivos. Qtd produzida: {PRODQTY_0}");
                return result;
            }

            decimal roundedProdQty = Math.Round(PRODQTY_0, tabUnit.UOMDEC_0);

            if (PRODQTY_0 != roundedProdQty)
            {
                result.AddInvalidMessage($"A unidade do Artigo a produzir '{mfgItm.ITMREF_0}' é '{tabUnit.UOM_0}' e a mesma está definida com '{tabUnit.UOMDEC_0}' casas decimais.");

                result.AddInvalidMessage($"Uma vez que a quantidade produzida é '{PRODQTY_0}' a mesma é inválida devido a conter mais casas decimais do que o permitido.");
            }

            return result;
        }

        private async Task<Result<StojouData>> IsItmMasterValid(Result<StojouData> result, string ITMREF_0, string mfgItmStu) //Verifico se existe casas decimais associadas à unidade de stock na tabela TABUNIT
        {
            Result<ITMMASTER> resultItmmaster = await itmMasterBLL.GetByUniqueField(ITMREF_0);
            ITMMASTER itmMaster = resultItmmaster.Obj;

            if (itmMaster == null)
            {
                result.AddInvalidMessage($"Não foi possível encontrar info do artigo '{ITMREF_0}'");
                result.AddInvalidMessage($"Tabela: {nameof(ITMMASTER)} / {nameof(ITMMASTER.ITMREF_0)}: {ITMREF_0}");

                return result;
            }

            if (itmMaster.STU_0 != mfgItmStu) //Verifico se a unidade stock da ITMMASTER corresponde à unidade stock da MFGITM
            {
                result.AddInvalidMessage($"A unidade stock do artigo '{ITMREF_0}' ao nível da OF é a '{mfgItmStu}' e ao nível do Artigo é a '{itmMaster.STU_0}'.");
                result.AddInvalidMessage($"Ambas tem de corresponder.");

                return result;
            }

            result.Obj.ItmMaster = itmMaster;
            return result;
        }

        private IEnumerable<MFGMAT> GetMfgMatOperationList(IEnumerable<MFGMAT> mfgMatList, string OPERATION_0, bool PRODOK_0)
        {
            List<MFGMAT> mfgMatOperationList = new List<MFGMAT>();

            foreach (MFGMAT mfgMatItem in mfgMatList)
            {
                if ((mfgMatItem.ZOPERCON_0 == OPERATION_0) //Se operação da mfgMat corresponde à operação atual
                    && ((PRODOK_0 && mfgMatItem.ZCONSPRDOK_0 == 2) || (!PRODOK_0 && mfgMatItem.ZCONSPRDNOK_0 == 2))) //Se consumo é OK e ZCONSPRDOK_0 == 2 ou consumo é NOK e ZCONSPRDNOK_0 == 2
                {
                    mfgMatOperationList.Add(mfgMatItem);
                }
            }

            return mfgMatOperationList;
        }

        private async Task LockMfgMatList(IUnitOfWork uow, IEnumerable<MFGMAT> mfgMatOperationList) //Bloquear os artigos na ITMLOCK para garantir a coerencia de consumo quando existe concorrencia
        {
            foreach (MFGMAT mfgMatItem in mfgMatOperationList)
            {
                await uow.ItmLockRepository.Insert(new ITMLOCK
                {
                    ITMREF_0 = mfgMatItem.ITMREF_0,
                    CREUSR_0 = AuthenticatedUser.USR_0,
                    UPDUSR_0 = AuthenticatedUser.USR_0
                });
            }
        }

        private async Task<Result<IEnumerable<STOJOU>>> GetConsumptionStojou(StojouData data)
        {
            List<STOJOU> stojouList = new List<STOJOU>();
            Result<IEnumerable<STOJOU>> resultStojouList = new Result<IEnumerable<STOJOU>>(true);

            foreach (MFGMAT mfgMatItem in data.MfgMatOperationList) // Percorro as MP's da OF
            {
                decimal necessityOri = 0;

                if (mfgMatItem.ZMODOCONS_0 == "100") //Modo consumo: Proporcional OF
                {
                    necessityOri = mfgMatItem.BOMSTUCOE_0 * mfgMatItem.BOMQTY_0 * data.ProductionData.PRODQTY_0; //Cálculo da BOM sobre a qtd produzida
                }
                else if (mfgMatItem.ZMODOCONS_0 == "101" || mfgMatItem.ZMODOCONS_0 == "102") //Modo consumo: Por Sublote / Lote
                {
                    necessityOri = mfgMatItem.ZQTYMC_0; //Quantidade correspondente ao modo consumo
                }
                else
                {
                    resultStojouList.AddInvalidMessage($"O modo de consumo '{mfgMatItem.ZMODOCONS_0}' não é válido em sistema.");
                }

                decimal roundedNecessityOri = Math.Round(necessityOri, data.TabUnit.UOMDEC_0);

                if (necessityOri <= 0)
                {
                    resultStojouList.AddInvalidMessage($"O cálculo da necessidade de consumo tem que ser positivo.");
                    resultStojouList.AddInvalidMessage($"Artigo: '{mfgMatItem.ITMREF_0}' / Código calculo consumo: '{mfgMatItem.ZMODOCONS_0}' / Valor consumo após cálculo: {necessityOri}");

                    return resultStojouList;
                }

                if (roundedNecessityOri <= 0)
                {
                    resultStojouList.AddInvalidMessage($"A configuração das casas decimais para a unidade stock do artigo encontra-se errada ou a quantidade produzida é demasiado baixa. Devido a isso ao arredondar o valor de consumo, o mesmo passou a ser 0.");
                    resultStojouList.AddInvalidMessage($"Quantidade produzida: {data.ProductionData.PRODQTY_0} / Necessidade sem arredondamento: {necessityOri} / Necessidade com arredondamento: {roundedNecessityOri}");
                    resultStojouList.AddInvalidMessage($"Artigo: {mfgMatItem.ITMREF_0} / Unidade stock: {mfgMatItem.STU_0} / Nº casas decimais: {data.TabUnit.UOMDEC_0}");

                    return resultStojouList;
                }

                resultStojouList = await AddConsumptionsToStojou(data, mfgMatItem, roundedNecessityOri);

                if (!resultStojouList.Success)
                {
                    return resultStojouList;
                }

                stojouList.AddRange(resultStojouList.Obj);
            }

            resultStojouList.Obj = stojouList;

            return resultStojouList;
        }

        private async Task<Result<IEnumerable<STOJOU>>> AddConsumptionsToStojou(StojouData data, MFGMAT mfgMat, decimal roundedNecessityOri)
        {
            Result<IEnumerable<STOJOU>> resultStojouList;
            bool isFIFO = true;

            if (isFIFO) //Algoritmo FIFO
            {
                resultStojouList = await GetFIFOConsumptionList(data, mfgMat, roundedNecessityOri);
            }
            else //Algoritmo alocação
            {
                resultStojouList = await GetAllocationConsumptionList(data, mfgMat, roundedNecessityOri);
            }

            return resultStojouList;
        }

        private async Task<Result<IEnumerable<STOJOU>>> GetFIFOConsumptionList(StojouData data, MFGMAT mfgMat, decimal roundedNecessityOri)
        {
            List<STOJOU> stojouList = new List<STOJOU>();
            Result<IEnumerable<STOJOU>> resultStojouList = new Result<IEnumerable<STOJOU>>(true);
            IEnumerable<STOJOU> stojouPendingList = await GetStojouPending(data.MfgHead.MFGFCY_0, mfgMat.ITMREF_0);
            IEnumerable<STOCK> stockList = await GetStockAvailable(data.MfgHead.MFGFCY_0, mfgMat.ITMREF_0, data.ConsumptionInfo.A4_0, data.ConsumptionInfo.A5_0);

            decimal roundedNecessity = roundedNecessityOri;

            foreach (STOCK stockItem in stockList)
            {
                stockItem.QTYPCU_0 = GetStockQtypcu(stojouPendingList, stockItem, data.TabUnit);

                if (stockItem.QTYPCU_0 > 0) //Apenas adiciono a linha à STOJOU se a qtd stock for válida (superior a 0)
                {
                    stockItem.QTYPCUORI_0 = stockItem.QTYPCU_0;

                    if (roundedNecessity < stockItem.QTYPCU_0) //Caso a necessidade é menor que a qtdstock, atribuo à qtdstock o mesmo valor da necessidade para apenas consumir isso
                    {
                        stockItem.QTYPCU_0 = roundedNecessity;
                    }

                    roundedNecessity -= stockItem.QTYPCU_0;

                    stojouList.Add(CreateConsumptionStojouModel(data, mfgMat, stockItem));

                    if (roundedNecessity <= 0)
                    {
                        break;
                    }
                }
            }

            resultStojouList.Obj = stojouList;

            if (roundedNecessity > 0) //Se a necessidade continua a ser superior a 0 significa que não tenho stock suficiente para o consumo
            {
                resultStojouList.AddInvalidMessage($"Não existe stock disponível suficiente para o estabelecimento: {data.MfgHead.MFGFCY_0} / localização: {data.ConsumptionInfo.A4_0} / status: {data.ConsumptionInfo.A5_0}");
                resultStojouList.AddInvalidMessage($"Artigo: {mfgMat.ITMREF_0} / Stock necessário: {roundedNecessityOri} / Stock disponível: {roundedNecessityOri - roundedNecessity}");
            }

            return resultStojouList;
        }

        private async Task<IEnumerable<STOCK>> GetStockAvailable(string STOFCY_0, string ITMREF_0, string LOC_0, string STA_0) //Obter stock filtrado pelo estabelecimento, Artigo e localização/status consumo
        {
            STOCK stockModel = new STOCK
            {
                STOFCY_0 = STOFCY_0,
                ITMREF_0 = ITMREF_0,
                LOC_0 = LOC_0,
                STA_0 = STA_0
            };

            Result<IEnumerable<STOCK>> resultStockList = await stockBLL.GetByFieldsWithStolot(stockModel);
            IEnumerable<STOCK> stockList = resultStockList.Obj;

            return stockList;
        }

        private async Task<IEnumerable<STOJOU>> GetStojouPending(string STOFCY_0, string ITMREF_0) //Obter stojou filtrada pelo estabelecimento, artigo, succeeded = 1, regflg = 1 e importflg = 2
        {
            STOJOU stojouModel = new STOJOU
            {
                STOFCY_0 = STOFCY_0,
                ITMREF_0 = ITMREF_0,
                SUCCESSFLG_0 = 1,
                REGFLG_0 = 1
            };

            IEnumerable<STOJOU> stojouList = await GetByFields(stojouModel);

            return stojouList;
        }

        private decimal GetStockQtypcu(IEnumerable<STOJOU> stojouList, STOCK stock, TABUNIT tabUnit)
        {
            IEnumerable<STOJOU> stojouTemp = stojouList.Where(st => st.ITMREF_0 == stock.ITMREF_0 && st.LOT_0 == stock.LOT_0 && st.SLO_0 == stock.SLO_0);

            if (stojouTemp.IsValid())
            {
                decimal sumPending = stojouTemp.Sum(o => o.USENETQTY_0); //Aqui obtenho o sumatório de todas as quantidades da stojouTemp

                stock.QTYPCU_0 -= sumPending; //Retiro à qtd stock a qtd pendente calculada em cima
            }

            return Math.Round(stock.QTYPCU_0, tabUnit.UOMDEC_0);
        }

        private async Task<Result<IEnumerable<STOJOU>>> GetAllocationConsumptionList(StojouData data, MFGMAT mfgMat, decimal roundedNecessityOri) //Obter stock filtrado pelo estabelecimento, Artigo e localização/status consumo
        {
            List<STOJOU> stojouList = new List<STOJOU>();
            Result<IEnumerable<STOJOU>> resultStojouList = new Result<IEnumerable<STOJOU>>(true);
            Result<IEnumerable<DOCALL>> resultDocAllList = await docAllBLL.GetDocItmAllAvailable(data.MfgHead.MFGFCY_0, data.MfgHead.MFGNUM_0, mfgMat.ITMREF_0);
            IEnumerable<DOCALL> docAllList = resultDocAllList.Obj;
            decimal roundedNecessity = roundedNecessityOri;

            foreach (DOCALL docAllItem in docAllList)
            {
                if (docAllItem.LOC_0 == data.ConsumptionInfo.A4_0 && docAllItem.STA_0 == data.ConsumptionInfo.A5_0) //Garanto que apenas considero o material alocado na localização e status consumo parametrizados em sistema
                {
                    STOCK st = new STOCK
                    {
                        STOFCY_0 = docAllItem.STOFCY_0,
                        ITMREF_0 = docAllItem.ITMREF_0,
                        LOT_0 = docAllItem.LOT_0,
                        SLO_0 = docAllItem.SLO_0,
                        LOC_0 = docAllItem.LOC_0,
                        STA_0 = docAllItem.STA_0
                    };
                    Result<IEnumerable<STOCK>> resultStockList = await stockBLL.GetByFields(st);
                    IEnumerable<STOCK> stockList = resultStockList.Obj;

                    if (!stockList.IsValid()) //Se o stock não corresponde
                    {
                        resultStojouList.AddInvalidMessage($"Não foi possível localizar o stock alocado.");
                        resultStojouList.AddInvalidMessage($"Estab: '{st.STOFCY_0}' / Artigo: '{st.ITMREF_0}' / Lote: '{st.LOT_0}' / Sublote: '{st.SLO_0}' / Localizacao: '{st.LOC_0}' / Status: '{st.STA_0}'");
                        resultStojouList.AddInvalidMessage($"Tabela: {nameof(DOCALL)} / {nameof(DOCALL.ROWID)} : {docAllItem.ROWID}");

                        break;
                    }

                    foreach (STOCK stockItem in stockList)
                    {
                        if (stockItem.QTYPCU_0 > 0) //Apenas adiciono a linha à STOJOU se a qtd stock for válida (superior a 0)
                        {
                            if (roundedNecessity < stockItem.QTYPCU_0) //Caso a necessidade é menor que a qtdstock, atribuo à qtdstock o mesmo valor da necessidade para apenas consumir isso
                            {
                                stockItem.QTYPCU_0 = roundedNecessity;
                            }

                            if (stockItem.QTYPCU_0 > docAllItem.QTYPCU_0)
                            {
                                stockItem.QTYPCU_0 = docAllItem.QTYPCU_0;
                            }

                            roundedNecessity -= stockItem.QTYPCU_0; //Abater quantidade na necessidade
                            docAllItem.QTYPCU_0 -= stockItem.QTYPCU_0; //Abater quantidade alocação

                            STOJOU consumptionStojou = CreateConsumptionStojouModel(data, mfgMat, stockItem);
                            consumptionStojou.DocAllModel = docAllItem;
                            stojouList.Add(consumptionStojou);

                            if (roundedNecessity <= 0)
                            {
                                break;
                            }
                        }
                    }
                }
            }

            resultStojouList.Obj = stojouList;

            if (roundedNecessity > 0) //Se a necessidade continua a ser superior a 0 significa que não tenho stock suficiente para o consumo
            {
                resultStojouList.AddInvalidMessage($"Não existe stock alocado disponível suficiente para o estabelecimento: {data.MfgHead.MFGFCY_0} / localização: {data.ConsumptionInfo.A4_0} / status: {data.ConsumptionInfo.A5_0}");
                resultStojouList.AddInvalidMessage($"Artigo: {mfgMat.ITMREF_0} / Stock necessário: {roundedNecessityOri} / Stock disponível: {roundedNecessityOri - roundedNecessity}");
                resultStojouList.AddInvalidMessage($"Tabela: {QueryStringHelper.TableName<DOCALL>()} / {nameof(DOCALL.STOFCY_0)} : {data.MfgHead.MFGFCY_0} / {nameof(DOCALL.DOCUMENT_0)} / {data.MfgHead.MFGNUM_0}");
            }

            return resultStojouList;
        }

        private STOJOU CreateConsumptionStojouModel(StojouData data, MFGMAT mfgMat, STOCK stock)
        {
            STOJOU stojou = new STOJOU()
            {
                WSCODE_0 = data.WsConsumption.CODE_0,
                CPY_0 = data.Facility.LEGCPY_0,
                STOFCY_0 = data.MfgHead.MFGFCY_0,
                DESSTOFCY_0 = string.Empty,
                MFGNUM_0 = data.MfgHead.MFGNUM_0,
                MFGTYP_0 = data.MfgHead.ZMFGTYP_0,
                MFGSTA_0 = data.MfgHead.MFGSTA_0,
                MFGLIN_0 = mfgMat.MFGLIN_0,
                BOMALT_0 = data.MfgItm.BOMALT_0,
                BOMQTY_0 = mfgMat.BOMQTY_0,
                BOMSEQ_0 = mfgMat.BOMSEQ_0,
                BOMNUM_0 = data.MfgItm.ITMREF_0,
                ITMREF_0 = stock.ITMREF_0,
                LOT_0 = stock.LOT_0,
                SLO_0 = stock.SLO_0,
                QTYPCUORI_0 = stock.QTYPCUORI_0,
                USENETQTY_0 = stock.QTYPCU_0,
                USEGROSSQTY_0 = stock.QTYPCU_0,
                DEFECT_0 = 0,
                USELOC_0 = stock.LOC_0,
                USELOCTYP_0 = stock.LOCTYP_0,
                DESLOC_0 = string.Empty,
                DESLOCTYP_0 = string.Empty,
                USESTA_0 = stock.STA_0,
                DESSTA_0 = string.Empty,
                USESTU_0 = mfgMat.STU_0,
                BOXID_0 = 0,
                CUSORDREF_0 = string.Empty,
                YTRANS_0 = "MKM",
                YDAT_0 = DateTime.Now.ToString("ddMMyyyy"),
                TRSTYP_0 = "6",
                SLOORI_0 = string.Empty,
                VCRNUMORI_0 = data.MfgHead.MFGNUM_0,
                VCRNUMDES_0 = string.Empty,
                SCANCODE_0 = Convert.ToInt32(stock.StolotModel?.USRFLD3_0),
                MACHINE_0 = data.ProductionData.MACHINE_0,
                MACNAM_0 = data.MacNam.TEXTE_0,
                OPERATION_0 = data.ProductionData.OPERATION_0,
                HOSTNAM_0 = data.ProductionData.HOSTNAM_0,
                IMPORTFLG_0 = 2,
                CREUSR_0 = AuthenticatedUser.USR_0,
                UPDUSR_0 = AuthenticatedUser.USR_0
            };

            return stojou;
        }

        private async Task<Result<STOJOU>> GetProductionStojou(IEnumerable<STOJOU> stojouProductionList, StojouData data)
        {
            //De futuro chamar funçao que controla se posso produzir ou não, dependendo dos consumos efetuados nas operações anteriores
            //Ter validações produção aqui

            string USELOCTYP_0;
            string USELOC_0;
            string USESTA_0;
            string SLO_0 = await GetNextProductionSlo(data.MfgHead.MFGNUM_0); //Obtenho o próximo sublote da OF
            Result<QuantityData> resultQuantityData = await CalculateQuantity(data, SLO_0); //Calculo defeitos se aplicável
            Result<STOJOU> resultStojou = Result<STOJOU>.ConvertAndGetFromResult(resultQuantityData);
            data.QuantityData = resultQuantityData.Obj;

            if (resultStojou.Success)
            {
                foreach (STOJOU stojouItem in stojouProductionList) //Atribuo às linhas de consumo o valor do nextSlo à propriedade SLOORI_0 para termos o rasto do sublote de produção correspondente aos consumo
                {
                    stojouItem.SLOORI_0 = SLO_0;
                }

                if (data.ProductionData.PRODOK_0)
                {
                    USELOCTYP_0 = data.ItmFacilit.DEFLOCTYP_0;
                    USELOC_0 = data.ItmFacilit.DEFLOC_0;
                    USESTA_0 = data.ProductionInfo.A4_0; //Status reporte OK

                    //Aqui é onde terei de acrescentar ainda uma forma de verificar se gera controlo qualidade, se sim atribuio o A5_0 que corresponde ao status reporte CQ

                    //USESTA_0 = productionInfo.A5_0; //Status reporte CQ
                }
                else
                {
                    USELOCTYP_0 = data.ItmFacilit.DEFLOCTYP_4;
                    USELOC_0 = data.ItmFacilit.DEFLOC_4;
                    USESTA_0 = data.ProductionInfo.A6_0; //Status reporte NOK
                }

                if (resultQuantityData.Obj.USENETQTY_0 <= 0 || resultQuantityData.Obj.USEGROSSQTY_0 <= 0)
                {
                    resultStojou.AddInvalidMessage($"Não é possível o registo de produção com quantidades não positivas. Qtd líquida: {resultQuantityData.Obj.USENETQTY_0} / Qtd Bruta: {resultQuantityData.Obj.USEGROSSQTY_0}");
                    resultStojou.AddInvalidMessage($"Verifique se o somatório dos defeitos é superior ou igual à quantidade produzida.");
                }

                if (resultStojou.Success)
                {
                    STOJOU stojou = CreateProductionStojouModel(data, SLO_0, USELOC_0, USELOCTYP_0, USESTA_0);
                    resultStojou.Obj = stojou;
                }
            }

            return resultStojou;
        }

        private async Task<Result<QuantityData>> CalculateQuantity(StojouData data, string SLO_0)
        {
            Result<QuantityData> result = new Result<QuantityData>(true);
            QuantityData quantityData = new QuantityData
            {
                QTYPCUORI_0 = data.ProductionData.PRODQTY_0,
                USENETQTY_0 = data.ProductionData.PRODQTY_0,
                USEGROSSQTY_0 = data.ProductionData.PRODQTY_0,
                DEFECT_0 = 0
            };

            if (data.OperationInfo.A1_0 == "2") //verifico se a operação atual está configurada para descontar defeitos sobre a quantidade a produzir
            {
                STODEFECT stoDefect = new STODEFECT
                {
                    MFGNUM_0 = data.ProductionData.MFGNUM_0,
                    ITMREF_0 = data.MfgItm.ITMREF_0,
                    LOT_0 = data.ProductionData.MFGNUM_0,
                    SLO_0 = string.Empty,
                    MACHINE_0 = data.ProductionData.MACHINE_0,
                    OPERATION_0 = data.ProductionData.OPERATION_0
                };

                Result<IEnumerable<STODEFECT>> resultStoDefectList = await stoDefectBLL.GetByStoDefect(stoDefect);
                IEnumerable<STODEFECT> defectList = resultStoDefectList.Obj;

                foreach (STODEFECT defectItem in defectList)
                {
                    defectItem.SLO_0 = SLO_0;

                    if (defectItem.DEFECTTYP_0 == "Bonificado")
                    {
                        if (defectItem.BONUSABLEFLG_0 == 2) //Se BONUSABLEFLG_0 == 2 significa que o cliente está definido como bonificável, ou seja, defeitos devem ser descontados à qtd liquida
                        {
                            quantityData.USENETQTY_0 -= defectItem.QTYPCU_0;
                        }

                        quantityData.DEFECT_0 += 1;
                    }
                    else //Aqui significa que o defeito é sucata
                    {
                        quantityData.USEGROSSQTY_0 -= defectItem.QTYPCU_0;
                        quantityData.USENETQTY_0 -= defectItem.QTYPCU_0;
                    }

                    Result<STODEFECT> resultStoDefect = await stoDefectBLL.Update(defectItem);
                    resultStoDefect.ConvertFromResult(resultStoDefect);

                    if (!resultStoDefect.Success)
                    {
                        break;
                    }
                }
            }

            result.Obj = quantityData;

            return result;
        }

        private STOJOU CreateProductionStojouModel(StojouData data, string SLO_0, string USELOC_0, string USELOCTYP_0, string USESTA_0)
        {
            STOJOU stojou = new STOJOU
            {
                WSCODE_0 = data.WsProduction.CODE_0,
                CPY_0 = data.Facility.LEGCPY_0,
                STOFCY_0 = data.MfgHead.MFGFCY_0,
                DESSTOFCY_0 = string.Empty,
                MFGNUM_0 = data.MfgHead.MFGNUM_0,
                MFGTYP_0 = data.MfgHead.ZMFGTYP_0,
                MFGSTA_0 = data.MfgHead.MFGSTA_0,
                MFGLIN_0 = data.MfgItm.MFGLIN_0,
                BOMALT_0 = data.MfgItm.BOMALT_0,
                BOMQTY_0 = 0,
                BOMSEQ_0 = 0,
                BOMNUM_0 = data.MfgItm.ITMREF_0,
                ITMREF_0 = data.MfgItm.ITMREF_0,
                LOT_0 = data.MfgHead.MFGNUM_0,
                SLO_0 = SLO_0,
                QTYPCUORI_0 = data.QuantityData.QTYPCUORI_0,
                USENETQTY_0 = data.QuantityData.USENETQTY_0,
                USEGROSSQTY_0 = data.QuantityData.USEGROSSQTY_0,
                DEFECT_0 = data.QuantityData.DEFECT_0,
                USELOC_0 = USELOC_0,
                USELOCTYP_0 = USELOCTYP_0,
                DESLOC_0 = string.Empty,
                DESLOCTYP_0 = string.Empty,
                USESTA_0 = USESTA_0,
                DESSTA_0 = string.Empty,
                USESTU_0 = data.MfgItm.STU_0,
                BOXID_0 = 0,
                CUSORDREF_0 = string.Empty,
                YTRANS_0 = "MKI",
                YDAT_0 = DateTime.Now.ToString("ddMMyyyy"),
                TRSTYP_0 = "5",
                SLOORI_0 = string.Empty,
                VCRNUMORI_0 = data.MfgHead.MFGNUM_0,
                VCRNUMDES_0 = data.ProductionData.VCRNUMDES_0,
                SCANCODE_0 = 0,
                MACHINE_0 = data.ProductionData.MACHINE_0,
                MACNAM_0 = data.MacNam.TEXTE_0,
                OPERATION_0 = data.ProductionData.OPERATION_0,
                HOSTNAM_0 = data.ProductionData.HOSTNAM_0,
                IMPORTFLG_0 = 1,
                CREUSR_0 = AuthenticatedUser.USR_0,
                UPDUSR_0 = AuthenticatedUser.USR_0
            };

            if (data.ProductionData.OPERATION_0.ToString() == data.MfgItm.ZOPERRPA_0) //Se operação enviada por parametro é igual à operação do reporte de produção adiciono linha de produçao à STOJOU
            {
                stojou.IMPORTFLG_0 = 2;
            }

            return stojou;
        }

        private async Task<Result<STOJOU>> InsertProductionList(IUnitOfWork uow, List<STOJOU> stojouInsertList)
        {
            Result<STOJOU> result = new Result<STOJOU>(true);
            string SLO_0 = string.Empty;

            if (stojouInsertList.IsValid())
            {
                foreach (STOJOU stojouItem in stojouInsertList)
                {
                    if (stojouItem.DocAllModel != null)
                    {
                        Result<DOCALL> resultDocAll = await docAllBLL.Update(stojouItem.DocAllModel); //Atualizar alocação se aplicável
                        result.ConvertFromResult(resultDocAll);

                        if (!result.Success)
                        {
                            break;
                        }
                    }

                    string jsonStojou = JsonConvert.SerializeObject(stojouItem);

                    if (stojouItem.WSCODE_0 == "PRODUCTION")
                    {
                        PRODUCTION production = JsonConvert.DeserializeObject<PRODUCTION>(jsonStojou);

                        await uow.ProductionRepository.Insert(production);
                    }
                    else if (stojouItem.WSCODE_0 == "CONSUMPTION")
                    {
                        CONSUMPTION consumption = JsonConvert.DeserializeObject<CONSUMPTION>(jsonStojou);

                        await uow.ConsumptionRepository.Insert(consumption);
                    }

                    if (stojouItem.IMPORTFLG_0 == 2) //Apenas insiro na STOJOU se é para importar no SAGE
                    {
                        result = await Insert(stojouItem);

                        if (!result.Success)
                        {
                            break;
                        }
                    }
                    else
                    {
                        result.Obj = stojouItem;
                    }
                }
            }
            else
            {
                result.AddInvalidMessage($"É obrigatório existir transformação de stock.");
                result.AddInvalidMessage($"Como tal é obrigatório existir pelo menos uma matéria prima a consumir ou a operação ser definida como reporte de PA.");
            }

            if (result.Success)
            {
                result.Messages.Clear();
                result.AddMessage($"Produção com o sublote {SLO_0} foi registada com sucesso.");
            }

            return result;
        }
        #endregion

        #region ImportWebService
        private async Task<Result<WEBSERVICE>> IsImportWSValid(string WEBSERVICE_0)
        {
            Result<WEBSERVICE> result = WEBSERVICE_0.IsValueValid<WEBSERVICE>(nameof(WEBSERVICE.CODE_0));

            if (result.Success)
            {
                Result<WEBSERVICE> webserviceResult = await webServiceBLL.GetByUniqueField(WEBSERVICE_0);
                WEBSERVICE webService = webserviceResult.Obj;

                if (webService != null)
                {
                    if (webService.IMPORTFLG_0 == 1)
                    {
                        result.AddInvalidMessage($"O webservice '{WEBSERVICE_0}' não se encontra definido como webservice de importação de dados.");
                    }
                }
                else
                {
                    result.AddInvalidMessage($"Não foi possível obter informação para o webservice '{WEBSERVICE_0}'");
                }

                result.Obj = webService;
            }

            return result;
        }

        private async Task<Result<STOJOU>> ProcessWebServiceData(IUnitOfWork uow, Result<WebServiceData> webServiceDataResult, STOJOU stojou)
        {
            Result<STOJOU> resultStojou = new Result<STOJOU>();

            if (webServiceDataResult != null && stojou != null)
            {
                UpdateByWebServiceResult(stojou, webServiceDataResult);

                if (!webServiceDataResult.Success) //Se falhou a importação com o Sage verificar o stock e atualizar os campos
                {
                    STOCK stock = new STOCK
                    {
                        STOFCY_0 = stojou.STOFCY_0,
                        ITMREF_0 = stojou.ITMREF_0,
                        LOT_0 = stojou.LOT_0,
                        SLO_0 = stojou.SLO_0,
                        LOC_0 = stojou.USELOC_0,
                        STA_0 = stojou.USESTA_0,
                        PCU_0 = stojou.USESTU_0
                    };

                    Result<IEnumerable<STOCK>> resultStockList = await stockBLL.GetByFields(stock, nameof(STOCK.STOFCY_0), nameof(STOCK.ITMREF_0), nameof(STOCK.LOT_0), 
                        nameof(STOCK.SLO_0), nameof(STOCK.LOC_0), nameof(STOCK.STA_0), nameof(STOCK.PCU_0));
                    IEnumerable<STOCK> stockList = resultStockList.Obj;

                    UpdateByStock(stojou, stockList);
                }

                resultStojou = await UpdateImportStojou(uow, stojou);
            }
            else
            {
                resultStojou.AddInvalidMessage("Parametros webServiceDataResult e stojou encontram-se null.");
            }

            return resultStojou;
        }

        private async Task<Result<STOJOU>> UpdateImportStojou(IUnitOfWork uow, STOJOU stojou)
        {
            Result<STOJOU> resultStojou = await Update(stojou);

            if (resultStojou.Success)
            {
                string jsonStojou = JsonConvert.SerializeObject(stojou);
                STOJOULOG stojouLog = JsonConvert.DeserializeObject<STOJOULOG>(jsonStojou);
                stojouLog.STOJOUID = stojou.ROWID;

                await uow.StojouLogRepository.Insert(stojouLog);
            }

            return resultStojou;
        }

        private void UpdateByWebServiceResult(STOJOU stojou, Result<WebServiceData> result)
        {
            if (stojou != null)
            {
                WebServiceData webServiceData = result.Obj;

                stojou.SUCCESSFLG_0 = Convert.ToInt32(result.Success) + 1;
                stojou.ATTEMPTS_0 += 1;
                stojou.XML_0 = webServiceData.XML;
                stojou.OUTPUTWS_0 = webServiceData.OUTPUTWS_0;
                stojou.VCRNUM_0 = result.Obj.Document;
            }
        }

        private void UpdateByStock(STOJOU stojou, IEnumerable<STOCK> stockList)
        {
            if (stojou != null)
            {
                if (stockList.IsValid())
                {
                    STOCK stock = stockList.First();

                    decimal sumQty = stockList.Sum(o => o.QTYPCU_0);

                    stojou.QTYPCU_0 = sumQty;
                    stojou.LOC_0 = stock.LOC_0;
                    stojou.STA_0 = stock.STA_0;
                    stojou.STU_0 = stock.PCU_0;
                    stojou.LOCTYP_0 = stock.LOCTYP_0;
                }
                else
                {
                    stojou.QTYPCU_0 = 0;
                    stojou.LOC_0 = "N/D";
                    stojou.STA_0 = "N/D";
                    stojou.STU_0 = "N/D";
                    stojou.LOCTYP_0 = "N/D";
                }
            }
        }
        #endregion
        #endregion
    }
}
