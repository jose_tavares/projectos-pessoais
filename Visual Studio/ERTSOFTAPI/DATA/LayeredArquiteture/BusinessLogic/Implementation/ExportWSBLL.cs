﻿namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{
    using System;
    using System.Xml;
    using MODELS.Helpers;
    using System.Diagnostics;
    using System.Transactions;
    using System.Data.SqlClient;
    using System.Threading.Tasks;
    using DATA.Helpers.StaticClasses;
    using System.Collections.Generic;
    using DATA.Helpers.AuxiliarClasses;
    using MODELS.BusinessObject.ERTSOFT.Database;
    using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;
    using System.Linq;
    using DATA.LayeredArquiteture.Repositories.UnitOfWork;

    public class ExportWSBLL : IExportWSBLL
    {
        #region Public Methods
        public USER AuthenticatedUser { get; set; }

        public ExportWSBLL()
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();
        }

        public async Task<IEnumerable<EXPORTWS>> GetAll()
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
            {
                IEnumerable<EXPORTWS> exportWSList = await uow.ExportWSRepository.GetAll();

                uow.Commit();

                return exportWSList;
            }
        }

        public async Task<IEnumerable<EXPORTWS>> GetPending()
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
            {
                IEnumerable<EXPORTWS> exportWSList = await uow.ExportWSRepository.GetPending();

                uow.Commit();

                return exportWSList;
            }
        }

        public async Task<EXPORTWS> GetByID(decimal id)
        {
            Result<EXPORTWS> result = id.IsValueValid<EXPORTWS>(nameof(EXPORTWS.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                {
                    EXPORTWS exportWS = await uow.ExportWSRepository.GetByID(id);

                    uow.Commit();

                    return exportWS;
                }
            }

            return null;
        }

        public async Task<EXPORTWS> GetByExportWS(string EXPORTWS_0)
        {
            Result<EXPORTWS> result = EXPORTWS_0.IsValueValid<EXPORTWS>(nameof(EXPORTWS.EXPORTWS_0));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                {
                    EXPORTWS exportWS = await uow.ExportWSRepository.GetByUniqueField(nameof(EXPORTWS.EXPORTWS_0), EXPORTWS_0);

                    uow.Commit();

                    return exportWS;
                }
            }

            return null;
        }

        public async Task<Result<EXPORTWS>> ExportPending()
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable, TransactionScopeOption.Required, TransactionManager.MaximumTimeout))
            {
                EXPORTWS export = await GetFirstPending();
                Result<EXPORTWS> result = new Result<EXPORTWS>(true);

                if (export != null)
                {
                    if (export.WebServiceModel != null)
                    {
                        Stopwatch sw = Stopwatch.StartNew();

                        if (export.NEXTREWRITEDATE_0 <= DateTime.UtcNow)
                        {
                            export.REWRITEFLG_0 = 2;
                        }

                        EXPORTWSLOG exportLog = GetExportLog(export);
                        Dictionary<string, SqlParameter> parameters = uow.ExportWSRepository.GetParameters(export.MAINTABLE_0);
                        Result<EXPORTWSLOG> resultExportWSLog = await uow.ExportWSLogRepository.Insert(exportLog);
                        result.ConvertFromResult(resultExportWSLog);

                        if (result.Success)
                        {
                            exportLog.ROWID = result.InsertedId;
                            await uow.ExportWSRepository.DeleteTemp(export);
                            (Result<WebServiceData> result, EXPORTWS export, EXPORTWSLOG exportLog, DateTime initialDate) data;
                            DateTime initialDate;

                            if (export.REWRITEFLG_0 == 1) //Aqui vou apenas escrever a diferença através de webservice
                            {
                                initialDate = export.NEXTWRITEDATE_0;
                                data = await ProcessByWebService(uow, export, exportLog, parameters, initialDate);
                            }
                            else //Aqui vou reescrever tudo através da query configurada no EXPORTWS
                            {
                                initialDate = export.NEXTREWRITEDATE_0;
                                data = await ProcessByQuery(uow, export, exportLog, parameters, initialDate);
                            }

                            export.REWRITEFLG_0 = 1;
                            export.LASTWRITEDATE_0 = data.initialDate;

                            await uow.ExportWSRepository.Update(export);

                            resultExportWSLog = await UpdateExportLog(uow, data.export, data.result, data.exportLog, Convert.ToInt32(sw.ElapsedMilliseconds));
                            result.ConvertFromResult(resultExportWSLog);

                            if (result.Success)
                            {
                                uow.Commit();
                            }
                        }
                    }
                    else
                    {
                        result.AddInvalidMessage($"Webservice da exportação não encontrado. Code a procurar: {export.WSCODE_0}");
                    }
                }

                return result;
            }
        }

        public async Task<Result<EXPORTWS>> Insert(EXPORTWS exportWS)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
            {
                Result<EXPORTWS> result = await IsValidInsert(exportWS);

                if (result.Success)
                {
                    exportWS.CREUSR_0 = AuthenticatedUser.USR_0;
                    exportWS.UPDUSR_0 = AuthenticatedUser.USR_0;

                    result = await uow.ExportWSRepository.Insert(exportWS);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<EXPORTWS>> Update(EXPORTWS exportWS)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
            {
                Result<EXPORTWS> result = await IsValidUpdate(exportWS);

                if (result.Success)
                {
                    exportWS.UPDUSR_0 = AuthenticatedUser.USR_0;

                    result = await uow.ExportWSRepository.Update(exportWS);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<EXPORTWS>> IsValidInsert(EXPORTWS exportWS)
        {
            // Add more validations if needed
            Result<EXPORTWS> result = await exportWS.IsClassValidAsync();

            if (result.Success)
            {
                EXPORTWS exportWSTemp = await GetByExportWS(exportWS.EXPORTWS_0);

                if (exportWSTemp != null)
                {
                    result.AddInvalidMessage($"Já existe uma exportação com o nome {exportWS.EXPORTWS_0}");
                }
            }

            return result;
        }

        public async Task<Result<EXPORTWS>> IsValidUpdate(EXPORTWS exportWS)
        {
            // Add more validations if needed
            Result<EXPORTWS> result = await exportWS.IsClassValidAsync();

            if (result.Success)
            {
                EXPORTWS exportWSTemp = await GetByID(exportWS.ROWID);

                if (exportWSTemp == null)
                {
                    result.AddInvalidMessage($"Não foi possível encontrar a exportação com o ID {exportWS.ROWID}.");
                }
                else if (exportWS.EXPORTWS_0 != exportWSTemp.EXPORTWS_0)
                {
                    result.AddInvalidMessage($"Não é permitido alterar o campo {nameof(exportWS.EXPORTWS_0)}");
                }
            }

            return result;
        }
        #endregion

        #region Private Methods
        private async Task<EXPORTWS> GetFirstPending()
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
            {
                EXPORTWS exportWS = await uow.ExportWSRepository.GetFirstPending();

                if (exportWS != null)
                {
                    exportWS.WebServiceModel = await uow.WebServiceRepository.GetByUniqueField(nameof(WEBSERVICE.CODE_0), exportWS.WSCODE_0);
                }

                uow.Commit();

                return exportWS;
            }
        }

        private EXPORTWSLOG GetExportLog(EXPORTWS export)
        {
            EXPORTWSLOG exportLog = new EXPORTWSLOG
            {
                EXPORTWS_0 = export.EXPORTWS_0,
                WSNAM_0 = export.WebServiceModel.NAM_0,
                WSCODE_0 = export.WSCODE_0,
                OUTPUTWS_0 = string.Empty,
                OUTPUT_0 = string.Empty,
                XML_0 = string.Empty,
                SUCCESSFLG_0 = 1,
                REWRITEFLG_0 = export.REWRITEFLG_0,
                CREUSR_0 = AuthenticatedUser.USR_0,
                UPDUSR_0 = AuthenticatedUser.USR_0
            };

            return exportLog;
        }

        private async Task<(Result<WebServiceData> result, EXPORTWS export, EXPORTWSLOG exportLog, DateTime initialDate)> ProcessByWebService(IUnitOfWork uow, EXPORTWS export, EXPORTWSLOG exportLog, Dictionary<string, SqlParameter> parameters, DateTime initialDate)
        {
            string XML = export.WebServiceModel.PARAMBEGIN_0.Replace("@DATE", export.LASTWRITEDATE_0.ToString("yyyyMMdd HH:mm:ss"));
            Result<WebServiceData> result = await WebServiceHelper.RunWebServiceAsync(export.WebServiceModel, XML);

            if (result.Success)
            {
                string lastRowDate = string.Empty;
                WebServiceData webServiceData = result.Obj;
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.LoadXml(webServiceData.ResultXML);
                XmlNodeList nodesList = xmlDocument.SelectNodes("RESULT/TAB/LIN");
                List<ExportData> exportDataList = new List<ExportData>();

                foreach (XmlNode node in nodesList)
                {
                    exportDataList.Add(uow.ExportWSRepository.GetExportDataByXMLNode(export.WebServiceModel, node, parameters, exportLog.ROWID));
                }

                exportLog = await uow.ExportWSRepository.InsertFromExportDataList(exportDataList, export, exportLog);

                if (exportDataList.IsValid())
                {
                    lastRowDate = exportDataList.Last().DateValue;
                }

                if (nodesList.Count == export.WebServiceModel.MAXROWS_0)
                {
                    initialDate = Convert.ToDateTime(lastRowDate);

                    if (export.LASTWRITEDATE_0 == initialDate)
                    {
                        initialDate = initialDate.AddSeconds(1);
                    }

                    export.NEXTWRITEDATE_0 = initialDate;
                }
                else
                {
                    export.NEXTWRITEDATE_0 = initialDate.AddSeconds(export.WRITEFREQUENCY_0);
                }

                exportLog.OUTPUT_0 = "Exportação efetuada com sucesso.";
            }
            else
            {
                exportLog.OUTPUT_0 = $"Ocorreu algum problema na invocação do Webservice {export.WebServiceModel.CODE_0}";

                export.NEXTWRITEDATE_0 = initialDate;
            }

            return (result, export, exportLog, initialDate);
        }

        private async Task<(Result<WebServiceData> result, EXPORTWS export, EXPORTWSLOG exportLog, DateTime initialDate)> ProcessByQuery(IUnitOfWork uow, EXPORTWS export, EXPORTWSLOG exportLog, Dictionary<string, SqlParameter> parameters, DateTime initialDate)
        {
            exportLog.DELETEDROWS_0 = await uow.ExportWSRepository.DeleteFromQuery(export.DELETEQUERY_0);

            using (SqlConnection connection = DBHelper.NewConnection("SAGE"))
            {
                using (SqlCommand command = connection.NewCommand(export.SELECTQUERY_0))
                {
                    using (SqlDataReader reader = command.NewDataReader())
                    {
                        while (reader.Read())
                        {
                            ExportData exportData = uow.ExportWSRepository.GetExportDataByDataReader(export.WebServiceModel, reader, parameters, exportLog.ROWID);

                            await uow.ExportWSRepository.InsertTemp(exportData, export);
                            await uow.ExportWSRepository.InsertLog(exportData, export, connection);
                            exportLog.INSERTEDROWS_0 += await uow.ExportWSRepository.InsertMain(exportData, export, connection);
                        }
                    }
                }
            }

            export.LASTREWRITEDATE_0 = initialDate;
            export.NEXTREWRITEDATE_0 = initialDate.AddSeconds(export.REWRITEFREQUENCY_0);

            Result<WebServiceData> result = new Result<WebServiceData>
            {
                Obj = new WebServiceData
                {
                    Document = string.Empty,
                    XML = string.Empty,
                    WebService = export.WebServiceModel
                }
            };

            return (result, export, exportLog, initialDate);
        }

        private async Task<Result<EXPORTWSLOG>> UpdateExportLog(IUnitOfWork uow, EXPORTWS export, Result<WebServiceData> webServiceDataResult, EXPORTWSLOG exportLog, int executionTime)
        {
            Result<EXPORTWSLOG> result = new Result<EXPORTWSLOG>(true);
            EXPORTWSLOG newExportLog = await uow.ExportWSLogRepository.GetByID(exportLog.ROWID);

            if (newExportLog != null)
            {
                newExportLog.WSNAM_0 = export.WebServiceModel.NAM_0;
                newExportLog.OUTPUT_0 = exportLog.OUTPUT_0;
                newExportLog.OUTPUTWS_0 = webServiceDataResult.ToString();
                newExportLog.XML_0 = webServiceDataResult.Obj.XML;
                newExportLog.INSERTEDROWS_0 = exportLog.INSERTEDROWS_0;
                newExportLog.UPDATEDROWS_0 = exportLog.UPDATEDROWS_0;
                newExportLog.DELETEDROWS_0 = exportLog.DELETEDROWS_0;
                newExportLog.EXECUTIONTIME_0 = executionTime;
                newExportLog.SUCCESSFLG_0 = Convert.ToInt32(webServiceDataResult.Success) + 1;

                result = await uow.ExportWSLogRepository.Update(newExportLog);
            }
            else
            {
                result.AddInvalidMessage($"Não foi possível encontrar no sistema o log de exportação para o Id {exportLog.ROWID}");
            }

            return result;
        }
        #endregion
    }
}
