﻿using System.Linq;
using MODELS.Helpers;
using System.Transactions;
using System.Threading.Tasks;
using DATA.Helpers.StaticClasses;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;
using DATA.LayeredArquiteture.Repositories.UnitOfWork;

namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{
    class DocAllBLL : IDocAllBLL
    {
        public USER AuthenticatedUser { get; set; }

        public DocAllBLL()
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();
        }

        public async Task<IEnumerable<DOCALL>> GetAll()
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Snapshot))
            {
                IEnumerable<DOCALL> docAllList = await uow.DocAllRepository.GetAll();

                uow.Commit();

                return docAllList;
            }
        }

        public async Task<IEnumerable<DOCALL>> GetByFields(DOCALL docAll)
        {
            Dictionary<string, object> fields = new Dictionary<string, object>();
            Dictionary<string, object> fieldsTemp = new Dictionary<string, object>
            {
                { nameof(docAll.DOCUMENT_0), docAll.DOCUMENT_0},
                { nameof(docAll.ITMREF_0), docAll.ITMREF_0 },
                { nameof(docAll.LOT_0), docAll.LOT_0 },
                { nameof(docAll.SLO_0), docAll.SLO_0 },
                { nameof(docAll.LOC_0), docAll.LOC_0 },
                { nameof(docAll.STA_0), docAll.STA_0 },
                { nameof(docAll.AVAILFLG_0), docAll.AVAILFLG_0 }
            };

            foreach (KeyValuePair<string, object> field in fieldsTemp)
            {
                if (field.Value != null)
                {
                    fields.Add(field.Key, field.Value);
                }
            }

            if (fields.Count > 0)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
                {
                    IEnumerable<DOCALL> docAllList = await uow.DocAllRepository.GetByFields(fields);

                    uow.Commit();

                    return docAllList;
                }
            }

            return Enumerable.Empty<DOCALL>();
        }

        public async Task<IEnumerable<DOCALL>> GetDocAllAvailable(string DOCUMENT_0)
        {
            Result<DOCALL> result = DOCUMENT_0.IsValueValid<DOCALL>(nameof(DOCALL.DOCUMENT_0));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
                {
                    IEnumerable<DOCALL> docAllList = await uow.DocAllRepository.GetAvailableByDocument(DOCUMENT_0);

                    uow.Commit();

                    return docAllList;
                }
            }

            return Enumerable.Empty<DOCALL>();
        }

        public async Task<IEnumerable<DOCALL>> GetDocItmAllAvailable(string STOFCY_0, string DOCUMENT_0, string ITMREF_0)
        {
            Result<DOCALL> result = DOCUMENT_0.IsValueValid<DOCALL>(nameof(DOCALL.DOCUMENT_0));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
                {
                    IEnumerable<DOCALL> docAllList = await uow.DocAllRepository.GetAvailableByStofcyDocumentItmref(STOFCY_0, DOCUMENT_0, ITMREF_0);

                    uow.Commit();

                    return docAllList;
                }
            }

            return Enumerable.Empty<DOCALL>();
        }

        public async Task<DOCALL> GetByID(decimal id)
        {
            Result<DOCALL> result = id.IsValueValid<DOCALL>(nameof(DOCALL.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
                {
                    DOCALL docAll = await uow.DocAllRepository.GetByID(id);

                    uow.Commit();

                    return docAll;
                }
            }

            return null;
        }

        public async Task<Result<DOCALL>> Insert(DOCALL docAll)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
            {
                Result<DOCALL> result = await IsValidInsert(docAll);

                if (result.Success)
                {
                    docAll.CREUSR_0 = AuthenticatedUser.USR_0;
                    docAll.UPDUSR_0 = AuthenticatedUser.USR_0;

                    result = await uow.DocAllRepository.Insert(docAll);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<DOCALL>> Update(DOCALL docAll)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
            {
                Result<DOCALL> result = await IsValidUpdate(docAll);

                if (result.Success)
                {
                    docAll.UPDUSR_0 = AuthenticatedUser.USR_0;

                    result = await uow.DocAllRepository.Update(docAll);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<DOCALL>> IsValidInsert(DOCALL docAll)
        {
            // Add more validations if needed
            Result<DOCALL> result = await docAll.IsClassValidAsync();

            return result;
        }

        public async Task<Result<DOCALL>> IsValidUpdate(DOCALL docAll)
        {
            // Add more validations if needed
            Result<DOCALL> result = await docAll.IsClassValidAsync();

            return result;
        }
    }
}
