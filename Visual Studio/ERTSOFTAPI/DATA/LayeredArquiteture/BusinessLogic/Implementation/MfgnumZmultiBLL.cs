﻿using MODELS.Helpers;
using System.Transactions;
using System.Threading.Tasks;
using DATA.Helpers.StaticClasses;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.Repositories.UnitOfWork;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;

namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{
    public class MfgnumZmultiBLL : IMfgnumZmultiBLL
    {
        public USER AuthenticatedUser { get; set; }

        public MfgnumZmultiBLL()
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();
        }

        public async Task<IEnumerable<MFGNUMZMULTI>> GetAll()
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
            {
                IEnumerable<MFGNUMZMULTI> mfgnumZmultiList = await uow.MfgnumZmultiRepository.GetAll();

                uow.Commit();

                return mfgnumZmultiList;
            }
        }

        public async Task<IEnumerable<MFGNUMZMULTI>> GetByMfgnumori(string MFGNUMORI_0)
        {
            Result<MFGNUMZMULTI> result = MFGNUMORI_0.IsValueValid<MFGNUMZMULTI>(nameof(MFGNUMZMULTI.MFGNUMORI_0));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                {
                    IEnumerable<MFGNUMZMULTI> mfgnumZmultiList = await uow.MfgnumZmultiRepository.GetByField(nameof(MFGNUMZMULTI.MFGNUMORI_0), MFGNUMORI_0);

                    uow.Commit();

                    return mfgnumZmultiList;
                }
            }

            return null;
        }

        public async Task<IEnumerable<MFGNUMZMULTI>> GetByMfgnum(string MFGNUM_0)
        {
            Result<MFGNUMZMULTI> result = MFGNUM_0.IsValueValid<MFGNUMZMULTI>(nameof(MFGNUMZMULTI.MFGNUM_0));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                {
                    IEnumerable<MFGNUMZMULTI> mfgnumZmultiList = await uow.MfgnumZmultiRepository.GetByField(nameof(MFGNUMZMULTI.MFGNUM_0), MFGNUM_0);

                    uow.Commit();

                    return mfgnumZmultiList;
                }
            }

            return null;
        }

        public async Task<MFGNUMZMULTI> GetByID(decimal id)
        {
            Result<MFGNUMZMULTI> result = id.IsValueValid<MFGNUMZMULTI>(nameof(MFGNUMZMULTI.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                {
                    MFGNUMZMULTI mfgnumZmulti = await uow.MfgnumZmultiRepository.GetByID(id);

                    uow.Commit();

                    return mfgnumZmulti;
                }
            }

            return null;
        }

        public async Task<MFGNUMZMULTI> GetByUniqueFields(string MFGNUMORI_0, string MFGNUM_0)
        {
            MFGNUMZMULTI mfgnumZmultiModel = new MFGNUMZMULTI
            {
                MFGNUMORI_0 = MFGNUMORI_0,
                MFGNUM_0 = MFGNUM_0
            };
            Result<MFGNUMZMULTI> result = mfgnumZmultiModel.IsPropertiesValid(nameof(MFGNUMZMULTI.MFGNUMORI_0), nameof(MFGNUMZMULTI.MFGNUM_0));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
                {
                    Dictionary<string, object> fields = new Dictionary<string, object>
                    {
                        { nameof(MFGNUMZMULTI.MFGNUMORI_0), MFGNUMORI_0 },
                        { nameof(MFGNUMZMULTI.MFGNUM_0), MFGNUM_0}
                    };

                    mfgnumZmultiModel = await uow.MfgnumZmultiRepository.GetByUniqueFields(fields);

                    uow.Commit();

                    return mfgnumZmultiModel;
                }
            }

            return null;
        }

        public async Task<Result<MFGNUMZMULTI>> Insert(MFGNUMZMULTI mfgnumZmulti)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
            {
                Result<MFGNUMZMULTI> result = await IsValidInsert(mfgnumZmulti);

                if (result.Success)
                {
                    mfgnumZmulti.CREUSR_0 = AuthenticatedUser.USR_0;
                    mfgnumZmulti.UPDUSR_0 = AuthenticatedUser.USR_0;

                    result = await uow.MfgnumZmultiRepository.Insert(mfgnumZmulti);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<MFGNUMZMULTI>> Update(MFGNUMZMULTI mfgnumZmulti)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
            {
                Result<MFGNUMZMULTI> result = await IsValidUpdate(mfgnumZmulti);

                if (result.Success)
                {
                    mfgnumZmulti.UPDUSR_0 = AuthenticatedUser.USR_0;

                    result = await uow.MfgnumZmultiRepository.Update(mfgnumZmulti);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<MFGNUMZMULTI>> IsValidInsert(MFGNUMZMULTI mfgnumZmulti)
        {
            Result<MFGNUMZMULTI> result = await mfgnumZmulti.IsClassValidAsync(true);

            if (result.Success)
            {
                MFGNUMZMULTI mfgnumZmultiTemp = await GetByUniqueFields(mfgnumZmulti.MFGNUMORI_0, mfgnumZmulti.MFGNUM_0);

                if (mfgnumZmultiTemp != null)
                {
                    result.AddInvalidMessage($"Já existe um registo com a OF original {mfgnumZmulti.MFGNUMORI_0}");
                }
            }

            return result;
        }

        public async Task<Result<MFGNUMZMULTI>> IsValidUpdate(MFGNUMZMULTI mfgnumZmulti)
        {
            // Add more validations if needed
            Result<MFGNUMZMULTI> result = await mfgnumZmulti.IsClassValidAsync();
   
            return result;
        }
    }
}
