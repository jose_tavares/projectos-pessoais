﻿using MODELS.Helpers;
using System.Transactions;
using System.Threading.Tasks;
using DATA.Helpers.StaticClasses;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;
using System.Linq;
using System;
using DATA.Helpers.AuxiliarClasses;
using DATA.LayeredArquiteture.Repositories.UnitOfWork;

namespace DATA.LayeredArquiteture.BusinessLogic.Implementation
{
    public class ImportWSBLL : IImportWSBLL
    {
        #region Private Variables
        private readonly IWebServiceBLL webServiceBLL;
        private readonly IWSParamBLL wsParamBLL;
        #endregion

        public USER AuthenticatedUser { get; set; }

        public ImportWSBLL(IWebServiceBLL webServiceBLL, IWSParamBLL wsParamBLL)
        {
            AuthenticatedUser = IdentityHelper.GetAuthenticatedUser();

            this.webServiceBLL = webServiceBLL;
            this.wsParamBLL = wsParamBLL;
        }

        public async Task<IEnumerable<IMPORTWS>> GetAll()
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
            {
                IEnumerable<IMPORTWS> importWSList = await uow.ImportWSRepository.GetAll();

                uow.Commit();

                return importWSList;
            }
        }

        public async Task<IMPORTWS> GetByID(decimal id)
        {
            Result<IMPORTWS> result = id.IsValueValid<IMPORTWS>(nameof(IMPORTWS.ROWID));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                {
                    IMPORTWS importWS = await uow.ImportWSRepository.GetByID(id);

                    uow.Commit();

                    return importWS;
                }
            }

            return null;
        }

        public async Task<dynamic> GetDynamicById(string TABLE_0, IEnumerable<string> fields, decimal id)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable))
            {
                dynamic import = await uow.ImportWSRepository.GetDynamicById(id, TABLE_0, fields);

                uow.Commit();

                return import;
            }
        }

        public async Task<IMPORTWS> GetByImportWS(string IMPORTWS_0)
        {
            Result<IMPORTWS> result = IMPORTWS_0.IsValueValid<IMPORTWS>(nameof(IMPORTWS.IMPORTWS_0));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                {
                    IMPORTWS importWS = await uow.ImportWSRepository.GetByUniqueField(nameof(IMPORTWS.IMPORTWS_0), IMPORTWS_0);

                    uow.Commit();

                    return importWS;
                }
            }

            return null;
        }

        public async Task<IMPORTWS> GetByWsCode(string WSCODE_0)
        {
            Result<IMPORTWS> result = WSCODE_0.IsValueValid<IMPORTWS>(nameof(IMPORTWS.WSCODE_0));

            if (result.Success)
            {
                using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
                {
                    IMPORTWS importWS = await uow.ImportWSRepository.GetByUniqueField(nameof(IMPORTWS.WSCODE_0), WSCODE_0);

                    uow.Commit();

                    return importWS;
                }
            }

            return null;
        }

        public async Task<dynamic> GetFirstPendingByWsCode(string WEBSERVICE_0)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
            {
                IMPORTWS importWS = await GetByWsCode(WEBSERVICE_0);

                if (importWS == null)
                {
                    return null;
                }

                WEBSERVICE webService = await webServiceBLL.GetByUniqueKey(WEBSERVICE_0);

                if (webService != null)
                {
                    webService.WSParamList = await wsParamBLL.GetByWsCode(WEBSERVICE_0);
                }

                dynamic importData = await uow.ImportWSRepository.GetFirstPendingByImportWsAndFields(importWS, importWS.webService.WSParamList.Where(w => w.TEXT_0 == 1).Select(w => w.VALUE_0).ToList());

                uow.Commit();

                return importData;
            }
        }

        public async Task<dynamic> GetFirstPendingByImportWsAndFields(IMPORTWS importWs, IEnumerable<string> fields)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Unspecified, TransactionScopeOption.Suppress))
            {
                dynamic importData = await uow.ImportWSRepository.GetFirstPendingByImportWsAndFields(importWs, fields);

                uow.Commit();

                return importData;
            }
        }

        #region ImportWebService
        private IEnumerable<string> GetSQLFields(IEnumerable<WSPARAM> wsParamList)
        {
            List<string> fields = new List<string>();

            foreach (WSPARAM wsParam in wsParamList)
            {
                if (wsParam.TEXT_0 == 1)
                {
                    fields.Add(wsParam.VALUE_0);
                }
            }

            return fields;
        }

        private async Task<Result<WEBSERVICE>> IsImportWSValid(string WEBSERVICE_0)
        {
            Result<WEBSERVICE> result = WEBSERVICE_0.IsValueValid<WEBSERVICE>(nameof(WEBSERVICE.CODE_0));

            if (result.Success)
            {
                WEBSERVICE webService = await webServiceBLL.GetByUniqueKey(WEBSERVICE_0);

                if (webService != null)
                {
                    if (webService.IMPORTFLG_0 == 2)
                    {
                        webService.WSParamList = await wsParamBLL.GetByWsCode(WEBSERVICE_0);

                        if (!webService.WSParamList.IsValid())
                        {
                            result.AddInvalidMessage($"Não existem parametros configurados para o webservice '{WEBSERVICE_0}'");
                        }
                    }
                    else
                    {
                        result.AddInvalidMessage($"O webservice '{WEBSERVICE_0}' não se encontra definido como webservice de importação de dados.");
                    }
                }
                else
                {
                    result.AddInvalidMessage($"Não foi possível obter informação para o webservice '{WEBSERVICE_0}'");
                }

                result.Obj = webService;
            }

            return result;
        }

        private async Task<(Result<dynamic> resultImport, WEBSERVICE webService)> IsValidImport(string WEBSERVICE_0)
        {
            Result<WEBSERVICE> resultWebService = await IsImportWSValid(WEBSERVICE_0);
            Result<dynamic> resultImportPending = Result<dynamic>.ConvertAndGetFromResult(resultWebService);

            if (resultImportPending.Success)
            {
                IMPORTWS importWs = await GetByWsCode(WEBSERVICE_0);
                IEnumerable<string> sqlFields = GetSQLFields(resultWebService.Obj.WSParamList);
                dynamic importPending = await GetFirstPendingByImportWsAndFields(importWs, sqlFields);

                if (importPending != null)
                {
                    resultImportPending.Obj = await GetDynamicById(importWs.SQLTABLE_0, sqlFields, importPending.ROWID);

                    Random rnd = new Random();
                    System.Threading.Thread.Sleep(rnd.Next(3, 15));

                    if (resultImportPending.Obj != null)
                    {
                        if (resultImportPending.Obj.SUCCESSFLG_0 == 2)
                        {
                            resultImportPending.AddInvalidMessage($"{importWs.SQLTABLE_0} já se encontra processada. Possivelmente existem pedidos em paralelo a correr e um anteriormente já integrou a linha com o ID {resultImportPending.Obj.ROWID}");
                        }
                        else
                        {

                        }
                    }
                    else
                    {
                        resultImportPending.AddInvalidMessage($"Não foi possível obter informação da {importWs.SQLTABLE_0} para o ID {resultImportPending.Obj.ROWID}");
                    }
                }
                else
                {
                    resultImportPending.AddInvalidMessage($"Nenhum registo pendente integração para o webservice {WEBSERVICE_0}");
                }

            }

            return (resultImportPending, resultWebService.Obj);
        }
        #endregion

        public async Task<Result<IMPORTWS>> ImportFirstPendingByWebService(string WEBSERVICE_0)
        {
            Result<IMPORTWS> resultImport = new Result<IMPORTWS>();

            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.Serializable, TransactionScopeOption.RequiresNew, TimeSpan.FromMinutes(5)))
            {
                (Result<dynamic> resultImport, WEBSERVICE webService) data = await IsValidImport(WEBSERVICE_0);
                resultImport.ConvertFromResult(data.resultImport);

                if (data.resultImport.Success)
                {
                    dynamic import = data.resultImport.Obj;

                    if (import.SUCCESSFLG_0 == 1)
                    {
                        string XML = WebServiceHelper.GetXMLByDynamic(import, data.webService.WSParamList);

                        Result<WebServiceData> webServiceDataResult = await WebServiceHelper.RunWebServiceAsync(data.webService, XML);
                        resultImport.ConvertFromResult(webServiceDataResult);
                        resultImport.AddMessage($"Linha {import.ROWID + (resultImport.Success ? string.Empty : " não")} foi processada com sucesso.");

                        Result<IMPORTWS> resultWebServiceData = await ProcessWebServiceData(webServiceDataResult, import);
                        resultImport.Success = resultWebServiceData.Success;

                        if (!resultImport.Success)
                        {
                            resultImport.Messages.AddRange(resultWebServiceData.Messages);
                        }

                        resultImport.Obj = import;
                    }
                    else
                    {
                        //await UpdateImportStojou(import);
                    }

                    if (data.resultImport.Success)
                    {
                        uow.Commit();
                    }
                }

                return resultImport;
            }
        }

        public async Task<Result<IMPORTWS>> Insert(IMPORTWS importWS)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
            {
                Result<IMPORTWS> result = await IsValidInsert(importWS);

                if (result.Success)
                {
                    importWS.CREUSR_0 = AuthenticatedUser.USR_0;
                    importWS.UPDUSR_0 = AuthenticatedUser.USR_0;

                    result = await uow.ImportWSRepository.Insert(importWS);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<IMPORTWS>> Update(IMPORTWS ImportWS)
        {
            using (IUnitOfWork uow = NinjectHelper.UowInstance(TransactionScopeAsyncFlowOption.Enabled, IsolationLevel.ReadCommitted))
            {
                Result<IMPORTWS> result = await IsValidUpdate(ImportWS);

                if (result.Success)
                {
                    ImportWS.UPDUSR_0 = AuthenticatedUser.USR_0;

                    result = await uow.ImportWSRepository.Update(ImportWS);

                    if (result.Success)
                    {
                        uow.Commit();
                    }
                }

                return result;
            }
        }

        public async Task<Result<IMPORTWS>> IsValidInsert(IMPORTWS ImportWS)
        {
            Result<IMPORTWS> result = await ImportWS.IsClassValidAsync(true);

            return result;
        }

        public async Task<Result<IMPORTWS>> IsValidUpdate(IMPORTWS ImportWS)
        {
            Result<IMPORTWS> result = await ImportWS.IsClassValidAsync(true);

            return result;
        }




        private async Task<Result<IMPORTWS>> ProcessWebServiceData(Result<WebServiceData> webServiceDataResult, dynamic dataRow)
        {
            Result<IMPORTWS> resultStojou = new Result<IMPORTWS>();

            if (webServiceDataResult != null && dataRow != null)
            {
                //UpdateByWebServiceResult(stojou, webServiceDataResult);

                if (!webServiceDataResult.Success) //Se falhou a importação com o Sage verificar o stock e atualizar os campos
                {
                    //STOCK stock = new STOCK
                    //{
                    //    STOFCY_0 = dataRow.STOFCY_0,
                    //    ITMREF_0 = dataRow.ITMREF_0,
                    //    LOT_0 = dataRow.LOT_0,
                    //    SLO_0 = dataRow.SLO_0,
                    //    LOC_0 = dataRow.USELOC_0,
                    //    STA_0 = dataRow.USESTA_0,
                    //    PCU_0 = dataRow.USESTU_0
                    //};

                    //IEnumerable<STOCK> stockList = await stockBLL.GetByFields(stock);

                    //UpdateByStock(stojou, stockList);
                }

                //resultStojou = await UpdateImportStojou(stojou);
            }
            else
            {
                resultStojou.AddInvalidMessage("Parametros webServiceDataResult e stojou encontram-se null.");
            }

            return resultStojou;
        }

        //private async Task<Result<STOJOU>> UpdateImportStojou(STOJOU stojou)
        //{
        //    Result<STOJOU> resultStojou = await Update(stojou);

        //    if (resultStojou.Success)
        //    {
        //        string jsonStojou = JsonConvert.SerializeObject(stojou);
        //        STOJOULOG stojouLog = JsonConvert.DeserializeObject<STOJOULOG>(jsonStojou);
        //        stojouLog.STOJOUID = stojou.ROWID;

        //        Result<STOJOULOG> resultStojouLog = await stojouLogBLL.Insert(stojouLog);
        //        resultStojou.ConvertFromResult(resultStojouLog);
        //    }

        //    return resultStojou;
        //}

        private void UpdateByWebServiceResult(STOJOU stojou, Result<WebServiceData> result)
        {
            if (stojou != null)
            {
                WebServiceData webServiceData = result.Obj;

                stojou.SUCCESSFLG_0 = Convert.ToInt32(result.Success) + 1;
                stojou.ATTEMPTS_0 += 1;
                stojou.XML_0 = webServiceData.XML;
                stojou.OUTPUTWS_0 = webServiceData.OUTPUTWS_0;
                stojou.VCRNUM_0 = result.Obj.Document;
            }
        }
    }
}
