﻿using System;
using System.Transactions;
using DATA.Helpers.StaticClasses;
using DATA.LayeredArquiteture.Repositories.Interfaces.Concrete;

namespace DATA.LayeredArquiteture.Repositories.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private bool disposed;
        private TransactionScope transactionScope;

        public UnitOfWork(TransactionScopeAsyncFlowOption? tsaf, IsolationLevel iso, TransactionScopeOption tso, TimeSpan timeout = default)
        {
            transactionScope = tsaf.HasValue ? DBHelper.NewTransaction(tsaf.Value, iso, tso, timeout) : DBHelper.NewTransaction(iso, tso, timeout);
        }

        ~UnitOfWork()
        {
            Dispose(false);
        }

        #region ERTSOFTAPI

        public IApiRequestRepository ApiRequestRepository => NinjectHelper.RepoInstance<IApiRequestRepository>("ERTSOFTAPI");

        public IAppRepository AppRepository => NinjectHelper.RepoInstance<IAppRepository>("ERTSOFTAPI");

        public IConfigValueRepository ConfigValueRepository => NinjectHelper.RepoInstance<IConfigValueRepository>("ERTSOFTAPI");

        public IConsumptionRepository ConsumptionRepository => NinjectHelper.RepoInstance<IConsumptionRepository>("ERTSOFTAPI");

        public IDocAllRepository DocAllRepository => NinjectHelper.RepoInstance<IDocAllRepository>("ERTSOFTAPI");

        public IImportWSRepository ImportWSRepository => NinjectHelper.RepoInstance<IImportWSRepository>("ERTSOFTAPI");

        public IImportWSDataRepository ImportWSDataRepository => NinjectHelper.RepoInstance<IImportWSDataRepository>("ERTSOFTAPI");

        public IImportWSLogRepository ImportWSLogRepository => NinjectHelper.RepoInstance<IImportWSLogRepository>("ERTSOFTAPI");

        public IExportWSRepository ExportWSRepository => NinjectHelper.RepoInstance<IExportWSRepository>("ERTSOFT");

        public IExportWSLogRepository ExportWSLogRepository => NinjectHelper.RepoInstance<IExportWSLogRepository>("ERTSOFT");

        public IItmLockRepository ItmLockRepository => NinjectHelper.RepoInstance<IItmLockRepository>("ERTSOFTAPI");

        public IModuleRepository ModuleRepository => NinjectHelper.RepoInstance<IModuleRepository>("ERTSOFTAPI");

        public IProductionRepository ProductionRepository => NinjectHelper.RepoInstance<IProductionRepository>("ERTSOFTAPI");

        public IProfileRepository ProfileRepository => NinjectHelper.RepoInstance<IProfileRepository>("ERTSOFTAPI");

        public IStoDefectRepository StoDefectRepository => NinjectHelper.RepoInstance<IStoDefectRepository>("ERTSOFTAPI");

        public IStojouRepository StojouRepository => NinjectHelper.RepoInstance<IStojouRepository>("ERTSOFTAPI");

        public IStojouLogRepository StojouLogRepository => NinjectHelper.RepoInstance<IStojouLogRepository>("ERTSOFTAPI");

        public IUserRepository UserRepository => NinjectHelper.RepoInstance<IUserRepository>("ERTSOFTAPI");

        public IWebServiceRepository WebServiceRepository => NinjectHelper.RepoInstance<IWebServiceRepository>("ERTSOFTAPI");

        public IWsFieldConfigRepository WsFieldConfigRepository => NinjectHelper.RepoInstance<IWsFieldConfigRepository>("ERTSOFTAPI");

        public IMfgnumZmultiRepository MfgnumZmultiRepository => NinjectHelper.RepoInstance<IMfgnumZmultiRepository>("ERTSOFTAPI");

        #endregion

        #region SAGE
        public IFacilityRepository FacilityRepository => NinjectHelper.RepoInstance<IFacilityRepository>("SAGE");

        public IItmMasterRepository ItmMasterRepository => NinjectHelper.RepoInstance<IItmMasterRepository>("SAGE");

        public IItmFacilitRepository ItmFacilitRepository => NinjectHelper.RepoInstance<IItmFacilitRepository>("SAGE");

        public IMfgHeadRepository MfgHeadRepository => NinjectHelper.RepoInstance<IMfgHeadRepository>("SAGE");

        public IMfgItmRepository MfgItmRepository => NinjectHelper.RepoInstance<IMfgItmRepository>("SAGE");

        public IMfgMatRepository MfgMatRepository => NinjectHelper.RepoInstance<IMfgMatRepository>("SAGE");

        public IStolotRepository StolotRepository => NinjectHelper.RepoInstance<IStolotRepository>("SAGE");

        public IStockRepository StockRepository => NinjectHelper.RepoInstance<IStockRepository>("SAGE");

        public IStolocRepository StolocRepository => NinjectHelper.RepoInstance<IStolocRepository>("SAGE");

        public ITabstastoRepository TabstastoRepository => NinjectHelper.RepoInstance<ITabstastoRepository>("SAGE");

        public ITabUnitRepository TabUnitRepository => NinjectHelper.RepoInstance<ITabUnitRepository>("SAGE");

        public IAtabdivRepository AtabdivRepository => NinjectHelper.RepoInstance<IAtabdivRepository>("SAGE");

        public IAtextraRepository AtextraRepository => NinjectHelper.RepoInstance<IAtextraRepository>("SAGE");

        public IMfgItmTrkRepository MfgItmTrkRepository => NinjectHelper.RepoInstance<IMfgItmTrkRepository>("SAGE");

        public IMfgMatTrkRepository MfgMatTrkRepository => NinjectHelper.RepoInstance<IMfgMatTrkRepository>("SAGE");

        #endregion

        public void Commit()
        {
            try
            {
                if (transactionScope != null)
                {
                    transactionScope.Complete();
                }
            }
            catch
            {
                throw;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    if (transactionScope != null)
                    {
                        transactionScope.Dispose();
                        transactionScope = null;
                    }
                }

                disposed = true;
            }
        }
    }
}
