﻿using System;
using DATA.LayeredArquiteture.Repositories.Interfaces.Concrete;

namespace DATA.LayeredArquiteture.Repositories.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        #region ERTSOFTAPI
        IApiRequestRepository ApiRequestRepository { get; }

        IAppRepository AppRepository { get; }

        IConfigValueRepository ConfigValueRepository { get; }

        IConsumptionRepository ConsumptionRepository { get; }

        IDocAllRepository DocAllRepository { get; }

        IImportWSRepository ImportWSRepository { get; }

        IImportWSDataRepository ImportWSDataRepository { get; }

        IImportWSLogRepository ImportWSLogRepository { get; }

        IExportWSRepository ExportWSRepository { get; }

        IExportWSLogRepository ExportWSLogRepository { get; }

        IItmLockRepository ItmLockRepository { get; }

        IModuleRepository ModuleRepository { get; }

        IProductionRepository ProductionRepository { get; }

        IProfileRepository ProfileRepository { get; }

        IStoDefectRepository StoDefectRepository { get; }

        IStojouRepository StojouRepository { get; }

        IStojouLogRepository StojouLogRepository { get; }

        IUserRepository UserRepository { get; }

        IWebServiceRepository WebServiceRepository { get; }

        IWsFieldConfigRepository WsFieldConfigRepository { get; }

        IMfgnumZmultiRepository MfgnumZmultiRepository { get; }

        #endregion

        #region SAGE
        IFacilityRepository FacilityRepository { get; }

        IItmMasterRepository ItmMasterRepository { get; }

        IItmFacilitRepository ItmFacilitRepository { get; }

        IMfgHeadRepository MfgHeadRepository { get; }

        IMfgItmRepository MfgItmRepository { get; }

        IMfgMatRepository MfgMatRepository { get; }

        IStolotRepository StolotRepository { get; }

        IStockRepository StockRepository { get; }

        IStolocRepository StolocRepository { get; }

        ITabstastoRepository TabstastoRepository { get; }

        ITabUnitRepository TabUnitRepository { get; }

        IAtabdivRepository AtabdivRepository { get; }

        IAtextraRepository AtextraRepository { get; }

        IMfgItmTrkRepository MfgItmTrkRepository { get; }

        IMfgMatTrkRepository MfgMatTrkRepository { get; }
        #endregion

        void Commit();
    }
}
