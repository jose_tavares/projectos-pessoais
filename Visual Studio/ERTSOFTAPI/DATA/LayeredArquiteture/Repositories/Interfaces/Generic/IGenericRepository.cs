﻿using MODELS.Helpers;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace DATA.LayeredArquiteture.Repositories.Interfaces.Generic
{
    public interface IGenericRepository<T> where T : class
    {
        /// <summary>
        /// The name of the DB connection which will be used to identify the connectionstring on App Settings configuration file.
        /// </summary>
        string ConnName { get; }

        /// <summary>
        /// Gets all the data.
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<T>> GetAll();

        /// <summary>
        /// Gets data filtered by ROWID_0.
        /// </summary>
        /// <returns></returns>
        Task<T> GetById(int id);

        /// <summary>
        /// Gets one element or none filtered by any unique field that matches table definition column name with unique constraint.
        /// Throws exception if more than one element is found.
        /// </summary>
        /// <param name="field"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        Task<T> GetByUniqueField(string field, object value);

        /// <summary>
        /// Gets one element or none filtered by one or multiple unique fields that matches table definition column names with unique constraints.
        /// Throws exception if more than one element is found.
        /// </summary>
        /// <param name="field"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        Task<T> GetByUniqueFields(Dictionary<string, object> fields);

        /// <summary>
        /// Gets all elements filtered by any field that matches table definition column names.
        /// </summary>
        /// <param name="field"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        Task<IEnumerable<T>> GetByField(string field, object value);

        /// <summary>
        /// Gets all elements filtered by multiple fields that matches table definition column names.
        /// </summary>
        /// <param name="field"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        Task<IEnumerable<T>> GetByFields(Dictionary<string, object> fields);

        /// <summary>
        /// Inserts one element by matching properties name to columns name.
        /// If excludeSqlDefaultValue is true, it only inserts values in columns that don't contain DefaultValue constraint.
        /// If excludeSqlDefaultValue is false, it inserts all values in columns.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="excludeSqlDefaultValue"></param>
        /// <returns>DataResultT<T> with generated ID assigned to InsertedId property.</returns>
        Task<Result<T>> Insert(T model);

        /// <summary>
        /// Updates one element by matching properties name to columns name.
        /// It garantuees automatically that UPDDAT_0 and UPDDATTIM_0 field will always be updated to the current date.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<Result<T>> Update(T model);

        /// <summary>
        /// Deletes one element by ROWID_0.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<Result<T>> Delete(int id);
    }
}