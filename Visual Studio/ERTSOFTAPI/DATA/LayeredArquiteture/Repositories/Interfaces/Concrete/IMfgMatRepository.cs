﻿using System.Threading.Tasks;
using System.Collections.Generic;
using DATA.LayeredArquiteture.Repositories.Interfaces.Generic;
using MODELS.BusinessObject.SAGEX3.Database;

namespace DATA.LayeredArquiteture.Repositories.Interfaces.Concrete
{
    public interface IMfgMatRepository : IGenericRepository<MFGMAT>
    {
        /// <summary>
        /// Gets List of MFGMAT filtered by MFGNUM_0.
        /// Conditions applied are ITMMASTER.STOMGTCOD_0 <> 1 and MATSTA_0 < 4
        /// </summary>
        /// <param name="MFGNUM_0"></param>
        /// <returns></returns>
        Task<IEnumerable<MFGMAT>> GetByMfgnum(string MFGNUM_0);

        /// <summary>
        /// Gets List of MFGMAT filtered by MFGNUM_0, ZOPERCON_0 and (ZCONSPRDOK_0 or ZCONSPRDNOK_0)
        /// Conditions applied are ITMMASTER.STOMGTCOD_0 <> 1 and MATSTA_0 < 4.
        /// Take in consideration that if PRODOK_0 parameter is true, it will filter ZCONSPRDOK_0 = 2, otherwise ZCONSPRDNOK_0 = 2
        /// </summary>
        /// <param name="MFGNUM_0"></param>
        /// <param name="ZOPERCON_0"></param>
        /// <param name="PRODOK_0"></param>
        /// <returns></returns>
        Task<IEnumerable<MFGMAT>> GetByMfgnumAndOperationAndProdType(string MFGNUM_0, string ZOPERCON_0, bool PRODOK_0);
    }
}