﻿using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.Repositories.Interfaces.Generic;

namespace DATA.LayeredArquiteture.Repositories.Interfaces.Concrete
{
    public interface IConfigValueRepository : IGenericRepository<CONFIGVALUE>
    {

    }
}
