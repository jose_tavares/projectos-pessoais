﻿using System.Threading.Tasks;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.Repositories.Interfaces.Generic;

namespace DATA.LayeredArquiteture.Repositories.Interfaces.Concrete
{
    public interface IModuleRepository : IGenericRepository<MODULE>
    {
        /// <summary>
        /// Gets MODULE or null filtered by APP_0 and MODULE_0 with an ScreenList associated to the MODULE.
        /// If more than one MODULE is found it throws an exception.
        /// </summary>
        /// <param name="APP_0"></param>
        /// <param name="MODULE_0"></param>
        /// <returns></returns>
        Task<MODULE> GetByUniqueFieldsWithScreenList(string APP_0, string MODULE_0);
    }
}
