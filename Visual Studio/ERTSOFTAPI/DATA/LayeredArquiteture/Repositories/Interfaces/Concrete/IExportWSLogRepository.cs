﻿using DATA.LayeredArquiteture.Repositories.Interfaces.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;

namespace DATA.LayeredArquiteture.Repositories.Interfaces.Concrete
{
    public interface IExportWSLogRepository : IGenericRepository<EXPORTWSLOG>
    {

    }
}
