﻿using System.Threading.Tasks;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.Repositories.Interfaces.Generic;

namespace DATA.LayeredArquiteture.Repositories.Interfaces.Concrete
{
    public interface IImportWSDataRepository : IGenericRepository<IMPORTWSDATA>
    {
        Task<IMPORTWSDATA> GetFirstPendingByWsCode(string WSCODE_0, int MAXATTEMPTS_0);
    }
}
