﻿using System.Threading.Tasks;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.Repositories.Interfaces.Generic;

namespace DATA.LayeredArquiteture.Repositories.Interfaces.Concrete
{
    public interface IImportWSRepository : IGenericRepository<IMPORTWS>
    {
        Task<dynamic> GetDynamicById(int id, string SQLTABLE_0, IEnumerable<string> fields);
    }
}
