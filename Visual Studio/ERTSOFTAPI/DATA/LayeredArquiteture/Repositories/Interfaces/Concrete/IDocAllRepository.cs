﻿using System.Threading.Tasks;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.Repositories.Interfaces.Generic;

namespace DATA.LayeredArquiteture.Repositories.Interfaces.Concrete
{
    public interface IDocAllRepository : IGenericRepository<DOCALL>
    {
        /// <summary>
        /// Gets the stock allocated filtered by DOCUMENT_0 ordered by UPDDATTIM_0.
        /// Only returns data with QTYPCU_0 &gt; 0 and USEFLG_0 = 2.
        /// </summary>
        /// <param name="DOCUMENT_0"></param>
        /// <returns></returns>
        Task<IEnumerable<DOCALL>> GetAvailableByDocument(string DOCUMENT_0);

        /// <summary>
        /// Gets the stock allocated filtered by STOFCY_0, DOCUMENT_0 and ITMREF_0 ordered by UPDDATTIM_0.
        /// Only returns data with QTYPCU_0 &gt; 0 and USEFLG_0 = 2.
        /// </summary>
        /// <param name="STOFCY_0"></param>
        /// <param name="DOCUMENT_0"></param>
        /// <param name="ITMREF_0"></param>
        /// <returns></returns>
        Task<IEnumerable<DOCALL>> GetAvailableByStofcyDocumentItmref(string STOFCY_0, string DOCUMENT_0, string ITMREF_0);
    }
}
