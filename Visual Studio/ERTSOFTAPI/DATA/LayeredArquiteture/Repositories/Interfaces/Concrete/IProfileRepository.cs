﻿using System.Threading.Tasks;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.Repositories.Interfaces.Generic;

namespace DATA.LayeredArquiteture.Repositories.Interfaces.Concrete
{
    public interface IProfileRepository : IGenericRepository<PROFILE>
    {
        /// <summary>
        /// Gets PROFILE or null filtered by PROFILE_0 with an UserList associated to the PROFILE.
        /// If more than one PROFILE is found it throws an exception.
        /// </summary>
        /// <param name="PROFILE_0"></param>
        /// <returns></returns>
        Task<PROFILE> GetByUniqueFieldWithUserList(string PROFILE_0);
    }
}