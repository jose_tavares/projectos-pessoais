﻿using System.Threading.Tasks;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.Repositories.Interfaces.Generic;

namespace DATA.LayeredArquiteture.Repositories.Interfaces.Concrete
{
    public interface IStojouRepository : IGenericRepository<STOJOU>
    {
        /// <summary>
        /// Gets List of STOJOU that is pending for import in Sage.
        /// Rules: STOJOU.WSCODE_0 = WEBSERVICE.CODE_0, WEBSERVICE.ENABFLG_0 = 2, WEBSERVICE.IMPORTFLG_0 = 2, STOJOU.ATTEMPTS_0 &lt; WEBSERVICE.ATTEMPTS_0, IMPORTFLG_0 = 2,
        /// SUCCESSFLG_0 = 1, REGFLG_0 = 1, FAILFLG_0 = 1
        /// Orders data by ATTEMPTS_0, CREDATTIM_0
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<STOJOU>> GetPending();

        /// <summary>
        /// Gets one STOJOU or null that is pending for import in Sage.
        /// Rules: STOJOU.WSCODE_0 = WEBSERVICE.CODE_0, WEBSERVICE.ENABFLG_0 = 2, WEBSERVICE.IMPORTFLG_0 = 2, STOJOU.ATTEMPTS_0 &lt; WEBSERVICE.ATTEMPTS_0, IMPORTFLG_0 = 2,
        /// SUCCESSFLG_0 = 1, REGFLG_0 = 1, FAILFLG_0 = 1
        /// Orders data by ATTEMPTS_0, CREDATTIM_0
        /// </summary>
        /// <param name="WEBSERVICE_0"></param>
        /// <returns></returns>
        Task<STOJOU> GetFirstPendingByWebService(string WEBSERVICE_0);

        /// <summary>
        /// Gets one STOJOU or null with WebServiceModel filled filtered by ROWID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<STOJOU> GetByIdWithWebService(int id);

        /// <summary>
        /// Gets one STOJOU or null with StojouLogList associated to STOJOU filtered by ROWID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<STOJOU> GetByIdWithStojouLogList(int id);

        /// <summary>
        /// Gets MAX(SLO_0) + 1 filtered by MFGNUM_0
        /// Rules: Formats always SLO_0 to 5 digits, WSCODE_0 = PRODUCTION
        /// </summary>
        /// <param name="MFGNUM_0"></param>
        /// <returns></returns>
        Task<string> GetNextProductionSlo(string MFGNUM_0);
    }
}
