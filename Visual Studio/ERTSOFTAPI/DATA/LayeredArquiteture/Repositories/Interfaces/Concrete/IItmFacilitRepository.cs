﻿using DATA.LayeredArquiteture.Repositories.Interfaces.Generic;
using MODELS.BusinessObject.SAGEX3.Database;

namespace DATA.LayeredArquiteture.Repositories.Interfaces.Concrete
{
    public interface IItmFacilitRepository : IGenericRepository<ITMFACILIT>
    {

    }
}
