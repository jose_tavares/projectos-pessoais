﻿using System.Threading.Tasks;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.Repositories.Interfaces.Generic;

namespace DATA.LayeredArquiteture.Repositories.Interfaces.Concrete
{
    public interface IAppRepository : IGenericRepository<APP>
    {
        /// <summary>
        /// Gets APP or null filtered by APP unique key with an ModuleList associated to the APP.
        /// </summary>
        /// <param name="APP_0"></param>
        /// <returns></returns>
        Task<APP> GetByUniqueFieldWithModuleList(string APP_0);
    }
}
