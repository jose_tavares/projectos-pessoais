﻿using MODELS.BusinessObject.SAGEX3.Database;
using DATA.LayeredArquiteture.Repositories.Interfaces.Generic;

namespace DATA.LayeredArquiteture.Repositories.Interfaces.Concrete
{
    public interface IMfgItmRepository : IGenericRepository<MFGITM>
    {

    }
}
