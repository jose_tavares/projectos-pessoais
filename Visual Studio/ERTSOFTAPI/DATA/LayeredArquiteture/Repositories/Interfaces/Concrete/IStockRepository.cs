﻿using System.Threading.Tasks;
using System.Collections.Generic;
using DATA.LayeredArquiteture.Repositories.Interfaces.Generic;
using MODELS.BusinessObject.SAGEX3.Database;

namespace DATA.LayeredArquiteture.Repositories.Interfaces.Concrete
{
    public interface IStockRepository : IGenericRepository<STOCK>
    {
        /// <summary>
        /// Returns List of Stock filtered by fields sent via parameter.
        /// It ensures that each item in list will contain StolotModel filled associated to STOCK ordered by STOLOT.CREDAT_0
        /// </summary>
        /// <param name="fields"></param>
        /// <returns></returns>
        Task<IEnumerable<STOCK>> GetByFieldsWithStolot(Dictionary<string, object> fields);
    }
}