﻿using System.Xml;
using MODELS.Helpers;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Collections.Generic;
using DATA.Helpers.AuxiliarClasses;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.Repositories.Interfaces.Generic;

namespace DATA.LayeredArquiteture.Repositories.Interfaces.Concrete
{
    public interface IExportWSRepository : IGenericRepository<EXPORTWS>
    {
        /// <summary>
        /// Gets List of EXPORTWS that is pending for integration.
        /// The conditions applied are ENABFLG = 2, NEXTWRITEDATE_0 &lt;= GETUTCDATE() and NEXTREWRITEDATE_0 &lt;= GETUTCDATE()
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<EXPORTWS>> GetPending();

        /// <summary>
        /// Gets the first EXPORTWS or null that is pending for integration.
        /// The conditions applied are ENABFLG = 2, NEXTWRITEDATE_0 &lt;= GETUTCDATE() and NEXTREWRITEDATE_0 &lt;= GETUTCDATE()
        /// </summary>
        /// <returns></returns>
        Task<EXPORTWS> GetFirstPending();

        /// <summary>
        /// Returns Dictionary filtered by DB TableName with column name as key and SqlParameter with all column properties as value.
        /// The value of dictionary will contain info such as data type, size, precision.
        /// </summary>
        /// <param name="tableName"></param>
        /// <returns></returns>
        Dictionary<string, SqlParameter> GetParameters(string tableName);

        /// <summary>
        /// Returns ExportData which contains SQLCommand to insert data and SQLCommand to update data based on all rows from the XLM sent via parameter.
        /// </summary>
        /// <param name="webService"></param>
        /// <param name="node"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        ExportData GetExportDataByXMLNode(EXPORTWS exportWs, XmlNode node, Dictionary<string, SqlParameter> parameters, decimal logId);

        /// <summary>
        /// Returns ExportData which contains SQLCommand to insert data and SQLCommand to update data based on all rows from the XLM sent via parameter.
        /// </summary>
        /// <param name="webService"></param>
        /// <param name="reader"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        ExportData GetExportDataByDataReader(EXPORTWS exportWs, SqlDataReader reader, Dictionary<string, SqlParameter> parameters, decimal logId);

        /// <summary>
        /// Loops through an IEnumerable of ExportData and inserts data into matching SQL tables from MAINTABLE_0 and LOGTABLE_0 properties on EXPORTWS model
        /// </summary>
        /// <param name="exportDataList"></param>
        /// <param name="export"></param>
        /// <param name="exportWsLog"></param>
        /// <returns></returns>
        Task<Result<EXPORTWSLOG>> InsertFromExportDataList(IEnumerable<ExportData> exportDataList, EXPORTWS export, EXPORTWSLOG exportLog);

        /// <summary>
        /// Loops through datareader and inserts datainto matching SQL tables from MAINTABLE_0 and LOGTABLE_0 properties on EXPORTWS model
        /// </summary>
        /// <param name="export"></param>
        /// <param name="exportLog"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        Task<Result<EXPORTWSLOG>> InsertFromSelectQuery(EXPORTWS export, EXPORTWSLOG exportLog, Dictionary<string, SqlParameter> parameters);

        /// <summary>
        /// Inserts row into SQL table that matches MAINTABLE_0 property from EXPORTWS.
        /// </summary>
        /// <param name="exportData"></param>
        /// <param name="export"></param>
        /// <returns></returns>
        Task<int> InsertMainTable(ExportData exportData, EXPORTWS export, SqlConnection connection);

        /// <summary>
        /// Inserts row into SQL table that matches LOGTABLE_0 property from EXPORTWS.
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="exportData"></param>
        /// <param name="export"></param>
        /// <returns></returns>
        Task<int> InsertLogTable(ExportData exportData, EXPORTWS export, SqlConnection connection);

        /// <summary>
        /// Updates row in SQL table that matches MAINTABLE_0 property from EXPORTWS.
        /// </summary>
        /// <param name="exportData"></param>
        /// <param name="export"></param>
        /// <returns></returns>
        Task<int> UpdateMainTable(ExportData exportData, EXPORTWS export, SqlConnection connection);

        /// <summary>
        /// Executes query in SQL that matches DELETEQUERY_0 property from EXPORTWS.
        /// </summary>
        /// <param name="export"></param>
        /// <returns></returns>
        Task<int> DeleteFromQuery(string query);
    }
}
