﻿using Dapper;
using System;
using System.Data;
using System.Linq;
using MODELS.Helpers;
using System.Threading.Tasks;
using DATA.Helpers.StaticClasses;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.Repositories.Interfaces.Generic;

namespace DATA.LayeredArquiteture.Repositories.Implementation.Generic
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        public string ConnName { get; }

        public GenericRepository(string connName)
        {
            ConnName = connName;
        }

        public async Task<IEnumerable<T>> GetAll()
        {
            string query = QueryStringHelper.SelectAll<T>();

            using (IDbConnection connection = DBHelper.NewConnection(ConnName))
            {
                IEnumerable<T> data = await connection.QueryAsync<T>(query);

                return data;
            }
        }

        public async Task<IEnumerable<T>> GetByField(string field, object value)
        {
            string query = QueryStringHelper.SelectByField<T>(field);

            using (IDbConnection connection = DBHelper.NewConnection(ConnName))
            {
                DynamicParameters param = new DynamicParameters();
                param.Add($"@{field}", value);

                IEnumerable<T> data = await connection.QueryAsync<T>(query, param);

                return data;
            }
        }

        public async Task<IEnumerable<T>> GetByFields(Dictionary<string, object> fields)
        {
            string query = QueryStringHelper.SelectByFields<T>(fields.Keys.ToArray());

            using (IDbConnection connection = DBHelper.NewConnection(ConnName))
            {
                IEnumerable<T> data = await connection.QueryAsync<T>(query, fields);

                return data;
            }
        }

        public async Task<T> GetById(int id)
        {
            string query = QueryStringHelper.SelectById<T>(); 

            using (IDbConnection connection = DBHelper.NewConnection(ConnName))
            {
                DynamicParameters param = new DynamicParameters();
                param.Add($"@{nameof(DBRules.ROWID)}", id);

                T data = await connection.QuerySingleOrDefaultAsync<T>(query, param);

                return data;
            }
        }

        public async Task<T> GetByUniqueField(string field, object value)
        {
            string query = QueryStringHelper.SelectByField<T>(field);

            using (IDbConnection connection = DBHelper.NewConnection(ConnName))
            {
                DynamicParameters param = new DynamicParameters();
                param.Add($"@{field}", value);

                T data = await connection.QuerySingleOrDefaultAsync<T>(query, param);

                return data;
            }
        }

        public async Task<T> GetByUniqueFields(Dictionary<string, object> fields)
        {
            string query = QueryStringHelper.SelectByFields<T>(fields.Keys.ToArray());

            using (IDbConnection connection = DBHelper.NewConnection(ConnName))
            {
                T data = await connection.QuerySingleOrDefaultAsync<T>(query, fields);

                return data;
            }
        }

        public async Task<Result<T>> Insert(T model)
        {
            Result<T> result = new Result<T>();
            string query = QueryStringHelper.Insert<T>();

            using (IDbConnection connection = DBHelper.NewConnection(ConnName))
            {
                int id = Convert.ToInt32(await connection.ExecuteScalarAsync(query, model));
                model.GetType().GetProperty(nameof(DBRules.ROWID)).SetValue(model, id);

                result.IsInserted(id, model);
            }

            return result;
        }

        public async Task<Result<T>> Update(T model)
        {
            Result<T> result = new Result<T>();
            string query = QueryStringHelper.Update<T>();

            using (IDbConnection connection = DBHelper.NewConnection(ConnName))
            {
                int affectedRows = await connection.ExecuteAsync(query, model);

                result.IsUpdated(affectedRows, model);
            }

            return result;
        }

        public async Task<Result<T>> Delete(int id)
        {
            Result<T> result = new Result<T>();
            string query = QueryStringHelper.Delete<T>();

            using (IDbConnection connection = DBHelper.NewConnection(ConnName))
            {
                DynamicParameters param = new DynamicParameters();
                param.Add($"@{nameof(DBRules.ROWID)}", id);

                int affectedRows = await connection.ExecuteAsync(query, param);

                result.IsDeleted(affectedRows);
            }

            return result;
        }
    }
}
