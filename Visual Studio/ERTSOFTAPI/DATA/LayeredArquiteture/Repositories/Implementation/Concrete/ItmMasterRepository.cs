﻿using DATA.LayeredArquiteture.Repositories.Implementation.Generic;
using DATA.LayeredArquiteture.Repositories.Interfaces.Concrete;
using MODELS.BusinessObject.SAGEX3.Database;

namespace DATA.LayeredArquiteture.Repositories.Implementation.Concrete
{
    public class ItmMasterRepository : GenericRepository<ITMMASTER>, IItmMasterRepository
    {
        public ItmMasterRepository(string connName) : base(connName)
        {

        }
    }
}
