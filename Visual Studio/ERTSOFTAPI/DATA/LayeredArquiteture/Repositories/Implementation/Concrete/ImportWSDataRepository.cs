﻿using Dapper;
using System.Data;
using System.Threading.Tasks;
using DATA.Helpers.StaticClasses;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.Repositories.Interfaces.Concrete;
using DATA.LayeredArquiteture.Repositories.Implementation.Generic;

namespace DATA.LayeredArquiteture.Repositories.Implementation.Concrete
{
    public class ImportWSDataRepository : GenericRepository<IMPORTWSDATA>, IImportWSDataRepository
    {
        public ImportWSDataRepository(string connName) : base(connName)
        {

        }

        public async Task<IMPORTWSDATA> GetFirstPendingByWsCode(string WSCODE_0, int MAXATTEMPTS_0)
        {
            string query = QueryStringHelper.SelectTopAll<IMPORTWSDATA>(1)
            .Where($"{nameof(IMPORTWSDATA.WSCODE_0)} = @{nameof(IMPORTWSDATA.WSCODE_0)}")
            .And($"{nameof(IMPORTWSDATA.ATTEMPTS_0)} < @{nameof(IMPORTWSDATA.ATTEMPTS_0)}")
            .And($"{nameof(IMPORTWSDATA.IMPORTFLG_0)} = 2")
            .And($"{nameof(IMPORTWSDATA.SUCCESSFLG_0)} = 1")
            .And($"{nameof(IMPORTWSDATA.REGFLG_0)} = 1")
            .And($"{nameof(IMPORTWSDATA.FAILFLG_0)} = 1")
            .OrderBy($"{nameof(IMPORTWSDATA.ATTEMPTS_0)}, {nameof(STOJOU.CREDATTIM_0)}"); ;

            using (IDbConnection connection = DBHelper.NewConnection(ConnName))
            {
                DynamicParameters param = new DynamicParameters();
                param.Add($"@{nameof(IMPORTWSDATA.WSCODE_0)}", WSCODE_0);
                param.Add($"@{nameof(IMPORTWSDATA.ATTEMPTS_0)}", MAXATTEMPTS_0);

                IMPORTWSDATA data = await connection.QuerySingleOrDefaultAsync<IMPORTWSDATA>(query, param);

                return data;
            }
        }
    }
}
