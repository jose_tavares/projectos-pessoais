﻿using Dapper;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using static Dapper.SqlMapper;
using System.Collections.Generic;
using DATA.Helpers.StaticClasses;
using DATA.LayeredArquiteture.Repositories.Interfaces.Concrete;
using DATA.LayeredArquiteture.Repositories.Implementation.Generic;
using MODELS.BusinessObject.SAGEX3.Database;

namespace DATA.LayeredArquiteture.Repositories.Implementation.Concrete
{
    public class MfgMatRepository : GenericRepository<MFGMAT>, IMfgMatRepository
    {
        public MfgMatRepository(string connName) : base(connName)
        {

        }

        public async Task<IEnumerable<MFGMAT>> GetByMfgnum(string MFGNUM_0)
        {
            string query = QueryStringHelper.SelectAll<MFGMAT>("M")
            .InnerJoin<ITMMASTER>("I", $"M.{nameof(MFGMAT.ITMREF_0)} = I.{nameof(ITMMASTER.ITMREF_0)}", false)
            .Where($"I.{nameof(ITMMASTER.STOMGTCOD_0)} <> 1")
            .And($"M.{nameof(MFGMAT.MATSTA_0)} < 4")
            .And($"M.{nameof(MFGMAT.MFGNUM_0)} = @MFGNUM_0");

            using (IDbConnection connection = DBHelper.NewConnection(ConnName))
            {
                DynamicParameters param = new DynamicParameters();
                param.Add($"@{nameof(MFGMAT.MFGNUM_0)}", MFGNUM_0);

                IEnumerable<MFGMAT> data = await connection.QueryAsync<MFGMAT>(query, param);

                return data;
            }
        }

        public async Task<IEnumerable<MFGMAT>> GetByMfgnumAndOperationAndProdType(string MFGNUM_0, string ZOPERCON_0, bool PRODOK_0)
        {
            string query = QueryStringHelper.SelectAll<MFGMAT>("M")
            .InnerJoin<ITMMASTER>("I", $"M.{nameof(MFGMAT.ITMREF_0)} = I.{nameof(ITMMASTER.ITMREF_0)}", false)
            .Where($"I.{nameof(ITMMASTER.STOMGTCOD_0)} <> 1")
            .And($"M.{nameof(MFGMAT.MATSTA_0)} < 4")
            .And($"M.{nameof(MFGMAT.MFGNUM_0)} = @{nameof(MFGMAT.MFGNUM_0)}")
            .And($"M.{nameof(MFGMAT.ZOPERCON_0)} = @{nameof(MFGMAT.ZOPERCON_0)}");

            if (PRODOK_0)
                query = query.And($"M.{nameof(MFGMAT.ZCONSPRDOK_0)} = 2");
            else
                query = query.And($"M.{nameof(MFGMAT.ZCONSPRDNOK_0)} = 2");

            using (IDbConnection connection = DBHelper.NewConnection(ConnName))
            {
                DynamicParameters param = new DynamicParameters();
                param.Add($"@{nameof(MFGMAT.MFGNUM_0)}", MFGNUM_0);
                param.Add($"@{nameof(MFGMAT.ZOPERCON_0)}", ZOPERCON_0);

                IEnumerable<MFGMAT> data = await connection.QueryAsync<MFGMAT>(query, param);

                return data;
            }
        }
    }
}
