﻿using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.Repositories.Interfaces.Concrete;
using DATA.LayeredArquiteture.Repositories.Implementation.Generic;

namespace DATA.LayeredArquiteture.Repositories.Implementation.Concrete
{
    public class ConfigValueRepository : GenericRepository<CONFIGVALUE>, IConfigValueRepository
    {
        public ConfigValueRepository(string connName) : base(connName)
        {

        }
    }
}
