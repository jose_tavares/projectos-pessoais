﻿using Dapper;
using System;
using System.Xml;
using System.Data;
using System.Linq;
using MODELS.Helpers;
using System.Data.SqlClient;
using System.Threading.Tasks;
using DATA.Helpers.StaticClasses;
using System.Collections.Generic;
using DATA.Helpers.AuxiliarClasses;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.Repositories.Interfaces.Concrete;
using DATA.LayeredArquiteture.Repositories.Implementation.Generic;

namespace DATA.LayeredArquiteture.Repositories.Implementation.Concrete
{
    public class ExportWSRepository : GenericRepository<EXPORTWS>, IExportWSRepository
    {
        public ExportWSRepository(string connName) : base(connName)
        {

        }

        public async Task<IEnumerable<EXPORTWS>> GetPending()
        {
            string query = QueryStringHelper.SelectAll<EXPORTWS>()
            .Where(nameof(EXPORTWS.ENABFLG_0) + " = 2")
            .And("(" + nameof(EXPORTWS.NEXTWRITEDATE_0) + " <= GETUTCDATE()")
            .Or(nameof(EXPORTWS.NEXTREWRITEDATE_0) + " < GETUTCDATE())");

            using (IDbConnection connection = DBHelper.NewConnection(ConnName))
            {
                IEnumerable<EXPORTWS> data = await connection.QueryAsync<EXPORTWS>(query);

                return data;
            }
        }

        public async Task<EXPORTWS> GetFirstPending()
        {
            string query = QueryStringHelper.SelectTopAll<EXPORTWS>(1)
            .Where(nameof(EXPORTWS.ENABFLG_0) + " = 2")
            .And("(" + nameof(EXPORTWS.NEXTWRITEDATE_0) + " <= GETUTCDATE()")
            .Or(nameof(EXPORTWS.NEXTREWRITEDATE_0) + " < GETUTCDATE())")
            .OrderBy(nameof(EXPORTWS.NEXTWRITEDATE_0));

            using (IDbConnection connection = DBHelper.NewConnection(ConnName))
            {
                EXPORTWS data = await connection.QuerySingleOrDefaultAsync<EXPORTWS>(query);

                return data;
            }
        }

        public Dictionary<string, SqlParameter> GetParameters(string tableName)
        {
            Dictionary<string, SqlParameter> parameters = tableName.GetSQLParameters(false, ConnName);

            return parameters;
        }

        public ExportData GetExportDataByXMLNode(EXPORTWS exportWs, XmlNode node, Dictionary<string, SqlParameter> parameters, decimal logId)
        {
            int count = 0;
            string keyValue = string.Empty;
            string dateValue = string.Empty;
            SqlCommand insertCommand = new SqlCommand("(");
            SqlCommand updateCommand = new SqlCommand();

            foreach (XmlNode field in node.SelectNodes("FLD"))
            {
                string fieldName = field.Attributes[0].Value;
                SqlParameter parameter = parameters.Values.ElementAt(count);

                parameter.Value = field.InnerText;

                insertCommand.CommandText += parameter.ParameterName + ", ";
                insertCommand.Parameters.Add(new SqlParameter(parameter.ParameterName, parameter.Value)
                {
                    SqlDbType = parameter.SqlDbType,
                    Size = parameter.Size,
                    Precision = parameter.Precision,
                    Scale = parameter.Scale
                });

                updateCommand.CommandText += "[" + parameters.Keys.ElementAt(count) + "] = " + parameter.ParameterName + ", ";
                updateCommand.Parameters.Add(new SqlParameter(parameter.ParameterName, parameter.Value)
                {
                    SqlDbType = parameter.SqlDbType,
                    Size = parameter.Size,
                    Precision = parameter.Precision,
                    Scale = parameter.Scale
                });

                if (exportWs.KEYFLD_0 == fieldName)
                {
                    keyValue = field.InnerText;
                }
                else if (exportWs.DATEFLD_0 == fieldName)
                {
                    dateValue = field.InnerText;
                }

                count += 1;
            }

            insertCommand.CommandText = insertCommand.CommandText.Remove(insertCommand.CommandText.Length - 2);
            updateCommand.CommandText = updateCommand.CommandText.Remove(updateCommand.CommandText.Length - 2);
            insertCommand.CommandText += ")";
            updateCommand.CommandText += " WHERE [" + exportWs.KEYFLD_0 + "] = @" + exportWs.KEYFLD_0;

            ExportData exportData = new ExportData(insertCommand, updateCommand, logId, keyValue, dateValue);

            return exportData;
        }

        public ExportData GetExportDataByDataReader(EXPORTWS exportWs, SqlDataReader reader, Dictionary<string, SqlParameter> parameters, decimal logId)
        {
            int count = 0;
            string keyValue = string.Empty;
            string dateValue = string.Empty;
            SqlCommand insertCommand = new SqlCommand("(");
            SqlCommand updateCommand = new SqlCommand();

            for (int i = 0; i < reader.FieldCount; i++)
            {
                SqlParameter parameter = parameters.Values.ElementAt(count);

                parameters.Values.ElementAt(count).Value = reader[i];

                insertCommand.CommandText += parameter.ParameterName + ", ";
                insertCommand.Parameters.Add(new SqlParameter(parameter.ParameterName, parameter.Value)
                {
                    SqlDbType = parameter.SqlDbType,
                    Size = parameter.Size,
                    Precision = parameter.Precision,
                    Scale = parameter.Scale
                });

                updateCommand.CommandText += "[" + parameters.Keys.ElementAt(count) + "] = " + parameter.ParameterName + ", ";
                updateCommand.Parameters.Add(new SqlParameter(parameter.ParameterName, parameter.Value)
                {
                    SqlDbType = parameter.SqlDbType,
                    Size = parameter.Size,
                    Precision = parameter.Precision,
                    Scale = parameter.Scale
                });

                if (exportWs.KEYFLD_0 + "_0" == reader.GetName(i))
                {
                    keyValue = reader[i].ToString();
                }
                else if (exportWs.DATEFLD_0 + "_0" == reader.GetName(i))
                {
                    dateValue = reader[i].ToString();
                }

                count += 1;
            }

            insertCommand.CommandText = insertCommand.CommandText.Remove(insertCommand.CommandText.Length - 2);
            updateCommand.CommandText = updateCommand.CommandText.Remove(updateCommand.CommandText.Length - 2);
            insertCommand.CommandText += ")";
            updateCommand.CommandText += " WHERE [" + exportWs.KEYFLD_0 + "] = @" + exportWs.KEYFLD_0;

            ExportData exportData = new ExportData(insertCommand, updateCommand, logId, keyValue, dateValue);

            return exportData;
        }

        public async Task<int> InsertMainTable(ExportData exportData, EXPORTWS export, SqlConnection connection)
        {
            SqlCommand command = exportData.InsertCommand.Clone();
            command.Connection = connection;
            command.CommandText = "INSERT INTO " + export.MAINTABLE_0 + " VALUES " + command.CommandText;

            using (command)
            {
                int affectedRows = await command.ExecuteNonQueryAsync();

                return affectedRows;
            }
        }

        public async Task<int> InsertLogTable(ExportData exportData, EXPORTWS export, SqlConnection connection)
        {
            SqlCommand command = exportData.InsertCommand.Clone();
            command.Connection = connection;

            command.CommandText = "INSERT INTO " + export.LOGTABLE_0 + " VALUES (" + command.CommandText.Remove(0, 1);
            command.CommandText = command.CommandText.Remove(command.CommandText.Length - 1, 1);
            command.CommandText += ", @CREDATTIMLOG_0, @ROWIDLOG)";

            command.Parameters.Add("@CREDATTIMLOG_0", SqlDbType.DateTime).Value = DateTime.UtcNow;
            command.Parameters.Add("@ROWIDLOG", SqlDbType.Decimal).Value = exportData.LogId;

            using (command)
            {
                int affectedRows = await command.ExecuteNonQueryAsync();

                return affectedRows;
            }
        }

        public async Task<Result<EXPORTWSLOG>> InsertFromExportDataList(IEnumerable<ExportData> exportDataList, EXPORTWS export, EXPORTWSLOG exportLog)
        {
            Result<EXPORTWSLOG> exportWsLogResult = new Result<EXPORTWSLOG>(true);

            exportLog.SUCCESSFLG_0 = 2;

            foreach (ExportData exportDataItem in exportDataList)
            {
                try
                {
                    using (SqlConnection connection = DBHelper.NewConnection(ConnName))
                    {
                        await InsertLogTable(exportDataItem, export, connection);

                        int affectedRows = await UpdateMainTable(exportDataItem, export, connection);

                        if (affectedRows == 0)
                        {
                            exportLog.INSERTEDROWS_0 += await InsertMainTable(exportDataItem, export, connection);
                        }
                        else
                        {
                            exportLog.UPDATEDROWS_0 += affectedRows;
                        }
                    }
                }
                catch (Exception e)
                {  
                    exportLog.OUTPUT_0 = $"Exportação efetuada sem sucesso. Chave: {exportDataItem.KeyValue} / Data: {exportDataItem.DateValue}.";
                    exportLog.OUTPUT_0 += $" Exceção: {e.Message}";
                    exportWsLogResult.Success = false;

                    break;
                }
            }

            exportWsLogResult.Obj = exportLog;

            return exportWsLogResult;
        }

        public async Task<Result<EXPORTWSLOG>> InsertFromSelectQuery(EXPORTWS export, EXPORTWSLOG exportLog, Dictionary<string, SqlParameter> parameters)
        {
            Result<EXPORTWSLOG> exportWsLogResult = new Result<EXPORTWSLOG>(true);

            exportLog.SUCCESSFLG_0 = 2;

            using (SqlConnection sageConn = DBHelper.NewConnection("SAGE", false))
            {
                sageConn.ConnectionString += "Enlist=false;";

                using (SqlCommand command = sageConn.NewCommand(export.SELECTQUERY_0))
                {
                    using (SqlDataReader reader = command.NewDataReader())
                    {
                        using (SqlConnection connection = DBHelper.NewConnection(ConnName))
                        {
                            while (reader.Read())
                            {
                                ExportData exportData = GetExportDataByDataReader(export, reader, parameters, exportLog.ROWID);

                                try
                                {
                                    await InsertLogTable(exportData, export, connection);
                                    exportLog.INSERTEDROWS_0 += await InsertMainTable(exportData, export, connection);
                                }
                                catch (Exception e)
                                {
                                    exportLog.OUTPUT_0 = $"Exportação efetuada sem sucesso. Chave: {exportData.KeyValue} / Data: {exportData.DateValue}.";
                                    exportLog.OUTPUT_0 += $" Exceção: {e.Message}";
                                    exportWsLogResult.Success = false;

                                    break;
                                }                   
                            }
                        }
                    }
                }
            }

            exportWsLogResult.Obj = exportLog;

            return exportWsLogResult;
        }

        public async Task<int> UpdateMainTable(ExportData exportData, EXPORTWS export, SqlConnection connection)
        {
            SqlCommand command = exportData.UpdateCommand.Clone();
            command.Connection = connection;
            command.CommandText = "UPDATE " + export.MAINTABLE_0 + " SET " + command.CommandText;

            using (command)
            {
                int affectedRows = await command.ExecuteNonQueryAsync();

                return affectedRows;
            }
        }

        public async Task<int> DeleteFromQuery(string query)
        {
            using (SqlConnection connection = DBHelper.NewConnection(ConnName))
            {
                using (SqlCommand command = connection.NewCommand(query))
                {
                    int affectedRows = await command.ExecuteNonQueryAsync();

                    return affectedRows;
                }
            }
        }
    }
}
