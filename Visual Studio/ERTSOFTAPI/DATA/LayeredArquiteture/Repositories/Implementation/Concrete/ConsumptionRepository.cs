﻿using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.Repositories.Interfaces.Concrete;
using DATA.LayeredArquiteture.Repositories.Implementation.Generic;

namespace DATA.LayeredArquiteture.Repositories.Implementation.Concrete
{
    public class ConsumptionRepository : GenericRepository<CONSUMPTION>, IConsumptionRepository
    {
        public ConsumptionRepository(string connName) : base(connName)
        {

        }
    }
}
