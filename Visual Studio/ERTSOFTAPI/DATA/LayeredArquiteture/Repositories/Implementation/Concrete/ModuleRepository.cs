﻿using Dapper;
using System.Data;
using System.Threading.Tasks;
using static Dapper.SqlMapper;
using DATA.Helpers.StaticClasses;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.Repositories.Interfaces.Concrete;
using DATA.LayeredArquiteture.Repositories.Implementation.Generic;

namespace DATA.LayeredArquiteture.Repositories.Implementation.Concrete
{
    public class ModuleRepository : GenericRepository<MODULE>, IModuleRepository
    {
        public ModuleRepository(string connName) : base(connName)
        {

        }

        public async Task<MODULE> GetByUniqueFieldsWithScreenList(string APP_0, string MODULE_0)
        {
            string query = QueryStringHelper.SelectByFields<MODULE>(nameof(MODULE.APP_0), nameof(MODULE.MODULE_0)) + "; "
                + QueryStringHelper.SelectByFields<SCREEN>(nameof(SCREEN.APP_0), nameof(SCREEN.MODULE_0)) + ";";

            using (IDbConnection connection = DBHelper.NewConnection(ConnName))
            {
                DynamicParameters param = new DynamicParameters();
                param.Add($"@{nameof(SCREEN.APP_0)}", APP_0);
                param.Add($"@{nameof(SCREEN.MODULE_0)}", MODULE_0);

                GridReader multipleData = await connection.QueryMultipleAsync(query, param);
                MODULE moduleModel = await multipleData.ReadSingleOrDefaultAsync<MODULE>();

                if (moduleModel != null)
                {
                    moduleModel.ScreenList = await multipleData.ReadAsync<SCREEN>();
                }

                return moduleModel;
            }
        }
    }
}
