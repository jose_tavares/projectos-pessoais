﻿using DATA.LayeredArquiteture.Repositories.Interfaces.Concrete;
using DATA.LayeredArquiteture.Repositories.Implementation.Generic;
using MODELS.BusinessObject.SAGEX3.Database;

namespace DATA.LayeredArquiteture.Repositories.Implementation.Concrete
{
    public class StolotRepository : GenericRepository<STOLOT>, IStolotRepository
    {
        public StolotRepository(string connName) : base(connName)
        {

        }
    }
}
