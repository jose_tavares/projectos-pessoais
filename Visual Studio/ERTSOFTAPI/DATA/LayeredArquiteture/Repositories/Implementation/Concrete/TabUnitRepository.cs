﻿using DATA.LayeredArquiteture.Repositories.Interfaces.Concrete;
using DATA.LayeredArquiteture.Repositories.Implementation.Generic;
using MODELS.BusinessObject.SAGEX3.Database;

namespace DATA.LayeredArquiteture.Repositories.Implementation.Concrete
{
    public class TabUnitRepository : GenericRepository<TABUNIT>, ITabUnitRepository
    {
        public TabUnitRepository(string connName) : base(connName)
        {

        }
    }
}
