﻿using DATA.LayeredArquiteture.Repositories.Interfaces.Concrete;
using DATA.LayeredArquiteture.Repositories.Implementation.Generic;
using MODELS.BusinessObject.SAGEX3.Database;

namespace DATA.LayeredArquiteture.Repositories.Implementation.Concrete
{
    public class MfgHeadRepository : GenericRepository<MFGHEAD>, IMfgHeadRepository
    {
        public MfgHeadRepository(string connName) : base(connName)
        {

        }
    }
}
