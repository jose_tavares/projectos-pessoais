﻿using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.Repositories.Interfaces.Concrete;
using DATA.LayeredArquiteture.Repositories.Implementation.Generic;

namespace DATA.LayeredArquiteture.Repositories.Implementation.Concrete
{
    public class StoDefectRepository : GenericRepository<STODEFECT>, IStoDefectRepository
    {
        public StoDefectRepository(string connName) : base(connName)
        {

        }
    }
}
