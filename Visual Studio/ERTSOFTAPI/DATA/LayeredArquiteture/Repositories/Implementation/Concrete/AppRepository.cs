﻿using Dapper;
using System.Data;
using System.Threading.Tasks;
using static Dapper.SqlMapper;
using DATA.Helpers.StaticClasses;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.Repositories.Interfaces.Concrete;
using DATA.LayeredArquiteture.Repositories.Implementation.Generic;

namespace DATA.LayeredArquiteture.Repositories.Implementation.Concrete
{
    public class AppRepository : GenericRepository<APP>, IAppRepository
    {
        public AppRepository(string connName) : base(connName)
        {

        }

        public async Task<APP> GetByUniqueFieldWithModuleList(string APP_0)
        {
            string query = QueryStringHelper.SelectByFields<APP>(nameof(APP.APP_0)) + "; "
                + QueryStringHelper.SelectByFields<MODULE>(nameof(MODULE.APP_0)) + ";";

            using (IDbConnection connection = DBHelper.NewConnection(ConnName))
            {
                DynamicParameters param = new DynamicParameters();
                param.Add($"@{nameof(MODULE.APP_0)}", APP_0);

                GridReader multipleData = await connection.QueryMultipleAsync(query, param);
                APP appModel = await multipleData.ReadSingleOrDefaultAsync<APP>();

                if (appModel != null)
                {
                    appModel.ModuleList = await multipleData.ReadAsync<MODULE>();
                }

                return appModel;
            }
        }
    }
}
