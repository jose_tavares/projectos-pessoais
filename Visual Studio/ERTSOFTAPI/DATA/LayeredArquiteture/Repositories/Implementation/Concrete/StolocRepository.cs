﻿using DATA.LayeredArquiteture.Repositories.Interfaces.Concrete;
using DATA.LayeredArquiteture.Repositories.Implementation.Generic;
using MODELS.BusinessObject.SAGEX3.Database;

namespace DATA.LayeredArquiteture.Repositories.Implementation.Concrete
{
    public class StolocRepository : GenericRepository<STOLOC>, IStolocRepository
    {
        public StolocRepository(string connName) : base(connName)
        {

        }
    }
}
