﻿namespace DATA.LayeredArquiteture.Repositories.Implementation.Concrete
{
    using Dapper;
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;
    using static Dapper.SqlMapper;
    using System.Collections.Generic;
    using DATA.Helpers.StaticClasses;
    using MODELS.BusinessObject.ERTSOFT.Database;
    using DATA.LayeredArquiteture.Repositories.Interfaces.Concrete;
    using DATA.LayeredArquiteture.Repositories.Implementation.Generic;

    public class StojouRepository : GenericRepository<STOJOU>, IStojouRepository
    {
        public StojouRepository(string connName) : base(connName)
        {

        }

        public async Task<IEnumerable<STOJOU>> GetPending()
        {
            string query = QueryStringHelper.SelectAll<STOJOU>("S")
            .InnerJoin<WEBSERVICE>("W", $"S.{nameof(STOJOU.WSCODE_0)} = W.{nameof(WEBSERVICE.CODE_0)}", false)
            .Where($"S.{nameof(STOJOU.ATTEMPTS_0)} < 10")
            .And($"W.{nameof(WEBSERVICE.IMPORTFLG_0)} = 2")
            .And($"W.{nameof(WEBSERVICE.ENABFLG_0)} = 2")
            .And($"S.{nameof(STOJOU.SUCCESSFLG_0)} = 1")
            .And($"S.{nameof(STOJOU.IMPORTFLG_0)} = 2")
            .And($"S.{nameof(STOJOU.REGFLG_0)} = 1")
            .And($"S.{nameof(STOJOU.FAILFLG_0)} = 1")
            .OrderBy($"S.{nameof(STOJOU.ATTEMPTS_0)}, S.{nameof(STOJOU.CREDATTIM_0)}");

            using (IDbConnection connection = DBHelper.NewConnection(ConnName))
            {
                IEnumerable<STOJOU> data = await connection.QueryAsync<STOJOU>(query);

                return data;
            }
        }

        public async Task<STOJOU> GetFirstPendingByWebService(string WEBSERVICE_0)
        {
            string query = QueryStringHelper.SelectTopAll<STOJOU>(1, "S")
            .InnerJoin<WEBSERVICE>("W", $"S.{nameof(STOJOU.WSCODE_0)} = W.{nameof(WEBSERVICE.CODE_0)}", false)
            .Where($"S.{nameof(STOJOU.ATTEMPTS_0)} < 10")
            .And($"W.{nameof(WEBSERVICE.IMPORTFLG_0)} = 2")
            .And($"W.{nameof(WEBSERVICE.CODE_0)} = @{nameof(WEBSERVICE.CODE_0)}")
            .And($"W.{nameof(WEBSERVICE.ENABFLG_0)} = 2")
            .And($"S.{nameof(STOJOU.SUCCESSFLG_0)} = 1")
            .And($"S.{nameof(STOJOU.IMPORTFLG_0)} = 2")
            .And($"S.{nameof(STOJOU.REGFLG_0)} = 1")
            .And($"S.{nameof(STOJOU.FAILFLG_0)} = 1")
            .OrderBy($"S.{nameof(STOJOU.ATTEMPTS_0)}, S.{nameof(STOJOU.CREDATTIM_0)}");

            using (IDbConnection connection = DBHelper.NewConnection(ConnName))
            {
                DynamicParameters param = new DynamicParameters();
                param.Add($"@{nameof(WEBSERVICE.CODE_0)}", WEBSERVICE_0);

                STOJOU data = await connection.QuerySingleOrDefaultAsync<STOJOU>(query, param);

                return data;
            }
        }

        public async Task<STOJOU> GetByIdWithWebService(int id)
        {
            string query = QueryStringHelper.SelectById<STOJOU>() + "; "
            + QueryStringHelper.SelectAll<WEBSERVICE>("W").InnerJoin<STOJOU>("S", $"W.{nameof(WEBSERVICE.CODE_0)} = S.{nameof(STOJOU.WSCODE_0)}", false)
            .Where($"S.{nameof(STOJOU.ROWID)} = @{nameof(STOJOU.ROWID)}");

            using (IDbConnection connection = DBHelper.NewConnection(ConnName))
            {
                DynamicParameters param = new DynamicParameters();
                param.Add($"@{nameof(STOJOU.ROWID)}", id);

                GridReader multipleData = await connection.QueryMultipleAsync(query, param);
                STOJOU stojou = await multipleData.ReadSingleOrDefaultAsync<STOJOU>();

                if (stojou != null)
                {
                    stojou.WebServiceModel = await multipleData.ReadSingleOrDefaultAsync<WEBSERVICE>();
                }

                return stojou;
            }
        }

        public async Task<STOJOU> GetByIdWithStojouLogList(int id)
        {
            string query = QueryStringHelper.SelectById<STOJOU>() + "; "
            + QueryStringHelper.SelectByField<STOJOULOG>(nameof(STOJOULOG.STOJOUID)) + ";";

            using (IDbConnection connection = DBHelper.NewConnection(ConnName))
            {
                DynamicParameters param = new DynamicParameters();
                param.Add($"@{nameof(STOJOU.ROWID)}", id);
                param.Add($"@{nameof(STOJOULOG.STOJOUID)}", id);

                GridReader multipleData = await connection.QueryMultipleAsync(query, param);
                STOJOU stojou = await multipleData.ReadSingleOrDefaultAsync<STOJOU>();

                if (stojou != null)
                {
                    stojou.StojouLogList = await multipleData.ReadAsync<STOJOULOG>();
                }

                return stojou;
            }
        }

        public async Task<string> GetNextProductionSlo(string MFGNUM_0)
        {
            string query = $"SELECT ISNULL(MAX(FORMAT(CONVERT(int, {nameof(STOJOU.SLO_0)} + 1), 'D5')), FORMAT(1, 'D5')) AS 'MAXSLO' FROM {QueryStringHelper.TableName<STOJOU>()}"
            .Where($"{nameof(STOJOU.WSCODE_0)} = 'PRODUCTION' AND {nameof(STOJOU.MFGNUM_0)} = @{nameof(STOJOU.MFGNUM_0)}");

            using (IDbConnection connection = DBHelper.NewConnection(ConnName))
            {
                DynamicParameters param = new DynamicParameters();
                param.Add($"@{nameof(STOJOU.MFGNUM_0)}", MFGNUM_0);

                string data = await connection.QuerySingleAsync<string>(query, MFGNUM_0);

                return data;
            }
        }
    }
}
