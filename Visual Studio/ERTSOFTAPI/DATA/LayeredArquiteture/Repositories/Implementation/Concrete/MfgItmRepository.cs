﻿using MODELS.BusinessObject.SAGEX3.Database;
using DATA.LayeredArquiteture.Repositories.Interfaces.Concrete;
using DATA.LayeredArquiteture.Repositories.Implementation.Generic;

namespace DATA.LayeredArquiteture.Repositories.Implementation.Concrete
{
    public class MfgItmRepository : GenericRepository<MFGITM>, IMfgItmRepository
    {
        public MfgItmRepository(string connName) : base(connName)
        {

        }
    }
}
