﻿using DATA.LayeredArquiteture.Repositories.Interfaces.Concrete;
using DATA.LayeredArquiteture.Repositories.Implementation.Generic;
using MODELS.BusinessObject.SAGEX3.Database;

namespace DATA.LayeredArquiteture.Repositories.Implementation.Concrete
{
    public class MfgMatTrkRepository : GenericRepository<MFGMATTRK>, IMfgMatTrkRepository
    {
        public MfgMatTrkRepository(string connName) : base(connName)
        {

        }
    }
}
