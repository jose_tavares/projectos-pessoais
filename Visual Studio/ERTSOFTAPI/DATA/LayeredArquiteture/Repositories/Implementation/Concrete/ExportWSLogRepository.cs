﻿using DATA.LayeredArquiteture.Repositories.Implementation.Generic;
using DATA.LayeredArquiteture.Repositories.Interfaces.Concrete;
using MODELS.BusinessObject.ERTSOFT.Database;

namespace DATA.LayeredArquiteture.Repositories.Implementation.Concrete
{
    public class ExportWSLogRepository : GenericRepository<EXPORTWSLOG>, IExportWSLogRepository
    {
        public ExportWSLogRepository(string connName) : base(connName)
        {

        }
    }
}
