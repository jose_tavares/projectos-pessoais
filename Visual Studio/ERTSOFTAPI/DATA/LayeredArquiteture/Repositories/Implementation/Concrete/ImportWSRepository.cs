﻿using Dapper;
using System.Data;
using System.Threading.Tasks;
using System.Collections.Generic;
using DATA.Helpers.StaticClasses;
using MODELS.BusinessObject.ERTSOFT.Database;
using System.Linq;
using DATA.LayeredArquiteture.Repositories.Interfaces.Concrete;
using DATA.LayeredArquiteture.Repositories.Implementation.Generic;

namespace DATA.LayeredArquiteture.Repositories.Implementation.Concrete
{
    public class ImportWSRepository : GenericRepository<IMPORTWS>, IImportWSRepository
    {
        public ImportWSRepository(string connName) : base(connName)
        {

        }

        public async Task<dynamic> GetDynamicById(int id, string SQLTABLE_0, IEnumerable<string> fields)
        {
            string query = "SELECT ";

            foreach (string field in fields)
            {
                query += $"{field}, ";
            }

            if (!fields.Contains(nameof(IMPORTWS.ROWID)))
            {
                query += $"{nameof(IMPORTWS.ROWID)}, ";
            }

            query = query.Remove(query.Length - 2);

            query += $" FROM {SQLTABLE_0}"
            .Where($"{nameof(IMPORTWS.ROWID)} = @{nameof(IMPORTWS.ROWID)}");

            using (IDbConnection connection = DBHelper.NewConnection(ConnName))
            {
                DynamicParameters param = new DynamicParameters();
                param.Add($"@{nameof(IMPORTWS.ROWID)}", id);

                dynamic data = await connection.QuerySingleOrDefaultAsync<dynamic>(query, param);

                return data;
            }
        }
    }
}
