﻿using Dapper;
using System.Data;
using System.Threading.Tasks;
using static Dapper.SqlMapper;
using DATA.Helpers.StaticClasses;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.Repositories.Interfaces.Concrete;
using DATA.LayeredArquiteture.Repositories.Implementation.Generic;

namespace DATA.LayeredArquiteture.Repositories.Implementation.Concrete
{
    public class ProfileRepository : GenericRepository<PROFILE>, IProfileRepository
    {
        public ProfileRepository(string connName) : base(connName)
        {
        }

        public async Task<PROFILE> GetByUniqueFieldWithUserList(string PROFILE_0)
        {
            string query = QueryStringHelper.SelectByFields<PROFILE>(nameof(PROFILE.PROFILE_0)) + "; "
                + QueryStringHelper.SelectByFields<USER>(nameof(USER.PROFILE_0)) + ";";

            using (IDbConnection connection = DBHelper.NewConnection(ConnName))
            {
                DynamicParameters param = new DynamicParameters();
                param.Add($"@{nameof(PROFILE.PROFILE_0)}", PROFILE_0);

                GridReader multipleData = await connection.QueryMultipleAsync(query, param);
                PROFILE profileModel = await multipleData.ReadSingleOrDefaultAsync<PROFILE>();

                if (profileModel != null)
                {
                    profileModel.UserList = await multipleData.ReadAsync<USER>();
                }

                return profileModel;
            }
        }
    }
}
