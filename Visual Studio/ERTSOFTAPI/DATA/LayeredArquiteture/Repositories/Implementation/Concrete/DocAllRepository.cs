﻿using Dapper;
using System.Data;
using System.Threading.Tasks;
using System.Collections.Generic;
using DATA.Helpers.StaticClasses;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.Repositories.Interfaces.Concrete;
using DATA.LayeredArquiteture.Repositories.Implementation.Generic;

namespace DATA.LayeredArquiteture.Repositories.Implementation.Concrete
{
    public class DocAllRepository : GenericRepository<DOCALL>, IDocAllRepository
    {
        public DocAllRepository(string connName) : base(connName)
        {

        }

        public async Task<IEnumerable<DOCALL>> GetAvailableByDocument(string DOCUMENT_0)
        {
            string query = QueryStringHelper.SelectAll<DOCALL>("DA")
            .Where($"{nameof(DOCALL.DOCUMENT_0)} = @{nameof(DOCALL.DOCUMENT_0)}")
            .And($"{nameof(DOCALL.QTYPCU_0)} > 0")
            .And($"{nameof(DOCALL.AVAILFLG_0)} = 2")
            .OrderBy(nameof(DOCALL.UPDDATTIM_0));

            using (IDbConnection connection = DBHelper.NewConnection(ConnName))
            {
                DynamicParameters param = new DynamicParameters();
                param.Add($"@{nameof(DOCALL.DOCUMENT_0)}", DOCUMENT_0);

                IEnumerable<DOCALL> data = await connection.QueryAsync<DOCALL>(query, param);

                return data;
            }
        }

        public async Task<IEnumerable<DOCALL>> GetAvailableByStofcyDocumentItmref(string STOFCY_0, string DOCUMENT_0, string ITMREF_0)
        {
            string query = QueryStringHelper.SelectAll<DOCALL>("DA")
            .Where($"{nameof(DOCALL.STOFCY_0)} = @{nameof(DOCALL.STOFCY_0)}")
            .And($"{nameof(DOCALL.DOCUMENT_0)} = @{nameof(DOCALL.DOCUMENT_0)}")
            .And($"{nameof(DOCALL.ITMREF_0)} = @{nameof(DOCALL.ITMREF_0)}")
            .And($"{nameof(DOCALL.QTYPCU_0)} > 0")
            .And($"{nameof(DOCALL.AVAILFLG_0)} = 2")
            .OrderBy(nameof(DOCALL.UPDDATTIM_0));

            using (IDbConnection connection = DBHelper.NewConnection(ConnName))
            {
                DynamicParameters param = new DynamicParameters();
                param.Add($"@{nameof(DOCALL.STOFCY_0)}", STOFCY_0);
                param.Add($"@{nameof(DOCALL.DOCUMENT_0)}", DOCUMENT_0);
                param.Add($"@{nameof(DOCALL.ITMREF_0)}", ITMREF_0);

                IEnumerable<DOCALL> data = await connection.QueryAsync<DOCALL>(query, param);

                return data;
            }
        }
    }
}
