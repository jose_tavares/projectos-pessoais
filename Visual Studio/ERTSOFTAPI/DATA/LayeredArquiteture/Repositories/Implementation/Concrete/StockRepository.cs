﻿using Dapper;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using DATA.Helpers.StaticClasses;
using DATA.LayeredArquiteture.Repositories.Interfaces.Concrete;
using DATA.LayeredArquiteture.Repositories.Implementation.Generic;
using MODELS.BusinessObject.SAGEX3.Database;

namespace DATA.LayeredArquiteture.Repositories.Implementation.Concrete
{
    public class StockRepository : GenericRepository<STOCK>, IStockRepository
    {
        public StockRepository(string connName) : base(connName) { }

        public async Task<IEnumerable<STOCK>> GetByFieldsWithStolot(Dictionary<string, object> fields)
        {
            string where = string.Empty;

            foreach (string field in fields.Keys)
            {
                where += "S." + field + " = @" + field + " AND ";
            }

            string query = QueryStringHelper.SelectAll<STOCK>("S")
            .InnerJoin<STOLOT>("ST", $"S.{nameof(STOCK.ITMREF_0)} = ST.{nameof(STOLOT.ITMREF_0)} AND S.{nameof(STOCK.LOT_0)} = ST.{nameof(STOLOT.LOT_0)} AND S.{nameof(STOCK.SLO_0)} = ST.{nameof(STOLOT.SLO_0)}")
            .Where(where);

            query = query.Remove(query.Length - 5);
            query.OrderBy($"ST.{nameof(STOLOT.LOTCREDAT_0)}, ST.{nameof(STOLOT.ROWID)}");

            using (IDbConnection connection = DBHelper.NewConnection(ConnName))
            {
                IEnumerable<STOCK> data = await connection.QueryAsync<STOCK, STOLOT, STOCK>(query, (s, st) =>
                {
                    s.StolotModel = st;

                    return s;

                }, fields, splitOn: $"{nameof(STOCK.ITMREF_0)}, {nameof(STOCK.LOT_0)}, {nameof(STOCK.SLO_0)}");

                return data;
            }
        }
    }
}
