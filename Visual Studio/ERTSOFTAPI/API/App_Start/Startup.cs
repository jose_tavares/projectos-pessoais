﻿using Owin;
using System;
using API.Providers;
using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;

namespace API
{
    public partial class Startup
    {
        public static OAuthAuthorizationServerOptions OAuthOptions { get; private set; }

        static Startup()
        {
            OAuthOptions = new OAuthAuthorizationServerOptions
            {
                TokenEndpointPath = new PathString("/api/User/Login"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(15),
                AllowInsecureHttp = false,
                Provider = new OAuthProvider(),
                RefreshTokenProvider = new RefreshTokenProvider()
            };
        }

        public void ConfigureAuth(IAppBuilder app)
        {
            app.UseOAuthBearerTokens(OAuthOptions);
        }
    }
}