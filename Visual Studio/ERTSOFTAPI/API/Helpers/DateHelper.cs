﻿using System;

namespace API.Helpers
{
    public static class DateHelper
    {
        public static string MillisecondsToTime(this long miliseconds, string format)
        {
            TimeSpan timeSpan = TimeSpan.FromMilliseconds(miliseconds);

            return timeSpan.ToString(format);
        }
    }
}