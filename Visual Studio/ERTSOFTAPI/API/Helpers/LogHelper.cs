﻿using System;
using Serilog;
using System.Net;
using API.Helpers;
using System.Linq;
using Serilog.Events;
using Newtonsoft.Json;
using System.Net.Http;
using System.Diagnostics;
using Newtonsoft.Json.Linq;
using System.Security.Principal;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.BusinessLogic.Implementation;
using System.Threading.Tasks;
using MODELS.Helpers;

namespace DATA.Helpers.StaticClasses
{
    public static class LogHelper
    {
        private static readonly Dictionary<int, LogEventLevel> logEventLevels = new Dictionary<int, LogEventLevel>();

        static LogHelper()
        {
            IEnumerable<int> statusCodeList = EnumHelper.GetEnumValues<HttpStatusCode>()
                                              .Distinct()
                                              .Select(selector => (int)selector);

            foreach (int statusCodeItemValue in statusCodeList)
            {
                if ((statusCodeItemValue >= 100) && (statusCodeItemValue <= 101))
                {
                    logEventLevels.Add(statusCodeItemValue, LogEventLevel.Debug);
                }

                else if ((statusCodeItemValue >= 200) && (statusCodeItemValue <= 206))
                {
                    logEventLevels.Add(statusCodeItemValue, LogEventLevel.Information);
                }

                else if ((statusCodeItemValue >= 300) && (statusCodeItemValue <= 426))
                {
                    logEventLevels.Add(statusCodeItemValue, LogEventLevel.Warning);
                }

                else if ((statusCodeItemValue >= 500) && (statusCodeItemValue <= 505))
                {
                    logEventLevels.Add(statusCodeItemValue, LogEventLevel.Error);
                }
            }
        }

        public async static Task LogHttpRequest(HttpRequestMessage request, HttpStatusCode statusCode, string requestBody, string responseBody, long executionTime)
        {
            IIdentity clientUser = request.GetRequestContext().Principal.Identity;
            int statusCodeValue = (int)statusCode;
            string user = clientUser.GetField(nameof(USER.USR_0)) ?? "N/D";
            string username = clientUser.GetField(nameof(USER.USERNAM_0)) ?? "N/D";
            string clientApp = request.Headers.TryGetValues("AppName", out IEnumerable<string> headerValues) ? headerValues.FirstOrDefault() : "N/D";

            try
            {
                await LogHttpRequestDB(statusCodeValue, user, username, clientApp, request, statusCode, requestBody, responseBody, executionTime);
            }
            catch (Exception ex) //Se ocorrer um erro significa que não foi possível guardar o log na BD. Guardar o log com a exceção através do Seq + Serilog
            {
                LogHttpRequestSeriLog(statusCodeValue, user, username, clientApp, request, statusCode, requestBody, responseBody, executionTime, ex);
            }
        }

        public async static Task LogHttpRequestDB(int statusCodeValue, string user, string username, string clientApp, HttpRequestMessage request, HttpStatusCode statusCode, string requestBody, string responseBody, long executionTime)
        {
            APIREQUEST apiRequest = new APIREQUEST
            {
                CLIENTAPP_0 = clientApp,
                USER_0 = user,
                USERNAM_0 = username,
                STATUSCODE_0 = statusCodeValue,
                STATUSDES_0 = statusCode.ToString(),
                URI_0 = request.RequestUri.AbsoluteUri,
                METHOD_0 = request.Method.Method,
                CONTROLLER_0 = request.GetActionDescriptor()?.ControllerDescriptor?.ControllerName ?? string.Empty,
                REQUESTBODY_0 = requestBody,
                RESPONSEBODY_0 = GetResponseBodyFiltered(responseBody),
                TOKEN_0 = request.Headers?.Authorization?.Parameter ?? string.Empty,
                EXECUTIONTIME_0 = executionTime,
                CREUSR_0 = user,
                UPDUSR_0 = user
            };

            Result<APIREQUEST> result = await new ApiRequestBLL().Insert(apiRequest);

            if (!result.Success)
            {
                throw new Exception(result.ToString());
            }
        }

        public static void LogHttpRequestSeriLog(int statusCodeValue, string user, string username, string clientApp, HttpRequestMessage request, HttpStatusCode statusCode, string requestBody, string responseBody, long executionTime, Exception ex)
        {
            ILogger logger = GetSeriLog();

            logger = logger.ForContext("A: ClientApp", clientApp);
            logger = logger.ForContext("B: User", user);
            logger = logger.ForContext("C: Username", username);
            logger = logger.ForContext("D: ExecutionTime", executionTime.MillisecondsToTime("hh':'mm':'ss'.'fff"));
            logger = logger.ForContext("E: StatusCode", $"{statusCode} ({statusCodeValue})");
            logger = logger.ForContext("F: RequestMethod", request.Method.Method);
            logger = logger.ForContext("G: RequestController", request.GetActionDescriptor()?.ControllerDescriptor?.ControllerName);
            logger = logger.ForContext("H: RequestToken", request.Headers?.Authorization?.Parameter);
            logger = logger.ForContext("I: RequestUri", request.RequestUri.AbsoluteUri);
            logger = logger.ForContext("J: RequestBody", requestBody);

            LogEventLevel logEventLevel = logEventLevels[statusCodeValue];

            switch (logEventLevel)
            {
                case LogEventLevel.Error:
                case LogEventLevel.Fatal:
                    CustomException exception = JsonConvert.DeserializeObject<CustomException>(responseBody);

                    logger = (exception != null) && (exception.ExceptionMessage != null)
                        ? logger.ForContext("K: ResponseBody", exception, true)
                        : logger.ForContext("K: ResponseBody", GetResponseBodyFiltered(responseBody));
                    break;

                default:
                    logger = logger.ForContext("K: ResponseBody", GetResponseBodyFiltered(responseBody));
                    break;
            }

            logger = logger.ForContext("L: ApiRequestException", ex, true);

            string template = $"ID : {Guid.NewGuid()} / CLIENTAPP : {clientApp} / USER: {user} / USERNAME: {username}";

            logger.Write(logEventLevel, template);
        }

        private static ILogger GetSeriLog()
        {
            try
            {
                return new LoggerConfiguration()
                .ReadFrom.AppSettings()
                .CreateLogger();
            }
            catch (Exception e)
            {
                Debug.Write(e.Message);
                throw;
            }
        }

        private static string GetResponseBodyFiltered(string responseBody)
        {
            try
            {
                if (!string.IsNullOrEmpty(responseBody))
                {
                    JToken token = JToken.Parse(responseBody);

                    if (token is JArray)
                    {
                        int maxCollectionCount = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["serilog:destructure:ToMaximumCollectionCount.maximumCollectionCount"]);
                        IEnumerable<object> dataList = token.ToList<object>().Take(maxCollectionCount);
                        responseBody = JsonConvert.SerializeObject(dataList);
                    }
                }
            }
            catch
            {

            }
            
            return responseBody;
        }
    }

    public class CustomException
    {
        public string ExceptionMessage { get; set; }

        public string ExceptionType { get; set; }

        public string StackTrace { get; set; }

        public CustomException InnerException { get; set; }
    }
}