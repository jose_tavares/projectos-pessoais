﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;

namespace API.Helpers
{
    public static class IdentityExtended
    {
        public static int GetId(this IIdentity identity)
        {
            string id = identity.GetField(nameof(USER.ROWID));

            return id != null ? Convert.ToInt32(id) : default;
        }

        public static string GetField(this IIdentity identity, string field)
        {
            if (identity.IsAuthenticated)
            {
                IEnumerable<Claim> claims = ((ClaimsIdentity)identity).Claims;
                Claim claimType = claims.SingleOrDefault(c => c.Type == field);

                return claimType.Value;
            }

            return null;
        }
    }
}