﻿using System;
using System.Net;
using MODELS.Helpers;
using System.Web.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;
using MODELS.BusinessObject.SAGEX3.Database;

namespace API.Controllers
{
    [Authorize]
    public class STOCKController : ApiController
    {
        private readonly IStockBLL stockBLL;

        public STOCKController(IStockBLL stockBLL)
        {
            this.stockBLL = stockBLL;
        }

        // GET: api/STOCK
        public async Task<IHttpActionResult> Get()
        {
            try
            {
                Result<IEnumerable<STOCK>> stockListResult = await stockBLL.GetAll();

                if (stockListResult.Success)
                {
                    return Ok(stockListResult.Obj);
                }

                return Content(HttpStatusCode.NotFound, stockListResult.ToString());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // GET: api/STOCK/5
        public async Task<IHttpActionResult> Get(int id)
        {
            try
            {
                Result<STOCK> stockResult = await stockBLL.GetById(id);

                if (stockResult.Success)
                {
                    return Ok(stockResult.Obj);
                }

                return Content(HttpStatusCode.NotFound, stockResult.ToString());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
