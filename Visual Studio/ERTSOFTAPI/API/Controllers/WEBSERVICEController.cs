﻿using System;
using System.Net;
using MODELS.Helpers;
using System.Web.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;

namespace API.Controllers
{
    [Authorize]
    public class WEBSERVICEController : ApiController
    {
        private readonly IWebServiceBLL webServiceBLL;

        public WEBSERVICEController(IWebServiceBLL webServiceBLL)
        {
            this.webServiceBLL = webServiceBLL;
        }

        // GET: api/WEBSERVICE
        public async Task<IHttpActionResult> Get()
        {
            try
            {
                Result<IEnumerable<WEBSERVICE>> webServiceListResult = await webServiceBLL.GetAll();

                if (webServiceListResult.Success)
                {
                    return Ok(webServiceListResult.Obj);
                }

                return Content(HttpStatusCode.NotFound, webServiceListResult.ToString());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [Route("api/WEBSERVICE/GetByImportFlg")]
        public async Task<IHttpActionResult> GetByImportFlg([FromUri] int IMPORTFLG_0)
        {
            try
            {
                Result<IEnumerable<WEBSERVICE>> webServiceListResult = await webServiceBLL.GetByImportFlg(IMPORTFLG_0);

                if (webServiceListResult.Success)
                {
                    return Ok(webServiceListResult.Obj);
                }

                return Content(HttpStatusCode.NotFound, webServiceListResult.ToString());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // GET: api/WEBSERVICE/5
        public async Task<IHttpActionResult> Get(int id)
        {
            try
            {
                Result<WEBSERVICE> webServiceResult = await webServiceBLL.GetById(id);

                if (webServiceResult.Success)
                {
                    return Ok(webServiceResult.Obj);
                }

                return Content(HttpStatusCode.NotFound, webServiceResult.ToString());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [Route("api/WEBSERVICE/GetByWebService/")]
        public async Task<IHttpActionResult> GetByWebService([FromUri] string WEBSERVICE_0)
        {
            try
            {
                Result<WEBSERVICE> webserviceResult = await webServiceBLL.GetByUniqueField(WEBSERVICE_0);

                if (webserviceResult.Success)
                {
                    return Ok(webserviceResult.Obj);
                }

                return Content(HttpStatusCode.NotFound, webserviceResult.ToString());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // POST: api/WEBSERVICE
        public async Task<IHttpActionResult> Post([FromBody] WEBSERVICE webService)
        {
            try
            {
                if (webService != null)
                {
                    Result<WEBSERVICE> result = await webServiceBLL.Insert(webService);

                    if (result.Success)
                    {
                        return Ok(result);
                    }

                    return BadRequest(result.ToString());
                }

                return BadRequest($"O objeto {nameof(webService)} não pode ser null.");
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // PUT: api/WEBSERVICE/5
        public async Task<IHttpActionResult> Put([FromBody] WEBSERVICE webService)
        {
            try
            {
                if (webService != null)
                {
                    Result<WEBSERVICE> result = await webServiceBLL.Update(webService);

                    if (result.Success)
                    {
                        return Ok(result);
                    }

                    return BadRequest(result.ToString());
                }

                return BadRequest($"O objeto {nameof(webService)} não pode ser null.");
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
