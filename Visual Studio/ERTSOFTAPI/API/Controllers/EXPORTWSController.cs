﻿using System;
using System.Net;
using MODELS.Helpers;
using System.Web.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;

namespace API.Controllers
{
    [Authorize]
    public class EXPORTWSController : ApiController
    {
        private readonly IExportWSBLL exportBLL;

        public EXPORTWSController(IExportWSBLL exportBLL)
        {
            this.exportBLL = exportBLL;
        }

        // GET: api/EXPORTWS
        public async Task<IHttpActionResult> Get()
        {
            try
            {
                Result<IEnumerable<EXPORTWS>> exportWsListResult = await exportBLL.GetAll();

                if (exportWsListResult.Success)
                {
                    return Ok(exportWsListResult.Obj);
                }

                return Content(HttpStatusCode.NotFound, exportWsListResult.ToString());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // GET: api/EXPORTWS/5
        public async Task<IHttpActionResult> Get(int id)
        {
            try
            {
                Result<EXPORTWS> exportResult = await exportBLL.GetById(id);

                if (exportResult.Success)
                {
                    return Ok(exportResult.Obj);
                }

                return Content(HttpStatusCode.NotFound, exportResult.ToString());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [Route("api/EXPORTWS/GetPending")]
        public async Task<IHttpActionResult> GetPending()
        {
            try
            {
                IEnumerable<EXPORTWS> exportList = await exportBLL.GetPending();

                return Ok(exportList);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [Route("api/EXPORTWS/ExportPending")]
        public async Task<IHttpActionResult> ExportPending()
        {
            try
            {
                Result<EXPORTWS> result = await exportBLL.ExportPending();

                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // POST: api/APP
        public async Task<IHttpActionResult> Post([FromBody] EXPORTWS export)
        {
            try
            {
                if (export != null)
                {
                    Result<EXPORTWS> result = await exportBLL.Insert(export);

                    if (result.Success)
                    {
                        return Ok(result);
                    }

                    return BadRequest(result.ToString());
                }

                return BadRequest($"O objeto {nameof(export)} não pode ser null.");
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // PUT: api/APP/5
        public async Task<IHttpActionResult> Put([FromBody] EXPORTWS export)
        {
            try
            {
                if (export != null)
                {
                    Result<EXPORTWS> result = await exportBLL.Update(export);

                    if (result.Success)
                    {
                        return Ok(result);
                    }

                    return BadRequest(result.ToString());
                }

                return BadRequest($"O objeto {nameof(export)} não pode ser null.");
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}