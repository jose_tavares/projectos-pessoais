﻿using System;
using System.Net;
using MODELS.Helpers;
using System.Web.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;

namespace API.Controllers
{
    [Authorize]
    public class IMPORTWSController : ApiController
    {
        private readonly IImportWSBLL importWsBLL;

        public IMPORTWSController(IImportWSBLL importWsBLL)
        {
            this.importWsBLL = importWsBLL;
        }

        // GET: api/STOJOU
        public async Task<IHttpActionResult> Get()
        {
            try
            {
                Result<IEnumerable<IMPORTWS>> importWsListResult = await importWsBLL.GetAll();

                if (importWsListResult.Success)
                {
                    return Ok(importWsListResult.Obj);
                }

                return Content(HttpStatusCode.NotFound, importWsListResult.ToString());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [Route("api/IMPORTWS/GetByWebService")]
        public async Task<IHttpActionResult> GetByWebService([FromUri] string WEBSERVIE_0)
        {
            try
            {
                Result<IMPORTWS> importWsResult = await importWsBLL.GetByUniqueField(WEBSERVIE_0);

                if (importWsResult.Success)
                {
                    return Ok(importWsResult.Obj);
                }

                return Content(HttpStatusCode.NotFound, importWsResult.ToString());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [Route("api/IMPORTWS/GetFirstPendingByWebService")]
        public async Task<IHttpActionResult> GetFirstPendingByWebService([FromUri] string WEBSERVICE_0)
        {
            try
            {
                (Result<dynamic>, IMPORTWS importWs) data = await importWsBLL.GetFirstPendingByWsCode(WEBSERVICE_0);

                return Ok(data);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [Route("api/IMPORTWS/ImportFirstPendingByWebService")]
        public async Task<IHttpActionResult> ImportFirstPendingByWebService([FromUri] string WEBSERVICE_0)
        {
            try
            {
                Result<IMPORTWS> result = await importWsBLL.ImportFirstPendingByWsCode(WEBSERVICE_0);

                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // GET: api/STOJOU/5
        public async Task<IHttpActionResult> Get(int id)
        {
            try
            {
                Result<IMPORTWS> importWsResult = await importWsBLL.GetById(id);

                if (importWsResult.Success)
                {
                    return Ok(importWsResult.Obj);
                }

                return Content(HttpStatusCode.NotFound, importWsResult.ToString());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
