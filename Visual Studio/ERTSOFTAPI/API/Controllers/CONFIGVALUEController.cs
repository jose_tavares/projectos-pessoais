﻿using System;
using System.Net;
using MODELS.Helpers;
using System.Web.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;

namespace API.Controllers
{
    [Authorize]
    public class CONFIGVALUEController : ApiController
    {
        private readonly IConfigValueBLL configValueBLL;

        public CONFIGVALUEController(IConfigValueBLL configValueBLL)
        {
            this.configValueBLL = configValueBLL;
        }

        // GET: api/APP
        public async Task<IHttpActionResult> Get()
        {
            try
            {
                Result<IEnumerable<CONFIGVALUE>> configValueListResult = await configValueBLL.GetAll();

                if (configValueListResult.Success)
                {
                    return Ok(configValueListResult.Obj);
                }

                return Content(HttpStatusCode.NotFound, configValueListResult.ToString());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // GET: api/APP/5
        public async Task<IHttpActionResult> Get(int id)
        {
            try
            {
                Result<CONFIGVALUE> atabdivResult = await configValueBLL.GetById(id);

                if (atabdivResult.Success)
                {
                    return Ok(atabdivResult.Obj);
                }

                return Content(HttpStatusCode.NotFound, atabdivResult.ToString());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        public async Task<IHttpActionResult> Post([FromBody] CONFIGVALUE configValue)
        {
            try
            {
                Result<CONFIGVALUE> result = await configValueBLL.Insert(configValue);

                if (result.Success)
                {
                    return Ok(result);
                }

                return BadRequest(result.ToString());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        public async Task<IHttpActionResult> Put([FromBody] CONFIGVALUE configValue)
        {
            try
            {
                Result<CONFIGVALUE> result = await configValueBLL.Update(configValue);

                if (result.Success)
                {
                    return Ok(result);
                }

                return BadRequest(result.ToString());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
