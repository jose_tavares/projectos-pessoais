﻿using System;
using System.Net;
using MODELS.Helpers;
using System.Web.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;

namespace API.Controllers
{
    [Authorize]
    public class MFGNUMZMULTIController : ApiController
    {
        private readonly IMfgnumZmultiBLL mfgnumZmultiBLL;

        public MFGNUMZMULTIController(IMfgnumZmultiBLL mfgnumZmultiBLL)
        {
            this.mfgnumZmultiBLL = mfgnumZmultiBLL;
        }

        public async Task<IHttpActionResult> Get()
        {
            try
            {
                Result<IEnumerable<MFGNUMZMULTI>> mfgnumzmultiListResult = await mfgnumZmultiBLL.GetAll();

                if (mfgnumzmultiListResult.Success)
                {
                    return Ok(mfgnumzmultiListResult.Obj);
                }

                return Content(HttpStatusCode.NotFound, mfgnumzmultiListResult.ToString());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // GET: api/APP/5
        public async Task<IHttpActionResult> Get(int id)
        {
            try
            {
                Result<MFGNUMZMULTI> mfgnumZmultiResult = await mfgnumZmultiBLL.GetById(id);

                if (mfgnumZmultiResult.Success)
                {
                    return Ok(mfgnumZmultiResult.Obj);
                }

                return Content(HttpStatusCode.NotFound, mfgnumZmultiResult.ToString());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpPost]
        [Route("api/MFGNUMZMULTI/InsertMfgnumZmulti")]
        public async Task<IHttpActionResult> InsertMfgnumZmulti([FromUri] string MFGNUMORI_0, [FromUri] string MFGNUM_0)
        {
            try
            {
                if (MFGNUMORI_0 != null)
                {
                    MFGNUMZMULTI mfgnum = new MFGNUMZMULTI
                    {
                        MFGNUMORI_0 = MFGNUMORI_0,
                        MFGNUM_0 = MFGNUM_0
                    };

                    Result<MFGNUMZMULTI> result = await mfgnumZmultiBLL.Insert(mfgnum);

                    if (result.Success)
                    {
                        return Ok(result);
                    }

                    return BadRequest(result.ToString());
                }

                return BadRequest($"O objeto {nameof(MFGNUMORI_0)} não pode ser null.");
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // POST: api/APP
        public async Task<IHttpActionResult> Post([FromBody] MFGNUMZMULTI mfgnumZmulti)
        {
            try
            {
                if (mfgnumZmulti != null)
                {
                    Result<MFGNUMZMULTI> result = await mfgnumZmultiBLL.Insert(mfgnumZmulti);

                    if (result.Success)
                    {
                        return Ok(result);
                    }

                    return BadRequest(result.ToString());
                }

                return BadRequest($"O objeto {nameof(mfgnumZmulti)} não pode ser null.");
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // PUT: api/APP/5
        public async Task<IHttpActionResult> Put([FromBody] MFGNUMZMULTI mfgnumZmulti)
        {
            try
            {
                if (mfgnumZmulti != null)
                {
                    Result<MFGNUMZMULTI> result = await mfgnumZmultiBLL.Update(mfgnumZmulti);

                    if (result.Success)
                    {
                        return Ok(result);
                    }

                    return BadRequest(result.ToString());
                }

                return BadRequest($"O objeto {nameof(mfgnumZmulti)} não pode ser null.");
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
