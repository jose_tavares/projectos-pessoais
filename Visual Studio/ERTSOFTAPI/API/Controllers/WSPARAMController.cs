﻿using System;
using System.Net;
using MODELS.Helpers;
using System.Web.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;

namespace API.Controllers
{
    [Authorize]
    public class WSPARAMController : ApiController
    {
        private readonly IWSParamBLL wsParamBLL;

        public WSPARAMController(IWSParamBLL wsParamBLL)
        {
            this.wsParamBLL = wsParamBLL;
        }

        // GET: api/WSPARAM
        public async Task<IHttpActionResult> Get()
        {
            try
            {
                IEnumerable<WSPARAM> wsParamList = await wsParamBLL.GetAll();

                return Ok(wsParamList);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [Route("api/WSPARAM/GetByWebService/")]
        public async Task<IHttpActionResult> GetByWebService([FromUri] string webService)
        {
            try
            {
                if (!string.IsNullOrEmpty(webService))
                {
                    IEnumerable<WSPARAM> wsParamList = await wsParamBLL.GetByWsCode(webService);

                    return Ok(wsParamList);
                }

                return BadRequest($"O parametro {nameof(webService)} não pode ser null.");
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // GET: api/WSPARAM/5
        public async Task<IHttpActionResult> Get(decimal id)
        {
            try
            {
                WSPARAM wsParam = await wsParamBLL.GetByID(id);

                if (wsParam != null)
                {
                    return Ok(wsParam);
                }

                return StatusCode(HttpStatusCode.NoContent);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // POST: api/WSPARAM
        public async Task<IHttpActionResult> Post([FromBody] WSPARAM wsParam)
        {
            try
            {
                if (wsParam != null)
                {
                    Result<WSPARAM> result = await wsParamBLL.Insert(wsParam);

                    if (result.Success)
                    {
                        return Ok(result);
                    }

                    return BadRequest(result.ToString());
                }

                return BadRequest($"O objeto {nameof(wsParam)} não pode ser null.");
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // PUT: api/WSPARAM/5
        public async Task<IHttpActionResult> Put([FromBody] WSPARAM wsParam)
        {
            try
            {
                if (wsParam != null)
                {
                    Result<WSPARAM> result = await wsParamBLL.Update(wsParam);

                    if (result.Success)
                    {
                        return Ok(result);
                    }

                    return BadRequest(result.ToString());
                }

                return BadRequest($"O objeto {nameof(wsParam)} não pode ser null.");
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
