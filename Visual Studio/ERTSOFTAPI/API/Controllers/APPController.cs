﻿using System;
using System.Net;
using MODELS.Helpers;
using System.Web.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;

namespace API.Controllers
{
    [Authorize]
    public class APPController : ApiController
    {
        private readonly IAppBLL appBLL;

        public APPController(IAppBLL appBLL)
        {
            this.appBLL = appBLL;
        }

        public async Task<IHttpActionResult> Get()
        {
            try
            {
                Result<IEnumerable<APP>> appListResult = await appBLL.GetAll();

                if (appListResult.Success)
                {
                    return Ok(appListResult.Obj);
                }

                return Content(HttpStatusCode.NotFound, appListResult.ToString());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        public async Task<IHttpActionResult> Get(int id)
        {
            try
            {
                Result<APP> appResult = await appBLL.GetById(id);

                if (appResult.Success)
                {
                    return Ok(appResult.Obj);
                }

                return Content(HttpStatusCode.NotFound, appResult.ToString());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [Route("api/APP/GetByUniqueField")]
        public async Task<IHttpActionResult> GetByUniqueField([FromUri] string APP_0)
        {
            try
            {
                Result<APP> appResult = await appBLL.GetByUniqueField(APP_0);

                if (appResult.Success)
                {
                    return Ok(appResult.Obj);
                }

                return Content(HttpStatusCode.NotFound, appResult.ToString());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [Route("api/APP/GetByAppWithModuleList")]
        public async Task<IHttpActionResult> GetByAppWithModuleList([FromUri] string APP_0)
        {
            try
            {
                Result<APP> appResult = await appBLL.GetByUniqueFieldWithModuleList(APP_0);

                if (appResult.Success)
                {
                    return Ok(appResult.Obj);
                }

                return Content(HttpStatusCode.NotFound, appResult.ToString());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        public async Task<IHttpActionResult> Post([FromBody] APP app)
        {
            try
            {
                Result<APP> result = await appBLL.Insert(app);

                if (result.Success)
                {
                    return Ok(result);
                }

                return BadRequest(result.ToString());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        public async Task<IHttpActionResult> Put([FromBody] APP app)
        {
            try
            {
                Result<APP> result = await appBLL.Update(app);

                if (result.Success)
                {
                    return Ok(result);
                }

                return BadRequest(result.ToString());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
