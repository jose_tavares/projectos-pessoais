﻿using System;
using System.Net;
using API.Helpers;
using MODELS.Helpers;
using System.Web.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;

namespace API.Controllers
{
    [Authorize]
    public class USERController : ApiController
    {
        private readonly IUserBLL userBLL;

        public USERController(IUserBLL userBLL)
        {
            this.userBLL = userBLL;
        }

        // GET: api/User
        public async Task<IHttpActionResult> Get()
        {
            try
            {
                Result<IEnumerable<USER>> userListResult = await userBLL.GetAll();

                if (userListResult.Success)
                {
                    return Ok(userListResult.Obj);
                }

                return Content(HttpStatusCode.NotFound, userListResult.ToString());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [Route("api/USER/GetByProfile")]
        public async Task<IHttpActionResult> GetByProfile([FromUri] string PROFILE_0)
        {
            try
            {
                Result<IEnumerable<USER>> userResult = await userBLL.GetByProfile(PROFILE_0);

                if (userResult.Success)
                {
                    return Ok(userResult.Obj);
                }

                return Content(HttpStatusCode.NotFound, userResult.ToString());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // GET: api/User/5
        public async Task<IHttpActionResult> Get(int id)
        {
            try
            {
                Result<USER> userResult = await userBLL.GetById(id);

                if (userResult.Success)
                {
                    return Ok(userResult.Obj);
                }

                return Content(HttpStatusCode.NotFound, userResult.ToString());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [Route("api/USER/GetByUser")]
        public async Task<IHttpActionResult> GetByUser([FromUri] string USER_0)
        {
            try
            {
                Result<USER> userResult = await userBLL.GetByUser(USER_0);

                if (userResult.Success)
                {
                    return Ok(userResult.Obj);
                }

                return Content(HttpStatusCode.NotFound, userResult.ToString());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [Route("api/USER/GetByUsername")]
        public async Task<IHttpActionResult> GetByUsername([FromUri] string USERNAM_0)
        {
            try
            {
                Result<USER> userResult = await userBLL.GetByUsername(USERNAM_0);

                if (userResult.Success)
                {
                    return Ok(userResult.Obj);
                }

                return Content(HttpStatusCode.NotFound, userResult.ToString());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // POST: api/User
        public async Task<IHttpActionResult> Post([FromBody] USER user)
        {
            try
            {
                if (user != null)
                {
                    Result<USER> result = await userBLL.Insert(user);

                    if (result.Success)
                    {
                        return Ok(result);
                    }

                    return BadRequest(result.ToString());
                }

                return BadRequest($"O objeto {nameof(user)} não pode ser null.");
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // PUT: api/User/5
        public async Task<IHttpActionResult> Put([FromBody] USER user)
        {
            try
            {
                if (user != null)
                {
                    Result<USER> result = await userBLL.Update(user);

                    if (result.Success)
                    {
                        return Ok(result);
                    }

                    return BadRequest(result.ToString());
                }

                return BadRequest($"O objeto {nameof(user)} não pode ser null.");
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpPut]
        [Route("api/USER/Logout")]
        public async Task<IHttpActionResult> Logout()
        {
            try
            {
                int id = User.Identity.GetId();
                Result<USER> result = await userBLL.Logout(id);

                if (result.Success)
                {
                    return Ok(result);
                }

                return BadRequest(result.ToString());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}