﻿using System;
using System.Net;
using MODELS.Helpers;
using System.Web.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;

namespace API.Controllers
{
    [Authorize]
    public class PROFILEController : ApiController
    {
        private readonly IProfileBLL profileBLL;

        public PROFILEController(IProfileBLL profileBLL)
        {
            this.profileBLL = profileBLL;
        }

        // GET: api/PROFILE
        public async Task<IHttpActionResult> Get()
        {
            try
            {
                Result<IEnumerable<PROFILE>> profileListResult = await profileBLL.GetAll();

                if (profileListResult.Success)
                {
                    return Ok(profileListResult.Obj);
                }

                return Content(HttpStatusCode.NotFound, profileListResult.ToString());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // GET: api/PROFILE/5
        public async Task<IHttpActionResult> Get(int id)
        {
            try
            {
                Result<PROFILE> profileResult = await profileBLL.GetById(id);

                if (profileResult.Success)
                {
                    return Ok(profileResult.Obj);
                }

                return Content(HttpStatusCode.NotFound, profileResult.ToString());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [Route("api/PROFILE/GetByProfileWithUserList")]
        public async Task<IHttpActionResult> GetByProfileWithUserList([FromUri] string PROFILE_0)
        {
            try
            {
                Result<PROFILE> profileResult = await profileBLL.GetByUniqueFieldWithUserList(PROFILE_0);

                if (profileResult.Success)
                {
                    return Ok(profileResult.Obj);
                }

                return Content(HttpStatusCode.NotFound, profileResult.ToString());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // POST: api/PROFILE
        public async Task<IHttpActionResult> Post([FromBody] PROFILE profile)
        {
            try
            {
                if (profile != null)
                {
                    Result<PROFILE> result = await profileBLL.Insert(profile);

                    if (result.Success)
                    {
                        return Ok(result);
                    }

                    return BadRequest(result.ToString());
                }

                return BadRequest($"O objeto {nameof(profile)} não pode ser null.");
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // PUT: api/PROFILE/5
        public async Task<IHttpActionResult> Put([FromBody] PROFILE profile)
        {
            try
            {
                if (profile != null)
                {
                    Result<PROFILE> result = await profileBLL.Update(profile);

                    if (result.Success)
                    {
                        return Ok(result);
                    }

                    return BadRequest(result.ToString());
                }

                return BadRequest($"O objeto {nameof(profile)} não pode ser null.");
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
