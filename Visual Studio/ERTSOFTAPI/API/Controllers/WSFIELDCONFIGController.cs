﻿using System;
using System.Net;
using MODELS.Helpers;
using System.Web.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;

namespace API.Controllers
{
    [Authorize]
    public class WSFIELDCONFIGController : ApiController
    {
        private readonly IWsFieldConfigBLL wsFieldConfigBLL;

        public WSFIELDCONFIGController(IWsFieldConfigBLL wsFieldConfigBLL)
        {
            this.wsFieldConfigBLL = wsFieldConfigBLL;
        }

        // GET: api/WSFIELDCONFIG
        public async Task<IHttpActionResult> Get()
        {
            try
            {
                Result<IEnumerable<WSFIELDCONFIG>> wsFieldConfigListResult = await wsFieldConfigBLL.GetAll();

                if (wsFieldConfigListResult.Success)
                {
                    return Ok(wsFieldConfigListResult.Obj);
                }

                return Content(HttpStatusCode.NotFound, wsFieldConfigListResult.ToString());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [Route("api/WSFIELDCONFIG/GetByWsCode")]
        public async Task<IHttpActionResult> GetByWsCode([FromUri] string WSCODE_0)
        {
            try
            {
                Result<IEnumerable<WSFIELDCONFIG>> wsFieldConfigListResult = await wsFieldConfigBLL.GetByWsCode(WSCODE_0);

                if (wsFieldConfigListResult.Success)
                {
                    return Ok(wsFieldConfigListResult.Obj);
                }

                return Content(HttpStatusCode.NotFound, wsFieldConfigListResult.ToString());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // GET: api/WSFIELDCONFIG/5
        public async Task<IHttpActionResult> Get(int id)
        {
            try
            {
                Result<WSFIELDCONFIG> wsFieldConfigResult = await wsFieldConfigBLL.GetById(id);

                if (wsFieldConfigResult.Success)
                {
                    return Ok(wsFieldConfigResult.Obj);
                }

                return Content(HttpStatusCode.NotFound, wsFieldConfigResult.ToString());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // POST: api/WSFIELDCONFIG
        public async Task<IHttpActionResult> Post([FromBody] WSFIELDCONFIG wsFieldConfig)
        {
            try
            {
                if (wsFieldConfig != null)
                {
                    Result<WSFIELDCONFIG> result = await wsFieldConfigBLL.Insert(wsFieldConfig);

                    if (result.Success)
                    {
                        return Ok(result);
                    }

                    return BadRequest(result.ToString());
                }

                return BadRequest($"O objeto {nameof(wsFieldConfig)} não pode ser null.");
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // PUT: api/WSFIELDCONFIG/5
        public async Task<IHttpActionResult> Put([FromBody] WSFIELDCONFIG wsFieldConfig)
        {
            try
            {
                if (wsFieldConfig != null)
                {
                    Result<WSFIELDCONFIG> result = await wsFieldConfigBLL.Update(wsFieldConfig);

                    if (result.Success)
                    {
                        return Ok(result);
                    }

                    return BadRequest(result.ToString());
                }

                return BadRequest($"O objeto {nameof(wsFieldConfig)} não pode ser null.");
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
