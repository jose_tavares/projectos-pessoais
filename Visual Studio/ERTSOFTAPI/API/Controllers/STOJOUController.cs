﻿namespace API.Controllers
{
    using System;
    using System.Net;
    using MODELS.Helpers;
    using System.Web.Http;
    using System.Threading.Tasks;
    using System.Collections.Generic;
    using DATA.Helpers.AuxiliarClasses;
    using MODELS.BusinessObject.ERTSOFT.Database;
    using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;

    [Authorize]
    public class STOJOUController : ApiController
    {
        private readonly IStojouBLL stojouBLL;

        public STOJOUController(IStojouBLL stojouBLL)
        {
            this.stojouBLL = stojouBLL;
        }

        // GET: api/STOJOU
        public async Task<IHttpActionResult> Get()
        {
            try
            {
                Result<IEnumerable<STOJOU>> stojouListResult = await stojouBLL.GetAll();

                if (stojouListResult.Success)
                {
                    return Ok(stojouListResult.Obj);
                }

                return Content(HttpStatusCode.NotFound, stojouListResult.ToString());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [Route("api/STOJOU/GetByWSCode")]
        public async Task<IHttpActionResult> GetByWSCode([FromUri] string WSCODE_0)
        {
            try
            {
                if (!string.IsNullOrEmpty(WSCODE_0))
                {
                    IEnumerable<STOJOU> stojouList = await stojouBLL.GetByWSCode(WSCODE_0);

                    return Ok(stojouList);
                }

                return BadRequest($"O parametro {nameof(WSCODE_0)} não pode ser null.");
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [Route("api/STOJOU/GetPending")]
        public async Task<IHttpActionResult> GetPending()
        {
            try
            {
                IEnumerable<STOJOU> stojouList = await stojouBLL.GetPending();

                return Ok(stojouList);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [Route("api/STOJOU/GetFirstPendingByWebService")]
        public async Task<IHttpActionResult> GetFirstPendingByWebService([FromUri] string WEBSERVICE_0)
        {
            try
            {
                STOJOU stojou = await stojouBLL.GetFirstPendingByWebService(WEBSERVICE_0);

                if (stojou != null)
                {
                    return Ok(stojou);
                }

                return StatusCode(HttpStatusCode.NoContent);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [Route("api/STOJOU/ImportFirstPendingByWebService")]
        public async Task<IHttpActionResult> ImportFirstPendingByWebService([FromUri] string WEBSERVICE_0)
        {
            try
            {
                Result<STOJOU> result = await stojouBLL.ImportFirstPendingByWebService(WEBSERVICE_0);

                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // GET: api/STOJOU/5
        public async Task<IHttpActionResult> Get(int id)
        {
            try
            {
                Result<STOJOU> stojouResult = await stojouBLL.GetById(id);

                if (stojouResult.Success)
                {
                    return Ok(stojouResult.Obj);
                }

                return Content(HttpStatusCode.NotFound, stojouResult.ToString());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [Route("api/STOJOU/GetByIdWithWebService/{id}")]
        public async Task<IHttpActionResult> GetByIdWithWebService(int id)
        {
            try
            {
                STOJOU stojou = await stojouBLL.GetByIdWithWebService(id);

                if (stojou != null)
                {
                    return Ok(stojou);
                }

                return StatusCode(HttpStatusCode.NoContent);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [Route("api/STOJOU/GetStojouLogListByStojouID/{id}")]
        public async Task<IHttpActionResult> GetStojouLogListByStojouID(int id)
        {
            try
            {
                STOJOU stojou = await stojouBLL.GetByIdWithStojouLogList(id);

                if (stojou != null)
                {
                    return Ok(stojou);
                }

                return StatusCode(HttpStatusCode.NoContent);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpPost]
        [Route("api/STOJOU/InsertProduction/")]
        public async Task<IHttpActionResult> InsertProduction([FromBody] ProductionData productionData)
        {
            try
            {
                Result<STOJOU> result = await stojouBLL.InsertProduction(productionData);

                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // PUT: api/STOJOU/5
        public async Task<IHttpActionResult> Put([FromBody] STOJOU stojou)
        {
            try
            {
                if (stojou != null)
                {
                    Result<STOJOU> result = await stojouBLL.Update(stojou);

                    if (result.Success)
                    {
                        return Ok(result);
                    }

                    return BadRequest(result.ToString());
                }

                return BadRequest($"O objeto {nameof(stojou)} não pode ser null.");
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
