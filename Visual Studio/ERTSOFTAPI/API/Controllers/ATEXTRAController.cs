﻿using System;
using System.Net;
using MODELS.Helpers;
using System.Web.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using MODELS.BusinessObject.SAGEX3.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;

namespace API.Controllers
{
    [Authorize]
    public class ATEXTRAController : ApiController
    {
        private readonly IAtextraBLL atextraBLL;

        public ATEXTRAController(IAtextraBLL atextraBLL)
        {
            this.atextraBLL = atextraBLL;
        }

        // GET: api/APP
        public async Task<IHttpActionResult> Get()
        {
            try
            {
                Result<IEnumerable<ATEXTRA>> atextraListResult = await atextraBLL.GetAll();

                if (atextraListResult.Success)
                {
                    return Ok(atextraListResult.Obj);
                }

                return Content(HttpStatusCode.NotFound, atextraListResult.ToString());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // GET: api/APP/5
        public async Task<IHttpActionResult> Get(int id)
        {
            try
            {
                Result<ATEXTRA> atextraResult = await atextraBLL.GetById(id);

                if (atextraResult.Success)
                {
                    return Ok(atextraResult.Obj);
                }

                return Content(HttpStatusCode.NotFound, atextraResult.ToString());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [Route("api/ATEXTRA/GetByUniqueFields")]
        public async Task<IHttpActionResult> GetByUniqueFields([FromUri] string CODFIC_0, [FromUri] string ZONE_0, [FromUri] string LANGUE_0, [FromUri] string IDENT_1, [FromUri] string IDENT_2)
        {
            try
            {
                Result<ATEXTRA> atextraResult = await atextraBLL.GetByUniqueFields(CODFIC_0, ZONE_0, LANGUE_0, IDENT_1, IDENT_2);

                if (atextraResult.Success)
                {
                    return Ok(atextraResult.Obj);
                }

                return Content(HttpStatusCode.NotFound, atextraResult.ToString());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
