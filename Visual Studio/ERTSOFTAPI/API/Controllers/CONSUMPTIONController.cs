﻿using System;
using System.Net;
using MODELS.Helpers;
using System.Web.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;

namespace API.Controllers
{
    [Authorize]
    public class CONSUMPTIONController : ApiController
    {
        private readonly IConsumptionBLL consumptionBLL;

        public CONSUMPTIONController(IConsumptionBLL consumptionBLL)
        {
            this.consumptionBLL = consumptionBLL;
        }

        public async Task<IHttpActionResult> Get()
        {
            try
            {
                Result<IEnumerable<CONSUMPTION>> consumptionListResult = await consumptionBLL.GetAll();

                if (consumptionListResult.Success)
                {
                    return Ok(consumptionListResult.Obj);
                }

                return Content(HttpStatusCode.NotFound, consumptionListResult.ToString());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        public async Task<IHttpActionResult> Get(int id)
        {
            try
            {
                Result<CONSUMPTION> consumptionResult = await consumptionBLL.GetById(id);

                if (consumptionResult.Success)
                {
                    return Ok(consumptionResult.Obj);
                }

                return Content(HttpStatusCode.NotFound, consumptionResult.ToString());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [Route("api/APP/GetByMfgnum")]
        public async Task<IHttpActionResult> GetByMfgnum([FromUri] string MFGNUM_0, [FromUri] int REGFLG_0)
        {
            try
            {
                Result<IEnumerable<CONSUMPTION>> consumptionListResult = await consumptionBLL.GetByMfgnum(MFGNUM_0, REGFLG_0);

                if (consumptionListResult.Success)
                {
                    return Ok(consumptionListResult.Obj);
                }

                return Content(HttpStatusCode.NotFound, consumptionListResult.ToString());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [Route("api/APP/GetByMfgnumItmref")]
        public async Task<IHttpActionResult> GetByMfgnumItmref([FromUri] string MFGNUM_0, [FromUri] string ITMREF_0, [FromUri] int REGFLG_0)
        {
            try
            {
                Result<IEnumerable<CONSUMPTION>> consumptionListResult = await consumptionBLL.GetByMfgnumItmref(MFGNUM_0, ITMREF_0, REGFLG_0);

                if (consumptionListResult.Success)
                {
                    return Ok(consumptionListResult.Obj);
                }

                return Content(HttpStatusCode.NotFound, consumptionListResult.ToString());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [Route("api/APP/GetByMfgnumSloori")]
        public async Task<IHttpActionResult> GetByMfgnumSloori([FromUri] string MFGNUM_0, [FromUri] string SLOORI_0, [FromUri] int REGFLG_0)
        {
            try
            {
                Result<IEnumerable<CONSUMPTION>> consumptionListResult = await consumptionBLL.GetByMfgnumSloori(MFGNUM_0, SLOORI_0, REGFLG_0);

                if (consumptionListResult.Success)
                {
                    return Ok(consumptionListResult.Obj);
                }

                return Content(HttpStatusCode.NotFound, consumptionListResult.ToString());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
