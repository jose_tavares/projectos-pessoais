﻿using System;
using System.Net;
using MODELS.Helpers;
using System.Web.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using MODELS.BusinessObject.SAGEX3.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;

namespace API.Controllers
{
    [Authorize]
    public class ATABDIVController : ApiController
    {
        private readonly IAtabdivBLL atabdivBLL;

        public ATABDIVController(IAtabdivBLL atabdivBLL)
        {
            this.atabdivBLL = atabdivBLL;
        }

        // GET: api/APP
        public async Task<IHttpActionResult> Get()
        {
            try
            {
                Result<IEnumerable<ATABDIV>> atabDivListResult = await atabdivBLL.GetAll();

                if (atabDivListResult.Success)
                {
                    return Ok(atabDivListResult.Obj);
                }

                return Content(HttpStatusCode.NotFound, atabDivListResult.ToString());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // GET: api/APP/5
        public async Task<IHttpActionResult> Get(int id)
        {
            try
            {
                Result<ATABDIV> atabdivResult = await atabdivBLL.GetById(id);

                if (atabdivResult.Success)
                {
                    return Ok(atabdivResult.Obj);
                }

                return Content(HttpStatusCode.NotFound, atabdivResult.ToString());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [Route("api/ATABDIV/GetByNumtab")]
        public async Task<IHttpActionResult> GetByNumtab([FromUri] short NUMTAB_0)
        {
            try
            {
                Result<IEnumerable<ATABDIV>> atabdivResult = await atabdivBLL.GetByNumtab(NUMTAB_0);

                if (atabdivResult.Success)
                {
                    return Ok(atabdivResult.Obj);
                }

                return Content(HttpStatusCode.NotFound, atabdivResult.ToString());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [Route("api/ATABDIV/GetByUniqueFields")]
        public async Task<IHttpActionResult> GetByUniqueFields([FromUri] short NUMTAB_0, [FromUri] string CODE_0)
        {
            try
            {
                Result<ATABDIV> atabdivResult = await atabdivBLL.GetByUniqueFields(NUMTAB_0, CODE_0);

                if (atabdivResult.Success)
                {
                    return Ok(atabdivResult.Obj);
                }

                return Content(HttpStatusCode.NotFound, atabdivResult.ToString());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
