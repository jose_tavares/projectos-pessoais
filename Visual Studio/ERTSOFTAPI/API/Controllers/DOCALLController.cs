﻿using System;
using System.Net;
using MODELS.Helpers;
using System.Web.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;

namespace API.Controllers
{
    [Authorize]
    public class DOCALLController : ApiController
    {
        private readonly IDocAllBLL docAllBLL;

        public DOCALLController(IDocAllBLL docAllBLL)
        {
            this.docAllBLL = docAllBLL;
        }

        public async Task<IHttpActionResult> Get()
        {
            try
            {
                Result<IEnumerable<DOCALL>> docAllListResult = await docAllBLL.GetAll();

                if (docAllListResult.Success)
                {
                    return Ok(docAllListResult.Obj);
                }

                return Content(HttpStatusCode.NotFound, docAllListResult.ToString());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [Route("api/DOCALL/GetDocAllAvailable")]
        public async Task<IHttpActionResult> GetDocAllAvailable([FromUri] string DOCUMENT_0)
        {
            try
            {
                Result<IEnumerable<DOCALL>> docAllListResult = await docAllBLL.GetDocAllAvailable(DOCUMENT_0);

                if (docAllListResult.Success)
                {
                    return Ok(docAllListResult.Obj);
                }

                return Content(HttpStatusCode.NotFound, docAllListResult.ToString());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [Route("api/DOCALL/GetDocItmAllAvailable")]
        public async Task<IHttpActionResult> GetDocItmAllAvailable([FromUri] string STOFCY_0, [FromUri] string DOCUMENT_0, [FromUri] string ITMREF_0)
        {
            try
            {
                Result<IEnumerable<DOCALL>> docAllListResult = await docAllBLL.GetDocItmAllAvailable(STOFCY_0, DOCUMENT_0, ITMREF_0);

                if (docAllListResult.Success)
                {
                    return Ok(docAllListResult.Obj);
                }

                return Content(HttpStatusCode.NotFound, docAllListResult.ToString());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        public async Task<IHttpActionResult> Get(int id)
        {
            try
            {
                Result<DOCALL> atabdivResult = await docAllBLL.GetById(id);

                if (atabdivResult.Success)
                {
                    return Ok(atabdivResult.Obj);
                }

                return Content(HttpStatusCode.NotFound, atabdivResult.ToString());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        public async Task<IHttpActionResult> Post([FromBody] DOCALL docAll)
        {
            try
            {
                Result<DOCALL> result = await docAllBLL.Insert(docAll);

                if (result.Success)
                {
                    return Ok(result);
                }

                return BadRequest(result.ToString());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // PUT: api/APP/5
        public async Task<IHttpActionResult> Put([FromBody] DOCALL docAll)
        {
            try
            {
                Result<DOCALL> result = await docAllBLL.Update(docAll);

                if (result.Success)
                {
                    return Ok(result);
                }

                return BadRequest(result.ToString());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
