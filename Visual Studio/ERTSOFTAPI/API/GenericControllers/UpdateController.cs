﻿namespace API.GenericControllers
{
    using System;
    using MODELS.Helpers;
    using System.Web.Http;
    using System.Threading.Tasks;
    using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Generic;

    public abstract class UpdateController<T> : CreateController<T> where T : class
    {
        private readonly IUpdateBLL<T> updateBLL;

        protected UpdateController(IUpdateBLL<T> updateBLL) : base(updateBLL)
        {
            this.updateBLL = updateBLL;
        }

        public async Task<IHttpActionResult> Put([FromBody] T t)
        {
            try
            {
                if (t != null)
                {
                    Result<T> result = await updateBLL.Update(t);

                    if (result.Success)
                    {
                        return Ok(result);
                    }

                    return BadRequest(result.ToString());
                }

                return BadRequest($"O objeto {nameof(t)} não pode ser null.");
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
