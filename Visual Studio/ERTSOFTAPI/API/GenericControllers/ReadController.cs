﻿namespace API.GenericControllers
{
    using System;
    using System.Net;
    using MODELS.Helpers;
    using System.Web.Http;
    using System.Threading.Tasks;
    using System.Collections.Generic;
    using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Generic;

    [Authorize]
    public abstract class ReadController<T> : ApiController where T : class
    {
        private readonly IReadBLL<T> readBLL;

        protected ReadController(IReadBLL<T> readBLL)
        {
            this.readBLL = readBLL;
        }

        public async Task<IHttpActionResult> Get()
        {
            try
            {
                Result<IEnumerable<T>> genericListResult = await readBLL.GetAll();

                if (genericListResult.Success)
                {
                    return Ok(genericListResult);
                }

                return Content(HttpStatusCode.NotFound, genericListResult.ToString());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        public async Task<IHttpActionResult> Get(int id)
        {
            try
            {
                Result<T> tResult = await readBLL.GetById(id);

                if (tResult.Success)
                {
                    return Ok(tResult.Obj);
                }

                return Content(HttpStatusCode.NotFound, tResult.ToString());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
