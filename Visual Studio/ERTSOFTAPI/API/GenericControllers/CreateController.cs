﻿namespace API.GenericControllers
{
    using System;
    using MODELS.Helpers;
    using System.Web.Http;
    using System.Threading.Tasks;
    using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Generic;

    public abstract class CreateController<T> : ReadController<T> where T : class
    {
        private readonly ICreateBLL<T> createBLL;

        public CreateController(ICreateBLL<T> createBLL) : base(createBLL)
        {
            this.createBLL = createBLL;
        }

        public async Task<IHttpActionResult> Post([FromBody] T t)
        {
            try
            {
                if (t != null)
                {
                    Result<T> result = await createBLL.Insert(t);

                    if (result.Success)
                    {
                        return Ok(result);
                    }

                    return BadRequest(result.ToString());
                }

                return BadRequest($"O objeto {nameof(t)} não pode ser null.");
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
