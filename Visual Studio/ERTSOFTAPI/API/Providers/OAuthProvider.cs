﻿using DATA.Ninject;
using MODELS.Helpers;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.Owin.Security;
using System.Collections.Generic;
using Microsoft.Owin.Security.OAuth;
using MODELS.BusinessObject.ERTSOFT.Database;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;

namespace API.Providers
{
    public class OAuthProvider : OAuthAuthorizationServerProvider
    {
        #region[GrantResourceOwnerCredentials]
        public override Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            return Task.Factory.StartNew(() =>
            {
                if (!string.IsNullOrEmpty(context.UserName) && !string.IsNullOrEmpty(context.Password))
                {
                    IUserBLL userBLL = NinjectWebCommon.GetInstance<IUserBLL>();
                    Result<USER> result = AsyncHelper.RunSync(() => userBLL.Login(context.UserName, context.Password));

                    if (result.Success)
                    {
                        USER user = result.Obj;

                        List<Claim> claims = new List<Claim>()
                        {
                            new Claim(nameof(USER.ROWID), user.ROWID.ToString()),
                            new Claim(nameof(USER.USR_0), user.USR_0),
                            new Claim(nameof(USER.USERNAM_0), user.USERNAM_0),
                            new Claim(nameof(USER.PROFILE_0), user.PROFILE_0)
                        };

                        ClaimsIdentity oAuthIdentity = new ClaimsIdentity(claims, Startup.OAuthOptions.AuthenticationType);
                        AuthenticationProperties properties = CreateProperties(claims);
                        AuthenticationTicket ticket = new AuthenticationTicket(oAuthIdentity, properties);
                        context.Validated(ticket);
                    }
                    else
                    {
                        context.SetError("InvalidAccess", result.ToString());
                    }
                }
                else
                {
                    context.SetError("BadRequest", $"Os parametros {nameof(context.UserName)}/{nameof(context.Password)} não podem ser null ou vazio.");
                }
            });
        }
        #endregion

        #region[ValidateClientAuthentication]
        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            if (context.ClientId == null)
            {
                context.Validated();
            }

            return Task.FromResult<object>(null);
        }
        #endregion

        #region[TokenEndpoint]
        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }
        #endregion

        #region[CreateProperties]
        public static AuthenticationProperties CreateProperties(List<Claim> claims)
        {
            IDictionary<string, string> data = new Dictionary<string, string>();

            foreach (Claim claim in claims)
            {
                data.Add(claim.Type, claim.Value);
            }

            return new AuthenticationProperties(data);
        }
        #endregion
    }
}