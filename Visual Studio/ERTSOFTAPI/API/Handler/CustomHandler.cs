﻿using System.Net;
using System.Net.Http;
using System.Threading;
using System.Diagnostics;
using System.Threading.Tasks;
using DATA.Helpers.StaticClasses;

namespace API.Handler
{
    public class CustomHandler : DelegatingHandler
    {
        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            if (!IsApiRequest(request))
            {
                return await base.SendAsync(request, cancellationToken);
            }

            return await ProcessHttpResponseMessage(request, cancellationToken);
        }

        private async Task<HttpResponseMessage> ProcessHttpResponseMessage(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            string requestBody = string.Empty;
            string responseBody = string.Empty;

            if (request.Content != null)
            {
                requestBody = await request.Content.ReadAsStringAsync();
            }

            Stopwatch sw = Stopwatch.StartNew();
            HttpResponseMessage result = request.Headers.Contains("AppName")
                ? await base.SendAsync(request, cancellationToken)
                : request.CreateErrorResponse(HttpStatusCode.BadRequest, "AppName não encontrada nos headers.");

            sw.Stop();

            if (result.Content != null)
            {
                responseBody = await result.Content.ReadAsStringAsync();
            }

            if (!responseBody.Contains("nenhum registo pendente"))
            {
                await LogHelper.LogHttpRequest(request, result.StatusCode, requestBody, responseBody, sw.ElapsedMilliseconds);
            }

            return result;
        }

        private bool IsApiRequest(HttpRequestMessage request)
        {
            return request.RequestUri.PathAndQuery.StartsWith("/api");
        }
    }
}