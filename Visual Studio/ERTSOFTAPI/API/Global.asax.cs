﻿namespace API
{
    using System.Web;
    using DATA.Ninject;
    using System.Web.Mvc;
    using Newtonsoft.Json;
    using System.Web.Http;
    using System.Web.Routing;

    public class WebApiApplication : HttpApplication
    {
        protected void Application_Start() //Primeiro método iniciado
        {
            AreaRegistration.RegisterAllAreas(); //Regista todas as áreas MVC do projeto. (Controllers, Views, Models)
            GlobalConfiguration.Configure(WebApiConfig.Register); //Regista configurações ao nível do web API, mapas das rotas, deletage handlers
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters); //Regista os filtros sobre os requests feitos à web API
            RouteConfig.RegisterRoutes(RouteTable.Routes); //Regista as rotas
            NinjectWebCommon.RegisterNinject(GlobalConfiguration.Configuration); //Regista o ninject, biblioteca necessária para gestão de injeção de dependências do código
            MvcHandler.DisableMvcResponseHeader = true; //Desativa o cabeçalho no browser por questões de segurança
            JsonConvert.DefaultSettings = () => new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            };
        }

        protected void Application_PreSendRequestHeaders() //Método necessário para controlar o envio da informação dos cabeçalhos nos requests
        {
            if (HttpContext.Current != null)
            {
                HttpContext.Current.Response.Headers.Remove("Server"); //Remove o nome do servidor que é enviado no cabeçalho por questões de segurança
            }
        }
    }
}
