﻿using MODELS.Attributes;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestERTSOFTAPI.MODELS.Attributes
{
    [TestClass]
    public class DecimalAttributeUnitTest
    {
        [TestMethod]
        public void DecimalAttributePrecisionValidTestMethod()
        {
            DecimalAttribute decimalAttribute = new DecimalAttribute(28, 13);
            decimal testValue = 1234567891234567.50m;
            bool result = decimalAttribute.IsValid(testValue);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void DecimalAttributeScaleValidTestMethod()
        {
            DecimalAttribute decimalAttribute = new DecimalAttribute(28, 13);
            decimal testValue = 1234567891234567.123456789m;
            bool result = decimalAttribute.IsValid(testValue);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void DecimalAttributeMaxPrecisionValidTestMethod()
        {
            DecimalAttribute decimalAttribute = new DecimalAttribute(28, 13);
            decimal testValue = 123456789123456789123456.5023m;
            bool result = decimalAttribute.IsValid(testValue);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void DecimalAttributeMaxScaleValidTestMethod()
        {
            DecimalAttribute decimalAttribute = new DecimalAttribute(28, 13);
            decimal testValue = 12345678912345.5012345678912m;
            bool result = decimalAttribute.IsValid(testValue);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void DecimalAttributeMinPrecisionValidTestMethod()
        {
            DecimalAttribute decimalAttribute = new DecimalAttribute(28, 13);
            decimal testValue = 1m;
            bool result = decimalAttribute.IsValid(testValue);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void DecimalAttributeMinScaleValidTestMethod()
        {
            DecimalAttribute decimalAttribute = new DecimalAttribute(28, 13);
            decimal testValue = 12345678912345m;
            bool result = decimalAttribute.IsValid(testValue);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void DecimalAttributePrecisionInvalidTestMethod()
        {
            DecimalAttribute decimalAttribute = new DecimalAttribute(28, 13);
            decimal testValue = 12311234567891234567.123456789m;
            bool result = decimalAttribute.IsValid(testValue);

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void DecimalAttributeScaleInvalidTestMethod()
        {
            DecimalAttribute decimalAttribute = new DecimalAttribute(28, 13);
            decimal testValue = 23112334567.12345678912345m;
            bool result = decimalAttribute.IsValid(testValue);

            Assert.IsFalse(result);
        }
    }
}
