﻿using DATA.Helpers.StaticClasses;
using System;
using System.Net;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestERTSOFTAPI.DATA.Helpers
{
    [TestClass]
    public class EnumHelperUnitTest
    {
        [TestMethod]
        public void GetEnumValuesTestMethodIsEnumFalse()
        {
            void getEnumValuesFail() => EnumHelper.GetEnumValues<int>();

            Assert.ThrowsException<ArgumentException>(getEnumValuesFail);
        }

        [TestMethod]
        public void GetEnumValuesTestMethodIsEnumTrue()
        {
            HttpStatusCode[] statuscodes = EnumHelper.GetEnumValues<HttpStatusCode>();

            Assert.IsTrue(statuscodes.IsValid());
        }
    }
}
