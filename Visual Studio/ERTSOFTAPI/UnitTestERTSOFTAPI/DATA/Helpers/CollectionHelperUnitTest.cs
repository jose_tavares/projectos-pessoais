﻿using Bogus;
using System;
using DATA.Helpers.StaticClasses;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestERTSOFTAPI.DATA.Helpers
{
    [TestClass]
    public class CollectionHelperUnitTest
    {
        #region Add
        [TestMethod]
        public void AddTestMethodOneElement()
        {
            string[] newArray = new string[0];
            int initialLength = newArray.Length;

            newArray = newArray.Add(GenerateRandomNames());
            int finalLength = newArray.Length;

            Assert.IsTrue(initialLength < finalLength);
        }

        [TestMethod]
        public void AddTestMethodMultipleElements()
        {
            string[] newArray = new string[0];
            int testNumber = GenerateRandomNumber(10, 20);

            for (int i = 0; i < testNumber; i++)
            {
                newArray = newArray.Add(GenerateRandomNames());
            }

            Assert.IsTrue(newArray.Length == testNumber);
        }
        #endregion

        #region IsValid
        [TestMethod]
        public void IsValidTestMethodTrueWithoutPredicate()
        {
            List<string> list = new List<string>
            {
                GenerateRandomNames()
            };

            Assert.IsTrue(list.IsValid());
        }

        [TestMethod]
        public void IsValidTestMethodTrueWithPredicate()
        {
            List<int> list = new List<int>();
            int random = GenerateRandomNumber(1, 9);

            for (int i = 0; i < 10; i++)
            {
                list.Add(i);
            }

            Assert.IsTrue(list.IsValid(item => item == random));
        }

        [TestMethod]
        public void IsValidTestMethodNullWithoutPredicate()
        {
            List<string> list = null;

            Assert.IsFalse(list.IsValid());
        }

        [TestMethod]
        public void IsValidTestMethodEmptyWithoutPredicate()
        {
            List<string> list = new List<string>();

            Assert.IsFalse(list.IsValid());
        }

        [TestMethod]
        public void IsValidTestMethodEmptyWithPredicate()
        {
            List<int> list = new List<int>();
            int random = GenerateRandomNumber(11, 20);

            for (int i = 0; i < 10; i++)
            {
                list.Add(i);
            }

            Assert.IsFalse(list.IsValid(item => item == random));
        }
        #endregion

        #region Private Methods
        private string GenerateRandomNames()
        {
            return new Faker("pt_PT").Parse("{{name.fullname}}");
        }

        private int GenerateRandomNumber(int minValue, int maxValue)
        {
            return new Random().Next(minValue, maxValue);
        }
        #endregion
    }
}
