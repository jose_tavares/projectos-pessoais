﻿using DATA.Ninject;
using MODELS.Helpers;
using System.Threading.Tasks;
using DATA.Helpers.StaticClasses;
using MODELS.BusinessObject.ERTSOFT.Database;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;

namespace UnitTestERTSOFTAPI.DATA.BLL
{
    [TestClass]
    public class ModuleBLLUnitTest
    {
        [TestInitialize]
        public void TestInitialize()
        {
            NinjectWebCommon.Start();
        }

        [TestCleanup]
        public void TestCleanup()
        {
            NinjectWebCommon.Stop();
        }

        [TestMethod]
        public async Task GetByAppWithModuleListTestMethod()
        {
            IModuleBLL appBLL = NinjectWebCommon.GetInstance<IModuleBLL>();

            Result<MODULE> moduleResult = await appBLL.GetByUniqueFieldsWithScreenList("ERTSERVICE", "IT");
            MODULE moduleModel = moduleResult.Obj;

            Assert.IsNotNull(moduleModel);
            Assert.IsNotNull(moduleModel.ScreenList.IsValid());
        }
    }
}
