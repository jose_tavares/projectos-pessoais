﻿using DATA.Ninject;
using MODELS.Helpers;
using System.Threading.Tasks;
using DATA.Helpers.StaticClasses;
using MODELS.BusinessObject.ERTSOFT.Database;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;

namespace UnitTestERTSOFTAPI.DATA.BLL
{
    [TestClass]
    public class ProfileBLLUnitTest
    {
        [TestInitialize]
        public void TestInitialize()
        {
            NinjectWebCommon.Start();
        }

        [TestCleanup]
        public void TestCleanup()
        {
            NinjectWebCommon.Stop();
        }

        [TestMethod]
        public async Task GetByProfileModelWithUserListTest()
        {
            IProfileBLL appBLL = NinjectWebCommon.GetInstance<IProfileBLL>();

            Result<PROFILE> profileResult = await appBLL.GetByUniqueFieldWithUserList("ADMIN");
            PROFILE profile = profileResult.Obj;

            Assert.IsNotNull(profile);
            Assert.IsTrue(profile.UserList.IsValid());
        }
    }
}
