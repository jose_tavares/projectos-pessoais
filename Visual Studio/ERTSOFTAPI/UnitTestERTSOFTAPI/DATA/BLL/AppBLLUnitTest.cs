﻿using DATA.Ninject;
using MODELS.Helpers;
using System.Threading.Tasks;
using DATA.Helpers.StaticClasses;
using MODELS.BusinessObject.ERTSOFT.Database;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;

namespace UnitTestERTSOFTAPI.DATA.BLL
{
    [TestClass]
    public class AppBLLUnitTest
    {
        [TestInitialize]
        public void TestInitialize()
        {
            NinjectWebCommon.Start();
        }

        [TestCleanup]
        public void TestCleanup()
        {
            NinjectWebCommon.Stop();
        }

        [TestMethod]
        public async Task GetByAppWithModuleListTestMethod()
        {
            IAppBLL appBLL = NinjectWebCommon.GetInstance<IAppBLL>();

            Result<APP> appResult = await appBLL.GetByUniqueFieldWithModuleList("ERTSERVICE");
            APP app = appResult.Obj;

            Assert.IsNotNull(app);
            Assert.IsTrue(app.ModuleList.IsValid());
        }
    }
}
