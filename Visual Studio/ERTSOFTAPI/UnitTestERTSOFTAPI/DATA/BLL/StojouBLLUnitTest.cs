﻿using DATA.Ninject;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestERTSOFTAPI.DATA.BLL
{
    [TestClass]
    public class StojouBLLUnitTest
    {
        [TestInitialize]
        public void TestInitialize()
        {
            NinjectWebCommon.Start();
        }

        [TestCleanup]
        public void TestCleanup()
        {
            NinjectWebCommon.Stop();
        }

        //[TestMethod]
        //public async Task ImportPendingTestMethod()
        //{
        //    IStojouBLL stojouBLL = NinjectWebCommon.GetInstance<IStojouBLL>();
        //    IEnumerable<STOJOU> stojouList = await stojouBLL.GetAll();

        //    STOJOU stojou = stojouList.FirstOrDefault(s => s.SUCCEEDED_0 == 1);

        //    if (stojou != null)
        //    {
        //        if (stojou.ATTEMPTS_0 > 10)
        //        {
        //            stojou.ATTEMPTS_0 -= 1;
        //            await stojouBLL.Update(stojou);
        //        }
        //    }

        //    IEnumerable<WebServiceData> webServiceDataList = await stojouBLL.ImportPending();

        //    Assert.IsTrue(webServiceDataList.Count() > 0);
        //}
    }
}
