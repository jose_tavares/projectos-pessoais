﻿using DATA.Ninject;
using API.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DATA.LayeredArquiteture.BusinessLogic.Interfaces.Concrete;
using System.Threading.Tasks;
using System.Web.Http;
using System.Linq;
using System.Collections.Generic;
using MODELS.BusinessObject.ERTSOFT.Database;
using System.Web.Http.Results;

namespace UnitTestERTSOFTAPI.API
{
    [TestClass]
    public class USERControllerUnitTest
    {
        [TestInitialize]
        public void TestInitialize()
        {
            NinjectWebCommon.Start();
        }

        [TestCleanup]
        public void TestCleanup()
        {
            NinjectWebCommon.Stop();
        }

        [TestMethod]
        public async Task GetAll()
        {
            IUserBLL testUsers = NinjectWebCommon.GetInstance<IUserBLL>();
            USERController controller = new USERController(testUsers);

            IHttpActionResult actionResult = await controller.Get();
            Assert.AreEqual((await testUsers.GetAll()).Count(), ((OkNegotiatedContentResult<IEnumerable<USER>>)actionResult).Content.Count());
        }
    }
}
