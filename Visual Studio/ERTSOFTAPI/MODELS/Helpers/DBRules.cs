﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MODELS.Attributes;

namespace MODELS.Helpers
{
    public class DBRules
    {
        #region Properties
        [NotMapped]
        public static IReadOnlyCollection<string> ValidPropsOnCreate = new List<string>
        {
            nameof(CREDAT_0),
            nameof(CREUSR_0),
            nameof(UPDDAT_0),
            nameof(UPDUSR_0),
            nameof(CREDATTIM_0),
            nameof(UPDDATTIM_0),
            nameof(ROWID)
        };

        [NotMapped]
        public static IReadOnlyCollection<string> ValidPropsOnUpdate = new List<string>
        {
            nameof(CREDAT_0),
            nameof(CREUSR_0),
            nameof(UPDDAT_0),
            nameof(UPDUSR_0),
            nameof(CREDATTIM_0),
            nameof(UPDDATTIM_0)
        };

        [NotMapped]
        public static IReadOnlyCollection<string> IgnorePropsOnUpdate = new List<string>
        {
            nameof(CREDAT_0),
            nameof(CREUSR_0),
            nameof(CREDATTIM_0),
            nameof(ROWID)
        };

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [SqlDefaultValue(DefaultValue = "getdate()")]
        public DateTime CREDAT_0 { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 1)]
        public string CREUSR_0 { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [SqlDefaultValue(DefaultValue = "getdate()")]
        public DateTime UPDDAT_0 { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 1)]
        public string UPDUSR_0 { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [SqlDefaultValue(DefaultValue = "getdate()")]
        public DateTime CREDATTIM_0 { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [SqlDefaultValue(DefaultValue = "getdate()")]
        public DateTime UPDDATTIM_0 { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Decimal(38, 1)]
        [Range(1, int.MaxValue)]
        [Key]
        public int ROWID { get; set; }

        #endregion

        #region Constructors

        public DBRules()
        {
            ROWID = 0;
        }

        public DBRules(int ID)
        {
            ROWID = ID;
        }

        #endregion
    }
}
