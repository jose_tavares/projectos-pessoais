﻿namespace MODELS.Helpers
{
    using System;
    using System.Collections.Generic;

    public class Result<T> where T : class
    {
        #region Properties
        public bool Success { get; set; }

        public List<string> Messages { get; private set; }

        public int InsertedId { get; set; }

        public int AffectedRows { get; set; }

        public T Obj { get; set; }

        #endregion

        #region Constructors
        public Result()
        {
            Messages = new List<string>();
            InsertedId = -1;
            AffectedRows = -1;
        }

        public Result(bool success)
        {
            Messages = new List<string>();
            Success = success;
        }
        #endregion

        #region Public Methods
        public bool IsInserted(int id, T obj, int affectedRows = 1)
        {
            InsertedId = id;
            Obj = obj;
            AffectedRows = affectedRows;

            if (id > 0)
                Inserted(id);
            else
                NotInserted();

            return Success;
        }

        public bool IsUpdated(int affectedRows, T obj)
        {
            AffectedRows = affectedRows;
            Obj = obj;

            if (AffectedRows > 0)
                Updated();
            else
                NotUpdated();

            return Success;
        }

        public bool IsDeleted(int affectedRows)
        {
            AffectedRows = affectedRows;

            if (AffectedRows > 0)
                Deleted();
            else
                NotDeleted();

            return Success;
        }

        public void AddMessage(string message)
        {
            Messages.Add(message);
        }

        public void AddValidMessage(string message)
        {
            Success = true;
            Messages.Add(message);
        }

        public void AddInvalidMessage(string message)
        {
            Success = false;
            Messages.Add(message);
        }

        public void NotFound()
        {
            Type type = typeof(T);

            if (ClassHelper.IsCollectionType(type) || ClassHelper.IsEnumerableType(type))
            {
                type = type.GetGenericArguments()[0];
            }

            AddInvalidMessage($"Não existem modelos '{type.Name}' disponíveis na BD.");
        }

        public void NotFound(int id)
        {
            Type type = typeof(T);

            if (ClassHelper.IsCollectionType(type) || ClassHelper.IsEnumerableType(type))
            {
                type = type.GetGenericArguments()[0];
            }

            AddInvalidMessage($"Não foi possível encontrar o(s) modelo(s) '{type.Name}' para o filtro aplicado.");
            AddInvalidMessage($"{nameof(DBRules.ROWID)} = '{id}'");
        }

        public void NotFound(string field, object value)
        {
            Type type = typeof(T);

            if (ClassHelper.IsCollectionType(type) || ClassHelper.IsEnumerableType(type))
            {
                type = type.GetGenericArguments()[0];
            }

            AddInvalidMessage($"Não foi possível encontrar o(s) modelo(s) '{type.Name}' para o filtro aplicado.");
            AddInvalidMessage($"{field} = '{value}'");
        }

        public void NotFound(Dictionary<string, object> fields)
        {
            foreach (KeyValuePair<string, object> field in fields)
            {
                AddMessage($"{field.Key} = '{field.Value}'");
            }

            Type type = typeof(T);

            if (ClassHelper.IsCollectionType(type) || ClassHelper.IsEnumerableType(type))
            {
                type = type.GetGenericArguments()[0];
            }

            Messages.Insert(0, $"Não foi possível encontrar o(s) modelo(s) '{type.Name}' para os filtros aplicados.");
        }

        public void ConvertFromResult<T1>(Result<T1> result, bool keepOldMessages = false) where T1 : class
        {
            InsertedId = result.InsertedId;
            AffectedRows = result.AffectedRows;
            Success = result.Success;

            if (!keepOldMessages)
            {
                Messages = result.Messages;
            }
            else
            {
                for (int i = 0; i < result.Messages.Count; i++)
                {
                    Messages.Insert(i, result.Messages[i]);
                }
            }                   
        }

        public static Result<T> ConvertAndGetFromResult<T1>(Result<T1> result) where T1 : class
        {
            return new Result<T>(true)
            {
                InsertedId = result.InsertedId,
                AffectedRows = result.AffectedRows,
                Success = result.Success,
                Messages = result.Messages
            };
        }

        public override string ToString()
        {
            string finalMessage = string.Empty;

            foreach (string message in Messages)
            {
                var index = Messages.IndexOf(message);

                if (index == Messages.Count - 1)
                {
                    finalMessage += message;
                    break;
                }

                finalMessage += message + Environment.NewLine;
            }

            return finalMessage;
        }
        #endregion

        #region Private Methods
        private void Inserted(int id)
        {
            AddValidMessage("O recurso solicitado foi criado com sucesso com o ID " + id);
        }

        private void NotInserted()
        {
            AddInvalidMessage("Ocorreu um problema ao criar o recurso solicitado.");
        }

        private void Updated()
        {
            AddValidMessage("O recurso solicitado foi atualizado com sucesso.");
        }

        private void NotUpdated()
        {
            AddInvalidMessage("Ocorreu um problema ao atualizar o recurso solicitado.");
        }

        private void Deleted()
        {
            AddValidMessage("O recurso solicitado foi eliminado com sucesso.");
        }

        private void NotDeleted()
        {
            AddInvalidMessage("Ocorreu um problema ao eliminar o recurso solicitado.");
        }
        #endregion
    }
}
