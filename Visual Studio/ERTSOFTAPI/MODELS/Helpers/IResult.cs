﻿using System.Collections.Generic;

namespace MODELS.Helpers
{
    public interface IResult<T> where T : class
    {
        #region Public Properties
        int AffectedRows { get; set; }

        decimal InsertedId { get; set; }
        
        List<string> Messages { get; }
        
        T Obj { get; set; }
        
        bool Success { get; set; }
        #endregion

        #region Public Methods
        void AddInvalidMessage(string message);
        
        void AddMessage(string message);
        
        void AddValidMessage(string message);
        
        void ConvertFromResult<T1>(IResult<T1> result) where T1 : class;
        
        bool IsDeleted(int affectedRows);
        
        bool IsInserted(decimal id, T obj, int affectedRows = 1);
        
        bool IsUpdated(int affectedRows, T obj);
        
        string ToString();
        #endregion
    }
}