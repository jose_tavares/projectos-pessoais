﻿using System;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using MODELS.Attributes;
using System.ComponentModel.DataAnnotations.Schema;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Collections;

namespace MODELS.Helpers
{
    public static class ClassHelper
    {
        public static bool IsSimpleType(Type type)
        {
            return Type.GetTypeCode(type) != TypeCode.Object;
        }

        public static bool IsEnumerableType(Type type)
        {
            return (type.GetInterface(nameof(IEnumerable)) != null);
        }

        public static bool IsCollectionType(Type type)
        {
            return (type.GetInterface(nameof(ICollection)) != null);
        }

        public static async Task<Result<T>> IsClassValidAsync<T>(this T obj) where T : class
        {
            return await Task.Run(() => obj.IsClassValid());
        }

        public static Result<T> IsClassValid<T>(this T obj) where T : class
        {
            Result<T> result = new Result<T>
            {
                Obj = obj
            };

            if (obj == null)
            {
                result.AddInvalidMessage($"O modelo '{typeof(T).Name}' não pode ser null.");

                return result;
            }

            List<ValidationResult> validationResults = new List<ValidationResult>();

            result.Success = Validator.TryValidateObject(obj, new ValidationContext(obj), validationResults, true);

            foreach (ValidationResult validationResult in validationResults)
            {
                result.AddInvalidMessage(validationResult.ErrorMessage);
            }

            return result;
        }

        public static async Task<Result<T>> IsPropertiesValidAsync<T>(this T obj, params string[] propNames) where T : class
        {
            return await Task.Run(() => obj.IsPropertiesValid(propNames));
        }

        public static Result<T> IsPropertiesValid<T>(this T obj, params string[] propNames) where T : class
        {
            Result<T> result = new Result<T>
            {
                Obj = obj
            };

            if (obj == null)
            {
                result.AddInvalidMessage($"O modelo '{typeof(T).Name}' não pode ser null.");

                return result;
            }

            List<ValidationResult> validationResults = new List<ValidationResult>();

            foreach (string propName in propNames)
            {
                Validator.TryValidateProperty(obj.GetType().GetProperty(propName).GetValue(obj), new ValidationContext(obj) { MemberName = propName }, validationResults);
            }

            if (validationResults.Count == 0)
            {
                result.Success = true;

                return result;
            }

            foreach (ValidationResult validationResult in validationResults)
            {
                result.AddInvalidMessage(validationResult.ErrorMessage);
            }

            return result;
        }

        public static Result<T> IsPropertiesValid<T>(Dictionary<string, object> fields) where T : class
        {
            Result<T> result = new Result<T>();
            List<ValidationResult> validationResults = new List<ValidationResult>();

            Type type = typeof(T);

            if (IsCollectionType(type) || IsEnumerableType(type))
            {
                type = type.GetGenericArguments()[0];
            }

            foreach (KeyValuePair<string, object> field in fields)
            {
                Validator.TryValidateProperty(field.Value, new ValidationContext(Activator.CreateInstance(type)) { MemberName = field.Key }, validationResults);
            }

            if (validationResults.Count == 0)
            {
                result.Success = true;

                return result;
            }

            foreach (ValidationResult validationResult in validationResults)
            {
                result.AddInvalidMessage(validationResult.ErrorMessage);
            }

            return result;
        }

        public static async Task<Result<T>> IsValueValidAsync<T>(this object value, string propName) where T : class
        {
            return await Task.Run(() => value.IsValueValid<T>(propName));
        }

        public static Result<T> IsValueValid<T>(this object value, string propName) where T : class
        {
            Result<T> result = new Result<T>();

            if (value == null)
            {
                result.AddInvalidMessage($"O valor da propriedade '{propName}' não pode ser null.");

                return result;
            }

            Type type = typeof(T);

            if (IsCollectionType(type) || IsEnumerableType(type))
            {
                type = type.GetGenericArguments()[0];
            }

            List<ValidationResult> validationResults = new List<ValidationResult>();
            IEnumerable<ValidationAttribute> attributes = type.GetProperty(propName).GetCustomAttributes(false).OfType<ValidationAttribute>().ToArray();

            result.Success = Validator.TryValidateValue(value, new ValidationContext(value) { MemberName = propName }, validationResults, attributes);

            foreach (ValidationResult validationResult in validationResults)
            {
                result.AddInvalidMessage(validationResult.ErrorMessage);
            }

            return result;
        }

        public static Result<object> GetPropValueResult(this object obj, string propName)
        {
            Result<object> result = new Result<object>(true);

            foreach (string name in propName.Split('.'))
            {
                if (obj == null)
                {
                    result.AddInvalidMessage("O parametro obj não pode ser null.");
                    return result;
                }

                PropertyInfo info = obj.GetType().GetProperty(name);

                if (info == null)
                {
                    result.AddInvalidMessage($"A propriedade '{propName}' não existe na classe '{obj.GetType().Name}'");
                    return result;
                }

                result.Obj = info.GetValue(obj, null);
            }

            return result;
        }

        public static object GetPropValue(this object obj, string propName)
        {
            foreach (string name in propName.Split('.'))
            {
                if (obj == null)
                {
                    return null;
                }

                PropertyInfo info = obj.GetType().GetProperty(name);

                if (info == null)
                {
                    return null;
                }

                obj = info.GetValue(obj, null);
            }

            return obj;
        }

        public static IEnumerable<PropertyInfo> GetSimpleProperties<T>()
        {
            List<PropertyInfo> propertyList = new List<PropertyInfo>();

            foreach (PropertyInfo property in typeof(T).GetProperties())
            {
                if (IsSimpleType(property.PropertyType))
                {
                    propertyList.Add(property);
                }
            }

            return propertyList;
        }

        public static object GetPropertyDefaultValue(this PropertyInfo property)
        {
            Attribute defaultAttr = property.GetCustomAttribute(typeof(DefaultValueAttribute));

            if (defaultAttr != null)
            {
                return (defaultAttr as DefaultValueAttribute).Value;
            }

            var propertyType = property.PropertyType;

            return propertyType.IsValueType ? Activator.CreateInstance(propertyType) : null;
        }

        public static Result<Dictionary<string, object>> GetDictionaryFieldsFromObject<T>(T obj, params string[] propNames) where T : class
        {
            Result<Dictionary<string, object>> resultValueFields = new Result<Dictionary<string, object>>(true);

            Type type = typeof(T);

            if (IsCollectionType(type) || IsEnumerableType(type))
            {
                type = type.GetGenericArguments()[0];
            }

            if (obj == null)
            {
                resultValueFields.AddInvalidMessage($"O modelo '{type.Name}' não pode ser null.");

                return resultValueFields;
            }

            if (propNames == null || propNames.Length == 0)
            {
                resultValueFields.AddInvalidMessage("Não foram enviados campos para aplicar filtros e por isso a pesquisa não irá ser efetuada.");

                return resultValueFields;
            }

            Dictionary<string, object> valueFields = new Dictionary<string, object>();

            foreach (string field in propNames)
            {
                Result<object> resultValue = obj.GetPropValueResult(field);

                if (!resultValue.Success)
                {
                    resultValueFields.AddInvalidMessage(resultValue.ToString());

                    continue;
                }

                valueFields.Add(field, resultValue.Obj);
            }

            Result<T> result = IsPropertiesValid<T>(valueFields);

            if (!result.Success)
            {
                resultValueFields.AddInvalidMessage(result.ToString());

                return resultValueFields;
            }

            resultValueFields.Obj = valueFields;
            return resultValueFields;
        }

        public static T GetObjectFromResult<T>(Result<T> resultT) where T : class
        {
            return resultT.Obj;
        }
    }
}