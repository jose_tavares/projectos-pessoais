﻿using System;
using MODELS.Helpers;
using MODELS.Attributes;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MODELS.BusinessObject.SAGEX3.Database
{
    // MFGITM
    [Table("[MFGITM]")]
    public partial class MFGITM : DBRules
    {
        [MaxLength(20)]
        [StringLength(20)]
        [Required(AllowEmptyStrings = true)]
        public string MFGNUM_0 { get; set; } // MFGNUM_0 (length: 20)

        [Required]
        public int MFGLIN_0 { get; set; }

        [MaxLength(20)]
        [StringLength(20)]
        [Required(AllowEmptyStrings = true)]
        public string ITMREF_0 { get; set; } // ITMREF_0 (length: 20)

        [Required]
        public byte MFGSTA_0 { get; set; } // MFGSTA_0

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string PLNFCY_0 { get; set; } // PLNFCY_0 (length: 5)

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string MFGFCY_0 { get; set; } // MFGFCY_0 (length: 5)

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string TCLCOD_0 { get; set; } // TCLCOD_0 (length: 5)

        [MaxLength(20)]
        [StringLength(20)]
        [Required(AllowEmptyStrings = true)]
        public string TSICOD_0 { get; set; } // TSICOD_0 (length: 20)

        [MaxLength(20)]
        [StringLength(20)]
        [Required(AllowEmptyStrings = true)]
        public string TSICOD_1 { get; set; } // TSICOD_1 (length: 20)

        [MaxLength(20)]
        [StringLength(20)]
        [Required(AllowEmptyStrings = true)]
        public string TSICOD_2 { get; set; } // TSICOD_2 (length: 20)

        [MaxLength(20)]
        [StringLength(20)]
        [Required(AllowEmptyStrings = true)]
        public string TSICOD_3 { get; set; } // TSICOD_3 (length: 20)

        [MaxLength(20)]
        [StringLength(20)]
        [Required(AllowEmptyStrings = true)]
        public string TSICOD_4 { get; set; } // TSICOD_4 (length: 20)

        [MaxLength(15)]
        [StringLength(15)]
        [Required(AllowEmptyStrings = true)]
        public string BPCNUM_0 { get; set; } // BPCNUM_0 (length: 15)

        [MaxLength(30)]
        [StringLength(30)]
        [Required(AllowEmptyStrings = true)]
        public string MFGDES_0 { get; set; } // MFGDES_0 (length: 30)

        [Required]
        public DateTime STRDAT_0 { get; set; } // STRDAT_0

        [Required]
        public DateTime ENDDAT_0 { get; set; } // ENDDAT_0

        [MaxLength(3)]
        [StringLength(3)]
        [Required(AllowEmptyStrings = true)]
        public string UOM_0 { get; set; } // UOM_0 (length: 3)

        [MaxLength(3)]
        [StringLength(3)]
        [Required(AllowEmptyStrings = true)]
        public string STU_0 { get; set; } // STU_0 (length: 3)

        [Required]
        [Decimal(28, 13)]
        public decimal EXTQTY_0 { get; set; } // EXTQTY_0

        [Required]
        public short BOMALT_0 { get; set; } // BOMALT_0

        [MaxLength(100)]
        [StringLength(100)]
        [Required(AllowEmptyStrings = true)]
        public string ZITMDES_0 { get; set; } // ZITMDES_0 (length: 100)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string ZLEVEL_0 { get; set; } // ZLEVEL_0 (length: 10)

        [Required]
        [Decimal(28, 13)]
        public decimal ZESIMPORTA_0 { get; set; } // ZESIMPORTA_0

        [Required]
        [Decimal(28, 13)]
        public decimal ZESREALIZA_0 { get; set; } // ZESREALIZA_0

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string ZLEVELINT_0 { get; set; } // ZLEVELINT_0 (length: 10)

        [MaxLength(20)]
        [StringLength(20)]
        [Required(AllowEmptyStrings = true)]
        public string ZITMREFBPC_0 { get; set; } // ZITMREFBPC_0 (length: 20)

        [MaxLength(100)]
        [StringLength(100)]
        [Required(AllowEmptyStrings = true)]
        public string ZMANIFOF_0 { get; set; } // ZMANIFOF_0 (length: 100)

        [Required]
        public byte ZOFEMUSOES_0 { get; set; } // ZOFEMUSOES_0

        [Required]
        [Decimal(14, 3)]
        public decimal ZMARGEMPA_0 { get; set; } // ZMARGEMPA_0

        [Required]
        [Decimal(14, 3)]
        public decimal ZQTYETIQOK_0 { get; set; } // ZQTYETIQOK_0

        [Required]
        [Decimal(14, 3)]
        public decimal ZQTYETIQNOK_0 { get; set; } // ZQTYETIQNOK_0

        [MaxLength(20)]
        [StringLength(20)]
        [Required(AllowEmptyStrings = true)]
        public string ZOPERRPA_0 { get; set; } // ZOPERRPA_0 (length: 20)

        public MFGITM()
        {
            InitializePartial();
        }

        partial void InitializePartial();
    }
}
