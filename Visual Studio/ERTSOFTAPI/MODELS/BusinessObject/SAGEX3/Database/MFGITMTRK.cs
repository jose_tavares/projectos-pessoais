﻿using MODELS.Helpers;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MODELS.BusinessObject.SAGEX3.Database
{
    [Table("[MFGITMTRK]")]
    public class MFGITMTRK : DBRules
    {
        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string MFGTRKNUM_0 { get; set; }

        [MaxLength(20)]
        [StringLength(20)]
        [Required(AllowEmptyStrings = true)]
        public string ZESSTROWID_0 { get; set; }
    }
}
