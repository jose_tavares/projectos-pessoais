﻿using System;
using MODELS.Helpers;
using MODELS.Attributes;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MODELS.BusinessObject.SAGEX3.Database
{
    [Table("ERT.[FACILITY]")]
    public partial class FACILITY : DBRules
    {
        [Required]
        [Range(0, int.MaxValue)]
        [SqlDefaultValue(DefaultValue = "((1))")]
        public int UPDTICK_0 { get; set; } // UPDTICK_0

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string FCY_0 { get; set; } // FCY_0 (length: 5)

        [MaxLength(35)]
        [StringLength(35)]
        [Required(AllowEmptyStrings = true)]
        public string FCYNAM_0 { get; set; } // FCYNAM_0 (length: 35)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string FCYSHO_0 { get; set; } // FCYSHO_0 (length: 10)

        [MaxLength(3)]
        [StringLength(3)]
        [Required(AllowEmptyStrings = true)]
        public string CRY_0 { get; set; } // CRY_0 (length: 3)

        [MaxLength(20)]
        [StringLength(20)]
        [Required(AllowEmptyStrings = true)]
        public string CRN_0 { get; set; } // CRN_0 (length: 20)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string NAF_0 { get; set; } // NAF_0 (length: 10)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string BPTNUM_0 { get; set; } // BPTNUM_0 (length: 10)

        [Required]
        public byte MFGFLG_0 { get; set; } // MFGFLG_0

        [Required]
        public byte SALFLG_0 { get; set; } // SALFLG_0

        [Required]
        public byte PURFLG_0 { get; set; } // PURFLG_0

        [Required]
        public byte WRHFLG_0 { get; set; } // WRHFLG_0

        [Required]
        public byte FINFLG_0 { get; set; } // FINFLG_0

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string FINRSPFCY_0 { get; set; } // FINRSPFCY_0 (length: 5)

        [Required]
        public short DADFLG_0 { get; set; } // DADFLG_0

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string PAYBAN_0 { get; set; } // PAYBAN_0 (length: 5)

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string LEGCPY_0 { get; set; } // LEGCPY_0 (length: 5)

        [MaxLength(20)]
        [StringLength(20)]
        [Required(AllowEmptyStrings = true)]
        public string LEG_0 { get; set; } // LEG_0 (length: 20)

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string BPAADD_0 { get; set; } // BPAADD_0 (length: 5)

        [MaxLength(30)]
        [StringLength(30)]
        [Required(AllowEmptyStrings = true)]
        public string BIDNUM_0 { get; set; } // BIDNUM_0 (length: 30)

        [MaxLength(15)]
        [StringLength(15)]
        [Required(AllowEmptyStrings = true)]
        public string CNTNAM_0 { get; set; } // CNTNAM_0 (length: 15)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string DADFCY_0 { get; set; } // DADFCY_0 (length: 10)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string ACCCOD_0 { get; set; } // ACCCOD_0 (length: 10)

        [MaxLength(16)]
        [Required]
        public byte[] AUUID_0 { get; set; } // AUUID_0 (length: 16)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string GEOCOD_0 { get; set; } // GEOCOD_0 (length: 10)

        [MaxLength(35)]
        [StringLength(35)]
        [Required(AllowEmptyStrings = true)]
        public string EORINUM_0 { get; set; } // EORINUM_0 (length: 35)

        [Required]
        public byte ORICERFLG_0 { get; set; } // ORICERFLG_0

        [MaxLength(30)]
        [StringLength(30)]
        [Required(AllowEmptyStrings = true)]
        public string REXNUM_0 { get; set; } // REXNUM_0 (length: 30)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string DIE_0 { get; set; } // DIE_0 (length: 10)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string DIE_1 { get; set; } // DIE_1 (length: 10)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string DIE_2 { get; set; } // DIE_2 (length: 10)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string DIE_3 { get; set; } // DIE_3 (length: 10)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string DIE_4 { get; set; } // DIE_4 (length: 10)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string DIE_5 { get; set; } // DIE_5 (length: 10)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string DIE_6 { get; set; } // DIE_6 (length: 10)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string DIE_7 { get; set; } // DIE_7 (length: 10)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string DIE_8 { get; set; } // DIE_8 (length: 10)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string DIE_9 { get; set; } // DIE_9 (length: 10)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string DIE_10 { get; set; } // DIE_10 (length: 10)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string DIE_11 { get; set; } // DIE_11 (length: 10)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string DIE_12 { get; set; } // DIE_12 (length: 10)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string DIE_13 { get; set; } // DIE_13 (length: 10)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string DIE_14 { get; set; } // DIE_14 (length: 10)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string DIE_15 { get; set; } // DIE_15 (length: 10)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string DIE_16 { get; set; } // DIE_16 (length: 10)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string DIE_17 { get; set; } // DIE_17 (length: 10)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string DIE_18 { get; set; } // DIE_18 (length: 10)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string DIE_19 { get; set; } // DIE_19 (length: 10)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string INSCTYFLG_0 { get; set; } // INSCTYFLG_0 (length: 10)

        [MaxLength(15)]
        [StringLength(15)]
        [Required(AllowEmptyStrings = true)]
        public string CCE_0 { get; set; } // CCE_0 (length: 15)

        [MaxLength(15)]
        [StringLength(15)]
        [Required(AllowEmptyStrings = true)]
        public string CCE_1 { get; set; } // CCE_1 (length: 15)

        [MaxLength(15)]
        [StringLength(15)]
        [Required(AllowEmptyStrings = true)]
        public string CCE_2 { get; set; } // CCE_2 (length: 15)

        [MaxLength(15)]
        [StringLength(15)]
        [Required(AllowEmptyStrings = true)]
        public string CCE_3 { get; set; } // CCE_3 (length: 15)

        [MaxLength(15)]
        [StringLength(15)]
        [Required(AllowEmptyStrings = true)]
        public string CCE_4 { get; set; } // CCE_4 (length: 15)

        [MaxLength(15)]
        [StringLength(15)]
        [Required(AllowEmptyStrings = true)]
        public string CCE_5 { get; set; } // CCE_5 (length: 15)

        [MaxLength(15)]
        [StringLength(15)]
        [Required(AllowEmptyStrings = true)]
        public string CCE_6 { get; set; } // CCE_6 (length: 15)

        [MaxLength(15)]
        [StringLength(15)]
        [Required(AllowEmptyStrings = true)]
        public string CCE_7 { get; set; } // CCE_7 (length: 15)

        [MaxLength(15)]
        [StringLength(15)]
        [Required(AllowEmptyStrings = true)]
        public string CCE_8 { get; set; } // CCE_8 (length: 15)

        [MaxLength(15)]
        [StringLength(15)]
        [Required(AllowEmptyStrings = true)]
        public string CCE_9 { get; set; } // CCE_9 (length: 15)

        [MaxLength(15)]
        [StringLength(15)]
        [Required(AllowEmptyStrings = true)]
        public string CCE_10 { get; set; } // CCE_10 (length: 15)

        [MaxLength(15)]
        [StringLength(15)]
        [Required(AllowEmptyStrings = true)]
        public string CCE_11 { get; set; } // CCE_11 (length: 15)

        [MaxLength(15)]
        [StringLength(15)]
        [Required(AllowEmptyStrings = true)]
        public string CCE_12 { get; set; } // CCE_12 (length: 15)

        [MaxLength(15)]
        [StringLength(15)]
        [Required(AllowEmptyStrings = true)]
        public string CCE_13 { get; set; } // CCE_13 (length: 15)

        [MaxLength(15)]
        [StringLength(15)]
        [Required(AllowEmptyStrings = true)]
        public string CCE_14 { get; set; } // CCE_14 (length: 15)

        [MaxLength(15)]
        [StringLength(15)]
        [Required(AllowEmptyStrings = true)]
        public string CCE_15 { get; set; } // CCE_15 (length: 15)

        [MaxLength(15)]
        [StringLength(15)]
        [Required(AllowEmptyStrings = true)]
        public string CCE_16 { get; set; } // CCE_16 (length: 15)

        [MaxLength(15)]
        [StringLength(15)]
        [Required(AllowEmptyStrings = true)]
        public string CCE_17 { get; set; } // CCE_17 (length: 15)

        [MaxLength(15)]
        [StringLength(15)]
        [Required(AllowEmptyStrings = true)]
        public string CCE_18 { get; set; } // CCE_18 (length: 15)

        [MaxLength(15)]
        [StringLength(15)]
        [Required(AllowEmptyStrings = true)]
        public string CCE_19 { get; set; } // CCE_19 (length: 15)

        [Required]
        public byte UVYDAY_0 { get; set; } // UVYDAY_0

        [Required]
        public byte UVYDAY_1 { get; set; } // UVYDAY_1

        [Required]
        public byte UVYDAY_2 { get; set; } // UVYDAY_2

        [Required]
        public byte UVYDAY_3 { get; set; } // UVYDAY_3

        [Required]
        public byte UVYDAY_4 { get; set; } // UVYDAY_4

        [Required]
        public byte UVYDAY_5 { get; set; } // UVYDAY_5

        [Required]
        public byte UVYDAY_6 { get; set; } // UVYDAY_6

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string UVYCOD_0 { get; set; } // UVYCOD_0 (length: 5)

        [Required]
        public byte IVYFLG_0 { get; set; } // IVYFLG_0

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string IVYFCY_0 { get; set; } // IVYFCY_0 (length: 5)

        [Required]
        public byte WRHGES_0 { get; set; } // WRHGES_0

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string RCPWRH_0 { get; set; } // RCPWRH_0 (length: 5)

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string MFPWRH_0 { get; set; } // MFPWRH_0 (length: 5)

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string TRAWRH_0 { get; set; } // TRAWRH_0 (length: 5)

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string RTNWRH_0 { get; set; } // RTNWRH_0 (length: 5)

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string MFRWRH_0 { get; set; } // MFRWRH_0 (length: 5)

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string SHIWRH_0 { get; set; } // SHIWRH_0 (length: 5)

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string MFGWRH_0 { get; set; } // MFGWRH_0 (length: 5)

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string TRFWRH_0 { get; set; } // TRFWRH_0 (length: 5)

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string SCOWRH_0 { get; set; } // SCOWRH_0 (length: 5)

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string SCCWRH_0 { get; set; } // SCCWRH_0 (length: 5)

        [Required]
        public byte SPAOPEIGIC_0 { get; set; } // SPAOPEIGIC_0

        [MaxLength(6)]
        [StringLength(6)]
        [Required(AllowEmptyStrings = true)]
        public string STRHOU_0 { get; set; } // STRHOU_0 (length: 6)

        [MaxLength(6)]
        [StringLength(6)]
        [Required(AllowEmptyStrings = true)]
        public string ENDHOU_0 { get; set; } // ENDHOU_0 (length: 6)

        [Required]
        public short PAYFLG_0 { get; set; } // PAYFLG_0

        [Required]
        [Decimal(27, 13)]
        public decimal ZOBJSTOCK_0 { get; set; } // ZOBJSTOCK_0

        [Required]
        public short HRMDADFLG_0 { get; set; } // HRMDADFLG_0

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string HRMDADFCY_0 { get; set; } // HRMDADFCY_0 (length: 10)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string BPASGE_0 { get; set; } // BPASGE_0 (length: 10)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string BPADCL_0 { get; set; } // BPADCL_0 (length: 10)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string CNTDDS_0 { get; set; } // CNTDDS_0 (length: 10)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string CODCRA_0 { get; set; } // CODCRA_0 (length: 10)

        [Required]
        public short REGPRH_0 { get; set; } // REGPRH_0

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string PRF_0 { get; set; } // PRF_0 (length: 10)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string SRV_0 { get; set; } // SRV_0 (length: 10)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string CLLCVT_0 { get; set; } // CLLCVT_0 (length: 10)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string RSKWRK_0 { get; set; } // RSKWRK_0 (length: 10)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string HRMPAYBAN_0 { get; set; } // HRMPAYBAN_0 (length: 10)

        [Required]
        public short HRMTAXWAG_0 { get; set; } // HRMTAXWAG_0

        [Required]
        public short SECPRH_0 { get; set; } // SECPRH_0

        [Required]
        public short FLGAPP_0 { get; set; } // FLGAPP_0

        [Required]
        public short FLGFOR_0 { get; set; } // FLGFOR_0

        [Required]
        public short FLGPEC_0 { get; set; } // FLGPEC_0

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string CHEF_0 { get; set; } // CHEF_0 (length: 10)

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string ZGRPESTABWF_0 { get; set; } // ZGRPESTABWF_0 (length: 5)

        public FACILITY()
        {
            InitializePartial();
        }

        partial void InitializePartial();
    }
}
