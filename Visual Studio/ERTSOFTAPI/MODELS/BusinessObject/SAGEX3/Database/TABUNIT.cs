﻿using MODELS.Helpers;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MODELS.BusinessObject.SAGEX3.Database
{
    // TABUNIT
    [Table("[TABUNIT]")]
    public partial class TABUNIT : DBRules
    {
        [MaxLength(3)]
        [StringLength(3)]
        [Required(AllowEmptyStrings = true)]
        public string UOM_0 { get; set; } // UOM_0 (length: 3)

        [Required]
        public short UOMDEC_0 { get; set; } // UOMDEC_0

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string UOMSYM_0 { get; set; } // UOMSYM_0 (length: 5)

        [Required]
        public byte UOMTYP_0 { get; set; } // UOMTYP_0

        public TABUNIT()
        {
            InitializePartial();
        }

        partial void InitializePartial();
    }
}
