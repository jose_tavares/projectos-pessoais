﻿using System;
using MODELS.Helpers;
using MODELS.Attributes;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MODELS.BusinessObject.SAGEX3.Database
{
    // STOLOT
    [Table("[STOLOT]")]
    public partial class STOLOT : DBRules
    {
        [Required]
        [Range(0, int.MaxValue)]
        [SqlDefaultValue(DefaultValue = "((1))")]
        public int UPDTICK_0 { get; set; } // UPDTICK_0

        [MaxLength(20)]
        [StringLength(20)]
        [Required(AllowEmptyStrings = true)]
        public string ITMREF_0 { get; set; } // ITMREF_0 (length: 20)

        [MaxLength(15)]
        [StringLength(15)]
        [Required(AllowEmptyStrings = true)]
        public string LOT_0 { get; set; } // LOT_0 (length: 15)

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string SLO_0 { get; set; } // SLO_0 (length: 5)

        [Required]
        public byte VCRTYP_0 { get; set; } // VCRTYP_0

        [MaxLength(20)]
        [StringLength(20)]
        [Required(AllowEmptyStrings = true)]
        public string VCRNUM_0 { get; set; } // VCRNUM_0 (length: 20)

        [Required]
        [Range(0, int.MaxValue)]
        public int VCRLIN_0 { get; set; } // VCRLIN_0

        [MaxLength(15)]
        [StringLength(15)]
        [Required(AllowEmptyStrings = true)]
        public string BPSLOT_0 { get; set; } // BPSLOT_0 (length: 15)

        [MaxLength(20)]
        [StringLength(20)]
        [Required(AllowEmptyStrings = true)]
        public string USRFLD1_0 { get; set; } // USRFLD1_0 (length: 20)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string USRFLD2_0 { get; set; } // USRFLD2_0 (length: 10)

        [Required]
        [Decimal(11, 1)]
        public decimal USRFLD3_0 { get; set; } // USRFLD3_0

        [Required]
        public DateTime USRFLD4_0 { get; set; } // USRFLD4_0

        [Required]
        public DateTime LOTCREDAT_0 { get; set; } // LOTCREDAT_0

        [Required]
        public DateTime REFPER_0 { get; set; } // REFPER_0

        [Required]
        public short SHL_0 { get; set; } // SHL_0

        [Required]
        public DateTime SHLDAT_0 { get; set; } // SHLDAT_0

        [Required]
        [Decimal(14, 5)]
        public decimal POT_0 { get; set; } // POT_0

        [Required]
        [Decimal(14, 5)]
        public decimal ACT_0 { get; set; } // ACT_0

        [MaxLength(15)]
        [StringLength(15)]
        [Required(AllowEmptyStrings = true)]
        public string BPSNUM_0 { get; set; } // BPSNUM_0 (length: 15)

        [Required]
        public byte SHLUOM_0 { get; set; } // SHLUOM_0

        [Required]
        public byte SHLLTIUOM_0 { get; set; } // SHLLTIUOM_0

        [Required]
        public short SHLLTI_0 { get; set; } // SHLLTI_0

        [Required]
        public DateTime NEWLTIDAT_0 { get; set; } // NEWLTIDAT_0

        [Required]
        [Decimal(18, 7)]
        public decimal DLU_0 { get; set; } // DLU_0

        [Required]
        public DateTime DLUDAT_0 { get; set; } // DLUDAT_0

        [Required]
        public DateTime LTIDAT_0 { get; set; } // LTIDAT_0

        [Required]
        [Range(0, int.MaxValue)]
        public int EXPNUM_0 { get; set; } // EXPNUM_0

        [MaxLength(16)]
        [Required]
        public byte[] AUUID_0 { get; set; } // AUUID_0 (length: 16)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string ECCVALMAJ_0 { get; set; } // ECCVALMAJ_0 (length: 10)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string ECCVALMIN_0 { get; set; } // ECCVALMIN_0 (length: 10)

        [MaxLength(20)]
        [StringLength(20)]
        [Required(AllowEmptyStrings = true)]
        public string ZCRONOMOV_0 { get; set; } // ZCRONOMOV_0 (length: 20)

        public STOLOT()
        {
            InitializePartial();
        }

        partial void InitializePartial();
    }
}
