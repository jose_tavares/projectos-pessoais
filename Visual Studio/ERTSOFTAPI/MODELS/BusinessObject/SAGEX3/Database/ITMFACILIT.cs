﻿using System;
using MODELS.Helpers;
using MODELS.Attributes;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MODELS.BusinessObject.SAGEX3.Database
{
    // ITMFACILIT
    [Table("ERT.[ITMFACILIT]")]
    public partial class ITMFACILIT : DBRules
    {
        [Required]
        [Range(0, int.MaxValue)]
        [SqlDefaultValue(DefaultValue = "((1))")]
        public int UPDTICK_0 { get; set; } // UPDTICK_0

        [MaxLength(20)]
        [StringLength(20)]
        [Required(AllowEmptyStrings = true)]
        public string ITMREF_0 { get; set; } // ITMREF_0 (length: 20)

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string STOFCY_0 { get; set; } // STOFCY_0 (length: 5)

        [Required]
        public byte CUNFLG_0 { get; set; } // CUNFLG_0

        [MaxLength(20)]
        [StringLength(20)]
        [Required(AllowEmptyStrings = true)]
        public string CUNLISNUM_0 { get; set; } // CUNLISNUM_0 (length: 20)

        [Required]
        public byte ABCCLS_0 { get; set; } // ABCCLS_0

        [Required]
        public byte LOCMGTCOD_0 { get; set; } // LOCMGTCOD_0

        [Required]
        public byte STOCOD_0 { get; set; } // STOCOD_0

        [Required]
        public byte CUNCOD_0 { get; set; } // CUNCOD_0

        [MaxLength(3)]
        [StringLength(3)]
        [Required(AllowEmptyStrings = true)]
        public string SESCOD_0 { get; set; } // SESCOD_0 (length: 3)

        [Required]
        public short DAYCOV_0 { get; set; } // DAYCOV_0

        [Required]
        [Decimal(9, 4)]
        public decimal TOTLTI_0 { get; set; } // TOTLTI_0

        [Required]
        [Decimal(9, 4)]
        public decimal QUALTI_0 { get; set; } // QUALTI_0

        [Required]
        public short PLH_0 { get; set; } // PLH_0

        [Required]
        public byte PLHUOT_0 { get; set; } // PLHUOT_0

        [Required]
        public short FOH_0 { get; set; } // FOH_0

        [Required]
        public byte FOHUOT_0 { get; set; } // FOHUOT_0

        [Required]
        [Decimal(9, 4)]
        public decimal OFS_0 { get; set; } // OFS_0

        [Required]
        [Decimal(9, 4)]
        public decimal MFGLTI_0 { get; set; } // MFGLTI_0

        [Required]
        [Decimal(9, 4)]
        public decimal PRPLTI_0 { get; set; } // PRPLTI_0

        [Required]
        public short MIC_0 { get; set; } // MIC_0

        [Required]
        public byte REOMGTCOD_0 { get; set; } // REOMGTCOD_0

        [Required]
        public short REOPER_0 { get; set; } // REOPER_0

        [Required]
        public byte REOCOD_0 { get; set; } // REOCOD_0

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string REOFCY_0 { get; set; } // REOFCY_0 (length: 5)

        [MaxLength(3)]
        [StringLength(3)]
        [Required(AllowEmptyStrings = true)]
        public string REOPOL_0 { get; set; } // REOPOL_0 (length: 3)

        [Required]
        [Decimal(28, 13)]
        public decimal SAFSTO_0 { get; set; } // SAFSTO_0

        [Required]
        [Decimal(28, 13)]
        public decimal SAFSTOCLC_0 { get; set; } // SAFSTOCLC_0

        [Required]
        [Decimal(28, 13)]
        public decimal REOTSD_0 { get; set; } // REOTSD_0

        [Required]
        [Decimal(28, 13)]
        public decimal REOTSDCLC_0 { get; set; } // REOTSDCLC_0

        [Required]
        [Decimal(28, 13)]
        public decimal MAXSTO_0 { get; set; } // MAXSTO_0

        [Required]
        [Decimal(28, 13)]
        public decimal MAXSTOCLC_0 { get; set; } // MAXSTOCLC_0

        [Required]
        [Decimal(28, 13)]
        public decimal REOMINQTY_0 { get; set; } // REOMINQTY_0

        [Required]
        [Decimal(28, 13)]
        public decimal REOMINCLC_0 { get; set; } // REOMINCLC_0

        [Required]
        [Decimal(28, 13)]
        public decimal MFGLOTQTY_0 { get; set; } // MFGLOTQTY_0

        [Required]
        [Decimal(10, 4)]
        public decimal SHR_0 { get; set; } // SHR_0

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string PLANNER_0 { get; set; } // PLANNER_0 (length: 5)

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string BUY_0 { get; set; } // BUY_0 (length: 5)

        [Required]
        public byte OTRSTYP_0 { get; set; } // OTRSTYP_0

        [Required]
        public byte OTRSTYP_1 { get; set; } // OTRSTYP_1

        [Required]
        public byte OTRSTYP_2 { get; set; } // OTRSTYP_2

        [Required]
        public byte OTRSTYP_3 { get; set; } // OTRSTYP_3

        [Required]
        public byte OTRSTYP_4 { get; set; } // OTRSTYP_4

        [MaxLength(3)]
        [StringLength(3)]
        [Required(AllowEmptyStrings = true)]
        public string OVECOD_0 { get; set; } // OVECOD_0 (length: 3)

        [MaxLength(3)]
        [StringLength(3)]
        [Required(AllowEmptyStrings = true)]
        public string OVECOD_1 { get; set; } // OVECOD_1 (length: 3)

        [MaxLength(3)]
        [StringLength(3)]
        [Required(AllowEmptyStrings = true)]
        public string OVECOD_2 { get; set; } // OVECOD_2 (length: 3)

        [MaxLength(3)]
        [StringLength(3)]
        [Required(AllowEmptyStrings = true)]
        public string OVECOD_3 { get; set; } // OVECOD_3 (length: 3)

        [MaxLength(3)]
        [StringLength(3)]
        [Required(AllowEmptyStrings = true)]
        public string OVECOD_4 { get; set; } // OVECOD_4 (length: 3)

        [Required]
        public byte OVECPNFLG_0 { get; set; } // OVECPNFLG_0

        [Required]
        public byte OVECPNFLG_1 { get; set; } // OVECPNFLG_1

        [Required]
        public byte OVECPNFLG_2 { get; set; } // OVECPNFLG_2

        [Required]
        public byte OVECPNFLG_3 { get; set; } // OVECPNFLG_3

        [Required]
        public byte OVECPNFLG_4 { get; set; } // OVECPNFLG_4

        [Required]
        public byte STDCSTUPD_0 { get; set; } // STDCSTUPD_0

        [Required]
        public byte CUTCSTUPD_0 { get; set; } // CUTCSTUPD_0

        [Required]
        public byte BUDCSTUPD_0 { get; set; } // BUDCSTUPD_0

        [Required]
        public byte SIMCSTUPD_0 { get; set; } // SIMCSTUPD_0

        [MaxLength(20)]
        [StringLength(20)]
        [Required(AllowEmptyStrings = true)]
        public string MFGROU_0 { get; set; } // MFGROU_0 (length: 20)

        [MaxLength(20)]
        [StringLength(20)]
        [Required(AllowEmptyStrings = true)]
        public string CSTROU_0 { get; set; } // CSTROU_0 (length: 20)

        [MaxLength(20)]
        [StringLength(20)]
        [Required(AllowEmptyStrings = true)]
        public string RCCROU_0 { get; set; } // RCCROU_0 (length: 20)

        [Required]
        public short MFGROUALT_0 { get; set; } // MFGROUALT_0

        [Required]
        public short CSTROUALT_0 { get; set; } // CSTROUALT_0

        [Required]
        public short RCCROUALT_0 { get; set; } // RCCROUALT_0

        [Required]
        public byte MFGSHTCOD_0 { get; set; } // MFGSHTCOD_0

        [Required]
        public byte RELSCATIA_0 { get; set; } // RELSCATIA_0

        [Required]
        public short MONPROYEA_0 { get; set; } // MONPROYEA_0

        [Required]
        public short MONPROMON_0 { get; set; } // MONPROMON_0

        [Required]
        public short YEAPROYEA_0 { get; set; } // YEAPROYEA_0

        [Required]
        [Decimal(10, 4)]
        public decimal CLEPCTAUT_0 { get; set; } // CLEPCTAUT_0

        [MaxLength(20)]
        [StringLength(20)]
        [Required(AllowEmptyStrings = true)]
        public string CFGVCRNUM_0 { get; set; } // CFGVCRNUM_0 (length: 20)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string ISM_0 { get; set; } // ISM_0 (length: 10)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string WGRACS_0 { get; set; } // WGRACS_0 (length: 10)

        [Required]
        public short REDMODFLG_0 { get; set; } // REDMODFLG_0

        [Required]
        [Decimal(28, 8)]
        public decimal ITMTOLPOS_0 { get; set; } // ITMTOLPOS_0

        [Required]
        [Decimal(28, 8)]
        public decimal ITMTOLNEG_0 { get; set; } // ITMTOLNEG_0

        [MaxLength(3)]
        [StringLength(3)]
        [Required(AllowEmptyStrings = true)]
        public string VLTCOD_0 { get; set; } // VLTCOD_0 (length: 3)

        [Required]
        [Decimal(8, 3)]
        public decimal PROPER_0 { get; set; } // PROPER_0

        [Required]
        public byte LOCNUM_0 { get; set; } // LOCNUM_0

        [Required]
        public byte LOCNUM_1 { get; set; } // LOCNUM_1

        [Required]
        public byte LOCNUM_2 { get; set; } // LOCNUM_2

        [Required]
        public byte LOCNUM_3 { get; set; } // LOCNUM_3

        [Required]
        public byte LOCNUM_4 { get; set; } // LOCNUM_4

        [Required]
        public byte LOCNUM_5 { get; set; } // LOCNUM_5

        [Required]
        public byte LOCNUM_6 { get; set; } // LOCNUM_6

        [Required]
        public byte LOCNUM_7 { get; set; } // LOCNUM_7

        [Required]
        public byte LOCNUM_8 { get; set; } // LOCNUM_8

        [Required]
        public byte LOCNUM_9 { get; set; } // LOCNUM_9

        [Required]
        public byte LOCNUM_10 { get; set; } // LOCNUM_10

        [Required]
        public byte LOCNUM_11 { get; set; } // LOCNUM_11

        [Required]
        public byte LOCNUM_12 { get; set; } // LOCNUM_12

        [Required]
        public byte LOCNUM_13 { get; set; } // LOCNUM_13

        [Required]
        public byte LOCNUM_14 { get; set; } // LOCNUM_14

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string DEFLOCTYP_0 { get; set; } // DEFLOCTYP_0 (length: 5)

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string DEFLOCTYP_1 { get; set; } // DEFLOCTYP_1 (length: 5)

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string DEFLOCTYP_2 { get; set; } // DEFLOCTYP_2 (length: 5)

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string DEFLOCTYP_3 { get; set; } // DEFLOCTYP_3 (length: 5)

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string DEFLOCTYP_4 { get; set; } // DEFLOCTYP_4 (length: 5)

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string DEFLOCTYP_5 { get; set; } // DEFLOCTYP_5 (length: 5)

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string DEFLOCTYP_6 { get; set; } // DEFLOCTYP_6 (length: 5)

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string DEFLOCTYP_7 { get; set; } // DEFLOCTYP_7 (length: 5)

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string DEFLOCTYP_8 { get; set; } // DEFLOCTYP_8 (length: 5)

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string DEFLOCTYP_9 { get; set; } // DEFLOCTYP_9 (length: 5)

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string DEFLOCTYP_10 { get; set; } // DEFLOCTYP_10 (length: 5)

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string DEFLOCTYP_11 { get; set; } // DEFLOCTYP_11 (length: 5)

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string DEFLOCTYP_12 { get; set; } // DEFLOCTYP_12 (length: 5)

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string DEFLOCTYP_13 { get; set; } // DEFLOCTYP_13 (length: 5)

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string DEFLOCTYP_14 { get; set; } // DEFLOCTYP_14 (length: 5)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string DEFLOC_0 { get; set; } // DEFLOC_0 (length: 10)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string DEFLOC_1 { get; set; } // DEFLOC_1 (length: 10)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string DEFLOC_2 { get; set; } // DEFLOC_2 (length: 10)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string DEFLOC_3 { get; set; } // DEFLOC_3 (length: 10)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string DEFLOC_4 { get; set; } // DEFLOC_4 (length: 10)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string DEFLOC_5 { get; set; } // DEFLOC_5 (length: 10)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string DEFLOC_6 { get; set; } // DEFLOC_6 (length: 10)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string DEFLOC_7 { get; set; } // DEFLOC_7 (length: 10)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string DEFLOC_8 { get; set; } // DEFLOC_8 (length: 10)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string DEFLOC_9 { get; set; } // DEFLOC_9 (length: 10)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string DEFLOC_10 { get; set; } // DEFLOC_10 (length: 10)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string DEFLOC_11 { get; set; } // DEFLOC_11 (length: 10)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string DEFLOC_12 { get; set; } // DEFLOC_12 (length: 10)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string DEFLOC_13 { get; set; } // DEFLOC_13 (length: 10)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string DEFLOC_14 { get; set; } // DEFLOC_14 (length: 10)

        [Required]
        public byte WIPPRO_0 { get; set; } // WIPPRO_0

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string PCK_0 { get; set; } // PCK_0 (length: 5)

        [Required]
        [Decimal(18, 7)]
        public decimal PCKCAP_0 { get; set; } // PCKCAP_0

        [Required]
        public byte STOMGTCOD_0 { get; set; } // STOMGTCOD_0

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string PTOCOD_0 { get; set; } // PTOCOD_0 (length: 5)

        [Required]
        public byte PCKSTKFLG_0 { get; set; } // PCKSTKFLG_0

        [Required]
        public byte QUAFLG_0 { get; set; } // QUAFLG_0

        [MaxLength(8)]
        [StringLength(8)]
        [Required(AllowEmptyStrings = true)]
        public string QLYCRD_0 { get; set; } // QLYCRD_0 (length: 8)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string QUAACS_0 { get; set; } // QUAACS_0 (length: 10)

        [Required]
        [Range(0, int.MaxValue)]
        public int QUAFRY_0 { get; set; } // QUAFRY_0

        [Required]
        [Range(0, int.MaxValue)]
        public int QUANUM_0 { get; set; } // QUANUM_0

        [Required]
        public byte SMPTYP_0 { get; set; } // SMPTYP_0

        [Required]
        public byte SMPMOD_0 { get; set; } // SMPMOD_0

        [Required]
        public byte GENLEVINS_0 { get; set; } // GENLEVINS_0

        [Required]
        public byte NQA_0 { get; set; } // NQA_0

        [Required]
        [Range(0, int.MaxValue)]
        public int QUAADXUID_0 { get; set; } // QUAADXUID_0

        [Required]
        [Range(0, int.MaxValue)]
        public int QUANUMUID_0 { get; set; } // QUANUMUID_0

        [Required]
        public short SHLLTI_0 { get; set; } // SHLLTI_0

        [Required]
        public byte SHLLTIUOM_0 { get; set; } // SHLLTIUOM_0

        [MaxLength(1)]
        [StringLength(1)]
        [Required(AllowEmptyStrings = true)]
        public string NEWLTISTA_0 { get; set; } // NEWLTISTA_0 (length: 1)

        [Required]
        [Decimal(18, 7)]
        public decimal DLU_0 { get; set; } // DLU_0

        [MaxLength(8)]
        [StringLength(8)]
        [Required(AllowEmptyStrings = true)]
        public string LTIQLYCRD_0 { get; set; } // LTIQLYCRD_0 (length: 8)

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string ORDWRH_0 { get; set; } // ORDWRH_0 (length: 5)

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string MATWRH_0 { get; set; } // MATWRH_0 (length: 5)

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string SHIWRH_0 { get; set; } // SHIWRH_0 (length: 5)

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string MFGWRH_0 { get; set; } // MFGWRH_0 (length: 5)

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string TRFWRH_0 { get; set; } // TRFWRH_0 (length: 5)

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string SCOWRH_0 { get; set; } // SCOWRH_0 (length: 5)

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string SCCWRH_0 { get; set; } // SCCWRH_0 (length: 5)

        [Required]
        public byte PCKFLG_0 { get; set; } // PCKFLG_0

        [Required]
        [Range(0, int.MaxValue)]
        public int EXPNUM_0 { get; set; } // EXPNUM_0

        [MaxLength(20)]
        [StringLength(20)]
        [Required(AllowEmptyStrings = true)]
        public string STAFED_0 { get; set; } // STAFED_0 (length: 20)

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string FRTCLS_0 { get; set; } // FRTCLS_0 (length: 5)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string NMFC_0 { get; set; } // NMFC_0 (length: 10)

        [MaxLength(3)]
        [StringLength(3)]
        [Required(AllowEmptyStrings = true)]
        public string VLTCODHIS_0 { get; set; } // VLTCODHIS_0 (length: 3)

        [MaxLength(3)]
        [StringLength(3)]
        [Required(AllowEmptyStrings = true)]
        public string VLTCODHIS_1 { get; set; } // VLTCODHIS_1 (length: 3)

        [MaxLength(3)]
        [StringLength(3)]
        [Required(AllowEmptyStrings = true)]
        public string VLTCODHIS_2 { get; set; } // VLTCODHIS_2 (length: 3)

        [Required]
        public DateTime VLTCODDAT_0 { get; set; } // VLTCODDAT_0

        [Required]
        public DateTime VLTCODDAT_1 { get; set; } // VLTCODDAT_1

        [Required]
        public DateTime VLTCODDAT_2 { get; set; } // VLTCODDAT_2

        [MaxLength(16)]
        [Required]
        public byte[] AUUID_0 { get; set; } // AUUID_0 (length: 16)

        [Required]
        [Decimal(21, 6)]
        public decimal ZSTDCST_0 { get; set; } // ZSTDCST_0

        [Required]
        public byte PCKSERFLG_0 { get; set; } // PCKSERFLG_0

        [Required]
        public short COMSEQCON_0 { get; set; } // COMSEQCON_0

        [Required]
        [Decimal(21, 6)]
        public decimal ZOVERHEADS_0 { get; set; } // ZOVERHEADS_0

        [Required]
        public byte LPNMGTCOD_0 { get; set; } // LPNMGTCOD_0

        [Required]
        public short PJMSTRSTK_0 { get; set; } // PJMSTRSTK_0

        [Required]
        [Decimal(21, 6)]
        public decimal ZTRANSPIN_0 { get; set; } // ZTRANSPIN_0

        [MaxLength(30)]
        [StringLength(30)]
        [Required(AllowEmptyStrings = true)]
        public string TCTRNUM_0 { get; set; } // TCTRNUM_0 (length: 30)

        [MaxLength(30)]
        [StringLength(30)]
        [Required(AllowEmptyStrings = true)]
        public string TCTRNUM_1 { get; set; } // TCTRNUM_1 (length: 30)

        [MaxLength(30)]
        [StringLength(30)]
        [Required(AllowEmptyStrings = true)]
        public string TCTRNUM_2 { get; set; } // TCTRNUM_2 (length: 30)

        [MaxLength(30)]
        [StringLength(30)]
        [Required(AllowEmptyStrings = true)]
        public string TCTRNUM_3 { get; set; } // TCTRNUM_3 (length: 30)

        [MaxLength(3)]
        [StringLength(3)]
        [Required(AllowEmptyStrings = true)]
        public string PCU_0 { get; set; } // PCU_0 (length: 3)

        [MaxLength(3)]
        [StringLength(3)]
        [Required(AllowEmptyStrings = true)]
        public string PCU_1 { get; set; } // PCU_1 (length: 3)

        [MaxLength(3)]
        [StringLength(3)]
        [Required(AllowEmptyStrings = true)]
        public string PCU_2 { get; set; } // PCU_2 (length: 3)

        [MaxLength(3)]
        [StringLength(3)]
        [Required(AllowEmptyStrings = true)]
        public string PCU_3 { get; set; } // PCU_3 (length: 3)

        [Required]
        [Decimal(21, 6)]
        public decimal ZGEA_0 { get; set; } // ZGEA_0

        [Required]
        [Decimal(18, 7)]
        public decimal TCTRPCUCOE_0 { get; set; } // TCTRPCUCOE_0

        [Required]
        [Decimal(18, 7)]
        public decimal TCTRPCUCOE_1 { get; set; } // TCTRPCUCOE_1

        [Required]
        [Decimal(18, 7)]
        public decimal TCTRPCUCOE_2 { get; set; } // TCTRPCUCOE_2

        [Required]
        [Decimal(18, 7)]
        public decimal TCTRPCUCOE_3 { get; set; } // TCTRPCUCOE_3

        [MaxLength(30)]
        [StringLength(30)]
        [Required(AllowEmptyStrings = true)]
        public string TCTRDEF_0 { get; set; } // TCTRDEF_0 (length: 30)

        [Required]
        [Decimal(21, 6)]
        public decimal ZTRANSPOU_0 { get; set; } // ZTRANSPOU_0

        [Required]
        public byte EXCFDMA_0 { get; set; } // EXCFDMA_0

        [Required]
        [Decimal(21, 6)]
        public decimal ZMATLEV0_0 { get; set; } // ZMATLEV0_0

        [Required]
        [Decimal(21, 6)]
        public decimal ZCRAW_0 { get; set; } // ZCRAW_0

        [Required]
        [Decimal(14, 3)]
        public decimal ZCOH_0 { get; set; } // ZCOH_0

        [Required]
        [Decimal(21, 6)]
        public decimal ZCCTI_0 { get; set; } // ZCCTI_0

        [Required]
        [Decimal(14, 3)]
        public decimal ZCGEA_0 { get; set; } // ZCGEA_0

        [Required]
        [Decimal(21, 6)]
        public decimal ZCCTO_0 { get; set; } // ZCCTO_0

        [Required]
        [Range(0, int.MaxValue)]
        public int ZUID_0 { get; set; } // ZUID_0

        [Required]
        public byte ZCSTTYP_0 { get; set; } // ZCSTTYP_0

        [Required]
        [Range(0, int.MaxValue)]
        public int ZITCSEQ_0 { get; set; } // ZITCSEQ_0

        [Required]
        public byte ZCONTROL_0 { get; set; } // ZCONTROL_0

        [Required]
        public short ZCONTROLA_0 { get; set; } // ZCONTROLA_0

        [Required]
        public short ZCONTROLQ_0 { get; set; } // ZCONTROLQ_0

        [Required]
        public short ZMULTIPLO_0 { get; set; } // ZMULTIPLO_0

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string ZSTOFCYREC_0 { get; set; } // ZSTOFCYREC_0 (length: 5)

        [MaxLength(20)]
        [StringLength(20)]
        [Required(AllowEmptyStrings = true)]
        public string ZMFGTYP_0 { get; set; } // ZMFGTYP_0 (length: 20)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string ZPASTAOK_0 { get; set; } // ZPASTAOK_0 (length: 10)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string ZPASACQ_0 { get; set; } // ZPASACQ_0 (length: 10)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string ZPASTANOK_0 { get; set; } // ZPASTANOK_0 (length: 10)

        public ITMFACILIT()
        {
            InitializePartial();
        }

        partial void InitializePartial();
    }
}
