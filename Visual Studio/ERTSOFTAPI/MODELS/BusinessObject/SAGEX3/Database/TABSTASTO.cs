﻿using System;
using MODELS.Helpers;
using MODELS.Attributes;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MODELS.BusinessObject.SAGEX3.Database
{
    // TABSTASTO
    [Table("[TABSTASTO]")]
    public partial class TABSTASTO : DBRules
    {
        [MaxLength(3)]
        [StringLength(3)]
        [Required(AllowEmptyStrings = true)]
        public string STASTO_0 { get; set; } // STASTO_0 (length: 3)

        [MaxLength(30)]
        [StringLength(30)]
        [Required(AllowEmptyStrings = true)]
        public string STADES_0 { get; set; } // STADES_0 (length: 30)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string STASHO_0 { get; set; } // STASHO_0 (length: 10)

        [MaxLength(3)]
        [StringLength(3)]
        [Required(AllowEmptyStrings = true)]
        public string EXYQQQSTA_0 { get; set; } // EXYQQQSTA_0 (length: 3)

        [MaxLength(3)]
        [StringLength(3)]
        [Required(AllowEmptyStrings = true)]
        public string EXYRRRSTA_0 { get; set; } // EXYRRRSTA_0 (length: 3)

        [Required]
        [Decimal(28, 13)]
        public decimal ZCOEFVAL_0 { get; set; } // ZCOEFVAL_0

        public TABSTASTO()
        {
            InitializePartial();
        }

        partial void InitializePartial();
    }
}
