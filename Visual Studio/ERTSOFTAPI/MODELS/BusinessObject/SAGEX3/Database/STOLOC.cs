﻿using System;
using MODELS.Helpers;
using MODELS.Attributes;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MODELS.BusinessObject.SAGEX3.Database
{
    // STOLOC
    [Table("[STOLOC]")]
    public partial class STOLOC : DBRules
    {
        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string STOFCY_0 { get; set; } // STOFCY_0 (length: 5)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string LOC_0 { get; set; } // LOC_0 (length: 10)

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string WRH_0 { get; set; } // WRH_0 (length: 5)

        [Required]
        public byte OCPCOD_0 { get; set; } // OCPCOD_0

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string LOCTYP_0 { get; set; } // LOCTYP_0 (length: 5)

        [Required]
        public byte LOCCAT_0 { get; set; } // LOCCAT_0

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string LOCNUMFMT_0 { get; set; } // LOCNUMFMT_0 (length: 10)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string PPSSEQ_0 { get; set; } // PPSSEQ_0 (length: 10)

        [Required]
        public byte MONITMFLG_0 { get; set; } // MONITMFLG_0

        [Required]
        public byte DEDFLG_0 { get; set; } // DEDFLG_0

        [Required]
        public byte REAFLG_0 { get; set; } // REAFLG_0

        [Required]
        public byte FRGMGTMOD_0 { get; set; } // FRGMGTMOD_0

        [Required]
        public short TEMLTI_0 { get; set; } // TEMLTI_0

        [Required]
        public byte FILMGTFLG_0 { get; set; } // FILMGTFLG_0

        [MaxLength(20)]
        [StringLength(20)]
        [Required(AllowEmptyStrings = true)]
        public string AUZSST_0 { get; set; } // AUZSST_0 (length: 20)

        [Required]
        public DateTime AVADAT_0 { get; set; } // AVADAT_0

        [MaxLength(6)]
        [StringLength(6)]
        [Required(AllowEmptyStrings = true)]
        public string AVAHOU_0 { get; set; } // AVAHOU_0 (length: 6)

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string ALLUSR_0 { get; set; } // ALLUSR_0 (length: 5)

        [Required]
        public DateTime ALLDAT_0 { get; set; } // ALLDAT_0

        [MaxLength(6)]
        [StringLength(6)]
        [Required(AllowEmptyStrings = true)]
        public string ALLHOU_0 { get; set; } // ALLHOU_0 (length: 6)

        [Required]
        [Decimal(13, 4)]
        public decimal MAXAUZWEI_0 { get; set; } // MAXAUZWEI_0

        [Required]
        public short WID_0 { get; set; } // WID_0

        [Required]
        public short HEI_0 { get; set; } // HEI_0

        [Required]
        public short DTH_0 { get; set; } // DTH_0

        [Required]
        public byte LOKSTA_0 { get; set; } // LOKSTA_0

        [Required]
        public byte CUNLOKFLG_0 { get; set; } // CUNLOKFLG_0

        [MaxLength(3)]
        [StringLength(3)]
        [Required(AllowEmptyStrings = true)]
        public string PCU_0 { get; set; } // PCU_0 (length: 3)

        [Required]
        [Decimal(18, 7)]
        public decimal PCUSTUCOE_0 { get; set; } // PCUSTUCOE_0

        [Required]
        [Decimal(28, 13)]
        public decimal QTYPCU_0 { get; set; } // QTYPCU_0

        [Required]
        [Decimal(28, 13)]
        public decimal MAXQTYPCU_0 { get; set; } // MAXQTYPCU_0

        [Required]
        [Range(0, int.MaxValue)]
        public int EXPNUM_0 { get; set; } // EXPNUM_0

        [MaxLength(3)]
        [StringLength(3)]
        [Required(AllowEmptyStrings = true)]
        public string ZUNIDADE_0 { get; set; } // ZUNIDADE_0 (length: 3)

        public STOLOC()
        {
            InitializePartial();
        }

        partial void InitializePartial();
    }
}
