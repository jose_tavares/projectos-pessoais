﻿using System;
using MODELS.Helpers;
using MODELS.Attributes;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MODELS.BusinessObject.SAGEX3.Database
{
    // MFGMAT
    [Table("[MFGMAT]")]
    public partial class MFGMAT : DBRules
    {
        [MaxLength(20)]
        [StringLength(20)]
        [Required(AllowEmptyStrings = true)]
        public string MFGNUM_0 { get; set; } // MFGNUM_0 (length: 20)

        [Required]
        [Range(0, int.MaxValue)]
        public int MFGLIN_0 { get; set; } // MFGLIN_0

        [MaxLength(20)]
        [StringLength(20)]
        [Required(AllowEmptyStrings = true)]
        public string ITMREF_0 { get; set; } // ITMREF_0 (length: 20)

        [Required]
        public byte MFGSTA_0 { get; set; } // MFGSTA_0

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string PLNFCY_0 { get; set; } // PLNFCY_0 (length: 5)

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string MFGFCY_0 { get; set; } // MFGFCY_0 (length: 5)

        [Required]
        public short BOMSEQ_0 { get; set; } // BOMSEQ_0

        [Required]
        [Decimal(10, 4)]
        public decimal SCA_0 { get; set; } // SCA_0

        [MaxLength(3)]
        [StringLength(3)]
        [Required(AllowEmptyStrings = true)]
        public string STU_0 { get; set; } // STU_0 (length: 3)

        [Required]
        [Decimal(18, 7)]
        public decimal BOMSTUCOE_0 { get; set; } // BOMSTUCOE_0

        [Required]
        [Decimal(28, 13)]
        public decimal BOMQTY_0 { get; set; } // BOMQTY_0

        [Required]
        [Decimal(28, 13)]
        public decimal RETQTY_0 { get; set; } // RETQTY_0

        [Required]
        [Decimal(28, 13)]
        public decimal USEQTY_0 { get; set; } // USEQTY_0

        [Required]
        public byte MATSTA_0 { get; set; } // MATSTA_0

        [MaxLength(15)]
        [StringLength(15)]
        [Required(AllowEmptyStrings = true)]
        public string ZLOTFORN_0 { get; set; } // ZLOTFORN_0 (length: 15)

        [Required]
        public byte ZFACEPR_0 { get; set; } // ZFACEPR_0

        [Required]
        public byte ZFACESEC_0 { get; set; } // ZFACESEC_0

        [MaxLength(20)]
        [StringLength(20)]
        [Required(AllowEmptyStrings = true)]
        public string ZOPERCON_0 { get; set; } // ZOPERCON_0 (length: 20)

        [Required]
        public byte ZCONSPRDOK_0 { get; set; } // ZCONSPRDOK_0

        [Required]
        public byte ZCONSPRDNOK_0 { get; set; } // ZCONSPRDNOK_0

        [MaxLength(20)]
        [StringLength(20)]
        [Required(AllowEmptyStrings = true)]
        public string ZMODOCONS_0 { get; set; } // ZMODOCONS_0 (length: 20)

        [Required]
        [Decimal(14, 3)]
        public decimal ZQTYMC_0 { get; set; } // ZQTYMC_0

        public MFGMAT()
        {
            InitializePartial();
        }

        partial void InitializePartial();
    }
}
