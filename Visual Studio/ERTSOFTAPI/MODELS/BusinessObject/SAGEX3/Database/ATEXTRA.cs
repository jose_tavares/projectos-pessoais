﻿using MODELS.Helpers;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MODELS.BusinessObject.SAGEX3.Database
{
    // ATEXTRA
    [Table("[ATEXTRA]")]
    public partial class ATEXTRA : DBRules
    {
        [MaxLength(12)]
        [StringLength(12)]
        [Required(AllowEmptyStrings = true)]
        public string CODFIC_0 { get; set; } // CODFIC_0 (length: 12)

        [MaxLength(12)]
        [StringLength(12)]
        [Required(AllowEmptyStrings = true)]
        public string ZONE_0 { get; set; } // ZONE_0 (length: 12)

        [MaxLength(3)]
        [StringLength(3)]
        [Required(AllowEmptyStrings = true)]
        public string LANGUE_0 { get; set; } // LANGUE_0 (length: 3)

        [MaxLength(3)]
        [StringLength(3)]
        [Required(AllowEmptyStrings = true)]
        public string LANORI_0 { get; set; } // LANORI_0 (length: 3)

        [MaxLength(80)]
        [StringLength(80)]
        [Required(AllowEmptyStrings = true)]
        public string IDENT1_0 { get; set; } // IDENT1_0 (length: 80)

        [MaxLength(80)]
        [StringLength(80)]
        [Required(AllowEmptyStrings = true)]
        public string IDENT2_0 { get; set; } // IDENT2_0 (length: 80)

        [MaxLength(80)]
        [StringLength(80)]
        [Required(AllowEmptyStrings = true)]
        public string TEXTE_0 { get; set; } // TEXTE_0 (length: 80)

        public ATEXTRA()
        {
            InitializePartial();
        }

        partial void InitializePartial();
    }
}
