﻿using System;
using MODELS.Helpers;
using MODELS.Attributes;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MODELS.BusinessObject.SAGEX3.Database
{
    // STOCK
    [Table("[STOCK]")]
    public partial class STOCK : DBRules
    {
        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string STOFCY_0 { get; set; } // STOFCY_0 (length: 5)

        [MaxLength(20)]
        [StringLength(20)]
        [Required(AllowEmptyStrings = true)]
        public string ITMREF_0 { get; set; } // ITMREF_0 (length: 20)

        [MaxLength(15)]
        [StringLength(15)]
        [Required(AllowEmptyStrings = true)]
        public string LOT_0 { get; set; } // LOT_0 (length: 15)

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string SLO_0 { get; set; } // SLO_0 (length: 5)

        [MaxLength(15)]
        [StringLength(15)]
        [Required(AllowEmptyStrings = true)]
        public string BPSLOT_0 { get; set; } // BPSLOT_0 (length: 15)

        [MaxLength(3)]
        [StringLength(3)]
        [Required(AllowEmptyStrings = true)]
        public string STA_0 { get; set; } // STA_0 (length: 3)

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string LOC_0 { get; set; } // LOC_0 (length: 10)

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string LOCTYP_0 { get; set; } // LOCTYP_0 (length: 5)

        [Required]
        public byte LOCCAT_0 { get; set; } // LOCCAT_0

        [MaxLength(3)]
        [StringLength(3)]
        [Required(AllowEmptyStrings = true)]
        public string PCU_0 { get; set; } // PCU_0 (length: 3)

        [Required]
        [Decimal(18, 7)]
        public decimal PCUSTUCOE_0 { get; set; } // PCUSTUCOE_0

        [Required]
        [Decimal(28, 13)]
        public decimal QTYPCU_0 { get; set; } // QTYPCU_0

        [Required]
        [Decimal(28, 13)]
        public decimal QTYPCUORI_0 { get; set; } // QTYPCUORI_0

        [Required]
        [Decimal(28, 13)]
        public decimal CUMALLQTY_0 { get; set; } // CUMALLQTY_0

        [Required]
        [Decimal(28, 13)]
        public decimal CUMALLQTA_0 { get; set; } // CUMALLQTA_0

        [Required]
        [Decimal(28, 13)]
        public decimal CUMWIPQTY_0 { get; set; } // CUMWIPQTY_0

        [Required]
        [Decimal(28, 13)]
        public decimal CUMWIPQTA_0 { get; set; } // CUMWIPQTA_0

        [Required]
        public byte CUNLOKFLG_0 { get; set; } // CUNLOKFLG_0

        [Required]
        public byte ZPDAPICADO_0 { get; set; } // ZPDAPICADO_0

        public STOCK()
        {

        }
    }
}
