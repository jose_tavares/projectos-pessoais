﻿using System;
using MODELS.Helpers;
using MODELS.Attributes;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MODELS.BusinessObject.SAGEX3.Database
{
    // MFGHEAD
    [Table("[MFGHEAD]")]
    public partial class MFGHEAD : DBRules
    {
        [MaxLength(20)]
        [StringLength(20)]
        [Required(AllowEmptyStrings = true)]
        public string MFGNUM_0 { get; set; } // MFGNUM_0 (length: 20)

        [Required]
        public byte MFGSTA_0 { get; set; } // MFGSTA_0

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string PLNFCY_0 { get; set; } // PLNFCY_0 (length: 5)

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string MFGFCY_0 { get; set; } // MFGFCY_0 (length: 5)

        [Required]
        public DateTime STRDAT_0 { get; set; } // STRDAT_0

        [Required]
        public DateTime ENDDAT_0 { get; set; } // ENDDAT_0

        [MaxLength(3)]
        [StringLength(3)]
        [Required(AllowEmptyStrings = true)]
        public string STU_0 { get; set; } // STU_0 (length: 3)

        [Required]
        [Decimal(28, 13)]
        public decimal EXTQTY_0 { get; set; } // EXTQTY_0

        [Required]
        [Decimal(28, 13)]
        public decimal CPLQTY_0 { get; set; } // CPLQTY_0

        [Required]
        [Decimal(28, 13)]
        public decimal REJCPLQTY_0 { get; set; } // REJCPLQTY_0

        [Required]
        [Decimal(28, 13)]
        public decimal QUACPLQTY_0 { get; set; } // QUACPLQTY_0

        [Required]
        [Decimal(28, 13)]
        public decimal RMNEXTQTY_0 { get; set; } // RMNEXTQTY_0

        [Required]
        public byte ZOFRETRAB_0 { get; set; } // ZOFRETRAB_0

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string ZMOTRETRAB_0 { get; set; } // ZMOTRETRAB_0 (length: 10)

        [Required]
        public byte ZOFAMOSTRA_0 { get; set; } // ZOFAMOSTRA_0

        [MaxLength(20)]
        [StringLength(20)]
        [Required(AllowEmptyStrings = true)]
        public string ZPAIP_0 { get; set; } // ZPAIP_0 (length: 20)

        [MaxLength(90)]
        [StringLength(90)]
        [Required(AllowEmptyStrings = true)]
        public string ZOBS_0 { get; set; } // ZOBS_0 (length: 90)

        [Required]
        public byte ZIMPPAIP_0 { get; set; } // ZIMPPAIP_0

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string ZDERROGA_0 { get; set; } // ZDERROGA_0 (length: 10)

        [MaxLength(15)]
        [StringLength(15)]
        [Required(AllowEmptyStrings = true)]
        public string BPCNUM_0 { get; set; } // BPCNUM_0 (length: 15)

        [Required]
        public byte ZFECHOOF_0 { get; set; } // ZFECHOOF_0

        [Required]
        [Decimal(28, 13)]
        public decimal ZESIMPORTA_0 { get; set; } // ZESIMPORTA_0

        [Required]
        [Decimal(28, 13)]
        public decimal ZESREALIZA_0 { get; set; } // ZESREALIZA_0

        [Required]
        [Range(0, int.MaxValue)]
        public int ZENSAIO_0 { get; set; } // ZENSAIO_0

        [Required]
        public byte ZLIBERT1FACE_0 { get; set; } // ZLIBERT1FACE_0

        [Required]
        public byte ZOFSUBCONTR_0 { get; set; } // ZOFSUBCONTR_0

        [Required]
        public byte ZPRODISO_0 { get; set; } // ZPRODISO_0

        [Required]
        public byte ZOFENG_0 { get; set; } // ZOFENG_0

        [Required]
        public byte ZMULTI_0 { get; set; } // ZMULTI_0

        [Required]
        [MaxLength(20)]
        [StringLength(20)]
        public string ZMFGTYP_0 { get; set; } // ZMFGTYP_0

        public MFGHEAD()
        {
            InitializePartial();
        }

        partial void InitializePartial();
    }
}
