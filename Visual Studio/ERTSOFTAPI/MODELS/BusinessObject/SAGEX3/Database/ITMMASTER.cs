﻿using System;
using MODELS.Helpers;
using MODELS.Attributes;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MODELS.BusinessObject.SAGEX3.Database
{
    // ITMMASTER
    [Table("[ITMMASTER]")]
    public partial class ITMMASTER : DBRules
    {
        [MaxLength(20)]
        [StringLength(20)]
        [Required(AllowEmptyStrings = true)]
        public string ITMREF_0 { get; set; } // ITMREF_0 (length: 20)

        [MaxLength(20)]
        [StringLength(20)]
        [Required(AllowEmptyStrings = true)]
        public string TSICOD_0 { get; set; } // TSICOD_0 (length: 20)

        [MaxLength(20)]
        [StringLength(20)]
        [Required(AllowEmptyStrings = true)]
        public string TSICOD_1 { get; set; } // TSICOD_1 (length: 20)

        [MaxLength(20)]
        [StringLength(20)]
        [Required(AllowEmptyStrings = true)]
        public string TSICOD_2 { get; set; } // TSICOD_2 (length: 20)

        [MaxLength(20)]
        [StringLength(20)]
        [Required(AllowEmptyStrings = true)]
        public string TSICOD_3 { get; set; } // TSICOD_3 (length: 20)

        [MaxLength(20)]
        [StringLength(20)]
        [Required(AllowEmptyStrings = true)]
        public string TSICOD_4 { get; set; } // TSICOD_4 (length: 20)

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string TCLCOD_0 { get; set; } // TCLCOD_0 (length: 5)

        [MaxLength(3)]
        [StringLength(3)]
        [Required(AllowEmptyStrings = true)]
        public string DIE_0 { get; set; } // DIE_0 (length: 3)

        [MaxLength(3)]
        [StringLength(3)]
        [Required(AllowEmptyStrings = true)]
        public string DIE_1 { get; set; } // DIE_1 (length: 3)

        [MaxLength(3)]
        [StringLength(3)]
        [Required(AllowEmptyStrings = true)]
        public string DIE_2 { get; set; } // DIE_2 (length: 3)

        [MaxLength(3)]
        [StringLength(3)]
        [Required(AllowEmptyStrings = true)]
        public string DIE_3 { get; set; } // DIE_3 (length: 3)

        [MaxLength(3)]
        [StringLength(3)]
        [Required(AllowEmptyStrings = true)]
        public string DIE_4 { get; set; } // DIE_4 (length: 3)

        [MaxLength(3)]
        [StringLength(3)]
        [Required(AllowEmptyStrings = true)]
        public string DIE_5 { get; set; } // DIE_5 (length: 3)

        [MaxLength(3)]
        [StringLength(3)]
        [Required(AllowEmptyStrings = true)]
        public string DIE_6 { get; set; } // DIE_6 (length: 3)

        [MaxLength(3)]
        [StringLength(3)]
        [Required(AllowEmptyStrings = true)]
        public string DIE_7 { get; set; } // DIE_7 (length: 3)

        [MaxLength(3)]
        [StringLength(3)]
        [Required(AllowEmptyStrings = true)]
        public string DIE_8 { get; set; } // DIE_8 (length: 3)

        [MaxLength(3)]
        [StringLength(3)]
        [Required(AllowEmptyStrings = true)]
        public string DIE_9 { get; set; } // DIE_9 (length: 3)

        [MaxLength(3)]
        [StringLength(3)]
        [Required(AllowEmptyStrings = true)]
        public string DIE_10 { get; set; } // DIE_10 (length: 3)

        [MaxLength(3)]
        [StringLength(3)]
        [Required(AllowEmptyStrings = true)]
        public string DIE_11 { get; set; } // DIE_11 (length: 3)

        [MaxLength(3)]
        [StringLength(3)]
        [Required(AllowEmptyStrings = true)]
        public string DIE_12 { get; set; } // DIE_12 (length: 3)

        [MaxLength(3)]
        [StringLength(3)]
        [Required(AllowEmptyStrings = true)]
        public string DIE_13 { get; set; } // DIE_13 (length: 3)

        [MaxLength(3)]
        [StringLength(3)]
        [Required(AllowEmptyStrings = true)]
        public string DIE_14 { get; set; } // DIE_14 (length: 3)

        [MaxLength(3)]
        [StringLength(3)]
        [Required(AllowEmptyStrings = true)]
        public string DIE_15 { get; set; } // DIE_15 (length: 3)

        [MaxLength(3)]
        [StringLength(3)]
        [Required(AllowEmptyStrings = true)]
        public string DIE_16 { get; set; } // DIE_16 (length: 3)

        [MaxLength(3)]
        [StringLength(3)]
        [Required(AllowEmptyStrings = true)]
        public string DIE_17 { get; set; } // DIE_17 (length: 3)

        [MaxLength(3)]
        [StringLength(3)]
        [Required(AllowEmptyStrings = true)]
        public string DIE_18 { get; set; } // DIE_18 (length: 3)

        [MaxLength(3)]
        [StringLength(3)]
        [Required(AllowEmptyStrings = true)]
        public string DIE_19 { get; set; } // DIE_19 (length: 3)

        [MaxLength(15)]
        [StringLength(15)]
        [Required(AllowEmptyStrings = true)]
        public string CCE_0 { get; set; } // CCE_0 (length: 15)

        [MaxLength(15)]
        [StringLength(15)]
        [Required(AllowEmptyStrings = true)]
        public string CCE_1 { get; set; } // CCE_1 (length: 15)

        [MaxLength(15)]
        [StringLength(15)]
        [Required(AllowEmptyStrings = true)]
        public string CCE_2 { get; set; } // CCE_2 (length: 15)

        [MaxLength(15)]
        [StringLength(15)]
        [Required(AllowEmptyStrings = true)]
        public string CCE_3 { get; set; } // CCE_3 (length: 15)

        [MaxLength(15)]
        [StringLength(15)]
        [Required(AllowEmptyStrings = true)]
        public string CCE_4 { get; set; } // CCE_4 (length: 15)

        [MaxLength(15)]
        [StringLength(15)]
        [Required(AllowEmptyStrings = true)]
        public string CCE_5 { get; set; } // CCE_5 (length: 15)

        [MaxLength(15)]
        [StringLength(15)]
        [Required(AllowEmptyStrings = true)]
        public string CCE_6 { get; set; } // CCE_6 (length: 15)

        [MaxLength(15)]
        [StringLength(15)]
        [Required(AllowEmptyStrings = true)]
        public string CCE_7 { get; set; } // CCE_7 (length: 15)

        [MaxLength(15)]
        [StringLength(15)]
        [Required(AllowEmptyStrings = true)]
        public string CCE_8 { get; set; } // CCE_8 (length: 15)

        [MaxLength(15)]
        [StringLength(15)]
        [Required(AllowEmptyStrings = true)]
        public string CCE_9 { get; set; } // CCE_9 (length: 15)

        [MaxLength(15)]
        [StringLength(15)]
        [Required(AllowEmptyStrings = true)]
        public string CCE_10 { get; set; } // CCE_10 (length: 15)

        [MaxLength(15)]
        [StringLength(15)]
        [Required(AllowEmptyStrings = true)]
        public string CCE_11 { get; set; } // CCE_11 (length: 15)

        [MaxLength(15)]
        [StringLength(15)]
        [Required(AllowEmptyStrings = true)]
        public string CCE_12 { get; set; } // CCE_12 (length: 15)

        [MaxLength(15)]
        [StringLength(15)]
        [Required(AllowEmptyStrings = true)]
        public string CCE_13 { get; set; } // CCE_13 (length: 15)

        [MaxLength(15)]
        [StringLength(15)]
        [Required(AllowEmptyStrings = true)]
        public string CCE_14 { get; set; } // CCE_14 (length: 15)

        [MaxLength(15)]
        [StringLength(15)]
        [Required(AllowEmptyStrings = true)]
        public string CCE_15 { get; set; } // CCE_15 (length: 15)

        [MaxLength(15)]
        [StringLength(15)]
        [Required(AllowEmptyStrings = true)]
        public string CCE_16 { get; set; } // CCE_16 (length: 15)

        [MaxLength(15)]
        [StringLength(15)]
        [Required(AllowEmptyStrings = true)]
        public string CCE_17 { get; set; } // CCE_17 (length: 15)

        [MaxLength(15)]
        [StringLength(15)]
        [Required(AllowEmptyStrings = true)]
        public string CCE_18 { get; set; } // CCE_18 (length: 15)

        [MaxLength(15)]
        [StringLength(15)]
        [Required(AllowEmptyStrings = true)]
        public string CCE_19 { get; set; } // CCE_19 (length: 15)

        [MaxLength(30)]
        [StringLength(30)]
        [Required(AllowEmptyStrings = true)]
        public string ITMDES1_0 { get; set; } // ITMDES1_0 (length: 30)

        [MaxLength(30)]
        [StringLength(30)]
        [Required(AllowEmptyStrings = true)]
        public string ITMDES2_0 { get; set; } // ITMDES2_0 (length: 30)

        [MaxLength(30)]
        [StringLength(30)]
        [Required(AllowEmptyStrings = true)]
        public string ITMDES3_0 { get; set; } // ITMDES3_0 (length: 30)

        [Required]
        public byte ITMSTA_0 { get; set; } // ITMSTA_0

        [MaxLength(3)]
        [StringLength(3)]
        [Required(AllowEmptyStrings = true)]
        public string STU_0 { get; set; } // STU_0 (length: 3)

        [MaxLength(3)]
        [StringLength(3)]
        [Required(AllowEmptyStrings = true)]
        public string WEU_0 { get; set; } // WEU_0 (length: 3)

        [Required]
        [Decimal(13, 4)]
        public decimal ITMWEI_0 { get; set; } // ITMWEI_0

        [MaxLength(3)]
        [StringLength(3)]
        [Required(AllowEmptyStrings = true)]
        public string PCU_0 { get; set; } // PCU_0 (length: 3)

        [Required]
        public short STOMGTCOD_0 { get; set; }

        [Required]
        [Decimal(18, 7)]
        public decimal PCUSTUCOE_0 { get; set; } // PCUSTUCOE_0

        [MaxLength(12)]
        [StringLength(12)]
        [Required(AllowEmptyStrings = true)]
        public string CUSREF_0 { get; set; } // CUSREF_0 (length: 12)

        [Required]
        public byte ZREPORTES_0 { get; set; } // ZREPORTES_0

        [Required]
        public byte ZCOMPRAS_0 { get; set; } // ZCOMPRAS_0

        [Required]
        public byte ZLOGISTICA_0 { get; set; } // ZLOGISTICA_0

        [Required]
        public byte ZFINANCEIRA_0 { get; set; } // ZFINANCEIRA_0

        [Required]
        public byte ZCQ_0 { get; set; } // ZCQ_0

        [MaxLength(10)]
        [StringLength(10)]
        [Required(AllowEmptyStrings = true)]
        public string ZMPD_0 { get; set; } // ZMPD_0 (length: 10)

        [Required]
        public byte ZOEKOTEX_0 { get; set; } // ZOEKOTEX_0

        [MaxLength(20)]
        [StringLength(20)]
        [Required(AllowEmptyStrings = true)]
        public string ZOEM_0 { get; set; } // ZOEM_0 (length: 20)

        [Required]
        [Decimal(28, 13)]
        public decimal ZLITROS_0 { get; set; } // ZLITROS_0

        [Required]
        public byte ZISP_0 { get; set; } // ZISP_0

        [Required]
        [Decimal(28, 13)]
        public decimal ZDENSIDADE_0 { get; set; } // ZDENSIDADE_0

        [Required]
        public byte ZADR_0 { get; set; } // ZADR_0

        [Required]
        public byte ZTIPO_0 { get; set; } // ZTIPO_0

        [Required]
        public byte ZMANWINWIN_0 { get; set; } // ZMANWINWIN_0

        [Required]
        public byte ZSUBCONTR_0 { get; set; } // ZSUBCONTR_0

        [Required]
        public byte ZACSTD_0 { get; set; } // ZACSTD_0

        [Required]
        public byte ZACSTDA_0 { get; set; } // ZACSTDA_0

        [MaxLength(20)]
        [StringLength(20)]
        [Required(AllowEmptyStrings = true)]
        public string ZCOR_0 { get; set; } // ZCOR_0 (length: 20)

        [Required]
        public short ZPROD2SLOTE_0 { get; set; } // ZPROD2SLOTE_0

        [Required]
        public byte ZCONTROLQTY_0 { get; set; } // ZCONTROLQTY_0

        [MaxLength(15)]
        [StringLength(15)]
        [Required(AllowEmptyStrings = true)]
        public string ZRASTOCLI_0 { get; set; } // ZRASTOCLI_0 (length: 15)

        [Required]
        public byte ZPRODISO_0 { get; set; } // ZPRODISO_0

        [MaxLength(20)]
        [StringLength(20)]
        [Required(AllowEmptyStrings = true)]
        public string ZOPERRPA_0 { get; set; } // ZOPERRPA_0 (length: 20)

        public ITMMASTER()
        {
            InitializePartial();
        }

        partial void InitializePartial();
    }
}
