﻿using System;
using MODELS.Helpers;
using MODELS.Attributes;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MODELS.BusinessObject.SAGEX3.Database
{
    // ATABDIV
    [Table("[ATABDIV]")]
    public partial class ATABDIV : DBRules
    {
        [Required]
        [Range(0, short.MaxValue)]
        public short NUMTAB_0 { get; set; } // NUMTAB_0

        [MaxLength(20)]
        [StringLength(20)]
        [Required(AllowEmptyStrings = true)]
        public string CODE_0 { get; set; } // CODE_0 (length: 20)

        [MaxLength(40)]
        [StringLength(40)]
        [Required(AllowEmptyStrings = true)]
        public string A1_0 { get; set; } // A1_0 (length: 40)

        [MaxLength(40)]
        [StringLength(40)]
        [Required(AllowEmptyStrings = true)]
        public string A2_0 { get; set; } // A2_0 (length: 40)

        [MaxLength(40)]
        [StringLength(40)]
        [Required(AllowEmptyStrings = true)]
        public string A3_0 { get; set; } // A3_0 (length: 40)

        [MaxLength(40)]
        [StringLength(40)]
        [Required(AllowEmptyStrings = true)]
        public string A4_0 { get; set; } // A4_0 (length: 40)

        [MaxLength(40)]
        [StringLength(40)]
        [Required(AllowEmptyStrings = true)]
        public string A5_0 { get; set; } // A5_0 (length: 40)

        [MaxLength(40)]
        [StringLength(40)]
        [Required(AllowEmptyStrings = true)]
        public string A6_0 { get; set; } // A6_0 (length: 40)

        [MaxLength(40)]
        [StringLength(40)]
        [Required(AllowEmptyStrings = true)]
        public string A7_0 { get; set; } // A7_0 (length: 40)

        [MaxLength(40)]
        [StringLength(40)]
        [Required(AllowEmptyStrings = true)]
        public string A8_0 { get; set; } // A8_0 (length: 40)

        [MaxLength(40)]
        [StringLength(40)]
        [Required(AllowEmptyStrings = true)]
        public string A9_0 { get; set; } // A9_0 (length: 40)

        [MaxLength(40)]
        [StringLength(40)]
        [Required(AllowEmptyStrings = true)]
        public string A10_0 { get; set; } // A10_0 (length: 40)

        [MaxLength(40)]
        [StringLength(40)]
        [Required(AllowEmptyStrings = true)]
        public string A11_0 { get; set; } // A11_0 (length: 40)

        [MaxLength(40)]
        [StringLength(40)]
        [Required(AllowEmptyStrings = true)]
        public string A12_0 { get; set; } // A12_0 (length: 40)

        [MaxLength(40)]
        [StringLength(40)]
        [Required(AllowEmptyStrings = true)]
        public string A13_0 { get; set; } // A13_0 (length: 40)

        [MaxLength(40)]
        [StringLength(40)]
        [Required(AllowEmptyStrings = true)]
        public string A14_0 { get; set; } // A14_0 (length: 40)

        [MaxLength(40)]
        [StringLength(40)]
        [Required(AllowEmptyStrings = true)]
        public string A15_0 { get; set; } // A15_0 (length: 40)

        [Required]
        [Decimal(24, 7)]
        public decimal N1_0 { get; set; } // N1_0

        [Required]
        [Decimal(24, 7)]
        public decimal N2_0 { get; set; } // N2_0

        [Required]
        [Decimal(24, 7)]
        public decimal N3_0 { get; set; } // N3_0

        [Required]
        [Decimal(24, 7)]
        public decimal N4_0 { get; set; } // N4_0

        [Required]
        [Decimal(24, 7)]
        public decimal N5_0 { get; set; } // N5_0

        [Required]
        [Decimal(24, 7)]
        public decimal N6_0 { get; set; } // N6_0

        [Required]
        [Decimal(24, 7)]
        public decimal N7_0 { get; set; } // N7_0

        [Required]
        [Decimal(24, 7)]
        public decimal N8_0 { get; set; } // N8_0

        [Required]
        [Decimal(24, 7)]
        public decimal N9_0 { get; set; } // N9_0

        [Required]
        [Decimal(24, 7)]
        public decimal N10_0 { get; set; } // N10_0

        [Required]
        [Decimal(24, 7)]
        public decimal N11_0 { get; set; } // N11_0

        [Required]
        [Decimal(24, 7)]
        public decimal N12_0 { get; set; } // N12_0

        [Required]
        [Decimal(24, 7)]
        public decimal N13_0 { get; set; } // N13_0

        [Required]
        [Decimal(24, 7)]
        public decimal N14_0 { get; set; } // N14_0

        [Required]
        [Decimal(24, 7)]
        public decimal N15_0 { get; set; } // N15_0

        [Required]
        public byte DEFVAL_0 { get; set; } // DEFVAL_0

        [MaxLength(20)]
        [StringLength(20)]
        [Required(AllowEmptyStrings = true)]
        public string DEPCOD_0 { get; set; } // DEPCOD_0 (length: 20)

        [MaxLength(5)]
        [StringLength(5)]
        [Required(AllowEmptyStrings = true)]
        public string SOC_0 { get; set; } // SOC_0 (length: 5)

        [MaxLength(20)]
        [StringLength(20)]
        [Required(AllowEmptyStrings = true)]
        public string LEG_0 { get; set; } // LEG_0 (length: 20)

        [Required]
        public byte ENAFLG_0 { get; set; } // ENAFLG_0

        public ATABDIV()
        {
            InitializePartial();
        }

        partial void InitializePartial();
    }
}
