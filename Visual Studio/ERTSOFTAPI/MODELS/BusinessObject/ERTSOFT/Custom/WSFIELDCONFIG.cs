﻿using System.ComponentModel.DataAnnotations.Schema;

namespace MODELS.BusinessObject.ERTSOFT.Database
{
    public partial class WSFIELDCONFIG
    {
        [NotMapped]
        public WEBSERVICE WebserviceModel { get; set; }
    }
}
