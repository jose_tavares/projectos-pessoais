﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace MODELS.BusinessObject.ERTSOFT.Database
{
    
    public partial class APP
    {
        [NotMapped]
        public IEnumerable<MODULE> ModuleList { get; set; }
    }
}
