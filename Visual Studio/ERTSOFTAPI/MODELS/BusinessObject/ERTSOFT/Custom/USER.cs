﻿using System.ComponentModel.DataAnnotations.Schema;

namespace MODELS.BusinessObject.ERTSOFT.Database
{
    public partial class USER
    {
        [NotMapped]
        public PROFILE ProfileModel { get; set; }
    }
}
