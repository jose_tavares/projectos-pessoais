﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace MODELS.BusinessObject.ERTSOFT.Database
{
    public partial class WEBSERVICE
    {
        [NotMapped]
        public IEnumerable<WSFIELDCONFIG> WsFieldConfigList { get; set; }

        [NotMapped]
        public IEnumerable<IMPORTWSDATA> ImportWsDataList { get; set; }

        [NotMapped]
        public IMPORTWS ImportWsModel { get; set; }
    }
}
