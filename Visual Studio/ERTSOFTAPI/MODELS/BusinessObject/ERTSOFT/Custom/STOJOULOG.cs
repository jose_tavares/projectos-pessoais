﻿using System.ComponentModel.DataAnnotations.Schema;

namespace MODELS.BusinessObject.ERTSOFT.Database
{
    public partial class STOJOULOG
    {
        [NotMapped]
        public STOJOU StojouModel { get; set; }
    }
}
