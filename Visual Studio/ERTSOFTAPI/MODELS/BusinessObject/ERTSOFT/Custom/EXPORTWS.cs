﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace MODELS.BusinessObject.ERTSOFT.Database
{
    public partial class EXPORTWS
    {
        [NotMapped]
        public WEBSERVICE WebServiceModel { get; set; }

        [NotMapped]
        public IEnumerable<EXPORTWSLOG> ExportWSLogList { get; set; }
    }
}
