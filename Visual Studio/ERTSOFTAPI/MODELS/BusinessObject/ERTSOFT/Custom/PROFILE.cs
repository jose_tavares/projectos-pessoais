﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace MODELS.BusinessObject.ERTSOFT.Database
{  
    public partial class PROFILE
    {
        [NotMapped]
        public IEnumerable<USER> UserList { get; set; }
    }
}
