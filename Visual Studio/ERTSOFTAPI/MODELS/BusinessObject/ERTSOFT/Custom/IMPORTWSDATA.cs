﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace MODELS.BusinessObject.ERTSOFT.Database
{
    public partial class IMPORTWSDATA
    {
        [NotMapped]
        public IMPORTWS ImportWsModel { get; set; }
    }
}
