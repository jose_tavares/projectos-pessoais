﻿using System.ComponentModel.DataAnnotations.Schema;

namespace MODELS.BusinessObject.ERTSOFT.Database
{
    public partial class IMPORTWS
    {
        [NotMapped]
        public WEBSERVICE WebServiceModel { get; set; }

        [NotMapped]
        public IMPORTWSDATA ImportWsDataModel { get; set; }
    }
}