﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace MODELS.BusinessObject.ERTSOFT.Database
{
    public partial class STOJOU
    {
        [NotMapped]
        public IEnumerable<STOJOULOG> StojouLogList { get; set; }

        [NotMapped]
        public WEBSERVICE WebServiceModel { get; set; }

        [NotMapped]
        public DOCALL DocAllModel { get; set; }
    }
}
