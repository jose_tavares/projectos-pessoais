﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace MODELS.BusinessObject.ERTSOFT.Database
{
    public partial class MODULE
    {
        [NotMapped]
        public APP AppModel { get; set; }

        [NotMapped]
        public IEnumerable<SCREEN> ScreenList { get; set; }
    }
}
