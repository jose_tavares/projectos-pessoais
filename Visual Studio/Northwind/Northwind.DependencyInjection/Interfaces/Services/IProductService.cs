﻿namespace Northwind.DependencyInjection.Interfaces.Services
{
    using System;
    using System.Collections.Generic;
    using Northwind.Domain.Models;

    public interface IProductService
    {
        Tuple<int, IEnumerable<Product>> Entities(int page);
    }
}
