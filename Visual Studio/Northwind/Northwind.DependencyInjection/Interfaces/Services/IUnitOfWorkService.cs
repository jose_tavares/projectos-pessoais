﻿namespace Northwind.DependencyInjection.Interfaces.Services
{
    using System;

    public interface IUnitOfWorkService : IDisposable
    {
        IGenericService<TEntity> GetService<TEntity>() where TEntity : class;
    }
}
