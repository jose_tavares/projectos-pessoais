﻿namespace Northwind.DependencyInjection.Interfaces.Services
{
    using System;
    using System.Collections.Generic;

    public interface IGenericService<TEntity> where TEntity : class
    {
        Tuple<int, IEnumerable<TEntity>> Entities(string requestUri, int page);
    }
}
