﻿namespace Northwind.DependencyInjection.Interfaces.Repositories
{
    using Northwind.Common.Pagination;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;

    public interface IGenericRepository<TEntity> where TEntity : class
    {
        int Count { get; }

        /// <summary>
        ///     <para>
        ///         Begins tracking the given entity, and any other reachable entities that are
        ///         not already being tracked, in the <see cref="Microsoft.EntityFrameworkCore.EntityState.Added" /> state such that they will
        ///         be inserted into the database when <see cref="Microsoft.EntityFrameworkCore.DbContext.SaveChanges()" /> is called.
        ///     </para>
        ///     <para>
        ///         This method is async only to allow special value generators, such as the one used by
        ///         'Microsoft.EntityFrameworkCore.Metadata.SqlServerValueGenerationStrategy.SequenceHiLo',
        ///         to access the database asynchronously. For all other cases the non async method should be used.
        ///     </para>
        ///     <para>
        ///         Use <see cref="Microsoft.EntityFrameworkCore.ChangeTracking.EntityEntry.State" /> to set the state of only a single entity.
        ///     </para>
        /// </summary>
        /// <param name="entity"> The entity to add. </param>
        /// <returns>
        ///     A task that represents the asynchronous Add operation. The task result contains the
        ///     <see cref="Microsoft.EntityFrameworkCore.ChangeTracking.EntityEntry{TEntity}" /> for the entity. The entry provides access to change tracking
        ///     information and operations for the entity.
        /// </returns>
        Task AddAsync(TEntity entity);

        bool Any(Expression<Func<TEntity, bool>> predicate);

        Task<List<TEntity>> Entities();

        PagedResult<TEntity> Entities(int page);

        void Remove(TEntity entity);

        void RemoveRange(IEnumerable<TEntity> entities);

        ValueTask<TEntity> FindAsync(params object[] keyValues);

        void Update(TEntity entity);

        IQueryable<TEntity> Where(Expression<Func<TEntity, bool>> predicate);
    }
}
