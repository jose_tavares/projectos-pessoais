﻿namespace Northwind.DependencyInjection.Interfaces.Repositories
{
    using System.Threading.Tasks;
    using Northwind.Domain.Models;
    using System.Collections.Generic;
    using Northwind.Common.Pagination;

    public interface IProductRepository
    {
        int Count { get; }

        Task<int> AddAsync(Product product);

        Task<bool> AddMultipleAsync(IEnumerable<Product> products);

        bool Any(int id);

        ValueTask<Product> FindAsync(int id);

        Task<List<Product>> Entities();

        PagedResult<Product> Entities(int page);

        Task<int> Remove(Product product);

        Task<int> Update(Product product);
    }
}
