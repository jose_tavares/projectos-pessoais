﻿namespace Northwind.DependencyInjection.Interfaces.Repositories
{
    using System;
    using System.Threading.Tasks;

    public interface IUnitOfWorkRepository : IDisposable
    {
        IGenericRepository<TEntity> GetRepository<TEntity>() where TEntity : class;

        Task BeginTransactionAsync();

        Task CommitAsync();

        Task<int> SaveChangesAsync();
    }
}
