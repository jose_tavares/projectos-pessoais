﻿namespace Northwind.DependencyInjection.Infrastructure
{
    using AutoMapper;
    using Microsoft.Extensions.DependencyInjection;
    using Northwind.DependencyInjection.Implementations.Repositories;
    using Northwind.DependencyInjection.Implementations.Services;
    using Northwind.DependencyInjection.Interfaces.Repositories;
    using Northwind.DependencyInjection.Interfaces.Services;
    using Northwind.Domain.MappingProfile;

    public static class DependencyInjectionStartup
    {
        public static void ConfigureServices(this IServiceCollection services, bool isApplication = false)
        {
            services.AddScopeds(isApplication);

            services.AddAutoMapper(mapperConfigurationExpression =>
                mapperConfigurationExpression.AddMappingProfiles(isApplication));
        }

        private static void AddScopeds(this IServiceCollection services, bool isApplication)
        {
            if (isApplication)
            {
                services.AddScoped<IUnitOfWorkService, UnitOfWorkService>();
                services.AddScoped<IProductService, ProductService>();

            }
            else
            {
                services.AddScoped<IUnitOfWorkRepository, UnitOfWorkRepository>();
                services.AddScoped<IProductRepository, ProductRepository>();
            }
        }

        private static void AddMappingProfiles(this IMapperConfigurationExpression mapperConfigurationExpression, bool isApplication) => mapperConfigurationExpression.AddProfile(new ProductMappingProfile(isApplication));
    }
}
