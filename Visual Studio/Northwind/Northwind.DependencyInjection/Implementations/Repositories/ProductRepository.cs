﻿namespace Northwind.DependencyInjection.Implementations.Repositories
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Northwind.Common.Pagination;
    using Northwind.DependencyInjection.Interfaces.Repositories;
    using Northwind.Domain.Models;

    public class ProductRepository : IProductRepository
    {
        private readonly IUnitOfWorkRepository unitOfWorkRepository;
        private readonly IGenericRepository<Product> productRepository;

        public ProductRepository(IUnitOfWorkRepository unitOfWorkRepository)
        {
            this.unitOfWorkRepository = unitOfWorkRepository;
            productRepository = unitOfWorkRepository.GetRepository<Product>();
        }

        public int Count =>
            productRepository.Count;

        public async Task<int> AddAsync(Product product)
        {
            await unitOfWorkRepository.BeginTransactionAsync();
            await productRepository.AddAsync(product);
            int result = await unitOfWorkRepository.SaveChangesAsync();
            await unitOfWorkRepository.CommitAsync();

            return result;
        }

        public async Task<bool> AddMultipleAsync(IEnumerable<Product> products)
        {
            var addTasks = products.Select(product => AddAsync(product));
            int[] entries = await Task.WhenAll(addTasks);

            return products.Count() == entries.Count();
        }

        public bool Any(int id) =>
            productRepository.Any(any => any.ProductId == id);

        public ValueTask<Product> FindAsync(int id) =>
            productRepository.FindAsync(id);

        public Task<List<Product>> Entities() =>
            productRepository.Entities();

        public Task<int> Remove(Product product)
        {
            productRepository.Remove(product);
            return unitOfWorkRepository.SaveChangesAsync();
        }

        public Task<int> Update(Product product)
        {
            productRepository.Update(product);
            return unitOfWorkRepository.SaveChangesAsync();
        }

        public PagedResult<Product> Entities(int page) =>
            productRepository.Entities(page);
    }
}
