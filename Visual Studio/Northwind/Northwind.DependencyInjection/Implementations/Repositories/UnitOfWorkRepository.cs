﻿namespace Northwind.DependencyInjection.Implementations.Repositories
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Storage;
    using Northwind.Common.Extensions;
    using Northwind.DependencyInjection.Interfaces.Repositories;
    using Northwind.Domain.Models;

    public class UnitOfWorkRepository : IUnitOfWorkRepository
    {
        private readonly NorthwindContext context;
        private bool disposed = false;
        private IDbContextTransaction transaction;

        public UnitOfWorkRepository() =>
            context = new NorthwindContext();

        public IGenericRepository<TEntity> GetRepository<TEntity>() where TEntity : class
            => new GenericRepository<TEntity>(context);

        public async Task BeginTransactionAsync()
            => transaction = await context.Database.BeginTransactionAsync();

        public async Task CommitAsync()
        {
            try
            {
                await transaction.CommitAsync();
            }
            catch (Exception)
            {
                await transaction.RollbackAsync();
                throw;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public Task<int> SaveChangesAsync()
        {
            var entites = context.ChangeTracker.Entries()
                          .Where(where => where.State == EntityState.Added
                              || where.State == EntityState.Deleted
                              || where.State == EntityState.Modified)
                          .Select(select => select.Entity);

            if (entites.Validate())
            {
                foreach (object entity in entites)
                {
                    var validationContext = new ValidationContext(entity);

                    Validator.ValidateObject(entity, validationContext, true);
                }
            }

            return context.SaveChangesAsync();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }

                disposed = true;
            }
        }
    }
}
