﻿namespace Northwind.DependencyInjection.Implementations.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using Microsoft.EntityFrameworkCore;
    using Northwind.Common.Extensions;
    using Northwind.Common.Pagination;
    using Northwind.DependencyInjection.Interfaces.Repositories;
    using Northwind.Domain.Models;

    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        private readonly DbSet<TEntity> entities;
        private readonly NorthwindContext context;

        public GenericRepository(NorthwindContext context)
        {
            this.context = context;
            entities = context.Set<TEntity>();
        }

        public int Count =>
            entities.Count();

        public async Task AddAsync(TEntity entity) =>
            await entities.AddAsync(entity);

        public bool Any(Expression<Func<TEntity, bool>> predicate) =>
            entities.Any(predicate);

        public Task<List<TEntity>> Entities() =>
            entities.ToListAsync();

        public PagedResult<TEntity> Entities(int page) =>
            entities.GetPaged(page, 10);

        public ValueTask<TEntity> FindAsync(params object[] keyValues) =>
            entities.FindAsync(keyValues);

        public void Remove(TEntity entity) =>
            entities.Remove(entity);

        public void RemoveRange(IEnumerable<TEntity> entities) =>
            this.entities.RemoveRange(entities);

        public void Update(TEntity entity)
        {
            context.Entry(entity).State = EntityState.Modified;
            entities.Update(entity);
        }

        public IQueryable<TEntity> Where(Expression<Func<TEntity, bool>> predicate) =>
            entities.Where(predicate);
    }
}
