﻿namespace Northwind.DependencyInjection.Implementations.Services
{
    using System;
    using System.Net.Http;
    using Northwind.DependencyInjection.Interfaces.Services;

    public class UnitOfWorkService : IUnitOfWorkService
    {
        private readonly HttpClient client;

        private bool disposed = false;

        public UnitOfWorkService() =>
            client = new HttpClient()
            {
                BaseAddress = new Uri("https://localhost:44330/api/")
            };

        public IGenericService<TEntity> GetService<TEntity>() where TEntity : class
        {
            return new GenericService<TEntity>(client);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    client.Dispose();
                }

                disposed = true;
            }
        }
    }
}
