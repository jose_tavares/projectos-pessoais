﻿namespace Northwind.DependencyInjection.Implementations.Services
{
    using System;
    using System.Collections.Generic;
    using Northwind.DependencyInjection.Interfaces.Services;
    using Northwind.Domain.Models;

    public class ProductService : IProductService
    {
        private readonly IGenericService<Product> productService;

        public ProductService(IUnitOfWorkService unitOfWorkService) => 
            productService = unitOfWorkService.GetService<Product>();

        public Tuple<int, IEnumerable<Product>> Entities(int page) =>
            productService.Entities("products", page);
    }
}
