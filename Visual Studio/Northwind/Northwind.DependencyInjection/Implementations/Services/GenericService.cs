﻿namespace Northwind.DependencyInjection.Implementations.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using Northwind.Common.Extensions;
    using Northwind.DependencyInjection.Interfaces.Services;

    public class GenericService<TEntity> : IGenericService<TEntity> where TEntity : class
    {
        private readonly HttpClient client;

        public GenericService(HttpClient client) =>
            this.client = client;

        public Tuple<int, IEnumerable<TEntity>> Entities(string requestUri, int page)
        {
            var responseTask = client.GetAsync(requestUri + "?page=" + page);
            responseTask.Wait();

            var result = responseTask.Result;
            if (result.IsSuccessStatusCode)
            {
                var readTask = result.Content.ReadAsJsonAsync<Tuple<int, IEnumerable<TEntity>>>();

                return readTask.Result;
            }

            return Tuple.Create(0, Enumerable.Empty<TEntity>());
        }
    }
}
