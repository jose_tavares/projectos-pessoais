namespace Northwind.Test
{
    using FluentValidation;
    using Moq;
    using Northwind.DependencyInjection.Interfaces.Repositories;
    using Northwind.Domain.EntityValidators;
    using Northwind.Domain.Models;
    using NUnit.Framework;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class ProductRepositoryTest
    {
        private Mock<IProductRepository> _productRepositoryMock;
        private ProductServiceArrange _productServiceArrange;

        [SetUp]
        public void Setup()
        {
            _productRepositoryMock = new Mock<IProductRepository>();
            _productServiceArrange = new ProductServiceArrange(_productRepositoryMock.Object);
        }

        [Test]
        public async Task TestEntities()
        {
            var mockProducts = new List<Product>
            {
                new Product { ProductId = 1, ProductName = "Chai", UnitPrice = 180000 },
                new Product { ProductId = 2, ProductName = "Chang", UnitPrice = 190000 }
            };

            _productRepositoryMock
                .Setup(repo => repo.Entities())
                .ReturnsAsync(mockProducts);

            var result = await _productServiceArrange.Entities();

            Assert.That(result.Count(), Is.EqualTo(2));
        }

        [Test]
        public async Task TestAdd_ValidEntity_SuccessfullyAdded()
        {
            // Arrange
            var validProduct = new Product
            {
                ProductId = 1,
                ProductName = "Chai"
            };

            _productRepositoryMock.Setup(repo => repo.AddAsync(It.IsAny<Product>())).Returns(Task.FromResult(1));

            // Act
            await _productServiceArrange.AddAsync(validProduct);

            // Assert
            _productRepositoryMock.Verify(repo => repo.AddAsync(It.IsAny<Product>()), Times.Once);
        }

        [Test]
        public void TestAdd_InvalidEntity_ValidationExceptionThrown()
        {
            // Arrange
            var invalidProduct = new Product
            {
                ProductId = 1,
                ProductName = string.Empty
            };

            // Act & Assert
            Assert.ThrowsAsync<ValidationException>(async () => await _productServiceArrange.AddAsync(invalidProduct));
            _productRepositoryMock.Verify(repo => repo.AddAsync(It.IsAny<Product>()), Times.Never);
        }
    }

    public class ProductServiceArrange
    {
        private readonly IProductRepository productRepository;

        public ProductServiceArrange(IProductRepository productRepository)
        {
            this.productRepository = productRepository;
        }

        public async Task<List<Product>> Entities()
        {
            return await productRepository.Entities();
        }

        public async Task<int> AddAsync(Product product)
        {
            var validator = new ProductValidator();

            var validationResult = await validator.ValidateAsync(product);

            if (!validationResult.IsValid)
            {
                throw new ValidationException(validationResult.Errors);
            }

            return await productRepository.AddAsync(product);
        }
    }
}