﻿namespace Northwind.Domain.ViewModels
{
    using System.ComponentModel.DataAnnotations;

    public class ProductViewModel
    {
        [Display(Name = "#")]
        public int ProductId { get; set; }

        [Display(Name = "Nome")]
        public string ProductName { get; set; }

        [Display(Name = "Preço unitário")]
        public decimal? UnitPrice { get; set; }
    }
}
