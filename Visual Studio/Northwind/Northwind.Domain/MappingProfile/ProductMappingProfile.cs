﻿namespace Northwind.Domain.MappingProfile
{
    using AutoMapper;
    using Northwind.Domain.DTOs;
    using Northwind.Domain.Models;
    using Northwind.Domain.ViewModels;

    public class ProductMappingProfile : Profile
    {
        public ProductMappingProfile(bool isApplication)
        {
            if (isApplication)
            {
                DomainToViewModel();
                ViewModelToDomain();
            }
            else
            {
                DomainToDTO();
                DTOToDomain();
            }
        }

        private void DomainToDTO() =>
            CreateMap<Product, ProductDTO>();

        private void DTOToDomain() =>
            CreateMap<ProductDTO, Product>();

        private void DomainToViewModel() =>
            CreateMap<Product, ProductViewModel>();

        private void ViewModelToDomain() =>
            CreateMap<ProductViewModel, Product>();
    }
}
