﻿namespace Northwind.Domain.EntityValidators
{
    using FluentValidation;
    using Northwind.Domain.Models;

    public class ProductValidator : AbstractValidator<Product>
    {
        public ProductValidator()
        {
            RuleFor(product => product.ProductId).NotEmpty().WithMessage("ProductId is required.");
            RuleFor(product => product.ProductName).NotEmpty().WithMessage("ProductName is required.");
            //RuleFor(product => product.UnitPrice).InclusiveBetween(2.5, 263.5).WithMessage("ProductName is required.");
        }
    }
}
