namespace Northwind.API.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.EntityFrameworkCore;
    using Northwind.DependencyInjection.Interfaces.Repositories;
    using Northwind.Domain.DTOs;
    using Northwind.Domain.Models;

    [ApiController]
    [Route("api/[controller]")]
    public class ProductsController : ControllerBase
    {
        private readonly IMapper mapper;
        private readonly IProductRepository productRepository;

        public ProductsController(IMapper mapper, IProductRepository productRepository)
        {
            this.mapper = mapper;
            this.productRepository = productRepository;
        }

        // GET: api/Products
        [HttpGet]
        public async Task<ActionResult<Tuple<int, IEnumerable<ProductDTO>>>> GetProducts(int page = 1)
        {
            var products = productRepository.Entities(page);

            return Ok(Tuple.Create(products.PageCount,
                   await products.Results.Select(select =>
                        mapper.Map<Product, ProductDTO>(select)).ToListAsync()));
        }

        // GET: api/Products/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ProductDTO>> GetProduct(int id)
        {
            var product = await productRepository.FindAsync(id);

            if (product == null)
            {
                return NotFound();
            }

            return Ok(mapper.Map<Product, ProductDTO>(product));
        }

        // PUT: api/Products/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProduct(int id, ProductDTO productDTO)
        {
            if (id != productDTO.ProductId)
            {
                return BadRequest();
            }

            try
            {
                await productRepository.Update(mapper.Map<ProductDTO, Product>(productDTO));
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!productRepository.Any(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Products
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<ProductDTO>> PostProduct(ProductDTO product)
        {
            var isSaved = await productRepository.AddAsync(mapper.Map<ProductDTO, Product>(product));

            if (isSaved > 0)
            {
                return CreatedAtAction("GetProduct", new { id = product.ProductId }, product);
            }

            return BadRequest();
        }

        // DELETE: api/Products/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProduct(int id)
        {
            var product = await productRepository.FindAsync(id);

            if (product == null)
            {
                return NotFound();
            }

            var isDeleted = await productRepository.Remove(product);

            if (isDeleted > 0)
            {
                return NoContent();
            }

            return BadRequest();
        }
    }
}
