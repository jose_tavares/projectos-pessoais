﻿namespace CSharp_Solid1;

public abstract class Transportador : IDisposable
{
    public static Transportador GetTransportador(Pedido pedido)
    {
        var instance = GetInstanciaAdequada(typeof(Transportador).EncontraSubClasses(), pedido);
        return instance;
    }

    private static Transportador GetInstanciaAdequada(IEnumerable<Type> types, Pedido pedido)
    {
        foreach (var @class in types)
        {
            try
            {
                var instance = Activator.CreateInstance(@class) as Transportador;
                var isSuitable = instance.IsAdequadoParaPedido(pedido);

                if (isSuitable != true)
                {
                    instance.Dispose();
                    continue;
                }
                return instance;
            }
            catch 
            {
                continue;
            }
        }

        throw new NotImplementedException("Não foi possível encontrar um tipo " +
                                           "de transporte para o pedido: " + pedido);
    }

    public abstract void Transporte(Pedido pedido);
    protected abstract bool IsAdequadoParaPedido(Pedido pedido);

    public void Dispose()
    {
        GC.SuppressFinalize(this);
    }
}
