﻿using CSharp_Solid1;

List<Pedido> pedidos = new() { new Pedido { Urgente = false, Distancia = 100, Peso = 50 },
                               new Pedido { Urgente = false, Distancia = 2000, Peso = 4000 },
                               new Pedido { Urgente = false, Distancia = 1100, Peso = 5 },
                               new Pedido { Urgente = true,  Distancia = 1200, Peso = 250 }
};

foreach (var pedido in pedidos)
{
    Console.WriteLine("----------------------");
    Console.WriteLine(pedido.ToString());
    Transportador.GetTransportador(pedido).Transporte(pedido);
    Thread.Sleep(200);
}
Console.ReadKey();