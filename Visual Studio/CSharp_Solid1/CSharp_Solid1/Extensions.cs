﻿namespace CSharp_Solid1;

public static class Extensions
{
    public static IEnumerable<Type> EncontraSubClasses(this Type baseType)
    {
        var assembly = baseType.Assembly;

        return assembly.GetTypes().Where(t => t.IsSubclassOf(baseType));
    }
}
