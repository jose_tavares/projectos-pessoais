﻿namespace CSharp_Solid1;

public class TransporteAereo : Transportador
{
    public override void Transporte(Pedido pedido)
    {
        Console.WriteLine($"Pedido enviado via transporte aéreo.");
    }

    protected override bool IsAdequadoParaPedido(Pedido pedido)
    {
        return pedido.Urgente || (pedido.Distancia >= 1000 && pedido.Peso <= 100);
    }
}
