﻿namespace CSharp_Solid1;

public class TransporteRodoviario : Transportador
{
    public override void Transporte(Pedido pedido)
    {
        Console.WriteLine($"Pedido enviado via transporte rodoviário.");
    }

    protected override bool IsAdequadoParaPedido(Pedido pedido)
    {
        return !pedido.Urgente && pedido.Distancia < 1000;
    }
}
