﻿namespace CSharp_Solid1;

public class Pedido
{
    public int Distancia { get; set; }
    public int Peso { get; set; }
    public bool Urgente { get; set; }

    public override string ToString()
    {
        return $"Distância: {Distancia}km, Peso: {Peso}kg, Urgente: {(Urgente ? "S" : "N")}";
    }
}
