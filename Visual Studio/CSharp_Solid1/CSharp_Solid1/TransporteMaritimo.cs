﻿namespace CSharp_Solid1;

public class TransporteMaritimo : Transportador
{
    public override void Transporte(Pedido pedido)
    {
        Console.WriteLine($"Pedido enviado via transporte marítimo.");
    }

    protected override bool IsAdequadoParaPedido(Pedido pedido)
    {
        return !pedido.Urgente && (pedido.Distancia >= 1000 && pedido.Peso > 100);
    }
}
