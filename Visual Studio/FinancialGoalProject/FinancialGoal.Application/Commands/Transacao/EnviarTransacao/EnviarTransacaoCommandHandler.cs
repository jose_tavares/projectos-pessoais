﻿using FinancialGoal.Core.Enums;
using FinancialGoal.Infrastructure.Persistence;
using MediatR;

namespace FinancialGoal.Application.Commands.Transacao.EnviarTransacao
{
    public class EnviarTransacaoCommandHandler : IRequestHandler<EnviarTransacaoCommand, bool>
    {
        private readonly IUnitOfWork _unitOfWork;

        public EnviarTransacaoCommandHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<bool> Handle(EnviarTransacaoCommand request, CancellationToken cancellationToken)
        {
            var objetivoFinanceiro = await _unitOfWork.ObjetivoFinanceiroRepository.BuscarPorId(request.IdObjetivo);

            if (objetivoFinanceiro == null)
                return false;

            if (request.Tipo == TipoTransacao.Saque)
                request.Quantidade *= -1;

            Core.Entities.Transacao transacao = new(request.Quantidade, request.Tipo, request.DataTransacao, request.IdObjetivo);

            await _unitOfWork.BeginTransactionAsync();
            await _unitOfWork.TransacaoRepository.AddAsync(transacao, objetivoFinanceiro);
            await _unitOfWork.CompleteAsync();
            await _unitOfWork.CommitAsync();

            return true;
        }
    }
}
