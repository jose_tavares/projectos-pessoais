﻿using MediatR;

namespace FinancialGoal.Application.Commands.ObjetivosFinanceiros.CriarObjetivo
{
    public class AddObjetivoCommand : IRequest<int>
    {
        public string Titulo { get; set; }

        public decimal? QuantidadeAlvo { get; set; }

        public DateTime? Prazo { get; set; }
    }
}
