﻿using FinancialGoal.Application.ViewModels;
using MediatR;

namespace FinancialGoal.Application.Queries.ObjetivoFinanceiro.BuscarTodos
{
    public class BuscarTodosQuery : IRequest<List<ObjetivoFinanceiroViewModel>> { }
}
