﻿using FinancialGoal.Core.Repositories;

namespace FinancialGoal.Infrastructure.Persistence
{
    public interface IUnitOfWork : IDisposable
    {
        IObjetivoFinanceiroRepository ObjetivoFinanceiroRepository { get; }

        IRelatorioRepository RelatorioRepository { get; }

        ITransacaoRepository TransacaoRepository { get; }

        Task BeginTransactionAsync();

        Task CommitAsync();

        Task<int> CompleteAsync();
    }
}
