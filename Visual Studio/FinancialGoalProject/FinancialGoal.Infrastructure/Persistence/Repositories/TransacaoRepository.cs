﻿using FinancialGoal.Core.Entities;
using FinancialGoal.Core.Repositories;
using Microsoft.EntityFrameworkCore;

namespace FinancialGoal.Infrastructure.Persistence.Repositories
{
    public class TransacaoRepository : ITransacaoRepository
    {
        private readonly ObjetivoFinanceiroDbContext _dbcontext;

        public TransacaoRepository(ObjetivoFinanceiroDbContext dbcontext)
            => _dbcontext = dbcontext;

        public async Task<Transacao> BuscarPorId(int id)
            => await _dbcontext.Transacoes.SingleOrDefaultAsync(t => t.Id == id) ??
                throw new Exception($"Transição com id {id} inexistente!");

        public async Task<List<Transacao>> BuscarTodos()
            => await _dbcontext.Transacoes.ToListAsync();

        public async Task AddAsync(Transacao transacao, ObjetivoFinanceiro objetivoFinanceiro)
        {
            if (objetivoFinanceiro != null)
            {
                transacao.ObjetivoFinanceiro = objetivoFinanceiro;

                objetivoFinanceiro.Transacoes ??= [];
                objetivoFinanceiro.Transacoes.Add(transacao);

                _dbcontext.Update(objetivoFinanceiro);
            }

            await _dbcontext.Transacoes.AddAsync(transacao);
        }

        public async Task Delete(int id)
        {
            Transacao transacoes = await _dbcontext.Transacoes.SingleOrDefaultAsync(o => o.Id == id) ??
                throw new Exception($"Transição com id {id} inexistente!");

            RemoverAsync(transacoes);
        }

        public void RemoverAsync(Transacao transacao)
            => _dbcontext.Remove(transacao);

        public async Task SaveChangesAsync()
            => await _dbcontext.SaveChangesAsync();
    }
}
