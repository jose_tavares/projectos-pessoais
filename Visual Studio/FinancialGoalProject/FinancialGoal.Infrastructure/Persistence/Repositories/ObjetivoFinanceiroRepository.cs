﻿using FinancialGoal.Core.Entities;
using FinancialGoal.Core.Repositories;
using Microsoft.EntityFrameworkCore;

namespace FinancialGoal.Infrastructure.Persistence.Repositories
{
    public class ObjetivoFinanceiroRepository : IObjetivoFinanceiroRepository
    {
        private readonly ObjetivoFinanceiroDbContext _dbcontext;

        public ObjetivoFinanceiroRepository(ObjetivoFinanceiroDbContext dbcontext)
            => _dbcontext = dbcontext;

        public async Task<ObjetivoFinanceiro> BuscarPorId(int id)
            => await _dbcontext.ObjetivoFinanceiro.SingleOrDefaultAsync(o => o.Id == id) ??
                throw new Exception($"O Objetivo com id {id} não existe");

        public async Task<List<ObjetivoFinanceiro>> BuscarTodos()
            => await _dbcontext.ObjetivoFinanceiro.ToListAsync();

        public async Task AddAsync(ObjetivoFinanceiro objetivoFinanceiro)
            => await _dbcontext.ObjetivoFinanceiro.AddAsync(objetivoFinanceiro);

        public async Task Delete(int id)
        {
            ObjetivoFinanceiro? objetivo = await _dbcontext.ObjetivoFinanceiro.SingleOrDefaultAsync(o => o.Id == id) ??
                throw new Exception($"O Objetivo com {id} não existe");

            RemoverAsync(objetivo);
        }

        public void RemoverAsync(ObjetivoFinanceiro objetivoFinanceiro)
            => _dbcontext.Remove(objetivoFinanceiro);

        public async Task SaveChangesAsync()
            => await _dbcontext.SaveChangesAsync();
    }
}
