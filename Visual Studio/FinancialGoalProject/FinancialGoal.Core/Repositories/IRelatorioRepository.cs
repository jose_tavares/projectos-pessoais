﻿using FinancialGoal.Core.DTO_s;

namespace FinancialGoal.Core.Repositories
{
    public interface IRelatorioRepository
    {
        Task<List<RelatorioDto>> BuscarRelatorio();
    }
}
