﻿using FinancialGoal.Core.Entities;

namespace FinancialGoal.Core.Repositories
{
    public interface ITransacaoRepository
    {
        Task<List<Transacao>> BuscarTodos();

        Task<Transacao> BuscarPorId(int id);

        Task AddAsync(Transacao transacao, ObjetivoFinanceiro objetivoFinanceiro);

        Task Delete(int id);

        Task SaveChangesAsync();
    }
}
