﻿using FinancialGoal.Core.Entities;

namespace FinancialGoal.Core.Repositories
{
    public interface IObjetivoFinanceiroRepository
    {
        Task<List<ObjetivoFinanceiro>> BuscarTodos();

        Task<ObjetivoFinanceiro> BuscarPorId(int id);

        Task AddAsync(ObjetivoFinanceiro objetivoFinanceiro);

        Task Delete(int id);

        Task SaveChangesAsync();
    }
}
