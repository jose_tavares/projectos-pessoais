﻿using BlazorTarefas.Data;
using Microsoft.EntityFrameworkCore;

namespace BlazorTarefas.Services
{
    public class TarefaService(ApplicationDbContext context) : ITarefaService
    {
        private readonly ApplicationDbContext _context = context;

        public async Task<Tarefa> Add(Tarefa tarefa)
        {
            _context.Tarefas.Add(tarefa);
            await _context.SaveChangesAsync();
            return tarefa;
        }

        public async Task<Tarefa> Delete(int id)
        {
            var tarefa = await _context.Tarefas.FindAsync(id);
            _context.Tarefas.Remove(tarefa);
            await _context.SaveChangesAsync();
            return tarefa;
        }

        public async Task<List<Tarefa>> Get()
        {
            return await _context.Tarefas.ToListAsync();
        }

        public async Task<Tarefa> Get(int id)
        {
            var tarefa = await _context.Tarefas.FindAsync(id);
            return tarefa;
        }

        public async Task<Tarefa> Update(Tarefa tarefa)
        {
            _context.Entry(tarefa).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return tarefa;
        }
    }
}
