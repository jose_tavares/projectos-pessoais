﻿using BlazorTarefas.Data;

namespace BlazorTarefas.Services
{
    public interface ITarefaService
    {
        Task<List<Tarefa>> Get();

        Task<Tarefa> Get(int id);
        
        Task<Tarefa> Add(Tarefa tarefa);
        
        Task<Tarefa> Update(Tarefa tarefa);
        
        Task<Tarefa> Delete(int id);
    }
}
