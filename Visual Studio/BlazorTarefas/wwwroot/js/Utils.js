﻿function ShowModal(modalId) {
    document.getElementById(modalId).style.display = 'block';
}

function CloseModal(modalId) {
    document.getElementById(modalId).style.display = 'none';
}