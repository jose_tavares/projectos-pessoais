﻿using Microsoft.EntityFrameworkCore;

namespace BlazorTarefas.Data
{
    public class ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : DbContext(options)
    {
        public DbSet<Tarefa>? Tarefas { get; set; }

        public override int SaveChanges()
        {
            return base.SaveChanges();
        }
    }
}
