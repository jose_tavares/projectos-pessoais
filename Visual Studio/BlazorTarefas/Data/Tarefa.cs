﻿using System.ComponentModel.DataAnnotations;

namespace BlazorTarefas.Data
{
    public class Tarefa
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Informe o nome")]
        [StringLength(80, ErrorMessage = "Nome muito longo.")]
        public string? Nome { get; set; }

        [Required(ErrorMessage = "Informe o status")]
        public string? Status { get; set; }

        [Required(ErrorMessage = "Informe a data")]
        public DateTime ConclusaoEm { get; set; }
    }
}
