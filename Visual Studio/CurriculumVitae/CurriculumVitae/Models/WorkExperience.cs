﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CurriculumVitae.Models
{
    public class WorkExperience
    {
        [Display(Name = "Nome da Empresa")]
        public string CompanyName { get; set; }

        [Display(Name = "Função")]
        public string Function { get; set; }

        [Display(Name = "Início")]
        public DateTime Begin { get; set; }

        [Display(Name = "Fim")]
        public DateTime End { get; set; }
    }
}