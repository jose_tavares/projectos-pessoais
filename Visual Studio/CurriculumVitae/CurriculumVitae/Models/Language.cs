﻿using System.ComponentModel.DataAnnotations;

namespace CurriculumVitae.Models
{
    public class Language
    {
        [Display(Name = "Nome")]
        public string Name { get; set; }

        [Display(Name = "Classificação")]
        public string Classification { get; set; }
    }
}