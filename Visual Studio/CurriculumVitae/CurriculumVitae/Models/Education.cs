﻿using System.ComponentModel.DataAnnotations;

namespace CurriculumVitae.Models
{
    public class Education
    {
        [Display(Name = "Nome da Instituiçao de Ensino")]
        public string EducationalInstitution { get; set; }

        [Display(Name = "Nome do Curso")]
        public string Course { get; set; }

        [Display(Name = "Ano de Conclusão")]
        public int ConclusionYear { get; set; }

        [Display(Name = "Média de Curso")]
        public int AverageCourse { get; set; }
    }
}