﻿using System.ComponentModel.DataAnnotations;

namespace CurriculumVitae.Models
{
    public class Skill
    {
        [Display(Name = "Nome")]
        public string Name { get; set; }

        public string PictureUrl { get; set; }
    }
}