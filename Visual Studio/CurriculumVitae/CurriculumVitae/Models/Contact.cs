﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CurriculumVitae.Models
{
    public class Contact
    {
        [Display(Name = "Nome")]
        public string Name { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Data de Nascimento")]
        public DateTime Birth { get; set; }

        [Display(Name = "Localidade")]
        public string Location { get; set; }

        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Display(Name = "Telemóvel")]
        [DataType(DataType.PhoneNumber)]
        public int PhoneNumber { get; set; }
    }
}