﻿using System;
using System.Linq;
using System.Net.Http;
using CurriculumVitae.Models;
using System.Collections.Generic;

namespace CurriculumVitae.Repository
{
    public class CRepository : IRepository
    {
        private readonly HttpClient _client = new HttpClient();
        private readonly string _uriString = "http://localhost:62232/";

        public string GetAbout()
        {
            using (_client)
            {
                _client.BaseAddress = new Uri(_uriString);

                var responseTask = _client.GetAsync("about");
                responseTask.Wait();

                var result = responseTask.Result;

                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<string>();
                    readTask.Wait();

                    return readTask.Result;
                }
            }

            return null;
        }

        public Contact GetContact()
        {
            using (_client)
            {
                _client.BaseAddress = new Uri(_uriString);

                //HTTP GET
                var responseTask = _client.GetAsync("contact");
                responseTask.Wait();

                var result = responseTask.Result;

                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<Contact>();
                    readTask.Wait();

                    return readTask.Result;
                }
            }

            return null;
        }

        public Education GetEducation()
        {
            using (_client)
            {
                _client.BaseAddress = new Uri(_uriString);

                //HTTP GET
                var responseTask = _client.GetAsync("education");
                responseTask.Wait();

                var result = responseTask.Result;

                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<Education>();
                    readTask.Wait();

                    return readTask.Result;
                }
            }

            return null;
        }

        public IList<string> GetHome()
        {
            using (_client)
            {
                _client.BaseAddress = new Uri(_uriString);

                var responseTask = _client.GetAsync("home");
                responseTask.Wait();

                var result = responseTask.Result;

                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<IList<string>>();
                    readTask.Wait();

                    var values = readTask.Result;
                    return values.ToList();
                }
            }

            return null;
        }

        public IList<Language> GetLanguages()
        {
            using (_client)
            {
                _client.BaseAddress = new Uri(_uriString);

                var responseTask = _client.GetAsync("languages");
                responseTask.Wait();

                var result = responseTask.Result;

                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<IList<Language>>();
                    readTask.Wait();

                    var values = readTask.Result;
                    return values;
                }
            }

            return null;
        }

        public IList<Skill> GetSkills()
        {
            using (_client)
            {
                _client.BaseAddress = new Uri(_uriString);

                //HTTP GET
                var responseTask = _client.GetAsync("skills");
                responseTask.Wait();

                var result = responseTask.Result;

                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<IList<Skill>>();
                    readTask.Wait();

                    return readTask.Result;
                }
            }

            return null;
        }

        public IList<WorkExperience> GetWorkExperiences()
        {
            using (_client)
            {
                _client.BaseAddress = new Uri(_uriString);

                //HTTP GET
                var responseTask = _client.GetAsync("work_experience");
                responseTask.Wait();

                var result = responseTask.Result;

                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<IList<WorkExperience>>();
                    readTask.Wait();

                    return readTask.Result;
                }
            }

            return null;
        }
    }
}