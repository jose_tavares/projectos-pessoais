﻿using CurriculumVitae.Models;
using System.Collections.Generic;

namespace CurriculumVitae.Repository
{
    public interface IRepository
    {
        string GetAbout();

        Contact GetContact();

        Education GetEducation();

        IList<string> GetHome();

        IList<Language> GetLanguages();

        IList<Skill> GetSkills();

        IList<WorkExperience> GetWorkExperiences();
    }
}
