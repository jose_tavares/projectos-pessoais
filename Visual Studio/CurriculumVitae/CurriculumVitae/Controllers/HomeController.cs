﻿using System;
using System.Net;
using System.Web.Mvc;
using System.Net.Http;
using CurriculumVitae.Models;
using CurriculumVitae.Helpers;
using CurriculumVitae.Repository;
using System.Collections.Generic;

namespace CurriculumVitae.Controllers
{
    public class HomeController : Controller
    {
        private readonly IRepository _repository;

        public HomeController(IRepository repository) => _repository = repository;

        public ActionResult Index()
        {
            var values = _repository.GetHome();

            if (values.Validate())
            {
                return View(values);
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Erro no servidor.");
        }

        public ActionResult About()
        {
            var title = "Sobre Mim";

            var text = _repository.GetAbout();

            if (!string.IsNullOrEmpty(text))
            {
                return View(Tuple.Create(title, text));
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Erro no servidor.");
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Contactos pessoais.";

            var contact = _repository.GetContact();

            if (contact != null)
            {
                return View(contact);
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Erro no servidor.");
        }

        public ActionResult Skills()
        {
            var skills = _repository.GetSkills();

            if (skills.Validate())
            {
                return View(skills);
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Erro no servidor.");
        }

        public ActionResult Education()
        {
            var education = _repository.GetEducation();

            if (education != null)
            {
                return View(education);
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Erro no servidor.");
        }

        public ActionResult WorkExperience()
        {
            var workExperiences = _repository.GetWorkExperiences();

            if (workExperiences.Validate())
            {
                return View(workExperiences);
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Erro no servidor.");
        }

        public ActionResult Languages()
        {
            var languages = _repository.GetLanguages();

            if (languages.Validate())
            {
                return View(languages);
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Erro no servidor.");
        }
    }
}