﻿using System.Linq;
using System.Collections.Generic;

namespace CurriculumVitae.Helpers
{
    public static class List
    {
        public static bool Validate<T>(this IEnumerable<T> list)
        {
            return ((list != null) && list.Any()) ? true : false;
        }
    }
}