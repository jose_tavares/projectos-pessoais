﻿using System;
using System.Web.Http;
using CurriculumVitaeAPI.Models;
using System.Collections.Generic;

namespace CurriculumVitaeAPI.Controllers
{
    public class ValuesController : ApiController
    {
        // GET api/values
        public IEnumerable<string> Get() => new string[] { "value1", "value2" };

        // GET api/values/5
        public string Get(int id) => "value";

        // POST api/values
        public void Post([FromBody]string value) { }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value) { }

        // DELETE api/values/5
        public void Delete(int id) { }

        [HttpGet]
        [Route("home")]
        public IEnumerable<string> GetHomeValues() =>
            new string[]
            {
                "Curriculum Vitae in ASP.NET Framework application",
                "Apresentação da minha trajetória educacional e as minhas experiências profissionais em formato de aplicação de forma mostrar as minhas habilidades técnicas.",
                "/Documents/Curriculum Vitae.pdf",
                "CV em PDF"
            };

        [HttpGet]
        [Route("contact")]
        public IHttpActionResult GetContact()
        {
            var contact = new Contact
            {
                Birth = new DateTime(1992, 3, 5),
                Email = "jpbtavares@gmail.com",
                Location = "Oliveira de Azeméis",
                Name = "José Pedro Bértola Tavares",
                PhoneNumber = 911030557
            };

            return Ok(contact);
        }

        [HttpGet]
        [Route("about")]
        public string GetAboutValue() =>
            "Sou um verdadeiro apaixonado por tecnologia e por desenvolvimento." + Environment.NewLine + Environment.NewLine + "Desde cedo comecei a ter interesse na área de informática, tendo primeiro contacto num computador com Sistema Operativo MS-DOS, que despertou curiosidade de como as máquinas funcionam e como é possível interagi-las.Mas foi no secundário que aprendi o conceito de algoritmia e introdução de programação, e por fim, foi na faculdade onde fortaleci conhecimentos técnicos de engenharia.Os contactos com diversas realidades fazem de mim hoje um jovem profissional mais preparado para dar resposta a diferentes desafios." + Environment.NewLine + Environment.NewLine + "O que não conheço aprendo e o que sei melhoro, com técnicas de investigação e trocas de informações de colegas de trabalho, integrando mais facilmente numa equipa em que aplica metodologias ágeis." + Environment.NewLine + Environment.NewLine + "Tenho particular interesse pelo desenvolvimento Back-End, mas estou sempre disponível para novos desafios, de forma a enriquecer a minha experiência profissional.";

        [HttpGet]
        [Route("skills")]
        public IHttpActionResult GetSkills()
        {
            var skills = new List<Skill>
            {
                new Skill { Name = "Angular", PictureUrl = "logo-angular.png" },
                new Skill { Name = "JQuery", PictureUrl = "logo-jquery.png"}
            };

            return Ok(skills);
        }

        [HttpGet]
        [Route("education")]
        public IHttpActionResult GetEducation()
        {
            var education = new Education
            {
                AverageCourse = 13,
                ConclusionYear = 2018,
                Course = "Licenciatura em Engenharia Informática",
                EducationalInstitution = "ISEP – INSTITUTO SUPERIOR DE ENGENHARIA DO PORTO"
            };

            return Ok(education);
        }

        [HttpGet]
        [Route("work_experience")]
        public IHttpActionResult GetWorkExperience()
        {
            var workExperiences = new List<WorkExperience>
            {
                new WorkExperience
                {
                    Begin = new DateTime(2019, 10, 1),
                    CompanyName = "POÇA CONSULTING, LDA.",
                    End = DateTime.Now,
                    Function = "Programador Web"
                },
                new WorkExperience
                {
                    Begin = new DateTime(2019, 02, 1),
                    CompanyName = "CAMBRAGEST - SERVIÇOS DE GESTÃO E SOFTWARE, LDA.",
                    End = new DateTime(2019, 10, 1),
                    Function = "Estágio Profissional"
                }
            };

            return Ok(workExperiences);
        }

        [HttpGet]
        [Route("languages")]
        public IHttpActionResult GetLanguages()
        {
            var languages = new List<Language>
            {
                new Language
                {
                    Name = "Inglês",
                    Rate = 5
                }
            };

            languages.ForEach(each =>
                each.SetClassification()
            );

            return Ok(languages);
        }
    }
}
