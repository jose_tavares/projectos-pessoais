﻿namespace CurriculumVitaeAPI.Models
{
    public class Skill
    {
        private string _pictureURL = "/Pictures/";

        public string Name { get; set; }

        public string PictureUrl
        {
            get => _pictureURL;

            set => _pictureURL += value;
        }
    }
}