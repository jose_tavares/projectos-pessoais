﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CurriculumVitaeAPI.Models
{
    public class WorkExperience
    {
        public string CompanyName { get; set; }

        public string Function { get; set; }

        public DateTime Begin { get; set; }

        public DateTime End { get; set; }
    }
}