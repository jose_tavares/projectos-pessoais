﻿namespace CurriculumVitaeAPI.Models
{
    public class Education
    {
        public string EducationalInstitution { get; set; }

        public string Course { get; set; }

        public int ConclusionYear { get; set; }

        public int AverageCourse { get; set; }
    }
}