﻿using System;

namespace CurriculumVitaeAPI.Models
{
    public class Contact
    {
        public string Name { get; set; }

        public DateTime Birth { get; set; }

        public string Location { get; set; }

        public string Email { get; set; }

        public int PhoneNumber { get; set; }
    }
}