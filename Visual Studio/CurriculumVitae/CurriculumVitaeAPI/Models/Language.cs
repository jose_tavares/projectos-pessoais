﻿namespace CurriculumVitaeAPI.Models
{
    public class Language
    {
        public string Name { get; set; }

        public int Rate { private get; set; }

        public string Classification { get; private set; }

        public void SetClassification()
        {
            Classification = new string('★', Rate) + new string('☆', 10 - Rate);
        }
    }
}