﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contatos.Domain.Interfaces
{
    public interface IRepository<TEntity> where TEntity : class
    {
        IEnumerable<TEntity> GetAll();

        TEntity GetById(int id);

        void Save(TEntity entity);
    }
}
