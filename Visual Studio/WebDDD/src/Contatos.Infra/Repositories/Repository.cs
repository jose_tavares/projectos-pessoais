﻿using System.Linq;
using Contatos.Infra.Context;
using System.Collections.Generic;
using Contatos.Domain.Interfaces;

namespace Contatos.Infra.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity
    {
        protected readonly AppDbContext _context;

        public Repository(AppDbContext context) =>
            _context = context;

        public virtual IEnumerable<TEntity> GetAll()
        {
            var query = _context.Set<TEntity>();

            return query.AsEnumerable();
        }

        public virtual TEntity GetById(int id)
        {
            var query = _context.Set<TEntity>().Where(where => where.Id == id);

            return query.FirstOrDefault();
        }

        public virtual void Save(TEntity entity) =>
            _context.Set<TEntity>().Add(entity);
    }
}
