﻿using System.Linq;
using Contatos.Domain.Models;
using Contatos.Infra.Context;
using System.Collections.Generic;

namespace Contatos.Infra.Repositories
{
    public class ContatoRepository : Repository<Contato>
    {
        public ContatoRepository(AppDbContext context) : base(context) { }

        public override IEnumerable<Contato> GetAll()
        {
            var query = _context.Set<Contato>();

            return query.AsEnumerable();
        }

        public override Contato GetById(int id)
        {
            var query = _context.Set<Contato>().Where(where => where.Id == id);

            return query.FirstOrDefault();
        }
    }
}
