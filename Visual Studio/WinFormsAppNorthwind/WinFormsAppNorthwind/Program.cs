namespace WinFormsAppNorthwind
{
    using Microsoft.Extensions.DependencyInjection;
    using Northwind.Application.Controllers;
    using Northwind.Application.Forms;
    using Northwind.DependencyInjection.Implementations;
    using Northwind.DependencyInjection.Interfaces;
    using System;
    using System.Windows.Forms;

    static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            ServiceCollection services = new ServiceCollection();

            services.AddTransient<IUnitOfWorkRepository, UnitOfWorkRepository>()
                .AddTransient<IProductRepository, ProductRepository>()
                .AddTransient<IProductController, ProductController>()
                .AddSingleton<FormMain>();

            using ServiceProvider serviceProvider = services.BuildServiceProvider();
            Application.Run(serviceProvider.GetRequiredService<FormMain>());
        }
    }
}
