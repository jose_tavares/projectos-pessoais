﻿namespace Northwind.Application.Controllers
{
    using Northwind.Common.Pagination;
    using Northwind.DependencyInjection.Interfaces;
    using Northwind.Domain.Models;

    public class ProductController : IProductController
    {
        private readonly IProductRepository productRepository;

        public ProductController(IProductRepository productRepository) =>
            this.productRepository = productRepository;

        public PagedResult<Product> ProductPagedResult(int currentPage, int pageSize, string productNameFilter) =>
            productRepository.Entities(currentPage, pageSize, predicate => predicate.ProductName.Contains(productNameFilter) || string.IsNullOrWhiteSpace(productNameFilter));
    }
}
