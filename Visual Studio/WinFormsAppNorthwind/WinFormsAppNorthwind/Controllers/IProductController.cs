﻿using Northwind.Common.Pagination;
using Northwind.Domain.Models;

namespace Northwind.Application.Controllers
{
    public interface IProductController
    {
        PagedResult<Product> ProductPagedResult(int currentPage, int pageSize, string productNameFilter);
    }
}