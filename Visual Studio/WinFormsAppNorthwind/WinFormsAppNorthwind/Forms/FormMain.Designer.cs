﻿
namespace Northwind.Application.Forms
{
    partial class FormMain
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listViewProducts = new System.Windows.Forms.ListView();
            this.Button_PreviousPage = new System.Windows.Forms.Button();
            this.Button_NextPage = new System.Windows.Forms.Button();
            this.TextBoxFilterProduct = new System.Windows.Forms.TextBox();
            this.LabelFilterProduct = new System.Windows.Forms.Label();
            this.ButtonClearFilterProduct = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // listViewProducts
            // 
            this.listViewProducts.HideSelection = false;
            this.listViewProducts.Location = new System.Drawing.Point(12, 56);
            this.listViewProducts.Name = "listViewProducts";
            this.listViewProducts.Size = new System.Drawing.Size(776, 353);
            this.listViewProducts.TabIndex = 0;
            this.listViewProducts.UseCompatibleStateImageBehavior = false;
            this.listViewProducts.View = System.Windows.Forms.View.Details;
            // 
            // Button_PreviousPage
            // 
            this.Button_PreviousPage.Location = new System.Drawing.Point(12, 415);
            this.Button_PreviousPage.Name = "Button_PreviousPage";
            this.Button_PreviousPage.Size = new System.Drawing.Size(75, 23);
            this.Button_PreviousPage.TabIndex = 1;
            this.Button_PreviousPage.Text = "Anterior";
            this.Button_PreviousPage.UseVisualStyleBackColor = true;
            this.Button_PreviousPage.Click += new System.EventHandler(this.Button_PreviousPage_Click);
            // 
            // Button_NextPage
            // 
            this.Button_NextPage.Location = new System.Drawing.Point(713, 415);
            this.Button_NextPage.Name = "Button_NextPage";
            this.Button_NextPage.Size = new System.Drawing.Size(75, 23);
            this.Button_NextPage.TabIndex = 2;
            this.Button_NextPage.Text = "Seguinte";
            this.Button_NextPage.UseVisualStyleBackColor = true;
            this.Button_NextPage.Click += new System.EventHandler(this.Button_NextPage_Click);
            // 
            // TextBoxFilterProduct
            // 
            this.TextBoxFilterProduct.Location = new System.Drawing.Point(68, 12);
            this.TextBoxFilterProduct.Name = "TextBoxFilterProduct";
            this.TextBoxFilterProduct.Size = new System.Drawing.Size(100, 23);
            this.TextBoxFilterProduct.TabIndex = 3;
            this.TextBoxFilterProduct.KeyUp += new System.Windows.Forms.KeyEventHandler(this.TextBoxFilterProduct_KeyUp);
            // 
            // LabelFilterProduct
            // 
            this.LabelFilterProduct.AutoSize = true;
            this.LabelFilterProduct.Location = new System.Drawing.Point(12, 15);
            this.LabelFilterProduct.Name = "LabelFilterProduct";
            this.LabelFilterProduct.Size = new System.Drawing.Size(50, 15);
            this.LabelFilterProduct.TabIndex = 4;
            this.LabelFilterProduct.Text = "Produto";
            // 
            // ButtonClearFilterProduct
            // 
            this.ButtonClearFilterProduct.Location = new System.Drawing.Point(174, 11);
            this.ButtonClearFilterProduct.Name = "ButtonClearFilterProduct";
            this.ButtonClearFilterProduct.Size = new System.Drawing.Size(75, 23);
            this.ButtonClearFilterProduct.TabIndex = 5;
            this.ButtonClearFilterProduct.Text = "Limpar";
            this.ButtonClearFilterProduct.UseVisualStyleBackColor = true;
            this.ButtonClearFilterProduct.Click += new System.EventHandler(this.ButtonClearFilterProduct_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.ButtonClearFilterProduct);
            this.Controls.Add(this.LabelFilterProduct);
            this.Controls.Add(this.TextBoxFilterProduct);
            this.Controls.Add(this.Button_NextPage);
            this.Controls.Add(this.Button_PreviousPage);
            this.Controls.Add(this.listViewProducts);
            this.Name = "FormMain";
            this.Text = "Northwind";
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView listViewProducts;
        private System.Windows.Forms.Button Button_PreviousPage;
        private System.Windows.Forms.Button Button_NextPage;
        private System.Windows.Forms.TextBox TextBoxFilterProduct;
        private System.Windows.Forms.Label LabelFilterProduct;
        private System.Windows.Forms.Button ButtonClearFilterProduct;
    }
}

