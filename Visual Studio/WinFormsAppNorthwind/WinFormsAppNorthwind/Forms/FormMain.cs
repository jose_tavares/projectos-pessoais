﻿namespace Northwind.Application.Forms
{
    using Northwind.Application.Controllers;
    using Northwind.Common.Pagination;
    using Northwind.Domain.Models;
    using System;
    using System.Windows.Forms;

    public partial class FormMain : Form
    {
        private int pageCount;
        private int currentPage = 1;
        private readonly IProductController productController;

        public FormMain(IProductController productController)
        {
            InitializeComponent();
            this.productController = productController;
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            listViewProducts.Columns.Add(new ColumnHeader { Text = "Id" });
            listViewProducts.Columns.Add(new ColumnHeader { Text = "Nome" });
            listViewProducts.Columns.Add(new ColumnHeader { Text = "Preço unitário" });

            LoadListViewProducts();
        }

        private void Button_NextPage_Click(object sender, EventArgs e)
        {
            if (currentPage < pageCount)
            {
                currentPage++;
                LoadListViewProducts();
            }
        }

        private void Button_PreviousPage_Click(object sender, EventArgs e)
        {
            if (currentPage > 1)
            {
                currentPage--;
                LoadListViewProducts();
            }
        }

        private void TextBoxFilterProduct_KeyUp(object sender, KeyEventArgs e) =>
            LoadListViewProducts(true);

        private void ButtonClearFilterProduct_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(TextBoxFilterProduct.Text))
            {
                TextBoxFilterProduct.Text = string.Empty;
                LoadListViewProducts(true);
            }
        }

        private void LoadListViewProducts(bool isFiltered = false)
        {
            if (listViewProducts.Items.Count > 0)
            {
                listViewProducts.Items.Clear();
            }

            if (isFiltered)
            {
                currentPage = 1;
            }

            PagedResult<Product> loadProductPagedResult = productController.ProductPagedResult(currentPage, 17, TextBoxFilterProduct.Text.Trim());
            pageCount = loadProductPagedResult.PageCount;

            foreach (Product item in loadProductPagedResult.Results)
            {
                string[] row = { item.ProductId.ToString(), item.ProductName, item.UnitPrice.ToString() };

                listViewProducts.Items.Add(new ListViewItem(row));
            }

            foreach (ColumnHeader item in listViewProducts.Columns)
            {
                item.Width = -2;
            }
        }
    }
}
