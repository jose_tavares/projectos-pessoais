﻿namespace Northwind.DependencyInjection.Implementations
{
    using Northwind.Common.Pagination;
    using Northwind.DependencyInjection.Interfaces;
    using Northwind.Domain.Models;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;

    public class ProductRepository : IProductRepository
    {
        private readonly IUnitOfWorkRepository unitOfWorkRepository;

        public ProductRepository(IUnitOfWorkRepository unitOfWorkRepository) =>
            this.unitOfWorkRepository = unitOfWorkRepository;

        public int Count =>
            unitOfWorkRepository.ProductRepository.Count;

        public Task<int> Add(Product product)
        {
            unitOfWorkRepository.ProductRepository.Add(product);
            return unitOfWorkRepository.SaveChangesAsync();
        }

        public bool Any(int id) =>
            unitOfWorkRepository.ProductRepository.Any(any => any.ProductId == id);

        public ValueTask<Product> FindAsync(int id) =>
            unitOfWorkRepository.ProductRepository.FindAsync(id);

        public Task<List<Product>> Entities() =>
            unitOfWorkRepository.ProductRepository.Entities();

        public Task<int> Remove(Product product)
        {
            unitOfWorkRepository.ProductRepository.Remove(product);
            return unitOfWorkRepository.SaveChangesAsync();
        }

        public Task<int> Update(Product product)
        {
            unitOfWorkRepository.ProductRepository.Update(product);
            return unitOfWorkRepository.SaveChangesAsync();
        }

        public PagedResult<Product> Entities(int page, int pageSize, Expression<Func<Product, bool>> predicate) =>
            unitOfWorkRepository.ProductRepository.Entities(page, pageSize, predicate);

        public IQueryable<Product> Where(Expression<Func<Product, bool>> predicate) =>
            unitOfWorkRepository.ProductRepository.Where(predicate);
    }
}
