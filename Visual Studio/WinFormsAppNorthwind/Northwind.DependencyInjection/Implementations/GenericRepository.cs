﻿namespace Northwind.DependencyInjection.Implementations
{
    using Microsoft.EntityFrameworkCore;
    using Northwind.Common.Extensions;
    using Northwind.Common.Pagination;
    using Northwind.DependencyInjection.Interfaces;
    using Northwind.Domain.Models;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;

    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        private readonly DbSet<TEntity> entities;
        private readonly NorthwindContext context;

        public GenericRepository(NorthwindContext context)
        {
            this.context = context;
            entities = context.Set<TEntity>();
        }

        public int Count =>
            entities.Count();

        public void Add(TEntity entity) =>
            entities.Add(entity);

        public bool Any(Expression<Func<TEntity, bool>> predicate) =>
            entities.Any(predicate);

        public Task<List<TEntity>> Entities() =>
            entities.ToListAsync();

        public PagedResult<TEntity> Entities(int page, int pageSize, Expression<Func<TEntity, bool>> predicate = null) =>
            entities.GetPaged(page, pageSize, predicate);

        public ValueTask<TEntity> FindAsync(params object[] keyValues) =>
            entities.FindAsync(keyValues);

        public void Remove(TEntity entity) =>
            entities.Remove(entity);

        public void RemoveRange(IEnumerable<TEntity> entities) =>
            this.entities.RemoveRange(entities);

        public void Update(TEntity entity)
        {
            context.Entry(entity).State = EntityState.Modified;
            entities.Update(entity);
        }

        public IQueryable<TEntity> Where(Expression<Func<TEntity, bool>> predicate) =>
            entities.Where(predicate);

        public Task<TEntity> SingleOrDefaultAsync(Expression<Func<TEntity, bool>> predicate) =>
            entities.SingleOrDefaultAsync(predicate);
    }
}
