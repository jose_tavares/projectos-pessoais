﻿namespace Northwind.DependencyInjection.Implementations
{
    using Microsoft.EntityFrameworkCore;
    using Northwind.Common.Extensions;
    using Northwind.DependencyInjection.Interfaces;
    using Northwind.Domain.Models;
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Threading.Tasks;

    public class UnitOfWorkRepository : IUnitOfWorkRepository
    {
        private readonly NorthwindContext context = new NorthwindContext();
        private bool disposed = false;

        public UnitOfWorkRepository() =>
            ProductRepository = new GenericRepository<Product>(context);

        public IGenericRepository<Product> ProductRepository { get; }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public Task<int> SaveChangesAsync()
        {
            var entites = context.ChangeTracker.Entries()
                          .Where(where => (where.State == EntityState.Added)
                              || (where.State == EntityState.Deleted)
                              || (where.State == EntityState.Modified))
                          .Select(select => select.Entity);

            if (entites.Validate())
            {
                foreach (var entity in entites)
                {
                    var validationContext = new ValidationContext(entity);

                    Validator.ValidateObject(entity, validationContext, true);
                }
            }

            return context.SaveChangesAsync();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed && disposing)
            {
                context.Dispose();
            }

            disposed = true;
        }
    }
}
