﻿namespace Northwind.DependencyInjection.Interfaces
{
    using System;
    using System.Threading.Tasks;
    using Northwind.Domain.Models;

    public interface IUnitOfWorkRepository : IDisposable
    {
        IGenericRepository<Product> ProductRepository { get; }

        Task<int> SaveChangesAsync();
    }
}
