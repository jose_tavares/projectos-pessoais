﻿namespace Northwind.DependencyInjection.Interfaces
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Linq.Expressions;
    using Northwind.Domain.Models;
    using System.Collections.Generic;
    using Northwind.Common.Pagination;

    public interface IProductRepository
    {
        int Count { get; }

        Task<int> Add(Product product);

        bool Any(int id);

        ValueTask<Product> FindAsync(int id);

        Task<List<Product>> Entities();

        PagedResult<Product> Entities(int page, int pageSize, Expression<Func<Product, bool>> predicate = null);

        Task<int> Remove(Product product);

        Task<int> Update(Product product);

        IQueryable<Product> Where(Expression<Func<Product, bool>> predicate);
    }
}
