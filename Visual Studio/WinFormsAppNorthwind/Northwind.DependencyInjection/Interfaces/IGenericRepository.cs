﻿namespace Northwind.DependencyInjection.Interfaces
{
    using Northwind.Common.Pagination;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;

    public interface IGenericRepository<TEntity> where TEntity : class
    {
        int Count { get; }

        void Add(TEntity entity);

        bool Any(Expression<Func<TEntity, bool>> predicate);

        Task<List<TEntity>> Entities();

        PagedResult<TEntity> Entities(int page, int pageSize, Expression<Func<TEntity, bool>> predicate = null);

        void Remove(TEntity entity);

        void RemoveRange(IEnumerable<TEntity> entities);

        ValueTask<TEntity> FindAsync(params object[] keyValues);

        void Update(TEntity entity);

        IQueryable<TEntity> Where(Expression<Func<TEntity, bool>> predicate);

        Task<TEntity> SingleOrDefaultAsync(Expression<Func<TEntity, bool>> predicate);
    }
}
