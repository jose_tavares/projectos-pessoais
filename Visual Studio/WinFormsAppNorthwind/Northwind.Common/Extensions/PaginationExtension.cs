﻿namespace Northwind.Common.Extensions
{
    using Northwind.Common.Pagination;
    using System;
    using System.Linq;
    using System.Linq.Expressions;

    public static class PaginationExtension
    {
        public static PagedResult<T> GetPaged<T>(this IQueryable<T> query, int page, int pageSize, Expression<Func<T, bool>> predicate = null) where T : class
        {
            if (predicate != null)
            {
                query = query.Where(predicate);
            }

            PagedResult<T> result = new PagedResult<T>
            {
                CurrentPage = page,
                PageSize = pageSize,
                RowCount = query.Count()
            };

            double pageCount = (double)result.RowCount / pageSize;
            result.PageCount = (int)Math.Ceiling(pageCount);

            int skip = (page - 1) * pageSize;
            result.Results = query.Skip(skip).Take(pageSize);

            return result;
        }
    }
}
