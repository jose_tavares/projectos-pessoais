﻿namespace Northwind.Common.Extensions
{
    using System.Collections.Generic;
    using System.Linq;

    public static class CollectionExtension
    {
        public static bool Validate<T>(this IEnumerable<T> list) =>
            (list != null) && list.Any();
    }
}
