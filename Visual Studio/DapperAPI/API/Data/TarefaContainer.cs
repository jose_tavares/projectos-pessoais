﻿using System.Collections.Generic;

namespace API.Data
{
    public class TarefaContainer
    {
        public int Contador { get; set; }

        public IEnumerable<Tarefa> Tarefas { get; set; }
    }
}
