﻿using API.Data;
using API.Repositories;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TarefasController : ControllerBase
    {
        private readonly ITarefaRepository _tarefaRepo;
        
        public TarefasController(ITarefaRepository tarefaRepo)
        {
            _tarefaRepo = tarefaRepo;
        }

        [HttpGet]
        [Route("tarefas")]
        public async Task<IActionResult> GetTarefasAsync()
        {
            IEnumerable<Tarefa> result = await _tarefaRepo.GetTarefasAsync();
            return Ok(result);
        }

        [HttpGet]
        [Route("tarefa")]
        public async Task<IActionResult> GetTodoItemByIdAsync(int id)
        {
            Tarefa tarefa = await _tarefaRepo.GetTarefaByIdAsync(id);
            return Ok(tarefa);
        }

        [HttpGet]
        [Route("tarefascontador")]
        public async Task<IActionResult> GetTodosAndCountAsync()
        {
            TarefaContainer resultado = await _tarefaRepo.GetTarefasEContadorAsync();
            return Ok(resultado);
        }

        [HttpPost]
        [Route("criartarefa")]
        public async Task<IActionResult> SaveAsync(Tarefa novaTarefa)
        {
            int result = await _tarefaRepo.SaveAsync(novaTarefa);
            return Ok(result);
        }

        [HttpPost]
        [Route("atualizastatus")]
        public async Task<IActionResult> UpdateTodoStatusAsync(Tarefa atualizaTarefa)
        {
            int result = await _tarefaRepo.UpdateTarefaStatusAsync(atualizaTarefa);
            return Ok(result);
        }

        [HttpDelete]
        [Route("deletatarefa")]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            int resultado = await _tarefaRepo.DeleteAsync(id);
            return Ok(resultado);
        }

    }
}
