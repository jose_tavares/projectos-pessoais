﻿using Dapper;
using API.Data;
using System.Data;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace API.Repositories
{
    public class TarefaRepository : ITarefaRepository
    {
        private readonly DbSession _db;

        public TarefaRepository(DbSession dbSession)
        {
            _db = dbSession;
        }

        public async Task<IEnumerable<Tarefa>> GetTarefasAsync()
        {
            using IDbConnection conn = _db.Connection;
            string query = "SELECT * FROM Tarefas";
            IEnumerable<Tarefa> tarefas = await conn.QueryAsync<Tarefa>(sql: query);
            return tarefas;
        }

        public async Task<Tarefa> GetTarefaByIdAsync(int id)
        {
            using IDbConnection conn = _db.Connection;
            string query = "SELECT * FROM Tarefas WHERE Id = @id";
            Tarefa tarefa = await conn.QueryFirstOrDefaultAsync<Tarefa>
                (sql: query, param: new { id });

            return tarefa;
        }
        
        public async Task<TarefaContainer> GetTarefasEContadorAsync()
        {
            using IDbConnection conn = _db.Connection;
            string query =
                @"SELECT COUNT(*) FROM Tarefas
    				SELECT * FROM Tarefas";

            var reader = await conn.QueryMultipleAsync(sql: query);

            return new TarefaContainer
            {
                Contador = await reader.ReadFirstOrDefaultAsync<int>(),
                Tarefas = await reader.ReadAsync<Tarefa>()
            };
        }
        
        public async Task<int> SaveAsync(Tarefa novaTarefa)
        {
            using IDbConnection conn = _db.Connection;
            string command = @"
    				INSERT INTO Tarefas(Descricao, IsCompleta)
    				VALUES(@Descricao, @IsCompleta)";

            var result = await conn.ExecuteAsync(sql: command, param: novaTarefa);
            return result;
        }
        
        public async Task<int> UpdateTarefaStatusAsync(Tarefa atualizaTarefa)
        {
            using IDbConnection conn = _db.Connection;
            string command = @"
    		     UPDATE Tarefas SET IsCompleta = @IsCompleta WHERE Id = @Id";

            var result = await conn.ExecuteAsync(sql: command, param: atualizaTarefa);
            return result;
        }
        
        public async Task<int> DeleteAsync(int id)
        {
            using IDbConnection conn = _db.Connection;
            string command = @"DELETE FROM Tarefas WHERE Id = @id";
            var resultado = await conn.ExecuteAsync(sql: command, param: new { id });
            return resultado;
        }
    }
}
