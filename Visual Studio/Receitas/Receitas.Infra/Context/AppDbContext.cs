﻿using Receitas.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Receitas.Infra.Context
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        { }

        public DbSet<Receita> Receitas { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Receita>()
                .Property(p => p.Nome)
                .HasMaxLength(80);

            modelBuilder.Entity<Receita>()
                .Property(p => p.Preparo)
                .HasMaxLength(500);

            modelBuilder.Entity<Receita>()
                .Property(p => p.Ingredientes)
                .HasMaxLength(500);

            modelBuilder.Entity<Receita>()
               .Property(p => p.Imagem)
               .HasMaxLength(255);

            modelBuilder.Entity<Receita>()
               .HasData(
                    new Receita
                    {
                        Id = 1,
                        Nome = "Bolo de Chocolate",
                        Preparo = "Em um liquidificador adicione os ovos, o chocolate em pó, a manteiga, a farinha de trigo, o açúcar e o leite, depois bata por 5 minutos.Adicione o fermento e misture com uma espátula delicadamente.Em uma forma untada, " + "despeje a massa e asse em forno médio (180 ºC) preaquecido por cerca de 40 minutos.",
                        Ingredientes = "4 ovos, 4 colheres(sopa) de chocolate em pó, 2 colheres(sopa) de manteiga," +
                        "3 xícaras(chá) de farinha de trigo, 2 xícaras(chá) de açúcar, 2 colheres(sopa) de fermento, 1 xícara(chá) de leite",
                        Imagem = "bolochocolate.jpg",
                        Rendimento = "10 porções",
                        Tempo = 40
                    },
                    new Receita
                    {
                        Id = 2,
                        Nome = "Pudim de Leite Condensado",
                        Preparo = "Primeiro, bata bem os ovos no liquidificador e acrescente o leite condensado e o leite e bata novamente.Asse em forno médio por 45 minutos, com a assadeira redonda dentro de uma maior com água. " +
                        "Espete um garfo para ver se está bem assado.Deixe esfriar e desenforme.",
                        Ingredientes = "1 lata de leite condensado, 1 lata de leite, 3 ovos, 1 chícara de açucar, 1/2 xícara de água",
                        Imagem = "pudimleite.jpg",
                        Rendimento = "8 porções",
                        Tempo = 60
                    }
                );
        }
    }
}