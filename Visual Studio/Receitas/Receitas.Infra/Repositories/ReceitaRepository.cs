﻿using Receitas.Infra.Context;
using Receitas.Domain.Entities;
using Receitas.Domain.Interfaces;
using System.Collections.Generic;

namespace Receitas.Infra.Repositories
{
    public class ReceitaRepository : IReceitaRepository
    {
        private readonly AppDbContext _context;
        
        public ReceitaRepository(AppDbContext context)
        {
            _context = context;
        }

        public IEnumerable<Receita> GetReceitas()
        {
            return _context.Receitas;
        }
    }
}
