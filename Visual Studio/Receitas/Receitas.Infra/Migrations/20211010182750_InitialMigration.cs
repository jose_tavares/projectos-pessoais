﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Receitas.Infra.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Receitas",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Tempo = table.Column<int>(type: "int", nullable: false),
                    Nome = table.Column<string>(type: "nvarchar(80)", maxLength: 80, nullable: true),
                    Imagem = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    Preparo = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    Rendimento = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Ingredientes = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Receitas", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "Receitas",
                columns: new[] { "Id", "Imagem", "Ingredientes", "Nome", "Preparo", "Rendimento", "Tempo" },
                values: new object[] { 1, "bolochocolate.jpg", "4 ovos, 4 colheres(sopa) de chocolate em pó, 2 colheres(sopa) de manteiga,3 xícaras(chá) de farinha de trigo, 2 xícaras(chá) de açúcar, 2 colheres(sopa) de fermento, 1 xícara(chá) de leite", "Bolo de Chocolate", "Em um liquidificador adicione os ovos, o chocolate em pó, a manteiga, a farinha de trigo, o açúcar e o leite, depois bata por 5 minutos.Adicione o fermento e misture com uma espátula delicadamente.Em uma forma untada, despeje a massa e asse em forno médio (180 ºC) preaquecido por cerca de 40 minutos.", "10 porções", 40 });

            migrationBuilder.InsertData(
                table: "Receitas",
                columns: new[] { "Id", "Imagem", "Ingredientes", "Nome", "Preparo", "Rendimento", "Tempo" },
                values: new object[] { 2, "pudimleite.jpg", "1 lata de leite condensado, 1 lata de leite, 3 ovos, 1 chícara de açucar, 1/2 xícara de água", "Pudim de Leite Condensado", "Primeiro, bata bem os ovos no liquidificador e acrescente o leite condensado e o leite e bata novamente.Asse em forno médio por 45 minutos, com a assadeira redonda dentro de uma maior com água. Espete um garfo para ver se está bem assado.Deixe esfriar e desenforme.", "8 porções", 60 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Receitas");
        }
    }
}
