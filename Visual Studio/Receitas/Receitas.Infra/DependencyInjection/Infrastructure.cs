﻿using Receitas.Infra.Context;
using Receitas.Domain.Interfaces;
using Receitas.Infra.Repositories;
using Microsoft.EntityFrameworkCore;
using Receitas.Application.Services;
using Receitas.Application.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Receitas.Infra.DependencyInjection
{
    public static class Infrastructure
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<AppDbContext>(options =>
                options.UseSqlServer(
                    configuration.GetConnectionString("DefaultConnection"),
                    b => b.MigrationsAssembly(typeof(AppDbContext)
                            .Assembly.FullName)));

            services.AddDatabaseDeveloperPageExceptionFilter();

            services.AddScoped<IReceitaService, ReceitaService>();
            services.AddScoped<IReceitaRepository, ReceitaRepository>();

            return services;
        }
    }
}
