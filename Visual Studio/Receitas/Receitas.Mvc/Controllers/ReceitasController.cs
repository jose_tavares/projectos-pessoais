﻿using Microsoft.AspNetCore.Mvc;
using Receitas.Application.Interfaces;

namespace Receitas.Mvc.Controllers
{
    public class ReceitasController : Controller
    {
        private readonly IReceitaService _receitaService;

        public ReceitasController(IReceitaService receitaService)
        {
            _receitaService = receitaService;
        }

        public IActionResult Index()
        {
            var model = _receitaService.GetReceitas();

            return View(model);
        }
    }
}
