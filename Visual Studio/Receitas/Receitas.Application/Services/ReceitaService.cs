﻿using Receitas.Domain.Interfaces;
using Receitas.Application.Interfaces;
using Receitas.Application.ViewModels;

namespace Receitas.Application.Services
{
    public class ReceitaService : IReceitaService
    {
        private readonly IReceitaRepository _receitaRepository;
        
        public ReceitaService(IReceitaRepository receitaRepository)
        {
            _receitaRepository = receitaRepository;
        }

        public ReceitaViewModel GetReceitas()
        {
            return new ReceitaViewModel()
            {
                Receitas = _receitaRepository.GetReceitas()
            };
        }
    }
}
