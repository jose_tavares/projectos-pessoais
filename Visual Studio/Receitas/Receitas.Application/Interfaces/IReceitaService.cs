﻿using Receitas.Application.ViewModels;

namespace Receitas.Application.Interfaces
{
    public interface IReceitaService
    {
        ReceitaViewModel GetReceitas();
    }
}
