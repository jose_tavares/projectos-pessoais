﻿using Receitas.Domain.Entities;
using System.Collections.Generic;

namespace Receitas.Application.ViewModels
{
    public class ReceitaViewModel
    {
        public IEnumerable<Receita> Receitas { get; set; }
    }
}
