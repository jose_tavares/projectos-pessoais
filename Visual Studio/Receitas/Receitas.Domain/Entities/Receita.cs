﻿namespace Receitas.Domain.Entities
{
    public class Receita
    {
        public int Id { get; set; }

        public int Tempo { get; set; }

        public string Nome { get; set; }

        public string Imagem { get; set; }

        public string Preparo { get; set; }

        public string Rendimento { get; set; }

        public string Ingredientes { get; set; }
    }
}
