﻿using Receitas.Domain.Entities;
using System.Collections.Generic;

namespace Receitas.Domain.Interfaces
{
    public interface IReceitaRepository
    {
        IEnumerable<Receita> GetReceitas();
    }
}
