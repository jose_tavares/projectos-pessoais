﻿namespace WiredBrainCoffee.ConsoleApp.Model;

public partial class Person
{
    public string? FirstName { get; set; }

    public string? LastName { get; set; }
}