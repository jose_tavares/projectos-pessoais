﻿namespace eStore.Domain.Entities
{
    using eStore.Domain.Validation;

    public sealed class Category : Entity
    {
        #region Properties
        public string Name { get; private set; }

        public ICollection<Product> Products { get; set; }
        #endregion

        #region Constructors
        public Category(string name)
        {
            ValidateDomain(name);
        }

        public Category(int id, string name)
        {
            DomainExceptionValidation.When(id < 0, "Invalid Id value.");
            Id = id;
            ValidateDomain(name);
        }
        #endregion

        #region Methods
        public void Update(string name)
        {
            ValidateDomain(name);
        }

        private void ValidateDomain(string name)
        {
            DomainExceptionValidation.When(string.IsNullOrEmpty(name), "Invalid name. Name is required");
            DomainExceptionValidation.When(name.Length < 3, "Invalid name, too short, minimum 3 characters");
            Name = name;
        }
        #endregion
    }
}
