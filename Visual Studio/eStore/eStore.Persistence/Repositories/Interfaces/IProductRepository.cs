﻿namespace eStore.Persistence.Repositories.Interfaces
{
    using eStore.Domain.Entities;

    public interface IProductRepository : IGenericRepository<Product>
    {
        Task<IEnumerable<Product>> GetByCategoryId(int? id);
    }
}
