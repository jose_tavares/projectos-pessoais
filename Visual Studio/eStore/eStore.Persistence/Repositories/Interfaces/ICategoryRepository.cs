﻿namespace eStore.Persistence.Repositories.Interfaces
{
    using eStore.Domain.Entities;

    public interface ICategoryRepository : IGenericRepository<Category> { }
}
