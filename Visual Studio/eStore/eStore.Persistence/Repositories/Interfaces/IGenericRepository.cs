﻿namespace eStore.Persistence.Repositories.Interfaces
{
    public interface IGenericRepository<Entity> where Entity : class
    {
        Task<IEnumerable<Entity>> GetAll();

        Task<Entity> GetById(int? id);

        Task<int> Create(Entity entity);

        Task<int> Update(Entity entity);

        Task<int> Remove(Entity entity);
    }
}
