﻿namespace eStore.Persistence.Repositories.Implementations
{
    using eStore.Persistence.Context;
    using eStore.Persistence.Repositories.Interfaces;
    using Microsoft.EntityFrameworkCore;

    public class GenericRepository<Entity> : IGenericRepository<Entity> where Entity : class
    {
        private readonly ApplicationDbContext context;
        private readonly DbSet<Entity> entities;

        public GenericRepository(ApplicationDbContext context)
        {
            this.context = context;
            entities = context.Set<Entity>();
        }

        public async Task<int> Create(Entity entity)
        {
            entities.Add(entity);
            return await context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Entity>> GetAll()
        {
            return await entities.ToListAsync();
        }

        public async Task<Entity> GetById(int? id)
        {
            return await entities.FindAsync(id);
        }

        public async Task<int> Remove(Entity entity)
        {
            entities.Remove(entity);
            return await context.SaveChangesAsync();
        }

        public async Task<int> Update(Entity entity)
        {
            entities.Update(entity);
            return await context.SaveChangesAsync();
        }
    }
}
