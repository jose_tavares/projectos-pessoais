﻿namespace eStore.Persistence.Repositories.Implementations
{
    using eStore.Domain.Entities;
    using eStore.Persistence.Context;
    using eStore.Persistence.Repositories.Interfaces;
    using Microsoft.EntityFrameworkCore;
    using System.Threading.Tasks;

    public class ProductRepository : GenericRepository<Product>, IProductRepository
    {
        private readonly ApplicationDbContext context;

        public ProductRepository(ApplicationDbContext context) : base(context)
        {
            this.context = context;
        }

        public async Task<IEnumerable<Product>> GetByCategoryId(int? id)
        {
            IQueryable<Product> products = context.Products.Where(product => product.CategoryId == id);

            return await products.ToListAsync();
        }
    }
}
