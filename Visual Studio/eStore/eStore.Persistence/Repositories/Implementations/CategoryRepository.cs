﻿namespace eStore.Persistence.Repositories.Implementations
{
    using eStore.Domain.Entities;
    using eStore.Persistence.Context;
    using eStore.Persistence.Repositories.Interfaces;

    public class CategoryRepository : GenericRepository<Category>, ICategoryRepository
    {
        public CategoryRepository(ApplicationDbContext context) : base(context) { }
    }
}
