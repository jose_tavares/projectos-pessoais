﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace eStore.Persistence.Migrations
{
    public partial class SeedProducts : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("INSERT INTO Products(Name,Description,Price,Stock,CategoryId,Image) " +
            "VALUES('Caderno espiral','Caderno espiral 100 fôlhas',7.45,50,1,'carderno.jpg')");

            migrationBuilder.Sql("INSERT INTO Products(Name,Description,Price,Stock,CategoryId,Image) " +
            "VALUES('Estojo escolar','Estojo escolar cinza',5.65,70,1,'estojo.jpg')");

            migrationBuilder.Sql("INSERT INTO Products(Name,Description,Price,Stock,CategoryId,Image) " +
            "VALUES('Borracha escolar','Borracha branca pequena',3.25,80,1,'borracha.jpg')");

            migrationBuilder.Sql("INSERT INTO Products(Name,Description,Price,Stock,CategoryId,Image) " +
            "VALUES('Calculadora escolar','Calculadora simples',15.39,20,2,'calculadora.jpg')");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DELETE FROM Products");
        }
    }
}
