﻿// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace eStore.API.Controllers
{
    using eStore.Application.Services.Interfaces;
    using Microsoft.AspNetCore.Mvc;

    [Route("api/[controller]")]
    [ApiController]
    public class GenericController<DTO, Entity> : ControllerBase where DTO : class where Entity : class
    {
        private readonly IGenericService<DTO, Entity> genericService;

        public GenericController(IGenericService<DTO, Entity> genericService)
        {
            this.genericService = genericService;
        }

        // GET: api/<GenericController>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DTO>>> Get()
        {
            IEnumerable<DTO> dtos = await genericService.GetAll();

            if (dtos == null)
            {
                return NotFound();
            }

            return Ok(dtos);
        }

        // GET api/<GenericController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<DTO>> Get(int id)
        {
            DTO dto = await genericService.GetById(id);

            if (dto == null)
            {
                return NotFound();
            }

            return Ok(dto);
        }

        // POST api/<GenericController>
        [HttpPost]
        public async Task<ActionResult> Post([FromBody] DTO dto)
        {
            bool result = await genericService.Add(dto);

            if (!result)
            {
                return NoContent();
            }

            return Accepted();
        }

        // PUT api/<GenericController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<GenericController>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            DTO dto = await genericService.GetById(id);

            if (dto == null)
            {
                return NotFound();
            }

            bool result = await genericService.Remove(dto);

            if (!result)
            {
                return NoContent();
            }

            return Ok();
        }
    }
}
