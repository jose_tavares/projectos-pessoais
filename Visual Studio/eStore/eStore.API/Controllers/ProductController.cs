﻿namespace eStore.API.Controllers
{
    using eStore.Application.DTOs;
    using eStore.Application.Services.Interfaces;
    using eStore.Domain.Entities;
    using Microsoft.AspNetCore.Mvc;

    public class ProductController : GenericController<ProductDTO, Product>
    {
        private readonly IProductService productService;

        public ProductController(IProductService productService) : base(productService)
        {
            this.productService = productService;
        }

        [HttpGet("Category/{id}")]
        public async Task<ActionResult<IEnumerable<ProductDTO>>> GetByCategoryId(int id)
        {
            IEnumerable<ProductDTO> productDtos = await productService.GetByCategoryId(id);

            if (productDtos == null)
            {
                return NotFound();
            }

            return Ok(productDtos);
        }
    }
}
