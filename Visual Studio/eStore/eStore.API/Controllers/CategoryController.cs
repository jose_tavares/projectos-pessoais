﻿namespace eStore.API.Controllers
{
    using eStore.Application.DTOs;
    using eStore.Application.Services.Interfaces;
    using eStore.Domain.Entities;

    public class CategoryController : GenericController<CategoryDTO, Category>
    {
        public CategoryController(ICategoryService categoryService) : base(categoryService) { }
    }
}
