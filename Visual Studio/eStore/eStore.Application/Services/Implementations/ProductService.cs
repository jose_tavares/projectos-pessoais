﻿namespace eStore.Application.Services.Implementations
{
    using AutoMapper;
    using eStore.Application.DTOs;
    using eStore.Application.Services.Interfaces;
    using eStore.Domain.Entities;
    using eStore.Persistence.Repositories.Implementations;
    using eStore.Persistence.Repositories.Interfaces;
    using System.Threading.Tasks;

    public class ProductService : GenericService<ProductDTO, Product>, IProductService
    {
        private readonly IMapper mapper;
        private readonly IProductRepository productRepository;

        public ProductService(IMapper mapper, IProductRepository productRepository) : base(mapper, productRepository)
        {
            this.mapper = mapper;
            this.productRepository = productRepository;
        }

        public async Task<IEnumerable<ProductDTO>> GetByCategoryId(int? id)
        {
            IEnumerable<Product> products = await productRepository.GetByCategoryId(id);

            return mapper.Map<IEnumerable<ProductDTO>>(products);
        }
    }
}
