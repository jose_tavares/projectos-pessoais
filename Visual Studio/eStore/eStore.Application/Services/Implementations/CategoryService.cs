﻿namespace eStore.Application.Services.Implementations
{
    using AutoMapper;
    using eStore.Application.DTOs;
    using eStore.Application.Services.Interfaces;
    using eStore.Domain.Entities;
    using eStore.Persistence.Repositories.Interfaces;

    public class CategoryService : GenericService<CategoryDTO, Category>, ICategoryService
    {
        public CategoryService(IMapper mapper, ICategoryRepository categoryRepository) : base(mapper, categoryRepository) { }
    }
}
