﻿namespace eStore.Application.Services.Implementations
{
    using AutoMapper;
    using eStore.Application.Services.Interfaces;
    using eStore.Persistence.Repositories.Interfaces;
    using System.Reflection;

    public class GenericService<DTO, Entity> : IGenericService<DTO, Entity> where DTO : class where Entity : class
    {
        private readonly IMapper mapper;
        private readonly IGenericRepository<Entity> genericRepository;

        public GenericService(IMapper mapper, IGenericRepository<Entity> genericRepository)
        {
            this.mapper = mapper;
            this.genericRepository = genericRepository;
        }

        public async Task<bool> Add(DTO dto)
        {
            Entity mapEntity = mapper.Map<Entity>(dto);
            int result = await genericRepository.Create(mapEntity);

            return result == 1;
        }

        public async Task<IEnumerable<DTO>> GetAll()
        {
            IEnumerable<Entity> entities = await genericRepository.GetAll();

            return mapper.Map<IEnumerable<DTO>>(entities);
        }

        public async Task<DTO> GetById(int? id)
        {
            Entity entity = await genericRepository.GetById(id);

            return mapper.Map<DTO>(entity);
        }

        public async Task<bool> Remove(DTO dto)
        {
            Type t = dto.GetType();
            PropertyInfo prop = t.GetProperty("Id");

            int value = (int)prop.GetValue(dto);
            Entity entity = await genericRepository.GetById(value);

            int result = await genericRepository.Remove(entity);

            return result == 1;
        }

        public async Task Update(DTO dto)
        {
            Entity mapEntity = mapper.Map<Entity>(dto);
            await genericRepository.Update(mapEntity);
        }
    }
}
