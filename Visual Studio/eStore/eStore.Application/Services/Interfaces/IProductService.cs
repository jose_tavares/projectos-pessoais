﻿namespace eStore.Application.Services.Interfaces
{
    using eStore.Application.DTOs;
    using eStore.Domain.Entities;

    public interface IProductService : IGenericService<ProductDTO, Product>
    {
        Task<IEnumerable<ProductDTO>> GetByCategoryId(int? id);
    }
}
