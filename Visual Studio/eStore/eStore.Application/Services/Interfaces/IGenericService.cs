﻿namespace eStore.Application.Services.Interfaces
{
    public interface IGenericService<DTO, Entity> where DTO : class where Entity : class
    {
        Task<IEnumerable<DTO>> GetAll();

        Task<DTO> GetById(int? id);

        Task<bool> Add(DTO dto);

        Task Update(DTO dto);

        Task<bool> Remove(DTO dto);
    }
}
