﻿namespace eStore.Application.Services.Interfaces
{
    using eStore.Application.DTOs;
    using eStore.Domain.Entities;

    public interface ICategoryService : IGenericService<CategoryDTO, Category> { }
}
