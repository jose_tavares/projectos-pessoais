﻿namespace eStore.Application.Mappings
{
    using AutoMapper;
    using eStore.Application.DTOs;
    using eStore.Domain.Entities;

    public class DomainToDTOMappingProfile : Profile
    {
        public DomainToDTOMappingProfile()
        {
            CreateMap<Product, ProductDTO>().ReverseMap();
            CreateMap<Category, CategoryDTO>().ReverseMap();
        }
    }
}
