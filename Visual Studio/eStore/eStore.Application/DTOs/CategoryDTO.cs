﻿namespace eStore.Application.DTOs
{
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;

    public class CategoryDTO : EntityDTO
    {
        [MinLength(3)]
        [MaxLength(100)]
        [DisplayName("Name")]
        [Required(ErrorMessage = "The Name is Required")]
        public string Name { get; set; }
    }
}
