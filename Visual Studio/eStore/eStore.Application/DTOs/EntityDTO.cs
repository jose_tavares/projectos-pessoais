﻿namespace eStore.Application.DTOs
{
    public abstract class EntityDTO
    {
        public int Id { get; set; }
    }
}
