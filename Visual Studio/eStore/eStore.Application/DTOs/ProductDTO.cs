﻿namespace eStore.Application.DTOs
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class ProductDTO : EntityDTO
    {
        [MinLength(3)]
        [MaxLength(100)]
        [Required(ErrorMessage = "The Name is Required")]
        public string Name { get; set; }

        [MinLength(5)]
        [MaxLength(200)]
        [Required(ErrorMessage = "The Description is Required")]
        public string Description { get; set; }

        [DataType(DataType.Currency)]
        [Column(TypeName = "decimal(18,2)")]
        [DisplayFormat(DataFormatString = "{0:C2}")]
        [Required(ErrorMessage = "The Price is Required")]
        public decimal Price { get; set; }

        [Required(ErrorMessage = "The Stock is Required")]
        [Range(1, 9999)]
        public int Stock { get; set; }

        [MaxLength(250)]
        public string Image { get; set; }

        public CategoryDTO Category { get; set; }

        public int CategoryId { get; set; }
    }
}
