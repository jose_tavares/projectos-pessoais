﻿using InventarioNet.Models;

namespace InventarioNet.Repositories
{
    public class ProdutoRepository : GenericRepository<Produto>, IProdutoRepository
    {
        public ProdutoRepository(AppDbContext repositoryContext)
             : base(repositoryContext) { }
    }
}
