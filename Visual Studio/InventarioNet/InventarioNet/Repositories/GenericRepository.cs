﻿using System;
using InventarioNet.Models;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace InventarioNet.Repositories
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        private readonly AppDbContext _context;

        public GenericRepository(AppDbContext context) =>
            _context = context;

        public async Task Delete(int id)
        {
            var entity = await GetById(id);

            _context.Set<T>().Remove(entity);

            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<T>> GetAll() => 
            await _context.Set<T>().AsNoTracking().ToListAsync();

        public async Task<T> GetById(int id) =>
            await _context.Set<T>().FindAsync(id);

        public async Task Insert(T obj)
        {
            await _context.Set<T>().AddAsync(obj);

            await _context.SaveChangesAsync();
        }

        public async Task Update(int id, T obj)
        {
            _context.Set<T>().Update(obj);
            
            await _context.SaveChangesAsync();
        }
    }
}
