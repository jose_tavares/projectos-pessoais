﻿using System.Threading.Tasks;
using System.Collections.Generic;

namespace InventarioNet.Repositories
{
    public interface IGenericRepository<T> where T : class
    {
        Task Delete(int id);

        Task<IEnumerable<T>> GetAll();

        Task<T> GetById(int id);

        Task Insert(T obj);

        Task Update(int id, T obj);
    }
}
