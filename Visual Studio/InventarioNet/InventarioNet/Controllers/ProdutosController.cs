﻿using System;
using System.Linq;
using InventarioNet.Models;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using InventarioNet.Repositories;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace InventarioNet.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class ProdutosController : ControllerBase
    {
        private readonly IProdutoRepository _repository;

        public ProdutosController(IProdutoRepository repository) =>
            _repository = repository;

        [HttpGet]
        [AllowAnonymous]
        public ActionResult<string> Get() =>
            "ProdutosController ::  Acessado em  : " + DateTime.Now.ToLongDateString();

        [HttpGet("todos")]
        public async Task<ActionResult<IEnumerable<Produto>>> GetProdutos()
        {
            var produtos = await _repository.GetAll();

            if (produtos == null)
            { return BadRequest(); }

            return Ok(produtos.ToList());
        }

        // GET: api/Products/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Produto>> GetProduto(int id)
        {
            var produto = await _repository.GetById(id);

            if (produto == null)
            { return NotFound("Produto não encontrado pelo id informado"); }

            return Ok(produto);
        }

        // POST api/<controller>  
        [HttpPost]
        public async Task<IActionResult> PostProduto([FromBody] Produto produto)
        {
            if (produto == null)
            { return BadRequest("Produto é null"); }

            await _repository.Insert(produto);

            return CreatedAtAction(nameof(GetProduto), new { Id = produto.ProdutoId }, produto);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutProduto(int id, Produto produto)
        {
            if (id != produto.ProdutoId)
            { return BadRequest($"O código do produto {id} não confere"); }

            try { await _repository.Update(id, produto); }

            catch (DbUpdateConcurrencyException) { throw; }

            return Ok("Atualização do produto realizada com sucesso");
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<Produto>> DeleteProduto(int id)
        {
            var produto = await _repository.GetById(id);

            if (produto == null)
            { return NotFound($"Produto de {id} foi não encontrado"); }

            await _repository.Delete(id);

            return Ok(produto);
        }
    }
}
