﻿using System;
using System.Text;
using InventarioNet.Models;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.Extensions.Configuration;

namespace InventarioNet.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TokenController : ControllerBase
    {
        private readonly AppDbContext _context;
        private readonly IConfiguration _configuration;

        public TokenController
            (AppDbContext context,
            IConfiguration configuration)
        {
            _context = context;
            _configuration = configuration;
        }

        [HttpPost]
        public async Task<IActionResult> AutenticaUsuario(Usuario _usuario)
        {
            if ((_usuario != null) &&
                (_usuario.Email != null) &&
                (_usuario.Senha != null))
            {
                var usuario = await GetUsuario(_usuario.Email, _usuario.Senha);

                if (usuario != null)
                {
                    //cria claims baseado nas informações do usuário
                    var claims = new[] {
                        new Claim(JwtRegisteredClaimNames.Sub, _configuration["Jwt:Subject"]),
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                        new Claim(JwtRegisteredClaimNames.Iat, DateTime.UtcNow.ToString()),
                        new Claim("Id", usuario.UsuarioId.ToString()),
                        new Claim("Nome", usuario.Nome),
                        new Claim("Login", usuario.Login),
                        new Claim("Email", usuario.Email)
                    };

                    var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]));
                    
                    var signIn = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
                    
                    var token = new JwtSecurityToken(_configuration["Jwt:Issuer"],
                                 _configuration["Jwt:Audience"], claims,
                                 expires: DateTime.UtcNow.AddDays(1), signingCredentials: signIn);

                    return Ok(new JwtSecurityTokenHandler().WriteToken(token));
                }
                else
                {
                    return BadRequest("Credenciais inválidas");
                }
            }
            else
            {
                return BadRequest();
            }
        }

        private async Task<Usuario> GetUsuario(string email, string password) =>
            await _context.Usuarios.FirstOrDefaultAsync(usuario =>
                (usuario.Email == email) && (usuario.Senha == password));
    }
}
