﻿namespace GaleriaFotos.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Foto
    {
        [Key]
        public int Id { get; set; }

        [StringLength(100)]
        public string Titulo { get; set; }

        [StringLength(100)]
        public string Nome { get; set; }

        [StringLength(255)]
        public string Caminho { get; set; }

        public int? Visualizacoes { get; set; }

        [NotMapped]
        [Required(ErrorMessage = "Foto é requerida.")]
        public List<IFormFile> ArquivoFoto { get; set; }
    }
}
