﻿namespace GaleriaFotos.ViewModels
{
    using GaleriaFotos.Models;

    public class FotoViewModel
    {
        public Foto Fotografia { get; set; }

        public List<Foto> ListaFotos { get; set; }
    }
}
