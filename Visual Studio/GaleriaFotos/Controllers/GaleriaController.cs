﻿namespace GaleriaFotos.Controllers
{
    using GaleriaFotos.Models;
    using GaleriaFotos.ViewModels;
    using Microsoft.AspNetCore.Mvc;

    public class GaleriaController : Controller
    {
        private readonly AppDbContext _db;

        public GaleriaController(AppDbContext db)
        {
            _db = db;
        }

        public IActionResult Index()
        {
            FotoViewModel viewModel = new()
            {
                ListaFotos = _db.Fotos.ToList(),
                Fotografia = new Foto()
            };

            return View(viewModel);
        }

        [HttpPost]
        public IActionResult IncluiFotos(FotoViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Index");
            }

            List<IFormFile> files = model.Fotografia.ArquivoFoto;

            if (files.Count > 0)
            {
                foreach (IFormFile item in files)
                {
                    Foto fotografia = new();

                    string guid = Guid.NewGuid().ToString();
                    string filePath = "wwwroot/fotografias/" + guid + item.FileName;
                    string fileName = guid + item.FileName;

                    using FileStream stream = System.IO.File.Create(filePath);

                    item.CopyTo(stream);
                    fotografia.Nome = fileName;
                    fotografia.Caminho = filePath;
                    fotografia.Titulo = item.FileName;
                    fotografia.Visualizacoes = 1;
                    _db.Fotos.Add(fotografia);
                    _db.SaveChanges();
                }

                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
        }

        public IActionResult DeletaFoto(int id)
        {
            Foto? photo = _db.Fotos.Find(id);

            if (photo != null)
            {
                _db.Fotos.Remove(photo);
                _db.SaveChanges();
            }

            return RedirectToAction("index");
        }
    }
}
