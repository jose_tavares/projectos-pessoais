﻿using System;
using System.Threading;

namespace Threads
{
    class Program
    {
        static void Main()
        {
            Thread workerThread = new Thread(new ThreadStart(Child));
            workerThread.Start();

            for (int i = 0; ; i++)
            {
                Console.WriteLine($"Main thread: {i + 1}");
                Thread.Sleep(200);
            }
        }

        /// <summary>  
        ///  This code is executed by a secondary thread  
        /// </summary>  
        static void Child()
        {
            for (int i = 0; ; i++)
            {
                Console.WriteLine($"Worker thread: {i + 1}");
                Thread.Sleep(1000);
            }
        }
    }
}
