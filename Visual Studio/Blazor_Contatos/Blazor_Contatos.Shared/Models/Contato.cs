﻿using System.Text.Json.Serialization;
using System.ComponentModel.DataAnnotations;

namespace Blazor_Contatos.Shared.Models
{
    public class Contato
    {
        [Key]
        [JsonPropertyName("id")]
        public long Id { get; set; }

        [Required]
        [JsonPropertyName("nome")]
        public string Nome { get; set; }

        [Required]
        [JsonPropertyName("email")]
        public string Email { get; set; }

        [Required]
        [JsonPropertyName("telefone")]
        public string Telefone { get; set; }
    }
}
