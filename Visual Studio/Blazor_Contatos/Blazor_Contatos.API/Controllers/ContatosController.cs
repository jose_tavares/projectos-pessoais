﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using Blazor_Contatos.Shared.Models;

namespace Blazor_Contatos.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ContatosController : ControllerBase
    {
        private static readonly List<Contato> _contatos =
            Enumerable.Range(1, 5).Select(index => new Contato
            {
                Id = index,
                Nome = $"Nome {index} Sobrenome {index}",
                Email = $"email{index}@teste.com",
                Telefone = $"+11 555 987{index}"
            }).ToList();

        [HttpGet]
        public ActionResult<IEnumerable<Contato>> Get() => _contatos;

        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<Contato> Get(int id)
        {
            var contato = _contatos.FirstOrDefault(first => first.Id == id);

            if (contato == null)
            {
                return NotFound();
            }

            return contato;
        }

        [HttpPost]
        public void Create([FromBody] Contato contato) => _contatos.Add(contato);

        [HttpPut("{id}")]
        public void Edit(int id, [FromBody] Contato contato)
        {
            int index = _contatos.FindIndex(i => i.Id == id);

            if (index > -1)
            {
                _contatos[index] = contato;
            }
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            int index = _contatos.FindIndex(i => i.Id == id);

            if (index > -1)
            {
                _contatos.RemoveAt(index);
            }
        }
    }
}