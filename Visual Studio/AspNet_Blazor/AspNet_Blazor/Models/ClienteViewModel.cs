﻿namespace AspNet_Blazor.Models
{
    public class ClienteViewModel
    {
        public string Nome { get; set; }
        public string Email { get; set; }
    }
}
