﻿using System;
using System.IO;
using Microsoft.Extensions.Logging;

namespace AspnCoreMvcLogging.Logging
{
    public class CustomLogger : ILogger
    {
        private readonly string _loggerName;
        private readonly CustomLoggerProviderConfiguration _loggerConfiguration;

        public CustomLogger(string loggerName, CustomLoggerProviderConfiguration loggerConfiguration)
        {
            _loggerName = loggerName;
            _loggerConfiguration = loggerConfiguration;
        }

        public IDisposable BeginScope<TState>(TState state) => null;

        public bool IsEnabled(LogLevel logLevel)
        {
            throw new NotImplementedException();
        }

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            var caminhoArquivoLog = AppContext.BaseDirectory + "log.txt";

            using (var streamWriter = new StreamWriter(caminhoArquivoLog, true))
            {
                streamWriter.WriteLine(string.Format("{0}: {1} - {2}", logLevel.ToString(), eventId.Id, formatter(state, exception)));
                streamWriter.Close();
            }
        }
    }
}
