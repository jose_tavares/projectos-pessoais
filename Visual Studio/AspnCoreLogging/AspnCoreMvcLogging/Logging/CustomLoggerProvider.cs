﻿using System;
using System.Collections.Concurrent;
using Microsoft.Extensions.Logging;

namespace AspnCoreMvcLogging.Logging
{
    public class CustomLoggerProvider : ILoggerProvider
    {
        private readonly CustomLoggerProviderConfiguration _loggerConfiguration;
        private readonly ConcurrentDictionary<string, CustomLogger> loggers = new ConcurrentDictionary<string, CustomLogger>();

        public CustomLoggerProvider(CustomLoggerProviderConfiguration loggerConfiguration) =>
            _loggerConfiguration = loggerConfiguration;

        public ILogger CreateLogger(string categoryName) =>
            loggers.GetOrAdd(categoryName, name =>
                new CustomLogger(name, _loggerConfiguration));

        public void Dispose() { }
    }
}
