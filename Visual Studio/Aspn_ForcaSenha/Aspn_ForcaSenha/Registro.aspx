﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Registro.aspx.cs" Inherits="Aspn_ForcaSenha.Registro" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Medindo a força da Palavra-passe</title>
    <script src="verificaForcaDaSenha.js"></script>
</head>
<body>
    <h2>Registo</h2>
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="lblTexto" Text="Defina a palavra-passe" runat="server"></asp:Label>
            <br />
            <asp:TextBox ID="txtSenha" runat="server" TextMode="Password" onkeyup="verificaForcaDaSenha()"></asp:TextBox>
            <asp:Label ID="lblmensagem" runat="server"></asp:Label>
        </div>
    </form>
</body>
</html>
