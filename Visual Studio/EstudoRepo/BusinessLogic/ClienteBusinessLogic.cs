﻿using System;
using System.Linq;
using DataAccess.Domain;
using DataAccess.Repository;
using System.Collections.Generic;

namespace BusinessLogic
{
    public class ClienteBusinessLogic : IDisposable
    {
        private readonly UnitOfWork _unitOfWork;

        public ClienteBusinessLogic() => _unitOfWork = new UnitOfWork();

        public ClienteBusinessLogic(UnitOfWork unitOfWork) => _unitOfWork = unitOfWork;

        public IEnumerable<Cliente> ListarClientes() => _unitOfWork.ClienteRepository.Get().ToList().AsEnumerable();

        public void AdicionarCliente(Cliente cliente)
        {
            _unitOfWork.ClienteRepository.Add(cliente);
            _unitOfWork.Commit();
        }

        public void ExcluirCliente(Cliente cliente)
        {
            _unitOfWork.ClienteRepository.Delete(cliente);
            _unitOfWork.Commit();
        }

        public void AlterarCliente(Cliente cliente)
        {
            _unitOfWork.ClienteRepository.Update(cliente);
            _unitOfWork.Commit();
        }

        public Cliente GetClientePorId(int codigo) => _unitOfWork.ClienteRepository.GetById(predicate => predicate.ClienteId == codigo);

        public void Dispose() => _unitOfWork.Dispose();
    }
}
