﻿using System;
using System.Linq;
using DataAccess.Domain;
using DataAccess.Context;
using DataAccess.Repository;
using System.Linq.Expressions;
using System.Collections.Generic;

namespace BusinessLogic
{
    public class ClienteRepository : IRepository<Cliente>, IClienteRepository
    {
        private readonly AppDbContext _context;

        public ClienteRepository(AppDbContext context) => _context = context;

        public void Add(Cliente entity) => _context.Clientes.Add(entity);

        public void Delete(Cliente entity) => _context.Clientes.Remove(entity);

        public IEnumerable<Cliente> Get() => _context.Clientes;

        public IEnumerable<Cliente> Get(Expression<Func<Cliente, bool>> predicate) => _context.Clientes.Where(predicate);

        public Cliente GetById(Expression<Func<Cliente, bool>> predicate) => _context.Clientes.FirstOrDefault(predicate);

        public IEnumerable<Cliente> GetClientesPorNome() => Get().OrderBy(keySelector => keySelector.Nome);

        public void Update(Cliente entity) => _context.Clientes.Update(entity);
    }
}
