﻿using DataAccess.Domain;

namespace DataAccess.Repository
{
    public interface IUnitOfWork
    {
        IRepository<Cliente> ClienteRepository { get; }

        void Commit();
    }
}
