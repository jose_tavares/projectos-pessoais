﻿using System;
using System.Linq.Expressions;
using System.Collections.Generic;

namespace DataAccess.Repository
{
    public interface IRepository<T> where T : class
    {
        void Add(T entity);

        void Delete(T entity);

        IEnumerable<T> Get();

        IEnumerable<T> Get(Expression<Func<T, bool>> predicate);

        T GetById(Expression<Func<T, bool>> predicate);

        void Update(T entity);
    }
}
