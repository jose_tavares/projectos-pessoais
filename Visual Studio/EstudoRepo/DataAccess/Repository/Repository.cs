﻿using System;
using System.Linq;
using DataAccess.Context;
using System.Linq.Expressions;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Repository
{
    public class Repository<T> : IRepository<T> where T : class
    {
        private readonly AppDbContext _context;

        public Repository(AppDbContext context) => _context = context;

        public void Add(T entity) => _context.Set<T>().Add(entity);

        public void Delete(T entity) => _context.Set<T>().Remove(entity);

        public IEnumerable<T> Get() => _context.Set<T>();

        public IEnumerable<T> Get(Expression<Func<T, bool>> predicate) => _context.Set<T>().Where(predicate);

        public T GetById(Expression<Func<T, bool>> predicate) => _context.Set<T>().SingleOrDefault(predicate);

        public void Update(T entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
            _context.Set<T>().Update(entity);
        }
    }
}
