﻿using System;
using DataAccess.Domain;
using DataAccess.Context;

namespace DataAccess.Repository
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private readonly AppDbContext _context;

        private readonly Repository<Cliente> _clienteRepository;

        public IRepository<Cliente> ClienteRepository => _clienteRepository ?? new Repository<Cliente>(_context);

        public UnitOfWork() => _context = new AppDbContext();

        public UnitOfWork(AppDbContext context) => _context = context;

        public void Commit() => _context.SaveChanges();

        public void Dispose() => _context.Dispose();
    }
}
