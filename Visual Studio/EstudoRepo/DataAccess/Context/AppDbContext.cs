﻿using DataAccess.Domain;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Context
{
    public class AppDbContext : DbContext
    {
        public DbSet<Cliente> Clientes { get; set; }

        public AppDbContext() { }

        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) =>
            optionsBuilder.UseSqlServer(@"Data Source=192.168.10.11\SQLDEV, 1533;Initial Catalog=Cadastro; Persist Security Info=True;MultipleActiveResultSets=True;user id=sa;Password=_PWD4sa_");
    }
}
