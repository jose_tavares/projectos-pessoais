﻿using System.Web.Mvc;
using System.Web.Helpers;

namespace UsandoChartHelper.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Chart()
        {
            var macChart = new Chart(600, 400)
                .AddTitle("Campeonato Paulista - 2012")
                .AddSeries
                (
                    "CampeonatoPaulista",
                    xValue: new [] { "São Paulo", "Corinthians", "Palmeiras", "Santos", "Mogi Mirim" },
                    yValues: new[] { "34", "34", "32", "30", "30" }
                ).Write();

            return View(macChart);
        }
    }
}