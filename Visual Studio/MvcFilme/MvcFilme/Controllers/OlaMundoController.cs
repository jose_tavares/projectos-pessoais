﻿using Microsoft.AspNetCore.Mvc;
using System.Text.Encodings.Web;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MvcFilme.Controllers
{
    public class OlaMundoController : Controller
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult BemVindo(string nome, int ID = 1)
        {
            ViewData["Mensagem"] = "Olá " + nome;
            ViewData["NumVezes"] = ID;

            return View();
        }
    }
}
