﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MvcFilme.Models
{
    public class Filme
    {
        public int ID { get; set; }

        [Required]
        [StringLength(60, MinimumLength = 3)]
        public string Titulo { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Data de Lançamento")]
        public DateTime Lancamento { get; set; }

        [Required]
        [StringLength(30)]
        [Display(Name = "Gênero")]
        [RegularExpression(@"^[A-Z]+[a-zA-Z''-'\s]*$")]
        public string Genero { get; set; }

        [Range(1, 100)]
        [Display(Name = "Preço")]
        [DataType(DataType.Currency)]
        public decimal Preco { get; set; }

        [Required]
        [StringLength(5)]
        [RegularExpression(@"^[A-Z]+[a-zA-Z''-'\s]*$")]
        public string Classificacao { get; set; }
    }
}
