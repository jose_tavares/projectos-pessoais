﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace MvcFilme.Models
{
    public class FilmeGeneroViewModel
    {
        public List<Filme> filmes;

        public SelectList generos;

        public string FilmeGenero { get; set; }
    }
}
