﻿using System;
using System.Linq;
using MvcFilme.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace MvcFilme.Context
{
    public class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new MvcFilmeContext
                (serviceProvider.GetRequiredService<DbContextOptions<MvcFilmeContext>>()))
            {
                if (context.Filmes.Any())
                {
                    return;
                }

                context.Filmes.AddRange(
                     new Filme
                     {
                         Titulo = "A Bela e a Fera",
                         Classificacao = "L",
                         Lancamento = DateTime.Parse("2017-3-16"),
                         Genero = "Fantasia/Romance",
                         Preco = 7.99M
                     },
                     new Filme
                     {
                         Titulo = "Caça-Fantasmas",
                         Classificacao = "L",
                         Lancamento = DateTime.Parse("1984-3-13"),
                         Genero = "Comedia",
                         Preco = 8.99M
                     },
                     new Filme
                     {
                         Titulo = "Kong - A ilha da Caveira",
                         Classificacao = "L",
                         Lancamento = DateTime.Parse("2017-3-09"),
                         Genero = "Ficção",
                         Preco = 9.99M
                     },
                   new Filme
                   {
                       Titulo = "Rio Bravo",
                       Classificacao = "L",
                       Lancamento = DateTime.Parse("1959-4-15"),
                       Genero = "Western",
                       Preco = 3.99M
                   }
                );

                context.SaveChanges();
            }
        }
    }
}
