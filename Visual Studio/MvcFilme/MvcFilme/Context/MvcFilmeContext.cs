﻿using MvcFilme.Models;
using Microsoft.EntityFrameworkCore;

namespace MvcFilme.Context
{
    public class MvcFilmeContext : DbContext
    {
        public DbSet<Filme> Filmes { get; set; }

        public MvcFilmeContext(DbContextOptions<MvcFilmeContext> options) : base(options) { }
    }
}
