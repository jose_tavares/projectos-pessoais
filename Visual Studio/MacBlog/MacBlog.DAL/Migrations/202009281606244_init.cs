namespace MacBlog.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Blogs",
                c => new
                    {
                        BlogId = c.Int(nullable: false, identity: true),
                        Titulo = c.String(nullable: false, maxLength: 150),
                        Corpo = c.String(nullable: false),
                        CriadoEm = c.DateTime(nullable: false),
                        AtualizadoEm = c.DateTime(nullable: false),
                        CategoriaId = c.Int(nullable: false),
                        AutorId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.BlogId)
                .ForeignKey("dbo.Categorias", t => t.CategoriaId)
                .ForeignKey("dbo.Usuarios", t => t.AutorId)
                .Index(t => t.CategoriaId)
                .Index(t => t.AutorId);
            
            CreateTable(
                "dbo.Categorias",
                c => new
                    {
                        CategoriaId = c.Int(nullable: false, identity: true),
                        CategoriaNome = c.String(nullable: false, maxLength: 200),
                    })
                .PrimaryKey(t => t.CategoriaId);
            
            CreateTable(
                "dbo.Comentarios",
                c => new
                    {
                        ComentarioId = c.Int(nullable: false, identity: true),
                        Corpo = c.String(),
                        CriadoEm = c.DateTime(),
                        BlogId = c.Int(nullable: false),
                        PostId = c.Int(),
                    })
                .PrimaryKey(t => t.ComentarioId)
                .ForeignKey("dbo.Usuarios", t => t.PostId)
                .ForeignKey("dbo.Blogs", t => t.BlogId)
                .Index(t => t.BlogId)
                .Index(t => t.PostId);
            
            CreateTable(
                "dbo.Usuarios",
                c => new
                    {
                        UsuarioId = c.Int(nullable: false, identity: true),
                        Email = c.String(),
                        EmailConfirmado = c.Boolean(nullable: false),
                        Senha = c.String(),
                        NomeUsuario = c.String(),
                    })
                .PrimaryKey(t => t.UsuarioId);
            
            CreateTable(
                "dbo.Perfils",
                c => new
                    {
                        PerfilId = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 150),
                    })
                .PrimaryKey(t => t.PerfilId);
            
            CreateTable(
                "dbo.UsuarioPerfis",
                c => new
                    {
                        UsuarioId = c.Int(nullable: false),
                        PerfilId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.UsuarioId, t.PerfilId })
                .ForeignKey("dbo.Usuarios", t => t.UsuarioId, cascadeDelete: true)
                .ForeignKey("dbo.Perfils", t => t.PerfilId, cascadeDelete: true)
                .Index(t => t.UsuarioId)
                .Index(t => t.PerfilId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Comentarios", "BlogId", "dbo.Blogs");
            DropForeignKey("dbo.UsuarioPerfis", "PerfilId", "dbo.Perfils");
            DropForeignKey("dbo.UsuarioPerfis", "UsuarioId", "dbo.Usuarios");
            DropForeignKey("dbo.Comentarios", "PostId", "dbo.Usuarios");
            DropForeignKey("dbo.Blogs", "AutorId", "dbo.Usuarios");
            DropForeignKey("dbo.Blogs", "CategoriaId", "dbo.Categorias");
            DropIndex("dbo.UsuarioPerfis", new[] { "PerfilId" });
            DropIndex("dbo.UsuarioPerfis", new[] { "UsuarioId" });
            DropIndex("dbo.Comentarios", new[] { "PostId" });
            DropIndex("dbo.Comentarios", new[] { "BlogId" });
            DropIndex("dbo.Blogs", new[] { "AutorId" });
            DropIndex("dbo.Blogs", new[] { "CategoriaId" });
            DropTable("dbo.UsuarioPerfis");
            DropTable("dbo.Perfils");
            DropTable("dbo.Usuarios");
            DropTable("dbo.Comentarios");
            DropTable("dbo.Categorias");
            DropTable("dbo.Blogs");
        }
    }
}
