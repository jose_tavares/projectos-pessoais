﻿using System;
using MacBlog.DAL.Modelo;

namespace MacBlog.DAL.Repositorio
{
    public interface IUnitOfWork : IDisposable
    {
        IRepositorio<Categoria> CategoriaRepositorio { get; }
        IRepositorio<Blog> BlogRepositorio { get; }
        IRepositorio<Comentario> ComentarioRepositorio { get; }
        IRepositorio<Usuario> UsuarioRepositorio { get; }
        IRepositorio<Perfil> PerfilRepositorio { get; }

        void Commit();
    }
}



