﻿using System;
using System.Web.Mvc;
using System.Web.Helpers;
using Mvc_UIHint1.Models;
using Mvc_UIHint1.Repository;
using System.ComponentModel;
using System.Linq;

namespace Mvc_UIHint1.Controllers
{
    public class ProdutosController : Controller
    {
        private readonly IProdutosRepository _produtosRepository;

        public ProdutosController(IProdutosRepository produtosRepository) =>
            _produtosRepository = produtosRepository;

        // GET: Produtos
        public ActionResult Index(bool erro = false)
        {
            var produtos = _produtosRepository.GetAll();

            var grid = new WebGrid(produtos);

            var webGrid = grid.GetHtml(tableStyle: "table table-bordered table-responsive table-hover",
                columns: grid.Columns(
                    grid.Column(nameof(Categoria) + "." + nameof(Categoria.Nome), "Nome da Categoria"),
                    grid.Column(nameof(Produto.Nome), "Nome do Produto"),
                    grid.Column(nameof(Produto.ValidadeProdutoMeses), "Validade Produtos (meses)"),
                    grid.Column(nameof(Produto.DataAquisicao), "Data Entrada")
                )
            );

            return View(Tuple.Create(webGrid, erro));
        }

        public ActionResult Create()
        {
            var model = new Produto();

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Produto model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            return RedirectToAction("Index", new { erro = !_produtosRepository.Add(model) });
        }

        public ActionResult Edit(int id)
        {
            var model = _produtosRepository.Get(id);

            if (model != null)
            {
                return View(model);
            }

            return RedirectToAction("Index", new { erro = true });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Produto model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var find = _produtosRepository.Get(model.ProdutoId);

            return RedirectToAction("Index", new { erro = !_produtosRepository.Update(find, model) });
        }

        public ActionResult Delete(int id)
        {
            return RedirectToAction("Index", new { erro = !_produtosRepository.Delete(id) });
        }
    }
}