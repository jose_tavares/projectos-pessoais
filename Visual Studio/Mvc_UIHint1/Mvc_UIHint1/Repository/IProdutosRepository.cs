﻿using Mvc_UIHint1.Models;
using System.Collections.Generic;

namespace Mvc_UIHint1.Repository
{
    public interface IProdutosRepository
    {
        IEnumerable<Produto> GetAll();

        Produto Get(int id);

        bool Add(Produto old);

        bool Update(Produto old, Produto uptate);

        bool Delete(int id);
    }
}
