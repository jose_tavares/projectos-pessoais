﻿using System;
using System.Linq;
using Mvc_UIHint1.Models;
using System.Collections.Generic;

namespace Mvc_UIHint1.Repository
{
    public class ProdutosRepository : IProdutosRepository
    {
        private readonly ProdutoDbContext _context = new ProdutoDbContext();

        public bool Add(Produto oldItem)
        {
            if (oldItem == null)
            {
                return false;
            }

            var newItem = new Produto
            {
                Nome = oldItem.Nome,
                CategoriaId = oldItem.CategoriaId,
                ValidadeProdutoMeses = oldItem.ValidadeProdutoMeses,
                DataAquisicao = oldItem.DataAquisicao
            };

            _context.Produtos.Add(newItem);
            _context.SaveChanges();

            return true;
        }

        public bool Delete(int id)
        {
            var find = Get(id);

            if (find == null)
            {
                return false;
            }

            _context.Produtos.Remove(find);
            _context.SaveChanges();

            return true;
        }

        public Produto Get(int id) => _context.Produtos.FirstOrDefault(first => first.ProdutoId == id);

        public IEnumerable<Produto> GetAll() => _context.Produtos.ToList();

        public bool Update(Produto old, Produto update)
        {
            if ((old == null) || (update == null))
            {
                return false;
            }

            old.Nome = update.Nome;
            old.CategoriaId = update.CategoriaId;
            old.ValidadeProdutoMeses = update.ValidadeProdutoMeses;
            old.DataAquisicao = update.DataAquisicao;

            _context.SaveChanges();

            return true;
        }
    }
}