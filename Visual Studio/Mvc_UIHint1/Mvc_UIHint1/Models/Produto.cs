﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mvc_UIHint1.Models
{
    [Table("Produtos")]
    public class Produto
    {
        public int ProdutoId { get; set; }

        [DisplayName("Nome do Produto")]
        public string Nome { get; set; }

        [DisplayName("Categoria")]
        [UIHint("ListaCategoria")]
        public int CategoriaId { get; set; }

        [UIHint("ListaMeses")]
        [DataType(DataType.Duration)]
        [DisplayName("Validade Produtos (meses)")]
        public decimal ValidadeProdutoMeses { get; set; }

        [UIHint("Calendario")]
        [DisplayName("Data Entrada")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? DataAquisicao { get; set; }

        public virtual Categoria Categoria { get; set; }
    }
}