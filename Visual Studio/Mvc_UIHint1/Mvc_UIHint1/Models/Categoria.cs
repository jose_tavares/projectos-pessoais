﻿using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mvc_UIHint1.Models
{
    [Table("Categorias")]
    public class Categoria
    {
        public int CategoriaId { get; set; }

        [DisplayName("Nome da Categoria")]
        public string Nome { get; set; }

        public List<Produto> Produtos { get; } = new List<Produto>();
    }
}