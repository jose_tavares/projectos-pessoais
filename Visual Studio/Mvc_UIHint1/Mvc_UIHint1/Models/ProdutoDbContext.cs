﻿using System.Data.Entity;

namespace Mvc_UIHint1.Models
{
    public class ProdutoDbContext : DbContext
    {
        public DbSet<Produto> Produtos { get; set; }
        public DbSet<Categoria> Categorias { get; set; }

        public ProdutoDbContext() : base("ProdutoDbContext") { }
    }
}