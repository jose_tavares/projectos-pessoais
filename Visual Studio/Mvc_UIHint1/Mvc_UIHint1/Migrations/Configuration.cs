﻿namespace Mvc_UIHint1.Migrations
{
    using Mvc_UIHint1.Models;
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<ProdutoDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ProdutoDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method
            //  to avoid creating duplicate seed data.
            context.Categorias.AddOrUpdate(addOrUpdate => addOrUpdate.CategoriaId,
                new Categoria { CategoriaId = 1, Nome = "Bebidas" },
                new Categoria { CategoriaId = 2, Nome = "Condimentos" },
                new Categoria { CategoriaId = 3, Nome = "Acessórios" },
                new Categoria { CategoriaId = 4, Nome = "Refrigerantes" }
            );
        }
    }
}
