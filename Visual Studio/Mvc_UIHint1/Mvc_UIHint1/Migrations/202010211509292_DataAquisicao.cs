﻿namespace Mvc_UIHint1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DataAquisicao : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Produtos", "DataAquisicao", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Produtos", "DataAquisicao");
        }
    }
}
