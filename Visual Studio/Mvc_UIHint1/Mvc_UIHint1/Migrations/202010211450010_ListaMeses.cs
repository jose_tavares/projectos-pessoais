﻿namespace Mvc_UIHint1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ListaMeses : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Produtos", "ValidadeProdutoMeses", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Produtos", "ValidadeProdutoMeses");
        }
    }
}
