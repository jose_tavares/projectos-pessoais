﻿using PostcodesSearch.MVC.Models;
using System.Collections.Generic;
using System.Net.Http;

namespace PostcodesSearch.MVC.DependencyInjection
{
    public class PostcodeService : IPostcodeService
    {
        private readonly HttpClient client = new HttpClient();

        public bool AddPostcode(string id)
        {
            var postTask = client.PostAsJsonAsync("http://localhost:20811/api/postcode", id);
            postTask.Wait();

            var result = postTask.Result;

            if (result.IsSuccessStatusCode)
            {
                return true;
            }

            return false;
        }

        public PostcodeModel GetCoordinatesByPostCode(string id)
        {
            var responseTask = client.GetAsync("http://localhost:20811/api/postcode?id=" + id);
            responseTask.Wait();

            var result = responseTask.Result;

            if (result.IsSuccessStatusCode)
            {
                var readTask = result.Content.ReadAsAsync<PostcodeModel>();
                readTask.Wait();

                return readTask.Result;
            }

            return new PostcodeModel();
        }

        public IEnumerable<PostcodeModel> GetPostcodesId()
        {
            var responseTask = client.GetAsync("http://localhost:20811/api/postcode/id");
            responseTask.Wait();

            var result = responseTask.Result;

            if (result.IsSuccessStatusCode)
            {
                var readTask = result.Content.ReadAsAsync<IEnumerable<PostcodeModel>>();
                readTask.Wait();

                return readTask.Result;
            }

            return null;
        }
    }
}
