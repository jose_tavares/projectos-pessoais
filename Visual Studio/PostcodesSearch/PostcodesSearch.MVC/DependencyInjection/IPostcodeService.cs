﻿using PostcodesSearch.MVC.Models;
using System.Collections.Generic;

namespace PostcodesSearch.MVC.DependencyInjection
{
    public interface IPostcodeService
    {
        PostcodeModel GetCoordinatesByPostCode(string id);

        IEnumerable<PostcodeModel> GetPostcodesId();

        bool AddPostcode(string id);
    }
}
