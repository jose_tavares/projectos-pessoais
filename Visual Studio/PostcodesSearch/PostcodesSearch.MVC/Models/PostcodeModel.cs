﻿namespace PostcodesSearch.MVC.Models
{
    public class PostcodeModel
    {
        public string Id { get; set; }

        public double Longitude { get; set; }

        public double Latitude { get; set; }
    }
}
