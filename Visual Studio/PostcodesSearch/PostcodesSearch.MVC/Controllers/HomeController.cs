﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using PostcodesSearch.MVC.DependencyInjection;
using System.Linq;

namespace PostcodesSearch.MVC.Controllers
{
    public class HomeController : Controller
    {
        private readonly IPostcodeService postcodeService;

        public HomeController(IPostcodeService postcodeService) =>
            this.postcodeService = postcodeService;

        [HttpGet]
        public IActionResult Index()
        {
            var postcodesId = postcodeService.GetPostcodesId();

            var model = ((postcodesId != null) && postcodesId.Any()) ?
                postcodesId.Select(postcodeId => new SelectListItem
                {
                    Text = postcodeId.Id,
                    Value = postcodeId.Id
                }) :
                null;

            return View(model);
        }

        [HttpGet]
        public JsonResult GetCoordinates(string id)
        {
            var postcode = postcodeService.GetCoordinatesByPostCode(id);

            if ((postcode.Latitude > 0) && (postcode.Longitude > 0))
            {
                postcodeService.AddPostcode(id);
            }

            return new JsonResult(new
            {
                postcode.Latitude,
                postcode.Longitude
            });
        }
    }
}
