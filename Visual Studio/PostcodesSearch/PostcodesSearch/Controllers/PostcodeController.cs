﻿using Microsoft.AspNetCore.Mvc;
using PostcodesSearch.DependencyInjection;
using PostcodesSearch.DTOs;
using PostcodesSearch.Models;
using System.Collections.Generic;
using System.Net;
using System.Text.Json;

namespace PostcodesSearch.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PostcodeController : ControllerBase
    {
        private readonly IPostcodeRepository postcodeService;

        public PostcodeController(IPostcodeRepository postcodeService) =>
            this.postcodeService = postcodeService;

        [HttpGet]
        public ActionResult<PostcodeResultDTO> Get(string id)
        {
            var postcodeDTO = postcodeService.GetPostcodeById(id);

            return postcodeDTO.status switch
            {
                (int)HttpStatusCode.BadRequest => BadRequest(postcodeDTO.error),
                (int)HttpStatusCode.NotFound => NotFound(postcodeDTO.error),
                _ => Ok(postcodeDTO.result)
            };
        }

        [HttpGet]
        [Route("id")]
        public ActionResult<IEnumerable<PostcodeModel>> Get() =>
            Ok(postcodeService.GetPostcodesId());

        [HttpPost]
        public ActionResult<PostcodeModel> Post(JsonElement id)
        {
            var isAdded = postcodeService.AddPostcode(id.ToString());

            if (isAdded)
            {
                return Ok($"Postcode {id} added");
            }

            return BadRequest();
        }
    }
}
