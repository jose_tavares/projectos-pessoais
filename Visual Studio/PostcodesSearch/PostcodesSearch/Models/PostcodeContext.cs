﻿using Microsoft.EntityFrameworkCore;

namespace PostcodesSearch.Models
{
    public class PostcodeContext : DbContext
    {
        public DbSet<PostcodeModel> PostcodeModels { get; set; }

        public PostcodeContext() : base() { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder
                    .UseLazyLoadingProxies()
                    .UseSqlServer("Data Source=DESKTOP-8473561;Initial Catalog=PostcodeDB;Integrated Security=True");
            }
        }
    }
}
