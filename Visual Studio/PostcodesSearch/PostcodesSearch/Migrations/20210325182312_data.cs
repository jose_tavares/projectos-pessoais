﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PostcodesSearch.Migrations
{
    public partial class data : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("INSERT INTO PostcodeModels VALUES ('N76RS');" +
                "INSERT INTO PostcodeModels VALUES ('SW46TA');" +
                "INSERT INTO PostcodeModels VALUES ('W1B3AG');" +
                "INSERT INTO PostcodeModels VALUES ('PO63TD');");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DELETE FROM PostcodeModels");
        }
    }
}
