﻿using PostcodesSearch.DTOs;
using PostcodesSearch.Models;
using System.Collections.Generic;

namespace PostcodesSearch.DependencyInjection
{
    public interface IPostcodeRepository
    {
        PostcodeDTO GetPostcodeById(string id);

        IEnumerable<PostcodeModel> GetPostcodesId();

        bool AddPostcode(string postcodeId);
    }
}
