﻿using Newtonsoft.Json;
using PostcodesSearch.DTOs;
using PostcodesSearch.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;

namespace PostcodesSearch.DependencyInjection
{
    public class PostcodeRepository : IPostcodeRepository
    {
        private readonly PostcodeContext context = new PostcodeContext();

        public bool AddPostcode(string postcodeid)
        {
            try
            {
                context.PostcodeModels.Add(new PostcodeModel { Id = postcodeid });
                return context.SaveChanges() != 0;
            }

            catch { return false; }

        }

        public PostcodeDTO GetPostcodeById(string id)
        {
            var client = new HttpClient()
            {
                BaseAddress = new Uri("http://api.postcodes.io/"),
            };

            var responseTask = client.GetAsync("postcodes/" + id);
            responseTask.Wait();

            var readTask = responseTask.Result.Content.ReadAsStringAsync();
            readTask.Wait();

            return JsonConvert.DeserializeObject<PostcodeDTO>(readTask.Result);
        }

        public IEnumerable<PostcodeModel> GetPostcodesId() =>
            context.PostcodeModels;
    }
}
