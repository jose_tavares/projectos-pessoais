import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Postcode } from './PostcodeModel';

@Injectable({
  providedIn: 'root',
})
export class AppService {
  rootURL = 'http://localhost:4599/api/postcode?id=';

  constructor(private http: HttpClient) {}

  getData(id: string): Observable<Postcode> {
    return this.http.get<Postcode>(this.rootURL + id);
  }
}
