import { Component, OnInit } from '@angular/core';
import { AppService } from './app.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Postcode } from './PostcodeModel';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})

export class AppComponent implements OnInit {
  title = 'Postcodes Search';

  constructor(private AppService: AppService) { }

  postcode: Postcode;
  PostcodeForm: FormGroup;
  submitted = false;

  ngOnInit(): void {
    this.PostcodeForm = new FormGroup({
      id: new FormControl('', [Validators.required])
    });

    this.postcode = new Postcode();
  }

  Send() {
    this.submitted = true;
    if (this.PostcodeForm.invalid) {
      return;
    }

    this.AppService.getData(this.PostcodeForm.value.id).subscribe((result) => {
      this.postcode = result;
    });
  }
}
