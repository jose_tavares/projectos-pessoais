export class Postcode {
    id: string;
    latitude: number;
    longitude: number;
}