﻿namespace PostcodesSearch.DTOs
{
    public class PostcodeDTO
    {
        public int status { get; set; }

        public string error { get; set; }

        public PostcodeResultDTO result { get; set; }
    }
}
