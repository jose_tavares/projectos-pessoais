﻿namespace PostcodesSearch.DTOs
{
    public class PostcodeResultDTO
    {
        public double longitude { get; set; }

        public double latitude { get; set; }
    }
}
