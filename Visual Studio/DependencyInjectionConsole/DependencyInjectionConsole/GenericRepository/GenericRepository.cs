﻿using System;

namespace DependencyInjectionConsole.GenericRepository
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        public void Add(T obj) => Console.WriteLine("Adicionando objeto.");
    }
}
