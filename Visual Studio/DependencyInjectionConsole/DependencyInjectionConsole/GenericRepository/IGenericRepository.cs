﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DependencyInjectionConsole.GenericRepository
{
    public interface IGenericRepository<T> where T : class
    {
        void Add(T obj);
    }
}
