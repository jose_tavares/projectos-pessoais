﻿namespace DependencyInjectionConsole.Services
{
    public class ServicoB : IServico
    {
        public string Servico() => "Serviço B";
    }
}
