﻿using System;

namespace DependencyInjectionConsole.Services
{
    public class ServicoA : IServico
    {
        public string Servico() => "Serviço A";
    }
}
