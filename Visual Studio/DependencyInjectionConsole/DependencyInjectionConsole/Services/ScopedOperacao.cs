﻿using System;

namespace DependencyInjectionConsole.Services
{
    public class ScopedOperacao
    {
        public ScopedOperacao() => Console.WriteLine("Serviço Scoped foi criado.");
    }
}
