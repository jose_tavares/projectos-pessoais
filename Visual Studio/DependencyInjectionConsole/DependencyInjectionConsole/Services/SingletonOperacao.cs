﻿using System;

namespace DependencyInjectionConsole.Services
{
    public class SingletonOperacao
    {
        public SingletonOperacao() => Console.WriteLine("Serviço Singleton foi criado.");
    }
}
