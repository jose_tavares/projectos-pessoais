﻿using System;

namespace DependencyInjectionConsole.Services
{
    public class TransientOperacao
    {
        public TransientOperacao() => Console.WriteLine("Serviço Transient foi criado.");
    }
}
