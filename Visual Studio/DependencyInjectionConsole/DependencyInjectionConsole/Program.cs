﻿using System;
using DependencyInjectionConsole.GenericRepository;
using DependencyInjectionConsole.Models;
using DependencyInjectionConsole.Services;
using Microsoft.Extensions.DependencyInjection;

namespace DependencyInjectionConsole
{
    class Program
    {
        private static IServiceProvider _serviceProvider;

        static void Main(string[] args)
        {
            //MacorattiNet_Page_1();

            //MacorattiNet_Page_2_Demo2();

            //RegistroInstanciaDemo();

            //RegistroGenerico();

            //MultiploRegistro();

            MultiploRegistro2();
            Console.ReadLine();
        }

        private static void RegisterServices()
        {
            var collection = new ServiceCollection()
                .AddScoped<ITesteService, TesteService>()
                .AddScoped<IDemoService, DemoService>();

            _serviceProvider = collection.BuildServiceProvider();
        }

        private static void DisposeServices()
        {
            if (_serviceProvider == null)
            {
                return;
            }

            if (_serviceProvider is IDisposable)
            {
                ((IDisposable)_serviceProvider).Dispose();
            }
        }

        private static void MacorattiNet_Page_1()
        {
            RegisterServices();

            Console.WriteLine("Iniciando Aplicação");
            var teste = _serviceProvider.GetService<ITesteService>();
            teste.ServicoTeste();

            Console.WriteLine("Operação Concluída");
            Console.ReadLine();
            DisposeServices();
        }

        private static void MacorattiNet_Page_2_Demo1()
        {
            var services = new ServiceCollection();

            services.AddTransient<TransientOperacao>();
            services.AddScoped<ScopedOperacao>();
            services.AddSingleton<SingletonOperacao>();

            var serviceProvider = services.BuildServiceProvider();

            Console.WriteLine("-------- Primeiro Request --------\n");

            var transientService = serviceProvider.GetService<TransientOperacao>();
            var scopedService = serviceProvider.GetService<ScopedOperacao>();
            var singletonService = serviceProvider.GetService<SingletonOperacao>();

            Console.WriteLine();
            Console.WriteLine("-------- Segundo Request --------\n");

            var transientService2 = serviceProvider.GetService<TransientOperacao>();
            var scopedService2 = serviceProvider.GetService<ScopedOperacao>();
            var singletonService2 = serviceProvider.GetService<SingletonOperacao>();

            Console.WriteLine();
            Console.WriteLine(new String('-', 30));
            Console.ReadLine();
        }

        private static void MacorattiNet_Page_2_Demo2()
        {
            var services = new ServiceCollection();

            services.AddTransient<TransientOperacao>();
            services.AddScoped<ScopedOperacao>();
            services.AddSingleton<SingletonOperacao>();

            var serviceProvider = services.BuildServiceProvider();

            Console.WriteLine();
            Console.WriteLine("-------- Primeiro Request --------");
            using (var scope = serviceProvider.CreateScope())
            {
                var transientService = scope.ServiceProvider.GetService<TransientOperacao>();
                var scopedService = scope.ServiceProvider.GetService<ScopedOperacao>();
                var singletonService = scope.ServiceProvider.GetService<SingletonOperacao>();
            }

            Console.WriteLine();
            Console.WriteLine("-------- Segundo Request --------");
            using (var scope = serviceProvider.CreateScope())
            {
                var transientService = scope.ServiceProvider.GetService<TransientOperacao>();
                var scopedService = scope.ServiceProvider.GetService<ScopedOperacao>();
                var singletonService = scope.ServiceProvider.GetService<SingletonOperacao>();
            }

            Console.WriteLine();
            Console.WriteLine("-----------------------------");
        }

        private static void RegistroInstanciaDemo()
        {
            var instancia = new MinhaInstancia
            {
                Valor = 44
            };

            var services = new ServiceCollection();

            services.AddSingleton(instancia);

            foreach (var service in services)
            {
                if (service.ServiceType == typeof(MinhaInstancia))
                {
                    var instanciaRegistada = (MinhaInstancia)service.ImplementationInstance;

                    Console.WriteLine("Instância Registada: " + instanciaRegistada.Valor);
                }
            }

            var serviceProvider = services.BuildServiceProvider();
            var minhaInstancia = serviceProvider.GetService<MinhaInstancia>();

            Console.WriteLine("Serviço Registado pelo registo de instância: " + minhaInstancia.Valor);
            Console.ReadLine();
        }

        private static void RegistroGenerico()
        {
            var cliente = new Cliente
            {
                Nome = "Macoratti"
            };

            var services = new ServiceCollection();
            services.AddTransient(typeof(IGenericRepository<>), typeof(GenericRepository<>));

            var serviceProvider = services.BuildServiceProvider();
            var servico = serviceProvider.GetService<IGenericRepository<Cliente>>();

            servico.Add(cliente);
            Console.ReadLine();
        }

        private static void MultiploRegistro()
        {
            var servicos = new ServiceCollection();

            servicos.AddTransient<IServico, ServicoA>();
            servicos.AddTransient<IServico, ServicoB>();

            var serviceProvider = servicos.BuildServiceProvider();

            var meusServicos = serviceProvider.GetServices<IServico>();
            var meuServico = serviceProvider.GetService<IServico>();

            Console.WriteLine("----- Serviços -----------");

            foreach (var servico in meusServicos)
            {
                Console.WriteLine(servico.Servico());
            }

            Console.WriteLine("----- Serviço ------------");
            Console.WriteLine(meuServico.Servico());

            Console.ReadLine();
        }

        //???
        private static void MultiploRegistro2()
        {
            var servicos = new ServiceCollection();
            servicos.AddTransient<IServico, ServicoA>();
            servicos.AddTransient<IServico, ServicoB>();

            servicos.AddTransient<Func<string, IServico>>(serviceProvider => key =>
            {
                switch (key)
                {
                    case "Servico A":
                        return serviceProvider.GetService<ServicoA>();
                    case "Servico B":
                        return serviceProvider.GetService<ServicoB>();
                    default:
                        return null;
                }
            });
        }
    }
}
