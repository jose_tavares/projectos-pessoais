using NUnit.Framework;
using System.Reflection;
using System;
using Bosch.Exercise.ConsoleApp;

namespace Bosch.Exercise.TestProject
{
    public class ZipFolderTests
    {
        private ZipFolder zipFolder;

        [SetUp]
        public void Setup()
        {
            zipFolder = ZipFolder.GetInstance();
        }

        #region Create
        [Test]
        public void Create_WithValidInput_CreatesLocalFileArchive()
        {
            string
                sourceFolderPath = "C:\\temp",                                      //  - a pasta a zipar(e.g.C:\\temp)
                zipFileName = "final.zip",                                          //  - o nome final do ficheiro zip(e.g.final.zip)
                outputType = "localFile";                                           //  - tipo de output(e.g.localFile, filesShare, SMTP)

            string[]
                excludedExtensions = { ".bin" },                                    //  - uma lista de extens�es a excluir(e.g. .bmp, .jpg, .txt)
                excludedDirectories = { "git" },                                    //  - uma lista de diret�rios a excluir(e.g.git, diret�rio)
                excludedFiles = { "file2" },                                        //  - uma lista de ficheiros a excluir(e.g.ficheiro1, filcheiro2)
                outputArguments = { "C:" };                                         // localFile     

            //  - par�metros opcionais de acordo com o tipo de output(e.g.path do fileshare)

            Assert.DoesNotThrow(() =>
                zipFolder.Create
                    (sourceFolderPath,
                    zipFileName,
                    excludedExtensions,
                    excludedDirectories,
                    excludedFiles,
                    outputType,
                    outputArguments)
                );
        }

        [Test]
        public void Create_WithValidInput_CreatesArchiveForSMTP()
        {
            string
                sourceFolderPath = "C:\\temp",                                 //  - a pasta a zipar(e.g.C:\\temp)
                zipFileName = "final.zip",                                     //  - o nome final do ficheiro zip(e.g.final.zip)
                outputType = "smtp";                                           //  - tipo de output(e.g.localFile, filesShare, SMTP)

            string[]
                excludedExtensions = { ".bin" },                               //  - uma lista de extens�es a excluir(e.g. .bmp, .jpg, .txt)
                excludedDirectories = { "git" },                               //  - uma lista de diret�rios a excluir(e.g.git, diret�rio)
                excludedFiles = { "file2" },                                   //  - uma lista de ficheiros a excluir(e.g.ficheiro1, filcheiro2)
                outputArguments = { "smtp.office365.com", "source@outlook.pt", "Test123", "destination@gmail.com" };    // smtp     

            //  - par�metros opcionais de acordo com o tipo de output(e.g.path do fileshare)
            //  For SMTP: {[Host],[Sender Email],[Sender Password],[Recipient Email]}.
            //  Example {smtp.office365.com,source@outlook.pt,Test123,destination@gmail.com}

            Assert.DoesNotThrow(() =>
                zipFolder.Create
                    (sourceFolderPath,
                    zipFileName,
                    excludedExtensions,
                    excludedDirectories,
                    excludedFiles,
                    outputType,
                    outputArguments)
                );
        }

        [Test]
        public void Create_WithInvalidOutputType_HandlesException()
        {
            // Arrange
            string sourceFolderPath = "C:\\Users\\jpbta\\Desktop\\temp";
            string zipFileName = "output.zip";
            string[] excludedExtensions = { ".tmp" };
            string[] excludedDirectories = { "excludeThis" };
            string[] excludedFiles = { "file.txt" };
            string outputType = "invalidOutputType"; // Invalid output type
            string[] outputArguments = { "outputPath" };

            // Act and Assert
            // Verify that an exception is thrown
            Assert.Throws<Exception>(() =>
                zipFolder.Create(
                    sourceFolderPath,
                    zipFileName,
                    excludedExtensions,
                    excludedDirectories,
                    excludedFiles,
                    outputType,
                    outputArguments)
                );
        }
        #endregion

        #region Private Methods
        #region IsExtensionExcluded
        [Test]
        public void IsExtensionExcluded_WhenExtensionNotInExcludedExtensions_ReturnsFalse()
        {
            // Arrange
            string fileExtension = ".txt";
            string[] excludedExtensions = { ".jpg", ".png" };

            // Act
            MethodInfo methodInfo = typeof(ZipFolder).GetMethod("IsExtensionExcluded", BindingFlags.NonPublic | BindingFlags.Instance);
            bool result = (bool)methodInfo.Invoke(zipFolder, new object[] { fileExtension, excludedExtensions });

            // Assert
            Assert.That(result, Is.False);
        }

        [Test]
        public void IsExtensionExcluded_WhenExtensionInExcludedExtensions_ReturnsTrue()
        {
            // Arrange
            string fileExtension = ".jpg";
            string[] excludedExtensions = { ".jpg", ".png" };

            // Act
            MethodInfo methodInfo = typeof(ZipFolder).GetMethod("IsExtensionExcluded", BindingFlags.NonPublic | BindingFlags.Instance);
            bool result = (bool)methodInfo.Invoke(zipFolder, new object[] { fileExtension, excludedExtensions });

            // Assert
            Assert.That(result, Is.True);
        }

        [Test]
        public void IsExtensionExcluded_WithEmptyExcludedExtensions_ReturnsFalse()
        {
            // Arrange
            string fileExtension = ".txt";
            string[] excludedExtensions = { }; // Empty

            // Act
            MethodInfo methodInfo = typeof(ZipFolder).GetMethod("IsExtensionExcluded", BindingFlags.NonPublic | BindingFlags.Instance);
            bool result = (bool)methodInfo.Invoke(zipFolder, new object[] { fileExtension, excludedExtensions });

            // Assert
            Assert.That(result, Is.False);
        }

        [Test]
        public void IsExtensionExcluded_WithNullExcludedExtensions_ReturnsFalse()
        {
            // Arrange
            string fileExtension = ".txt";
            string[] excludedExtensions = null;

            // Act
            MethodInfo methodInfo = typeof(ZipFolder).GetMethod("IsExtensionExcluded", BindingFlags.NonPublic | BindingFlags.Instance);
            bool result = (bool)methodInfo.Invoke(zipFolder, new object[] { fileExtension, excludedExtensions });

            // Assert
            Assert.That(result, Is.False);
        }
        #endregion

        #region IsDirectoryExcluded
        [Test]
        public void IsDirectoryExcluded_WhenDirectoryNotInExcludedDirectories_ReturnsFalse()
        {
            // Arrange
            string relativePath = "path\\to\\directory";
            string[] excludedDirectories = { "folder1", "folder2" };

            // Act
            MethodInfo methodInfo = typeof(ZipFolder).GetMethod("IsDirectoryExcluded", BindingFlags.NonPublic | BindingFlags.Instance);
            bool result = (bool)methodInfo.Invoke(zipFolder, new object[] { relativePath, excludedDirectories });

            // Assert
            Assert.That(result, Is.False);
        }

        [Test]
        public void IsDirectoryExcluded_WhenDirectoryInExcludedDirectories_ReturnsTrue()
        {
            // Arrange
            string relativePath = "path\\to\\folder1";
            string[] excludedDirectories = { "folder1", "folder2" };

            // Act
            MethodInfo methodInfo = typeof(ZipFolder).GetMethod("IsDirectoryExcluded", BindingFlags.NonPublic | BindingFlags.Instance);
            bool result = (bool)methodInfo.Invoke(zipFolder, new object[] { relativePath, excludedDirectories });

            // Assert
            Assert.That(result, Is.True);
        }

        [Test]
        public void IsDirectoryExcluded_WithEmptyExcludedDirectories_ReturnsFalse()
        {
            // Arrange
            string relativePath = "path\\to\\folder1";
            string[] excludedDirectories = { };

            // Act
            MethodInfo methodInfo = typeof(ZipFolder).GetMethod("IsDirectoryExcluded", BindingFlags.NonPublic | BindingFlags.Instance);
            bool result = (bool)methodInfo.Invoke(zipFolder, new object[] { relativePath, excludedDirectories });

            // Assert
            Assert.That(result, Is.False);
        }

        [Test]
        public void IsDirectoryExcluded_WithNullExcludedDirectories_ReturnsFalse()
        {
            // Arrange
            string relativePath = "path\\to\\folder1";
            string[] excludedDirectories = null;

            // Act
            MethodInfo methodInfo = typeof(ZipFolder).GetMethod("IsDirectoryExcluded", BindingFlags.NonPublic | BindingFlags.Instance);
            bool result = (bool)methodInfo.Invoke(zipFolder, new object[] { relativePath, excludedDirectories });

            // Assert
            Assert.That(result, Is.False);
        }
        #endregion

        #region IsFileExcluded
        [Test]
        public void IsFileExcluded_WhenFileNotInExcludedFiles_ReturnsFalse()
        {
            // Arrange
            string fileName = "file.txt";
            string[] excludedFiles = { "file2.txt", "file3.txt" };

            // Act
            MethodInfo methodInfo = typeof(ZipFolder).GetMethod("IsFileExcluded", BindingFlags.NonPublic | BindingFlags.Instance);
            bool result = (bool)methodInfo.Invoke(zipFolder, new object[] { fileName, excludedFiles });

            // Assert
            Assert.That(result, Is.False);
        }

        [Test]
        public void IsFileExcluded_WhenFileInExcludedFiles_ReturnsTrue()
        {
            // Arrange
            string fileName = "file.txt";
            string[] excludedFiles = { "file.txt", "file3.txt" };

            // Act
            MethodInfo methodInfo = typeof(ZipFolder).GetMethod("IsFileExcluded", BindingFlags.NonPublic | BindingFlags.Instance);
            bool result = (bool)methodInfo.Invoke(zipFolder, new object[] { fileName, excludedFiles });

            // Assert
            Assert.That(result, Is.True);
        }

        [Test]
        public void IsFileExcluded_WithEmptyExcludedFiles_ReturnsFalse()
        {
            // Arrange
            string fileName = "file.txt";
            string[] excludedFiles = { }; // Empty

            // Act
            MethodInfo methodInfo = typeof(ZipFolder).GetMethod("IsFileExcluded", BindingFlags.NonPublic | BindingFlags.Instance);
            bool result = (bool)methodInfo.Invoke(zipFolder, new object[] { fileName, excludedFiles });

            // Assert
            Assert.That(result, Is.False);
        }

        [Test]
        public void IsFileExcluded_WithNullExcludedFiles_ReturnsFalse()
        {
            // Arrange
            string fileName = "file.txt";
            string[] excludedFiles = null;

            // Act
            MethodInfo methodInfo = typeof(ZipFolder).GetMethod("IsFileExcluded", BindingFlags.NonPublic | BindingFlags.Instance);
            bool result = (bool)methodInfo.Invoke(zipFolder, new object[] { fileName, excludedFiles });

            // Assert
            Assert.That(result, Is.False);
        }
        #endregion
        #endregion
    }
}