﻿using System;

namespace Bosch.Exercise.ConsoleApp
{
    internal class Program
    {
        //  Como utilizador posso invocar a aplicação via linha de comandos passando como argumentos:
        //  - a pasta a zipar(e.g.C:\\temp)
        //  - o nome final do ficheiro zip(e.g.final.zip)
        //  - uma lista de extensões a excluir(e.g. .bmp, .jpg, .txt)
        //  - uma lista de diretórios a excluir(e.g.git, diretório)
        //  - uma lista de ficheiros a excluir(e.g.ficheiro1, filcheiro2)
        //  - tipo de output(e.g.localFile, filesShare, SMTP)
        //      DUVIDA -> localFile e fileShare não tem o mesmo comportamento dando path?
        //  - parâmetros opcionais de acordo com o tipo de output(e.g.path do fileshare)

        static void Main(string[] args)
        {
            string
                sourceFolderPath = args[0],                 //  - a pasta a zipar(e.g.C:\\temp)
                zipFileName = args[1],                      //  - o nome final do ficheiro zip(e.g.final.zip)
                outputType = args[5];                       //  - tipo de output(e.g.localFile, filesShare, SMTP)

            string[]
                excludedExtensions = args[2].Split(','),    //  - uma lista de extensões a excluir(e.g. .bmp, .jpg, .txt)
                excludedDirectories = args[3].Split(','),   //  - uma lista de diretórios a excluir(e.g.git, diretório)
                excludedFiles = args[4].Split(','),         //  - uma lista de ficheiros a excluir(e.g.ficheiro1, filcheiro2)
                outputArguments = args[6].Split(',');       //  - parâmetros opcionais de acordo com o tipo de output(e.g.path do fileshare)

            try
            {
                ZipFolder
                    .GetInstance()
                    .Create
                        (sourceFolderPath,
                        zipFileName,
                        excludedExtensions,
                        excludedDirectories,
                        excludedFiles,
                        outputType,
                        outputArguments);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"An error occurred: {ex.Message}");
            }
        }
    }
}
