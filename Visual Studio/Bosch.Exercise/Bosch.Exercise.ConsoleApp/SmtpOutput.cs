﻿using System;
using System.Net.Mail;
using System.Net;
using System.IO;

namespace Bosch.Exercise.ConsoleApp
{
    internal class SmtpOutput
    {
        #region Public Properties
        public string Host { private get; set; }

        public string SenderEmail { private get; set; }

        public string SenderPassword { private get; set; }

        public string RecipientEmail { private get; set; }

        public string AttachmentFilePath { private get; set; }
        #endregion

        public void SendEmailWithAttachment()
        {
            SmtpClient smtpClient = new SmtpClient(Host)
            {
                Port = 587,
                Credentials = new NetworkCredential(SenderEmail, SenderPassword),
                EnableSsl = true
            };

            MailMessage mailMessage = new MailMessage(SenderEmail, RecipientEmail)
            {
                Subject = "Zip Attachment",
                Body = "Dear User,\n\nYou receive a ZIP attachment.\n\nBest regards.",
                IsBodyHtml = false
            };

            if (!string.IsNullOrEmpty(AttachmentFilePath) && File.Exists(AttachmentFilePath))
            {
                Attachment attachment = new Attachment(AttachmentFilePath);
                mailMessage.Attachments.Add(attachment);
            }

            try
            {
                smtpClient.Send(mailMessage);
                Console.WriteLine("Email sent successfully.");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error sending email: " + ex.Message);
            }
        }
    }
}
