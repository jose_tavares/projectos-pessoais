﻿using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.IO;
using System.Linq;
using System.Text;

namespace Bosch.Exercise.ConsoleApp
{
    public class ZipFolder
    {
        #region Private Variables
        private static ZipFolder instance = null;

        private static readonly object lockObject = new object();
        #endregion

        #region Private Constructor
        private ZipFolder() { }
        #endregion

        #region Public Methods
        public static ZipFolder GetInstance()
        {
            if (instance == null)
            {
                lock (lockObject)
                {
                    instance ??= new ZipFolder();
                }
            }

            return instance;
        }

        public void Create
            (string sourceFolderPath,
            string zipFileName,
            string[] excludedExtensions,
            string[] excludedDirectories,
            string[] excludedFiles,
            string outputType,
            string[] outputArguments)
        {
            if (!outputType.ToLower().Equals("localfile") && !outputType.ToLower().Equals("filesshare") && !outputType.ToLower().Equals("smtp"))
            {
                throw new Exception("Invalid output type specified.");
            }

            DirectoryInfo directoryInfo = new DirectoryInfo(sourceFolderPath);

            string archiveFileName = outputType.ToLower().Equals("localfile")
                ? outputArguments[0] + "\\" + zipFileName
                : directoryInfo.Parent.FullName + "\\" + zipFileName;

            if (File.Exists(archiveFileName))
            {
                File.Delete(archiveFileName);
            }

            foreach (string file in Directory.GetFiles(sourceFolderPath, "*.*", SearchOption.AllDirectories))
            {
                string fileName = Path.GetFileName(file);
                string fileExtension = Path.GetExtension(file);
                string relativeSourcePath = Path.GetRelativePath(sourceFolderPath, file);

                bool isExtensionExcluded = IsExtensionExcluded(fileExtension, excludedExtensions);
                bool isDirectoryExcluded = IsDirectoryExcluded(relativeSourcePath, excludedDirectories);
                bool isFileExcluded = IsFileExcluded(fileName, excludedFiles);

                if (!isExtensionExcluded && !isDirectoryExcluded && !isFileExcluded)
                {
                    Localfile(archiveFileName, relativeSourcePath, file);
                }
            }

            if (outputType.ToLower().Equals("smtp"))
            {
                Smtp(archiveFileName, outputArguments);
            }
        }
        #endregion

        #region Private Methods    
        #region Exclusions
        private bool IsExtensionExcluded(string fileExtension, string[] excludedExtensions)
        {
            if (excludedExtensions == null)
            {
                return false;
            }

            return excludedExtensions.Contains(fileExtension, StringComparer.OrdinalIgnoreCase);
        }

        private bool IsDirectoryExcluded(string relativePath, string[] excludedDirectories)
        {
            bool found = false;

            if (excludedDirectories == null)
            {
                return found;
            }

            string[] directories = relativePath.Split("\\");

            foreach (string directory in directories.Where(where => !where.Contains('.')))
            {
                if (excludedDirectories.Contains(directory, StringComparer.OrdinalIgnoreCase))
                {
                    found = true;
                    break;
                }
            }

            return found;
        }

        private bool IsFileExcluded(string fileName, string[] excludedFiles)
        {
            bool found = false;

            if (excludedFiles == null)
            {
                return found;
            }

            foreach (string excludedFile in excludedFiles)
            {
                if (fileName.Contains(excludedFile, StringComparison.OrdinalIgnoreCase))
                {
                    found = true;
                    break;
                }
            }

            return found;
        }
        #endregion

        #region Output
        private void Localfile(string archiveFileName, string relativeSourcePath, string file)
        {
            using ZipArchive archive = ZipFile.Open(archiveFileName, ZipArchiveMode.Create);

            // Create an entry in the zip archive with the relative path
            ZipArchiveEntry entry = archive.CreateEntry(relativeSourcePath);

            // Add the file to the zip archive
            using Stream entryStream = entry.Open();
            using FileStream fileStream = File.OpenRead(file);

            fileStream.CopyTo(entryStream);
        }

        private void Smtp(string archiveFileName, string[] outputArguments)
        {
            SmtpOutput smtpOutput = new SmtpOutput
            {
                Host = outputArguments[0],
                SenderEmail = outputArguments[1],
                SenderPassword = outputArguments[2],
                RecipientEmail = outputArguments[3],
                AttachmentFilePath = archiveFileName
            };

            smtpOutput.SendEmailWithAttachment();
        }
        #endregion 
        #endregion
    }
}
