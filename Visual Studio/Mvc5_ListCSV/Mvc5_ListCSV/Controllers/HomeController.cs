﻿using Mvc5_ListCSV.Models;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace Mvc5_ListCSV.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            AppDbContext db = new AppDbContext();

            var employees = db.Employees.ToList();

            return View(employees);
        }

        [HttpPost]
        public FileResult Exportar()
        {
            AppDbContext db = new AppDbContext();

            //obtem uma lista de objetos Employee
            List<object> employees = (from employee in db.Employees.ToList().Take(9)
                                      select new[] { employee.EmployeeId.ToString(),
                                                            employee.FirstName,
                                                            employee.City,
                                                            employee.Country
                                }).ToList<object>();

            //Insere o nome das colunas
            employees.Insert(0, new string[4] { "Employee ID", "Employee Name", "City", "Country" });

            StringBuilder sb = new StringBuilder();

            //percore os funcionarios e gera o CSV
            for (int i = 0; i < employees.Count; i++)
            {
                string[] employee = (string[])employees[i];
                for (int j = 0; j < employee.Length; j++)
                {
                    //anexa dados com separador
                    sb.Append(employee[j] + ',');
                }

                //Anexa uma nova linha
                sb.Append("\r\n");
            }
            return File(Encoding.UTF8.GetBytes(sb.ToString()), "text/csv", "GridFuncionarios.csv");
        }
    }
}