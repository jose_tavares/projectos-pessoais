﻿using System.Data.Entity;

namespace Mvc5_ListCSV.Models
{
    public class AppDbContext : DbContext
    {
        public AppDbContext() : base("AppDbContext")
        {}

        public DbSet<Employee> Employees { get; set; }
    }
}