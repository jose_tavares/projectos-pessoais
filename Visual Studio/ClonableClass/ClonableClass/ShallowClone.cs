﻿namespace ClonableClass
{
    public class ShallowClone : IShallowCopy<ShallowClone>
    {
        public ShallowClone ShallowCopy() => (ShallowClone)MemberwiseClone();
    }
}
