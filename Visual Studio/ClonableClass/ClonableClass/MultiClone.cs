﻿namespace ClonableClass
{
    using System;
    using System.IO;
    using System.Runtime.Serialization.Formatters.Binary;

    [Serializable]
    public class MultiClone : IShallowCopy<MultiClone>, IDeepCopy<MultiClone>
    {
        public MultiClone ShallowCopy() => (MultiClone)MemberwiseClone();

        public MultiClone DeepCopy()
        {
            BinaryFormatter bF = new BinaryFormatter();
            MemoryStream memStream = new MemoryStream();
            bF.Serialize(memStream, this);
            memStream.Flush();
            memStream.Position = 0;

            return (MultiClone)bF.Deserialize(memStream);
        }
    }
}
