﻿namespace ClonableClass
{
    public interface IDeepCopy<T>
    {
        T DeepCopy();
    }
}
