﻿namespace ClonableClass
{
    using System;

    [Serializable]
    public class Cliente : MultiClone
    {
        public int Id { get; set; }

        public string Nome { get; set; }

        public string Email { get; set; }

        public Endereco Endereco { get; set; }
    }
}
