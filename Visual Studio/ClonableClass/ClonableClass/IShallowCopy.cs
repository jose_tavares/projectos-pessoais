﻿namespace ClonableClass
{
    public interface IShallowCopy<T>
    {
        T ShallowCopy();
    }
}
