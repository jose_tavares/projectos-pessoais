﻿namespace ClonableClass
{
    using System;

    public class Program
    {
        public static void Main(string[] args)
        {
            if (args is null)
            {
                throw new ArgumentNullException(nameof(args));
            }

            Cliente cliente1 = new Cliente
            {
                Id = 1,
                Nome = "Macoratti",
                Endereco = new Endereco
                {
                    Local = "Rua Projectada 100"
                }
            };

            Cliente cliente2 = (Cliente)cliente1.ShallowCopy();
            cliente2.Endereco.Local = "Clonagem superficial";
            Console.WriteLine(cliente2.Endereco.Local);
            Console.WriteLine(cliente1.Endereco.Local);

            Console.WriteLine();

            Cliente cliente3 = (Cliente)cliente1.DeepCopy();
            cliente3.Endereco.Local = "Clonagem profunda";
            Console.WriteLine(cliente3.Endereco.Local);
            Console.WriteLine(cliente1.Endereco.Local);
        }
    }
}
