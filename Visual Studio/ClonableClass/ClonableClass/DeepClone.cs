﻿namespace ClonableClass
{
    using System.IO;
    using System.Runtime.Serialization.Formatters.Binary;

    public class DeepClone : IDeepCopy<DeepClone>
    {
        public DeepClone DeepCopy()
        {
            BinaryFormatter bF = new BinaryFormatter();
            MemoryStream memStream = new MemoryStream();
            bF.Serialize(memStream, this);
            memStream.Flush();
            memStream.Position = 0;

            return (DeepClone)bF.Deserialize(memStream);
        }
    }
}
