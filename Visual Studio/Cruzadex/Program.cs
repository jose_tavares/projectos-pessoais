﻿namespace Cruzadex
{
    internal class Program
    {
        static int ways = 0;

        static void Main(string[] args)
        {
            int n1 = 10;

            List<string> matrix =
            [
                "*#********",
                "*#********",
                "*#****#***",
                "*#****#***",
                "*#****#***",
                "*#****#***",
                "*#****#***",
                "*#*######*",
                "*#********",
                "***#######"
            ];

            List<string> words = ["PUNJAB", "JHARKHAND", "MIZORAM", "MUMBAI"];

            ways = 0;

            SolvePuzzle(words, matrix, 0, n1);

            Console.WriteLine("Number of ways to fill the grid is " + ways);
        }

        static void SolvePuzzle(List<string> words, List<string> matrix, int index, int n)
        {
            if (index < words.Count)
            {
                string currentWord = words[index];
                int maxLen = n - currentWord.Length;

                for (int i = 0; i < n; i++)
                {
                    for (int j = 0; j <= maxLen; j++)
                    {
                        List<string> temp = CheckVertical(j, i, new List<string>(matrix), currentWord);

                        if (temp[0][0] != '@')
                        {
                            SolvePuzzle(words, temp, index + 1, n);
                        }
                    }
                }

                for (int i = 0; i < n; i++)
                {
                    for (int j = 0; j <= maxLen; j++)
                    {
                        List<string> temp = CheckHorizontal(i, j, new List<string>(matrix), currentWord);

                        if (temp[0][0] != '@')
                        {
                            SolvePuzzle(words, temp, index + 1, n);
                        }
                    }
                }
            }
            else
            {
                Console.WriteLine(ways + 1 + " way to solve the puzzle ");
                PrintMatrix(matrix, n);
                Console.WriteLine();

                ways++;
                return;
            }
        }

        static List<string> CheckVertical(int x, int y, List<string> matrix, string currentWord)
        {
            int n = currentWord.Length;

            for (int i = 0; i < n; i++)
            {
                if (matrix[x + i][y] == '#' || matrix[x + i][y] == currentWord[i])
                {
                    matrix[x + i] = matrix[x + i].Remove(y, 1).Insert(y, currentWord[i].ToString());
                }
                else
                {
                    matrix[0] = matrix[0].Remove(0, 1).Insert(0, "@");
                    return matrix;
                }
            }

            return matrix;
        }
        
        static List<string> CheckHorizontal(int x, int y, List<string> matrix, string currentWord)
        {
            int n = currentWord.Length;

            for (int i = 0; i < n; i++)
            {
                if (matrix[x][y + i] == '#' || matrix[x][y + i] == currentWord[i])
                {
                    matrix[x] = matrix[x].Remove(y + i, 1).Insert(y + i, currentWord[i].ToString());
                }
                else
                {
                    matrix[0] = matrix[0].Remove(0, 1).Insert(0, "@");
                    return matrix;
                }
            }

            return matrix;
        }

        static void PrintMatrix(List<string> matrix, int n)
        {
            for (int i = 0; i < n; i++)
            {
                Console.WriteLine(matrix[i]);
            }
        }
    }
}
