﻿using System.Web.Mvc;
using System.Web.Helpers;
using Mvc5_Tabelas.Models;
using Mvc5_Tabelas.Services;
using System.Collections.Generic;

namespace Mvc5_Tabelas.Controllers
{
    public class HomeController : Controller
    {
        private readonly List<Funcionario> funcionarios = FuncionarioServices.GetFuncionarios();

        // GET: Home
        // pagina principal
        public ActionResult Index()
        {
            return View();
        }

        //tabela : foreach
        public ActionResult TabelaForeach()
        {
            return View(funcionarios);
        }

        //tabela : webgrid
        public ActionResult TabelaWebGrid()
        {
            var grid = new WebGrid(funcionarios, canPage: false);

            var webGrid = grid.GetHtml(tableStyle: "table table-bordered table-responsive table-hover",
                columns: grid.Columns(
                    grid.Column("FuncionarioId", "Codigo"),
                    grid.Column("Nome", "Nome"),
                    grid.Column("Email", "Email"),
                    grid.Column("Salario", "Salário"),
                    grid.Column("Nascimento", "Nascimento"),
                    grid.Column("Sexo", "Sexo"),
                    grid.Column("Setor", "Setor")
                )
            );

            return View(webGrid);
        }

        //tabela : json
        public ActionResult TabelaJsonView()
        {
            return View();
        }

        public JsonResult FuncionariosDados()
        {
            return Json(funcionarios, JsonRequestBehavior.AllowGet);
        }
    }
}