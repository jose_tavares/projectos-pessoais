﻿$(document).ready(function () {
    GetDadosFuncionarios();
});

function GetDadosFuncionarios() {
    $.get('/Home/FuncionariosDados', {}, function (data) {
        var tblFuncionario = $("#tblFuncionario");
        $.each(data, function (index, item) {
            var tr = $("<tr></tr>");
            tr.html(("<td>" + item.FuncionarioId + "</td>")
                + " " + ("<td>" + item.Nome + "</td>")
                + " " + ("<td>" + item.Email + "</td>")
                + " " + ("<td>" + item.Salario + "</td>")
                + " " + ("<td>" + ToJavaScriptDate(item.Nascimento) + "</td>")
                + " " + ("<td>" + item.Sexo + "</td>")
                + " " + ("<td>" + item.Setor + "</td>"));
            tblFuncionario.append(tr);
        });
    });
}

function ToJavaScriptDate(value) {
    var pattern = /Date\(([^)]+)\)/;
    var results = pattern.exec(value);
    var dt = new Date(parseFloat(results[1]));
    return (dt.getMonth() + 1) + "/" + dt.getDate() + "/" + dt.getFullYear();
}