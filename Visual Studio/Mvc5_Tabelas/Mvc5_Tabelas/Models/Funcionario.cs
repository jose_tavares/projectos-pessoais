﻿using System;

namespace Mvc5_Tabelas.Models
{
    public class Funcionario
    {
        public int FuncionarioId { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public decimal Salario { get; set; }
        public DateTime Nascimento { get; set; }
        public string Sexo { get; set; }
        public string Setor { get; set; }
    }
}