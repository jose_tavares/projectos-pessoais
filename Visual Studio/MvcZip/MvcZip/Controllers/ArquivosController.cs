﻿using System;
using System.IO;
using System.Web;
using System.Linq;
using System.Web.Mvc;
using System.IO.Compression;
using System.Collections.Generic;
using FileOperations = System.IO.File;

namespace MvcZip.Controllers
{
    public class ArquivosController : Controller
    {
        // GET: Arquivos
        [HttpGet]
        public ActionResult Index(string error = null, string success = null)
        {
            return View(new[] { error, success });
        }

        [HttpPost]
        public ActionResult Index(HttpPostedFileBase arquivoZIP)
        {
            try
            {
                var uploads = Server.MapPath("~/uploads");

                if ((arquivoZIP != null) && (arquivoZIP.ContentLength > 0))
                {
                    var allowedExtensions = new[] { ".zip", ".rar" };
                    var checkExtension = Path.GetExtension(arquivoZIP.FileName).ToLower();

                    if (!allowedExtensions.Contains(checkExtension))
                    {
                        return View(new[] { "Selecione um arquivo Zip ou Rar menos que 20 MB.", string.Empty });
                    }
                    else
                    {
                        using (var arquivo = new ZipArchive(arquivoZIP.InputStream))
                        {
                            foreach (var entrada in arquivo.Entries)
                            {
                                entrada.ExtractToFile(Path.Combine(uploads, entrada.FullName));
                            }
                        }
                    }
                }

                return View(new[] { string.Empty, "Arquivo submetido com sucesso." });
            }
            catch (Exception)
            {
                return View(new[] { "Envio e extração de arquivo ZIP/RAR falhou.", string.Empty });
            }
        }

        [HttpGet]
        public ActionResult BaixarArquivos()
        {
            return View(Directory.EnumerateFiles(Server.MapPath("~/pdfs")));
        }

        [HttpGet]
        public ActionResult Download(List<string> arquivos)
        {
            try
            {
                if ((arquivos != null) && arquivos.Any())
                {
                    var arquivoZipado = Server.MapPath("~/arquivoZipado.zip");

                    var temp = Server.MapPath("~/temp");

                    if (FileOperations.Exists(arquivoZipado))
                    {
                        FileOperations.Delete(arquivoZipado);
                    }

                    Directory.EnumerateFiles(temp).ToList().ForEach(each =>
                        FileOperations.Delete(each)
                    );

                    arquivos.ForEach(each =>
                        FileOperations.Copy(each, Path.Combine(temp, Path.GetFileName(each)))
                    );

                    ZipFile.CreateFromDirectory(temp, arquivoZipado);

                    return File(arquivoZipado, "application/zip", "arquivoZipado.zip");
                }
                else
                {
                    return View(model: "Selecione pelo menos um arquivo...");
                }
            }
            catch (Exception ex)
            {
                return View(model: "Falha ao gerar e enviar o arquivo compactado " + ex.Message + "...");
            }
        }
    }
}