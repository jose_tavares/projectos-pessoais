﻿
namespace CapgeminiExercise
{
    public sealed class Linq : IEvenNumbers
    {
        public void GetAndPrint(IEnumerable<int> numbers)
        {
            Console.Write("Linq: ");

            foreach (var number in numbers.Where(x => x % 2 == 0))
            {
                Console.Write(number + " ");
            }
        }
    }
}
