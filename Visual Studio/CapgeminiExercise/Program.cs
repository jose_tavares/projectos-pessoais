﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace CapgeminiExercise
{
    internal class Program
    {
        static void Main(string[] args)
        {
            ServiceCollection services = [];

            services.TryAddEnumerable
            ([
                ServiceDescriptor.Scoped<IEvenNumbers, Loop>(),
                ServiceDescriptor.Scoped<IEvenNumbers, Linq>()
            ]);

            List<int> numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

            using ServiceProvider serviceProvider = services.BuildServiceProvider();

            foreach (IEvenNumbers evenNumbers in serviceProvider.GetServices<IEvenNumbers>())
            {
                evenNumbers.GetAndPrint(numbers);
                Console.WriteLine();
            }

            Console.ReadLine();
        }
    }
}
