﻿namespace CapgeminiExercise
{
    public sealed class Loop : IEvenNumbers
    {
        public void GetAndPrint(IEnumerable<int> numbers)
        {
            Console.Write("Loop: ");

            foreach (var number in numbers)
            {
                if (number % 2 == 0)
                {
                    Console.Write(number + " ");
                }
            }
        }
    }
}
