﻿namespace CapgeminiExercise
{
    public interface IEvenNumbers
    {
        void GetAndPrint(IEnumerable<int> numbers);
    }
}
