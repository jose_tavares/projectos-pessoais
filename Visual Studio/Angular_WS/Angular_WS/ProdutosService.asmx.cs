﻿using System;
using System.Web.Services;
using System.Configuration;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Web.Script.Serialization;

namespace Angular_WS
{
    /// <summary>
    /// Summary description for ProdutosService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class ProdutosService : WebService
    {
        [WebMethod]
        public void GetProdutos()
        {
            List<Produto> listaProdutos = new List<Produto>();

            var connectionString = ConfigurationManager.ConnectionStrings["ConexaoSQLServer"].ConnectionString;

            using (var sqlConnection = new SqlConnection(connectionString))
            {
                var sqlCommand = new SqlCommand("Select * from Produtos", sqlConnection);
                sqlConnection.Open();

                var sqlDataReader = sqlCommand.ExecuteReader();

                while (sqlDataReader.Read())
                {
                    listaProdutos.Add(new Produto
                    {
                        Id = Convert.ToInt32(sqlDataReader["Id"]),
                        Nome = sqlDataReader["Nome"].ToString(),
                        Descricao = sqlDataReader["Descricao"].ToString(),
                        Preco = Convert.ToDecimal(sqlDataReader["Preco"]),
                        Estoque = Convert.ToInt32(sqlDataReader["Estoque"])
                    });
                }
            }

            var javaScriptSerializer = new JavaScriptSerializer();
            Context.Response.Write(javaScriptSerializer.Serialize(listaProdutos));
        }
    }
}
