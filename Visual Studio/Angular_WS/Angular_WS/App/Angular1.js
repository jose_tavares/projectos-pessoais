﻿var app = angular.module("ProdutosApp", []);

app.controller("produtosCtrl", function ($scope, $http) {
    $http.get('ProdutosService.asmx/GetProdutos')
        .then(function (response) {
            $scope.produtos = response.data;
        });
});