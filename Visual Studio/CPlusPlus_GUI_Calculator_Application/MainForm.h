#pragma once
#include <string>
#include <functional>

namespace CPlusPlusGUICalculatorApplication {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for MyForm
	/// </summary>
	public ref class MyForm : public System::Windows::Forms::Form
	{
	public:
		MyForm(void);

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~MyForm();

	private:
		System::Windows::Forms::NumericUpDown^ numericUpDown1;
		System::Windows::Forms::NumericUpDown^ numericUpDown2;
		System::Windows::Forms::ComboBox^ OperatorComboBox;
		System::Windows::Forms::Button^ CalculateButton;

		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container^ components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void);
#pragma endregion

#pragma region Events
		System::Void CalculateButton_Click(System::Object^ sender, System::EventArgs^ e);
#pragma endregion

#pragma region Auxiliar

#pragma endregion

	};
}
