#include "MainForm.h"

using namespace System;
using namespace System::Windows::Forms;

[STAThread]
void main(array<String^>^ args)
{
	Application::EnableVisualStyles();

	Application::SetCompatibleTextRenderingDefault(false);

	CPlusPlusGUICalculatorApplication::MyForm form;

	Application::Run(% form);
}

#pragma region Constructors
CPlusPlusGUICalculatorApplication::MyForm::MyForm(void)
{
	InitializeComponent();
	//
	//TODO: Add the constructor code here
	//
}

CPlusPlusGUICalculatorApplication::MyForm::~MyForm()
{
	if (components)
	{
		delete components;
	}
}
#pragma endregion

#pragma region Events
void CPlusPlusGUICalculatorApplication::MyForm::InitializeComponent(void)

{
	this->numericUpDown1 = (gcnew System::Windows::Forms::NumericUpDown());
	this->numericUpDown2 = (gcnew System::Windows::Forms::NumericUpDown());
	this->OperatorComboBox = (gcnew System::Windows::Forms::ComboBox());
	this->CalculateButton = (gcnew System::Windows::Forms::Button());
	(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDown1))->BeginInit();
	(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDown2))->BeginInit();
	this->SuspendLayout();
	// 
	// numericUpDown1
	// 
	this->numericUpDown1->Location = System::Drawing::Point(12, 0);
	this->numericUpDown1->Name = L"numericUpDown1";
	this->numericUpDown1->Size = System::Drawing::Size(120, 22);
	this->numericUpDown1->TabIndex = 0;
	// 
	// numericUpDown2
	// 
	this->numericUpDown2->Location = System::Drawing::Point(150, 0);
	this->numericUpDown2->Name = L"numericUpDown2";
	this->numericUpDown2->Size = System::Drawing::Size(120, 22);
	this->numericUpDown2->TabIndex = 1;
	// 
	// OperatorComboBox
	// 
	this->OperatorComboBox->FormattingEnabled = true;
	this->OperatorComboBox->Items->AddRange(gcnew cli::array< System::Object^  >(4) { L"+", L"-", L"*", L"/" });
	this->OperatorComboBox->Location = System::Drawing::Point(80, 85);
	this->OperatorComboBox->Name = L"OperatorComboBox";
	this->OperatorComboBox->Size = System::Drawing::Size(121, 24);
	this->OperatorComboBox->TabIndex = 2;
	// 
	// CalculateButton
	// 
	this->CalculateButton->Location = System::Drawing::Point(80, 218);
	this->CalculateButton->Name = L"CalculateButton";
	this->CalculateButton->Size = System::Drawing::Size(121, 23);
	this->CalculateButton->TabIndex = 3;
	this->CalculateButton->Text = L"Calculate";
	this->CalculateButton->UseVisualStyleBackColor = true;
	this->CalculateButton->Click += gcnew System::EventHandler(this, &MyForm::CalculateButton_Click);
	// 
	// MyForm
	// 
	this->AutoScaleDimensions = System::Drawing::SizeF(8, 16);
	this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
	this->ClientSize = System::Drawing::Size(282, 253);
	this->Controls->Add(this->CalculateButton);
	this->Controls->Add(this->OperatorComboBox);
	this->Controls->Add(this->numericUpDown2);
	this->Controls->Add(this->numericUpDown1);
	this->Name = L"MyForm";
	this->Text = L"Calculator";
	(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDown1))->EndInit();
	(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDown2))->EndInit();
	this->ResumeLayout(false);

}

System::Void CPlusPlusGUICalculatorApplication::MyForm::CalculateButton_Click(System::Object^ sender, System::EventArgs^ e)
{
	if (OperatorComboBox->SelectedItem == nullptr)
	{
		MessageBox::Show("Select operator.");
		return;
	}

	if (OperatorComboBox->SelectedItem->ToString() == "+")
	{
		auto result = Decimal::Add(numericUpDown1->Value, numericUpDown2->Value);
		MessageBox::Show(numericUpDown1->Text + " + " + numericUpDown2->Text + " = " + result);
	}
	else if (OperatorComboBox->SelectedItem->ToString() == "-")
	{
		auto result = Decimal::Subtract(numericUpDown1->Value, numericUpDown2->Value);
		MessageBox::Show(numericUpDown1->Text + " - " + numericUpDown2->Text + " = " + result);
	}
	else if (OperatorComboBox->SelectedItem->ToString() == "*")
	{
		auto result = Decimal::Multiply(numericUpDown1->Value, numericUpDown2->Value);
		MessageBox::Show(numericUpDown1->Text + " * " + numericUpDown2->Text + " = " + result);
	}
	else if (OperatorComboBox->SelectedItem->ToString() == "/")
	{
		try
		{
			auto result = Decimal::Divide(numericUpDown1->Value, numericUpDown2->Value);
			MessageBox::Show(numericUpDown1->Text + " / " + numericUpDown2->Text + " = " + result);
		}
		catch (System::DivideByZeroException^ e)
		{
			MessageBox::Show(e->Message);
		}
	}
}
#pragma endregion

#pragma region Auxiliar
#pragma endregion