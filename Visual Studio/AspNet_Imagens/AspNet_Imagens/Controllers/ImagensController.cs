﻿using AspNet_Imagens.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.IO;
using System.Linq;

namespace AspNet_Imagens.Controllers
{
    public class ImagensController : Controller
    {
        private readonly AppDbContext db;

        public ImagensController(AppDbContext _db)
        {
            db = _db;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult UploadImagem()
        {
            foreach (var file in Request.Form.Files)
            {
                var img = new Imagem
                {
                    ImagemTitulo = file.FileName
                };

                var ms = new MemoryStream();
                file.CopyTo(ms);
                img.ImagemDados = ms.ToArray();
                ms.Close();
                ms.Dispose();
                db.Imagens.Add(img);
                db.SaveChanges();
            }

            ViewBag.Message = $"A Imagem foi armazenada na base de dados !";

            return View("Index");
        }

        [HttpPost]
        public ActionResult RecuperarImagem()
        {
            var img = db.Imagens.OrderByDescending(i => i.ImagemId)
                                    .FirstOrDefault();
            if (img != null)
            {
                var imageBase64Data = Convert.ToBase64String(img.ImagemDados);

                var imageDataURL =
                    string.Format("data:image/jpg;base64,{0}", imageBase64Data);

                ViewBag.ImageTitle = img.ImagemTitulo;
                ViewBag.ImageDataUrl = imageDataURL;
            }

            return View("Index");
        }
    }
}
