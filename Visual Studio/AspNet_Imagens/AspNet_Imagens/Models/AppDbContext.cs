﻿using Microsoft.EntityFrameworkCore;

namespace AspNet_Imagens.Models
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
        }

        public DbSet<Imagem> Imagens { get; set; }
    }
}
