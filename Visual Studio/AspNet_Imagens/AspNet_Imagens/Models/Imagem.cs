﻿namespace AspNet_Imagens.Models
{
    public class Imagem
    {
        public int ImagemId { get; set; }

        public string ImagemTitulo { get; set; }

        public byte[] ImagemDados { get; set; }
    }
}
