﻿using System.Data.Entity;

namespace Filmoteca.Models
{
    public class FilmeDb : DbContext
    {
        public FilmeDb() : base("FilmeDb") { }

        public DbSet<Filme> Filmes { get; set; }
    }
}