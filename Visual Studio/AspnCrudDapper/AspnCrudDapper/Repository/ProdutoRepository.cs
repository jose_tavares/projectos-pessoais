﻿using Dapper;
using System;
using System.Linq;
using System.Data.SqlClient;
using AspnCrudDapper.Entities;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;

namespace AspnCrudDapper.Repository
{
    public class ProdutoRepository : IProdutoRepository
    {
        private readonly IConfiguration _configuration;

        public ProdutoRepository(IConfiguration configuration) =>
            _configuration = configuration;

        public int Add(Produto produto)
        {
            int count = 0;
            var connectionString = GetConnection();

            using (var sqlConnection = new SqlConnection(connectionString))
            {
                try
                {
                    sqlConnection.Open();

                    var query = "INSERT INTO Produtos(Nome, Estoque, Preco) VALUES(@Nome, @Estoque, @Preco); " +
                        "SELECT CAST(SCOPE_IDENTITY() as INT); ";

                    count = sqlConnection.Execute(query, produto);
                }

                catch (Exception ex) { throw ex; }

                finally { sqlConnection.Close(); }

                return count;
            }
        }

        public int Delete(int id)
        {
            int count = 0;
            var connectionString = GetConnection();

            using (var sqlConnection = new SqlConnection(connectionString))
            {
                try
                {
                    sqlConnection.Open();

                    var query = "DELETE FROM Produtos WHERE ProdutoId =" + id;
                    count = sqlConnection.Execute(query);
                }

                catch (Exception ex) { throw ex; }

                finally { sqlConnection.Close(); }

                return count;
            }
        }

        public int Edit(Produto produto)
        {
            int count = 0;
            var connectionString = GetConnection();

            using (var sqlConnection = new SqlConnection(connectionString))
            {
                try
                {
                    sqlConnection.Open();

                    var query = "UPDATE Produtos SET Nome = @Nome, Estoque = @Estoque, Preco = @Preco " +
                        "WHERE ProdutoId = " + produto.ProdutoId;

                    count = sqlConnection.Execute(query, produto);
                }

                catch (Exception ex) { throw ex; }

                finally { sqlConnection.Close(); }

                return count;
            }
        }

        public Produto Get(int id)
        {
            Produto produto = null;
            var connectionString = GetConnection();

            using (var sqlConnection = new SqlConnection(connectionString))
            {
                try
                {
                    sqlConnection.Open();

                    var query = "SELECT * FROM Produtos WHERE ProdutoId =" + id;
                    produto = sqlConnection.Query<Produto>(query).FirstOrDefault();
                }

                catch (Exception ex) { throw ex; }

                finally { sqlConnection.Close(); }

                return produto;
            }
        }

        public IEnumerable<Produto> GetProdutos()
        {
            IEnumerable<Produto> produtos = null;
            var connectionString = GetConnection();

            using (var sqlConnection = new SqlConnection(connectionString))
            {
                try
                {
                    sqlConnection.Open();

                    var query = "SELECT * FROM Produtos";

                    produtos = sqlConnection.Query<Produto>(query);
                }

                catch (Exception ex) { throw ex; }

                finally { sqlConnection.Close(); }

                return produtos;
            }
        }

        private string GetConnection() =>
            _configuration.GetSection("ConnectionStrings").
                GetSection("ProdutoConnection").Value;
    }
}
