﻿using AspnCrudDapper.Entities;
using System.Collections.Generic;

namespace AspnCrudDapper.Repository
{
    public interface IProdutoRepository
    {
        int Add(Produto produto);

        int Delete(int id);

        int Edit(Produto produto);

        Produto Get(int id);

        IEnumerable<Produto> GetProdutos();
    }
}
