﻿using Microsoft.AspNetCore.Mvc;
using AspnCrudDapper.Repository;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace AspnCrudDapper.Pages.Produto
{
    public class AddModel : PageModel
    {
        private readonly IProdutoRepository _produtoRepository;

        #region Public Properties
        [BindProperty]
        public Entities.Produto Produto { get; set; }

        [TempData]
        public string Message { get; set; }
        #endregion

        public AddModel(IProdutoRepository produtoRepository) =>
            _produtoRepository = produtoRepository;

        #region Public Methods
        public void OnGet() => Page();

        public IActionResult OnPost()
        {
            if (ModelState.IsValid)
            {
                var count = _produtoRepository.Add(Produto);

                if (count > 0)
                {
                    Message = "Novo Produto incluído com sucesso!";

                    return RedirectToPage("/Produto/Index");
                }
            }

            return Page();
        }
        #endregion
    }
}