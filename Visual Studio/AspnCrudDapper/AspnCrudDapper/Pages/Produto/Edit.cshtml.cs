﻿using Microsoft.AspNetCore.Mvc;
using AspnCrudDapper.Repository;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace AspnCrudDapper.Pages.Produto
{
    public class EditModel : PageModel
    {
        private readonly IProdutoRepository _produtoRepository;

        [BindProperty]
        public Entities.Produto Produto { get; set; }

        public EditModel(IProdutoRepository produtoRepository) =>
            _produtoRepository = produtoRepository;

        public void OnGet(int id) =>
            Produto = _produtoRepository.Get(id);

        public IActionResult OnPost()
        {
            if (ModelState.IsValid)
            {
                var count = _produtoRepository.Edit(Produto);

                if (count > 0)
                {
                    return RedirectToPage("/Produto/Index");
                }
            }

            return Page();
        }
    }
}