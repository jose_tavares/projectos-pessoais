﻿using Microsoft.AspNetCore.Mvc;
using AspnCrudDapper.Repository;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace AspnCrudDapper.Pages.Produto
{
    public class IndexModel : PageModel
    {
        private readonly IProdutoRepository _produtoRepository;

        #region Public Properties
        [BindProperty]
        public IEnumerable<Entities.Produto> ListaProdutos { get; set; }

        [TempData]
        public string Message { get; set; }
        #endregion

        public IndexModel(IProdutoRepository produtoRepository) =>
            _produtoRepository = produtoRepository;

        #region Public Methods
        public void OnGet() => ListaProdutos = _produtoRepository.GetProdutos();        

        public IActionResult OnPostDelete(int id)
        {
            if (id > 0)
            {
                var count = _produtoRepository.Delete(id);

                if (count > 0)
                {
                    Message = "Produto Apagado com sucesso!";
                    return RedirectToPage("/Produto/Index");
                }
            }

            return Page();
        }
        #endregion
    }
}