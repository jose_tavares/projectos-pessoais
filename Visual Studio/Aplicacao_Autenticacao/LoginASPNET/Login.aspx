﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Mestre.Master" CodeBehind="Login.aspx.vb" Inherits="LoginASPNET.Login" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:Login ID="Login1" runat="Server" 
        DestinationPageUrl="~/Default.aspx"  
        TitleText="Informe nome do Usuário e Senha :<br /><br />"
        CreateUserText="Novo Usuário?"
        CreateUserUrl="~/Login/Registrar.aspx"
        PasswordRecoveryText="Esqueceu a Senha?"
        PasswordRecoveryUrl="~/Login/Recupera.aspx" FailureText="Seu Login falhou, tente novamente" 
         PasswordLabelText="Senha:" PasswordRequiredErrorMessage="A senha é obrigatória" 
         RememberMeText="Lembre-me da próxima vez." UserNameLabelText="Usuário :" 
         UserNameRequiredErrorMessage="O usuário é obrigatório" BackColor="#F7F7DE" BorderColor="#CCCC99" BorderStyle="Solid" BorderWidth="1px" Font-Names="Verdana" Font-Size="10pt" Height="214px" Width="354px" >
         <TitleTextStyle BackColor="#6B696B" Font-Bold="True" ForeColor="#FFFFFF" />
</asp:Login>
</asp:Content>
