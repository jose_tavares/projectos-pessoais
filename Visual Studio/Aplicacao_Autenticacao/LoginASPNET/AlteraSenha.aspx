﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Mestre.Master" CodeBehind="AlteraSenha.aspx.vb" Inherits="LoginASPNET.AlteraSenha" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ChangePassword ID="ChangePassword1" runat="server"
      ChangePasswordTitleText="Altere a sua senha<br /><br />"
       PasswordLabelText="Informe a Senha Atual:"
       NewPasswordLabelText="Informe a nova Senha:"
       ConfirmNewPasswordLabelText="Confirme a nova Senha:" CancelButtonText="Cancela" ChangePasswordButtonText="Alterar Senha" ChangePasswordFailureText="Senha incorreta ou nova Senha inválida. Tamanho mínimo da senha {0}. Caracteres não alfanuméricos requeridos: {1}." ConfirmPasswordCompareErrorMessage="As senhas não conferem" ConfirmPasswordRequiredErrorMessage="A nova senha deve ser confirmada" ContinueButtonText="Continuar" NewPasswordRegularExpressionErrorMessage="Informe uma senha diferente" NewPasswordRequiredErrorMessage="A nova senha deve ser informada" PasswordRequiredErrorMessage="A senha é obrigatória" SuccessText="Sua senha foi alterada !" SuccessTitleText="Alteração de senha finalizada" UserNameLabelText="Usuário:" UserNameRequiredErrorMessage="Nome do usuário deve ser informado" BackColor="#F7F7DE" BorderColor="#CCCC99" BorderStyle="Solid" BorderWidth="1px" Font-Names="Verdana" Font-Size="10pt"
     >
    <TitleTextStyle BackColor="#6B696B" Font-Bold="True" ForeColor="#FFFFFF" />
</asp:ChangePassword>
</asp:Content>
