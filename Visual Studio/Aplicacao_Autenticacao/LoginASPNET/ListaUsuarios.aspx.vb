﻿Public Class ListaUsuarios
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim usuarios As MembershipUserCollection
        usuarios = Membership.GetAllUsers()

        For Each user As MembershipUser In usuarios
            If user.IsOnline Then
                lblUsuarios.Text += user.UserName + "<br />"
            End If
        Next

    End Sub

End Class