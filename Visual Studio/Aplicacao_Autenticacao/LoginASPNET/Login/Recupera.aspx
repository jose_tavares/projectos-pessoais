﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Mestre.Master" CodeBehind="Recupera.aspx.vb" Inherits="LoginASPNET.Recupera" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:PasswordRecovery ID="PasswordRecovery1" runat="server"
      SuccessPageUrl="~/Login/Recuperada.aspx" AnswerRequiredErrorMessage="A resposta é requerida" 
        UserNameRequiredErrorMessage="O usuário é requerida" UserNameLabelText="Usuário:" 
        UserNameTitleText="Esqueceu a Senha?" UserNameInstructionText="Informe o seu usuário para receber a senha" 
        AnswerLabelText="Resposta:" GeneralFailureText="Não foi possível recuperar a sua senha. Tente novamente." 
        QuestionFailureText="Sua resposta não pode ser verificada" QuestionLabelText="Questão:" 
        QuestionTitleText="Confirmação de identidade" QuestionInstructionText="Responda à pergunta para receber a senha" 
        SuccessText="A senha foi enviada" UserNameFailureText="Não foi possível acessar sua informação" 
        SubmitButtonText="Submeter" BackColor="#F7F7DE" BorderColor="#CCCC99" BorderStyle="Solid" BorderWidth="1px" 
        Font-Names="Verdana" Font-Size="10pt">
        <MailDefinition From="admin@macoratti.com" Subject="Recuperação de Senha" BodyFileName="~/Login/MensagemSenha.txt"> 
        </MailDefinition>
        <TitleTextStyle BackColor="#6B696B" Font-Bold="True" ForeColor="#FFFFFF" />
    </asp:PasswordRecovery>

</asp:Content>
