﻿<%@ Page Title="Registro de Usuário" Language="vb" AutoEventWireup="false" MasterPageFile="~/Mestre.Master" CodeBehind="Registrar.aspx.vb" Inherits="LoginASPNET.Registrar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:CreateUserWizard ID="CreateUserWizard1" runat="server"
          ContinueDestinationPageUrl="~/Default.aspx"
          CreateUserButtonText="Criar Conta" AnswerLabelText="Questão de Segurança" AnswerRequiredErrorMessage="A questão de segurança é obrigatória" CancelButtonText="Cancela" CompleteSuccessText="Sua conta foi criada com sucesso" ConfirmPasswordCompareErrorMessage="As senhas devem conferir" ConfirmPasswordLabelText="Confirmar Senha" ConfirmPasswordRequiredErrorMessage="A confirmação é obrigatória" ContinueButtonText="Continuar" DuplicateEmailErrorMessage="O e-mail que você informou já esta sendo usado. Informe outro email" DuplicateUserNameErrorMessage="Informe outro nome de usuário" EmailRegularExpressionErrorMessage="Informe outro email" EmailRequiredErrorMessage="Informe o E-mail" FinishCompleteButtonText="Encerrar" FinishPreviousButtonText="Anterior" InvalidAnswerErrorMessage="Informe outra questão de segurança" InvalidEmailErrorMessage="Informe um e-mail válido" InvalidPasswordErrorMessage="Tamanho mínimo da senha: {0}. Caracteres não alfanuméricos requeridos: {1}." InvalidQuestionErrorMessage="Informe uma questão de segurança diferente" PasswordLabelText="Senha:" PasswordRegularExpressionErrorMessage="Informe outra senha" PasswordRequiredErrorMessage="Informe a Senha" QuestionLabelText="Questão de Segurança:" QuestionRequiredErrorMessage="A questão de segurança é obrigatória" StartNextButtonText="Próximo" StepNextButtonText="Próximo" StepPreviousButtonText="Anterior" UnknownErrorMessage="Sua conta não foi criada. Tente novamente" UserNameLabelText="Usuário:" UserNameRequiredErrorMessage="Informe o usuário" BackColor="#F7F7DE" BorderColor="#CCCC99" BorderStyle="Solid" BorderWidth="1px" Font-Names="Verdana" Font-Size="10pt">
          <TitleTextStyle BackColor="#6B696B" Font-Bold="True" ForeColor="#FFFFFF" />
          <WizardSteps> 
       <asp:CreateUserWizardStep ID="CreateUserWizardStep1" runat="server">
        </asp:CreateUserWizardStep>
          <asp:CompleteWizardStep ID="CompleteWizardStep1" runat="server">
         </asp:CompleteWizardStep>
         </WizardSteps> 
          <ContinueButtonStyle BackColor="#FFFBFF" BorderColor="#CCCCCC" BorderStyle="Solid" BorderWidth="1px" Font-Names="Verdana" ForeColor="#284775" />
          <CreateUserButtonStyle BackColor="#FFFBFF" BorderColor="#CCCCCC" BorderStyle="Solid" BorderWidth="1px" Font-Names="Verdana" ForeColor="#284775" />
          <MailDefinition From="Admin@pirata.com" Subject="Sua nova conta" BodyFileName="~/Login/NovoUsuario.txt" />
          <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="#FFFFFF" HorizontalAlign="Center" />
          <NavigationButtonStyle BackColor="#FFFBFF" BorderColor="#CCCCCC" BorderStyle="Solid" BorderWidth="1px" Font-Names="Verdana" ForeColor="#284775" />
          <SideBarButtonStyle BorderWidth="0px" Font-Names="Verdana" ForeColor="#FFFFFF" />
          <SideBarStyle BackColor="#7C6F57" BorderWidth="0px" Font-Size="0.9em" VerticalAlign="Top" />
          <StepStyle BorderWidth="0px" />
         </asp:CreateUserWizard>
</asp:Content>
