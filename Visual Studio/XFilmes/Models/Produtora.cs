﻿using System.ComponentModel.DataAnnotations;

namespace XFilmes.Models
{
    public class Produtora
    {
        public int ProdutoraId { get; set; }

        [Required(ErrorMessage = "Informe o nome")]
        public string? Nome { get; set; }

        public string? Logo { get; set; }

        [Range(100000, 999999999)]
        [Display(Name = "Receita Anual")]
        [Required(ErrorMessage = "Informe a receita anual")]
        [DisplayFormat(DataFormatString = "{0:n2}", ApplyFormatInEditMode = true)]
        public decimal ReceitaAnual { get; set; } = 0.00M;


        [Display(Name = "Data de Fundação")]
        [Required(ErrorMessage = "Informe a data de fundação")]
        public DateTime DataCriacao { get; set; } = DateTime.Now;


        //relacionamento um-para-muitos com Filme
        public ICollection<Filme> Filmes { get; set; } = new List<Filme>();

        //relacionamento um-para-um com Matriz
        public Matriz? Matriz { get; set; }
    }
}
