﻿using System.ComponentModel.DataAnnotations;

namespace XFilmes.Models
{
    public class Filme
    {
        public int FilmeId { get; set; }

        public string? Nome { get; set; }

        public string? Poster { get; set; }

        [Display(Name = "Orçamento")]
        [DisplayFormat(DataFormatString = "{0:n2}", ApplyFormatInEditMode = true)]
        public decimal Orcamento { get; set; }

        [DisplayFormat(DataFormatString = "{0:n2}", ApplyFormatInEditMode = true)]
        public decimal Faturamento { get; set; }


        [Display(Name = "Data de Lançamento")]
        public DateTime DataLancamento { get; set; }

        #region one-to-Many com Produtora
        public int ProdutoraId { get; set; }

        public Produtora? Produtora { get; set; }
        #endregion

        // Many-to-Many com FilmeDistribuidora
        public IList<FilmeDistribuidora> FilmesDistribuidoras { get; set; } = new List<FilmeDistribuidora>();
    }
}
