﻿namespace XFilmes.Models
{
    public class FilmeDistribuidora
    {
        public int FilmeId { get; set; } //foreign key

        public int DistribuidoraId { get; set; } //foreign key

        public Filme? Filme { get; set; } //Reference navigation

        public Distribuidora? Distribuidora { get; set; } //Reference navigation
    }
}
