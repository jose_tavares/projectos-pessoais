﻿namespace XFilmes.Models
{
    public class Matriz
    {
        public int MatrizId { get; set; }

        public string? Pais { get; set; }

        public string? Cidade { get; set; }

        public string? Local { get; set; }

        public string? Telefone { get; set; }

        public string? Email { get; set; }

        #region one-to-Many com Produtora
        public int ProdutoraId { get; set; }

        public Produtora? Produtora { get; set; }
        #endregion
    }
}
