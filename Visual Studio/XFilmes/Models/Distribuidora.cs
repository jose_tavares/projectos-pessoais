﻿using System.ComponentModel.DataAnnotations;

namespace XFilmes.Models
{
    public class Distribuidora
    {
        public int DistribuidoraId { get; set; }

        public string? Nome { get; set; }

        [Required]
        public string? Local { get; set; }


        [Required]
        public string? Telefone { get; set; }

        // Many-to-Many with Movie
        public IList<FilmeDistribuidora> FilmesDistribuidoras { get; set; } = new List<FilmeDistribuidora>();
    }
}
