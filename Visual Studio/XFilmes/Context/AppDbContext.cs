﻿using Microsoft.EntityFrameworkCore;
using XFilmes.Models;

namespace XFilmes.Context
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

        public DbSet<Produtora>? Produtoras { get; set; }

        public DbSet<Filme>? Filmes { get; set; }

        public DbSet<Matriz>? Sedes { get; set; }

        public DbSet<Distribuidora>? Distribuidoras { get; set; }

        public DbSet<FilmeDistribuidora>? FilmesDistribuidoras { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            #region Produtora
            modelBuilder.Entity<Produtora>().HasKey(s => s.ProdutoraId);

            #region Produtora<->Matriz
            modelBuilder.Entity<Produtora>()
                .HasOne(p => p.Matriz)
                .WithOne(m => m.Produtora)
                .HasForeignKey<Matriz>(m => m.ProdutoraId);
            #endregion

            modelBuilder.Entity<Produtora>(entity =>
            {
                entity.Property(e => e.Nome)
                    .IsRequired()
                    .HasMaxLength(80);

                entity.Property(e => e.Logo)
                    .HasMaxLength(100);

                entity.Property(e => e.ReceitaAnual)
                    .IsRequired()
                    .HasColumnType("decimal(12,2)");

                entity.Property(e => e.DataCriacao)
                    .IsRequired()
                    .HasColumnType("Date");
            });
            #endregion

            #region Filme
            modelBuilder.Entity<Filme>().HasKey(s => s.FilmeId);

            modelBuilder.Entity<Filme>()
               .HasOne(e => e.Produtora)
               .WithMany(e => e.Filmes)
               .HasForeignKey(e => e.ProdutoraId)
               .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Filme>(entity =>
            {
                entity.Property(e => e.Nome)
                    .IsRequired()
                    .HasMaxLength(80);

                entity.Property(e => e.Poster)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Orcamento)
                    .IsRequired()
                    .HasColumnType("decimal(12,2)");

                entity.Property(e => e.Faturamento)
                    .IsRequired()
                    .HasColumnType("decimal(12,2)");

                entity.Property(e => e.DataLancamento)
                    .IsRequired()
                    .HasColumnType("Date");
            });
            #endregion

            #region Distribuidora
            modelBuilder.Entity<Distribuidora>().HasKey(s => s.DistribuidoraId);

            modelBuilder.Entity<Distribuidora>(entity =>
            {
                entity.Property(e => e.Nome)
                    .IsRequired()
                    .HasMaxLength(80);

                entity.Property(e => e.Local)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Telefone)
                    .IsRequired()
                    .HasMaxLength(25);
            });
            #endregion

            #region FilmeDistribuidora
            modelBuilder.Entity<FilmeDistribuidora>().HasKey(t => new { t.FilmeId, t.DistribuidoraId });

            modelBuilder.Entity<FilmeDistribuidora>()
               .HasOne(t => t.Filme)
               .WithMany(t => t.FilmesDistribuidoras)
               .HasForeignKey(t => t.FilmeId);

            modelBuilder.Entity<FilmeDistribuidora>()
               .HasOne(t => t.Distribuidora)
               .WithMany(t => t.FilmesDistribuidoras)
               .HasForeignKey(t => t.DistribuidoraId);
            #endregion
        }
    }
}
