﻿using Domain.Models;
using Repositories.Services;
using System.Collections.Generic;

namespace Repositories.Implementations
{
    public class TemplateImplementation : ITemplateService
    {
        private readonly IUnitOfWork _unitOfWork;

        public TemplateImplementation(IUnitOfWork unitOfWork) =>
            _unitOfWork = unitOfWork;

        public Template SingleOrDefault(int id) =>
            _unitOfWork.TemplateRepository.SingleOrDefault(single => single.TemplateId == id);

        public IEnumerable<Template> GetByFrameType(int id) =>
            _unitOfWork.TemplateRepository.Where(where => where.FrameType == id);
    }
}
