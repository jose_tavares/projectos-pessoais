﻿using System;
using System.Linq;
using Domain.Models;
using Common.Extensions;
using Repositories.Services;
using System.Collections.Generic;
using Domain.ViewModels.Management;

namespace Repositories.Implementations
{
    public class HomeImplementation : IHomeService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IFrameService _frameService;

        private readonly DateTime _sevenDaysAgo = DateTime.Now.AddDays(-7);

        public HomeImplementation
            (IUnitOfWork unitOfWork,
            IFrameService frameService)
        {
            _unitOfWork = unitOfWork;
            _frameService = frameService;
        }

        public int GetCountCanvases() =>
            _unitOfWork.CanvasRepository.All().Count();

        public int GetCountClock(bool previousSevenDays = false)
        {
            var clocks = _unitOfWork.ClockRepository.All();

            if (previousSevenDays)
            {
                clocks = clocks.Where(where => where.Frame.DateCreated >= _sevenDaysAgo);
            }

            return clocks.Count();
        }

        public int GetCountDisplays() =>
            _unitOfWork.DisplayRepository.All().Count();

        public int GetCountHtml(bool previousSevenDays = false)
        {
            var htmls = _unitOfWork.HtmlRepository.All();

            if (previousSevenDays)
            {
                htmls = htmls.Where(where => where.Frame.DateCreated >= _sevenDaysAgo);
            }

            return htmls.Count();
        }

        public int GetCountLevels() =>
            _unitOfWork.LevelRepository.All().Count();

        public int GetCountLocations() =>
            _unitOfWork.LocationRepository.All().Count();

        public int GetCountMemos(bool previousSevenDays = false)
        {
            var memos = _unitOfWork.MemoRepository.All();

            if (previousSevenDays)
            {
                memos = memos.Where(where => where.Frame.DateCreated >= _sevenDaysAgo);
            }

            return memos.Count();
        }

        public int GetCountOutlook(bool previousSevenDays = false)
        {
            var outlook = _unitOfWork.OutlookRepository.All();

            if (previousSevenDays)
            {
                outlook = outlook.Where(where => where.Frame.DateCreated >= _sevenDaysAgo);
            }

            return outlook.Count();
        }

        public int GetCountPanels() =>
            _unitOfWork.PanelRepository.All().Count();

        public int GetCountPowerbi(bool previousSevenDays = false)
        {
            var powerbi = _unitOfWork.PowerbiRepository.All();

            if (previousSevenDays)
            {
                powerbi = powerbi.Where(where => where.Frame.DateCreated >= _sevenDaysAgo);
            }

            return powerbi.Count();
        }

        public int GetCountReports(bool previousSevenDays = false)
        {
            var reports = _unitOfWork.ReportRepository.All();

            if (previousSevenDays)
            {
                reports = reports.Where(where => where.Frame.DateCreated >= _sevenDaysAgo);
            }

            return reports.Count();
        }

        public int GetCountVideos(bool previousSevenDays = false)
        {
            var videos = _unitOfWork.ReportRepository.All();

            if (previousSevenDays)
            {
                videos = videos.Where(where => where.Frame.DateCreated >= _sevenDaysAgo);
            }

            return videos.Count();
        }

        public int GetCountWeather(bool previousSevenDays = false)
        {
            var weather = _unitOfWork.WeatherRepository.All();

            if (previousSevenDays)
            {
                weather = weather.Where(where => where.Frame.DateCreated >= _sevenDaysAgo);
            }

            return weather.Count();
        }

        public int GetCountYoutube(bool previousSevenDays = false)
        {
            var youtube = _unitOfWork.YoutubeRepository.All();

            if (previousSevenDays)
            {
                youtube = youtube.Where(where => where.Frame.DateCreated >= _sevenDaysAgo);
            }

            return youtube.Count();
        }

        public string GetDurationHours() =>
            string.Format("{0:N2}", _unitOfWork.FrameRepository.All()
                .Select(select => Math.Round(select.Duration / 3600.0, 2))
                .DefaultIfEmpty(0)
                .Sum()
            );

        public int GetCountPicture(bool previousSevenDays = false)
        {
            var pictures = _unitOfWork.PictureRepository.All();

            if (previousSevenDays)
            {
                pictures = pictures.Where(where => where.Frame.DateCreated >= _sevenDaysAgo);
            }

            return pictures.Count();
        }

        public IEnumerable<ContentViewModel> GetTopFiveContents()
        {
            var frames = _unitOfWork.FrameRepository
                .Where(where =>
                    ((where.BeginsOn == null) ||
                        (where.BeginsOn <= DateTime.Now)) &&
                    ((where.EndsOn == null) ||
                        (where.EndsOn >= DateTime.Now)));

            var contentsViewModel = SpecificFrames(frames)
                .Select(select => new
                {
                    Name = select
                })
                .GroupBy(groupBy => groupBy.Name)
                .OrderByDescending(orderByDescending => orderByDescending.Count())
                .Take(5)
                .Select(select => new ContentViewModel { Name = select.Key, Count = select.Count() })
                .OrderByDescending(orderByDescending => orderByDescending.Count);

            return contentsViewModel;
        }

        private List<string> SpecificFrames(IEnumerable<Frame> frames)
        {
            var contentNames = new List<string>();

            foreach (var frame in frames)
            {
                var contentName = GetContentName(_frameService.GetSpeficicationFrame(frame));

                if (!string.IsNullOrEmpty(contentName))
                {
                    contentNames.Add(contentName);
                }
            }

            return contentNames;
        }

        private string GetContentName(object obj)
        {
            string contentValue = null;

            new Switch(obj)
                .Case<Html>(action =>
                    contentValue = action.Name ?? "Html"
                )
                .Case<Memo>(action =>
                    contentValue = action.Subject ?? "Memorando"
                )
                .Case<Outlook>(action =>
                    contentValue = action.Name ?? "Outlook"
                )
                .Case<Picture>(action =>
                {
                    var content = _unitOfWork.ContentRepository.SingleOrDefault(single => single.ContentId == action.ContentId);

                    contentValue = content?.Name ?? "Imagem";
                })
                .Case<Powerbi>(action =>
                    contentValue = action.Name ?? "Power Bi"
                )
                .Case<Report>(action =>
                    contentValue = action.Name ?? "Relatório"
                )
                .Case<VideoAlternative>(action =>
                {
                    var content = _unitOfWork.ContentRepository.SingleOrDefault(single => single.ContentId == action.ContentId);

                    contentValue = content?.Name ?? "Vídeo";
                })
                .Case<Weather>(action =>
                    contentValue = "Clima"
                )
                .Case<Youtube>(action =>
                    contentValue = action.Name ?? "Youtube"
                );

            return contentValue;
        }
    }
}