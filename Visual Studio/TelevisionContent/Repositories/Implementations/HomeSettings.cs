﻿using System.Linq;
using static Common.Enums;
using Repositories.Services;
using Domain.ViewModels.Management;

namespace Repositories.Implementations
{
    public class HomeSettings
    {
        private readonly IHomeService _homeService;
        private readonly IFrameService _frameService;

        public HomeSettings
            (IHomeService homeService,
            IFrameService frameService)
        {
            _homeService = homeService;
            _frameService = frameService;
        }

        public HomeViewModel GetViewModel()
        {
            return new HomeViewModel
            {
                CountActiveFrames = _frameService.All(timingOptions: TimingOptions.Active).Count(),
                CountCanvases = _homeService.GetCountCanvases(),
                CountClock = _homeService.GetCountClock(),
                CountClock7 = _homeService.GetCountClock(true),
                CountDisplays = _homeService.GetCountDisplays(),
                CountExpiredFrames = _frameService.All(timingOptions: TimingOptions.Expired).Count(),
                CountFrames = _frameService.All().Count(),
                CountHtml = _homeService.GetCountHtml(),
                CountHtml7 = _homeService.GetCountHtml(true),
                CountLevels = _homeService.GetCountLevels(),
                CountLocations = _homeService.GetCountLocations(),
                CountMemos = _homeService.GetCountMemos(),
                CountMemos7 = _homeService.GetCountMemos(true),
                CountOutlook = _homeService.GetCountOutlook(),
                CountOutlook7 = _homeService.GetCountOutlook(true),
                CountPanels = _homeService.GetCountPanels(),
                CountPendingFrames = _frameService.All(timingOptions: TimingOptions.Pending).Count(),
                CountPowerbi = _homeService.GetCountPowerbi(),
                CountPowerbi7 = _homeService.GetCountPowerbi(true),
                CountReports = _homeService.GetCountReports(),
                CountReports7 = _homeService.GetCountReports(true),
                CountVideos = _homeService.GetCountVideos(),
                CountVideos7 = _homeService.GetCountVideos(true),
                CountWeather = _homeService.GetCountWeather(),
                CountWeather7 = _homeService.GetCountWeather(true),
                CountYoutube = _homeService.GetCountYoutube(),
                CountYoutube7 = _homeService.GetCountYoutube(true),
                DurationHours = _homeService.GetDurationHours(),
                CountPictures = _homeService.GetCountPicture(),
                CountPictures7 = _homeService.GetCountPicture(true),
                TopFiveContentsViewModel = _homeService.GetTopFiveContents().ToList()
            };
        }
    }
}
