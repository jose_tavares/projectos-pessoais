﻿using System.Linq;
using Domain.Models;
using Repositories.Services;
using System.Collections.Generic;

namespace Repositories.Implementations
{
    public class PanelImplementation : IPanelService
    {
        private readonly IUnitOfWork _unitOfWork;

        public PanelImplementation(IUnitOfWork unitOfWork) =>
            _unitOfWork = unitOfWork;

        public bool Add(Panel panel)
        {
            _unitOfWork.PanelRepository.Add(panel);

            if (_unitOfWork.Save() == 0)
            {
                return false;
            }

            return true;
        }

        public bool Edit(Panel panel)
        {
            _unitOfWork.PanelRepository.Edit(panel);

            if (_unitOfWork.Save() == 0)
            {
                return false;
            }

            return true;
        }

        public bool Remove(int id)
        {
            _unitOfWork.PanelRepository.Remove
                (_unitOfWork.PanelRepository.SingleOrDefault(single => single.PanelId == id));

            if (_unitOfWork.Save() == 0)
            {
                return false;
            }

            return true;
        }

        public IEnumerable<Panel> All(int canvasId = 0, string painelName = null)
        {
            var panels = _unitOfWork.PanelRepository.All();

            if ((canvasId > 0) || !string.IsNullOrEmpty(painelName))
            {
                panels = panels.Where(where =>
                    ((canvasId == 0) ||
                        (where.CanvasId == canvasId)) &&
                    (string.IsNullOrEmpty(painelName) ||
                        where.Name.ToLower().Contains(painelName)));
            }

            return panels;
        }

        public IEnumerable<Panel> AllByCanvas(int id) =>
            _unitOfWork.PanelRepository.Where(where => where.CanvasId == id);

        public Panel SingleOrDefault(int id) =>
            _unitOfWork.PanelRepository.SingleOrDefault(single => single.PanelId == id);

        public Panel SingleOrDefaultByCanvas(int id) =>
            _unitOfWork.PanelRepository.SingleOrDefault(single => single.CanvasId == id);
    }
}