﻿using System;
using System.Linq;
using System.Text;
using Domain.Models;
using Common.Helpers;
using static Common.Enums;
using System.Globalization;
using Repositories.Services;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace Repositories.Implementations
{
    public class CanvasImplementation : ICanvasService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IPanelService _panelService;

        public CanvasImplementation
            (IUnitOfWork unitOfWork,
            IPanelService panelService)
        {
            _unitOfWork = unitOfWork;
            _panelService = panelService;
        }

        public bool Add(Canvas canvas)
        {
            var fullscreenPanel = new Panel
            {
                Left = 0,
                Top = 0,
                Height = canvas.Height,
                Width = canvas.Width,
                Name = "FullScreens",
                Canvas = canvas
            };

            fullscreenPanel.FullScreen = new FullScreen
            {
                PanelId = fullscreenPanel.PanelId
            };

            canvas.Panel.Add(fullscreenPanel);

            _unitOfWork.CanvasRepository.Add(canvas);

            if (_unitOfWork.Save() == 0)
            {
                return false;
            }

            return true;
        }

        public bool Edit(Canvas canvas)
        {
            _unitOfWork.CanvasRepository.Edit(canvas);

            if (_unitOfWork.Save() == 0)
            {
                return false;
            }

            return true;
        }

        public bool Remove(int id)
        {
            _unitOfWork.CanvasRepository.Remove
                (_unitOfWork.CanvasRepository.SingleOrDefault(single => single.CanvasId == id));

            if (_unitOfWork.Save() == 0)
            {
                return false;
            }

            return true;
        }

        public IEnumerable<Canvas> All() =>
            _unitOfWork.CanvasRepository.All();
        
        public Canvas SingleOrDefault(int id) =>
            _unitOfWork.CanvasRepository.SingleOrDefault(single => single.CanvasId == id);

        public string PresentationHead(Display displayModel, string scheme, string host, Func<string, string> url)
        {
            var head = new StringBuilder();

            // add meta
            head.AppendFormat(CultureInfo.InvariantCulture, "<meta name=\"server-latitude\" content=\"{0}\" />\n", ServerGeoData.Latitude);
            head.AppendFormat(CultureInfo.InvariantCulture, "<meta name=\"server-longitude\" content=\"{0}\" />\n", ServerGeoData.Longitude);
            head.AppendFormat(CultureInfo.InvariantCulture, "<meta name=\"server-time-zone\" content=\"{0}\" />\n", ServerGeoData.TimeZone.Id);
            head.AppendFormat(CultureInfo.InvariantCulture, "<meta name=\"server-offset-gmt\" content=\"{0}\" />\n", ServerGeoData.OffsetGMT);
            head.AppendFormat(CultureInfo.InvariantCulture, "<meta name=\"server-external-ip\" content=\"{0}\" />\n", ServerGeoData.ServerExternalIPAddress.ToString());
            head.Append("<meta name=\"apple-mobile-web-app-capable\" content=\"yes\" />\n");
            head.Append("<meta name=\"apple-mobile-web-app-status-bar-style\" content=\"black\" />\n");

            // add styles
            head.Append("<link rel=\"stylesheet\" href=\"" + scheme + "://" + host + url("/styles/style.css") + "\" type=\"text/css\" />\n");
            head.Append("<link rel=\"stylesheet\" href=\"" + scheme + "://" + host + url("/styles/custom.css") + "\" type=\"text/css\" />\n");
            head.Append("<style type=\"text/css\">\n");

            foreach (var panel in _panelService.AllByCanvas(displayModel.CanvasId))
            {
                head.Append(Style(panel));
            }

            head.Append("</style>\n");

            // scripts
            foreach (string js in JsLibs(scheme, host, url))
            {
                head.AppendFormat("<script src=\"{0}\" type=\"text/javascript\" charset=\"utf-8\"></script>\n", js);
            }

            head.Append("<script type=\"text/javascript\" charset=\"utf-8\">\r\n<!--\r\n_canvas=new DM.Canvas({\n");
            head.AppendFormat(CultureInfo.InvariantCulture, "displayId:{0},\n", displayModel.DisplayId);
            head.AppendFormat(CultureInfo.InvariantCulture, "temperatureUnit:'{0}',\n", displayModel.Location.TemperatureUnit);
            head.AppendFormat(CultureInfo.InvariantCulture, "dateFormat:'{0}',\n", displayModel.Location.DateFormat);
            head.AppendFormat(CultureInfo.InvariantCulture, "timeFormat:'{0}',\n", displayModel.Location.TimeFormat);
            head.AppendFormat(CultureInfo.InvariantCulture, "latitude:{0},\n", displayModel.Location.Latitude ?? default);
            head.AppendFormat(CultureInfo.InvariantCulture, "longitude:{0},\n", displayModel.Location.Longitude ?? default);
            head.AppendFormat(CultureInfo.InvariantCulture, "woeid:{0},\n", displayModel.Location.Woeid ?? default);
            head.AppendFormat(CultureInfo.InvariantCulture, "culture:'{0}',\n", displayModel.Location.Culture);
            head.AppendFormat(CultureInfo.InvariantCulture, "locationTime:'{0}',\n", TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, TimeZoneInfo.FindSystemTimeZoneById(displayModel.Location.TimeZone ?? ServerGeoData.TimeZone.Id)));
            head.AppendFormat(CultureInfo.InvariantCulture, "utcTime:'{0}',\n", DateTime.UtcNow);
            head.AppendFormat(CultureInfo.InvariantCulture, "width:{0},\n", displayModel.Canvas.Width);
            head.AppendFormat(CultureInfo.InvariantCulture, "height:{0},\n", displayModel.Canvas.Height);

            if (displayModel.Canvas.BackgroundImage > 0)
            {
                head.AppendFormat(CultureInfo.InvariantCulture, "backImage:{0},\n", displayModel.Canvas.BackgroundImage);
            }

            if (displayModel.Canvas.BackgroundColor != "")
            {
                head.AppendFormat(CultureInfo.InvariantCulture, "backColor:'{0}',\n", displayModel.Canvas.BackgroundColor);
            }

            head.AppendFormat(CultureInfo.InvariantCulture, "pollInterval:{0},\n", displayModel.PollInterval);
            head.AppendFormat(CultureInfo.InvariantCulture, "errorLength:{0},\n", displayModel.ErrorLength);
            head.AppendFormat(CultureInfo.InvariantCulture, "noScroll:{0},\n", displayModel.NoScroll ? "true" : "false");
            head.AppendFormat(CultureInfo.InvariantCulture, "noCursor:{0},\n", displayModel.NoCursor ? "true" : "false");
            head.AppendFormat(CultureInfo.InvariantCulture, "readyTimeout:{0},\n", displayModel.ReadyTimeout);
            head.Append("});\n-->\r\n</script>\n<style></style>\n");

            return head.ToString();
        }

        public string PresentationBody(Display displayModel)
        {
            StringBuilder body = new StringBuilder();
            body.AppendFormat(CultureInfo.InvariantCulture,
                    "<div id=\"segments\" style=\"width:{0}px;height:{1}px;\">\n",
                    displayModel.Canvas.Width,
                    displayModel.Canvas.Height
            );

            foreach (var panel in _panelService.AllByCanvas(displayModel.CanvasId))
            {
                if (panel.FullScreen != null)
                    body.Insert(0, FullscreenPanel(panel));
                else
                    body.Append(Panel(panel));
            }

            body.Append("</div>");

            return body.ToString();
        }

        private string[] JsLibs(string scheme, string host, Func<string, string> url)
        {
            return new string[]
            {
                // prototype & script-aculo-us
                scheme + "://" + host + url("/scripts/prototype.js"),
                scheme + "://" + host + url("/scripts/scriptaculous/scriptaculous.js"),

                // moment.js
                scheme + "://" + host + url("/scripts/moment-with-locales.js"),

                // canvas:
			    scheme + "://" + host + url("/files/js/canvas.js")
			
                // frame scripts
			    , scheme + "://" + host + url("/files/js/clock.js")
                , scheme + "://" + host + url("/files/js/iframe.js")
                , scheme + "://" + host + url("/files/js/memo.js")
                , scheme + "://" + host + url("/files/js/outlook.js")
                , scheme + "://" + host + url("/files/js/picture.js")
                , scheme + "://" + host + url("/files/js/video.js")
                , scheme + "://" + host + url("/files/js/weather.js")
                , scheme + "://" + host + url("/files/js/youtube.js")
                , scheme + "://" + host + url("/files/js/powerbi.js")
            };
        }

        private string Style(Panel panel)
        {
            return string.Format
                ("#panel{0}, #x_panel{0} {{position:absolute;overflow:hidden;margin:auto;top:{1}px;left:{2}px;width:{3}px;height:{4}px;}}\n",
                    panel.PanelId,
                    panel.Top,
                    panel.Left,
                    panel.Width,
                    panel.Height);
        }

        private string Panel(Panel panel)
        {
            return new StringBuilder()
                .AppendFormat(CultureInfo.InvariantCulture,
                    "<div class=\"panel\" id=\"panel{0}\" data-panel-id=\"{0}\" data-panel-width=\"{1}\" data-panel-height=\"{2}\" data-fade-length=\"{3}\"></div>\n",
                    panel.PanelId,
                    panel.Width,
                    panel.Height,
                    panel.FadeLength)
                .ToString();
        }

        private string FullscreenPanel(Panel panel)
        {
            return new StringBuilder()
                .AppendFormat(CultureInfo.InvariantCulture,
                    "<div id=\"screen\"><div class=\"fullpanel\" id=\"full\" data-panel-id=\"{0}\" data-panel-width=\"{1}\" data-panel-height=\"{2}\" data-fade-length=\"{3}\"></div></div>\n",
                    panel.PanelId,
                    panel.Width,
                    panel.Height,
                    panel.FadeLength)
                .ToString();
        }
    }
}