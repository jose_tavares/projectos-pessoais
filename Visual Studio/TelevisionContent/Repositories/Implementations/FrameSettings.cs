﻿using System;
using AutoMapper;
using System.Linq;
using Domain.Models;
using Common.Extensions;
using static Common.Enums;
using Repositories.Services;
using Domain.ViewModels.Management;

namespace Repositories.Implementations
{
    public class FrameSettings
    {
        private readonly IMapper _mapper;
        private readonly IFrameService _frameService;
        private readonly ICanvasService _canvasService;
        private readonly ITemplateService _templateService;

        public FrameSettings
            (IMapper mapper,
            IFrameService frameService,
            ICanvasService canvasService,
            ITemplateService templateService)
        {
            _mapper = mapper;
            _frameService = frameService;
            _canvasService = canvasService;
            _templateService = templateService;
        }

        public FramePageViewModel GetViewModel
            (int panelId = 0,
            int canvasId = 0,
            FrameTypes? frameType = null,
            TimingOptions? timingOptions = null)
        {
            var framesViewModel = _frameService.All(panelId, canvasId, frameType, timingOptions)
                    .OrderBy(frame => frame.Panel.Canvas.Name)
                    .ThenBy(frame => frame.Panel.Name)
                    .ThenBy(frame => (frame.Sort == null) ? frame.FrameId : (float)frame.Sort)
                    .ThenBy(frame => frame.FrameId)
                    .Select(select => _mapper.Map<Frame, FrameViewModel>(select)).ToList();

            foreach (var frameViewModel in framesViewModel)
            {
                frameViewModel.Icon = IconFromFrameType(_templateService.SingleOrDefault(frameViewModel.TemplateId).FrameType);

                frameViewModel.Content = _frameService.GetContentByFrame(frameViewModel.FrameId);

                frameViewModel.ContentId = _frameService.GetContentIdByFrame(frameViewModel.FrameId);
            }

            return new FramePageViewModel
            {
                FramesViewModel = framesViewModel,

                SelectListCanvases = _canvasService.All().ToSelectListItems
                    (text => text.Name,
                    value => value.CanvasId.ToString(),
                    isSelected => isSelected.CanvasId == canvasId,
                    defaultTextOption: "Todos"),

                SelectListFrameTypes = Enum.GetValues(typeof(FrameTypes)).Cast<FrameTypes>().ToSelectListItems
                    (text => text.GetDescription(),
                    value => value.GetValue().ToString(),
                    isSelected => frameType.HasValue && isSelected == frameType.Value,
                    defaultTextOption: "Todos"),

                SelectListTimingOptions = Enum.GetValues(typeof(TimingOptions)).Cast<TimingOptions>().ToSelectListItems
                    (text => text.GetDescription(),
                    value => value.GetValue().ToString(),
                    isSelected => timingOptions.HasValue && isSelected == timingOptions.Value,
                    defaultTextOption: "Todos")
            };
        }

        private static string IconFromFrameType(int? _frameType)
        {
            if (_frameType.HasValue) switch (_frameType.Value)
                {
                    case (int)FrameTypes.Clock:
                        return "/images/clock.png";

                    case (int)FrameTypes.Html:
                        return "/images/html.png";

                    case (int)FrameTypes.Memo:
                        return "/images/memo.png";

                    case (int)FrameTypes.Outlook:
                        return "/images/calendar.png";

                    case (int)FrameTypes.Picture:
                        return "/images/image.png";

                    case (int)FrameTypes.Powerbi:
                        return "/images/powerbi.png";

                    case (int)FrameTypes.Report:
                        return "/images/ssrs.png";

                    case (int)FrameTypes.Video:
                        return "/images/video_thmb.png";

                    case (int)FrameTypes.Weather:
                        return "/images/weather.png";

                    case (int)FrameTypes.Youtube:
                        return "/images/youtube.png";
                }

            return "/images/unknown.png";
        }
    }
}
