﻿using Domain.Models;
using Repositories.Services;
using System.Collections.Generic;

namespace Repositories.Implementations
{
    public class LocationImplementation : ILocationService
    {
        private readonly IUnitOfWork _unitOfWork;

        public LocationImplementation(IUnitOfWork unitOfWork) =>
            _unitOfWork = unitOfWork;

        public IEnumerable<Location> All() =>
            _unitOfWork.LocationRepository.All();

        public Location SingleOrDefault(int id) =>
            _unitOfWork.LocationRepository.SingleOrDefault(single => single.LocationId == id);
    }
}