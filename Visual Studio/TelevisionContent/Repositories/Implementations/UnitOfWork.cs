﻿using System;
using System.Linq;
using Domain.Models;
using Common.Extensions;
using Repositories.Services;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;

namespace Repositories.Implementations
{
    public class UnitOfWork : IUnitOfWork
    {
        private bool _disposed = false;

        private readonly DisplayMonkeyContext _context = new DisplayMonkeyContext();

        #region IGeneric Properties
        public IGeneric<Html> HtmlRepository { get; }

        public IGeneric<Memo> MemoRepository { get; }

        public IGeneric<Clock> ClockRepository { get; }

        public IGeneric<Frame> FrameRepository { get; }

        public IGeneric<Level> LevelRepository { get; }

        public IGeneric<Panel> PanelRepository { get; }

        public IGeneric<Video> VideoRepository { get; }

        public IGeneric<Canvas> CanvasRepository { get; }
        
        public IGeneric<Report> ReportRepository { get; }        

        public IGeneric<Content> ContentRepository { get; }
        
        public IGeneric<Display> DisplayRepository { get; }

        public IGeneric<Outlook> OutlookRepository { get; }

        public IGeneric<Picture> PictureRepository { get; }

        public IGeneric<Powerbi> PowerbiRepository { get; }

        public IGeneric<Weather> WeatherRepository { get; }
        
        public IGeneric<Youtube> YoutubeRepository { get; }

        public IGeneric<Location> LocationRepository { get; }

        public IGeneric<Template> TemplateRepository { get; }

        public IGeneric<Settings> SettingsRepository { get; }
        
        public IGeneric<FullScreen> FullScreenRepository { get; }

        public IGeneric<VideoAlternative> VideoAlternativeRepository { get; }
        #endregion

        public UnitOfWork()
        {
            HtmlRepository = new Generic<Html>(_context);

            MemoRepository = new Generic<Memo>(_context);

            ClockRepository = new Generic<Clock>(_context);
            
            FrameRepository = new Generic<Frame>(_context);

            LevelRepository = new Generic<Level>(_context);

            PanelRepository = new Generic<Panel>(_context);

            VideoRepository = new Generic<Video>(_context);
            
            CanvasRepository = new Generic<Canvas>(_context);

            ReportRepository = new Generic<Report>(_context);

            ContentRepository = new Generic<Content>(_context);

            DisplayRepository = new Generic<Display>(_context);

            OutlookRepository = new Generic<Outlook>(_context);

            PictureRepository = new Generic<Picture>(_context);

            PowerbiRepository = new Generic<Powerbi>(_context);

            WeatherRepository = new Generic<Weather>(_context);

            YoutubeRepository = new Generic<Youtube>(_context);

            LocationRepository = new Generic<Location>(_context);

            TemplateRepository = new Generic<Template>(_context);

            SettingsRepository = new Generic<Settings>(_context);

            FullScreenRepository = new Generic<FullScreen>(_context);

            VideoAlternativeRepository = new Generic<VideoAlternative>(_context);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public int Save()
        {
            var entites = _context.ChangeTracker.Entries()
                          .Where(where => (where.State == EntityState.Added) || (where.State == EntityState.Modified))
                          .Select(select => select.Entity);

            if (entites.Validate())
            {
                foreach (var entity in entites)
                {
                    var validationContext = new ValidationContext(entity);

                    Validator.ValidateObject(entity, validationContext, true);
                }
            }

            return _context.SaveChanges();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }

            _disposed = true;
        }
    }
}