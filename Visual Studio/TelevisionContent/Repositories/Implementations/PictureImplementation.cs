﻿using System;
using Domain.Models;
using Repositories.Services;
using System.Collections.Generic;

namespace Repositories.Implementations
{
    public class PictureImplementation : IPictureService
    {
        private readonly IUnitOfWork _unitOfWork;

        public PictureImplementation(IUnitOfWork unitOfWork) =>
            _unitOfWork = unitOfWork;

        public bool Add(Picture picture)
        {
            if ((picture == null) && (picture.Frame == null))
            {
                return false;
            }

            picture.Frame.DateCreated = DateTime.Now;

            _unitOfWork.PictureRepository.Add(picture);

            if (_unitOfWork.Save() == 0)
            {
                return false;
            }

            return true;
        }

        public bool Edit(Picture picture)
        {
            _unitOfWork.PictureRepository.Edit(picture);
            _unitOfWork.FrameRepository.Edit
                (_unitOfWork.FrameRepository.SingleOrDefault(single => single.FrameId == picture.FrameId));

            if (_unitOfWork.Save() == 0)
            {
                return false;
            }

            return true;
        }

        public bool Remove(int id)
        {
            _unitOfWork.FrameRepository.Remove
                (_unitOfWork.FrameRepository.SingleOrDefault(single => single.FrameId == id));

            if (_unitOfWork.Save() == 0)
            {
                return false;
            }

            return true;
        }

        public IEnumerable<Picture> All() =>
            _unitOfWork.PictureRepository.All();

        public IEnumerable<Picture> AllByContent(int id) =>
            _unitOfWork.PictureRepository.Where(where => where.ContentId == id);

        public Picture SingleOrDefault(int id) =>
            _unitOfWork.PictureRepository.SingleOrDefault(single => single.FrameId == id);
    }
}