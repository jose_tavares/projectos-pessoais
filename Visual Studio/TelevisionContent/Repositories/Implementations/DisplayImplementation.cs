﻿using System;
using System.Linq;
using Domain.Models;
using Common.Extensions;
using Repositories.Services;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace Repositories.Implementations
{
    public class DisplayImplementation : IDisplayService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILocationService _locationService;

        public DisplayImplementation
            (IUnitOfWork unitOfWork,
            ILocationService locationService)
        {
            _unitOfWork = unitOfWork;
            _locationService = locationService;
        }

        public bool Add(Display display)
        {
            _unitOfWork.DisplayRepository.Add(display);

            if (_unitOfWork.Save() == 0)
            {
                return false;
            }

            return true;
        }

        public bool Edit(Display display)
        {
            _unitOfWork.DisplayRepository.Edit(display);

            if (_unitOfWork.Save() == 0)
            {
                return false;
            }

            return true;
        }

        public bool Remove(int id)
        {
            _unitOfWork.DisplayRepository.Remove
                (_unitOfWork.DisplayRepository.SingleOrDefault(single => single.DisplayId == id));

            if (_unitOfWork.Save() == 0)
            {
                return false;
            }

            return true;
        }

        public IEnumerable<Display> All
            (int canvasId = 0,
            int locationId = 0,
            string name = null,
            string host = null)
        {
            var displays = _unitOfWork.DisplayRepository.All();

            if (canvasId > 0)
            {
                displays = displays.Where(where => where.CanvasId == canvasId);
            }

            if (locationId > 0)
            {
                var treeLocations = _locationService.All()
                    .FirstOrDefault(single => single.LocationId == locationId)
                    .SelfAndChildren(self => self.InverseArea)
                    .Select(select => select.LocationId);

                displays = displays.Where(where =>
                    treeLocations.Contains(where.LocationId));
            }

            if (!string.IsNullOrEmpty(name))
            {
                displays = displays.Where(where =>
                    where.Name.ToLower().Contains(name.ToLower())
                );
            }

            if (!string.IsNullOrEmpty(host))
            {
                displays = displays.Where(where =>
                    where.Host.ToLower().Contains(host.ToLower())
                );
            }

            return displays;
        }

        public Display SingleOrDefault(int id) =>
            _unitOfWork.DisplayRepository.SingleOrDefault(single => single.DisplayId == id);

        public Display SingleOrDefaultByCanvas(int id) =>
            _unitOfWork.DisplayRepository.SingleOrDefault(single => single.CanvasId == id);
    }
}
