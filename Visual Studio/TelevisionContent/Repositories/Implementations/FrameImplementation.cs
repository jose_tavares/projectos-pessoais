﻿using System;
using System.Linq;
using Domain.Models;
using Common.Extensions;
using static Common.Enums;
using Repositories.Services;
using System.Collections.Generic;

namespace Repositories.Implementations
{
    public class FrameImplementation : IFrameService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IContentService _contentService;
        private readonly ITemplateService _templateService;

        public FrameImplementation
            (IUnitOfWork unitOfWork,
            IContentService contentService,
            ITemplateService templateService)
        {
            _unitOfWork = unitOfWork;
            _contentService = contentService;
            _templateService = templateService;
        }

        public IEnumerable<Frame> All
            (int panelId = 0,
            int canvasId = 0,
            FrameTypes? frameType = null,
            TimingOptions? timingOptions = null)
        {
            var frames = _unitOfWork.FrameRepository.All();

            if (timingOptions.HasValue)
            {
                switch (timingOptions.Value)
                {
                    case TimingOptions.Active:
                        frames = frames.Where(where =>
                            ((where.BeginsOn == null) ||
                                (where.BeginsOn <= DateTime.Now)) &&
                            ((where.EndsOn == null) ||
                                (where.EndsOn > DateTime.Now)
                        ));
                        break;

                    case TimingOptions.Expired:
                        frames = frames.Where
                            (where => where.EndsOn <= DateTime.Now);
                        break;

                    case TimingOptions.Pending:
                        frames = frames.Where
                            (where => DateTime.Now <= where.BeginsOn);
                        break;

                    default:
                        break;
                }
            }

            if (canvasId > 0)
            {
                frames = frames.Where(where => where.Panel.CanvasId == canvasId);
            }

            if (panelId > 0)
            {
                frames = frames.Where(where => where.PanelId == panelId);
            }

            if (frameType.HasValue)
            {
                var templates = _templateService.GetByFrameType((int)frameType.Value);

                var framesTemp = new List<Frame>();

                if (templates.Validate())
                {
                    foreach (var template in templates)
                    {
                        if (template.Frame.Validate())
                        {
                            framesTemp.AddRange(template.Frame);
                        }
                    }
                }

                frames = frames.Where(where =>
                    framesTemp.Any(any => any.TemplateId == where.TemplateId)
                );
            }

            return frames;
        }

        public string GetContentByFrame(int id)
        {
            var frame = _unitOfWork.FrameRepository.SingleOrDefault(single => single.FrameId == id);

            if (frame == null)
            {
                throw new ArgumentException($"Não existe frame com id {id}");
            }

            return GetContentBySpecificFrame(GetSpeficicationFrame(frame));
        }

        public int? GetContentIdByFrame(int id)
        {
            var frame = _unitOfWork.FrameRepository.SingleOrDefault(single => single.FrameId == id);

            if (frame == null)
            {
                throw new ArgumentException($"Não existe frame com id {id}");
            }

            return GetContentIdBySpecificFrame(GetSpeficicationFrame(frame));
        }

        public FrameTypes? GetType(int id)
        {
            var frame = _unitOfWork.FrameRepository.SingleOrDefault(single => single.FrameId == id);

            if (frame == null)
            {
                return null;
            }

            return GetTypeBySpecificFrame(GetSpeficicationFrame(frame));
        }

        public object GetSpeficicationFrame(Frame frame)
        {
            if (_unitOfWork.HtmlRepository.SingleOrDefault(single => single.FrameId == frame.FrameId) != null)
            {
                var foundHtml = _unitOfWork.HtmlRepository.SingleOrDefault(single => single.FrameId == frame.FrameId);

                return foundHtml;
            }

            else if (_unitOfWork.MemoRepository.SingleOrDefault(single => single.FrameId == frame.FrameId) != null)
            {
                var foundMemo = _unitOfWork.MemoRepository.SingleOrDefault(single => single.FrameId == frame.FrameId);

                return foundMemo;
            }

            else if (_unitOfWork.OutlookRepository.SingleOrDefault(single => single.FrameId == frame.FrameId) != null)
            {
                var foundOutlook = _unitOfWork.OutlookRepository.SingleOrDefault(single => single.FrameId == frame.FrameId);

                return foundOutlook;
            }

            else if (_unitOfWork.PictureRepository.SingleOrDefault(single => single.FrameId == frame.FrameId) != null)
            {
                var foundPicture = _unitOfWork.PictureRepository.SingleOrDefault(single => single.FrameId == frame.FrameId);

                return foundPicture;
            }

            else if (_unitOfWork.PowerbiRepository.SingleOrDefault(single => single.FrameId == frame.FrameId) != null)
            {
                var foundPowerbi = _unitOfWork.PowerbiRepository.SingleOrDefault(single => single.FrameId == frame.FrameId);

                return foundPowerbi;
            }

            else if (_unitOfWork.ReportRepository.SingleOrDefault(single => single.FrameId == frame.FrameId) != null)
            {
                var foundReport = _unitOfWork.ReportRepository.SingleOrDefault(single => single.FrameId == frame.FrameId);

                return foundReport;
            }

            else if (_unitOfWork.VideoAlternativeRepository.SingleOrDefault(single => single.FrameId == frame.FrameId) != null)
            {
                var foundVideo = _unitOfWork.VideoAlternativeRepository.SingleOrDefault(single => single.FrameId == frame.FrameId);

                return foundVideo;
            }

            else if (_unitOfWork.WeatherRepository.SingleOrDefault(single => single.FrameId == frame.FrameId) != null)
            {
                var foundWeather = _unitOfWork.WeatherRepository.SingleOrDefault(single => single.FrameId == frame.FrameId);

                return foundWeather;
            }

            else if (_unitOfWork.YoutubeRepository.SingleOrDefault(single => single.FrameId == frame.FrameId) != null)
            {
                var foundYoutube = _unitOfWork.YoutubeRepository.SingleOrDefault(single => single.FrameId == frame.FrameId);

                return foundYoutube;
            }

            else if (_unitOfWork.ClockRepository.SingleOrDefault(single => single.FrameId == frame.FrameId) != null)
            {
                var foundYoutube = _unitOfWork.ClockRepository.SingleOrDefault(single => single.FrameId == frame.FrameId);

                return foundYoutube;
            }

            return null;
        }

        public Frame SingleOrDefault(int id) =>
            _unitOfWork.FrameRepository.SingleOrDefault(single => single.FrameId == id);

        private string GetContentBySpecificFrame(object obj)
        {
            string contentValue = null;

            new Switch(obj)
                .Case<Html>(action =>
                    contentValue = action.Name
                )
                .Case<Memo>(action =>
                    contentValue = action.Subject
                )
                .Case<Outlook>(action =>
                    contentValue = action.Name
                )
                .Case<Picture>(action =>
                {
                    var content = _unitOfWork.ContentRepository.SingleOrDefault(single => single.ContentId == action.ContentId);

                    contentValue = content?.Name;
                })
                .Case<Powerbi>(action =>
                    contentValue = action.Name
                )
                .Case<Report>(action =>
                    contentValue = action.Name
                )
                .Case<VideoAlternative>(action =>
                {
                    var content = _unitOfWork.ContentRepository.SingleOrDefault(single => single.ContentId == action.ContentId);

                    contentValue = content?.Name;
                })
                .Case<Weather>(action =>
                    contentValue = "Clima"
                )
                .Case<Youtube>(action =>
                    contentValue = action.Name
                );

            return contentValue;
        }

        private int? GetContentIdBySpecificFrame(object obj)
        {
            int? contentId = null;

            new Switch(obj)
                .Case<Picture>(action =>
                {
                    var content = _contentService.SingleOrDefault(action.ContentId);

                    contentId = content?.ContentId;
                })
                /*Incluir para novos tipos de frame (video, html, youtube, etc...)*/;

            return contentId;
        }

        private FrameTypes? GetTypeBySpecificFrame(object obj)
        {
            FrameTypes? getType = null;

            new Switch(obj)
                .Case<Clock>(action =>
                    getType = FrameTypes.Clock
                )
                .Case<Html>(action =>
                    getType = FrameTypes.Html
                )
                .Case<Memo>(action =>
                    getType = FrameTypes.Memo
                )
                .Case<Outlook>(action =>
                    getType = FrameTypes.Outlook
                )
                .Case<Picture>(action =>
                    getType = FrameTypes.Picture
                )
                .Case<Powerbi>(action =>
                    getType = FrameTypes.Powerbi
                )
                .Case<Report>(action =>
                    getType = FrameTypes.Report
                )
                .Case<VideoAlternative>(action =>
                    getType = FrameTypes.Video
                )
                .Case<Weather>(action =>
                    getType = FrameTypes.Weather
                )
                .Case<Youtube>(action =>
                    getType = FrameTypes.Youtube
                );

            return getType;
        }
    }
}