﻿using System;
using System.Linq;
using Domain.Models;
using Repositories.Services;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace Repositories.Implementations
{
    public class Generic<TEntity> : IGeneric<TEntity> where TEntity : class
    {
        private readonly DbContext _context;
        private readonly DbSet<TEntity> _entities;

        public Generic(DisplayMonkeyContext context)
        {
            _context = context;
            _entities = _context.Set<TEntity>();
        }

        public virtual void Add(TEntity entity) =>
            _entities.Add(entity);

        public virtual void Remove(TEntity entity) =>
            _entities.Remove(entity);

        public void RemoveAll(IEnumerable<TEntity> entities) =>
            _entities.RemoveRange(entities);

        public virtual void Edit(TEntity entity)
        {
            _entities.Attach(entity);
            _context.Entry(entity).State = EntityState.Modified;
        }

        public virtual IEnumerable<TEntity> Where(Expression<Func<TEntity, bool>> predicate) =>
            _entities.Where(predicate);

        public virtual TEntity SingleOrDefault(Expression<Func<TEntity, bool>> predicate) =>
            _entities.SingleOrDefault(predicate);

        public virtual IEnumerable<TEntity> All() => _entities;

        public virtual async Task<TEntity> FindAsync(params object[] keyValues) =>
            await _entities.FindAsync(keyValues);
    }
}