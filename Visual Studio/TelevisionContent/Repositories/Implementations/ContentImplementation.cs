﻿using Domain.Models;
using Common.Extensions;
using static Common.Enums;
using Repositories.Services;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Repositories.Implementations
{
    public class ContentImplementation : IContentService
    {
        private readonly IUnitOfWork _unitOfWork;

        public ContentImplementation
            (IUnitOfWork unitOfWork) =>
                _unitOfWork = unitOfWork;

        public bool Add(Content content)
        {
            _unitOfWork.ContentRepository.Add(content);

            if (_unitOfWork.Save() == 0)
            {
                return false;
            }

            return true;
        }

        public bool Edit(Content content)
        {
            _unitOfWork.ContentRepository.Edit(content);

            if (_unitOfWork.Save() == 0)
            {
                return false;
            }

            return true;
        }

        public bool Remove(Content content)
        {
            if (content.Picture.Validate())
            {
                var framePictures = new List<Frame>();

                foreach (var picture in content.Picture)
                {
                    var foundFramePictures = _unitOfWork.FrameRepository.Where(where => where.FrameId == picture.FrameId);

                    if (foundFramePictures.Validate())
                    {
                        framePictures.AddRange(foundFramePictures);
                    }
                }

                _unitOfWork.FrameRepository.RemoveAll(framePictures);
            }

            if (content.VideoAlternative.Validate())
            {
                var frameVideos = new List<Frame>();

                foreach (var video in content.VideoAlternative)
                {
                    var foundFrameVideos = _unitOfWork.FrameRepository.Where(where => where.FrameId == video.FrameId);

                    if (foundFrameVideos.Validate())
                    {
                        frameVideos.AddRange(foundFrameVideos);
                    }
                }

                _unitOfWork.FrameRepository.RemoveAll(frameVideos);
            }

            _unitOfWork.ContentRepository.Remove(content);

            if (_unitOfWork.Save() == 0)
            {
                return false;
            }

            return true;
        }

        public IEnumerable<Content> All(ContentTypes? contentTypes = null) =>
            (contentTypes == null) ?
                _unitOfWork.ContentRepository.All() :
                _unitOfWork.ContentRepository.Where(where => where.Type == (int)contentTypes);

        public Content SingleOrDefault(int id) =>
            _unitOfWork.ContentRepository.SingleOrDefault(single => single.ContentId == id);

        public async Task<Content> SingleOrDefaultAsync(int id) =>
            await _unitOfWork.ContentRepository.FindAsync(id);
    }
}