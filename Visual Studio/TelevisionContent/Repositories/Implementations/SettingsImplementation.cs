﻿using System;
using System.Linq;
using static Common.Enums;
using Repositories.Services;

namespace Repositories.Implementations
{
    public class SettingsImplementation : ISettingsService
    {
        private readonly IUnitOfWork _unitOfWork;

        public SettingsImplementation(IUnitOfWork unitOfWork) =>
            _unitOfWork = unitOfWork;

        public DisplayAutoLoadModes AutoLoadMode()
        {
            var setting = _unitOfWork.SettingsRepository.SingleOrDefault(single => single.Key == new Guid("AE1B2F10-9EC3-4429-97B5-C12D64575C41"));

            if (setting == null)
            {
                return DisplayAutoLoadModes.IP;
            }

            return (DisplayAutoLoadModes)
                    (((setting.Value == null) || 
                        (setting.Value.Length == 0))
                            ? 0 :
                            BitConverter.ToInt32(setting.Value.Reverse().ToArray(), 0));
        }
    }
}
