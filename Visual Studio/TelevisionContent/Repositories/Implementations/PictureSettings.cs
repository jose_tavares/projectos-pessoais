﻿using System;
using System.Linq;
using Common.Extensions;
using static Common.Enums;
using Repositories.Services;
using Domain.ViewModels.Management;

namespace Repositories.Implementations
{
    public class PictureSettings
    {
        private readonly IContentService _contentService;
        private readonly ITemplateService _templateService;

        public PictureSettings
            (IContentService contentService,
            ITemplateService templateService)
        {
            _contentService = contentService;
            _templateService = templateService;
        }

        public void SetSelectListItems(PictureViewModel pictureViewModel)
        {
            pictureViewModel.SelectListPictures = _contentService.All(ContentTypes.Picture).ToSelectListItems
                (text => text.Name,
                value => value.ContentId.ToString(),
                defaultTextOption: "Selecionar");

            pictureViewModel.SelectListTemplates = _templateService.GetByFrameType((int)FrameTypes.Picture)
                .OrderBy(orderBy => orderBy.Name)
                .ToSelectListItems
                    (text => text.Name,
                    value => value.TemplateId.ToString());

            pictureViewModel.SelectListModes = Enum.GetValues(typeof(RenderModes)).Cast<RenderModes>().ToSelectListItems
                (text => text.GetDescription(),
                value => value.GetValue().ToString());
        }
    }
}
