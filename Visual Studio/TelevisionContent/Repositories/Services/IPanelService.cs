﻿using Domain.Models;
using System.Collections.Generic;

namespace Repositories.Services
{
    public interface IPanelService
    {
        bool Add(Panel panel);

        bool Edit(Panel panel);

        bool Remove(int id);

        IEnumerable<Panel> All(int canvasId = 0, string painelName = null);

        IEnumerable<Panel> AllByCanvas(int id);

        Panel SingleOrDefault(int id);

        Panel SingleOrDefaultByCanvas(int id);
    }
}
