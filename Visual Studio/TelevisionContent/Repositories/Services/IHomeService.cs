﻿using System.Collections.Generic;
using Domain.ViewModels.Management;

namespace Repositories.Services
{
    public interface IHomeService
    {
        string GetDurationHours();

        int GetCountLevels();

        int GetCountLocations();

        int GetCountCanvases();

        int GetCountPanels();

        int GetCountDisplays();

        int GetCountHtml(bool previousSevenDays = false);

        int GetCountMemos(bool previousSevenDays = false);

        int GetCountOutlook(bool previousSevenDays = false);

        int GetCountPowerbi(bool previousSevenDays = false);

        int GetCountReports(bool previousSevenDays = false);

        int GetCountVideos(bool previousSevenDays = false);

        int GetCountYoutube(bool previousSevenDays = false);

        int GetCountClock(bool previousSevenDays = false);

        int GetCountWeather(bool previousSevenDays = false);

        int GetCountPicture(bool previousSevenDays = false);

        IEnumerable<ContentViewModel> GetTopFiveContents();
    }
}
