﻿using System;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.Collections.Generic;

namespace Repositories.Services
{
    public interface IGeneric<TEntity> where TEntity : class
    {
        void Add(TEntity entity);

        void Remove(TEntity entity);

        void RemoveAll(IEnumerable<TEntity> entities);

        void Edit(TEntity entity);

        IEnumerable<TEntity> All();

        IEnumerable<TEntity> Where(Expression<Func<TEntity, bool>> predicate);

        TEntity SingleOrDefault(Expression<Func<TEntity, bool>> predicate);

        Task<TEntity> FindAsync(params object[] keyValues);
    }
}
