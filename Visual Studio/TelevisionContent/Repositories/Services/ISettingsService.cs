﻿using static Common.Enums;

namespace Repositories.Services
{
    public interface ISettingsService
    {
        DisplayAutoLoadModes AutoLoadMode();
    }
}
