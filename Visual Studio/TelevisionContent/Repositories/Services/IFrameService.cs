﻿using Domain.Models;
using static Common.Enums;
using System.Collections.Generic;

namespace Repositories.Services
{
    public interface IFrameService
    {
        IEnumerable<Frame> All
            (int panelId = 0,
            int canvasId = 0,
            FrameTypes? frameType = null,
            TimingOptions? timingOptions = null);

        Frame SingleOrDefault(int id);

        string GetContentByFrame(int id);

        int? GetContentIdByFrame(int id);

        FrameTypes? GetType(int id);

        object GetSpeficicationFrame(Frame frame);
    }
}
