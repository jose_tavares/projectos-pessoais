﻿using Domain.Models;
using System.Collections.Generic;

namespace Repositories.Services
{
    public interface IPictureService
    {
        bool Add(Picture picture);

        bool Remove(int id);

        bool Edit(Picture picture);

        IEnumerable<Picture> All();

        IEnumerable<Picture> AllByContent(int id);
        
        Picture SingleOrDefault(int id);
    }
}
