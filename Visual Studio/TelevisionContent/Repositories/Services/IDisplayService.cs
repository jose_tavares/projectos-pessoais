﻿using Domain.Models;
using System.Collections.Generic;

namespace Repositories.Services
{
    public interface IDisplayService
    {
        bool Add(Display display);

        bool Edit(Display display);

        bool Remove(int id);

        IEnumerable<Display> All
            (int canvasId = 0,
            int locationId = 0,
            string name = null,
            string host = null);

        Display SingleOrDefault(int id);

        Display SingleOrDefaultByCanvas(int id);
    }
}
