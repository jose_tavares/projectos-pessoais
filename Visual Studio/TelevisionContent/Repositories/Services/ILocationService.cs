﻿using Domain.Models;
using System.Collections.Generic;

namespace Repositories.Services
{
    public interface ILocationService
    {
        public IEnumerable<Location> All();

        Location SingleOrDefault(int id);
    }
}
