﻿using Domain.Models;
using System.Collections.Generic;

namespace Repositories.Services
{
    public interface ITemplateService
    {
        Template SingleOrDefault(int id);

        IEnumerable<Template> GetByFrameType(int id);
    }
}
