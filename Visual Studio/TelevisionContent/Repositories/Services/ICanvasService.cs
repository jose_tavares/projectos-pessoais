﻿using System;
using Domain.Models;
using static Common.Enums;
using System.Collections.Generic;

namespace Repositories.Services
{
    public interface ICanvasService
    {
        bool Add(Canvas canvas);

        bool Edit(Canvas canvas);

        bool Remove(int id);

        IEnumerable<Canvas> All();

        Canvas SingleOrDefault(int id);

        string PresentationHead(Display displayModel, string scheme, string host, Func<string, string> url);

        string PresentationBody(Display displayModel);
    }
}
