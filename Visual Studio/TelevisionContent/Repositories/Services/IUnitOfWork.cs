﻿using System;
using Domain.Models;

namespace Repositories.Services
{
    public interface IUnitOfWork : IDisposable
    {
        #region IGeneric Properties       
        IGeneric<Html> HtmlRepository { get; }

        IGeneric<Memo> MemoRepository { get; }
        
        IGeneric<Clock> ClockRepository { get; }

        IGeneric<Frame> FrameRepository { get; }

        IGeneric<Level> LevelRepository { get; }

        IGeneric<Panel> PanelRepository { get; }

        IGeneric<Video> VideoRepository { get; }

        IGeneric<Canvas> CanvasRepository { get; }

        IGeneric<Report> ReportRepository { get; }

        IGeneric<Content> ContentRepository { get; }

        IGeneric<Display> DisplayRepository { get; }

        IGeneric<Outlook> OutlookRepository { get; }

        IGeneric<Picture> PictureRepository { get; }

        IGeneric<Powerbi> PowerbiRepository { get; }

        IGeneric<Weather> WeatherRepository { get; }

        IGeneric<Youtube> YoutubeRepository { get; }

        IGeneric<Location> LocationRepository { get; }

        IGeneric<Template> TemplateRepository { get; }

        IGeneric<Settings> SettingsRepository { get; }

        IGeneric<FullScreen> FullScreenRepository { get; }

        IGeneric<VideoAlternative> VideoAlternativeRepository { get; }
        #endregion

        int Save();
    }
}
