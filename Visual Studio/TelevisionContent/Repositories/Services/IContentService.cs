﻿using Domain.Models;
using static Common.Enums;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Repositories.Services
{
    public interface IContentService
    {
        bool Add(Content content);

        bool Edit(Content content);

        bool Remove(Content content);

        IEnumerable<Content> All(ContentTypes? contentTypes = null);

        Content SingleOrDefault(int id);

        Task<Content> SingleOrDefaultAsync(int id);
    }
}
