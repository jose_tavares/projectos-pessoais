﻿namespace Infrastructure
{
    using Common.Extensions;
    using Repositories.Services;
    using Repositories.Implementations;
    using Microsoft.Extensions.DependencyInjection;

    public static class Initializer
    {
        public static void Configure(IServiceCollection services)
        {
            services.AddLazyResolution();

            services.AddScoped<IUnitOfWork, UnitOfWork>();

            services.AddScoped<IHomeService, HomeImplementation>();

            services.AddScoped<IFrameService, FrameImplementation>();

            services.AddScoped<IPanelService, PanelImplementation>();

            services.AddScoped<ICanvasService, CanvasImplementation>();

            services.AddScoped<IContentService, ContentImplementation>();

            services.AddScoped<IDisplayService, DisplayImplementation>();

            services.AddScoped<IPictureService, PictureImplementation>();

            services.AddScoped<ILocationService, LocationImplementation>();

            services.AddScoped<ISettingsService, SettingsImplementation>();

            services.AddScoped<ITemplateService, TemplateImplementation>();
        }
    }
}
