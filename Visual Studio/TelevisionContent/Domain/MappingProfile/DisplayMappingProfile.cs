﻿using AutoMapper;
using Domain.Models;
using Domain.ViewModels.Management;

namespace Domain.MappingProfile
{
    public class DisplayMappingProfile : Profile
    {
        public DisplayMappingProfile()
        {
            DomainToViewModel();

            ViewModelToDomain();
        }

        private void DomainToViewModel()
        {
            CreateMap<Display, DisplayViewModel>()
                .ForMember
                    (destination => destination.AutoRefreshAt,
                    source => source.MapFrom
                        (map => map.RecycleTime.HasValue ? map.RecycleTime.Value.ToString(@"h\:mm") : null));

            CreateMap<Canvas, DisplayViewModel>()
                .ForMember
                    (destination => destination.Canvas,
                    source => source.MapFrom
                        (map => map.Name))
                .ForMember
                    (destination => destination.Name,
                    source => source.Ignore());

            CreateMap<Location, DisplayViewModel>()
                .ForMember
                    (destination => destination.Location,
                    source => source.MapFrom
                        (map => map.Name))
                .ForMember
                    (destination => destination.Name,
                    source => source.Ignore());
        }

        private void ViewModelToDomain()
        {
            CreateMap<DisplayViewModel, Display>()
                .ForMember
                    (destination => destination.RecycleTime,
                    source => source.Ignore());

            CreateMap<DisplayViewModel, Canvas>()
                .ForMember
                    (destination => destination.Name,
                    source => source.MapFrom
                        (map => map.Canvas));

            CreateMap<DisplayViewModel, Location>()
                .ForMember
                    (destination => destination.Name,
                    source => source.MapFrom
                        (map => map.Location));
        }
    }
}
