﻿using AutoMapper;
using Domain.Models;
using Domain.ViewModels.Management;

namespace Domain.MappingProfile
{
    public class ContentMappingProfile : Profile
    {
        public ContentMappingProfile()
        {
            DomainToViewModel();

            ViewModelToDomain();
        }

        private void DomainToViewModel()
        {
            CreateMap<Content, ContentWithSizeViewModel>();

            CreateMap<Content, ContentViewModel>();
        }

        private void ViewModelToDomain()
        {
            CreateMap<ContentViewModel, Content>()
                .ForMember
                    (destination => destination.Type,
                    source => source.MapFrom
                        (map => (int)map.Type));
        }
    }
}
