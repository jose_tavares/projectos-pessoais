﻿using AutoMapper;
using Domain.Models;
using Domain.ViewModels.Management;

namespace Domain.MappingProfile
{
    public class PanelMappingProfile : Profile
    {
        public PanelMappingProfile()
        {
            DomainToViewModel();

            ViewModelToDomain();
        }

        private void DomainToViewModel()
        {
            CreateMap<Panel, PanelViewModel>()
                .ForMember
                    (destination => destination.FrameType,
                    source => source.Ignore());

            CreateMap<Canvas, PanelViewModel>()
                .ForMember
                    (destination => destination.Canvas,
                    source => source.MapFrom
                        (map => map.Name))
                .ForMember
                    (destination => destination.Name,
                    source => source.Ignore())

                .ForMember
                    (destination => destination.Width,
                    source => source.Ignore())

                .ForMember
                    (destination => destination.Height,
                    source => source.Ignore());

            CreateMap<FullScreen, PanelViewModel>()
                .ForMember
                    (destination => destination.Top,
                    source => source.Ignore())

                .ForMember
                    (destination => destination.Left,
                    source => source.Ignore())

                .ForMember
                    (destination => destination.Width,
                    source => source.Ignore())

                .ForMember
                    (destination => destination.Height,
                    source => source.Ignore());
        }

        private void ViewModelToDomain()
        {
            CreateMap<PanelViewModel, Panel>();

            CreateMap<PanelViewModel, FullScreen>();
        }
    }
}
