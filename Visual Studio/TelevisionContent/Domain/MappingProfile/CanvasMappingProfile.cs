﻿using AutoMapper;
using Domain.Models;
using Domain.ViewModels.Management;

namespace Domain.MappingProfile
{
    public class CanvasMappingProfile : Profile
    {
        public CanvasMappingProfile()
        {
            DomainToViewModel();

            ViewModelToDomain();
        }


        private void DomainToViewModel() =>
            CreateMap<Canvas, CanvasViewModel>();

        private void ViewModelToDomain() =>
            CreateMap<CanvasViewModel, Canvas>();
    }
}
