﻿using AutoMapper;
using Domain.Models;
using Domain.ViewModels.Management;

namespace Domain.MappingProfile
{
    public class FrameMappingProfile : Profile
    {
        public FrameMappingProfile()
        {
            DomainToViewModel();

            ViewModelToDomain();
        }

        private void DomainToViewModel() =>
            CreateMap<Frame, FrameViewModel>()
                .ForMember
                    (destination => destination.Canvas,
                    source => source.MapFrom
                        (map => ((map.Panel != null) && (map.Panel.Canvas != null)) ?
                            map.Panel.Canvas.Name : string.Empty))
                .ForMember
                    (destination => destination.CanvasId,
                    source => source.MapFrom
                        (map => ((map.Panel != null) && (map.Panel.Canvas != null)) ?
                            map.Panel.Canvas.CanvasId : 0))
                .ForMember
                    (destination => destination.Panel,
                    source => source.MapFrom
                        (map => (map.Panel != null) ?
                            map.Panel.Name : string.Empty))
                .ForMember
                    (destination => destination.Icon,
                    source => source.Ignore())

                .ForMember
                    (destination => destination.Content,
                    source => source.Ignore())

                .ForMember
                    (destination => destination.ContentId,
                    source => source.Ignore());

        private void ViewModelToDomain()
        {
        }
    }
}