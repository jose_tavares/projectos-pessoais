﻿using AutoMapper;
using Domain.Models;
using Domain.ViewModels.Management;

namespace Domain.MappingProfile
{
    public class PictureMappingProfile : Profile
    {
        public PictureMappingProfile()
        {
            DomainToViewModel();

            ViewModelToDomain();
        }

        private void DomainToViewModel()
        {
            CreateMap<Picture, PictureViewModel>();

            CreateMap<Frame, PictureViewModel>()
                .ForMember
                    (destination => destination.BeginsOn,
                    source => source.MapFrom
                        (map => map.BeginsOn))
                .ForMember
                    (destination => destination.EndsOn,
                    source => source.MapFrom
                        (map => map.EndsOn))
                .ForMember
                    (destination => destination.Sort,
                    source => source.MapFrom
                        (map => map.Sort))
                .ForMember
                    (destination => destination.CacheInterval,
                    source => source.MapFrom
                        (map => map.CacheInterval))
                .ForMember
                    (destination => destination.Duration,
                    source => source.MapFrom
                        (map => map.Duration))
                .ForMember
                    (destination => destination.PanelId,
                    source => source.MapFrom
                        (map => map.PanelId))
                .ForMember
                    (destination => destination.CanvasId,
                    source => source.MapFrom
                        (map => (map.Panel != null) ? map.Panel.CanvasId : 0));
        }

        private void ViewModelToDomain()
        {
            CreateMap<PictureViewModel, Picture>();

            CreateMap<PictureViewModel, Frame>()
                .ForMember
                    (destination => destination.BeginsOn,
                    source => source.MapFrom
                        (map => map.BeginsOn))
                .ForMember
                    (destination => destination.EndsOn,
                    source => source.MapFrom
                        (map => map.EndsOn))
                .ForMember
                    (destination => destination.CacheInterval,
                    source => source.MapFrom
                        (map => map.CacheInterval))
                .ForMember
                    (destination => destination.Duration,
                    source => source.MapFrom
                        (map => map.Duration))
                .ForMember
                    (destination => destination.PanelId,
                    source => source.MapFrom
                        (map => map.PanelId))
                .ForMember
                    (destination => destination.Sort,
                    source => source.MapFrom
                        (map => map.Sort))
                .ForMember
                    (destination => destination.TemplateId,
                    source => source.MapFrom
                        (map => map.TemplateId));
        }
    }
}
