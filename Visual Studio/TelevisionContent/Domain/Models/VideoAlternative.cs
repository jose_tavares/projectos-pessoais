﻿using System;
using System.Collections.Generic;

namespace Domain.Models
{
    public partial class VideoAlternative
    {
        public int FrameId { get; set; }
        public int ContentId { get; set; }

        public virtual Content Content { get; set; }
        public virtual Video Frame { get; set; }
    }
}
