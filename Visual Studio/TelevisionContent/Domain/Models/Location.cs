﻿using System;
using System.Collections.Generic;

namespace Domain.Models
{
    public partial class Location
    {
        public Location()
        {
            Display = new HashSet<Display>();
            FrameLocation = new HashSet<FrameLocation>();
            InverseArea = new HashSet<Location>();
        }

        public int LocationId { get; set; }
        public int LevelId { get; set; }
        public string Name { get; set; }
        public int? AreaId { get; set; }
        public string TemperatureUnit { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public string DateFormat { get; set; }
        public string TimeFormat { get; set; }
        public int? Woeid { get; set; }
        public byte[] Version { get; set; }
        public string Culture { get; set; }
        public string TimeZone { get; set; }

        public virtual Location Area { get; set; }
        public virtual Level Level { get; set; }
        public virtual ICollection<Display> Display { get; set; }
        public virtual ICollection<FrameLocation> FrameLocation { get; set; }
        public virtual ICollection<Location> InverseArea { get; set; }
    }
}
