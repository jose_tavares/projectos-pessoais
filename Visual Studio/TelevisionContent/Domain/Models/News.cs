﻿using System;
using System.Collections.Generic;

namespace Domain.Models
{
    public partial class News
    {
        public int FrameId { get; set; }
        public int Source { get; set; }

        public virtual Frame Frame { get; set; }
    }
}
