﻿using System;
using System.Collections.Generic;

namespace Domain.Models
{
    public partial class Level
    {
        public Level()
        {
            Location = new HashSet<Location>();
        }

        public int LevelId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Location> Location { get; set; }
    }
}
