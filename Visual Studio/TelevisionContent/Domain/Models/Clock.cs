﻿using System;
using System.Collections.Generic;

namespace Domain.Models
{
    public partial class Clock
    {
        public int FrameId { get; set; }
        public int Type { get; set; }
        public bool? ShowDate { get; set; }
        public bool? ShowTime { get; set; }
        public string Label { get; set; }
        public string TimeZone { get; set; }
        public bool? ShowSeconds { get; set; }

        public virtual Frame Frame { get; set; }
    }
}
