﻿using System;
using System.Collections.Generic;

namespace Domain.Models
{
    public partial class Memo
    {
        public int FrameId { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }

        public virtual Frame Frame { get; set; }
    }
}
