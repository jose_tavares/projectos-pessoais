﻿using System;
using System.Collections.Generic;

namespace Domain.Models
{
    public partial class Users
    {
        public string Uname { get; set; }
        public string Pwd { get; set; }
        public string UserRole { get; set; }
    }
}
