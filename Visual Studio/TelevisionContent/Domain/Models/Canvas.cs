﻿using System;
using System.Collections.Generic;

namespace Domain.Models
{
    public partial class Canvas
    {
        public Canvas()
        {
            Display = new HashSet<Display>();
            Panel = new HashSet<Panel>();
        }

        public int CanvasId { get; set; }
        public string Name { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
        public int? BackgroundImage { get; set; }
        public string BackgroundColor { get; set; }
        public byte[] Version { get; set; }

        public virtual Content BackgroundImageNavigation { get; set; }
        public virtual ICollection<Display> Display { get; set; }
        public virtual ICollection<Panel> Panel { get; set; }
    }
}
