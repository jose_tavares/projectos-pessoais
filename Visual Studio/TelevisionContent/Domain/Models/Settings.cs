﻿using System;
using System.Collections.Generic;

namespace Domain.Models
{
    public partial class Settings
    {
        public Guid Key { get; set; }
        public byte[] Value { get; set; }
    }
}
