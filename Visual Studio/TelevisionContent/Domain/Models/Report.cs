﻿using System;
using System.Collections.Generic;

namespace Domain.Models
{
    public partial class Report
    {
        public int FrameId { get; set; }
        public string Path { get; set; }
        public string Name { get; set; }
        public int Mode { get; set; }
        public int ServerId { get; set; }

        public virtual Frame Frame { get; set; }
        public virtual ReportServer Server { get; set; }
    }
}
