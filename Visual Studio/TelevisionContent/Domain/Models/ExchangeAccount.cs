﻿using System;
using System.Collections.Generic;

namespace Domain.Models
{
    public partial class ExchangeAccount
    {
        public ExchangeAccount()
        {
            Outlook = new HashSet<Outlook>();
        }

        public int AccountId { get; set; }
        public string Name { get; set; }
        public string Account { get; set; }
        public byte[] Password { get; set; }
        public int EwsVersion { get; set; }
        public string Url { get; set; }

        public virtual ICollection<Outlook> Outlook { get; set; }
    }
}
