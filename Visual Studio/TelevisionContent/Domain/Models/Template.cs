﻿using System;
using System.Collections.Generic;

namespace Domain.Models
{
    public partial class Template
    {
        public Template()
        {
            Frame = new HashSet<Frame>();
        }

        public int TemplateId { get; set; }
        public string Name { get; set; }
        public string Html { get; set; }
        public int FrameType { get; set; }
        public byte[] Version { get; set; }

        public virtual ICollection<Frame> Frame { get; set; }
    }
}
