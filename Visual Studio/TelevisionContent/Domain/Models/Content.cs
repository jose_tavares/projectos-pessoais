﻿using System;
using System.Collections.Generic;

namespace Domain.Models
{
    public partial class Content
    {
        public Content()
        {
            Canvas = new HashSet<Canvas>();
            Picture = new HashSet<Picture>();
            VideoAlternative = new HashSet<VideoAlternative>();
        }

        public int ContentId { get; set; }
        public string Name { get; set; }
        public byte[] Data { get; set; }
        public int Type { get; set; }
        public byte[] Version { get; set; }

        public virtual ICollection<Canvas> Canvas { get; set; }
        public virtual ICollection<Picture> Picture { get; set; }
        public virtual ICollection<VideoAlternative> VideoAlternative { get; set; }
    }
}
