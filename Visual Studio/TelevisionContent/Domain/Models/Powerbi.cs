﻿using System;
using System.Collections.Generic;

namespace Domain.Models
{
    public partial class Powerbi
    {
        public int FrameId { get; set; }
        public int AccountId { get; set; }
        public int? Type { get; set; }
        public string Name { get; set; }
        public Guid? Dashboard { get; set; }
        public Guid? Tile { get; set; }
        public Guid? Report { get; set; }
        public Guid? Group { get; set; }
        public string Url { get; set; }

        public virtual AzureAccount Account { get; set; }
        public virtual Frame Frame { get; set; }
    }
}
