﻿using System;
using System.Collections.Generic;

namespace Domain.Models
{
    public partial class Html
    {
        public int FrameId { get; set; }
        public string Name { get; set; }
        public string Content { get; set; }

        public virtual Frame Frame { get; set; }
    }
}
