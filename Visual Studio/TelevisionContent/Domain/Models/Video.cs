﻿using System;
using System.Collections.Generic;

namespace Domain.Models
{
    public partial class Video
    {
        public Video()
        {
            VideoAlternative = new HashSet<VideoAlternative>();
        }

        public int FrameId { get; set; }
        public bool? PlayMuted { get; set; }
        public bool? AutoLoop { get; set; }

        public virtual Frame Frame { get; set; }
        public virtual ICollection<VideoAlternative> VideoAlternative { get; set; }
    }
}
