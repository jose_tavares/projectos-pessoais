﻿using System;
using System.Collections.Generic;

namespace Domain.Models
{
    public partial class FullScreen
    {
        public int PanelId { get; set; }
        public int? MaxIdleInterval { get; set; }

        public virtual Panel Panel { get; set; }
    }
}
