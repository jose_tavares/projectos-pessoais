﻿using System;
using System.Collections.Generic;

namespace Domain.Models
{
    public partial class AzureAccount
    {
        public AzureAccount()
        {
            Powerbi = new HashSet<Powerbi>();
        }

        public int AccountId { get; set; }
        public string Name { get; set; }
        public int Resource { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public string TenantId { get; set; }
        public string User { get; set; }
        public byte[] Password { get; set; }
        public string AccessToken { get; set; }
        public DateTime? ExpiresOn { get; set; }
        public string RefreshToken { get; set; }
        public string IdToken { get; set; }

        public virtual ICollection<Powerbi> Powerbi { get; set; }
    }
}
