﻿using System;
using System.Collections.Generic;

namespace Domain.Models
{
    public partial class Frame
    {
        public Frame()
        {
            FrameLocation = new HashSet<FrameLocation>();
        }

        public int FrameId { get; set; }
        public int PanelId { get; set; }
        public int Duration { get; set; }
        public DateTime? BeginsOn { get; set; }
        public DateTime? EndsOn { get; set; }
        public int? Sort { get; set; }
        public DateTime DateCreated { get; set; }
        public byte[] Version { get; set; }
        public int TemplateId { get; set; }
        public int CacheInterval { get; set; }

        public virtual Panel Panel { get; set; }
        public virtual Template Template { get; set; }
        public virtual Clock Clock { get; set; }
        public virtual Html Html { get; set; }
        public virtual Memo Memo { get; set; }
        public virtual News News { get; set; }
        public virtual Outlook Outlook { get; set; }
        public virtual Picture Picture { get; set; }
        public virtual Powerbi Powerbi { get; set; }
        public virtual Report Report { get; set; }
        public virtual Video Video { get; set; }
        public virtual Weather Weather { get; set; }
        public virtual Youtube Youtube { get; set; }
        public virtual ICollection<FrameLocation> FrameLocation { get; set; }
    }
}
