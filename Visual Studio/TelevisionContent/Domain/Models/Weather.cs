﻿using System;
using System.Collections.Generic;

namespace Domain.Models
{
    public partial class Weather
    {
        public int FrameId { get; set; }
        public int Type { get; set; }
        public int? Provider { get; set; }
        public int? AccountId { get; set; }

        public virtual OauthAccount Account { get; set; }
        public virtual Frame Frame { get; set; }
    }
}
