﻿using System;
using System.Collections.Generic;

namespace Domain.Models
{
    public partial class Youtube
    {
        public int FrameId { get; set; }
        public string Name { get; set; }
        public string YoutubeId { get; set; }
        public int Volume { get; set; }
        public bool AutoLoop { get; set; }
        public int Aspect { get; set; }
        public int Quality { get; set; }
        public int Start { get; set; }
        public int Rate { get; set; }

        public virtual Frame Frame { get; set; }
    }
}
