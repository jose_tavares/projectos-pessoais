﻿using System;
using System.Collections.Generic;

namespace Domain.Models
{
    public partial class Panel
    {
        public Panel()
        {
            Frame = new HashSet<Frame>();
        }

        public int PanelId { get; set; }
        public int CanvasId { get; set; }
        public int Top { get; set; }
        public int Left { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
        public string Name { get; set; }
        public byte[] Version { get; set; }
        public double FadeLength { get; set; }

        public virtual Canvas Canvas { get; set; }
        public virtual FullScreen FullScreen { get; set; }
        public virtual ICollection<Frame> Frame { get; set; }
    }
}
