﻿using System;
using System.Collections.Generic;

namespace Domain.Models
{
    public partial class Picture
    {
        public int FrameId { get; set; }
        public int ContentId { get; set; }
        public int Mode { get; set; }

        public virtual Content Content { get; set; }
        public virtual Frame Frame { get; set; }
    }
}
