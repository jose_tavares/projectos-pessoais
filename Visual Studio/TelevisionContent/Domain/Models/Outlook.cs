﻿using System;
using System.Collections.Generic;

namespace Domain.Models
{
    public partial class Outlook
    {
        public int FrameId { get; set; }
        public int Mode { get; set; }
        public string Name { get; set; }
        public string Mailbox { get; set; }
        public int ShowEvents { get; set; }
        public int AccountId { get; set; }
        public bool AllowReserve { get; set; }
        public int ShowAsFlags { get; set; }
        public int Privacy { get; set; }
        public string BookingSubject { get; set; }

        public virtual ExchangeAccount Account { get; set; }
        public virtual Frame Frame { get; set; }
    }
}
