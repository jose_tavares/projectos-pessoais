﻿using System;
using System.Collections.Generic;

namespace Domain.Models
{
    public partial class ReportServer
    {
        public ReportServer()
        {
            Report = new HashSet<Report>();
        }

        public int ServerId { get; set; }
        public string Name { get; set; }
        public string BaseUrl { get; set; }
        public string User { get; set; }
        public byte[] Password { get; set; }
        public string Domain { get; set; }

        public virtual ICollection<Report> Report { get; set; }
    }
}
