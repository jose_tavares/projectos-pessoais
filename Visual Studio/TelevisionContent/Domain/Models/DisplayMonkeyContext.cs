﻿namespace Domain.Models
{
    using Common.Helpers;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Diagnostics;

    public partial class DisplayMonkeyContext : DbContext
    {
        public DisplayMonkeyContext() { }

        public DisplayMonkeyContext(DbContextOptions<DisplayMonkeyContext> options)
            : base(options) { }

        public virtual DbSet<AzureAccount> AzureAccount { get; set; }

        public virtual DbSet<Canvas> Canvas { get; set; }

        public virtual DbSet<Clock> Clock { get; set; }

        public virtual DbSet<Content> Content { get; set; }

        public virtual DbSet<Display> Display { get; set; }

        public virtual DbSet<ExchangeAccount> ExchangeAccount { get; set; }

        public virtual DbSet<Frame> Frame { get; set; }

        public virtual DbSet<FrameLocation> FrameLocation { get; set; }

        public virtual DbSet<FullScreen> FullScreen { get; set; }

        public virtual DbSet<Html> Html { get; set; }

        public virtual DbSet<Level> Level { get; set; }

        public virtual DbSet<Location> Location { get; set; }

        public virtual DbSet<Memo> Memo { get; set; }

        public virtual DbSet<MigrationHistory> MigrationHistory { get; set; }

        public virtual DbSet<News> News { get; set; }

        public virtual DbSet<OauthAccount> OauthAccount { get; set; }

        public virtual DbSet<Outlook> Outlook { get; set; }

        public virtual DbSet<Panel> Panel { get; set; }

        public virtual DbSet<Picture> Picture { get; set; }

        public virtual DbSet<Powerbi> Powerbi { get; set; }

        public virtual DbSet<Report> Report { get; set; }

        public virtual DbSet<ReportServer> ReportServer { get; set; }

        public virtual DbSet<Settings> Settings { get; set; }

        public virtual DbSet<Template> Template { get; set; }

        public virtual DbSet<Users> Users { get; set; }

        public virtual DbSet<Video> Video { get; set; }

        public virtual DbSet<VideoAlternative> VideoAlternative { get; set; }

        public virtual DbSet<Weather> Weather { get; set; }

        public virtual DbSet<Youtube> Youtube { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder
                    .UseLazyLoadingProxies()
                    .ConfigureWarnings(warnings => warnings.Ignore(CoreEventId.DetachedLazyLoadingWarning))
                    .UseSqlServer(StringHelper.ConnectionString());
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AzureAccount>(entity =>
            {
                entity.HasKey(e => e.AccountId)
                    .HasName("PK_PowerbiAccount");

                entity.Property(e => e.AccessToken)
                    .HasMaxLength(8000)
                    .IsUnicode(false);

                entity.Property(e => e.ClientId)
                    .IsRequired()
                    .HasMaxLength(36)
                    .IsUnicode(false);

                entity.Property(e => e.ClientSecret)
                    .IsRequired()
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.ExpiresOn).HasColumnType("datetime");

                entity.Property(e => e.IdToken)
                    .HasMaxLength(8000)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Password).HasMaxLength(400);

                entity.Property(e => e.RefreshToken)
                    .HasMaxLength(8000)
                    .IsUnicode(false);

                entity.Property(e => e.TenantId)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.User)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Canvas>(entity =>
            {
                entity.Property(e => e.BackgroundColor)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Version)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken();

                entity.HasOne(d => d.BackgroundImageNavigation)
                    .WithMany(p => p.Canvas)
                    .HasForeignKey(d => d.BackgroundImage)
                    .HasConstraintName("FK_Canvas_Content");
            });

            modelBuilder.Entity<Clock>(entity =>
            {
                entity.HasKey(e => e.FrameId)
                    .HasName("PK_CLOCK");

                entity.Property(e => e.FrameId).ValueGeneratedNever();

                entity.Property(e => e.Label)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ShowDate)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.ShowSeconds)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.ShowTime)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.TimeZone)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Frame)
                    .WithOne(p => p.Clock)
                    .HasForeignKey<Clock>(d => d.FrameId)
                    .HasConstraintName("FK_Clock_Frame");
            });

            modelBuilder.Entity<Content>(entity =>
            {
                entity.Property(e => e.Data).IsRequired();

                entity.Property(e => e.Name)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Version)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken();
            });

            modelBuilder.Entity<Display>(entity =>
            {
                entity.Property(e => e.Host)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Version)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken();

                entity.HasOne(d => d.Canvas)
                    .WithMany(p => p.Display)
                    .HasForeignKey(d => d.CanvasId)
                    .HasConstraintName("FK_Display_Canvas");

                entity.HasOne(d => d.Location)
                    .WithMany(p => p.Display)
                    .HasForeignKey(d => d.LocationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Display_Location");
            });

            modelBuilder.Entity<ExchangeAccount>(entity =>
            {
                entity.HasKey(e => e.AccountId);

                entity.Property(e => e.Account)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Password).HasMaxLength(400);

                entity.Property(e => e.Url)
                    .HasMaxLength(250)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Frame>(entity =>
            {
                entity.Property(e => e.BeginsOn).HasColumnType("datetime");

                entity.Property(e => e.DateCreated)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Duration).HasDefaultValueSql("((1))");

                entity.Property(e => e.EndsOn).HasColumnType("datetime");

                entity.Property(e => e.Version)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken();

                entity.HasOne(d => d.Panel)
                    .WithMany(p => p.Frame)
                    .HasForeignKey(d => d.PanelId)
                    .HasConstraintName("FK_Frame_Panel");

                entity.HasOne(d => d.Template)
                    .WithMany(p => p.Frame)
                    .HasForeignKey(d => d.TemplateId)
                    .HasConstraintName("FK_Frame_Template");
            });

            modelBuilder.Entity<FrameLocation>(entity =>
            {
                entity.HasKey(e => new { e.FrameId, e.LocationId })
                    .HasName("PK_FRAME_LOCATION");

                entity.HasOne(d => d.Frame)
                    .WithMany(p => p.FrameLocation)
                    .HasForeignKey(d => d.FrameId)
                    .HasConstraintName("FK_FrameLocation_Frame");

                entity.HasOne(d => d.Location)
                    .WithMany(p => p.FrameLocation)
                    .HasForeignKey(d => d.LocationId)
                    .HasConstraintName("FK_FrameLlocation_Location");
            });

            modelBuilder.Entity<FullScreen>(entity =>
            {
                entity.HasKey(e => e.PanelId);

                entity.Property(e => e.PanelId).ValueGeneratedNever();

                entity.HasOne(d => d.Panel)
                    .WithOne(p => p.FullScreen)
                    .HasForeignKey<FullScreen>(d => d.PanelId)
                    .HasConstraintName("FK_FullScreen_Panel");
            });

            modelBuilder.Entity<Html>(entity =>
            {
                entity.HasKey(e => e.FrameId);

                entity.Property(e => e.FrameId).ValueGeneratedNever();

                entity.Property(e => e.Content).IsRequired();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.HasOne(d => d.Frame)
                    .WithOne(p => p.Html)
                    .HasForeignKey<Html>(d => d.FrameId)
                    .HasConstraintName("FK_Html_Frame");
            });

            modelBuilder.Entity<Level>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<Location>(entity =>
            {
                entity.Property(e => e.Culture)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.DateFormat)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.TemperatureUnit)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.TimeFormat)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.TimeZone)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Version)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken();

                entity.HasOne(d => d.Area)
                    .WithMany(p => p.InverseArea)
                    .HasForeignKey(d => d.AreaId)
                    .HasConstraintName("FK_Location_Area");

                entity.HasOne(d => d.Level)
                    .WithMany(p => p.Location)
                    .HasForeignKey(d => d.LevelId)
                    .HasConstraintName("FK_Location_Level");
            });

            modelBuilder.Entity<Memo>(entity =>
            {
                entity.HasKey(e => e.FrameId)
                    .HasName("PK_TEXT");

                entity.Property(e => e.FrameId).ValueGeneratedNever();

                entity.Property(e => e.Subject).HasMaxLength(200);

                entity.HasOne(d => d.Frame)
                    .WithOne(p => p.Memo)
                    .HasForeignKey<Memo>(d => d.FrameId)
                    .HasConstraintName("FK_Memo_Frame");
            });

            modelBuilder.Entity<MigrationHistory>(entity =>
            {
                entity.HasKey(e => e.MigrationId)
                    .HasName("PK_dbo.__MigrationHistory");

                entity.ToTable("__MigrationHistory");

                entity.Property(e => e.MigrationId).HasMaxLength(255);

                entity.Property(e => e.Model).IsRequired();

                entity.Property(e => e.ProductVersion)
                    .IsRequired()
                    .HasMaxLength(32);
            });

            modelBuilder.Entity<News>(entity =>
            {
                entity.HasKey(e => e.FrameId)
                    .HasName("PK_NEWS");

                entity.Property(e => e.FrameId).ValueGeneratedNever();

                entity.HasOne(d => d.Frame)
                    .WithOne(p => p.News)
                    .HasForeignKey<News>(d => d.FrameId)
                    .HasConstraintName("FK_News_Frame");
            });

            modelBuilder.Entity<OauthAccount>(entity =>
            {
                entity.HasKey(e => e.AccountId)
                    .HasName("PK__OauthAcc__349DA5A69BA2D612");

                entity.Property(e => e.AppId)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ClientId)
                    .IsRequired()
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.ClientSecret)
                    .IsRequired()
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<Outlook>(entity =>
            {
                entity.HasKey(e => e.FrameId);

                entity.Property(e => e.FrameId).ValueGeneratedNever();

                entity.Property(e => e.BookingSubject).HasMaxLength(100);

                entity.Property(e => e.Mailbox)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Name).HasMaxLength(100);

                entity.Property(e => e.ShowAsFlags).HasDefaultValueSql("((-1))");

                entity.HasOne(d => d.Account)
                    .WithMany(p => p.Outlook)
                    .HasForeignKey(d => d.AccountId)
                    .HasConstraintName("FK_Outlook_ExchangeAccount");

                entity.HasOne(d => d.Frame)
                    .WithOne(p => p.Outlook)
                    .HasForeignKey<Outlook>(d => d.FrameId)
                    .HasConstraintName("FK_Outlook_Frame");
            });

            modelBuilder.Entity<Panel>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Version)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken();

                entity.HasOne(d => d.Canvas)
                    .WithMany(p => p.Panel)
                    .HasForeignKey(d => d.CanvasId)
                    .HasConstraintName("FK_Panel_Canvas");
            });

            modelBuilder.Entity<Picture>(entity =>
            {
                entity.HasKey(e => e.FrameId)
                    .HasName("PK_IMAGE");

                entity.Property(e => e.FrameId).ValueGeneratedNever();

                entity.HasOne(d => d.Content)
                    .WithMany(p => p.Picture)
                    .HasForeignKey(d => d.ContentId)
                    .HasConstraintName("FK_Picture_Content");

                entity.HasOne(d => d.Frame)
                    .WithOne(p => p.Picture)
                    .HasForeignKey<Picture>(d => d.FrameId)
                    .HasConstraintName("FK_Picture_Frame");
            });

            modelBuilder.Entity<Powerbi>(entity =>
            {
                entity.HasKey(e => e.FrameId);

                entity.Property(e => e.FrameId).ValueGeneratedNever();

                entity.Property(e => e.Name).HasMaxLength(100);

                entity.Property(e => e.Url)
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.HasOne(d => d.Account)
                    .WithMany(p => p.Powerbi)
                    .HasForeignKey(d => d.AccountId)
                    .HasConstraintName("FK_Powerbi_AzureAccount");

                entity.HasOne(d => d.Frame)
                    .WithOne(p => p.Powerbi)
                    .HasForeignKey<Powerbi>(d => d.FrameId)
                    .HasConstraintName("FK_Powerbi_Frame");
            });

            modelBuilder.Entity<Report>(entity =>
            {
                entity.HasKey(e => e.FrameId)
                    .HasName("PK_REPORT");

                entity.Property(e => e.FrameId).ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Path)
                    .IsRequired()
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.HasOne(d => d.Frame)
                    .WithOne(p => p.Report)
                    .HasForeignKey<Report>(d => d.FrameId)
                    .HasConstraintName("FK_Report_Frame");

                entity.HasOne(d => d.Server)
                    .WithMany(p => p.Report)
                    .HasForeignKey(d => d.ServerId)
                    .HasConstraintName("FK_Report_ReportServer");
            });

            modelBuilder.Entity<ReportServer>(entity =>
            {
                entity.HasKey(e => e.ServerId);

                entity.Property(e => e.BaseUrl)
                    .IsRequired()
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Domain)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Password).HasMaxLength(1024);

                entity.Property(e => e.User)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Settings>(entity =>
            {
                entity.HasKey(e => e.Key)
                    .HasName("PK__Settings__C41E02899418C6E6")
                    .IsClustered(false);

                entity.Property(e => e.Key).ValueGeneratedNever();

                entity.Property(e => e.Value).HasMaxLength(512);
            });

            modelBuilder.Entity<Template>(entity =>
            {
                entity.Property(e => e.Html).IsRequired();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Version)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken();
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.HasKey(e => e.Uname)
                    .IsClustered(false);

                entity.Property(e => e.Uname)
                    .HasColumnName("uname")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Pwd)
                    .IsRequired()
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.UserRole)
                    .IsRequired()
                    .HasColumnName("userRole")
                    .HasMaxLength(25)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Video>(entity =>
            {
                entity.HasKey(e => e.FrameId)
                    .HasName("PK_VIDEO");

                entity.Property(e => e.FrameId).ValueGeneratedNever();

                entity.Property(e => e.AutoLoop)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.PlayMuted)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.Frame)
                    .WithOne(p => p.Video)
                    .HasForeignKey<Video>(d => d.FrameId)
                    .HasConstraintName("FK_Video_Frame");
            });

            modelBuilder.Entity<VideoAlternative>(entity =>
            {
                entity.HasKey(e => new { e.FrameId, e.ContentId })
                    .HasName("PK_VIDEO_ALTERNATIVE");

                entity.HasOne(d => d.Content)
                    .WithMany(p => p.VideoAlternative)
                    .HasForeignKey(d => d.ContentId)
                    .HasConstraintName("FK_VideoAlternative_Content");

                entity.HasOne(d => d.Frame)
                    .WithMany(p => p.VideoAlternative)
                    .HasForeignKey(d => d.FrameId)
                    .HasConstraintName("FK_VideoAalternative_Video");
            });

            modelBuilder.Entity<Weather>(entity =>
            {
                entity.HasKey(e => e.FrameId)
                    .HasName("PK_WEATHER");

                entity.Property(e => e.FrameId).ValueGeneratedNever();

                entity.HasOne(d => d.Account)
                    .WithMany(p => p.Weather)
                    .HasForeignKey(d => d.AccountId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_Weather_OauthAccount");

                entity.HasOne(d => d.Frame)
                    .WithOne(p => p.Weather)
                    .HasForeignKey<Weather>(d => d.FrameId)
                    .HasConstraintName("FK_Weather_Frame");
            });

            modelBuilder.Entity<Youtube>(entity =>
            {
                entity.HasKey(e => e.FrameId);

                entity.Property(e => e.FrameId).ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.YoutubeId)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.HasOne(d => d.Frame)
                    .WithOne(p => p.Youtube)
                    .HasForeignKey<Youtube>(d => d.FrameId)
                    .HasConstraintName("FK_Youtube_Frame");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
