﻿using System;
using System.Collections.Generic;

namespace Domain.Models
{
    public partial class FrameLocation
    {
        public int FrameId { get; set; }
        public int LocationId { get; set; }

        public virtual Frame Frame { get; set; }
        public virtual Location Location { get; set; }
    }
}
