﻿using System;
using System.Collections.Generic;

namespace Domain.Models
{
    public partial class Display
    {
        public int DisplayId { get; set; }
        public string Name { get; set; }
        public string Host { get; set; }
        public int CanvasId { get; set; }
        public int LocationId { get; set; }
        public byte[] Version { get; set; }
        public bool NoScroll { get; set; }
        public int ReadyTimeout { get; set; }
        public int PollInterval { get; set; }
        public int ErrorLength { get; set; }
        public bool NoCursor { get; set; }
        public TimeSpan? RecycleTime { get; set; }

        public virtual Canvas Canvas { get; set; }
        public virtual Location Location { get; set; }
    }
}
