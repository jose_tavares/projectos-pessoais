﻿using System;
using System.Collections.Generic;

namespace Domain.Models
{
    public partial class OauthAccount
    {
        public OauthAccount()
        {
            Weather = new HashSet<Weather>();
        }

        public int AccountId { get; set; }
        public int Provider { get; set; }
        public string Name { get; set; }
        public string AppId { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }

        public virtual ICollection<Weather> Weather { get; set; }
    }
}
