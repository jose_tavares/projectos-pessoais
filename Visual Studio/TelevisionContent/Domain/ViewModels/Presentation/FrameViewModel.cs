﻿using System;
using static Common.Enums;

namespace Domain.ViewModels.Presentation
{
    public class FrameViewModel
    {
        public int FrameId { get; set; }

        public int PanelId { get; set; }

        public int DisplayId { get; set; }

        public int Duration { get; set; }

        public int Sort { get; set; }

        public DateTime? BeginsOn { get; set; }

        public DateTime? EndsOn { get; set; }

        public DateTime? DateCreated { get; set; }

        public string TemplateName { get; set; }

        public string Html { get; set; }

        public FrameTypes FrameType { get; set; }

        public int CacheInterval { get; set; }

        public ulong Version { get; set; }
    }
}
