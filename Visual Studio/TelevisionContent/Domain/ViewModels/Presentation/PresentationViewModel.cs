﻿namespace Domain.ViewModels.Presentation
{
    public class PresentationViewModel
    {
        public string Title { get; set; }

        public string Head { get; set; }

        public string Body { get; set; }
    }
}
