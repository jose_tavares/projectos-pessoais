﻿using System;

namespace Domain.ViewModels.Presentation
{
    public class DisplayViewModel
    {
        public int DisplayId { get; set; }

        public int IdleInterval { get; set; }

        public int Hash { get; set; }

        public TimeSpan? RecycleTime { get; set; }
    }
}
