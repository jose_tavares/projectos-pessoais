﻿using System.Collections.Generic;

namespace Domain.ViewModels.Management
{
    public class HomeViewModel
    {
        #region Count Properties
        public int CountFrames { get; set; }

        public int CountActiveFrames { get; set; }

        public int CountExpiredFrames { get; set; }

        public int CountPendingFrames { get; set; }

        public string DurationHours { get; set; }

        public int CountLevels { get; set; }

        public int CountLocations { get; set; }

        public int CountCanvases { get; set; }

        public int CountPanels { get; set; }

        public int CountDisplays { get; set; }

        public int CountHtml { get; set; }

        public int CountHtml7 { get; set; }

        public int CountMemos { get; set; }

        public int CountMemos7 { get; set; }

        public int CountOutlook { get; set; }

        public int CountOutlook7 { get; set; }

        public int CountPowerbi { get; set; }

        public int CountPowerbi7 { get; set; }

        public int CountReports { get; set; }

        public int CountReports7 { get; set; }

        public int CountVideos { get; set; }

        public int CountVideos7 { get; set; }

        public int CountYoutube { get; set; }

        public int CountYoutube7 { get; set; }

        public int CountClock { get; set; }

        public int CountClock7 { get; set; }

        public int CountWeather { get; set; }

        public int CountWeather7 { get; set; }

        public int CountPictures { get; set; }

        public int CountPictures7 { get; set; }
        #endregion

        public List<ContentViewModel> TopFiveContentsViewModel { get; set; }
    }
}