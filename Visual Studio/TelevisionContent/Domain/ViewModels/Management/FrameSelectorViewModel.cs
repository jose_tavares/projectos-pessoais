﻿using static Common.Enums;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Domain.ViewModels.Management
{
    public class FrameSelectorViewModel
    {
        public int PanelId { get; set; }

        public int CanvasId { get; set; }

        public FrameTypes? FrameType { get; set; }

        public PanelViewModel PanelViewModel { get; set; }

        public IEnumerable<SelectListItem> SelectListCanvases { get; set; }

        public IEnumerable<SelectListItem> SelectListPanels { get; set; }

        public IEnumerable<SelectListItem> SelectListFrameTypes { get; set; }
    }
}
