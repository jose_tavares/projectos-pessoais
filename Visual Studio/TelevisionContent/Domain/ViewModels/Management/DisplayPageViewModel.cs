﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel.DataAnnotations;

namespace Domain.ViewModels.Management
{
    public class DisplayPageViewModel
    {
        [Display(Name = "Ecrãs")]
        public int CanvasId { get; set; }

        [Display(Name = "Localizações")]
        public int LocationId { get; set; }

        [Display(Name = "Nome")]
        public string Name { get; set; }

        [Display(Name = "Endereço de IP")]
        public string Host { get; set; }

        public IEnumerable<SelectListItem> SelectListCanvases { get; set; }

        public IEnumerable<SelectListItem> SelectListLocations { get; set; }

        public IEnumerable<DisplayViewModel> DisplayViewModels { get; set; }
    }
}
