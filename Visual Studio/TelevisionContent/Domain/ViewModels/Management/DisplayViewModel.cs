﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel.DataAnnotations;

namespace Domain.ViewModels.Management
{
    public class DisplayViewModel
    {
        public int DisplayId { get; set; }

        public int CanvasId { get; set; }

        public int LocationId { get; set; }

        [Display(Name = "Intervalo de atualização do estado de exibição em segundo plano (segundos)")]
        public int PollInterval { get; set; }

        [Display(Name = "Duração do popup de erro (segundos - use 0 para suprimir erros)")]
        public int ErrorLength { get; set; }

        [Display(Name = "Nome")]
        public string Name { get; set; }

        [Display(Name = "Endereço de IP")]
        public string Host { get; set; }

        [Display(Name = "Ecrã")]
        public string Canvas { get; set; }

        [Display(Name = "Localização")]
        public string Location { get; set; }

        [Display(Name = "Ocultar barras de deslocação da janela")]
        public bool NoScroll { get; set; }

        [Display(Name = "Esconder o ponteiro do rato")]
        public bool NoCursor { get; set; }

        [Display(Name = "Tempo limite do evento pronto para frame")]
        public int ReadyTimeout { get; set; }

        [Display(Name = "Tempo de reciclagem automática")]
        public string AutoRefreshAt { get; set; }

        public IEnumerable<SelectListItem> SelectListCanvases { get; set; }

        public IEnumerable<SelectListItem> SelectListLocations { get; set; }
    }
}
