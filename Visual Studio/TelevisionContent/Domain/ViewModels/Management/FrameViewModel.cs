﻿using System.ComponentModel;

namespace Domain.ViewModels.Management
{
    public class FrameViewModel
    {
        public int FrameId { get; set; }

        public int CanvasId { get; set; }

        public int PanelId { get; set; }

        public int TemplateId { get; set; }

        public int? ContentId { get; set; }

        public int CacheInterval { get; set; }

        [DisplayName("Tipo")]
        public string Icon { get; set; }

        [DisplayName("Ecrã")]
        public string Canvas { get; set; }

        [DisplayName("Painel")]
        public string Panel { get; set; }

        [DisplayName("Conteúdo Exibido/Informação")]
        public string Content { get; set; }

        [DisplayName("Duração (segundos)")]
        public int Duration { get; set; }
    }
}
