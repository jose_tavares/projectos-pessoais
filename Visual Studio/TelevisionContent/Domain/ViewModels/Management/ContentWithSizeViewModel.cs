﻿using static Common.Enums;
using System.ComponentModel.DataAnnotations;

namespace Domain.ViewModels.Management
{
    public class ContentWithSizeViewModel
    {
        public int ContentId { get; set; }

        [Display(Name = "Tamanho (KB)")]
        public int? Size { get; set; }

        [Display(Name = "Tipo")]
        public ContentTypes Type { get; set; }

        [Display(Name = "Nome")]
        public string Name { get; set; }
    }
}
