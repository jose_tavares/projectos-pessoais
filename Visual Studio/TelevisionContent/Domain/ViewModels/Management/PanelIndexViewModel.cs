﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Domain.ViewModels.Management
{
    public class PanelIndexViewModel
    {
        public int CanvasId { get; set; }

        public string PainelName { get; set; }

        public IEnumerable<SelectListItem> SelectListCanvases { get; set; }

        public List<PanelViewModel> PanelsViewModel { get; set; }
    }
}
