﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Domain.ViewModels.Management
{
    public class FramePageViewModel
    {
        public int CanvasId { get; set; }

        public int PanelId { get; set; }

        public int FrameTypelId { get; set; }

        public int TimingOptionsId { get; set; }

        public IEnumerable<FrameViewModel> FramesViewModel { get; set; }

        public IEnumerable<SelectListItem> SelectListCanvases { get; set; }

        public IEnumerable<SelectListItem> SelectListPanels { get; set; }

        public IEnumerable<SelectListItem> SelectListFrameTypes { get; set; }

        public IEnumerable<SelectListItem> SelectListTimingOptions { get; set; }
    }
}
