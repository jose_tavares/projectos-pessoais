﻿using static Common.Enums;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Domain.ViewModels.Management
{
    public class ContentPageViewModel
    {
        public ContentTypes? ContentTypes { get; set; }

        public List<ContentWithSizeViewModel> ContentsWithSizeViewModel { get; set; }

        public IEnumerable<SelectListItem> SelectListContentTypes { get; set; }
    }
}
