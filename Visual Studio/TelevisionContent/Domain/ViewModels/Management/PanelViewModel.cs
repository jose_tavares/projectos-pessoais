﻿using static Common.Enums;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel.DataAnnotations;

namespace Domain.ViewModels.Management
{
    public class PanelViewModel
    {
        public int PanelId { get; set; }

        public int CanvasId { get; set; }

        [Display(Name = "Nome")]
        public string Name { get; set; }

        [Display(Name = "Ecrã")]
        public string Canvas { get; set; }

        [Display(Name = "Width (%)")]
        public int Width { get; set; }

        [Display(Name = "Height (%)")]
        public int Height { get; set; }

        [Display(Name = "Top (% - 0 cima e 100 baixo)")]
        public int Top { get; set; }

        [Display(Name = "Left (% - 0 esquerda e 100 direita)")]
        public int Left { get; set; }

        [Display(Name = "Duração da transição do Frame (segundos)")]
        public double FadeLength { get; set; }

        public FrameTypes? FrameType { get; set; }

        public bool IsFullscreen { get; set; }

        [Display(Name = "Intervalo máximo de recorrência")]
        public int? MaxIdleInterval { get; set; }

        public IEnumerable<SelectListItem> SelectListCanvases { get; set; }
    }
}
