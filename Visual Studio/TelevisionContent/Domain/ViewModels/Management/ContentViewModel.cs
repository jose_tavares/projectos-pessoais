﻿using System;
using System.Linq;
using static Common.Enums;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain.ViewModels.Management
{
    public class ContentViewModel
    {
        public int ContentId { get; set; }

        public int Count { get; set; }

        [Display(Name = "Nome")]
        public string Name { get; set; }

        [Display(Name = "Tipo")]
        public ContentTypes Type { get; set; }

        public string SupportedImages { get; private set; } = string.Join(", ", Enum.GetValues(typeof(PictureSupportedFormats)).Cast<PictureSupportedFormats>());

        public string SupportedVideos { get; private set; } = string.Join(", ", Enum.GetValues(typeof(VideoSupportedFormats)).Cast<VideoSupportedFormats>());

        [DataType(DataType.Upload)]
        [Display(Name = "Fonte de Conteúdo")]
        public byte[] Data { get; set; }

        public string SupportedMediaFormatsMessage { get; private set; }

        public IEnumerable<IFormFile> Files { get; set; }

        public ContentViewModel()
        {
            SupportedMediaFormatsMessage = string.Format("Os formatos de média suportados incluem {0} para imagens e {1} para vídeo. Todos os outros conteúdos serão ignorados.", SupportedImages, SupportedVideos);
        }
    }
}
