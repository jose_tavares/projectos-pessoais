﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel.DataAnnotations;

namespace Domain.ViewModels.Management
{
    public class CanvasViewModel
    {
        public int CanvasId { get; set; }

        public int ContentId { get; set; }

        public int PanelId { get; set; }

        [Display(Name = "Nome")]
        public string Name { get; set; }        

        [Display(Name = "Width (%)")]
        public int Width { get; set; }

        [Display(Name = "Height (%)")]
        public int Height { get; set; }

        public int Top { get; set; }

        public int Left { get; set; }

        [Display(Name = "Imagem de Fundo")]
        public int? BackgroundImage { get; set; }

        [Display(Name = "Cor para Fundo de Ecrã")]
        public string BackgroundColor { get; set; }        

        public IEnumerable<PanelViewModel> PanelViewModels { get; set; }

        public IEnumerable<SelectListItem> SelectListBackgroundImages { get; set; }
    }
}
