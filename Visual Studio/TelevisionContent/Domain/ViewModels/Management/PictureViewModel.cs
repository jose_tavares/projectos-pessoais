﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel.DataAnnotations;

namespace Domain.ViewModels.Management
{
    public class PictureViewModel
    {
        public int FrameId { get; set; }

        public int CanvasId { get; set; }

        [Display(Name = "Modelo")]
        public int TemplateId { get; set; }

        public int PanelId { get; set; }

        [Display(Name = "Importar Imagem")]
        public int ContentId { get; set; }

        [Display(Name = "Cache (min.)")]
        public int CacheInterval { get; set; }

        public string Panel { get; set; }

        public string Canvas { get; set; }

        public int? Sort { get; set; }

        [Display(Name = "Começa a")]
        public DateTime? BeginsOn { get; set; }

        [Display(Name = "Termina a")]
        public DateTime? EndsOn { get; set; }

        [Display(Name = "Renderização")]
        public int Mode { get; set; }

        [Display(Name = "Duração (min.)")]
        public int Duration { get; set; }

        public IEnumerable<SelectListItem> SelectListPictures { get; set; }

        public IEnumerable<SelectListItem> SelectListTemplates { get; set; }

        public IEnumerable<SelectListItem> SelectListModes { get; set; }
    }
}
