﻿using AutoMapper;
using System.Linq;
using Domain.Models;
using Common.Extensions;
using static Common.Enums;
using Repositories.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Domain.ViewModels.Management;

namespace Presentation.Controllers
{
    public class HomeController : BaseController
    {
        private readonly IDisplayService _displayService;
        private readonly ISettingsService _settingsService;

        public HomeController
            (IMapper mapper,
            IUnitOfWork unitOfWork,
            IDisplayService displayService,
            ISettingsService settingsService) : base(mapper, unitOfWork)
        {
            _displayService = displayService;
            _settingsService = settingsService;
        }

        public IActionResult Index() => View();

        public IActionResult Displays()
        {
            string theHost = null;
            int displayId = int.MinValue;
            var mode = _settingsService.AutoLoadMode();
            var displayViewModels = new List<DisplayViewModel>();
            var displaysModel = _displayService.All().ToList();

            switch (mode)
            {
                case DisplayAutoLoadModes.IP:
                    theHost = HttpContext.Request.Host.Host;

                    if (string.IsNullOrEmpty(theHost))
                    {
                        theHost = HttpContext.Connection.RemoteIpAddress.ToString();
                    }

                    break;

                case DisplayAutoLoadModes.Cookie:
                    var cookie = HttpContext.Request.Cookies["DisplayId"];

                    if (!string.IsNullOrEmpty(cookie))
                    {
                        int.TryParse(cookie, out displayId);
                    }

                    break;

                default:
                    break;
            }

            if (displaysModel.Validate())
            {
                bool isEnter = false;
                foreach (var displayModel in displaysModel)
                {
                    switch (mode)
                    {
                        case DisplayAutoLoadModes.IP:
                            if (theHost != "::1" && (theHost == displayModel.Host))
                            {
                                isEnter = true;
                                displayViewModels.Add(_mapper.Map<Display, DisplayViewModel>(displayModel));
                            }

                            break;

                        case DisplayAutoLoadModes.Cookie:
                            if (displayId == displayModel.DisplayId)
                            {
                                isEnter = true;
                                displayViewModels.Add(_mapper.Map<Display, DisplayViewModel>(displayModel));
                            }

                            break;

                        default:
                            break;
                    }
                }

                if (!displayViewModels.Validate() && !isEnter)
                {
                    displayViewModels = displaysModel.Select(select => _mapper.Map<Display, DisplayViewModel>(select)).ToList();
                }
            }

            return View(new DisplayPageViewModel
            {
                DisplayViewModels = displayViewModels
            });
        }
    }
}
