﻿using System;
using AutoMapper;
using Common.Helpers;
using Repositories.Services;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Domain.ViewModels.Presentation;

namespace Presentation.Controllers
{
    public class DisplayController : BaseController
    {
        public DisplayController
            (IMapper mapper,
            IUnitOfWork unitOfWork) : base(mapper, unitOfWork) { }

        public async Task<JsonResult> DataAsync(int display)
        {
            try
            {
                DisplayViewModel data = new DisplayViewModel()
                {
                    DisplayId = 0,
                    IdleInterval = 0,
                    Hash = 0,
                    RecycleTime = null
                };

                var refreshAsync = await DataAccessHelper.RefreshAsync(display);

                data.DisplayId = refreshAsync.Item1;
                data.IdleInterval = refreshAsync.Item2;
                data.Hash = refreshAsync.Item3;
                data.RecycleTime = refreshAsync.Item4;

                return new JsonResult(new
                {
                    data.DisplayId,
                    data.IdleInterval,
                    data.Hash,
                    data.RecycleTime
                });
            }
            catch (Exception ex)
            {
                return new JsonResult(new
                {
                    Error = ex.Message
                });
            }
        }        
    }
}
