﻿using System;
using AutoMapper;
using static Common.Enums;
using Repositories.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Domain.ViewModels.Presentation;

namespace Presentation.Controllers
{
    public class CanvasController : BaseController
    {
        private readonly ICanvasService _canvasService;
        private readonly IDisplayService _displayService;
        private readonly ISettingsService _settingsService;

        public CanvasController
            (IMapper mapper,
            IUnitOfWork unitOfWork,
            ICanvasService canvasService,
            IDisplayService displayService,
            ISettingsService settingsService) : base(mapper, unitOfWork)
        {
            _canvasService = canvasService;
            _displayService = displayService;
            _settingsService = settingsService;
        }

        public IActionResult Index(int id = 0)
        {
            if (id == 0)
            {
                return Error();
            }

            var displayModel = _displayService.SingleOrDefault(id);

            if (displayModel == null)
            {
                return Error();
            }

            if ((displayModel.DisplayId == 0) || (displayModel.CanvasId == 0))
            {
                var cookie = HttpContext.Request.Cookies["DisplayId"];

                if (!string.IsNullOrEmpty(cookie))
                {
                    HttpContext.Response.Cookies.Append("DisplayId", cookie,
                        new CookieOptions
                        {
                            Expires = DateTime.Today.AddDays(-1)
                        });
                }

                return RedirectToAction("Displays", "Home");
            }

            if (_settingsService.AutoLoadMode() == DisplayAutoLoadModes.Cookie)
            {
                HttpContext.Response.Cookies.Append("DisplayId", displayModel.DisplayId.ToString(),
                    new CookieOptions
                    {
                        Expires = new DateTime(2038, 1, 1)
                    });
            }

            return View(new PresentationViewModel
            {
                Title = displayModel.Canvas.Name,

                Head = _canvasService.PresentationHead
                    (displayModel,
                    HttpContext.Request.Scheme,
                    HttpContext.Request.Host.ToString(),
                    url => Url.Content(url)),

                Body = _canvasService.PresentationBody(displayModel)
            });
        }
    }
}
