﻿using System;
using AutoMapper;
using System.Data;
using System.Threading;
using Common.Extensions;
using static Common.Enums;
using System.Globalization;
using Repositories.Services;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Domain.ViewModels.Presentation;

namespace Presentation.Controllers
{
    public class FrameController : BaseController
    {
        public FrameController
            (IMapper mapper,
            IUnitOfWork unitOfWork) : base(mapper, unitOfWork) { }

        public async Task<JsonResult> DataAsync(int panel, int display, int frame, string culture)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(culture))
                {
                    CultureInfo cultureInfo = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = cultureInfo;
                    Thread.CurrentThread.CurrentUICulture = cultureInfo;
                }

                var nextFrame = await GetNextFrameAsync(panel, display, frame);

                return new JsonResult(new
                {
                    nextFrame
                });
            }
            catch (Exception ex)
            {
                return new JsonResult(new
                {
                    Error = ex.Message
                });
            }
        }

        private async Task<FrameViewModel> GetNextFrameAsync(int panelId, int displayId, int previousFrameId)
        {
            FrameViewModel frame = new FrameViewModel
            {
                PanelId = panelId,
                DisplayId = displayId
            };

            using SqlCommand cmd = new SqlCommand("sp_GetNextFrame")
            {
                CommandType = CommandType.StoredProcedure
            };

            cmd.Parameters.Add("@panelId", SqlDbType.Int).Value = panelId;
            cmd.Parameters.Add("@displayId", SqlDbType.Int).Value = displayId;
            cmd.Parameters.Add("@lastFrameId", SqlDbType.Int).Value = previousFrameId;

            await cmd.ExecuteReaderExtAsync((dr) =>
            {
                frame = new FrameViewModel
                {
                    FrameId = dr.IntOrZero("FrameId"),
                    PanelId = dr.IntOrZero("PanelId"),
                    Duration = dr.IntOrDefault("Duration", 60),
                    Sort = dr.IntOrZero("Sort"),
                    BeginsOn = dr.AsNullable<DateTime>("BeginsOn"),
                    EndsOn = dr.AsNullable<DateTime>("EndsOn"),
                    DateCreated = dr.AsNullable<DateTime>("DateCreated"),
                    TemplateName = dr.StringOrDefault("TemplateName", "default"),
                    Html = dr.StringOrBlank("Html"),
                    FrameType = (FrameTypes)dr.IntOrZero("FrameType"),
                    CacheInterval = dr.IntOrZero("CacheInterval") < 0 ? 0 : dr.IntOrZero("CacheInterval"),
                    Version = BitConverter.ToUInt64(dr.ValueOrNull<byte[]>("Version"), 0)      // is never a null
                };

                return false;
            });

            return frame;
        }
    }
}
