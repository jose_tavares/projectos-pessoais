﻿using System;
using System.IO;
using AutoMapper;
using Common.Helpers;
using Common.Extensions;
using static Common.Enums;
using Repositories.Services;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;

namespace Presentation.Controllers
{
    public class ImageController : BaseController
    {
        private readonly IMemoryCache _memoryCache;
        private readonly IPanelService _panelService;
        private readonly IContentService _contentService;
        private readonly IPictureService _pictureService;

        public ImageController
            (IMapper mapper,
            IUnitOfWork unitOfWork,
            IMemoryCache memoryCache,
            IPanelService panelService,
            IContentService contentService,
            IPictureService pictureService) : base(mapper, unitOfWork)
        {
            _memoryCache = memoryCache;
            _panelService = panelService;
            _contentService = contentService;
            _pictureService = pictureService;
        }

        public async Task<IActionResult> DataAsync(int content, int frame)
        {
            byte[] data = null;

            if (frame > 0)
            {
                var pictureModel = _pictureService.SingleOrDefault(frame);

                if ((pictureModel != null) && (pictureModel.Frame.PanelId > 0))
                {
                    var panelModel = _panelService.SingleOrDefault(pictureModel.Frame.PanelId);

                    data = await _memoryCache.GetOrAddAbsoluteAsync
                        (string.Format("picture_{0}_{1}", pictureModel.FrameId, pictureModel.Frame.Version),
                        async (expire) =>
                        {
                            expire.When = DateTime.Now.AddMinutes(pictureModel.Frame.CacheInterval);

                            var contentModel = await _contentService.SingleOrDefaultAsync(pictureModel.ContentId);

                            if (contentModel.Data == null)
                            {
                                return null;
                            }

                            using MemoryStream trg = new MemoryStream();
                            using MemoryStream src = new MemoryStream(contentModel.Data);
                            ImageHelper.Write(src, trg, panelModel.Width, panelModel.Height, (RenderModes)pictureModel.Mode);

                            return trg.GetBuffer();
                        });
                }
            }
            else if (content > 0)
            {
                data = await _memoryCache.GetOrAddSlidingAsync
                    (string.Format("image_{0}_{1}x{2}_{3}", content, -1, -1, (int)RenderModes.Crop),
                    async (expire) =>
                    {
                        expire.After = TimeSpan.FromMinutes(60);

                        var contentModel = await _contentService.SingleOrDefaultAsync(content);

                        if (contentModel.Data == null)
                        {
                            return null;
                        }

                        using MemoryStream trg = new MemoryStream();
                        using MemoryStream src = new MemoryStream(contentModel.Data);
                        await Task.Run(() => ImageHelper.Write(src, trg, -1, -1, RenderModes.Crop));

                        return trg.GetBuffer();
                    });
            }

            if (data != null)
            {
                return File(data, "image/png");
            }

            return File("files/404.png", "image/png");
        }
    }
}