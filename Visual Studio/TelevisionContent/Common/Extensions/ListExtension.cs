﻿using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Common.Extensions
{
    public static class List
    {
        public static bool Validate<T>(this IEnumerable<T> list)
        {
            if ((list != null) && list.Any())
            {
                return true;
            }

            return false;
        }

        public static List<SelectListItem> ToSelectListItems<T>(this IEnumerable<T> enumerable, Func<T, string> text, Func<T, string> value, Func<T, bool> isSelected = null, string defaultTextOption = null, string defaultValueOption = "", bool isDefaultOptionDisabled = false, Func<T, string> groupedBy = null)
        {
            var selectListItem = new List<SelectListItem>();

            if (groupedBy != null)
            {
                var enumerableGrouping = enumerable.GroupBy(groupBy => groupedBy(groupBy));

                foreach (var groupingItem in enumerableGrouping)
                {
                    var selectListGroup = new SelectListGroup()
                    {
                        Name = groupingItem.Key
                    };

                    selectListItem = AddItems(selectListItem, groupingItem, text, value, isSelected, selectListGroup);
                }
            }
            else
            {
                selectListItem = AddItems(selectListItem, enumerable, text, value, isSelected);
            }

            if (!string.IsNullOrEmpty(defaultTextOption))
            {
                selectListItem = AddDefaultItem(selectListItem, defaultTextOption, defaultValueOption, isDefaultOptionDisabled);
            }

            return selectListItem;
        }

        private static List<SelectListItem> AddItems<T>(List<SelectListItem> items, IEnumerable<T> enumerable, Func<T, string> text, Func<T, string> value, Func<T, bool> isSelected, SelectListGroup selectListGroup = null)
        {
            items.AddRange(enumerable.Select(select => new SelectListItem()
            {
                Group = selectListGroup,
                Text = text(select),
                Value = value(select),
                Selected = (isSelected != null) && isSelected(select)
            }));

            return items;
        }

        private static List<SelectListItem> AddDefaultItem(List<SelectListItem> items, string defaultTextOption, string defaultValueOption, bool isDefaultOptionDisabled)
        {
            items.Insert(0, new SelectListItem()
            {
                Text = defaultTextOption,
                Value = defaultValueOption,
                Disabled = isDefaultOptionDisabled
            });

            return items;
        }
    }
}