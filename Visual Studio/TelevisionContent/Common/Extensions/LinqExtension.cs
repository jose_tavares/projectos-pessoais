﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace Common.Extensions
{
    public static class LinqExtension
    {
        public static IEnumerable<T> SelfAndChildren<T>
            (this T self,
            Func<T, IEnumerable<T>> children)
        {
            yield return self;

            var elements = children(self);

            foreach (var childrenElement in elements.SelectMany(element => element.SelfAndChildren(children)))
            {
                yield return childrenElement;
            }
        }
    }
}
