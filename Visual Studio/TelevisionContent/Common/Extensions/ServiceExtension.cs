﻿using System;
using Microsoft.Extensions.DependencyInjection;

namespace Common.Extensions
{
    public static class ServiceExtension
    {
        public static IServiceCollection AddLazyResolution(this IServiceCollection services)
        {
            return services.AddTransient
                (typeof(Lazy<>),
                typeof(LazilyResolved<>));
        }

        private class LazilyResolved<T> : Lazy<T>
        {
            public LazilyResolved(IServiceProvider serviceProvider)
                : base(serviceProvider.GetRequiredService<T>) { }
        }
    }
}
