﻿using System;

namespace Common.Extensions
{
    public class Switch
    {
        public object Object { get; private set; }

        public Switch(object o) => Object = o;
    }

    /// <summary>
    /// Extension, because otherwise casing fails on Switch==null
    /// </summary>
    public static class SwitchExtension
    {
        public static Switch Case<T>(this Switch s, Action<T> a) where T : class =>
            Case(s, o => true, a, false);

        public static Switch Case<T>(this Switch s, Action<T> a, bool fallThrough) where T : class =>
            Case(s, o => true, a, fallThrough);

        public static Switch Case<T>(this Switch s, Func<T, bool> c, Action<T> a) where T : class =>
            Case(s, c, a, false);

        public static Switch Case<T>(this Switch s, Func<T, bool> c, Action<T> a, bool fallThrough) where T : class
        {
            if (s == null)
            {
                return null;
            }

            if (s.Object is T t)
            {
                if (c(t))
                {
                    a(t);
                    return fallThrough ? s : null;
                }
            }

            return s;
        }
    }
}