﻿using System;
using System.Data;
using Common.Helpers;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Common.Extensions
{
    public static class DataAccessExtension
    {
        public static void ExecuteReaderExt(this SqlCommand command, Func<SqlDataReader, bool> callback)
        {
            ConnectionWrap(command, () =>
            {
                using SqlDataReader reader = command.ExecuteReader();
                while (reader.Read() && callback(reader)) ;
            });
        }

        public static async Task ExecuteReaderExtAsync(this SqlCommand command, Func<SqlDataReader, bool> callback)
        {
            await ConnectionWrapAsync(command, async () =>
            {
                using SqlDataReader reader = await Task<SqlDataReader>.Factory.FromAsync(command.BeginExecuteReader, command.EndExecuteReader, null);
                while (await reader.ReadAsync() && callback(reader)) ;
            });
        }

        public static async Task ExecuteNonQueryExtAsync(this SqlCommand command) =>
            await ConnectionWrapAsync(command, async () => await command.ExecuteNonQueryAsync());

        public static int IntOrZero(this SqlDataReader reader, string column) =>
            DbValueOrDefault(reader[column], 0);

        public static int IntOrDefault(this SqlDataReader reader, string column, int _default) =>
            DbValueOrDefault(reader[column], _default);

        public static T? AsNullable<T>(this SqlDataReader reader, string column) where T : struct =>
            DbAsNullable<T>(reader[column]);

        public static string StringOrDefault(this SqlDataReader reader, string column, string _default) =>
            DbValueOrDefault(reader[column], _default);

        public static string StringOrBlank(this SqlDataReader reader, string column) =>
            DbValueOrDefault(reader[column], "");

        public static T ValueOrNull<T>(this SqlDataReader reader, string column) where T : class =>
            DbValueOrDefault<T>(reader[column], default);

        private static void ConnectionWrap(SqlCommand command, Action action)
        {
            if (command.Connection == null)
            {
                using (command.Connection = new SqlConnection(StringHelper.ConnectionString()))
                {
                    command.Connection.Open();
                    action();
                }
            }
            else
            {
                if ((command.Connection.State == ConnectionState.Broken) || (command.Connection.State == ConnectionState.Closed))
                {
                    command.Connection.Open();
                }

                 action();
            }
        }

        private static async Task ConnectionWrapAsync(SqlCommand command, Func<Task> funcAsync)
        {
            if (command.Connection == null)
            {
                using (command.Connection = new SqlConnection(StringHelper.ConnectionString()))
                {
                    await command.Connection.OpenAsync();
                    await funcAsync();
                }
            }
            else
            {
                if ((command.Connection.State == ConnectionState.Broken) || (command.Connection.State == ConnectionState.Closed))
                {
                    await command.Connection.OpenAsync();
                }

                await funcAsync();
            }
        }

        private static T DbValueOrDefault<T>(object o, T _default)
        {
            var t = o;

            if (t == DBNull.Value)
            {
                return _default;
            }

            return (T)t;
        }

        private static T? DbAsNullable<T>(object o) where T : struct
        {
            var t = o;

            if (t == DBNull.Value)
            {
                return null;
            }

            return new T?((T)t);
        }
    }
}
