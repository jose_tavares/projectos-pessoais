﻿using System.ComponentModel;

namespace Common
{
    public class Enums
    {
        public enum FrameTypes
        {
            [Description("Relógio")]
            Clock,

            [Description("HTML")]
            Html,

            [Description("Memorando")]
            Memo,

            [Description("Outlook")]
            Outlook = 4,

            [Description("Imagem")]
            Picture,

            [Description("Relatório")]
            Report,

            [Description("Vídeo")]
            Video,

            [Description("Clima")]
            Weather,

            [Description("YouTube")]
            Youtube,

            [Description("Power Bi")]
            Powerbi
        }

        public enum TimingOptions
        {
            [Description("Pendente")]
            Pending,

            [Description("Ativo")]
            Active,

            [Description("Expirado")]
            Expired
        }

        public enum RenderModes
        {
            [Description("Corte")]
            Crop,

            [Description("Extensão")]
            Stretch,

            [Description("Ajustado")]
            Fit,

            [Description("Preenchimento")]
            Fill
        }

        public enum ContentTypes
        {
            [Description("Imagem")]
            Picture,

            [Description("Vídeo")]
            Video
        }

        public enum PictureSupportedFormats
        {
            BMP, GIF, JPG, JPEG, PNG, TIF, TIFF
        }

        public enum VideoSupportedFormats
        {
            AVI, MP4, MPG, MPEG, OGG, WEBM
        }

        public enum DisplayAutoLoadModes
        {
            IP, Cookie
        }
    }
}
