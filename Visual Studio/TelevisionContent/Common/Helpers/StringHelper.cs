﻿using System.IO;
using Newtonsoft.Json.Linq;

namespace Common.Helpers
{
    public static class StringHelper
    {
        public static string ConnectionString()
        {
            var json = File.ReadAllText(@"..\..\TelevisionContent\Infrastructure\appsettings.json");

            var jsonObject = JObject.Parse(json);

            return jsonObject.SelectToken("ConnectionStrings").SelectToken("DefaultConnection").Value<string>();
        }
    }
}
