﻿using System;
using System.Data;
using Common.Extensions;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Common.Helpers
{
    public static class DataAccessHelper
    {

        // mudar para um view model
        public static async Task<Tuple<int, int, int, TimeSpan>> RefreshAsync(int id)
        {
            int displayId = default, idleInterval = default, hash = default;
            TimeSpan recycleTime = default;

            using (SqlCommand cmd = new SqlCommand("sp_GetDisplayData"))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@displayId", id);

                await cmd.ExecuteReaderExtAsync((reader) =>
                {
                    displayId = reader.IntOrZero("DisplayId");
                    idleInterval = reader.IntOrZero("IdleInterval");
                    hash = reader.IntOrZero("Hash");

                    if (reader["RecycleTime"] != DBNull.Value)
                    {
                        recycleTime = (TimeSpan)reader["RecycleTime"];
                    }

                    return false;
                });
            }

            return Tuple.Create(displayId, idleInterval, hash, recycleTime);
        }
    }
}
