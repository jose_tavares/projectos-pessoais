﻿using System;
using System.Net;
using System.Text;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Common.Helpers
{
    public class ServerGeoData
    {
        public static double Latitude { get; private set; }

        public static double Longitude { get; private set; }

        public static TimeZoneInfo TimeZone { get; private set; }

        public static IPAddress ServerExternalIPAddress { get; private set; }

        public static int OffsetGMT
        {
            get
            {
                return (int)TimeZone.BaseUtcOffset.TotalMinutes;
            }
        }

        public int OffsetUtc
        {
            get
            {
                return (int)TimeZone.GetUtcOffset(DateTime.UtcNow).TotalMinutes;
            }
        }

        public DateTime ServerTime
        {
            get
            {
                return TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, TimeZone);
            }
        }

        static ServerGeoData()
        {
            Latitude = 0;
            Longitude = 0;
            TimeZone = TimeZoneInfo.Local;
            ServerExternalIPAddress = IPAddress.Any;

            GetServerDefaults();
        }

        private static void GetServerDefaults()
        {
            // get GEO data from GeoBytes
            string
                json = null,
                url = @"http://api.ipify.org/?format=json";

            using (WebClient client = new WebClient())
            {
                json = Encoding.ASCII.GetString(client.DownloadData(url));
            }

            if (json == null)
            {
                return;
            }

            if (!(JsonConvert.DeserializeObject(json) is Dictionary<string, object> data))
            {
                return;
            }

            // {"ip":"12.180.251.201"}
            if (data["ip"] is string ipaddress)
            {
                IPAddress.TryParse(ipaddress, out IPAddress ip);

                if (ip != null) ServerExternalIPAddress = ip;
            }

            url = string.Format(
                @"https://freegeoip.net/json/{0}",
                ServerExternalIPAddress
            );

            using (WebClient client = new WebClient())
            {
                json = Encoding.ASCII.GetString(client.DownloadData(url));
            }

            if (json == null)
            {
                return;
            }

            data = JsonConvert.DeserializeObject(json) as Dictionary<string, object>;

            if (data == null)
            {
                return;
            }

            /*{
                "ip" : "192.30.252.131",
                "country_code" : "US",
                "country_name" : "United States",
                "region_code" : "CA",
                "region_name" : "California",
                "city" : "San Francisco",
                "zip_code" : "94107",
                "time_zone" : "America/Los_Angeles",
                "latitude" : 37.77,
                "longitude" : -122.394,
                "metro_code" : 807
            }*/

            if (data["latitude"] != null)
            {
                Latitude = Convert.ToDouble(data["latitude"]);
            }

            if (data["longitude"] != null)
            {
                Longitude = Convert.ToDouble(data["longitude"]);
            }
        }
    }
}
