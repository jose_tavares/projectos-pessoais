﻿using System;
using System.IO;
using System.Drawing;
using static Common.Enums;
using System.Drawing.Imaging;

namespace Common.Helpers
{
    public static class ImageHelper
    {
        public static void Write(Stream inStream, Stream outStream, int panelWidth, int panelHeight, RenderModes mode)
        {
            Bitmap bmpSrc = null, bmpTrg = null;

            try
            {
                inStream.Position = 0;
                bmpSrc = new Bitmap(inStream);

                // convert TIFF to BMP, use only the first page
                FrameDimension fd = new FrameDimension(bmpSrc.FrameDimensionsList[0]);
                bmpSrc.SelectActiveFrame(fd, 0);

                // crop/fit/stretch
                int imageHeight = bmpSrc.Height, imageWidth = bmpSrc.Width, targetWidth, targetHeight;

                if (panelWidth <= 0) panelWidth = imageWidth;
                if (panelHeight <= 0) panelHeight = imageHeight;

                if ((panelWidth != imageWidth) || (panelHeight != imageHeight))
                {
                    switch (mode)
                    {
                        case RenderModes.Stretch:
                            if ((panelWidth <= 120) && (panelHeight <= 120))
                                bmpTrg = new Bitmap(bmpSrc.GetThumbnailImage(panelWidth, panelHeight, () => false, IntPtr.Zero));
                            else
                                bmpTrg = new Bitmap(bmpSrc, new Size(panelWidth, panelHeight));

                            break;

                        case RenderModes.Fit:
                            targetWidth = imageWidth;
                            targetHeight = imageHeight;
                            float scale = 1F;

                            // a. panel is greater than image: grow
                            if ((panelHeight > imageHeight) && (panelWidth > imageWidth))
                            {
                                scale = Math.Min((float)panelWidth / imageWidth, (float)panelHeight / imageHeight);
                                targetHeight = Math.Min((int)(imageHeight * scale), panelHeight);
                                targetWidth = Math.Min((int)(imageWidth * scale), panelWidth);
                            }

                            // b. image is greater than panel: shrink
                            else
                            {
                                scale = Math.Max((float)imageWidth / panelWidth, (float)imageHeight / panelHeight);
                                targetWidth = Math.Min((int)(imageWidth / scale), panelWidth);
                                targetHeight = Math.Min((int)(imageHeight / scale), panelHeight);
                            }

                            if (targetWidth <= 120 && targetHeight <= 120)
                                bmpTrg = new Bitmap(bmpSrc.GetThumbnailImage(targetWidth, targetHeight, () => false, IntPtr.Zero));
                            else
                                bmpTrg = new Bitmap(bmpSrc, new Size(targetWidth, targetHeight));

                            break;

                        case RenderModes.Crop:
                            targetWidth = Math.Min(panelWidth, imageWidth);
                            targetHeight = Math.Min(panelHeight, imageHeight);

                            bmpTrg = bmpSrc.Clone(
                                new Rectangle(0, 0, targetWidth, targetHeight),
                                bmpSrc.PixelFormat
                            );

                            break;
                    }

                    bmpTrg.Save(outStream, ImageFormat.Png);
                }

                else
                    bmpSrc.Save(outStream, ImageFormat.Png);

                outStream.Seek(0, SeekOrigin.Begin);
            }

            finally
            {
                if (bmpTrg != null) bmpTrg.Dispose();
                if (bmpSrc != null) bmpSrc.Dispose();

                GC.Collect();
            }
        }        
    }
}
