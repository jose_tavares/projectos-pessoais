﻿using System;
using System.IO;
using AutoMapper;
using System.Text;
using System.Linq;
using Domain.Models;
using Common.Helpers;
using Common.Extensions;
using static Common.Enums;
using Repositories.Services;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Domain.ViewModels.Management;

namespace Management.Controllers
{
    public class MediaController : BaseController
    {
        private readonly IContentService _contentService;

        public MediaController
            (IMapper mapper,
            IUnitOfWork unitOfWork,
            IContentService contentService) : base(mapper, unitOfWork) =>
                _contentService = contentService;

        public IActionResult Index(ContentPageViewModel contentPageViewModel)
        {
            var contentsModel = _contentService.All(contentPageViewModel.ContentTypes).ToList();

            var contentsWithSizeViewModel = contentsModel.Select(select =>
                _mapper.Map<Content, ContentWithSizeViewModel>(select)).ToList();

            contentsWithSizeViewModel.ForEach(each =>
                each.Size = contentsModel[contentsWithSizeViewModel.IndexOf(each)].Data.Length / 1024
            );

            contentPageViewModel.ContentsWithSizeViewModel = contentsWithSizeViewModel;

            contentPageViewModel.SelectListContentTypes = Enum.GetValues(typeof(ContentTypes)).Cast<ContentTypes>()
                .ToSelectListItems
                    (text => text.GetDescription(),
                    value => value.GetValue().ToString(),
                    isSelected => contentPageViewModel.ContentTypes.HasValue && (isSelected.GetValue() == (int)contentPageViewModel.ContentTypes.Value),
                    defaultTextOption: "Todos");

            return View(contentPageViewModel);
        }

        [HttpGet, ActionName("Thumb")]
        public async Task<IActionResult> ThumbAsync(int id, int width = 0, int height = 0, RenderModes mode = RenderModes.Fit, int trace = 0)
        {
            if (id == 0)
            {
                return new EmptyResult();
            }

            var errorMessage = new StringBuilder();

            try
            {
                var data = await _contentService.SingleOrDefaultAsync(id);

                using var source = new MemoryStream(data.Data);
                var destination = new MemoryStream();
                ImageHelper.Write(source, destination, width, height, mode);

                return new FileStreamResult(destination, "image/png");
            }

            catch (Exception ex)
            {
                if (trace > 0)
                {
                    errorMessage.Append(ex.ToString());
                }
            }

            if (trace == 0)
            {
                return RedirectToAction("BadImg");
            }
            else
            {
                return Content((errorMessage.Length == 0) ? "OK" : errorMessage.ToString());
            }
        }

        public IActionResult Create() =>
            View(new ContentViewModel());

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(ContentViewModel contentViewModel)
        {
            bool hasFiles = false, addedFiles = false;

            var pictureSupportedFormats = Enum.GetValues(typeof(PictureSupportedFormats))
                .Cast<PictureSupportedFormats>()
                .Select(select => select.ToString());

            var videoSupportedFormats = Enum.GetValues(typeof(VideoSupportedFormats))
                .Cast<VideoSupportedFormats>()
                .Select(select => select.ToString());

            if (contentViewModel.Files.Validate())
            {
                foreach (var file in contentViewModel.Files)
                {
                    if ((file != null) && file.Length > 0)
                    {
                        string extension = Path.GetExtension(file.FileName).Replace(".", "").ToUpper();
                        bool isPicture = pictureSupportedFormats.Contains(extension);
                        bool isVideo = videoSupportedFormats.Contains(extension);

                        if (isPicture || isVideo)
                        {
                            byte[] buffer = null;

                            using (var reader = new BinaryReader(file.OpenReadStream()))
                            {
                                buffer = reader.ReadBytes(Convert.ToInt32(file.Length));

                                contentViewModel.Type = isPicture ? ContentTypes.Picture : ContentTypes.Video;
                                contentViewModel.Name = Path.GetFileName(file.FileName);
                                contentViewModel.Data = buffer;

                                _contentService.Add
                                    (_mapper.Map<ContentViewModel, Content>(contentViewModel));

                                addedFiles = true;
                            }

                            hasFiles = true;
                        }
                    }
                }
            }

            if (addedFiles)
            {
                return RedirectToAction("Index");
            }

            else if (hasFiles)
            {
                return Error();
            }

            return View(contentViewModel);
        }

        public IActionResult Delete(int id = 0)
        {
            var contentModel = _contentService.SingleOrDefault(id);

            if (contentModel == null)
            {
                return Error();
            }

            var contentViewModel = _mapper.Map<Content, ContentViewModel>(contentModel);

            return View(contentViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(ContentViewModel contentViewModel)
        {
            var contentModel = _contentService.SingleOrDefault(contentViewModel.ContentId);

            if (contentModel == null)
            {
                return Error();
            }

            _contentService.Remove(contentModel);

            return RedirectToAction("Index");
        }

        public IActionResult Edit(int id = 0)
        {
            var contentModel = _contentService.SingleOrDefault(id);

            if (contentModel == null)
            {
                return Error();
            }

            var contentViewModel = _mapper.Map<Content, ContentViewModel>(contentModel);

            return View(contentViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(ContentViewModel contentViewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(contentViewModel);
            }

            var contentModel = _contentService.SingleOrDefault(contentViewModel.ContentId);

            if (contentModel == null)
            {
                return Error();
            }

            _mapper.Map(contentViewModel, contentModel);

            _contentService.Edit(contentModel);

            return RedirectToAction("Index");
        }

        [HttpGet, ActionName("Playback")]
        public async Task<IActionResult> PlaybackAsync(int id)
        {
            var contentModel = await _contentService.SingleOrDefaultAsync(id);

            if (contentModel.Data == null)
            {
                return Content("Não suportado");
            }

            string contentType = string.Format
                ("video/{0}",
                Path.GetExtension(contentModel.Name).Replace(".", "").ToLower());

            return File(contentModel.Data, contentType);
        }

        public IActionResult BadImg() =>
            File("images/question.png", "image/png");
    }
}
