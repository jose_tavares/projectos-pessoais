﻿using AutoMapper;
using Repositories.Services;
using Microsoft.AspNetCore.Mvc;
using Repositories.Implementations;

namespace Management.Controllers
{
    public class HomeController : BaseController
    {
        private readonly IHomeService _homeService;
        private readonly IFrameService _frameService;

        public HomeController
            (IMapper mapper,
            IUnitOfWork unitOfWork,
            IHomeService homeService,
            IFrameService frameService) : base(mapper, unitOfWork)
        {
            _homeService = homeService;
            _frameService = frameService;
        }

        public IActionResult Index() => View();

        public IActionResult Dashboard()
        {
            var homeViewModel = new HomeSettings(_homeService, _frameService).GetViewModel();

            return View(homeViewModel);
        }
    }
}
