﻿using AutoMapper;
using System.Linq;
using Domain.Models;
using Common.Extensions;
using static Common.Enums;
using Repositories.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Domain.ViewModels.Management;

namespace Management.Controllers
{
    public class CanvasController : BaseController
    {
        private readonly ICanvasService _canvasService;
        private readonly IContentService _contentService;

        public CanvasController
            (IMapper mapper,
            IUnitOfWork unitOfWork,
            ICanvasService canvasService,
            IContentService contentService) : base(mapper, unitOfWork)
        {
            _canvasService = canvasService;
            _contentService = contentService;
        }

        public IActionResult Index()
        {
            var canvasesViewModel = _canvasService.All().Select(select =>
                _mapper.Map<Canvas, CanvasViewModel>(select));

            return View(canvasesViewModel);
        }

        public IActionResult Create()
        {
            var canvasViewModel = new CanvasViewModel
            {
                SelectListBackgroundImages = _contentService.All(ContentTypes.Picture)
                    .OrderBy(orderBy => orderBy.Name).ToSelectListItems
                        (text => text.Name,
                        value => value.ContentId.ToString(),
                        defaultTextOption: "Sem fundo de ecrã")
            };

            return View(canvasViewModel);
        }

        [HttpPost]
        public IActionResult Create(CanvasViewModel canvasViewModel)
        {
            if (!ModelState.IsValid)
            {
                canvasViewModel.SelectListBackgroundImages = _contentService.All(ContentTypes.Picture)
                    .OrderBy(orderBy => orderBy.Name).ToSelectListItems
                        (text => text.Name,
                        value => value.ContentId.ToString(),
                        defaultTextOption: "Sem fundo de ecrã");
            }

            _canvasService.Add
                (_mapper.Map<CanvasViewModel, Canvas>
                    (canvasViewModel));

            return RedirectToAction("Index");
        }

        public IActionResult Delete(int id = 0)
        {
            var canvasModel = _canvasService.SingleOrDefault(id);

            if (canvasModel == null)
            {
                return Error();
            }

            var canvasViewModel = _mapper.Map<Canvas, CanvasViewModel>(canvasModel);

            return View(canvasViewModel);
        }

        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            if (_canvasService.SingleOrDefault(id) == null)
            {
                return Error();
            }

            _canvasService.Remove(id);

            return RedirectToAction("Index");
        }

        public IActionResult Edit(int id = 0)
        {
            var canvasModel = _canvasService.SingleOrDefault(id);

            if (canvasModel == null)
            {
                return Error();
            }

            var canvasViewModel = _mapper.Map<Canvas, CanvasViewModel>(canvasModel);

            canvasViewModel.SelectListBackgroundImages = _contentService.All(ContentTypes.Picture)
                    .OrderBy(orderBy => orderBy.Name).ToSelectListItems
                        (text => text.Name,
                        value => value.ContentId.ToString(),
                        defaultTextOption: "Sem fundo de ecrã");

            return View("Create", canvasViewModel);
        }

        [HttpPost]
        public IActionResult Edit(CanvasViewModel canvasViewModel)
        {
            if (!ModelState.IsValid)
            {
                canvasViewModel.SelectListBackgroundImages = _contentService.All(ContentTypes.Picture)
                    .OrderBy(orderBy => orderBy.Name).ToSelectListItems
                        (text => text.Name,
                        value => value.ContentId.ToString(),
                        defaultTextOption: "Sem fundo de ecrã");
            }

            var canvasModel = _canvasService.SingleOrDefault(canvasViewModel.CanvasId);

            if (canvasModel == null)
            {
                return Error();
            }

            _mapper.Map(canvasViewModel, canvasModel);

            _canvasService.Edit(canvasModel);

            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult Data(int id = 0)
        {
            var canvasesModel = _canvasService.All();

            var canvasesViewModel = canvasesModel.Select(select =>
                _mapper.Map<Canvas, CanvasViewModel>(select)).ToList();

            if (canvasesViewModel.Validate())
            {
                foreach (var canvasViewModel in canvasesViewModel)
                {
                    canvasViewModel.PanelViewModels = canvasesModel.First(first => first.CanvasId == canvasViewModel.CanvasId)
                        .Panel.Select(select =>
                            _mapper.Map<Panel, PanelViewModel>(select));

                    if (!canvasViewModel.PanelViewModels.Any())
                    {
                        continue;
                    }

                    SetIsFullscreen(canvasViewModel, canvasesModel);
                }
            }

            return Json(canvasesViewModel.Where(where => where.CanvasId == id).Select(canvas => new
            {
                canvas.BackgroundImage,
                canvas.BackgroundColor,
                canvas.Width,
                canvas.Height,
                Panels = canvas.PanelViewModels.Where(where => !where.IsFullscreen).Select(painel => new
                {
                    painel.PanelId,
                    painel.Name,
                    painel.Left,
                    painel.Top,
                    painel.Width,
                    painel.Height
                })
            }).FirstOrDefault());
        }

        private void SetIsFullscreen(CanvasViewModel canvasViewModel, IEnumerable<Canvas> canvasesModel)
        {
            foreach (var panelViewModel in canvasViewModel.PanelViewModels.ToList())
            {
                if (canvasesModel.First(first => first.CanvasId == canvasViewModel.CanvasId)
                    .Panel.First(first => first.PanelId == panelViewModel.PanelId).FullScreen != null)
                {
                    panelViewModel.IsFullscreen = true;
                }
            }
        }
    }
}
