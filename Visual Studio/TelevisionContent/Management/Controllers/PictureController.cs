﻿using AutoMapper;
using Domain.Models;
using Repositories.Services;
using Microsoft.AspNetCore.Mvc;
using Domain.ViewModels.Management;
using Repositories.Implementations;

namespace Management.Controllers
{
    public class PictureController : BaseController
    {
        private readonly IPanelService _panelService;
        private readonly ICanvasService _canvasService;
        private readonly IContentService _contentService;
        private readonly IPictureService _pictureService;
        private readonly ITemplateService _templateService;

        public PictureController
            (IMapper mapper,
            IUnitOfWork unitOfWork,
            IPanelService panelService,
            ICanvasService canvasService,
            IContentService contentService,
            IPictureService pictureService,
            ITemplateService templateService) : base(mapper, unitOfWork)
        {
            _panelService = panelService;
            _canvasService = canvasService;
            _contentService = contentService;
            _pictureService = pictureService;
            _templateService = templateService;
        }

        public IActionResult Create(int canvasId = 0, int panelId = 0)
        {
            if (panelId == 0)
            {
                return RedirectToAction("Create", "Frame");
            }

            var picturesViewModel = new PictureViewModel
            {
                CanvasId = canvasId,
                Canvas = _canvasService.SingleOrDefault(canvasId).Name,
                Panel = _panelService.SingleOrDefault(panelId).Name,
            };

            new PictureSettings(_contentService, _templateService)
                .SetSelectListItems(picturesViewModel);

            return View(picturesViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(PictureViewModel pictureViewModel)
        {
            if (!ModelState.IsValid)
            {
                new PictureSettings(_contentService, _templateService)
                    .SetSelectListItems(pictureViewModel);

                return View(pictureViewModel);
            }

            var pictureModel = _mapper.Map<PictureViewModel, Picture>(pictureViewModel);

            pictureModel.Frame = _mapper.Map<PictureViewModel, Frame>(pictureViewModel);

            _pictureService.Add(pictureModel);

            return RedirectToAction("Index", "Frame");
        }

        public IActionResult Delete(int id)
        {
            var pictureModel = _pictureService.SingleOrDefault(id);

            if (pictureModel == null)
            {
                return Error();
            }

            var pictureViewModel = _mapper.Map<Picture, PictureViewModel>(pictureModel);
            _mapper.Map(pictureModel.Frame, pictureViewModel);

            pictureViewModel.Canvas = _canvasService.SingleOrDefault(pictureViewModel.CanvasId).Name;
            pictureViewModel.Panel = _panelService.SingleOrDefault(pictureViewModel.PanelId).Name;

            return View(pictureViewModel);
        }

        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            var pictureModel = _pictureService.SingleOrDefault(id);

            if (pictureModel == null)
            {
                return Error();
            }

            _pictureService.Remove(pictureModel.FrameId);

            return RedirectToAction("Index", "Frame");
        }

        public IActionResult Edit(int id = 0, int canvasId = 0, int panelId = 0)
        {
            var pictureModel = _pictureService.SingleOrDefault(id);

            if ((pictureModel == null) && (pictureModel.Frame == null) && (pictureModel.Content == null))
            {
                return Error();
            }

            var pictureViewModel = _mapper.Map<Picture, PictureViewModel>(pictureModel);
            _mapper.Map(pictureModel.Frame, pictureViewModel);

            new PictureSettings(_contentService, _templateService)
                .SetSelectListItems(pictureViewModel);

            pictureViewModel.Canvas = _canvasService.SingleOrDefault(canvasId).Name;
            pictureViewModel.Panel = _panelService.SingleOrDefault(panelId).Name;

            return View("Create", pictureViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(PictureViewModel pictureViewModel)
        {
            if (!ModelState.IsValid)
            {
                new PictureSettings(_contentService, _templateService)
                    .SetSelectListItems(pictureViewModel);

                return View("Create", pictureViewModel);
            }

            var pictureModel = _pictureService.SingleOrDefault(pictureViewModel.FrameId);

            if ((pictureModel == null) && (pictureModel.Frame == null) && (pictureModel.Content == null))
            {
                return Error();
            }

            _mapper.Map(pictureViewModel, pictureModel);
            _mapper.Map(pictureViewModel, pictureModel.Frame);

            if (pictureModel.ContentId != pictureModel.Content.ContentId)
            {
                pictureModel.Content = _contentService.SingleOrDefault(pictureModel.ContentId);
            }

            _pictureService.Edit(pictureModel);

            return RedirectToAction("Index", "Frame");
        }
    }
}