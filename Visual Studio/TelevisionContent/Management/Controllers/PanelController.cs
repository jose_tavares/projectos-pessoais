﻿using AutoMapper;
using System.Linq;
using Domain.Models;
using Common.Extensions;
using Repositories.Services;
using Microsoft.AspNetCore.Mvc;
using Domain.ViewModels.Management;

namespace Management.Controllers
{
    public class PanelController : BaseController
    {
        private readonly IPanelService _panelService;
        private readonly ICanvasService _canvasService;

        public PanelController
            (IMapper mapper,
            IUnitOfWork unitOfWork,
            IPanelService panelService,
            ICanvasService canvasService) : base(mapper, unitOfWork)
        {
            _panelService = panelService;
            _canvasService = canvasService;
        }

        public IActionResult Index(PanelIndexViewModel panelIndexViewModel)
        {
            var panelsModel = _panelService.All(panelIndexViewModel.CanvasId, panelIndexViewModel.PainelName?.ToLower()).ToList();

            var panelsViewModel = panelsModel.Select(select =>
                _mapper.Map<Panel, PanelViewModel>(select)).ToList();

            if (panelsViewModel.Validate())
            {
                foreach (var panelViewModel in panelsViewModel)
                {
                    var canvasModel = panelsModel[panelsViewModel.IndexOf(panelViewModel)].Canvas;
                    var fullscreenModel = panelsModel[panelsViewModel.IndexOf(panelViewModel)].FullScreen;

                    if ((canvasModel == null) && (fullscreenModel == null))
                    {
                        continue;
                    }

                    if (canvasModel != null)
                    {
                        _mapper.Map(canvasModel, panelViewModel);
                    }

                    if (fullscreenModel != null)
                    {
                        _mapper.Map(fullscreenModel, panelViewModel);
                        panelViewModel.IsFullscreen = true;
                    }
                }
            }

            panelIndexViewModel.PanelsViewModel = panelsViewModel;
            panelIndexViewModel.SelectListCanvases = _canvasService.All().OrderBy(orderBy => orderBy.Name).ToSelectListItems
                (text => text.Name,
                value => value.CanvasId.ToString(),
                defaultTextOption: "Todos");

            return View(panelIndexViewModel);
        }

        public IActionResult Create(int canvasId = 0)
        {
            var panelViewModel = new PanelViewModel
            {
                SelectListCanvases = _canvasService.All().OrderBy(orderBy => orderBy.Name).ToSelectListItems
                    (text => text.Name,
                    value => value.CanvasId.ToString(),
                    isSelected => isSelected.CanvasId == canvasId)
            };

            return View(panelViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(PanelViewModel panelViewModel)
        {
            if (!ModelState.IsValid)
            {
                panelViewModel.SelectListCanvases = _canvasService.All().OrderBy(orderBy => orderBy.Name).ToSelectListItems
                    (text => text.Name,
                    value => value.CanvasId.ToString(),
                    isSelected => isSelected.CanvasId == panelViewModel.CanvasId);

                return View(panelViewModel);
            }

            _panelService.Add
                (_mapper.Map<PanelViewModel, Panel>(panelViewModel));

            return RedirectToAction("Index");
        }

        public IActionResult Delete(int id = 0)
        {
            var panelModel = _panelService.SingleOrDefault(id);

            if (panelModel == null)
            {
                return Error();
            }

            var panelViewModel = _mapper.Map<Panel, PanelViewModel>(panelModel);

            if (panelModel.Canvas != null)
            {
                _mapper.Map(panelModel.Canvas, panelViewModel);
            }

            if (panelModel.FullScreen != null)
            {
                _mapper.Map(panelModel.FullScreen, panelViewModel);
                panelViewModel.IsFullscreen = true;
            }

            return View(panelViewModel);
        }

        public IActionResult Edit(int id = 0, bool fullscreen = false)
        {
            if (fullscreen)
            {
                return RedirectToAction("EditFullscreen", new { id });
            }

            var panelModel = _panelService.SingleOrDefault(id);

            if (panelModel == null)
            {
                return Error();
            }

            var panelViewModel = _mapper.Map<Panel, PanelViewModel>(panelModel);

            if (panelModel.Canvas != null)
            {
                _mapper.Map(panelModel.Canvas, panelViewModel);
            }

            if (panelModel.FullScreen != null)
            {
                _mapper.Map(panelModel.FullScreen, panelViewModel);
            }

            panelViewModel.SelectListCanvases = _canvasService.All().OrderBy(orderBy => orderBy.Name).ToSelectListItems
                    (text => text.Name,
                    value => value.CanvasId.ToString(),
                    isSelected => isSelected.CanvasId == panelModel.CanvasId);

            return View("Create", panelViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(PanelViewModel panelViewModel)
        {
            var panelModel = _panelService.SingleOrDefault(panelViewModel.PanelId);

            if (panelModel == null)
            {
                return Error();
            }

            if (!ModelState.IsValid)
            {
                panelViewModel.SelectListCanvases = _canvasService.All().OrderBy(orderBy => orderBy.Name).ToSelectListItems
                    (text => text.Name,
                    value => value.CanvasId.ToString(),
                    isSelected => isSelected.CanvasId == panelViewModel.CanvasId);

                return View(panelViewModel);
            }

            _panelService.Edit
                (_mapper.Map(panelViewModel, panelModel));

            return RedirectToAction("Index");
        }

        public IActionResult EditFullscreen(int id = 0)
        {
            var fullscreenModel = _panelService.SingleOrDefault(id);

            if (fullscreenModel == null)
            {
                return Error();
            }

            var fullscreenViewModel = _mapper.Map<Panel, PanelViewModel>(fullscreenModel);
            _mapper.Map(fullscreenModel.Canvas, fullscreenViewModel);
            _mapper.Map(fullscreenModel.FullScreen, fullscreenViewModel);

            return View(fullscreenViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult EditFullscreen(PanelViewModel panelViewModel)
        {
            var panelModel = _panelService.SingleOrDefault(panelViewModel.PanelId);

            if (panelModel == null)
            {
                return Error();
            }

            if (!ModelState.IsValid)
            {
                return View(panelViewModel);
            }

            panelModel.FullScreen.MaxIdleInterval = panelViewModel.MaxIdleInterval;
            panelModel.FadeLength = panelViewModel.FadeLength;

            _panelService.Edit(panelModel);

            return RedirectToAction("Index");
        }

        [ValidateAntiForgeryToken]
        [HttpPost, ActionName("Delete")]
        public IActionResult DeleteConfirmed(int id)
        {
            var panelModel = _panelService.SingleOrDefault(id);

            if (panelModel == null)
            {
                return Error();
            }

            _panelService.Remove(id);

            return RedirectToAction("Index");
        }
    }
}
