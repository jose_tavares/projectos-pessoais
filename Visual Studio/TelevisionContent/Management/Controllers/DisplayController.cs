﻿using System;
using AutoMapper;
using System.Linq;
using Domain.Models;
using Common.Extensions;
using Repositories.Services;
using Microsoft.AspNetCore.Mvc;
using Domain.ViewModels.Management;

namespace Management.Controllers
{
    public class DisplayController : BaseController
    {
        private readonly ICanvasService _canvasService;
        private readonly IDisplayService _displayService;
        private readonly ILocationService _locationService;

        public DisplayController
            (IMapper mapper,
            IUnitOfWork unitOfWork,
            ICanvasService canvasService,
            IDisplayService displayService,
            ILocationService locationService) : base(mapper, unitOfWork)
        {
            _canvasService = canvasService;
            _displayService = displayService;
            _locationService = locationService;
        }

        public IActionResult Index(DisplayPageViewModel displayPageViewModel)
        {
            var displaysModel = _displayService.All
                (displayPageViewModel.CanvasId,
                displayPageViewModel.LocationId,
                displayPageViewModel.Name,
                displayPageViewModel.Host).ToList();

            var displayViewModels = displaysModel.Select(select =>
                _mapper.Map<Display, DisplayViewModel>(select)
            ).ToList();

            if (displayViewModels.Validate())
            {
                foreach (var displayViewModel in displayViewModels)
                {
                    var canvasModel = displaysModel[displayViewModels.IndexOf(displayViewModel)].Canvas;
                    var locationModel = displaysModel[displayViewModels.IndexOf(displayViewModel)].Location;

                    if ((canvasModel == null) && (locationModel == null))
                    {
                        continue;
                    }

                    if (canvasModel != null)
                    {
                        _mapper.Map(canvasModel, displayViewModel);
                    }

                    if (locationModel != null)
                    {
                        _mapper.Map(locationModel, displayViewModel);
                    }
                }
            }

            displayPageViewModel.DisplayViewModels = displayViewModels;
            displayPageViewModel.SelectListCanvases = _canvasService.All()
                .ToSelectListItems
                    (text => text.Name,
                    value => value.CanvasId.ToString(),
                    defaultTextOption: "Todos");

            displayPageViewModel.SelectListLocations = _locationService.All()
                .Select(select => new
                {
                    select.LocationId,
                    Name = (select.Area == null) ?
                        string.Format("{0}: {1}", select.Level.Name, select.Name) :
                        string.Format("{0}: {1}: {2}", select.Level.Name, select.Area.Name, select.Name)
                })
                .OrderBy(orderBy => orderBy.Name).ToSelectListItems
                    (text => text.Name,
                    value => value.LocationId.ToString(),
                    defaultTextOption: "Todos");

            return View(displayPageViewModel);
        }

        public IActionResult Create(int canvasId = 0, int locationId = 0)
        {
            var displayViewModel = new DisplayViewModel
            {
                CanvasId = canvasId,
                LocationId = locationId,

                SelectListCanvases = _canvasService.All()
                    .ToSelectListItems
                        (text => text.Name,
                        value => value.CanvasId.ToString()),

                SelectListLocations = _locationService.All()
                    .Select(select => new
                    {
                        select.LocationId,
                        Name = (select.Area == null) ?
                            string.Format("{0}: {1}", select.Level.Name, select.Name) :
                            string.Format("{0}: {1}: {2}", select.Level.Name, select.Area.Name, select.Name)
                    })
                    .OrderBy(orderBy => orderBy.Name).ToSelectListItems
                        (text => text.Name,
                        value => value.LocationId.ToString())
            };

            return View(displayViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(DisplayViewModel displayViewModel)
        {
            if (!ModelState.IsValid)
            {
                displayViewModel.SelectListCanvases = _canvasService.All()
                    .ToSelectListItems
                        (text => text.Name,
                        value => value.CanvasId.ToString());

                displayViewModel.SelectListLocations = _locationService.All()
                    .Select(select => new
                    {
                        select.LocationId,
                        Name = (select.Area == null) ?
                            string.Format("{0}: {1}", select.Level.Name, select.Name) :
                            string.Format("{0}: {1}: {2}", select.Level.Name, select.Area.Name, select.Name)
                    })
                    .OrderBy(orderBy => orderBy.Name).ToSelectListItems
                        (text => text.Name,
                        value => value.LocationId.ToString());

                return View(displayViewModel);
            }

            var displayModel = _mapper.Map<DisplayViewModel, Display>(displayViewModel);

            if (TimeSpan.TryParse(displayViewModel.AutoRefreshAt, out TimeSpan parsed))
            {
                displayModel.RecycleTime = parsed;
            }
            else
            {
                displayModel.RecycleTime = null;
            }

            _displayService.Add(displayModel);

            return RedirectToAction("Index");
        }

        public IActionResult Delete(int id = 0)
        {
            var displayModel = _displayService.SingleOrDefault(id);

            var displayViewModel = _mapper.Map<Display, DisplayViewModel>(displayModel);
            _mapper.Map(displayModel.Canvas, displayViewModel);
            _mapper.Map(displayModel.Location, displayViewModel);

            if (displayModel == null)
            {
                return Error();
            }

            return View(displayViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(DisplayViewModel displayViewModel)
        {

            var displayModel = _displayService.SingleOrDefault(displayViewModel.DisplayId);

            if (displayModel == null)
            {
                return Error();
            }

            _displayService.Remove(displayModel.DisplayId);

            return RedirectToAction("Index");
        }

        public IActionResult Edit(int id = 0)
        {
            var displayModel = _displayService.SingleOrDefault(id);

            if (displayModel == null)
            {
                return Error();
            }

            var displayViewModel = _mapper.Map<Display, DisplayViewModel>(displayModel);
            _mapper.Map(displayModel.Canvas, displayViewModel);
            _mapper.Map(displayModel.Location, displayViewModel);

            displayViewModel.SelectListCanvases = _canvasService.All()
                .ToSelectListItems
                    (text => text.Name,
                    value => value.CanvasId.ToString());

            displayViewModel.SelectListLocations = _locationService.All()
                .Select(select => new
                {
                    select.LocationId,
                    Name = (select.Area == null) ?
                        string.Format("{0}: {1}", select.Level.Name, select.Name) :
                        string.Format("{0}: {1}: {2}", select.Level.Name, select.Area.Name, select.Name)
                })
                .OrderBy(orderBy => orderBy.Name).ToSelectListItems
                    (text => text.Name,
                    value => value.LocationId.ToString());

            return View("Create", displayViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(DisplayViewModel displayViewModel)
        {
            var displayModel = _displayService.SingleOrDefault(displayViewModel.DisplayId);

            if (displayModel == null)
            {
                return Error();
            }

            if (!ModelState.IsValid)
            {
                displayViewModel.SelectListCanvases = _canvasService.All()
                    .ToSelectListItems
                        (text => text.Name,
                        value => value.CanvasId.ToString());

                displayViewModel.SelectListLocations = _locationService.All()
                    .Select(select => new
                    {
                        select.LocationId,
                        Name = (select.Area == null) ?
                            string.Format("{0}: {1}", select.Level.Name, select.Name) :
                            string.Format("{0}: {1}: {2}", select.Level.Name, select.Area.Name, select.Name)
                    })
                    .OrderBy(orderBy => orderBy.Name).ToSelectListItems
                        (text => text.Name,
                        value => value.LocationId.ToString());

                return View("Create", displayViewModel);
            }

            _mapper.Map(displayViewModel, displayModel);

            if (TimeSpan.TryParse(displayViewModel.AutoRefreshAt, out TimeSpan parsed))
            {
                displayModel.RecycleTime = parsed;
            }
            else
            {
                displayModel.RecycleTime = null;
            }

            _displayService.Edit(displayModel);

            return RedirectToAction("Index");
        }
    }
}
