﻿using System;
using AutoMapper;
using System.Linq;
using Domain.Models;
using Common.Extensions;
using static Common.Enums;
using Repositories.Services;
using Microsoft.AspNetCore.Mvc;
using Repositories.Implementations;
using Domain.ViewModels.Management;

namespace Management.Controllers
{
    public class FrameController : BaseController
    {
        private readonly IFrameService _frameService;
        private readonly IPanelService _panelService;
        private readonly ICanvasService _canvasService;
        private readonly ITemplateService _templateService;

        public FrameController
            (IMapper mapper,
            IUnitOfWork unitOfWork,
            IFrameService frameService,
            IPanelService panelService,
            ICanvasService canvasService,
            ITemplateService templateService) : base(mapper, unitOfWork)
        {
            _frameService = frameService;
            _panelService = panelService;
            _canvasService = canvasService;
            _templateService = templateService;
        }

        public IActionResult Index(int canvasId = 0, int panelId = 0, FrameTypes? frameType = null, TimingOptions? timingOption = null)
        {
            var framesPageViewModel = new FrameSettings(_mapper, _frameService, _canvasService, _templateService)
                .GetViewModel(panelId, canvasId, frameType, timingOption);

            return View(framesPageViewModel);
        }

        public IActionResult Create(int canvasId = 0, int panelId = 0, FrameTypes? frameType = null)
        {
            var panelViewModel = _mapper.Map<Panel, PanelViewModel>
                (_panelService.SingleOrDefault(canvasId));

            if (panelViewModel == null)
            {
                panelId = 0;
            }

            if (!frameType.HasValue || (panelViewModel == null))
            {
                if ((canvasId == 0) && (panelViewModel != null))
                {
                    canvasId = panelViewModel.CanvasId;
                }

                return RedirectToAction("ForFrameType", new
                {
                    canvasId,
                    panelId,
                    frameType
                });
            }

            return RedirectToAction("Create", frameType.Value.ToString());
        }

        public IActionResult Delete(int id = 0)
        {
            if (_frameService.SingleOrDefault(id) == null)
            {
                return Error();
            }

            var frameTypes = _frameService.GetType(id);

            if (!frameTypes.HasValue)
            {
                return Error();
            }

            return RedirectToAction("Delete", frameTypes.Value.ToString(), new { id });
        }

        public IActionResult Edit(int id = 0, int canvasId = 0, int panelId = 0)
        {
            if (_frameService.SingleOrDefault(id) == null)
            {
                return Error();
            }

            var frameTypes = _frameService.GetType(id);

            if (!frameTypes.HasValue && (canvasId == 0) && (panelId == 0))
            {
                return Error();
            }

            return RedirectToAction("Edit", frameTypes.Value.ToString(), new { id, panelId, canvasId });
        }

        public IActionResult ForFrameType(int canvasId = 0, int panelId = 0, FrameTypes? frameType = null)
        {
            if (canvasId > 0)
            {
                var canvas = _canvasService.All()
                    .OrderBy(orderBy => orderBy.Name)
                    .FirstOrDefault();

                if (canvas != null)
                {
                    canvasId = canvas.CanvasId;
                }
            }

            var frameSelector = new FrameSelectorViewModel
            {
                CanvasId = canvasId,
                PanelId = panelId,
                FrameType = frameType,

                SelectListCanvases = _canvasService.All().ToSelectListItems
                    (text => text.Name,
                    value => value.CanvasId.ToString(),
                    isSelected => isSelected.CanvasId == canvasId),

                SelectListFrameTypes = Enum.GetValues(typeof(FrameTypes)).Cast<FrameTypes>().ToSelectListItems
                    (text => text.GetDescription(),
                    value => value.GetValue().ToString(),
                    isSelected => frameType.HasValue && (isSelected == frameType.Value)),
            };

            return View(frameSelector);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ForFrameType(FrameSelectorViewModel frameSelector)
        {
            if ((frameSelector.PanelId == 0) || (frameSelector.FrameType == null))
            {
                return RedirectToAction("ForFrameType", new
                {
                    canvasId = frameSelector.CanvasId,
                    panelId = frameSelector.PanelId,
                    frameType = frameSelector.FrameType
                });
            }

            var panelViewModel = _mapper.Map<Panel, PanelViewModel>
                (_panelService.SingleOrDefault(frameSelector.PanelId));

            panelViewModel.FrameType = frameSelector.FrameType;

            return RedirectToAction("Create", panelViewModel.FrameType.ToString(), new
            {
                canvasId = panelViewModel.CanvasId,
                panelId = panelViewModel.PanelId
            });
        }

        public IActionResult PanelsByCanvas(int canvasId = 0, int panelId = 0)
        {
            var selectListPanels = _panelService.All(canvasId).ToSelectListItems
                (text => text.Name,
                value => value.PanelId.ToString(),
                isSelected => isSelected.PanelId == panelId,
                defaultTextOption: "Todos",
                groupedBy: groupedBy => groupedBy.Canvas.Name);

            return PartialView(selectListPanels);
        }
    }
}
