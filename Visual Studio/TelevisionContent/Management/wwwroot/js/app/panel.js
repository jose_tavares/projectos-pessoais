﻿var square = 500;
var scale = 1.0;
var url, url_data;

function resetCanvas() {
    $('#canvas')
        .css('background-color', '')
        .css('width', square + 'px')
        .css('height', square + 'px');

    $('#canvas img:first')
        .hide()
        .attr("src", "");

    $('#canvas div.panel, #canvas div.panel-high').remove();
}

$(document).ready(function () {
    $('#canvas').width(square).height(square);

    url = $('#canvas img:first').attr("src").replace(/qqq/g, square);
    url_data = $('#canvas').attr('data-path') + '/';

    $('#CanvasId').change(function () {
        resetCanvas();

        if (this.value > 0)
            $.ajax({
                url: url_data + this.value,
                type: "POST",
                datatype: "json",
                success: function (json) {
                    scale = (json.height > json.width) ? (square / json.height) : (square / json.width);
                    $('#canvas')
                        .css('background-color', json.backgroundColor)
                        .css('width', json.width * scale + 'px')
                        .css('height', json.height * scale + 'px');

                    if (json.backgroundImage) {
                        $('#canvas img:first')
                            .attr("src", url.replace("nnn", json.backgroundImage))
                            .css('max-width', json.width * scale + 'px')
                            .css('max-height', json.height * scale + 'px')
                            .show();
                    }

                    $.each(json.panels, function (i, p) {
                        $('#canvas')
                            .append($('<div>')
                                .text(p.name)
                                .addClass(p.panelId == $('#PanelId').val() ? 'panel-high' : 'panel')
                                .css('top', p.top * scale + 'px')
                                .css('left', p.left * scale + 'px')
                                .css('width', p.width * scale + 'px')
                                .css('height', p.height * scale + 'px')
                            );
                    });

                    if (!$('#PanelId').val()) {
                        $('#canvas').append(
                            $('<div>').addClass('panel-high')
                        );
                    }
                }
            });
    });

    $('#Top, #Left, #Width, #Height').change(function () {
        $('#canvas div.panel-high')
            .css('top', $('#Top').val() * scale + 'px')
            .css('left', $('#Left').val() * scale + 'px')
            .css('width', $('#Width').val() * scale + 'px')
            .css('height', $('#Height').val() * scale + 'px');
    });

    $('#Name').change(function () {
        $('#canvas div.panel-high')
            .text($('#Name').val());
    });

    // init #canvas
    $('#CanvasId').change();
    $('#Top').change();
    $('#Left').change();
    $('#Width').change();
    $('#Height').change();
});
