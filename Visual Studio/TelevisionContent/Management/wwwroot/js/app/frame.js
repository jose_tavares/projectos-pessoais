﻿$(document).ready(function () {
    fillPanels();

    $('select#canvasId').change(function () {
        fillPanels();
    });

    function fillPanels() {
        var canvasId = $('select#canvasId').val() || 0;
        var panelId = $('select#panelId').val() || 0;

        $.ajax({
            url: url = $(".gutter-b").data("panel-url"),
            data: {
                canvasId: canvasId,
                panelId: panelId
            }
        }).done(function (data) {
            $("div#panelSelect").empty().append(data);
        });
    }
});