﻿var autoRefreshAtElement = $('#AutoRefreshAt');
var chkAutoRefreshAtElement = $('#chk_AutoRefreshAt');

$(document).ready(function () {
    chkAutoRefreshAtElement.change(function () {
        if ($(this)[0].checked === true) {
            autoRefreshAtElement.prop('disabled', '');
        }
        else {
            autoRefreshAtElement.prop('disabled', 'disabled');
            autoRefreshAtElement.val('');
        }
    });

    $('input.timepicker').timepicker({
        'timeFormat': 'G:i',
        'show2400': false,
        'showMeridian': false
    });
});

$(window).on('load', function () {
    if (!isUndefinedNullOrWhiteSpace(chkAutoRefreshAtElement.data('autorefreshat'))) {
        chkAutoRefreshAtElement[0].checked = true;
    }

    if (chkAutoRefreshAtElement[0].checked === true) {
        autoRefreshAtElement.prop('disabled', '');
    }
    else {
        autoRefreshAtElement.prop('disabled', 'disabled');
        autoRefreshAtElement.val('');
    }
});