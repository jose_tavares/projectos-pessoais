﻿$(document).ready(function () {
    $('input[type="submit"]').click(function () {
        if ($('#Files').val() === "") {
            alert("Selecione o(s) ficheiros(s) para enviar primeiro.");
            return false;
        }
    });

    $('#Files').change(function () {
        if ($(this).val() != "" && typeof FileReader !== "undefined") {
            var file = document.getElementById('Files').files[0];
            var extension = /[^.]+$/.exec(file.name)[0].toUpperCase();

            if (($('#SupportedImages').val().indexOf(extension) < 0) && ($('#SupportedVideos').val().indexOf(extension) < 0)) {
                alert("Formato de ficheiro " + extension + " não é suportado");
                $(this).val("")
            }
        }
    });
});