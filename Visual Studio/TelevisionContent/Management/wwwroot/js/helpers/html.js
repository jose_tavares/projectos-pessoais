﻿function formatDoubleValues(numberValueElement, doubleValueElement, formElement) {
    numberValueElement.val(doubleValueElement.val().replace(',', '.'));

    formElement.submit(function () {
        doubleValueElement.val($(numberValueElement).val().replace('.', ','));
    });
}