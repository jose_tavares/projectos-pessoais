﻿function isUndefinedNullOrWhiteSpace(str) {
    return typeof str === 'undefined' || !str || !str.trim();
}