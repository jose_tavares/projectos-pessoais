﻿using Acme.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Acme.Biz.Tests
{
    [TestClass()]
    public class VendorTests
    {
        [TestMethod()]
        public void SendWelcomeEmail_ValidCompany_Success()
        {
            // Arrange
            Vendor vendor = new Vendor
            {
                CompanyName = "ABC Corp"
            };

            string expected = "Message sent: Hello ABC Corp";

            // Act
            string actual = vendor.SendWelcomeEmail("Test Message");

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void SendWelcomeEmail_EmptyCompany_Success()
        {
            // Arrange
            Vendor vendor = new Vendor
            {
                CompanyName = ""
            };

            string expected = "Message sent: Hello";

            // Act
            string actual = vendor.SendWelcomeEmail("Test Message");

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void SendWelcomeEmail_NullCompany_Success()
        {
            // Arrange
            Vendor vendor = new Vendor
            {
                CompanyName = null
            };

            string expected = "Message sent: Hello";

            // Act
            string actual = vendor.SendWelcomeEmail("Test Message");

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void PlaceOrderTest()
        {
            // Arrange
            Vendor vendor = new Vendor();
            Product product = new Product(1, "Saw", "");
            OperationResult expected = new OperationResult(true,
                "Order from Acme, Inc\r\nProduct: Saw\r\nQuantity: 12" +
                                     "\r\nInstructions: standard delivery");

            // Act
            OperationResult actual = vendor.PlaceOrder(product, 12);

            // Assert
            Assert.AreEqual(expected.Success, actual.Success);
            Assert.AreEqual(expected.Message, actual.Message);
        }
        [TestMethod()]
        public void PlaceOrder_3Parameters()
        {
            // Arrange
            Vendor vendor = new Vendor();
            Product product = new Product(1, "Saw", "");
            OperationResult expected = new OperationResult(true,
                "Order from Acme, Inc\r\nProduct: Saw\r\nQuantity: 12" +
                "\r\nDeliver By: " + new DateTime(DateTime.Now.Year + 1, 10, 25).ToString("d") +
                "\r\nInstructions: standard delivery");

            // Act
            OperationResult actual = vendor.PlaceOrder(product, 12,
                new DateTimeOffset(DateTime.Now.Year + 1, 10, 25, 0, 0, 0, new TimeSpan(-7, 0, 0)));

            // Assert
            Assert.AreEqual(expected.Success, actual.Success);
            Assert.AreEqual(expected.Message, actual.Message);
        }

        [TestMethod()]
        [ExpectedException(typeof(ArgumentNullException))]
        public void PlaceOrder_NullProduct_Exception()
        {
            // Arrange
            Vendor vendor = new Vendor();

            // Act
            OperationResult actual = vendor.PlaceOrder(null, 12);

            // Assert
            // Expected exception
        }

        [TestMethod()]
        public void PlaceOrder_NoDeliveryDate()
        {
            // Arrange
            Vendor vendor = new Vendor();
            Product product = new Product(1, "Saw", "");
            OperationResult expected = new OperationResult(true,
                        "Order from Acme, Inc\r\nProduct: Saw\r\nQuantity: 12" +
                        "\r\nInstructions: Deliver to Suite 42");

            // Act
            OperationResult actual = vendor.PlaceOrder(product, 12,
                                instructions: "Deliver to Suite 42");

            // Assert
            Assert.AreEqual(expected.Success, actual.Success);
            Assert.AreEqual(expected.Message, actual.Message);
        }

        [TestMethod()]
        public void ToStringTest()
        {
            // Arrange
            Vendor vendor = new Vendor
            {
                VendorId = 1,
                CompanyName = "ABC Corp"
            };

            string expected = "Vendor: ABC Corp (1)";

            // Act
            string actual = vendor.ToString();

            // Assert
            Assert.AreEqual(expected, actual);
        }
    }
}