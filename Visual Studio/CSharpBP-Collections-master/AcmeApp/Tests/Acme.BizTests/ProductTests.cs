﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Acme.Biz.Tests
{
    [TestClass()]
    public class ProductTests
    {

        [TestMethod()]
        public void CalculateSuggestedPriceTest()
        {
            // Arrange
            Product currentProduct = new Product(1, "Saw", "")
            {
                Cost = 50m
            };

            decimal expected = 55m;

            // Act
            decimal actual = currentProduct.CalculateSuggestedPrice(10m);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Product_Null()
        {
            //Arrange
            Product currentProduct = null;
            string companyName = currentProduct?.ProductVendor?.CompanyName;

            string expected = null;

            //Act
            string actual = companyName;

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void ProductName_Format()
        {
            //Arrange
            Product currentProduct = new Product();
            currentProduct.ProductName = "  Steel Hammer  ";

            string expected = "Steel Hammer";

            //Act
            string actual = currentProduct.ProductName;

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void ProductName_TooShort()
        {
            //Arrange
            Product currentProduct = new Product();
            currentProduct.ProductName = "aw";

            string expected = null;
            string expectedMessage = "Product Name must be at least 3 characters";

            //Act
            string actual = currentProduct.ProductName;
            string actualMessage = currentProduct.ValidationMessage;

            //Assert
            Assert.AreEqual(expected, actual);
            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod()]
        public void ProductName_TooLong()
        {
            //Arrange
            Product currentProduct = new Product
            {
                ProductName = "Steel Bladed Hand Saw"
            };

            string expected = null;
            string expectedMessage = "Product Name cannot be more than 20 characters";

            //Act
            string actual = currentProduct.ProductName;
            string actualMessage = currentProduct.ValidationMessage;

            //Assert
            Assert.AreEqual(expected, actual);
            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod()]
        public void ProductName_JustRight()
        {
            //Arrange
            var currentProduct = new Product
            {
                ProductName = "Saw"
            };

            string expected = "Saw";
            string expectedMessage = null;

            //Act
            string actual = currentProduct.ProductName;
            string actualMessage = currentProduct.ValidationMessage;

            //Assert
            Assert.AreEqual(expected, actual);
            Assert.AreEqual(expectedMessage, actualMessage);
        }
    }
}