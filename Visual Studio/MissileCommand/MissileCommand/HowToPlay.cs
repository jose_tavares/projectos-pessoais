using System;
using System.Drawing;
using System.Windows.Forms;

namespace MissileCommand
{
	public class HowToPlay : Form
	{
		#region Variables
		private Label lblPara1;
		private Label lblPara2;
		private Label lblPara3;
		private Label lblPara4;
		private Label lblPara5;
		private Label lblPara6;
		private Label lblPara7;
		private Label lblPara8;

		private Button btnOK;
		#endregion

		#region Constructor
		public HowToPlay()
		{
			// required for Windows Form Designer support
			InitializeComponent();
		}
		#endregion

		#region Events
		private void btnOK_Click(object sender, EventArgs e)
		{
			this.DialogResult = DialogResult.OK;
		}
		#endregion

		#region Windows Form Designer Generated Code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(HowToPlay));
			this.btnOK = new System.Windows.Forms.Button();
			this.lblPara1 = new System.Windows.Forms.Label();
			this.lblPara2 = new System.Windows.Forms.Label();
			this.lblPara3 = new System.Windows.Forms.Label();
			this.lblPara4 = new System.Windows.Forms.Label();
			this.lblPara5 = new System.Windows.Forms.Label();
			this.lblPara6 = new System.Windows.Forms.Label();
			this.lblPara7 = new System.Windows.Forms.Label();
			this.lblPara8 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// btnOK
			// 
			this.btnOK.CausesValidation = false;
			this.btnOK.Location = new System.Drawing.Point(8, 432);
			this.btnOK.Name = "btnOK";
			this.btnOK.Size = new System.Drawing.Size(72, 24);
			this.btnOK.TabIndex = 1;
			this.btnOK.Text = "OK";
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// lblPara1
			// 
			this.lblPara1.CausesValidation = false;
			this.lblPara1.Location = new System.Drawing.Point(8, 8);
			this.lblPara1.Name = "lblPara1";
			this.lblPara1.Size = new System.Drawing.Size(480, 24);
			this.lblPara1.TabIndex = 5;
			this.lblPara1.Text = "It\'s the future. World War III erupts as hundreds of nuclear weapons are launched" +
				". Life sucks.";
			// 
			// lblPara2
			// 
			this.lblPara2.CausesValidation = false;
			this.lblPara2.Location = new System.Drawing.Point(8, 32);
			this.lblPara2.Name = "lblPara2";
			this.lblPara2.Size = new System.Drawing.Size(480, 48);
			this.lblPara2.TabIndex = 6;
			this.lblPara2.Text = @"Or maybe not... You work at Missile Command .NET, a top secret military defense base. The project name is FART (First Anti-Atomic Response Team). Your mission is to protect your country's cities from incoming nukes by using intercepting missiles, called shots.";
			// 
			// lblPara3
			// 
			this.lblPara3.CausesValidation = false;
			this.lblPara3.Location = new System.Drawing.Point(8, 80);
			this.lblPara3.Name = "lblPara3";
			this.lblPara3.Size = new System.Drawing.Size(480, 40);
			this.lblPara3.TabIndex = 7;
			this.lblPara3.Text = "The mouse controls the targeting and firing of these shots. Simply move the mouse" +
				" targeting cross to where you want to aim, and then click a mouse button to fire" +
				".";
			// 
			// lblPara4
			// 
			this.lblPara4.CausesValidation = false;
			this.lblPara4.Location = new System.Drawing.Point(8, 120);
			this.lblPara4.Name = "lblPara4";
			this.lblPara4.Size = new System.Drawing.Size(480, 64);
			this.lblPara4.TabIndex = 8;
			this.lblPara4.Text = @"Once fired, a shot will fly to the target point and detonate upon reaching it. Hopefully, the resulting blast will trigger the detonation of incoming nukes. When nukes explode, they also may trigger other nukes, like a chain reaction. Note that nukes are tracked by colored lines with a white tip denoting the actual warhead position.";
			// 
			// lblPara5
			// 
			this.lblPara5.CausesValidation = false;
			this.lblPara5.Location = new System.Drawing.Point(8, 184);
			this.lblPara5.Name = "lblPara5";
			this.lblPara5.Size = new System.Drawing.Size(480, 64);
			this.lblPara5.TabIndex = 9;
			this.lblPara5.Text = @"Blasts that overlap cities will cause damage. As a city takes damage, its display color will change. A city changes color from gray to yellow at 1/3 damage, and from yellow to red at 2/3 damage. At full damage, the city is utterly destroyed and falls to radioactive ashes. Your base, represented by a small white square at ground level, can also be destroyed by blasts.";
			// 
			// lblPara6
			// 
			this.lblPara6.CausesValidation = false;
			this.lblPara6.Location = new System.Drawing.Point(8, 248);
			this.lblPara6.Name = "lblPara6";
			this.lblPara6.Size = new System.Drawing.Size(480, 64);
			this.lblPara6.TabIndex = 10;
			this.lblPara6.Text = @"You have a limited supply of shots, so use them strategically. If you run out of shots, or your base is destroyed, the targeting cross will disappear along with the base and you'll be left helpless against the remaining nukes. Be careful too, because shots can damage cities and even destroy your base if fired carelessly.";
			// 
			// lblPara7
			// 
			this.lblPara7.CausesValidation = false;
			this.lblPara7.Location = new System.Drawing.Point(8, 312);
			this.lblPara7.Name = "lblPara7";
			this.lblPara7.Size = new System.Drawing.Size(480, 40);
			this.lblPara7.TabIndex = 11;
			this.lblPara7.Text = "The game ends when all incoming nukes have been launched and detonated. Your fina" +
				"l score is equal to the percentage of undamaged city area multiplied by 10 times" +
				" the difficulty factor.";
			// 
			// lblPara8
			// 
			this.lblPara8.CausesValidation = false;
			this.lblPara8.Location = new System.Drawing.Point(8, 352);
			this.lblPara8.Name = "lblPara8";
			this.lblPara8.Size = new System.Drawing.Size(480, 72);
			this.lblPara8.TabIndex = 12;
			this.lblPara8.Text = @"Most options affect the difficulty factor that ranges from 0 (cake walk) to 10 (suicide). The more difficult the options, the more you'll be rewarded for undamaged city area and the higher the potential score. Increasing stats for nukes or cities gives a higher factor. Increasing stats for shots or screen size gives a lower factor. Blast stats have no effect because they apply to both nukes and shots.";
			// 
			// HowToPlay
			// 
			this.AutoScale = false;
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.CausesValidation = false;
			this.ClientSize = new System.Drawing.Size(497, 465);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.lblPara8,
																		  this.lblPara7,
																		  this.lblPara6,
																		  this.lblPara5,
																		  this.lblPara4,
																		  this.lblPara3,
																		  this.lblPara2,
																		  this.lblPara1,
																		  this.btnOK});
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "HowToPlay";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "How to Play";
			this.ResumeLayout(false);

		}
		#endregion
	}
}