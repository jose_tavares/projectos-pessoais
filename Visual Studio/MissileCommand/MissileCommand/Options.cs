using System;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace MissileCommand
{
	public class Options : Form
	{
		#region Variables
		private Label lblWidth;
		private Label lblHeight;
		private Label lblCities;
		private Label lblBlastRadius;
		private Label lblBlastDuration;
		private Label lblTotalShots;
		private Label lblMaxActiveShots;
		private Label lblShotSpeed;
		private Label lblTotalNukes;
		private Label lblMaxActiveNukes;
		private Label lblNukeAvgLaunchRate;
		private Label lblNukeSpeedInitial;
		private Label lblNukeSpeedFinal;
		private Label lblScreenHeader;
		private Label lblBlastHeader;
		private Label lblShotHeader;
		private Label lblNukeHeader;
		private Label lblDifficultyFactor;

		private ComboBox cboWidth;
		private ComboBox cboHeight;
		private ComboBox cboCities;
		private ComboBox cboBoomRadiusPixels;
		private ComboBox cboBoomTotalSeconds;
		private ComboBox cboGameShotsTotal;
		private ComboBox cboShots;
		private ComboBox cboShotPixelsPerSecond;
		private ComboBox cboGameNukesTotal;
		private ComboBox cboNukes;
		private ComboBox cboNukesPerSecond;
		private ComboBox cboNukePixelsPerSecondInitial;
		private ComboBox cboNukePixelsPerSecondFinal;

		private CheckBox chkRemember;

		private Button btnOK;
		private Button btnDefault;
		private Button btnCancel;

		private Settings s;

		private bool ComputeDifficulty;
		#endregion

		#region Constants
		private const string _MYFILE    = "Options.txt";
		private const string _DIFFLABEL = "Difficulty = ";
		private const char   _DELIMITER = '~';
		#endregion

		#region Constructor
		public Options(float TICKS_PER_SECOND)
		{
			// required for Windows Form Designer support
			InitializeComponent();

			s.TICKS_PER_SECOND = TICKS_PER_SECOND;
			LoadFromFile();
		}
		#endregion

		#region Private Methods
		private void Default()
		{
			ComboBox c;
			ComputeDifficulty = false;

			for(int i = 0; i < this.Controls.Count; i++)
			{
				if(this.Controls[i].Name.StartsWith("cbo"))
				{
					c = ((ComboBox)(this.Controls[i]));
					c.SelectedIndex = c.Items.Count / 2;
				}
			}

			ComputeDifficulty = true;
			lblDifficultyFactor.Text = _DIFFLABEL + "1.000";
		}

		private void LoadFromFile()
		{
			if(File.Exists(_MYFILE))
			{
				try
				{
					StreamReader sr = File.OpenText(_MYFILE);
					string hold = sr.ReadLine();
					sr.Close();

					string[] data = hold.Split(_DELIMITER);
					int j = 0;
					ComputeDifficulty = false;

					for(int i = 0; i < this.Controls.Count; i++)
					{
						if(this.Controls[i].Name.StartsWith("cbo"))
						{
							((ComboBox)(this.Controls[i])).SelectedIndex = int.Parse(data[j++]);
						}
					}

					ComputeDifficulty = true;
					Calculate();
				}
				catch
				{
					MessageBox.Show("Unable to read options from file.");
					Default();
				}
			}
			else
			{
				Default();
			}
		}

		private void SaveToFile()
		{
			try
			{
				StringBuilder sb = new StringBuilder();
				for(int i = 0; i < this.Controls.Count; i++)
				{
					if(this.Controls[i].Name.StartsWith("cbo"))
					{
						sb.Append(((ComboBox)(this.Controls[i])).SelectedIndex.ToString());
						sb.Append(_DELIMITER);
					}
				}

				StreamWriter sw = File.CreateText(_MYFILE);
				sw.WriteLine(sb.ToString());
				sw.Close();
			}
			catch
			{
				MessageBox.Show("Unable to write options to file.");
			}
		}

		private void Calculate()
		{
			int i = cboWidth.SelectedIndex / 2 + 1;
			if(cboCities.SelectedIndex > i)
			{
				ComputeDifficulty = false;
				cboCities.SelectedIndex = i;
				ComputeDifficulty = true;

				MessageBox.Show
				(
					"Too many cities are selected for the given width. " +
					"Cities has been reset to the limit of " + cboCities.Items[i].ToString() + "."
				);
			}

			ComboBox c;
			string name;
			float di;
			float df = 1;

			for(i = 0; i < this.Controls.Count; i++)
			{
				name = this.Controls[i].Name;
				if(name.StartsWith("cbo") && name.IndexOf("Boom") == -1)
				{
					c = (ComboBox)this.Controls[i];
					di = float.Parse(c.SelectedItem.ToString()) / float.Parse(c.Items[c.Items.Count / 2].ToString());
					if(name.IndexOf("Nuke") >= 0 || name.EndsWith("Cities"))
					{
						df *= di;
					}
					else
					{
						df /= di;
					}
				}
			}

			if(df > 10)df = 10;
			lblDifficultyFactor.Text = _DIFFLABEL + Math.Round(df, 3).ToString("F3");
		}
		#endregion

		#region Properties
		public Settings FormData
		{
			get
			{
				s.WIDTH                        = int.Parse(cboWidth.SelectedItem.ToString());
				s.HEIGHT                       = int.Parse(cboHeight.SelectedItem.ToString());
				s.SHOTS                        = int.Parse(cboShots.SelectedItem.ToString());
				s.NUKES                        = int.Parse(cboNukes.SelectedItem.ToString());
				s.CITIES                       = int.Parse(cboCities.SelectedItem.ToString());
				s.SHOT_PIXELS_PER_TICK         = float.Parse(cboShotPixelsPerSecond.SelectedItem.ToString()) / s.TICKS_PER_SECOND;
				s.NUKE_PIXELS_PER_TICK_INITIAL = float.Parse(cboNukePixelsPerSecondInitial.SelectedItem.ToString()) / s.TICKS_PER_SECOND;
				s.NUKE_PIXELS_PER_TICK_STEP    = (float.Parse(cboNukePixelsPerSecondFinal.SelectedItem.ToString()) - float.Parse(cboNukePixelsPerSecondInitial.SelectedItem.ToString()))/ float.Parse(cboGameNukesTotal.SelectedItem.ToString()) / s.TICKS_PER_SECOND;
				s.NUKE_ODDS_PER_TICK           = (int)Math.Round(s.TICKS_PER_SECOND / float.Parse(cboNukesPerSecond.SelectedItem.ToString()), 0);
				s.BOOM_RADIUS_PIXELS           = int.Parse(cboBoomRadiusPixels.SelectedItem.ToString());
				s.BOOM_TOTAL_TICKS             = (int)Math.Round(float.Parse(cboBoomTotalSeconds.SelectedItem.ToString()) * s.TICKS_PER_SECOND, 0);
				s.GAME_SHOTS_TOTAL             = int.Parse(cboGameShotsTotal.SelectedItem.ToString());
				s.GAME_NUKES_TOTAL             = int.Parse(cboGameNukesTotal.SelectedItem.ToString());
				s.DIFFICULTY_FACTOR            = float.Parse(lblDifficultyFactor.Text.Replace(_DIFFLABEL, ""));

				return(s);
			}
			set
			{
				s = value;
				ComputeDifficulty = false;

				cboWidth                      .SelectedItem = s.WIDTH.ToString();
				cboHeight                     .SelectedItem = s.HEIGHT.ToString();
				cboShots                      .SelectedItem = s.SHOTS.ToString();
				cboNukes                      .SelectedItem = s.NUKES.ToString();
				cboCities                     .SelectedItem = s.CITIES.ToString();
				cboShotPixelsPerSecond        .SelectedItem = Math.Round(s.SHOT_PIXELS_PER_TICK * s.TICKS_PER_SECOND, 0).ToString();
				cboNukePixelsPerSecondInitial .SelectedItem = Math.Round(s.NUKE_PIXELS_PER_TICK_INITIAL * s.TICKS_PER_SECOND, 0).ToString();
				cboNukePixelsPerSecondFinal   .SelectedItem = Math.Round((s.NUKE_PIXELS_PER_TICK_INITIAL + s.NUKE_PIXELS_PER_TICK_STEP * s.GAME_NUKES_TOTAL) * s.TICKS_PER_SECOND, 0).ToString();
				cboNukesPerSecond             .SelectedItem = Math.Round(s.TICKS_PER_SECOND / s.NUKE_ODDS_PER_TICK, 1).ToString("F1");
				cboBoomRadiusPixels           .SelectedItem = s.BOOM_RADIUS_PIXELS.ToString();
				cboBoomTotalSeconds           .SelectedItem = Math.Round(s.BOOM_TOTAL_TICKS / s.TICKS_PER_SECOND, 1).ToString("F1");
				cboGameShotsTotal             .SelectedItem = s.GAME_SHOTS_TOTAL.ToString();
				cboGameNukesTotal             .SelectedItem = s.GAME_NUKES_TOTAL.ToString();
				lblDifficultyFactor           .Text         = _DIFFLABEL + s.DIFFICULTY_FACTOR.ToString("F3");

				ComputeDifficulty = true;
			}
		}
		#endregion

		#region Events
		private void cbo_SelectedIndexChanged(object sender, EventArgs e)
		{
			if(ComputeDifficulty)Calculate();
		}

		private void btnOK_Click(object sender, EventArgs e)
		{
			if(chkRemember.Checked)SaveToFile();
			this.DialogResult = DialogResult.OK;
		}

		private void btnDefault_Click(object sender, EventArgs e)
		{
			Default();
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			this.DialogResult = DialogResult.Cancel;
		}
		#endregion

		#region Windows Form Designer Generated Code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Options));
            this.cboWidth = new System.Windows.Forms.ComboBox();
            this.cboHeight = new System.Windows.Forms.ComboBox();
            this.lblWidth = new System.Windows.Forms.Label();
            this.lblHeight = new System.Windows.Forms.Label();
            this.lblMaxActiveShots = new System.Windows.Forms.Label();
            this.cboShots = new System.Windows.Forms.ComboBox();
            this.lblMaxActiveNukes = new System.Windows.Forms.Label();
            this.cboNukes = new System.Windows.Forms.ComboBox();
            this.lblCities = new System.Windows.Forms.Label();
            this.cboCities = new System.Windows.Forms.ComboBox();
            this.lblShotSpeed = new System.Windows.Forms.Label();
            this.lblNukeSpeedInitial = new System.Windows.Forms.Label();
            this.lblNukeSpeedFinal = new System.Windows.Forms.Label();
            this.lblNukeAvgLaunchRate = new System.Windows.Forms.Label();
            this.lblBlastRadius = new System.Windows.Forms.Label();
            this.lblBlastDuration = new System.Windows.Forms.Label();
            this.lblTotalShots = new System.Windows.Forms.Label();
            this.lblTotalNukes = new System.Windows.Forms.Label();
            this.cboGameNukesTotal = new System.Windows.Forms.ComboBox();
            this.cboShotPixelsPerSecond = new System.Windows.Forms.ComboBox();
            this.cboGameShotsTotal = new System.Windows.Forms.ComboBox();
            this.cboBoomRadiusPixels = new System.Windows.Forms.ComboBox();
            this.cboNukesPerSecond = new System.Windows.Forms.ComboBox();
            this.cboBoomTotalSeconds = new System.Windows.Forms.ComboBox();
            this.cboNukePixelsPerSecondInitial = new System.Windows.Forms.ComboBox();
            this.cboNukePixelsPerSecondFinal = new System.Windows.Forms.ComboBox();
            this.lblNukeHeader = new System.Windows.Forms.Label();
            this.lblShotHeader = new System.Windows.Forms.Label();
            this.lblBlastHeader = new System.Windows.Forms.Label();
            this.lblScreenHeader = new System.Windows.Forms.Label();
            this.btnOK = new System.Windows.Forms.Button();
            this.lblDifficultyFactor = new System.Windows.Forms.Label();
            this.btnDefault = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.chkRemember = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // cboWidth
            // 
            this.cboWidth.CausesValidation = false;
            this.cboWidth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboWidth.Items.AddRange(new object[] {
            "200",
            "250",
            "300",
            "350",
            "400",
            "450",
            "500",
            "550",
            "600"});
            this.cboWidth.Location = new System.Drawing.Point(16, 48);
            this.cboWidth.MaxDropDownItems = 9;
            this.cboWidth.Name = "cboWidth";
            this.cboWidth.Size = new System.Drawing.Size(48, 21);
            this.cboWidth.TabIndex = 3;
            this.cboWidth.SelectedIndexChanged += new System.EventHandler(this.cbo_SelectedIndexChanged);
            // 
            // cboHeight
            // 
            this.cboHeight.CausesValidation = false;
            this.cboHeight.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboHeight.Items.AddRange(new object[] {
            "200",
            "250",
            "300",
            "350",
            "400",
            "450",
            "500",
            "550",
            "600"});
            this.cboHeight.Location = new System.Drawing.Point(80, 48);
            this.cboHeight.MaxDropDownItems = 9;
            this.cboHeight.Name = "cboHeight";
            this.cboHeight.Size = new System.Drawing.Size(48, 21);
            this.cboHeight.TabIndex = 4;
            this.cboHeight.SelectedIndexChanged += new System.EventHandler(this.cbo_SelectedIndexChanged);
            // 
            // lblWidth
            // 
            this.lblWidth.CausesValidation = false;
            this.lblWidth.Location = new System.Drawing.Point(16, 32);
            this.lblWidth.Name = "lblWidth";
            this.lblWidth.Size = new System.Drawing.Size(40, 16);
            this.lblWidth.TabIndex = 16;
            this.lblWidth.Text = "Width:";
            // 
            // lblHeight
            // 
            this.lblHeight.CausesValidation = false;
            this.lblHeight.Location = new System.Drawing.Point(80, 32);
            this.lblHeight.Name = "lblHeight";
            this.lblHeight.Size = new System.Drawing.Size(48, 16);
            this.lblHeight.TabIndex = 17;
            this.lblHeight.Text = "Height:";
            // 
            // lblMaxActiveShots
            // 
            this.lblMaxActiveShots.CausesValidation = false;
            this.lblMaxActiveShots.Location = new System.Drawing.Point(88, 112);
            this.lblMaxActiveShots.Name = "lblMaxActiveShots";
            this.lblMaxActiveShots.Size = new System.Drawing.Size(104, 16);
            this.lblMaxActiveShots.TabIndex = 22;
            this.lblMaxActiveShots.Text = "Max Active Shots:";
            // 
            // cboShots
            // 
            this.cboShots.CausesValidation = false;
            this.cboShots.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboShots.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9"});
            this.cboShots.Location = new System.Drawing.Point(88, 128);
            this.cboShots.MaxDropDownItems = 9;
            this.cboShots.Name = "cboShots";
            this.cboShots.Size = new System.Drawing.Size(48, 21);
            this.cboShots.TabIndex = 9;
            this.cboShots.SelectedIndexChanged += new System.EventHandler(this.cbo_SelectedIndexChanged);
            // 
            // lblMaxActiveNukes
            // 
            this.lblMaxActiveNukes.CausesValidation = false;
            this.lblMaxActiveNukes.Location = new System.Drawing.Point(88, 192);
            this.lblMaxActiveNukes.Name = "lblMaxActiveNukes";
            this.lblMaxActiveNukes.Size = new System.Drawing.Size(104, 16);
            this.lblMaxActiveNukes.TabIndex = 25;
            this.lblMaxActiveNukes.Text = "Max Active Nukes:";
            // 
            // cboNukes
            // 
            this.cboNukes.CausesValidation = false;
            this.cboNukes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboNukes.Items.AddRange(new object[] {
            "2",
            "4",
            "6",
            "8",
            "10",
            "12",
            "14",
            "16",
            "18"});
            this.cboNukes.Location = new System.Drawing.Point(88, 208);
            this.cboNukes.MaxDropDownItems = 9;
            this.cboNukes.Name = "cboNukes";
            this.cboNukes.Size = new System.Drawing.Size(48, 21);
            this.cboNukes.TabIndex = 12;
            this.cboNukes.SelectedIndexChanged += new System.EventHandler(this.cbo_SelectedIndexChanged);
            // 
            // lblCities
            // 
            this.lblCities.CausesValidation = false;
            this.lblCities.Location = new System.Drawing.Point(144, 32);
            this.lblCities.Name = "lblCities";
            this.lblCities.Size = new System.Drawing.Size(40, 16);
            this.lblCities.TabIndex = 18;
            this.lblCities.Text = "Cities:";
            // 
            // cboCities
            // 
            this.cboCities.CausesValidation = false;
            this.cboCities.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCities.Items.AddRange(new object[] {
            "2",
            "4",
            "6",
            "8",
            "10",
            "12"});
            this.cboCities.Location = new System.Drawing.Point(144, 48);
            this.cboCities.MaxDropDownItems = 9;
            this.cboCities.Name = "cboCities";
            this.cboCities.Size = new System.Drawing.Size(48, 21);
            this.cboCities.TabIndex = 5;
            this.cboCities.SelectedIndexChanged += new System.EventHandler(this.cbo_SelectedIndexChanged);
            // 
            // lblShotSpeed
            // 
            this.lblShotSpeed.CausesValidation = false;
            this.lblShotSpeed.Location = new System.Drawing.Point(192, 112);
            this.lblShotSpeed.Name = "lblShotSpeed";
            this.lblShotSpeed.Size = new System.Drawing.Size(104, 16);
            this.lblShotSpeed.TabIndex = 23;
            this.lblShotSpeed.Text = "Shot Speed (px/s):";
            // 
            // lblNukeSpeedInitial
            // 
            this.lblNukeSpeedInitial.CausesValidation = false;
            this.lblNukeSpeedInitial.Location = new System.Drawing.Point(16, 240);
            this.lblNukeSpeedInitial.Name = "lblNukeSpeedInitial";
            this.lblNukeSpeedInitial.Size = new System.Drawing.Size(136, 16);
            this.lblNukeSpeedInitial.TabIndex = 27;
            this.lblNukeSpeedInitial.Text = "Nuke Speed Initial (px/s):";
            // 
            // lblNukeSpeedFinal
            // 
            this.lblNukeSpeedFinal.CausesValidation = false;
            this.lblNukeSpeedFinal.Location = new System.Drawing.Point(152, 240);
            this.lblNukeSpeedFinal.Name = "lblNukeSpeedFinal";
            this.lblNukeSpeedFinal.Size = new System.Drawing.Size(144, 16);
            this.lblNukeSpeedFinal.TabIndex = 28;
            this.lblNukeSpeedFinal.Text = "Nuke Speed Final (px/ms):";
            // 
            // lblNukeAvgLaunchRate
            // 
            this.lblNukeAvgLaunchRate.CausesValidation = false;
            this.lblNukeAvgLaunchRate.Location = new System.Drawing.Point(192, 192);
            this.lblNukeAvgLaunchRate.Name = "lblNukeAvgLaunchRate";
            this.lblNukeAvgLaunchRate.Size = new System.Drawing.Size(152, 16);
            this.lblNukeAvgLaunchRate.TabIndex = 26;
            this.lblNukeAvgLaunchRate.Text = "Nuke Avg Launch Rate (1/s):";
            // 
            // lblBlastRadius
            // 
            this.lblBlastRadius.CausesValidation = false;
            this.lblBlastRadius.Location = new System.Drawing.Point(224, 32);
            this.lblBlastRadius.Name = "lblBlastRadius";
            this.lblBlastRadius.Size = new System.Drawing.Size(96, 16);
            this.lblBlastRadius.TabIndex = 19;
            this.lblBlastRadius.Text = "Blast Radius (px):";
            // 
            // lblBlastDuration
            // 
            this.lblBlastDuration.CausesValidation = false;
            this.lblBlastDuration.Location = new System.Drawing.Point(320, 32);
            this.lblBlastDuration.Name = "lblBlastDuration";
            this.lblBlastDuration.Size = new System.Drawing.Size(96, 16);
            this.lblBlastDuration.TabIndex = 20;
            this.lblBlastDuration.Text = "Blast Duration (s):";
            // 
            // lblTotalShots
            // 
            this.lblTotalShots.CausesValidation = false;
            this.lblTotalShots.Location = new System.Drawing.Point(16, 112);
            this.lblTotalShots.Name = "lblTotalShots";
            this.lblTotalShots.Size = new System.Drawing.Size(72, 16);
            this.lblTotalShots.TabIndex = 21;
            this.lblTotalShots.Text = "Total Shots:";
            // 
            // lblTotalNukes
            // 
            this.lblTotalNukes.CausesValidation = false;
            this.lblTotalNukes.Location = new System.Drawing.Point(16, 192);
            this.lblTotalNukes.Name = "lblTotalNukes";
            this.lblTotalNukes.Size = new System.Drawing.Size(72, 16);
            this.lblTotalNukes.TabIndex = 24;
            this.lblTotalNukes.Text = "Total Nukes:";
            // 
            // cboGameNukesTotal
            // 
            this.cboGameNukesTotal.CausesValidation = false;
            this.cboGameNukesTotal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboGameNukesTotal.Items.AddRange(new object[] {
            "20",
            "30",
            "40",
            "50",
            "60",
            "70",
            "80",
            "90",
            "100"});
            this.cboGameNukesTotal.Location = new System.Drawing.Point(16, 208);
            this.cboGameNukesTotal.MaxDropDownItems = 9;
            this.cboGameNukesTotal.Name = "cboGameNukesTotal";
            this.cboGameNukesTotal.Size = new System.Drawing.Size(48, 21);
            this.cboGameNukesTotal.TabIndex = 11;
            this.cboGameNukesTotal.SelectedIndexChanged += new System.EventHandler(this.cbo_SelectedIndexChanged);
            // 
            // cboShotPixelsPerSecond
            // 
            this.cboShotPixelsPerSecond.CausesValidation = false;
            this.cboShotPixelsPerSecond.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboShotPixelsPerSecond.Items.AddRange(new object[] {
            "200",
            "300",
            "400",
            "500",
            "600",
            "700",
            "800",
            "900",
            "1000"});
            this.cboShotPixelsPerSecond.Location = new System.Drawing.Point(192, 128);
            this.cboShotPixelsPerSecond.MaxDropDownItems = 9;
            this.cboShotPixelsPerSecond.Name = "cboShotPixelsPerSecond";
            this.cboShotPixelsPerSecond.Size = new System.Drawing.Size(48, 21);
            this.cboShotPixelsPerSecond.TabIndex = 10;
            this.cboShotPixelsPerSecond.SelectedIndexChanged += new System.EventHandler(this.cbo_SelectedIndexChanged);
            // 
            // cboGameShotsTotal
            // 
            this.cboGameShotsTotal.CausesValidation = false;
            this.cboGameShotsTotal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboGameShotsTotal.Items.AddRange(new object[] {
            "20",
            "30",
            "40",
            "50",
            "60",
            "70",
            "80",
            "90",
            "100"});
            this.cboGameShotsTotal.Location = new System.Drawing.Point(16, 128);
            this.cboGameShotsTotal.MaxDropDownItems = 9;
            this.cboGameShotsTotal.Name = "cboGameShotsTotal";
            this.cboGameShotsTotal.Size = new System.Drawing.Size(48, 21);
            this.cboGameShotsTotal.TabIndex = 8;
            this.cboGameShotsTotal.SelectedIndexChanged += new System.EventHandler(this.cbo_SelectedIndexChanged);
            // 
            // cboBoomRadiusPixels
            // 
            this.cboBoomRadiusPixels.CausesValidation = false;
            this.cboBoomRadiusPixels.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboBoomRadiusPixels.Items.AddRange(new object[] {
            "10",
            "15",
            "20",
            "25",
            "30",
            "35",
            "40",
            "45",
            "50"});
            this.cboBoomRadiusPixels.Location = new System.Drawing.Point(224, 48);
            this.cboBoomRadiusPixels.MaxDropDownItems = 9;
            this.cboBoomRadiusPixels.Name = "cboBoomRadiusPixels";
            this.cboBoomRadiusPixels.Size = new System.Drawing.Size(48, 21);
            this.cboBoomRadiusPixels.TabIndex = 6;
            this.cboBoomRadiusPixels.SelectedIndexChanged += new System.EventHandler(this.cbo_SelectedIndexChanged);
            // 
            // cboNukesPerSecond
            // 
            this.cboNukesPerSecond.CausesValidation = false;
            this.cboNukesPerSecond.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboNukesPerSecond.Items.AddRange(new object[] {
            "0.6",
            "0.8",
            "1.0",
            "1.3",
            "1.5",
            "1.7",
            "2.0",
            "2.2",
            "2.4"});
            this.cboNukesPerSecond.Location = new System.Drawing.Point(192, 208);
            this.cboNukesPerSecond.MaxDropDownItems = 9;
            this.cboNukesPerSecond.Name = "cboNukesPerSecond";
            this.cboNukesPerSecond.Size = new System.Drawing.Size(48, 21);
            this.cboNukesPerSecond.TabIndex = 13;
            this.cboNukesPerSecond.SelectedIndexChanged += new System.EventHandler(this.cbo_SelectedIndexChanged);
            // 
            // cboBoomTotalSeconds
            // 
            this.cboBoomTotalSeconds.CausesValidation = false;
            this.cboBoomTotalSeconds.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboBoomTotalSeconds.Items.AddRange(new object[] {
            "0.2",
            "0.4",
            "0.6",
            "0.8",
            "1.0",
            "1.3",
            "1.5",
            "1.7",
            "2.0"});
            this.cboBoomTotalSeconds.Location = new System.Drawing.Point(320, 48);
            this.cboBoomTotalSeconds.MaxDropDownItems = 9;
            this.cboBoomTotalSeconds.Name = "cboBoomTotalSeconds";
            this.cboBoomTotalSeconds.Size = new System.Drawing.Size(48, 21);
            this.cboBoomTotalSeconds.TabIndex = 7;
            this.cboBoomTotalSeconds.SelectedIndexChanged += new System.EventHandler(this.cbo_SelectedIndexChanged);
            // 
            // cboNukePixelsPerSecondInitial
            // 
            this.cboNukePixelsPerSecondInitial.CausesValidation = false;
            this.cboNukePixelsPerSecondInitial.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboNukePixelsPerSecondInitial.Items.AddRange(new object[] {
            "20",
            "40",
            "60",
            "80",
            "100",
            "125",
            "150",
            "175",
            "200"});
            this.cboNukePixelsPerSecondInitial.Location = new System.Drawing.Point(16, 256);
            this.cboNukePixelsPerSecondInitial.MaxDropDownItems = 9;
            this.cboNukePixelsPerSecondInitial.Name = "cboNukePixelsPerSecondInitial";
            this.cboNukePixelsPerSecondInitial.Size = new System.Drawing.Size(48, 21);
            this.cboNukePixelsPerSecondInitial.TabIndex = 14;
            this.cboNukePixelsPerSecondInitial.SelectedIndexChanged += new System.EventHandler(this.cbo_SelectedIndexChanged);
            // 
            // cboNukePixelsPerSecondFinal
            // 
            this.cboNukePixelsPerSecondFinal.CausesValidation = false;
            this.cboNukePixelsPerSecondFinal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboNukePixelsPerSecondFinal.Items.AddRange(new object[] {
            "60",
            "120",
            "180",
            "240",
            "300",
            "375",
            "450",
            "525",
            "600"});
            this.cboNukePixelsPerSecondFinal.Location = new System.Drawing.Point(152, 256);
            this.cboNukePixelsPerSecondFinal.MaxDropDownItems = 9;
            this.cboNukePixelsPerSecondFinal.Name = "cboNukePixelsPerSecondFinal";
            this.cboNukePixelsPerSecondFinal.Size = new System.Drawing.Size(48, 21);
            this.cboNukePixelsPerSecondFinal.TabIndex = 15;
            this.cboNukePixelsPerSecondFinal.SelectedIndexChanged += new System.EventHandler(this.cbo_SelectedIndexChanged);
            // 
            // lblNukeHeader
            // 
            this.lblNukeHeader.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblNukeHeader.CausesValidation = false;
            this.lblNukeHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNukeHeader.Location = new System.Drawing.Point(8, 168);
            this.lblNukeHeader.Name = "lblNukeHeader";
            this.lblNukeHeader.Size = new System.Drawing.Size(416, 120);
            this.lblNukeHeader.TabIndex = 32;
            this.lblNukeHeader.Text = "Armas Nucleares";
            // 
            // lblShotHeader
            // 
            this.lblShotHeader.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShotHeader.CausesValidation = false;
            this.lblShotHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblShotHeader.Location = new System.Drawing.Point(8, 88);
            this.lblShotHeader.Name = "lblShotHeader";
            this.lblShotHeader.Size = new System.Drawing.Size(416, 72);
            this.lblShotHeader.TabIndex = 31;
            this.lblShotHeader.Text = "Tiros";
            // 
            // lblBlastHeader
            // 
            this.lblBlastHeader.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBlastHeader.CausesValidation = false;
            this.lblBlastHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBlastHeader.Location = new System.Drawing.Point(216, 8);
            this.lblBlastHeader.Name = "lblBlastHeader";
            this.lblBlastHeader.Size = new System.Drawing.Size(208, 72);
            this.lblBlastHeader.TabIndex = 30;
            this.lblBlastHeader.Text = "Explos�es";
            // 
            // lblScreenHeader
            // 
            this.lblScreenHeader.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblScreenHeader.CausesValidation = false;
            this.lblScreenHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblScreenHeader.Location = new System.Drawing.Point(8, 8);
            this.lblScreenHeader.Name = "lblScreenHeader";
            this.lblScreenHeader.Size = new System.Drawing.Size(200, 72);
            this.lblScreenHeader.TabIndex = 29;
            this.lblScreenHeader.Text = "Tela";
            // 
            // btnOK
            // 
            this.btnOK.CausesValidation = false;
            this.btnOK.Location = new System.Drawing.Point(8, 328);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(72, 24);
            this.btnOK.TabIndex = 0;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // lblDifficultyFactor
            // 
            this.lblDifficultyFactor.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDifficultyFactor.CausesValidation = false;
            this.lblDifficultyFactor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDifficultyFactor.Location = new System.Drawing.Point(8, 296);
            this.lblDifficultyFactor.Name = "lblDifficultyFactor";
            this.lblDifficultyFactor.Size = new System.Drawing.Size(152, 24);
            this.lblDifficultyFactor.TabIndex = 33;
            // 
            // btnDefault
            // 
            this.btnDefault.CausesValidation = false;
            this.btnDefault.Location = new System.Drawing.Point(88, 328);
            this.btnDefault.Name = "btnDefault";
            this.btnDefault.Size = new System.Drawing.Size(72, 24);
            this.btnDefault.TabIndex = 1;
            this.btnDefault.Text = "Default";
            this.btnDefault.Click += new System.EventHandler(this.btnDefault_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.CausesValidation = false;
            this.btnCancel.Location = new System.Drawing.Point(168, 328);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(72, 24);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Cancela";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // chkRemember
            // 
            this.chkRemember.CausesValidation = false;
            this.chkRemember.Location = new System.Drawing.Point(168, 296);
            this.chkRemember.Name = "chkRemember";
            this.chkRemember.Size = new System.Drawing.Size(256, 24);
            this.chkRemember.TabIndex = 16;
            this.chkRemember.Text = "Use these options at startup";
            // 
            // Options
            // 
            this.CausesValidation = false;
            this.ClientSize = new System.Drawing.Size(433, 361);
            this.Controls.Add(this.chkRemember);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnDefault);
            this.Controls.Add(this.lblDifficultyFactor);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.cboNukePixelsPerSecondFinal);
            this.Controls.Add(this.cboNukePixelsPerSecondInitial);
            this.Controls.Add(this.cboBoomTotalSeconds);
            this.Controls.Add(this.cboNukesPerSecond);
            this.Controls.Add(this.cboBoomRadiusPixels);
            this.Controls.Add(this.cboGameShotsTotal);
            this.Controls.Add(this.cboShotPixelsPerSecond);
            this.Controls.Add(this.cboGameNukesTotal);
            this.Controls.Add(this.lblTotalNukes);
            this.Controls.Add(this.lblTotalShots);
            this.Controls.Add(this.lblBlastDuration);
            this.Controls.Add(this.lblBlastRadius);
            this.Controls.Add(this.lblNukeAvgLaunchRate);
            this.Controls.Add(this.lblNukeSpeedFinal);
            this.Controls.Add(this.lblNukeSpeedInitial);
            this.Controls.Add(this.lblShotSpeed);
            this.Controls.Add(this.cboCities);
            this.Controls.Add(this.lblCities);
            this.Controls.Add(this.cboNukes);
            this.Controls.Add(this.lblMaxActiveNukes);
            this.Controls.Add(this.cboShots);
            this.Controls.Add(this.lblMaxActiveShots);
            this.Controls.Add(this.lblHeight);
            this.Controls.Add(this.lblWidth);
            this.Controls.Add(this.cboHeight);
            this.Controls.Add(this.cboWidth);
            this.Controls.Add(this.lblNukeHeader);
            this.Controls.Add(this.lblShotHeader);
            this.Controls.Add(this.lblBlastHeader);
            this.Controls.Add(this.lblScreenHeader);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Options";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Options";
            this.ResumeLayout(false);

		}
		#endregion
	}
}