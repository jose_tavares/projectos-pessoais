using System;
using System.Drawing;
using System.Resources;
using System.Windows.Forms;

namespace MissileCommand
{
	public class About : Form
	{
		#region Variables
		private Label lblTitle;
		private Label lblVersion;
		private Label lblAuthor;
		private Label lblBlurp;

		private Button btnOK;

		private PictureBox Logo;
		#endregion

		#region Constructor
		public About()
		{
			// required for Windows Form Designer support
			InitializeComponent();

			Logo.Image =
			(
				new Icon
				(
					(Icon)((new ResourceManager(typeof(Game))).GetObject("$this.Icon")), 32, 32
				)
			).ToBitmap();
		}
		#endregion

		#region Events
		private void btnOK_Click(object sender, EventArgs e)
		{
			this.DialogResult = DialogResult.OK;
		}
		#endregion

		#region Windows Form Designer Generated Code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnOK = new System.Windows.Forms.Button();
			this.Logo = new System.Windows.Forms.PictureBox();
			this.lblTitle = new System.Windows.Forms.Label();
			this.lblVersion = new System.Windows.Forms.Label();
			this.lblAuthor = new System.Windows.Forms.Label();
			this.lblBlurp = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// btnOK
			// 
			this.btnOK.CausesValidation = false;
			this.btnOK.Location = new System.Drawing.Point(8, 152);
			this.btnOK.Name = "btnOK";
			this.btnOK.Size = new System.Drawing.Size(72, 24);
			this.btnOK.TabIndex = 0;
			this.btnOK.Text = "OK";
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// Logo
			// 
			this.Logo.Location = new System.Drawing.Point(8, 8);
			this.Logo.Name = "Logo";
			this.Logo.Size = new System.Drawing.Size(32, 32);
			this.Logo.TabIndex = 3;
			this.Logo.TabStop = false;
			// 
			// lblTitle
			// 
			this.lblTitle.CausesValidation = false;
			this.lblTitle.Location = new System.Drawing.Point(48, 8);
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Size = new System.Drawing.Size(232, 16);
			this.lblTitle.TabIndex = 1;
			this.lblTitle.Text = "Missile Command .NET";
			// 
			// lblVersion
			// 
			this.lblVersion.CausesValidation = false;
			this.lblVersion.Location = new System.Drawing.Point(48, 24);
			this.lblVersion.Name = "lblVersion";
			this.lblVersion.Size = new System.Drawing.Size(232, 16);
			this.lblVersion.TabIndex = 2;
			this.lblVersion.Text = "Version 1.0";
			// 
			// lblAuthor
			// 
			this.lblAuthor.CausesValidation = false;
			this.lblAuthor.Location = new System.Drawing.Point(48, 40);
			this.lblAuthor.Name = "lblAuthor";
			this.lblAuthor.Size = new System.Drawing.Size(232, 16);
			this.lblAuthor.TabIndex = 3;
			this.lblAuthor.Text = "� David Lambert 2003";
			// 
			// lblBlurp
			// 
			this.lblBlurp.CausesValidation = false;
			this.lblBlurp.Location = new System.Drawing.Point(8, 64);
			this.lblBlurp.Name = "lblBlurp";
			this.lblBlurp.Size = new System.Drawing.Size(272, 80);
			this.lblBlurp.TabIndex = 4;
			this.lblBlurp.Text = @"Welcome to my first .NET game, written entirely in C# using GDI+ for graphics. It's a tribute to the classic Missile Command game by Atari. Although done mostly to increase my coding skills, I'm very happy with the results. I hope that you enjoy it. You may contact me at dplambert@excite.com if you wish.";
			// 
			// About
			// 
			this.AutoScale = false;
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.CausesValidation = false;
			this.ClientSize = new System.Drawing.Size(289, 185);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.lblBlurp,
																		  this.lblAuthor,
																		  this.lblVersion,
																		  this.lblTitle,
																		  this.Logo,
																		  this.btnOK});
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "About";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "About Missile Command .NET";
			this.ResumeLayout(false);

		}
		#endregion
	}
}