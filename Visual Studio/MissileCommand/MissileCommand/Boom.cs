using System;
using System.Drawing;

namespace MissileCommand
{
	public class Boom
	{
		#region Instance Variables
		private float x;
		private float y;

		private float r;
		private float d;
		private float s;
		private int t;

		private bool active;
		#endregion

		#region Static Variables
		private static float rm;
		private static float dr;

		private static Brush[] b;

		private static RectangleF rec;
		#endregion

		#region Initialization Methods
		public Boom() {}

		public static int[] Init(int RADIUS_PIXELS, int TOTAL_TICKS)
		{
			rm = RADIUS_PIXELS;
			dr = rm * 2 / (float)TOTAL_TICKS;

			// colors must not be used anywhere else
			b = new Brush[]
			{
				Brushes.WhiteSmoke,
				Brushes.Gold,
				Brushes.Orange,
				Brushes.Crimson
			};

			rec = new RectangleF(0, 0, 0, 0);

			int[] argb = new int[b.Length];
			for(int i = 0; i < argb.Length; i++)
			{
				argb[i] = (new Pen(b[i])).Color.ToArgb();
			}
			return(argb);
		}

		public void Reset(int x, int y)
		{
			this.x = x;
			this.y = y;

			r = 0;
			d = 0;
			s = 1;
			t = 0;

			active = true;
		}
		#endregion

		#region Other Methods
		public bool Draw(Graphics g)
		{
			if(active)
			{
				if(r > 0)g.FillEllipse(b[t % b.Length], x - r, y - r, d, d);

				r += s * dr;
				d = r * 2;
				if(r >= rm)s = -1;
				t++;

				active = (r > 0);
				return(!active);
			}
			else
			{
				return(false);
			}
		}
		#endregion

		#region Properties
		public bool Active
		{
			get
			{
				return(active);
			}
		}

		public RectangleF Bounds
		{
			get
			{
				rec.X = x - r;
				rec.Y = y - r;
				rec.Width = rec.Height = d;
				return(rec);
			}
		}
		#endregion
	}
}