using System;
using System.Drawing;

namespace MissileCommand
{
	public class City
	{
		#region Instance Variables
		private float x;
		private int hp;
		private RectangleF[] r;

		private bool active;
		#endregion

		#region Static Variables
		private static float y;
		private static float w;
		private static float h;

		private static int hpi;
		private static int hpRed;
		private static int hpYellow;

		private static RectangleF rec;
		#endregion

		#region Initialization Methods
		public City()
		{
			r = new RectangleF[4];
			for(int i = 0; i < r.Length; i++)r[i] = new RectangleF(0, 0, 0, 0);
		}

		public static void Init(int Y_GROUND_ZERO)
		{
			y = Y_GROUND_ZERO;
			w = 24;
			h = w / 2;

			hpi      = (int)(w * h * 10 * 4 / Math.PI);
			hpRed    = hpi / 3;
			hpYellow = hpRed * 2;
			// PI = area of circle with of radius 1
			// 4  = area of square holding that circle
			// 4 / PI gives additional HP to compensate for fact that
			// booms are circles and not squares during collision detection

			rec = new RectangleF(0, 0, w, h);
		}

		public void Reset(int x)
		{
			this.x = x;
			hp = hpi;

			r[0].X = this.x - w / 2;
			r[1].X = this.x - w / 4;
			r[2].X = this.x;
			r[3].X = this.x + w / 4;

			if(r[0].Width == 0)
			{
				r[0].Y = y - h / 4;
				r[1].Y = y - h;
				r[2].Y = y - h / 2;
				r[3].Y = y - h * 3 / 4;

				r[0].Width = r[1].Width = r[2].Width = r[3].Width = w / 4;

				r[0].Height = h / 4;
				r[1].Height = h;
				r[2].Height = h / 2;
				r[3].Height = h * 3 / 4;
			}

			active = true;
		}
		#endregion

		#region Other Methods
		public void Draw(Graphics g)
		{
			if(hp > hpYellow)
			{
				g.FillRectangle(Brushes.Silver  , r[0]);
				g.FillRectangle(Brushes.DimGray , r[1]);
				g.FillRectangle(Brushes.DarkGray, r[2]);
				g.FillRectangle(Brushes.Gray    , r[3]);
			}
			else if(hp > hpRed)
			{
				g.FillRectangle(Brushes.LemonChiffon , r[0]);
				g.FillRectangle(Brushes.DarkGoldenrod, r[1]);
				g.FillRectangle(Brushes.Khaki        , r[2]);
				g.FillRectangle(Brushes.DarkOrange   , r[3]);
			}
			else
			{
				g.FillRectangle(Brushes.Pink      , r[0]);
				g.FillRectangle(Brushes.Firebrick , r[1]);
				g.FillRectangle(Brushes.LightCoral, r[2]);
				g.FillRectangle(Brushes.OrangeRed , r[3]);
			}
		}

		public void Collision(int damage)
		{
			hp -= damage;
			if(hp <= 0)
			{
				hp = 0;

				r[0].Y = r[1].Y = r[2].Y = r[3].Y = y - 2;
				r[0].Height = r[1].Height = r[2].Height = r[3].Height = 2;

				active = false;
			}
		}
		#endregion

		#region Properties
		public bool Active
		{
			get
			{
				return(active);
			}
		}

		public RectangleF Bounds
		{
			get
			{
				rec.X = r[0].X;
				rec.Y = r[1].Y;
				return(rec);
			}
		}

		public int HP
		{
			get
			{
				return(hp);
			}
		}

		public static int HPMax
		{
			get
			{
				return(hpi);
			}
		}
		#endregion
	}
}