using System.Reflection;

[assembly: AssemblyTitle("Missile Command .NET")]
[assembly: AssemblyDescription("A tribute to the classic Missile Command game by Atari, written in C# / .NET / GDI+ by David Lambert")]
[assembly: AssemblyProduct("Missile Command .NET")]
[assembly: AssemblyCopyright("David Lambert")]
[assembly: AssemblyVersion("1.0.0.0")]