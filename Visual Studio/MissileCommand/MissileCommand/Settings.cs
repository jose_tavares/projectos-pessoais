namespace MissileCommand
{
	public struct Settings
	{
		#region Variables
		// game settings
		// * denotes calculated
		public int WIDTH;
		public int HEIGHT;

		public int SHOTS;
		public int NUKES;
		public int BOOMS; // *
		public int CITIES;

		public float SHOT_PIXELS_PER_TICK;
		public float NUKE_PIXELS_PER_TICK_INITIAL;
		public float NUKE_PIXELS_PER_TICK_STEP;

		public int NUKE_ODDS_PER_TICK;
		public int BOOM_RADIUS_PIXELS;
		public int BOOM_TOTAL_TICKS;

		public int GAME_SHOTS_TOTAL;
		public int GAME_NUKES_TOTAL;

		public float TICKS_PER_SECOND; // *
		public float DIFFICULTY_FACTOR; // *
		#endregion
	}
}