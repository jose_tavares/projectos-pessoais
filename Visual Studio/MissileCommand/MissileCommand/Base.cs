using System;
using System.Drawing;

namespace MissileCommand
{
	public class Base
	{
		#region Variables
		private Point pt;
		private bool active;
		#endregion

		#region Constructor
		public Base(int x, int y)
		{
			pt = new Point(x, y);
			active = true;
		}
		#endregion

		#region Other Methods
		public void Draw(Graphics g)
		{
			if(active)g.DrawRectangle(Pens.White, pt.X - 2, pt.Y - 2, 4, 2);
		}

		public void OutOfShots()
		{
			active = false;
		}
		#endregion

		#region Properties
		public bool Active
		{
			get
			{
				return(active);
			}
		}

		public Point Location
		{
			get
			{
				return(pt);
			}
		}
		#endregion
	}
}