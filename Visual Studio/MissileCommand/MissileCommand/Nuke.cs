using System;
using System.Drawing;

namespace MissileCommand
{
	public class Nuke
	{
		#region Instance Variables
		private float x;
		private float y;

		private float xi;
		private float xf;

		private float dx;
		private float dy;

		private int pp;

		private bool active;
		#endregion

		#region Static Variables
		private static float yi;
		private static float yf;
		private static float dz;
		private static float ddz;

		private static int fired;

		private static Pen[] p;

		private static Point pt;
		#endregion

		#region Initialization Methods
		public Nuke() {}

		public static void Init(int Y_INITIAL, int Y_FINAL, float PIXELS_PER_TICK_INITIAL, float PIXELS_PER_TICK_STEP)
		{
			yi = Y_INITIAL;
			yf = Y_FINAL;
			dz = PIXELS_PER_TICK_INITIAL;
			ddz = PIXELS_PER_TICK_STEP;

			fired = 0;

			p = new Pen[]
			{
				new Pen(Color.Red, 2),
				new Pen(Color.Lime, 2),
				new Pen(Color.Blue, 2),
				new Pen(Color.Fuchsia, 2),
				new Pen(Color.Yellow, 2),
				new Pen(Color.Aqua, 2)
			};

			pt = new Point(0, 0);
		}

		public void Reset(int xi, int xf)
		{
			x = xi;
			y = yi;

			this.xi = xi;
			this.xf = xf;

			double r = Math.Atan(Math.Abs((double)(yf - yi) / (double)(xf - xi)));
			dx = (xf >= xi ? 1 : -1) * dz * (float)Math.Cos(r);
			dy = dz * (float)Math.Sin(r);
			dz += ddz;

			pp = fired % p.Length;

			fired++;

			active = true;
		}
		#endregion

		#region Other Methods
		public bool Draw(Graphics g)
		{
			if(active)
			{
				if(y > 3)g.DrawLine(p[pp], xi, yi, x, y);
				g.FillRectangle(Brushes.White, x - 1, y - 1, 2, 2);

				x += dx;
				y += dy;

				active = (y < yf);
				return(!active);
			}
			else
			{
				return(false);
			}
		}

		public void Collision()
		{
			active = false;
		}
		#endregion

		#region Properties
		public bool Active
		{
			get
			{
				return(active);
			}
		}

		public Point Location
		{
			get
			{
				pt.X = (int)x;
				pt.Y = (int)y;
				return(pt);
			}
		}

		public Point Destination
		{
			get
			{
				pt.X = (int)xf;
				pt.Y = (int)yf;
				return(pt);
			}
		}

		public static int Fired
		{
			get
			{
				return(fired);
			}
		}
		#endregion
	}
}