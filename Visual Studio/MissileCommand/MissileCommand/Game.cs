using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace MissileCommand
{
	public class Game : Form
	{
		#region Variables
		private About     frmAbout;
		private HowToPlay frmHowToPlay;
		private Options   frmOptions;

		private MainMenu oMainMenu;

		private PictureBox Screen;
		private Timer      Ticker;
		private IContainer components;

		private Bitmap   img;
		private Graphics g;

		private Random die;
		private int[]  BoomArgb;
		private bool   GameOver;

		private Settings s;

		private Shot[] oShot;
		private Boom[] oBoom;
		private Nuke[] oNuke;
		private City[] oCity;
		private Base   oBase;
		#endregion

		#region Constants
		private const string _MYFILE = "HighScore.txt";
		#endregion

		#region Constructor
		public Game()
		{
			// required for Windows Form Designer support
			InitializeComponent();

			// forms
			frmAbout     = new About();
			frmHowToPlay = new HowToPlay();
			frmOptions   = new Options(1000 / (float)Ticker.Interval);

			// default settings
			s = frmOptions.FormData;

			// build menus
			oMainMenu = new MainMenu();

			oMainMenu.MenuItems.AddRange
			(
				new MenuItem[]
				{
					new MenuItem("&Jogar"),
					new MenuItem("&Pausar", new EventHandler(this.Pause_Clicked)),
					new MenuItem("&Ajuda")
				}
			);

			oMainMenu.MenuItems[0].MenuItems.AddRange
			(
				new MenuItem[]
				{
					new MenuItem("&Novo ", new EventHandler(this.GameNew_Clicked)),
					new MenuItem("-"),
					new MenuItem("&Op��es...", new EventHandler(this.GameOptions_Clicked)),
					new MenuItem("-"),
					new MenuItem("Sa&ir ", new EventHandler(this.GameExit_Clicked))
				}
			);

			oMainMenu.MenuItems[2].MenuItems.AddRange
			(
				new MenuItem[]
				{
					new MenuItem("Como &Jogar...", new EventHandler(this.HelpHowToPlay_Clicked)),
					new MenuItem("-"),
					new MenuItem("&Comando de Misseis .NET...", new EventHandler(this.HelpAbout_Clicked))
				}
			);

			this.Menu = oMainMenu;

			// finally...
			Reset();
		}
		#endregion

		#region Private Methods
		private void Reset()
		{
			// start a new game

			// resize
			this.ClientSize = new Size(s.WIDTH, s.HEIGHT + 3);
			Screen.Size = new Size(s.WIDTH, s.HEIGHT);

			// build objects
			img = new Bitmap(s.WIDTH, s.HEIGHT);
			g = Graphics.FromImage(img);

			die = new Random();

			int i;

			oShot = new Shot[s.SHOTS];
			for(i = 0; i < s.SHOTS; i++)oShot[i] = new Shot();

			oNuke = new Nuke[s.NUKES];
			for(i = 0; i < s.NUKES; i++)oNuke[i] = new Nuke();

			s.BOOMS = s.SHOTS + s.NUKES;
			oBoom = new Boom[s.BOOMS];
			for(i = 0; i < s.BOOMS; i++)oBoom[i] = new Boom();

			oCity = new City[s.CITIES];
			for(i = 0; i < s.CITIES; i++)oCity[i] = new City();

			oBase = new Base(s.WIDTH / 2 - 2, s.HEIGHT - 1);

			// static Init() calls
			Shot.Init(s.WIDTH / 2, s.HEIGHT, s.SHOT_PIXELS_PER_TICK);
			Nuke.Init(0, s.HEIGHT, s.NUKE_PIXELS_PER_TICK_INITIAL, s.NUKE_PIXELS_PER_TICK_STEP);
			BoomArgb = Boom.Init(s.BOOM_RADIUS_PIXELS, s.BOOM_TOTAL_TICKS);
			City.Init(s.HEIGHT);

			// city placement
			int d = s.WIDTH / (s.CITIES + 1);
			for(i = 0; i < s.CITIES; i++)
			{
				oCity[i].Reset(d * (i + 1));
			}

			// finally...
			GameOver = false;
			oMainMenu.MenuItems[1].Text = "&Pausar";
			Screen.Cursor = Cursors.Cross;
			Ticker.Enabled = true;
		}

		private void Pause()
		{
			if(!GameOver)
			{
				oMainMenu.MenuItems[1].Text = "&Continuar";
				Screen.Cursor = Cursors.Default;
				Ticker.Enabled = false;
			}
		}

		private void Resume()
		{
			if(!GameOver)
			{
				oMainMenu.MenuItems[1].Text = "&Pausar";
				if(oBase.Active)Screen.Cursor = Cursors.Cross;
				Ticker.Enabled = true;
			}
		}

		private void GameEnd()
		{
			// game over!
			GameOver = true;
			Screen.Cursor = Cursors.Default;
			Ticker.Enabled = false;

			// compute score
			double pct = 0;
			for(int i = 0; i < s.CITIES; i++)pct += oCity[i].HP;
			pct = Math.Round(100 * pct / (s.CITIES * City.HPMax), 2);
			int score = (int)Math.Round(10 * s.DIFFICULTY_FACTOR * pct, 0);

			// high score?
			int high = LoadFromFile();
			if(score > high)SaveToFile(score);

			// draw game summary
			g.DrawString("FIM De JOGO", this.Font, Brushes.White, 10, 10);
			g.DrawString("N�o destru�dos = " + pct.ToString("F2") + "%", this.Font, Brushes.Silver, 10, 30);
			g.DrawString("Dificuldade = " + s.DIFFICULTY_FACTOR.ToString("F3"), this.Font, Brushes.Silver, 10, 50);
			g.DrawString("Seu Placar = " + score, this.Font, Brushes.Silver, 10, 70);
			g.DrawString("Maior Placar = " + high, this.Font, Brushes.Silver, 10, 90);
			if(score > high)g.DrawString("Voc� conseguir um placar bem legal!", this.Font, Brushes.White, 10, 110);
		}

		private int LoadFromFile()
		{
			int score;

			if(File.Exists(_MYFILE))
			{
				try
				{
					StreamReader sr = File.OpenText(_MYFILE);
					score = int.Parse(sr.ReadLine());
					sr.Close();
				}
				catch
				{
					MessageBox.Show("Unable to read high score from file.");
					score = 0;
				}
			}
			else
			{
				score = 0;
			}

			return(score);
		}

		private void SaveToFile(int score)
		{
			try
			{
				StreamWriter sw = File.CreateText(_MYFILE);
				sw.WriteLine(score.ToString());
				sw.Close();
			}
			catch
			{
				MessageBox.Show("Unable to write high score to file.");
			}
		}
		#endregion

		#region Tick Event
		private void Ticker_Tick(object sender, EventArgs e)
		{
			int i, j, argb;
			Point p;
			RectangleF r;

			// draw background
			g.FillRectangle(Brushes.Black, 0, 0, s.WIDTH, s.HEIGHT);

			// draw Shots
			for(i = 0; i < s.SHOTS; i++)
			{
				if(oShot[i].Draw(g))
				{
					// Shot i just reached destination, so trigger Boom j
					for(j = 0; j < s.BOOMS; j++)
					{
						if(!oBoom[j].Active)
						{
							p = oShot[i].Destination;
							oBoom[j].Reset(p.X, p.Y);
							break;
						}
					}
				}
			}

			// Nuke management
			if(Nuke.Fired == s.GAME_NUKES_TOTAL)
			{
				// end of game?
				for(i = 0; i < s.NUKES; i++)if(oNuke[i].Active)break;
				if(i == s.NUKES)
				{
					// no more active Nukes
					for(j = 0; j < s.BOOMS; j++)if(oBoom[j].Active)break;
					if(j == s.BOOMS)
					{
						// no more active Booms
						for(i = 0; i < s.SHOTS; i++)if(oShot[i].Active)break;
						if(i == s.SHOTS)
						{
							// no more active Shots
							GameEnd();
						}
					}
				}
			}
			else if(die.Next(1, s.NUKE_ODDS_PER_TICK) == 1)
			{
				// fire another nuke
				for(i = 0; i < s.NUKES; i++)
				{
					if(!oNuke[i].Active)
					{
						oNuke[i].Reset(die.Next(0, s.WIDTH), die.Next(0, s.WIDTH));
						break;
					}
				}
			}

			// draw Nukes
			for(i = 0; i < s.NUKES; i++)
			{
				if(oNuke[i].Draw(g))
				{
					// Nuke i just reached destination, so trigger Boom j
					for(j = 0; j < s.BOOMS; j++)
					{
						if(!oBoom[j].Active)
						{
							p = oNuke[i].Destination;
							oBoom[j].Reset(p.X, p.Y);
							break;
						}
					}
				}
			}

			// draw remaining objects
			oBase.Draw(g);
			for(i = 0; i < s.CITIES; i++)oCity[i].Draw(g);
			for(j = 0; j < s.BOOMS; j++)oBoom[j].Draw(g);

			// img completed, now check for collisions
			// changes are applied at the next tick
			for(i = 0; i < s.NUKES; i++)
			{
				if(oNuke[i].Active)
				{
					p = oNuke[i].Location;
					argb = img.GetPixel(p.X, p.Y).ToArgb();

					for(j = 0; j < BoomArgb.Length; j++)
					{
						if(argb == BoomArgb[j])
						{
							// collision!
							// Nuke i just reached a Boom, so trigger Boom j and make inactive
							// (j reused here)
							for(j = 0; j < s.BOOMS; j++)
							{
								if(!oBoom[j].Active)
								{
									oBoom[j].Reset(p.X, p.Y);
									oNuke[i].Collision();
									break;
								}
							}

							// move on to next Nuke
							break;
						}
					}
				}
			}

			if(oBase.Active)
			{
				p = oBase.Location;
				argb = img.GetPixel(p.X, p.Y).ToArgb();

				for(j = 0; j < BoomArgb.Length; j++)
				{
					if(argb == BoomArgb[j])
					{
						// collision!
						// Base is touching a Boom, so make inactive
						oBase.OutOfShots();
						Screen.Cursor = Cursors.Default;
						break;
					}
				}
			}

			for(i = 0; i < s.CITIES; i++)
			{
				for(j = 0; j < s.BOOMS; j++)
				{
					if(oCity[i].Bounds.IntersectsWith(oBoom[j].Bounds))
					{
						// collision!
						// City i is touching Boom j, so take damage
						r = oCity[i].Bounds;
						r.Intersect(oBoom[j].Bounds);
						oCity[i].Collision((int)(r.Width * r.Height));
					}
				}
			}

			// apply the graphics buffer
			Screen.Image = img;
		}
		#endregion

		#region Mouse / Focus Events
		private void Screen_MouseUp(object sender, MouseEventArgs e)
		{
			if(oBase.Active)
			{
				for(int i = 0; i < s.SHOTS; i++)
				{
					if(!oShot[i].Active)
					{
						oShot[i].Reset(e.X, e.Y);
						if(Shot.Fired == s.GAME_SHOTS_TOTAL)
						{
							oBase.OutOfShots();
							Screen.Cursor = Cursors.Default;
						}
						break;
					}
				}
			}
		}

		private void Game_Deactivate(object sender, EventArgs e)
		{
			if(Ticker.Enabled)Pause();
		}
		#endregion

		#region Menu Events
		private void GameNew_Clicked(object sender, EventArgs e)
		{
			Reset();
		}

		private void GameOptions_Clicked(object sender, EventArgs e)
		{
			frmOptions.ShowDialog(this);
			if(frmOptions.DialogResult == DialogResult.OK)
			{
				s = frmOptions.FormData;
				Reset();
			}
			else
			{
				frmOptions.FormData = s;
			}
		}

		private void GameExit_Clicked(object sender, EventArgs e)
		{
			this.Close();
		}

		private void Pause_Clicked(object sender, EventArgs e)
		{
			if(Ticker.Enabled)
			{
				Pause();
			}
			else
			{
				Resume();
			}
		}

		private void HelpHowToPlay_Clicked(object sender, EventArgs e)
		{
			frmHowToPlay.ShowDialog(this);
		}

		private void HelpAbout_Clicked(object sender, EventArgs e)
		{
			frmAbout.ShowDialog(this);
		}
		#endregion

		#region Main() / Dispose()
		[STAThread]
		public static void Main()
		{
			Application.Run(new Game());
		}

		protected override void Dispose(bool disposing)
		{
			if(disposing)
			{
				if(frmAbout     != null) frmAbout     .Dispose();
				if(frmHowToPlay != null) frmHowToPlay .Dispose();
				if(frmOptions   != null) frmOptions   .Dispose();

				if(oMainMenu    != null) oMainMenu    .Dispose();

				if(Screen       != null) Screen       .Dispose();
				if(Ticker       != null) Ticker       .Dispose();
				if(components   != null) components   .Dispose();

				if(img          != null) img          .Dispose();
				if(g            != null) g            .Dispose();
			}
			base.Dispose(disposing);
		}
		#endregion

		#region Windows Form Designer Generated Code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Game));
            this.Screen = new System.Windows.Forms.PictureBox();
            this.Ticker = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.Screen)).BeginInit();
            this.SuspendLayout();
            // 
            // Screen
            // 
            this.Screen.BackColor = System.Drawing.Color.Black;
            this.Screen.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.Screen.Location = new System.Drawing.Point(0, 0);
            this.Screen.Name = "Screen";
            this.Screen.Size = new System.Drawing.Size(566, 555);
            this.Screen.TabIndex = 0;
            this.Screen.TabStop = false;
            this.Screen.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Screen_MouseUp);
            // 
            // Ticker
            // 
            this.Ticker.Interval = 25;
            this.Ticker.Tick += new System.EventHandler(this.Ticker_Tick);
            // 
            // Game
            // 
            this.BackColor = System.Drawing.Color.Brown;
            this.CausesValidation = false;
            this.ClientSize = new System.Drawing.Size(564, 555);
            this.Controls.Add(this.Screen);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Game";
            this.Text = "Missile Command .NET";
            this.Deactivate += new System.EventHandler(this.Game_Deactivate);
            ((System.ComponentModel.ISupportInitialize)(this.Screen)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
	}
}