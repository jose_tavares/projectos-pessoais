using System;
using System.Drawing;

namespace MissileCommand
{
	public class Shot
	{
		#region Instance Variables
		private float x;
		private float y;

		private float xf;
		private float yf;

		private float dx;
		private float dy;

		private bool active;
		#endregion

		#region Static Variables
		private static float xi;
		private static float yi;
		private static float dz;

		private static int fired;

		private static Point pt;
		#endregion

		#region Initialization Methods
		public Shot() {}

		public static void Init(int X_INITIAL, int Y_INITIAL, float PIXELS_PER_TICK)
		{
			xi = X_INITIAL;
			yi = Y_INITIAL;
			dz = PIXELS_PER_TICK;

			fired = 0;

			pt = new Point(0, 0);
		}

		public void Reset(int xf, int yf)
		{
			x = xi;
			y = yi;

			this.xf = xf;
			this.yf = yf;

			double r = Math.Atan(Math.Abs((double)(this.yf - yi) / (double)(this.xf - xi)));
			dx = (this.xf >= xi ? 1 : -1) * dz * (float)Math.Cos(r);
			dy = -dz * (float)Math.Sin(r);

			fired++;

			active = true;
		}
		#endregion

		#region Other Methods
		public bool Draw(Graphics g)
		{
			if(active)
			{
				g.FillRectangle(Brushes.White, x - 1, y - 1, 2, 2);

				x += dx;
				y += dy;

				active = (y > yf);
				return(!active);
			}
			else
			{
				return(false);
			}
		}
		#endregion

		#region Properties
		public bool Active
		{
			get
			{
				return(active);
			}
		}

		public Point Destination
		{
			get
			{
				pt.X = (int)xf;
				pt.Y = (int)yf;
				return(pt);
			}
		}

		public static int Fired
		{
			get
			{
				return(fired);
			}
		}
		#endregion
	}
}