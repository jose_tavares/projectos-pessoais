﻿using System;
using AutoMapper;
using System.Linq;
using Shop.Domain.Entities;
using Shop.Domain.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Shop.Application.ViewModels;

namespace Shop.Web.Mvc.Controllers
{
    public class ProductsController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IProductRepository _productService;

        public ProductsController
            (IMapper mapper,
            IProductRepository productService)
        {
            _mapper = mapper;
            _productService = productService;
        }

        public IActionResult Index(bool error)
        {
            var productsViewModel = _productService.Get().Select
                (select => _mapper.Map<Product, ProductViewModel>(select));

            return View(Tuple.Create(productsViewModel, error));
        }

        public IActionResult Create() => PartialView();

        [HttpPost]
        public IActionResult Create(ProductViewModel productViewModel) =>
            RedirectToAction("Index", new
            {
                error = !_productService.Add
                    (_mapper.Map<ProductViewModel, Product>(productViewModel))
            });

        public IActionResult Delete(int id)
        {
            var productViewModel = _mapper.Map<Product, ProductViewModel>
                (_productService.Get(id));

            if (productViewModel == null)
            {
                return RedirectToAction("Index", new { error = true });
            }

            return PartialView(productViewModel);
        }

        [HttpPost]
        public IActionResult Delete(ProductViewModel productViewModel) =>
            RedirectToAction("Index", new { error = !_productService.Delete(productViewModel.Id) });

        public IActionResult Edit(int id)
        {
            var productViewModel = _mapper.Map<Product, ProductViewModel>
                (_productService.Get(id));

            if (productViewModel == null)
            {
                return RedirectToAction("Index", new { error = true });
            }

            return PartialView(productViewModel);
        }

        [HttpPost]
        public IActionResult Edit(ProductViewModel productViewModel) =>
            RedirectToAction("Index", new
            {
                error = !_productService.Edit
                    (_mapper.Map<ProductViewModel, Product>(productViewModel))
            });
    }
}