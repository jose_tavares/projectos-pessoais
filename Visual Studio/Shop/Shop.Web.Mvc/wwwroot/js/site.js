﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

$(function () {
    $(".create").click(function () {
        $("#modal").load("Products/Create", function () {
            $("#modal").modal();
        });
    });

    $(".edit").click(function () {
        var id = $(this).attr("data-id");

        $("#modal").load("Products/Edit?id=" + id, function () {
            $("#modal").modal();
        })
    });

    $(".delete").click(function () {
        var id = $(this).attr("data-id");

        $("#modal").load("Products/Delete?id=" + id, function () {
            $("#modal").modal();
        })
    });
});