﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shop.Application.Common
{
    public class ImageFilenameGenerator
    {
        public string Generate(int position, string extension, Guid guid = default)
        {
            if (guid == Guid.Empty)
                guid = Guid.NewGuid();

            return guid + "_" + position + extension;
        }

        public int ExtractPositionFromGeneratedFilename(string filename)
        {
            var positionStartIndex = filename.IndexOf("_");
            if (positionStartIndex < 0) return -1;

            var extensionStartIndex = filename.IndexOf(".");
            if (extensionStartIndex < 0) return -1;

            var positionString =
                filename.Substring(positionStartIndex + 1, extensionStartIndex - positionStartIndex - 1);

            var position = int.Parse(positionString);

            return position;
        }

        public string ExtractExtensionFromGeneratedFilename(string filename)
        {
            var extensionStartIndex = filename.IndexOf(".");
            return extensionStartIndex < 0 ? "" : filename.Substring(extensionStartIndex);
        }

        public string RemovePathFromFilename(string filename)
        {
            var pathEndIndex = filename.IndexOf("/");
            return pathEndIndex < 0 ? filename : filename.Substring(pathEndIndex + 1);
        }
    }
}
