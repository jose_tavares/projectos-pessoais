﻿using System.Collections.Generic;

namespace Shop.Application.Interfaces
{
    public interface IGenericService<T> where T : class
    {
        IEnumerable<T> Get();

        T Get(int id);

        bool Add(T newItem);

        bool Delete(int id);

        bool Edit(T editItem);
    }
}
