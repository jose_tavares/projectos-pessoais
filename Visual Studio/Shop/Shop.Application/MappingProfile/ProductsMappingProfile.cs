﻿using AutoMapper;
using Shop.Domain.Entities;
using Shop.Application.ViewModels;

namespace Shop.Application.MappingProfile
{
    public class ProductsMappingProfile : Profile
    {
        public ProductsMappingProfile()
        {
            DomainToViewModel();

            ViewModelToDomain();
        }

        private void DomainToViewModel() =>
            CreateMap<Product, ProductViewModel>();

        private void ViewModelToDomain() =>
            CreateMap<ProductViewModel, Product>();
    }
}
