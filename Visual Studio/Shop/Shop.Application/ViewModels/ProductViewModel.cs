﻿using System.ComponentModel;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Shop.Application.ViewModels
{
    public class ProductViewModel
    {
        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }

        [DisplayName("Nome")]
        public string Name { get; set; }

        [DisplayName("Preço")]
        [DataType(DataType.Currency)]
        public decimal Price { get; set; }

        [DisplayName("Quantidade")]
        public int Quantity { get; set; }
    }
}
