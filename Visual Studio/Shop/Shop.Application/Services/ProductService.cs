﻿using System;
using System.Net.Http;
using System.Collections.Generic;
using Shop.Application.Interfaces;
using Shop.Application.ViewModels;

namespace Shop.Application.Services
{
    public class ProductService : IGenericService<ProductViewModel>
    {
        private readonly HttpClient _client = new HttpClient();
        private readonly string _urlString = "https://localhost:44356/api/";

        public bool Add(ProductViewModel newProduct)
        {
            using (_client)
            {
                _client.BaseAddress = new Uri(_urlString);

                var postTask = _client.PostAsJsonAsync("products", newProduct);
                postTask.Wait();

                var result = postTask.Result;

                if (result.IsSuccessStatusCode)
                {
                    return true;
                }
            }

            return false;
        }

        public bool Delete(int id)
        {
            using (_client)
            {
                _client.BaseAddress = new Uri(_urlString);

                var deleteTask = _client.DeleteAsync("products/" + id.ToString());
                deleteTask.Wait();

                var result = deleteTask.Result;

                if (result.IsSuccessStatusCode)
                {
                    return true;
                }

                return false;
            }
        }

        public bool Edit(ProductViewModel editProduct)
        {
            using (_client)
            {
                _client.BaseAddress = new Uri(_urlString);

                var postTask = _client.PutAsJsonAsync("products", editProduct);
                postTask.Wait();

                var result = postTask.Result;

                if (result.IsSuccessStatusCode)
                {
                    return true;
                }
            }

            return false;
        }

        public ProductViewModel Get(int id)
        {
            using (_client)
            {
                _client.BaseAddress = new Uri(_urlString);

                var responseTask = _client.GetAsync("products/" + id.ToString());
                responseTask.Wait();

                var result = responseTask.Result;

                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<ProductViewModel>();
                    readTask.Wait();

                    return readTask.Result;
                }
            }

            return null;
        }

        public IEnumerable<ProductViewModel> Get()
        {
            using (_client)
            {
                _client.BaseAddress = new Uri(_urlString);

                var respondeTask = _client.GetAsync("products");
                respondeTask.Wait();

                var result = respondeTask.Result;

                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<IEnumerable<ProductViewModel>>();
                    readTask.Wait();

                    var values = readTask.Result;
                    return values;
                }
            }

            return null;
        }
    }
}
