﻿using Shop.Domain.Entities;
using Shop.Domain.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace Shop.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProductsController : ControllerBase
    {
        private readonly IGenericRepository<Product> _productRepository;

        public ProductsController(IGenericRepository<Product> productRepository) =>
            _productRepository = productRepository;

        [HttpGet]
        public IEnumerable<Product> Get() => _productRepository.Get();

        [HttpGet("{id}")]
        public Product Get(int? id) => _productRepository.Get(id.Value);

        [HttpPost]
        public IActionResult Post(Product newProduct)
        {
            if (_productRepository.Add(newProduct))
            {
                return Ok($"Produto {newProduct.Name} adicionado com sucesso.");
            }

            return BadRequest("Dados do produto inválidos.");
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int? id)
        {
            if (id.HasValue && _productRepository.Delete(id.Value))
            {
                return Ok($"Produto apagado com sucesso.");
            }

            return BadRequest("Dados do produto inválidos");
        }

        [HttpPut]
        public IActionResult Put(Product editProdut)
        {
            if (_productRepository.Edit(editProdut))
            {
                return Ok($"Produto {editProdut.Name} atualizado com sucesso.");
            }

            return BadRequest("Dados do produto inválidos.");
        }
    }
}