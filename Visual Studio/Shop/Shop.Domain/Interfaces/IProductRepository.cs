﻿using Shop.Domain.Entities;
using System.Collections.Generic;

namespace Shop.Domain.Interfaces
{
    public interface IProductRepository
    {
        bool Add(Product newProduct);

        bool Delete(int id);

        bool Edit(Product editProduct);

        Product Get(int id);

        IEnumerable<Product> Get();
    }
}
