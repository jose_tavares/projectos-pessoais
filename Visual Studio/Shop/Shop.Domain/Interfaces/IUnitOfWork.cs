﻿using System;
using Shop.Domain.Entities;

namespace Shop.Domain.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IGenericRepository<Product> ProductRepository { get; }

        int Save();
    }
}
