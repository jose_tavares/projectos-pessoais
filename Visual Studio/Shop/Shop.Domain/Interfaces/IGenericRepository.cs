﻿using System.Collections.Generic;

namespace Shop.Domain.Interfaces
{
    public interface IGenericRepository<T> where T : class
    {
        IEnumerable<T> Get();

        T Get(int id);

        bool Add(T newItem);

        bool Delete(int id);

        bool Edit(T editItem);
    }
}
