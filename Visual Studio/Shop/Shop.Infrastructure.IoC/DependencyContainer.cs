﻿using Shop.Domain.Interfaces;
using Shop.Application.Services;
using Shop.Application.Interfaces;
using Shop.Application.ViewModels;
using Shop.Infra.Data.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace Shop.Infrastructure.IoC
{
    public class DependencyContainer
    {
        public static void RegisterServices(IServiceCollection services)
        {
            services.AddScoped<IUnitOfWork, UnitOfWork>();

            services.AddScoped<IProductRepository, ProductRepository>();

            services.AddScoped<IGenericService<ProductViewModel>, ProductService>();
        }
    }
}
