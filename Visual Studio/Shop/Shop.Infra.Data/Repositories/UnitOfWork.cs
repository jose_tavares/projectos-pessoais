﻿using System;
using System.Linq;
using Shop.Domain.Entities;
using Shop.Domain.Interfaces;
using Shop.Infra.Data.Context;
using Shop.Application.Common;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;

namespace Shop.Infra.Data.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private bool _disposed = false;

        private readonly AppDbContext _context = new AppDbContext();

        public IGenericRepository<Product> ProductRepository { get; }

        public UnitOfWork() =>
            ProductRepository = new GenericRepository<Product>(_context);

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public int Save()
        {
            var entites = _context.ChangeTracker.Entries()
                          .Where(where => (where.State == EntityState.Added) || (where.State == EntityState.Modified))
                          .Select(select => select.Entity);

            if (entites.Validate())
            {
                foreach (var entity in entites)
                {
                    var validationContext = new ValidationContext(entity);

                    Validator.ValidateObject(entity, validationContext, true);
                }
            }

            return _context.SaveChanges();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }

            _disposed = true;
        }
    }
}
