﻿using Shop.Domain.Entities;
using Shop.Domain.Interfaces;
using System.Collections.Generic;

namespace Shop.Infra.Data.Repositories
{
    public class ProductRepository : IProductRepository
    {
        private readonly IUnitOfWork _unitOfWork;

        public ProductRepository(IUnitOfWork unitOfWork) =>
            _unitOfWork = unitOfWork;

        public bool Add(Product newProduct)
        {
            if (newProduct == null)
            {
                return false;
            }

            return _unitOfWork.ProductRepository.Add(newProduct);
        }

        public bool Delete(int id)
        {
            if (id == 0)
            {
                return false;
            }

            return _unitOfWork.ProductRepository.Delete(id);
        }

        public bool Edit(Product edit)
        {
            var product = _unitOfWork.ProductRepository.Get(edit.Id);

            if (product == null)
            {
                return false;
            }

            product.Name = edit.Name;
            product.Price = edit.Price;
            product.Quantity = edit.Quantity;            

            return _unitOfWork.ProductRepository.Edit(product);
        }

        public Product Get(int id) =>
            _unitOfWork.ProductRepository.Get(id);

        public IEnumerable<Product> Get() => 
            _unitOfWork.ProductRepository.Get();
    }
}
