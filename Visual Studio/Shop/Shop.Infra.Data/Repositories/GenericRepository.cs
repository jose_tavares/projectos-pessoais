﻿using Shop.Domain.Interfaces;
using Shop.Infra.Data.Context;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace Shop.Infra.Data.Repositories
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        private readonly DbContext _context;
        private readonly DbSet<TEntity> _entities;

        public GenericRepository(AppDbContext context)
        {
            _context = context;
            _entities = _context.Set<TEntity>();
        }

        public virtual bool Add(TEntity newItem)
        {
            if (newItem == null)
            {
                return false;
            }

            _entities.Add(newItem);

            if (_context.SaveChanges() == 0)
            {
                return false;
            }

            return true;
        }

        public virtual bool Delete(int id)
        {
            var entity = _entities.Find(id);

            if (entity == null)
            {
                return false;
            }

            _entities.Remove(entity);

            if (_context.SaveChanges() == 0)
            {
                return false;
            }

            return true;
        }

        public virtual bool Edit(TEntity editItem)
        {
            if (editItem == null)
            {
                return false;
            }

            _entities.Update(editItem);

            if (_context.SaveChanges() == 0)
            {
                return false;
            }

            return true;
        }

        public virtual IEnumerable<TEntity> Get() =>
            _entities;

        public virtual TEntity Get(int id) =>
            _entities.Find(id);
    }
}
