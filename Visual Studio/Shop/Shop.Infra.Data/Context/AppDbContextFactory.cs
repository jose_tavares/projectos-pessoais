﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Shop.Infra.Data.Context
{
    public class AppDbContextFactory : IDesignTimeDbContextFactory<AppDbContext>
    {
        public AppDbContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<AppDbContext>();
            optionsBuilder.UseSqlServer("Data Source=192.168.10.11\\SQLDEV, 1533;Initial Catalog=ShopDB; Persist Security Info=True;MultipleActiveResultSets=True;  user id=sa;Password=_PWD4sa_");

            return new AppDbContext(optionsBuilder.Options);
        }
    }
}
