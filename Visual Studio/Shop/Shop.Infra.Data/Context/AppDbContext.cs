﻿using Shop.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Shop.Infra.Data.Context
{
    public class AppDbContext : DbContext
    {
        public AppDbContext() { }

        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options) { }

        public DbSet<Product> Products { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder
                    .UseSqlServer("Data Source=192.168.10.11\\SQLDEV, 1533;Initial Catalog=ShopDB; Persist Security Info=True;MultipleActiveResultSets=True;  user id=sa;Password=_PWD4sa_");
            }
        }
    }
}
