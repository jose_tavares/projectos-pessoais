<?php

namespace App\Libraries;

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Twig\Extension\DebugExtension;
use Twig\Extension\StringLoaderExtension;
use Twig\Loader\FilesystemLoader;

class Twig
{
    private Environment $twig;

    public function __construct()
    {
        $loader = new FilesystemLoader(APPPATH . 'Views');
        $this->twig = new Environment($loader, [
            'cache' => WRITEPATH . 'cache/twig',
            'debug' => ENVIRONMENT !== 'production',
            'auto_reload' => true
        ]);
        
        // Add any Twig extensions you want to use
        $this->twig->addExtension(new DebugExtension());
        $this->twig->addExtension(new StringLoaderExtension());
        $this->twig->addExtension(new StringFunctionsExtension());
    }

    /**
     * @throws SyntaxError
     * @throws RuntimeError
     * @throws LoaderError
     */
    public function render($view, $data = [])
    {
        echo $this->twig->render($view . '.twig', $data);
    }
}
