<?php

namespace App\Libraries;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class StringFunctionsExtension extends AbstractExtension
{
    public function getFunctions(): array
    {
        return [
            new TwigFunction('strpos', [$this, 'strpos']),
        ];
    }

    public function strpos($haystack, $needle, $offset = 0)
    {
        return strpos($haystack, $needle, $offset);
    }
}
