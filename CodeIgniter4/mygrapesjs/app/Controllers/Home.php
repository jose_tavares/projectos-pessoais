<?php

namespace App\Controllers;

use App\Libraries\Twig;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class Home extends BaseController
{
    /**
     * @throws SyntaxError
     * @throws RuntimeError
     * @throws LoaderError
     */
    public function index()
    {
        $twig = new Twig();

        $twig->render('home.html', [
            'title' => 'TÍTULO EXEMPLO',
            'content' => 'Conteúdo exemplo'
        ]);
    }
}
