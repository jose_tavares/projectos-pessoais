import React, { Component } from "react";

class NomeForm extends Component {
  constructor(props) {
    super(props);

    this.state = { value: "", descricao: "", selectedValue: "" };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleSelectChange = this.handleSelectChange.bind(this);
  }

  handleChange(event) {
    if (event.target.id === "nome")
      this.setState({ value: event.target.value });
    else this.setState({ descricao: event.target.value });
  }

  handleSelectChange(event) {
    this.setState({ selectedValue: event.target.value });
  }

  handleSubmit(event) {
    alert(
      "Nome: " +
        this.state.value +
        "\nDescrição: " +
        this.state.descricao +
        "\nEscolhido: " +
        this.state.selectedValue
    );
    event.preventDefault();
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <label>
          Nome:
          <input
            id="nome"
            type="text"
            value={this.state.value}
            onChange={this.handleChange}
          />
        </label>
        <br />
        <br />
        <label>
          Descrição:
          <textarea name="descricao" onChange={this.handleChange} />
        </label>
        <br />
        <br />
        <label>
          Escolha um:
          <select
            value={this.state.selectedValue}
            onChange={this.handleSelectChange}
          >
            <option></option>
            <option value="visual basic">Visual Basic</option>
            <option value="react">React</option>
            <option value="python">Python</option>
          </select>
        </label>
        <input type="submit" value="Submeter" />
      </form>
    );
  }
}

export default NomeForm;
