import React, { Component } from "react";

class MeuButton extends Component {
  render() {
    return <button>{this.props.nome}</button>;
  }
}

class MinhaLabel extends Component {
  render() {
    return <p>{this.props.texto}</p>;
  }
}

class TesteProps extends Component {
  render() {
    return (
      <div className="TesteProps">
        <MinhaLabel texto="Macoratti.net" />
        <MeuButton nome="Botão 1" />
        <MeuButton nome="Botão 2" />
        <MeuButton nome="Botão 3" />
      </div>
    );
  }
}

export default TesteProps;
