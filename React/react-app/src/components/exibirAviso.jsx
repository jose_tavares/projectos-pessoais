import React, { Component } from "react";

class ExibirAviso extends Component {
  constructor(props) {
    super(props);

    this.state = {
      nome: "",
    };

    this.changeText = this.changeText.bind(this);
  }

  changeText(event) {
    this.setState({
      nome: event.target.value,
    });
  }

  exibirAviso = (event) => {
    alert("Sou um aviso...");
  };

  render() {
    return (
      <div>
        <button onClick={this.exibirAviso}>Clique aqui</button>
        <label htmlFor="nome">Informa o seu nome</label>
        <input type="text" id="nome" onChange={this.changeText} />
        <h3>{this.state.nome}</h3>
      </div>
    );
  }
}

export default ExibirAviso;
