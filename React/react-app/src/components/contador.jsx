import React, { Component } from "react";

class Contador extends Component {
  state = { contador: 0, itens: [] };

  estilos = {
    fontSize: 25,
    fontWeight: "normal",
  };

  renderItens() {
    if (this.state.itens.length === 0) {
      return <p>Não existem itens</p>;
    }

    return (
      <ul>
        {this.state.itens.map((item) => (
          <li key={item}>{item}</li>
        ))}
      </ul>
    );
  }

  render() {
    return (
      <React.Fragment>
        <span style={this.estilos} className={this.GetBadgeClasses()}>
          {this.formataContador()}
        </span>
        <button onClick={this.tratarIncremento} className="btn btn-secondary">
          Incrementar
        </button>
        <br />
        {this.state.itens.length === 0 && "Inclua um item..."}
        {this.renderItens()}
      </React.Fragment>
    );
  }

  GetBadgeClasses() {
    return (
      "badge m-2 badge-" + (this.state.contador > 10 ? "success" : "primary")
    );
  }

  formataContador() {
    const { contador } = this.state;
    return contador === 0 ? "Zero" : contador;
  }

  tratarIncremento = () => {
    this.setState({ contador: this.state.contador + 1 });
  };
}

export default Contador;
