import React from "react";
import OlaMundo from "./OlaMundo";

export default function App() {
  return (
    <div>
      <OlaMundo name="Macoratti.net" />
      <OlaMundo name="Quase tudo para .NET" />
    </div>
  );
}
