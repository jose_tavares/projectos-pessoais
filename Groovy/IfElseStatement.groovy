int myOpportunity = 10000

switch (myOpportunity) {
    case 0..999:
        println 'Email'
        break
    case 1000..4900:
        println 'Telephone'
        break
    default:
        println 'Face to face'
}
