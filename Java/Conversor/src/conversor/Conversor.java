/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package conversor;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 *
 * @author José Pedro
 */
public class Conversor extends JFrame {

    /**
     * @param args the command line arguments
     */
    private final JTextField txtCentigrados, txtFahrenheit;
    private final JButton btConverter, btLimpar, btSair;

    public Conversor(String titulo) {
        super(titulo);

        Container c = getContentPane();
        c.setLayout(new BorderLayout());

        JPanel pCentigrados = new JPanel();
        JPanel pFahrenheit = new JPanel();
        JPanel pBotoes = new JPanel();

        JLabel lblCentigrados = new JLabel("Centígrados");
        JLabel lblFahrenheit = new JLabel("Fahrenheit   ");

        txtCentigrados = new JTextField(5);
        txtFahrenheit = new JTextField(5);
        txtFahrenheit.setText("0");
        txtFahrenheit.setEditable(false);

        pCentigrados.add(lblCentigrados);
        pCentigrados.add(txtCentigrados);
        c.add(pCentigrados, BorderLayout.NORTH);

        pFahrenheit.add(lblFahrenheit);
        pFahrenheit.add(txtFahrenheit);
        c.add(pFahrenheit, BorderLayout.CENTER);

        btConverter = new JButton("Converter");
        btConverter.addActionListener(new Evento());
        this.getRootPane().setDefaultButton(btConverter);

        btLimpar = new JButton("Limpar");
        btLimpar.addActionListener(new Evento());

        btSair = new JButton("Sair");
        btSair.addActionListener(new Evento());

        pBotoes.add(btConverter);
        pBotoes.add(btLimpar);
        pBotoes.add(btSair);
        c.add(pBotoes, BorderLayout.SOUTH);

    }

    class Evento implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == btConverter) {
                try {
                    double fahrenheit = 1.8 * Double.parseDouble(txtCentigrados.getText()) + 32;
                    txtFahrenheit.setText(String.format("%.2f", fahrenheit));
                } catch (NumberFormatException nfe) {
                    JOptionPane.showMessageDialog(null, "Não foi introduzido um valor numérico",
                            "Conversor Centígrados - Fahrenheit", JOptionPane.ERROR_MESSAGE);
                    txtCentigrados.setText(null);
                    txtFahrenheit.setText("0");
                    txtCentigrados.requestFocus();
                }

            } else if (e.getSource() == btLimpar) {
                txtCentigrados.setText(null);
                txtFahrenheit.setText("0");
                txtCentigrados.requestFocus();
            } else {
                dispose();
            }
        }
    }

    public static void main(String[] args) {
        // TODO code application logic here

        JFrame frame = new Conversor("Conversor Centígrados - Fahrenheit");
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        frame.setSize(400, 300);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

    }
}
