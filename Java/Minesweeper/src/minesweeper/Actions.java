/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minesweeper;

import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import javafx.event.ActionEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;

/**
 *
 * @author jpbta
 */
public class Actions implements ActionListener, MouseListener {

    private final Minesweeper mine;

    public Actions(Minesweeper m) {
        mine = m;
    }

    public void actionPerformed(ActionEvent e) {
        mine.reset();

        mine.refresh();
    }

    public void mouseClicked(MouseEvent e) {
        if (e.getButton() == MouseButton.PRIMARY) {
            int x = (int) (e.getX() / 20);
            int y = (int) (e.getY() / 20);

            mine.select(x, y);
        }

        if (e.getButton() == MouseButton.SECONDARY) {
            int x = (int) (e.getX() / 20);
            int y = (int) (e.getY() / 20);

            mine.mark(x, y);
        }

        mine.refresh();
    }

    public void mouseEntered(MouseEvent e) {

    }

    public void mouseExited(MouseEvent e) {

    }

    public void mousePressed(MouseEvent e) {

    }

    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void actionPerformed(java.awt.event.ActionEvent ae) {
        
    }

    @Override
    public void mouseClicked(java.awt.event.MouseEvent me) {
        
    }

    @Override
    public void mousePressed(java.awt.event.MouseEvent me) {
        
    }

    @Override
    public void mouseReleased(java.awt.event.MouseEvent me) {
        
    }

    @Override
    public void mouseEntered(java.awt.event.MouseEvent me) {
        
    }

    @Override
    public void mouseExited(java.awt.event.MouseEvent me) {
        
    }

}
