/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minesweeper;

import java.awt.BorderLayout;
import java.util.Random;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author jpbta
 */
public class Minesweeper extends JFrame {

    private static Minesweeper instance;
    private static final long serialVersionUID = 1L;
    private final int width;
    private final int height;
    private final MinesweeperCell[][] cells;
    private final int difficulty;
    private final Board board;
    private final JButton reset;
    private boolean finished;

    private Minesweeper(int x, int y, int d) {
        width = x;
        height = y;
        difficulty = d;
        cells = new MinesweeperCell[width][height];

        reset();

        board = new Board(this);
        reset = new JButton("Reset");

        add(board, BorderLayout.CENTER);
        add(reset, BorderLayout.SOUTH);

        reset.addActionListener(new Actions(this));

        setTitle("Minesweeper");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        pack();
        setVisible(true);
    }
    
    public static Minesweeper getInstance(int x, int y, int d){
        instance = new Minesweeper(x, y, d);
        return instance;
    }

    public int getx() {
        return width;
    }

    public int gety() {
        return height;
    }

    public MinesweeperCell[][] getCells() {
        return cells;
    }

    public final void reset() {
        Random random = new Random();
        finished = false;

        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                MinesweeperCell c = new MinesweeperCell();
                cells[i][j] = c;
                int r = random.nextInt(100);

                if (r < difficulty) {
                    cells[i][j].setMine();
                }
            }
        }
        setNumbers();
    }

    private void setNumbers() {
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                int count = 0;

                if (i > 0 && j > 0 && cells[i - 1][j - 1].isMine()) {
                    count++;
                }
                if (j > 0 && cells[i][j - 1].isMine()) {
                    count++;
                }
                if (i < width - 1 && j > 0 && cells[i + 1][j - 1].isMine()) {
                    count++;
                }

                if (i > 0 && cells[i - 1][j].isMine()) {
                    count++;
                }
                if (i < width - 1 && cells[i + 1][j].isMine()) {
                    count++;
                }

                if (i > 0 && j < height - 1 && cells[i - 1][j + 1].isMine()) {
                    count++;
                }
                if (j < height - 1 && cells[i][j + 1].isMine()) {
                    count++;
                }
                if (i < width - 1 && j < height - 1 && cells[i + 1][j + 1].isMine()) {
                    count++;
                }

                cells[i][j].setNumber(count);

                if (cells[i][j].isMine()) {
                    cells[i][j].setNumber(-1);
                }

                if (cells[i][j].getNumber() == 0) {
                    cells[i][j].reveal();
                }
            }
        }

        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                if (i > 0 && j > 0 && cells[i - 1][j - 1].getNumber() == 0) {
                    cells[i][j].reveal();
                }
                if (j > 0 && cells[i][j - 1].getNumber() == 0) {
                    cells[i][j].reveal();
                }
                if (i < width - 1 && j > 0 && cells[i + 1][j - 1].getNumber() == 0) {
                    cells[i][j].reveal();
                }

                if (i > 0 && cells[i - 1][j].getNumber() == 0) {
                    cells[i][j].reveal();
                }
                if (i < width - 1 && cells[i + 1][j].getNumber() == 0) {
                    cells[i][j].reveal();
                }

                if (i > 0 && j < height - 1 && cells[i - 1][j + 1].getNumber() == 0) {
                    cells[i][j].reveal();
                }
                if (j < height - 1 && cells[i][j + 1].getNumber() == 0) {
                    cells[i][j].reveal();
                }
                if (i < width - 1 && j < height - 1 && cells[i + 1][j + 1].getNumber() == 0) {
                    cells[i][j].reveal();
                }
            }
        }
    }

    public void refresh() {
        board.repaint();
    }

    public void select(int x, int y) {
        if (cells[x][y].isFlagged()) {
            return;
        }
        cells[x][y].reveal();
        resetMarks();
        refresh();

        if (cells[x][y].isMine()) {
            loose();
        } else if (won()) {
            win();
        }
    }

    private void loose() {
        finished = true;
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                if (!cells[i][j].isObscured()) {
                    cells[i][j].unflag();
                }
                cells[i][j].reveal();
            }
        }
        refresh();
        JOptionPane.showMessageDialog(null, "BOOOOM!");
        reset();
    }

    private void win() {
        finished = true;
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                cells[i][j].reveal();
                if (!cells[i][j].isMine()) {
                    cells[i][j].unflag();
                }
            }
        }

        refresh();
        JOptionPane.showMessageDialog(null, "Congratulations! You won!");
        reset();
    }

    private boolean won() {
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                if (cells[i][j].isObscured() && !cells[i][j].isMine()) {
                    return false;
                }
            }
        }

        return true;
    }

    public void mark(int x, int y) {
        if (cells[x][y].isFlagged()) {
            cells[x][y].unflag();
        } else if (cells[x][y].isObscured()) {
            cells[x][y].flag();
        }

        resetMarks();
    }

    private void resetMarks() {
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                if (!cells[i][j].isObscured()) {
                    cells[i][j].unflag();
                }
            }
        }
    }

    public boolean isFinished() {
        return finished;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int x = 10;		//Width of the board
        int y = 10;		//Height of the board
        int d = 25;		//The difficulty of the game, the percentage of mines in the board. The number of mines per board is random, but this number is the probability that a cell will become
        //a mine.
        Minesweeper.getInstance(x, y, d);
    }

}
