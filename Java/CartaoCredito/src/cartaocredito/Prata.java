/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cartaocredito;

import java.util.ArrayList;

/**
 *
 * @author José Pedro
 */
public class Prata extends CartaoCredito {

    private float limCredito;
    private static float taxaJuro;

    public Prata(String nome, float limCredito) throws NumeroInvalidoException {
        setLimCredito(limCredito);
        super.setNome(nome);
        super.setCompras(new ArrayList<>());
        super.setNumcartao(CartaoCredito.getNumseq() + 1);
        CartaoCredito.setNumseq(CartaoCredito.getNumseq() + 1);
    }

    public float getLimCredito() {
        return limCredito;
    }

    public final void setLimCredito(float limCredito) throws NumeroInvalidoException {
        if (limCredito > 0) {
            this.limCredito = limCredito;
        } else {
            throw new NumeroInvalidoException();
        }
    }

    public float getTaxaJuro() {
        return taxaJuro;
    }

    public static void setTaxaJuro(float txJuro) {
        taxaJuro = txJuro;
    }

    @Override
    public float calculaSaldo() {
        float total = 0;
        total = super.getCompras().stream().map((c) -> c.getValor()).reduce(total, (accumulator, _item) -> accumulator + _item);
        if (total > getLimCredito()) {
            return getLimCredito() + (total - getLimCredito()) * (1 + (getTaxaJuro() / 100));
        } else {
            return total;
        }
    }
}
