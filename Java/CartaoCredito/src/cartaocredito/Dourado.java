/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cartaocredito;

import java.util.ArrayList;

/**
 *
 * @author José Pedro
 */
public class Dourado extends CartaoCredito {

    public Dourado(String nome) {
        super.setNome(nome);
        super.setCompras(new ArrayList<>());
        super.setNumcartao(CartaoCredito.getNumseq() + 1);
        CartaoCredito.setNumseq(CartaoCredito.getNumseq() + 1);
    }

    @Override
    public float calculaSaldo() {
        float total = 0;
        total = super.getCompras().stream().map((c) -> c.getValor()).reduce(total, (accumulator, _item) -> accumulator + _item);
        return total;
    }
}
