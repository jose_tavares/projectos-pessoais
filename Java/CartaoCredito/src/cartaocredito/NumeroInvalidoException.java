/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cartaocredito;

/**
 *
 * @author José Pedro
 */
public class NumeroInvalidoException extends Exception {

    public NumeroInvalidoException() {
        super("Número inválido!!!");
    }

    public NumeroInvalidoException(String msg) {
        super(msg);
    }
}
