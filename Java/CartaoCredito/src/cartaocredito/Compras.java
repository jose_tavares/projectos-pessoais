/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cartaocredito;

/**
 *
 * @author José Pedro
 */
public class Compras {

    private String descricao;
    private float valor;

    public Compras(String descricao, float valor) throws NumeroInvalidoException {
        setDescricao(descricao);
        setValor(valor);
    }

    public String getDescricao() {
        return descricao;
    }

    public final void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public float getValor() {
        return valor;
    }

    public final void setValor(float valor) throws NumeroInvalidoException {
        if (valor > 0) {
            this.valor = valor;
        } else {
            throw new NumeroInvalidoException();
        }
    }

    @Override
    public String toString() {
        return "Lista compras{" + "descricao : " + descricao + " - valor : " + valor + "€}";
    }
}
