/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cartaocredito;

import java.util.ArrayList;

/**
 *
 * @author José Pedro
 */
public abstract class CartaoCredito {

    private static int numseq;
    private int numcartao;
    private String nome;
    private ArrayList<Compras> compras;

    public static int getNumseq() {
        return numseq;
    }

    public static void setNumseq(int aNumseq) {
        numseq = aNumseq;
    }

    public abstract float calculaSaldo();

    public int getNumcartao() {
        return numcartao;
    }

    public void setNumcartao(int numcartao) {
        this.numcartao = numcartao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public ArrayList<Compras> getCompras() {
        return compras;
    }

    public void setCompras(ArrayList<Compras> compras) {
        this.compras = compras;
    }

    @Override
    public String toString() {
        return "Saldo do cartão N.º " + getNumcartao() + " - " + getNome() + " : " + calculaSaldo() + "€ "
                + getCompras();
    }
}
