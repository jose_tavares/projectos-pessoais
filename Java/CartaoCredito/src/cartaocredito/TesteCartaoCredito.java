/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cartaocredito;

import java.util.*;

/**
 *
 * @author José Pedro
 */
public class TesteCartaoCredito {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        try {
            ArrayList<CartaoCredito> cartoes = new ArrayList<>();

            cartoes.add(new Prata("Joana", 200));
            cartoes.add(new Dourado("Pedro"));
            cartoes.add(new Dourado("João"));
            cartoes.add(new Prata("Luisa", 100));

            Prata.setTaxaJuro(5f);

            cartoes.get(0).getCompras().add(new Compras("xanax", 235));
            cartoes.get(1).getCompras().add(new Compras("putas", 15));
            cartoes.get(2).getCompras().add(new Compras("binho", 10));
            cartoes.get(3).getCompras().add(new Compras("coca", 50));

            cartoes.forEach((c) -> {
                System.out.println(c);
            });

            System.out.println("\n---ORDENAÇÃO POR SALDO---");

            Collections.sort(cartoes, (CartaoCredito o1, CartaoCredito o2) -> (int) (o1.calculaSaldo() - o2.calculaSaldo()));

            cartoes.forEach((c) -> {
                System.out.println(c);
            });
        } catch (NumeroInvalidoException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
