package contabancaria;

import java.io.Serializable;

public class Conta implements Serializable {

    private static int totalContas = 0;
    private int nrConta;
    private String titular;
    private double saldo;

    public Conta(String titular, double saldo) {
        nrConta = ++totalContas;
        setTitular(titular);
        setSaldo(saldo);
    }

    public Conta(String titular) {
        this(titular, 0);
    }

    public Conta(Conta c) {
        nrConta = c.getNrConta();
        titular = c.getTitular();
        saldo = c.getSaldo();
    }

    public static void setTotalContas(int totalContas) {
        Conta.totalContas = totalContas;
    }

    public int getNrConta() {
        return nrConta;
    }

    public String getTitular() {
        return titular;
    }

    public double getSaldo() {
        return saldo;
    }

    public final void setTitular(String titular) {
        this.titular = titular;
    }

    private void setSaldo(double saldo) {
        this.saldo = saldo > 0 ? saldo : 0;
    }

    @Override
    public String toString() {
        return String.format("Nr Conta: %d - Titular: %s - Saldo: %.2f €", nrConta, titular, saldo);
    }

    /**
     *
     * @return @throws CloneNotSupportedException
     */
    @Override
    public Conta clone() throws CloneNotSupportedException {
        return new Conta(this);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 73 * hash + this.nrConta;
        hash = 73 * hash + (this.titular != null ? this.titular.hashCode() : 0);
        hash = 73 * hash + (int) (Double.doubleToLongBits(this.saldo) ^ (Double.doubleToLongBits(this.saldo) >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Conta other = (Conta) obj;
        if (this.nrConta != other.nrConta) {
            return false;
        }
        if ((this.titular == null) ? (other.titular != null) : !this.titular.equals(other.titular)) {
            return false;
        }
        return Double.doubleToLongBits(this.saldo) == Double.doubleToLongBits(other.saldo);
    }
}
