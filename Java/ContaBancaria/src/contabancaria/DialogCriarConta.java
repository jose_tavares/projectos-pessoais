package contabancaria;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

public class DialogCriarConta extends JDialog {

    private final JTextField txtTitular, txtSaldo;
    private final JButton btnOk, btnCancelar;

    public DialogCriarConta(Frame pai) {

        super(pai, "Criar Conta", true);

        JPanel p1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        p1.setBorder(new EmptyBorder(10, 0, 0, 0));
        JLabel lbl1 = new JLabel("Titular:");
        p1.add(lbl1);
        txtTitular = new JTextField(20);
        p1.add(txtTitular);

        JPanel p2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        JLabel lbl2 = new JLabel(" Saldo:");
        p2.add(lbl2);
        txtSaldo = new JTextField(10);
        p2.add(txtSaldo);

        TrataEvento t = new TrataEvento();

        JPanel p3 = new JPanel();
        p3.setBorder(new EmptyBorder(0, 10, 10, 10));
        btnOk = new JButton("OK");
        getRootPane().setDefaultButton(btnOk);
        btnOk.addActionListener(t);
        p3.add(btnOk);

        btnCancelar = new JButton("Cancelar");
        btnCancelar.addActionListener(t);
        p3.add(btnCancelar);

        add(p1, BorderLayout.NORTH);
        add(p2, BorderLayout.CENTER);
        add(p3, BorderLayout.SOUTH);

        setLocation(pai.getX() + 100, pai.getY() + 100);
        pack();
        setResizable(false);
        setVisible(true);
    }

    private class TrataEvento implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == btnOk) {
                if (txtTitular.getText().equals("")) {
                    JOptionPane.showMessageDialog(DialogCriarConta.this, "Tem que introduzir o nome do titular.", "Criação de Conta Bancária", JOptionPane.WARNING_MESSAGE);
                    txtTitular.requestFocus();
                } else {
                    String nome = txtTitular.getText();
                    float saldo;
                    try {
                        saldo = Float.parseFloat(txtSaldo.getText());
                        if (saldo < 0) {
                            JOptionPane.showMessageDialog(DialogCriarConta.this, "Tem que introduzir um valor não negativo no saldo.", "Criação de Conta Bancária", JOptionPane.WARNING_MESSAGE);
                            txtSaldo.requestFocus();
                        } else {
                            Contas.getContas().add(new Conta(nome, saldo));
                            dispose();
                        }
                    } catch (NumberFormatException ex) {
                        JOptionPane.showMessageDialog(DialogCriarConta.this, "Tem que introduzir um valor numérico no saldo.", "Criação de Conta Bancária", JOptionPane.WARNING_MESSAGE);
                        txtSaldo.requestFocus();
                    }
                }
            } else {
                dispose();
            }
        }
    }
}
