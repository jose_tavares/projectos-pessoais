package contabancaria;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import javax.swing.*;

public class Janela extends JFrame {

    public Janela() {

        super("Gestão de Contas Bancárias");

        JMenuBar menuBar;
        JMenu menu;
        JMenuItem menuItem;

        menuBar = new JMenuBar();

        menu = new JMenu("Gestão");
        menu.setMnemonic('G');
        menuBar.add(menu);

        menuItem = new JMenuItem("Criar", 'C');
        menuItem.setAccelerator(KeyStroke.getKeyStroke("ctrl C"));
        menuItem.addActionListener((ActionEvent e) -> {
            DialogCriarConta dialog = new DialogCriarConta(Janela.this);
        });
        menu.add(menuItem);

        menuItem = new JMenuItem("Consultar", 'A');
        menuItem.setAccelerator(KeyStroke.getKeyStroke("ctrl A"));
        menuItem.addActionListener((ActionEvent e) -> {
            if (!Contas.getContas().isEmpty()) {
                DialogConsultarConta dialog = new DialogConsultarConta(Janela.this);
            } else {
                JOptionPane.showMessageDialog(Janela.this, "Não há contas", "Consultar", JOptionPane.WARNING_MESSAGE);
            }
        });
        menu.add(menuItem);

        menuItem = new JMenuItem("Eliminar", 'E');
        menuItem.setAccelerator(KeyStroke.getKeyStroke("ctrl E"));
        menuItem.addActionListener((ActionEvent e) -> {
            if (!Contas.getContas().isEmpty()) {
                Object[] opcoes = Contas.getContas().toArray();
                Conta c = (Conta) JOptionPane.showInputDialog(Janela.this, "Escolha uma conta:",
                        "Eliminar Conta", JOptionPane.PLAIN_MESSAGE, null, opcoes, opcoes[0]);
                if (c != null) {
                    String[] opcoes2 = {"Sim", "Não"};
                    int resposta = JOptionPane.showOptionDialog(Janela.this,
                            "Eliminar\n" + c.toString(), "Eliminar Conta", 0,
                            JOptionPane.QUESTION_MESSAGE, null, opcoes2, opcoes2[0]);
                    if (resposta == 0) {
                        Contas.getContas().remove(c);
                    }
                }
            } else {
                JOptionPane.showMessageDialog(Janela.this, "Não há contas", "Eliminar", JOptionPane.WARNING_MESSAGE);
            }
        });
        menu.add(menuItem);

        JMenu subMenu = new JMenu("Listar");
        subMenu.setMnemonic('L');
        menu.add(subMenu);

        menuItem = new JMenuItem("Titulares", 'T');
        menuItem.setAccelerator(KeyStroke.getKeyStroke("alt T"));
        menuItem.addActionListener((ActionEvent e) -> {
            if (!Contas.getContas().isEmpty()) {
                JList lstTitulares = new JList(titulares().toArray());
                lstTitulares.setFixedCellWidth(50);
                JScrollPane scrollPane = new JScrollPane(lstTitulares);
//                    lstTitulares.setEnabled(false);
                JOptionPane.showMessageDialog(Janela.this, scrollPane, "Titulares", JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(Janela.this, "Não há contas", "Listar Titulares", JOptionPane.WARNING_MESSAGE);
            }
        });
        subMenu.add(menuItem);

        menuItem = new JMenuItem("Saldos", 'D');
        menuItem.setAccelerator(KeyStroke.getKeyStroke("alt D"));
        menuItem.addActionListener((ActionEvent e) -> {
            if (!Contas.getContas().isEmpty()) {
                JPanel pLista = new JPanel(new BorderLayout());
                JLabel lbl = new JLabel(String.format("%-22s Saldo (€)", "Nr. Conta"));
                pLista.add(lbl, BorderLayout.NORTH);
                JList lstSaldos = new JList(saldos().toArray());
                JScrollPane scrollPane = new JScrollPane(lstSaldos);
                pLista.add(scrollPane, BorderLayout.SOUTH);
                JOptionPane.showMessageDialog(Janela.this, pLista, "Saldos", JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(Janela.this, "Não há contas", "Listar Saldos", JOptionPane.WARNING_MESSAGE);
            }
        });
        subMenu.add(menuItem);

        menu.addSeparator();

        menuItem = new JMenuItem("Sair", 'S');
        menuItem.setAccelerator(KeyStroke.getKeyStroke("ctrl S"));
        menuItem.addActionListener((ActionEvent e) -> {
            fechar();
        });
        menu.add(menuItem);

        menu = new JMenu("Ficheiro");
        menu.setMnemonic('F');
        menuBar.add(menu);

        menuItem = new JMenuItem("Adicionar Contas", 'A');
        menuItem.addActionListener((ActionEvent e) -> {
            Contas.adicionarContas();
        });
        menu.add(menuItem);

        menuItem = new JMenuItem("Listagem de Contas", 'L');
        menuItem.addActionListener((ActionEvent e) -> {
            if (Contas.getContas().size() > 0) {
                Contas.criarFicheiroListagemContas();
            } else {
                JOptionPane.showMessageDialog(Janela.this, "Não há contas", "Listagem de Contas", JOptionPane.WARNING_MESSAGE);
            }
        });
        menu.add(menuItem);

        menu = new JMenu("Ajuda");
        menu.setMnemonic('A');
        menuBar.add(menu);

        menuItem = new JMenuItem("Acerca", 'C');
        menuItem.addActionListener((ActionEvent e) -> {
            JOptionPane.showMessageDialog(Janela.this, "PProg 2011/2012\n@Copyright", "Gestão de Contas Bancárias", JOptionPane.INFORMATION_MESSAGE);
        });
        menu.add(menuItem);
        setJMenuBar(menuBar);
        add(new JLabel(new ImageIcon("isep_logo.jpg")));
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                fechar();
            }
        });

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        pack();
        setResizable(false);
        setLocation(200, 200);
        setVisible(true);

    }

    private void fechar() {
        Object[] opSimNao = {"Sim", "Não"};
        if (JOptionPane.showOptionDialog(this, "Deseja fechar a aplicação?",
                "Gestão de Contas Bancárias", 0,
                JOptionPane.QUESTION_MESSAGE, null, opSimNao, opSimNao[1]) == 0) {
            Contas.gravarFicheiroContas();
            dispose();
        }
    }

    private ArrayList titulares() {
        ArrayList<String> titulares = new ArrayList();
        Contas.getContas().stream().filter((c) -> (!titulares.contains(c.getTitular()))).forEachOrdered((c) -> {
            titulares.add(c.getTitular());
        });
        return titulares;
    }

    private ArrayList saldos() {
        ArrayList<String> saldos = new ArrayList();
        Contas.getContas().forEach((c) -> {
            saldos.add(String.format("%-30d%.2f", c.getNrConta(), c.getSaldo()));
        });
        return saldos;
    }
}
