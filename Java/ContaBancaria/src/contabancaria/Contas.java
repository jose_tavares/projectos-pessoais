/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package contabancaria;

import java.io.*;
import java.util.*;
import javax.swing.*;

/**
 *
 * @author José Pedro
 */
public class Contas {

    private static ArrayList<Conta> contas = new ArrayList<Conta>();
    private static final Janela janela = new Janela();

    public static ArrayList<Conta> getContas() {
        return contas;
    }

    public static void gravarFicheiroContas() {
        try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("contas.bin"))) {
            out.writeObject(contas);
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(janela, "Ficheiro não criado", "Gravação das Contas",
                    JOptionPane.ERROR_MESSAGE);
        }
    }

    public static void lerFicheiroContas() {
        try (ObjectInputStream in = new ObjectInputStream(new FileInputStream("contas.bin"))) {
            contas = (ArrayList<Conta>) in.readObject();

            if (contas.size() > 0) {
                int maiorNrConta = contas.get(contas.size() - 1).getNrConta();
                Conta.setTotalContas(maiorNrConta);
            }
        } catch (ClassNotFoundException ex) {
            JOptionPane.showMessageDialog(janela, "Contas não Carregadas", "Carregamento das Contas",
                    JOptionPane.ERROR_MESSAGE);
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(janela, "Ficheiro não lido", "Carregamento das Contas",
                    JOptionPane.ERROR_MESSAGE);
        }
    }

    public static void adicionarContas() {
        try (Scanner in = new Scanner(new File("contas.txt"), "ISO-8859-1")) {
            ArrayList<Conta> tmp = new ArrayList<>();
            String[] dados;
            while (in.hasNextLine()) {
                dados = in.nextLine().split(";");
                tmp.add(new Conta(dados[0], Double.parseDouble(dados[1])));
            }

            contas.addAll(tmp);
            JOptionPane.showMessageDialog(janela, "Contas adicionadas com sucesso",
                    "Adicionar Contas", JOptionPane.INFORMATION_MESSAGE);
        } catch (FileNotFoundException ex) {
            JOptionPane.showMessageDialog(janela, "Ficheiro não encontrado!!!",
                    "Adicionar Contas", JOptionPane.ERROR_MESSAGE);
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(janela, "Ficheiro com saldos inválidos!!!",
                    "Adicionar Contas", JOptionPane.ERROR_MESSAGE);
        } catch (ArrayIndexOutOfBoundsException ex) {
            JOptionPane.showMessageDialog(janela, "Ficheiro com formato inválido!!!",
                    "Adicionar Contas", JOptionPane.ERROR_MESSAGE);
        }
    }

    public static void criarFicheiroListagemContas() {
        try (Formatter out = new Formatter(new File("listagem.txt"))) {
            out.format("%-15s%-20s%10s%n", "Nº. Conta", "Titular", "Saldo");
            contas.forEach((c) -> {
                out.format("%-15d%-20s%10.2f%n", c.getNrConta(), c.getTitular(), c.getSaldo());
            });

            JOptionPane.showMessageDialog(janela, "Ficheiro criado com sucesso",
                    "Listagem de Contas", JOptionPane.INFORMATION_MESSAGE);
        } catch (FileNotFoundException ex) {
            JOptionPane.showMessageDialog(janela, "Ficheiro não foi criado!!!",
                    "Listagem de Contas", JOptionPane.ERROR_MESSAGE);
        }

    }
}
