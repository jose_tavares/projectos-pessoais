/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algorithms;

/**
 *
 * @author jpbta
 */
public class Fibonacci {

    /**
     * Some first 10 primes fibonacci numbers
     * @return 
     */
    public static long bestSolution() {
        long sumTotal = 0;

        int i = 1;
        int count = 1;
        while(count <= 10) {
            int num = (int) ((getY_1(i) - getY_2(i)) / Math.sqrt(5.0));
            if (isPrime(num)) {
                sumTotal += num;
                count++;
            }
            i++;
        }

        return sumTotal;
    }

    public static double getY_1(int N) {
        return Math.pow((1 + Math.sqrt(5.0)) / 2.0, N);
    }

    public static double getY_2(int N) {
        return Math.pow((1 - Math.sqrt(5.0)) / 2.0, N);
    }

    public static boolean isPrime(int num) {
        if (num == 2 || num == 3) {
            return true;
        }

        if (num % 2 == 0 || num % 3 == 0) {
            return false;
        }

        int i = 5;
        int s = 2;

        while (i * i <= num) {
            if (num % i == 0) {
                return false;
            }

            i += s;
            s = 6 - s;
        }
        return true;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        System.out.print(bestSolution());
    }

}
