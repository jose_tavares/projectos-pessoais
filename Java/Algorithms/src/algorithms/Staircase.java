/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algorithms;

/**
 *
 * @author jpbta
 */
public class Staircase {

    private static int numWays(int n) {
        if (n == 0) {
            return 1;
        }
        int nums[] = new int[n + 1];
        nums[0] = 1;
        for (int i = 1; i <= n; i++) {
            int total = 0;
            for (int j : new int[]{1, 3, 5}) {
                if (i - j >= 0) {
                    total += nums[i - j];
                }
                nums[i] = total;
            }
        }
        return nums[n];
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        System.out.println(numWays(5));
    }

}
