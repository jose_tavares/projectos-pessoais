/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algorithms;

/**
 *
 * @author jpbta
 */
public class BinaryTree {

    private final int value;
    private final BinaryTree left, right;

    public BinaryTree() {
        this.value = 0;
        this.left = null;
        this.right = null;
    }

    public BinaryTree(int value) {
        this.value = value;
        this.left = null;
        this.right = null;
    }

    public BinaryTree(BinaryTree left, BinaryTree rigth) {
        this.value = 0;
        this.left = left;
        this.right = rigth;
    }

    public int totalSum() {
        return totalSum(this);
    }

    private int totalSum(BinaryTree root) {
        int mySum, leftSum, rightSum;
        if (root == null) {
            mySum = 0;        // Solution for the base case

            return mySum;     // Return solution
        } else {
            leftSum = totalSum(root.left);      // Solve smaller problem 1
            rightSum = totalSum(root.right);     // Solve smaller problem 2

            mySum = root.value + leftSum + rightSum;
            // Solve my problem using
            // solution of smaller problem   

            return mySum;     // Return solution
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        BinaryTree bt = new BinaryTree(
                new BinaryTree(
                        new BinaryTree(2),
                        new BinaryTree(3)
                ),
                new BinaryTree(3)
        );
        int totalSum = bt.totalSum();
        System.out.println(totalSum);
    }

}
