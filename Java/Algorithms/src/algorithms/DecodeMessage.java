/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algorithms;

/**
 *
 * @author jpbta
 */
public class DecodeMessage {

    private static int helper(String data, int k, Integer[] memo) {
        if (k == 0) {
            return 1;
        }

        int s = data.length() - k;
        if (data.charAt(s) == '0') {
            return 0;
        }

        if (memo[k] != null) {
            return memo[k];
        }

        int result = helper(data, k - 1, memo);
        for (int i = s; i < s + 2 - 1; i++) {
            if (k >= 2 && Character.getNumericValue(data.charAt(i)) <= 26) {
                result += helper(data, k - 2, memo);
            }
        }
        memo[k] = result;
        return result;
    }

    private static int numWays(String data) {
        Integer[] memo = new Integer[data.length() + 1];
        return helper(data, data.length(), memo);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        System.out.println(numWays("12"));
    }
}
