/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algorithms;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author jpbta
 */
public class FirstRecurringCharacter {

    private static char mainFuntion(String word) {
        Map<Character, Integer> counts = new HashMap<>();
        for (char c : word.toCharArray()) {
            if (counts.containsKey(c)) {
                return c;
            } else {
                counts.put(c, 1);
            }
        }
        return 0;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        System.out.println(mainFuntion("dbcaba"));
    }

}
