/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algorithms;

import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * @author jpbta
 */
public class SortArray {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int size = 1000000, total = 100;
        int array[] = new int[size];
        int arrayOrder[][] = new int[total][size];

        for (int i = 0; i < array.length; i++) {
            array[i] = ThreadLocalRandom.current().nextInt(1, total + 1);
        }

        for (int j = 0; j < array.length; j++) {
            arrayOrder[array[j] - 1][j] = array[j];
        }

        for (int t = 0; t < total; t++) {
            for (int k = 0; k < size; k++) {
                if (arrayOrder[t][k] != 0) {
                    System.out.println(arrayOrder[t][k]);
                }
            }
        }
    }

}
