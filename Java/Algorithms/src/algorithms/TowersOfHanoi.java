/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algorithms;

/**
 *
 * @author jpbta
 */
public class TowersOfHanoi {

    public static void move(int n, int from, int to, int via) {
        if (n == 1) {
            System.out.println("Move disk from pole " + from + " to pole " + to);
        } else {
            move(n - 1, from, via, to);
            move(1, from, to, via);
            move(n - 1, via, to, from);
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        move(4, 1, 2, 3);
    }

}
