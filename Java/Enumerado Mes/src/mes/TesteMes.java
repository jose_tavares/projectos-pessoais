package mes;

public class TesteMes {

    public static void main(String[] args) {

        Mes[] m = Mes.values();

        System.out.println("\nAno Corrente");
        for (Mes m1 : m) {
            System.out.printf("%-10s Dias=%d %n", m1, m1.numeroDiasAnoCorrente());
        }

        System.out.println("\nAno Bissexto: 2012");
        for (Mes m1 : m) {
            System.out.printf("%-10s Dias=%d %n", m1, m1.numeroDias(2012));
        }

        System.out.println("\nMeses pela Ordem (1,2, ...,12");
        for (int i = 1; i <= m.length; i++) {
            System.out.println(Mes.mes(i));
        }

    }
}
