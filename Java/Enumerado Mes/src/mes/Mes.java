package mes;

import java.util.Calendar;

public enum Mes {

    // Valores do tipo (Instâncias do tipo)
    Janeiro(31),
    Fevereiro(28),
    Marco(31) {
        @Override
        public String toString() {
            return "Março";
        }
    },
    Abril(30),
    Maio(31),
    Junho(30),
    Julho(31),
    Agosto(31), Setembro(30), Outubro(31),
    Novembro(30), Dezembro(31);

    // Variável de instância
    private final int dias;

    // Construtor do tipo
    private Mes(int dias) {
        this.dias = dias;
    }

    // Métodos de classe (aplicado ao tipo)
    public static Mes mes(int n) {
        return Mes.values()[n - 1];
    }

    private static boolean anoBissexto(int ano) {
        return ano % 4 == 0 && ano % 100 != 0 || ano % 400 == 0;
    }

    // Métodos de instância (aplicados aos valores do tipo)
    public int numeroDiasAnoCorrente() {
        Calendar c = Calendar.getInstance();
        if (ordinal() == 1 && anoBissexto(c.get(Calendar.YEAR))) {
            return dias + 1;
        }
        return dias;
    }

    public int numeroDias(int ano) {
        if (ordinal() == 1 && anoBissexto(ano)) {
            return dias + 1;
        }
        return dias;
    }

    public int ordem() {
        return ordinal() + 1;
    }

}
