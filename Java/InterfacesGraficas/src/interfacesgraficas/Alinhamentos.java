/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package interfacesgraficas;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 *
 * @author José Pedro
 */
public class Alinhamentos extends JFrame {

    private enum alinhamentos {

        HORIZONTAL, VERTICAL, PRINCIPAL, SECUNDARIA;
    }
    private JTextField stringField = new JTextField(13);
    private JPanel alinhada = new JPanel();
    private JComboBox<alinhamentos> alignCombo;

    public Alinhamentos() {
        super("Alinhamentos");

        //Parte fixa da esquerda
        JPanel oeste = new JPanel(new FlowLayout());
        JPanel cimaOeste = new JPanel(new BorderLayout(0, 10));
        JPanel stringPnl = new JPanel(new FlowLayout());
        JPanel alignPnl = new JPanel(new FlowLayout());

        //Escolha da palavra
        JLabel stringLbl = new JLabel("String: ");
        stringPnl.add(stringLbl);
        stringPnl.add(stringField);

        //Escolha do alinhamento
        JLabel alignLbl = new JLabel("Alinhamento: ");

        //String[] alignArr = {alinhamentos.HORIZONTAL.toString(),alinhamentos.PRINCIPAL.toString(),alinhamentos.SECUNDARIA.toString()};
        alignCombo = new JComboBox<>(alinhamentos.values());
        alignCombo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (stringField.getText().trim().equals("")) {
                    JOptionPane.showMessageDialog(Alinhamentos.this, "Vazio!!!", "Campo String", JOptionPane.ERROR_MESSAGE);
                } else {
                    Alinhamentos.this.remove(alinhada);
                    add(alinhar(stringField.getText(), (alinhamentos) alignCombo.getSelectedItem()), BorderLayout.CENTER);
                    Alinhamentos.this.revalidate();
                }
            }
        });

        alignPnl.add(alignLbl);
        alignPnl.add(alignCombo);

        cimaOeste.add(stringPnl, BorderLayout.NORTH);
        cimaOeste.add(alignPnl, BorderLayout.SOUTH);
        oeste.add(cimaOeste);
        add(oeste, BorderLayout.WEST);

        //Parte variável: letras dispostas
        stringField.setText("EXEMPLO");
        add(alinhar(stringField.getText(), alinhamentos.HORIZONTAL), BorderLayout.CENTER);
    }

    private JPanel alinhar(String palavra, alinhamentos alin) {
        if (null == alin) {
            alinhada = new JPanel(new GridLayout(palavra.length(), palavra.length()));
            for (int i = 0; i < palavra.length(); i++) {
                for (int j = (palavra.length() - 1); j >= 0; j--) {
                    if (j == i) {
                        alinhada.add(new JLabel("" + palavra.charAt(palavra.length() - 1 - i), JLabel.CENTER));
                    } else {
                        alinhada.add(new JLabel(""));
                    }
                }
            }
        } else switch (alin) {
            case HORIZONTAL:
                alinhada = new JPanel(new GridLayout(1, palavra.length()));
                for (int i = 0; i < palavra.length(); i++) {
                    alinhada.add(new JLabel("" + palavra.charAt(i), JLabel.CENTER));
                }   break;
            case PRINCIPAL:
                alinhada = new JPanel(new GridLayout(palavra.length(), palavra.length()));
                for (int i = 0; i < palavra.length(); i++) {
                    for (int j = 0; j < palavra.length(); j++) {
                        if (j == i) {
                            alinhada.add(new JLabel("" + palavra.charAt(i), JLabel.CENTER));
                        } else {
                            alinhada.add(new JLabel(""));
                        }
                    }
                }   break;
            case VERTICAL:
                alinhada = new JPanel(new GridLayout(palavra.length(), 1));
                for (int i = 0; i < palavra.length(); i++) {
                    alinhada.add(new JLabel("" + palavra.charAt(i), JLabel.CENTER));
                }   break;
            default:
                alinhada = new JPanel(new GridLayout(palavra.length(), palavra.length()));
                for (int i = 0; i < palavra.length(); i++) {
                    for (int j = (palavra.length() - 1); j >= 0; j--) {
                        if (j == i) {
                            alinhada.add(new JLabel("" + palavra.charAt(palavra.length() - 1 - i), JLabel.CENTER));
                        } else {
                            alinhada.add(new JLabel(""));
                        }
                    }
                }   break;
        }
        return alinhada;
    }

    public static void main(String[] args) {
        Alinhamentos a = new Alinhamentos();
        a.setSize(600, 150);
        a.setLocationRelativeTo(null);
        a.setDefaultCloseOperation(EXIT_ON_CLOSE);
        a.setVisible(true);
    }
}
