package interfacesgraficas;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 *
 * @author José Pedro
 */
public class ContadorCliques extends JFrame {

    //  variaveis
    private final String s = "Número de cliques: ";
    private int contador = 0;
    private JLabel lbl;
    private final JButton bt, bt2;

    //  construtores
    public ContadorCliques() {

        //  barra de titulo
        super("Contador de Cliques");

        //  layout de fluxo
        setLayout(new BorderLayout());

        //  rotulo
        JPanel p1 = new JPanel();
        lbl = new JLabel(s + contador);
        lbl.setFont(new Font("Times New Roman", Font.BOLD, 20));
        p1.add(lbl);
        add(p1, BorderLayout.CENTER);

        //  botao
        JPanel p2 = new JPanel();
        bt = new JButton("Clica aqui!");
        bt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                contador++;
                lbl.setText(s + contador);
            }
        });
        p2.add(bt);

        bt2 = new JButton("Reniciar");
        bt2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                contador = 0;
                lbl.setText(s + 0);
            }
        });
        p2.add(bt2);
        getRootPane().setDefaultButton(bt);
        add(p2, BorderLayout.SOUTH);

        //  janela
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(300, 200);
        setLocationRelativeTo(null);
        setVisible(true);
    }

    public static void main(String[] args) {
        // TODO code application logic here
        ContadorCliques jan = new ContadorCliques();
    }
}
