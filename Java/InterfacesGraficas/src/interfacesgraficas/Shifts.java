package interfacesgraficas;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 *
 * @author José Pedro
 */
public class Shifts extends JFrame {

    int nletras = 3;
    String[] todasletras = {"A", "B", "C", "D", "E", "F", "G"};
    JPanel pnlLetras = new JPanel(new GridLayout());

    public Shifts() {
        super("Shifts");
        Container c = getContentPane();
        c.setLayout(new BorderLayout());

        JPanel sul = new JPanel(new GridLayout(1, 2));
        sul.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        JPanel sulLeft = new JPanel(new FlowLayout(FlowLayout.LEFT));
        JPanel sulRight = new JPanel(new FlowLayout(FlowLayout.RIGHT));

        JMenuBar menu = new JMenuBar();
        JMenu opcoes = new JMenu("Opções");
        JMenuItem qtdLetras = new JMenuItem("Quantidade de Letras");
        qtdLetras.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String[] letras = {"2", "3", "4", "5", "6", "7"};
                nletras = Integer.parseInt((String) JOptionPane.showInputDialog(Shifts.this,
                        "Escolha quantidade de letras", "Letras", JOptionPane.QUESTION_MESSAGE, null,
                        letras, letras[0]));
                Shifts.this.remove(pnlLetras);
                Shifts.this.add(panelLetras(nletras), BorderLayout.CENTER);
                Shifts.this.revalidate();
            }
        });

        opcoes.add(qtdLetras);
        menu.add(opcoes);

        JButton left = new JButton("<<");
        left.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String temp;
                temp = ((JLabel) pnlLetras.getComponent(0)).getText();
                for (int i = 1; i < pnlLetras.getComponentCount(); i++) {
                    ((JLabel) pnlLetras.getComponent(i - 1)).setText(((JLabel) pnlLetras.getComponent(i)).getText());
                }
                ((JLabel) pnlLetras.getComponent(pnlLetras.getComponentCount() - 1)).setText(temp);
            }
        });
        JButton right = new JButton(">>");
        right.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String temp;
                temp = ((JLabel) pnlLetras.getComponent(pnlLetras.getComponentCount() - 1)).getText();
                for (int i = pnlLetras.getComponentCount() - 1; i > 0; i--) {
                    ((JLabel) pnlLetras.getComponent(i)).setText(((JLabel) pnlLetras.getComponent(i - 1)).getText());
                }
                ((JLabel) pnlLetras.getComponent(0)).setText(temp);
            }
        });

        sulLeft.add(left);
        sulRight.add(right);

        sul.add(sulLeft);
        sul.add(sulRight);

        setJMenuBar(menu);
        c.add(panelLetras(nletras), BorderLayout.CENTER);
        c.add(sul, BorderLayout.SOUTH);

    }

    private JPanel panelLetras(int qtd) {
        pnlLetras = new JPanel(new GridLayout(1, nletras));

        JLabel[] arrLabels = new JLabel[qtd];
        for (int i = 0; i < qtd; i++) {
            arrLabels[i] = new JLabel(todasletras[i]);
            arrLabels[i].setFont(new Font("Arial", Font.BOLD, 80));
            arrLabels[i].setHorizontalAlignment(JLabel.CENTER);
            pnlLetras.add(arrLabels[i]);
        }
        return pnlLetras;
    }

    public static void main(String[] args) {
        Shifts shifts = new Shifts();
        shifts.setDefaultCloseOperation(EXIT_ON_CLOSE);
        shifts.setSize(600, 400);
        shifts.setLocationRelativeTo(null);
        shifts.setVisible(true);
    }
}
