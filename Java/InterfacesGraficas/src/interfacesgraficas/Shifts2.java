package interfacesgraficas;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import static javax.swing.JFrame.EXIT_ON_CLOSE;

public class Shifts2 extends JFrame {

    private static final String[] letras = {"A", "B", "C", "D", "E", "F", "G"};
    private int totalLetras;
    private JLabel[] sequencia;
    private JPanel pCentro;
    private JTextField txtQuantidadeDeLetras;

    public Shifts2() {

        super("SHIFTS");

        JPanel pNorte = new JPanel(new FlowLayout(FlowLayout.LEFT));
        pNorte.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        add(pNorte, BorderLayout.NORTH);

        JPanel pNorte_1 = new JPanel();
        pNorte_1.setBorder(BorderFactory.createTitledBorder("Opção"));
        pNorte.add(pNorte_1);

        JLabel lbl1 = new JLabel("Quantidade de Letras [3,7]:");
        pNorte_1.add(lbl1);
        txtQuantidadeDeLetras = new JTextField(1);
        pNorte_1.add(txtQuantidadeDeLetras);

        pNorte_1.add(new JLabel("     "));

        JButton btConfirmar = new JButton("Confirmar");
        btConfirmar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String[] qtd = {"3", "4", "5", "6", "7"};
                    totalLetras = Integer.parseInt(txtQuantidadeDeLetras.getText());
                    if (totalLetras > 2 && totalLetras < 8) {
                        remove(pCentro);
                        pCentro = painelSequencia(totalLetras);
                        add(pCentro);
                        pCentro.revalidate();
                    } else {
                        JOptionPane.showMessageDialog(Shifts2.this, "Quantidade de Letras Inválida!!", "ERRO", JOptionPane.ERROR_MESSAGE);
                    }
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(Shifts2.this, "Quantidade de Letras Inválida!!", "ERRO", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        pNorte_1.add(btConfirmar);

        totalLetras = 3;
        pCentro = painelSequencia(3);
        add(pCentro);

        JPanel pSul = new JPanel(new GridLayout(1, 2));
        add(pSul, BorderLayout.SOUTH);

        JPanel pAnterior = new JPanel(new FlowLayout(FlowLayout.LEFT));
        pAnterior.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        pSul.add(pAnterior);

        JButton btAnterior = new JButton("   <<   ");
        btAnterior.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String aux = sequencia[0].getText();
                for (int j = 0; j < totalLetras - 1; j++) {
                    sequencia[j].setText(sequencia[j + 1].getText());
                }
                sequencia[totalLetras - 1].setText(aux);
            }
        });
        pAnterior.add(btAnterior);

        JPanel pSeguinte = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        pSeguinte.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        pSul.add(pSeguinte);

        JButton btSeguinte = new JButton("  >>  ");
        btSeguinte.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String aux = sequencia[totalLetras - 1].getText();
                for (int j = totalLetras - 1; j > 0; j--) {
                    sequencia[j].setText(sequencia[j - 1].getText());
                }
                sequencia[0].setText(aux);
            }
        });
        pSeguinte.add(btSeguinte);

        setDefaultCloseOperation(EXIT_ON_CLOSE);
        pack();
//        setSize(500, 200);
        setMinimumSize(new Dimension(getWidth(), getHeight()));
        setLocationRelativeTo(null);
        setVisible(true);
    }

    private JPanel painelSequencia(int totalLetras) {
        JPanel p = new JPanel(new GridLayout(1, totalLetras, 20, 10));
        sequencia = new JLabel[totalLetras];
        for (int j = 0; j < totalLetras; j++) {
            sequencia[j] = new JLabel(letras[j], JLabel.CENTER);
            sequencia[j].setFont(new Font("Arial", Font.BOLD, 50));
            p.add(sequencia[j]);
        }
        return p;
    }

    public static void main(String[] args) {
        Shifts2 shifts = new Shifts2();

    }
}
