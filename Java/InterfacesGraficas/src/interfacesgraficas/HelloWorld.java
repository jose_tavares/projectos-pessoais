package interfacesgraficas;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 *
 * @author José Pedro
 */
public class HelloWorld extends JFrame {

    //  veriaveis
    private final String s = "Hello World!";
    private final JLabel lbl;
    private final JButton bt;

    //  construtores
    public HelloWorld() {

        //  barra de titulo
        super("Hello World Swing");

        //  layout
        setLayout(new BorderLayout());

        //  rótulo
        JPanel p1 = new JPanel();
        lbl = new JLabel(s);
        lbl.setForeground(Color.blue);
        lbl.setHorizontalAlignment(JLabel.CENTER);
        lbl.setFont(new Font(null, 0, 30));
        p1.add(lbl);
        add(p1, BorderLayout.NORTH);

        //  botao
        JPanel p2 = new JPanel();
        bt = new JButton("ola");
        bt.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(HelloWorld.this,
                        "Clicou ''ola''", "Hello World Swing", JOptionPane.INFORMATION_MESSAGE, null);
            }
        });
        p2.add(bt);
        add(p2, BorderLayout.SOUTH);

        //  janela
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(270, 150);
        setLocationRelativeTo(null);
        setVisible(true);
    }

    public static void main(String[] args) {
        // TODO code application logic here

        HelloWorld jan = new HelloWorld();
    }
}
