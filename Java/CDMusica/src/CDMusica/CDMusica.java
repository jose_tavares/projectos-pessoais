package CDMusica;

import java.awt.*;
import java.awt.event.*;
import java.util.Vector;
import javax.swing.*;
import javax.swing.event.*;

public class CDMusica extends JFrame {

    JLabel lb1, lb2, lb3, lb4, lb5;
    JList listCDs, listPistas;
    JButton bTerminar;
    JTextField t1;
    Container principal;
    JPanel painel1, painel2, painel21, painel22, painel3;
    String[] s;
    Vector cds1;
    boolean[] cdsTocados;
    int cdActual;

    public CDMusica(Vector cds) {
        super("Leitor de CD's");

        //DEFINIÇAO DO LAYOUT DA JANELA 
        principal = getContentPane();
        setSize(600, 400);
        setLocationRelativeTo(null);
        principal.setLayout(new BorderLayout());

        //CRIAÇAO DOS PAINEIS E RESPECTIVOS COMPONENTES 
        // PAINEL 1 
        painel1 = new JPanel(new BorderLayout());
        lb1 = new JLabel("HORA DE MUSICA", JLabel.CENTER);
        painel1.add("North", lb1);
        painel1.add("South", new JPanel());

        // PAINEL 2 = PAINEL 21 + PAINEL 22 
        painel2 = new JPanel(new GridLayout(2, 1, 0, 0));
        painel21 = new JPanel(new FlowLayout());
        // Criação do vector que guarda os títulos dos CDs 
        s = new String[cds.size()];
        for (int i = 0; i < cds.size(); i++) {
            s[i] = (((CD) cds.elementAt(i)).getTitulo());
        }
        lb2 = new JLabel("Seleccione o CD:", JLabel.CENTER);
        listCDs = new JList(s);
        listCDs.setSelectedIndex(0);
        listCDs.setFixedCellHeight(15);
        listCDs.setFixedCellWidth(100);
        listCDs.setVisibleRowCount(3);
        listCDs.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        painel21.add(lb2);
        painel21.add(new JScrollPane(listCDs));
        painel22 = new JPanel(new FlowLayout());
        lb3 = new JLabel("Titulo do CD", JLabel.CENTER);
        t1 = new JTextField(15);
        t1.setEditable(false);
        lb4 = new JLabel("Pistas", JLabel.CENTER);
        listPistas = new JList();
        listPistas.setSelectedIndex(0);
        listPistas.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        listPistas.setFixedCellHeight(15);
        listPistas.setFixedCellWidth(100);
        listPistas.setVisibleRowCount(3);
        painel22.add(lb3);
        painel22.add(t1);
        painel22.add(lb4);
        painel22.add(new JScrollPane(listPistas));
        painel2.add(painel21);
        painel2.add(painel22);
        painel22.setVisible(false);

        // PAINEL 3  
        painel3 = new JPanel(new FlowLayout(FlowLayout.CENTER));
        bTerminar = new JButton("Terminar");
        painel3.add(bTerminar);

        // ADICIONAR PAINEIS AO PRINCIPAL 
        principal.add("North", painel1);
        principal.add("Center", painel2);
        principal.add("South", painel3);

        // CONSTRUCAO DOS LISTENERS 
        MeuWindowListener wl = new MeuWindowListener();
        addWindowListener(wl);
        BotaoListener bl = new BotaoListener();
        bTerminar.addActionListener(bl);
        ListaListener1 ll1 = new ListaListener1();
        ListaListener2 ll2 = new ListaListener2();
        listCDs.addListSelectionListener(ll1);
        listPistas.addListSelectionListener(ll2);

        // Criação do vector que guardará os indices dos CDs 
        // cujas músicas foram tocadas. 
        cds1 = cds;
        cdsTocados = new boolean[cds.size()];
        for (int i = 0; i < cds.size(); i++) {
            cdsTocados[i] = false;
        }
    }

    class ListaListener1 implements ListSelectionListener {

        @Override
        public void valueChanged(ListSelectionEvent e) {
            if (e.getValueIsAdjusting()) {
                return;
            }
            int numcd = listCDs.getSelectedIndex();
            cdActual = numcd;
            t1.setText(s[numcd]);
            listPistas.setListData(((CD) cds1.elementAt(numcd)).getMusicas());
            listPistas.repaint();
            validate();
            painel22.setVisible(true);
        }
    }

    class ListaListener2 implements ListSelectionListener {

        @Override
        public void valueChanged(ListSelectionEvent e) {
            if (e.getValueIsAdjusting()) {
                return;
            }
            if (((JList) e.getSource()).isSelectionEmpty()) {
                return;
            }
            String smusica;
            smusica = (String) (listPistas.getSelectedValue());
            JOptionPane.showMessageDialog(
                    null,
                    smusica,
                    "Esta a ouvir a musica ...",
                    JOptionPane.INFORMATION_MESSAGE);
            if (cdsTocados[cdActual] == false) {
                cdsTocados[cdActual] = true;
            }
        }
    }

    class BotaoListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            JButton b = (JButton) e.getSource();
            if (b.equals(bTerminar)) {
                JTextArea tarea = new JTextArea(15, 20);
                for (int i = 0; i < cdsTocados.length; i++) {
                    if (cdsTocados[i] == true) {
                        tarea.append(((CD) cds1.elementAt(i)).getTitulo());
                        tarea.append("\n");
                    }
                }
                JOptionPane.showMessageDialog(
                        null,
                        tarea,
                        "Cds tocados",
                        JOptionPane.INFORMATION_MESSAGE);

            }
        }
    }

    class MeuWindowListener extends WindowAdapter {

        @Override
        public void windowClosing(WindowEvent e) {
            System.exit(0);
        }
    }

    // Método para teste da aplicação 
    public static void main(String[] args) {
        Vector cds = new Vector(20, 5);
        CD cd;
        for (int i = 0; i < Discos.discos.length; i++) {
            cd = new CD(Discos.discos[i], Discos.musicas[i]);
            cds.addElement(cd);
        }
        JFrame jan = new CDMusica(cds);
        jan.setVisible(true);
    }
}
