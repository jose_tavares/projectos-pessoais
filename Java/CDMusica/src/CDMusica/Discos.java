/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package CDMusica;

/**
 *
 * @author José Pedro
 */
public class Discos {

    static String[] discos = {"RadioHead - Kid A",
        "AIR - 10000 HZ Legend",
        "Clã - Lustro",
        "Morfhine - The night",
        "Moloko - Things to make and do"
    };
    static String[][] musicas = {
        {"Everything in its right place",
            "Kid A",
            "The national anthem",
            "How to disappear completely",
            "Treefingers",
            "Optimist",
            "In limbo",
            "Idioteque",
            "Morning bell",
            "Motion picture soundtrack"
        },
        {"Electronic Performers",
            "How does it make you feel?",
            "Radio #1",
            "The Vagabond",
            "Radian",
            "Lucky and Unhappy",
            "Sex Born Poison",
            "People in the city"
        },
        {"Dançar na corda bamba",
            "H2omem",
            "Amigos de quem",
            "Osopro do coração",
            "Fahrenheit",
            "Lado esquerdo",
            "Osorriso Gioconda",
            "Curioso clã",
            "Bem versus mal",
            "Depois do amor",
            "A doença do bem",
            "Sangue frio"
        },
        {"The night",
            "So many ways",
            "Souvenir",
            "Top floor.Bottom buzzer",
            "Like a mirror",
            "A good woman is hard to find",
            "Rope on fire",
            "I'm yours.You are mine",
            "The way we met",
            "Slow numbers",
            "Take me with you"
        },
        {"Pure pleasure seeker",
            "Absent minded friends",
            "Indigo", "Remain the same",
            "A drop in the ocean",
            "Dumb inc",
            "Mother", "The time is now",
            "It's nothing",
            "Somebody somewhere",
            "Sing it back"
        }
    };
}
