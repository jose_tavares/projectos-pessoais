/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package CDMusica;

/**
 *
 * @author José Pedro
 */
public class CD {

    private final String titulo;
    private final String[] titulosMusicas;

    public CD(String t, String[] tm) {
        titulo = t;
        titulosMusicas = new String[tm.length];
        System.arraycopy(tm, 0, titulosMusicas, 0, tm.length);
    }

    public String getTitulo() {
        return titulo;
    }

    public String[] getMusicas() {
        return titulosMusicas;
    }

    public int getNumMusicas() {
        return titulosMusicas.length;
    }
}
