package condominio;

import condominio.Habitacao.Tipologia;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;

public class DialogHabitacao extends JDialog {

    public DialogHabitacao(JFrame FrameInicial) {
        super(FrameInicial, "Criar Habitação", true);

        Container c = getContentPane();
        c.setLayout(new GridLayout(4, 2, 0, 5));

        JPanel tipolo = new JPanel(new FlowLayout(FlowLayout.LEFT));
        JLabel ltipolo = new JLabel("Tipologia:");
        final JComboBox ctipolo = new JComboBox(Habitacao.Tipologia.values());
        tipolo.add(ltipolo);
        tipolo.add(ctipolo);
        c.add(tipolo);

        JPanel prop = new JPanel(new FlowLayout(FlowLayout.LEFT));
        JLabel lprop = new JLabel("Proprietário: ");
        final JTextField tprop = new JTextField(30);
        prop.add(lprop);
        prop.add(tprop);
        c.add(prop);

        JPanel area = new JPanel(new FlowLayout(FlowLayout.LEFT));
        JLabel larea = new JLabel("Área: ");
        final JTextField tarea = new JTextField(10);
        area.add(larea);
        area.add(tarea);
        c.add(area);

        JPanel butoes = new JPanel(new FlowLayout(FlowLayout.CENTER));
        JButton btok = new JButton("OK");
        JButton btcanc = new JButton("Cancelar");
        butoes.add(btok);
        butoes.add(btcanc);
        c.add(butoes);

        btok.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Habitacao.Tipologia tp;
                ArrayList<Fraccao> fr = new ArrayList<>();
                tp = (Tipologia) ctipolo.getSelectedItem();
                String prope = tprop.getText();
                String sarea = tarea.getText();
                double area1 = Double.parseDouble(sarea);
                try {
                    Habitacao ht = new Habitacao(prope, area1, tp);
                    fr.add(ht);
                    JOptionPane.showMessageDialog(null, "Habitação criada");
                    dispose();
                } catch (ValorNegativoOuNuloException ex) {
                    JOptionPane.showMessageDialog(null, "Insira valores válidos",
                            "Erro", JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        setLocation(200, 200);
        setSize(440, 230);
        setResizable(true);
        setVisible(true);
        setDefaultCloseOperation(DialogHabitacao.DISPOSE_ON_CLOSE);

    }
}
