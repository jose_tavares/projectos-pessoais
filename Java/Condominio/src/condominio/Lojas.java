package condominio;

public class Lojas extends Fraccao {

    private double areaMontra;

    public Lojas(String nome, double area) throws ValorNegativoOuNuloException {
        super(nome, area);
    }

    public Lojas(String nome, double area, double areaMontra) throws ValorNegativoOuNuloException {
        super(nome, area);
        setAreaMontra(areaMontra);
    }

    public final void setAreaMontra(double areaMontra) throws ValorNegativoOuNuloException {
        if (areaMontra <= 0) {
            throw new ValorNegativoOuNuloException();
        } else {
            this.areaMontra = areaMontra;
        }
    }

    public double getAreaMontra() {
        return areaMontra;
    }

    @Override
    public double calcularCondominio() {
        return (super.CustoM2() * (super.getArea() + this.areaMontra));
    }

    @Override
    public String toString() {
        return String.format("Loja: %s | Área: %.2f | Área de Montra: %.2f ",
                super.getProprietario(), super.getArea(), this.getAreaMontra());
    }
}
