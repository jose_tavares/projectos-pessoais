package condominio;

import java.util.*;

public class Condominio {

    public static void main(String[] args) throws ValorNegativoOuNuloException {
        FrameInicial fi = new FrameInicial();

        //
        ArrayList<Fraccao> a = new ArrayList<>();
        try {
            Habitacao h1 = new Habitacao("António", -60, Habitacao.Tipologia.T1);
            Habitacao h2 = new Habitacao("Ana", 160, Habitacao.Tipologia.T3);
            Lojas l1 = new Lojas("O Golfinho", -100, 20);
            Lojas l2 = new Lojas("Luisinha", -50, 0);

            Habitacao.setCustom2(1.2);
            Lojas.setCustom2(2);

            a.add(h1);
            a.add(h2);
            a.add(l1);
            a.add(l2);

            System.out.println("Fracções");

            a.forEach((f) -> {
                System.out.println(f + " Condomínio: " + f.calcularCondominio() + "€");
            });
        } catch (ValorNegativoOuNuloException e) {
            Habitacao h1 = new Habitacao("António", 60, Habitacao.Tipologia.T1);
            Habitacao h2 = new Habitacao("Ana", 160, Habitacao.Tipologia.T3);
            Lojas l1 = new Lojas("O Golfinho", 100, 20);
            Lojas l2 = new Lojas("Luisinha", 50, 10);

            Habitacao.setCustom2(1.2);
            Lojas.setCustom2(2);

            a.add(h1);
            a.add(h2);
            a.add(l1);
            a.add(l2);

            System.out.println("Fracções");

            a.forEach((f) -> {
                System.out.println(f + " Condomínio: " + f.calcularCondominio() + "€");
            });
        }

    }
}
