package condominio;

public class ValorNegativoOuNuloException extends Exception {

    public ValorNegativoOuNuloException() {
        super("Valor negativo ou nulo");
    }

    public ValorNegativoOuNuloException(String s) {
        super(s);
    }
}
