package condominio;

public class Habitacao extends Fraccao {

    public enum Tipologia {

        T0, T1, T2, T3, T4, T5
    };
    private Tipologia t;

    public Habitacao(String nome, double area, Tipologia t) throws ValorNegativoOuNuloException {

        super(nome, area);
        setTipologia(t);

    }

    public Habitacao(String nome, double area) throws ValorNegativoOuNuloException {
        super(nome, area);
    }

    public final void setTipologia(Tipologia t) {
        this.t = t;
    }

    public Tipologia getTipologia() {
        return t;
    }

    @Override
    public double calcularCondominio() {
        return super.CustoM2() * super.getArea();
    }

    @Override
    public String toString() {
        return "Proprietário: " + super.getProprietario() + " Área: " + super.getArea() + " Tipologia: " + this.getTipologia();
    }
}
