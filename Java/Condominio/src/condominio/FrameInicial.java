package condominio;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 *
 * @author Miguel
 */
public class FrameInicial extends JFrame {

    private final JMenuBar menubar;
    private final JMenu criar;
    private final JMenuItem habit, loja, sair;

    public FrameInicial() {
        super("Condomínio");

        Container c = getContentPane();

        menubar = new JMenuBar();
        setJMenuBar(menubar);

        criar = new JMenu("Criar");
        menubar.add(criar);

        habit = new JMenuItem("Habitações");
        loja = new JMenuItem("Lojas");
        sair = new JMenuItem("Sair");
        criar.add(habit);
        criar.add(loja);
        criar.add(sair);

        habit.addActionListener((ActionEvent e) -> {
            DialogHabitacao dh = new DialogHabitacao(FrameInicial.this);
        });

        sair.addActionListener((ActionEvent e) -> {
            System.exit(0);
        });

        setLocation(200, 200);
        setSize(300, 230);
        setResizable(false);
        setVisible(true);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(FrameInicial.EXIT_ON_CLOSE);
    }
}
