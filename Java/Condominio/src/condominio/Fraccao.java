package condominio;

public abstract class Fraccao {

    private String proprietario;
    private double area;
    private static double CustoM2;

    public Fraccao(String proprietario, double area) throws ValorNegativoOuNuloException {
        setProprietario(proprietario);
        setArea(area);
    }

    public Fraccao(Fraccao f) throws ValorNegativoOuNuloException {
        proprietario = f.getProprietario();
        area = f.getArea();
    }

    public final void setProprietario(String proprietario) {
        this.proprietario = proprietario;
    }

    public final void setArea(double area) throws ValorNegativoOuNuloException {
        if (area <= 0) {
            throw new ValorNegativoOuNuloException();
        } else {
            this.area = area;
        }
    }

    public static void setCustom2(double CustoM) {
        CustoM2 = CustoM;
    }

    public double CustoM2() {
        return CustoM2;
    }

    public String getProprietario() {
        return proprietario;
    }

    public double getArea() {
        return area;
    }

    public abstract double calcularCondominio();
}
