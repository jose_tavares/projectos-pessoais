/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpatutorial;

import javax.persistence.*;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        // Open a database connection
        // (create a new database if it doesn't exist yet):
        EntityManagerFactory emf
                = Persistence.createEntityManagerFactory("$objectdb/db/points.odb");
        EntityManager em = emf.createEntityManager();

        // Store 1000 Point objects in the database:
        em.getTransaction().begin();
        Query q1 = em.createQuery("DELETE FROM PointEntity");
        q1.executeUpdate();
        for (int i = 0; i < 1000; i++) {
            Point p = new Point(i, i);
            em.persist(p);
        }
        em.getTransaction().commit();

        // Find the number of Point objects in the database:
        Query q2 = em.createQuery("SELECT COUNT(p) FROM PointEntity p");
        System.out.println("Total Points: " + q2.getSingleResult());

        // Find the average X value:
        Query q3 = em.createQuery("SELECT AVG(p.x) FROM PointEntity p");
        System.out.println("Average X: " + q3.getSingleResult());

        // Retrieve all the Point objects from the database:
        TypedQuery<Point> query
                = em.createQuery("SELECT p FROM PointEntity p", Point.class);
        List<Point> results = query.getResultList();
        results.forEach((p) -> {
            System.out.println(p);
        });

        // Close the database connection:
        em.close();
        emf.close();
    }
}
