package baralho;

public class Carta {

    private int valor = NAOATRIBUIDO;
    private int naipe = NAOATRIBUIDO;
    private boolean faceCima = false;
    private int numId;
    private static int proxNumId = 0;
    public static final int NAOATRIBUIDO = -1;
    public static final int AS = 1;
    public static final int DAMA = 11;
    public static final int VALETE = 12;
    public static final int REI = 13;
    public static final int COPAS = 1;
    public static final int OUROS = 2;
    public static final int PAUS = 3;
    public static final int ESPADAS = 4;

    public Carta(int v, int n) {
        if (valorValido(v) && naipeValido(n)) {
            numId = proxNumId++;
            valor = v;
            naipe = n;
        } else {
            System.out.println("Erro. Carta nao valida");
        }
    }

    public static boolean valorValido(int v) {
        return v >= 1 && v <= 13;
    }

    public static boolean naipeValido(int n) {
        return n >= 1 && n <= 4;
    }

    public int getNumId() {
        return numId;
    }

    public int getValor() {
        return valor;
    }

    public int getNaipe() {
        return naipe;
    }

    public void virarCarta() {
        faceCima = !faceCima;
    }

    @Override
    public String toString() {
        String v;
        String n;
        switch (valor) {
            case AS:
                v = "As";
                break;
            case VALETE:
                v = "Valete";
                break;
            case DAMA:
                v = "Dama";
                break;
            case REI:
                v = "Rei";
                break;
            default:
                v = "" + valor;
                break;
        }
        switch (naipe) {
            case OUROS:
                n = "Ouros";
                break;
            case COPAS:
                n = "Copas";
                break;
            case ESPADAS:
                n = "Espadas";
                break;
            /*
         * naipe == PAUS
             */
            default:
                n = "Paus";
                break;
        }
        return v + " de " + n;
    }
}
