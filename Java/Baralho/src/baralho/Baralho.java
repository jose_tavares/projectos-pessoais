package baralho;

import java.util.Vector;

public class Baralho {

    private final Carta[] baralhoCartas;
    private int cartas;

    public Baralho() {
        baralhoCartas = new Carta[52];
        encher();
    }

    private void encher() {
        int ind = 0;
        for (int i = 1; i <= 4; i++) // naipe 
        {
            for (int j = 1; j <= 13; j++) // carta 
            {
                baralhoCartas[ind++] = new Carta(j, i);
            }
        }
        cartas = 52;
    }

    public int getCartas() {
        return cartas;
    }

    public Carta[] getBaralhoCartas() {
        return baralhoCartas;
    }

    public void baralhar() {
        System.out.println("Baralhar as cartas");
    }

    public void darCartas() {
        System.out.println("Dar as Cartas");
    }

    public void mostrar() {
        for (int i = 0; i < cartas; i++) {
            System.out.print("N.= " + baralhoCartas[i].getNumId() + "  ");
            System.out.print("Naipe = " + baralhoCartas[i].getNaipe() + "  ");
            System.out.print("Valor = " + baralhoCartas[i].getValor() + "\t");
            System.out.println(baralhoCartas[i]);
        }
    }

    public static void retirarCartasAleatoriamente(Baralho b) {
        int quant = b.getCartas();
        Carta[] a = b.getBaralhoCartas();
        Vector v = new Vector(quant);
        for (int i = 0; i < quant; i++) {
            v.addElement(a[i]);
        }
        while (v.size() > 0) {
            int r = (int) (Math.random() * v.size());
            System.out.println("Retirada a carta " + v.elementAt(r));
            v.removeElementAt(r);
        }
    }

    public static void main(String[] args) throws java.io.IOException {
        Baralho b = new Baralho();
        b.mostrar();
        System.out.println("\n\nRetirar cartas");
        retirarCartasAleatoriamente(b);
    }
}
