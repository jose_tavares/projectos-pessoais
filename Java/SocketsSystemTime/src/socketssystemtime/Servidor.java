/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socketssystemtime;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;

/**
 *
 * @author jpbta
 */
public class Servidor {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
        // TODO code application logic here
        ServerSocket listener = new ServerSocket(65333);
        System.out.println("Servidor em execução no porto 65333...");
        try {
            while (true) {
                Socket socket = listener.accept();
                System.out.println(socket.getRemoteSocketAddress().toString());
                try (PrintWriter out = new PrintWriter(socket.getOutputStream(), true)) {
                    out.println(new Date().toString());
                } finally {
                    socket.close();
                }
            }
        } finally {
            listener.close();
        }
    }

}
