/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socketssystemtime;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import javax.swing.JOptionPane;

/**
 *
 * @author jpbta
 */
public class Cliente {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
        // TODO code application logic here
        String ipServidor = JOptionPane.showInputDialog("Qual o IP do Servidor: ");
        Socket socket = new Socket(ipServidor, 65333);
        
        BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        String resposta = input.readLine();
        
        JOptionPane.showMessageDialog(null, resposta);
        System.exit(0);
    }
    
}
