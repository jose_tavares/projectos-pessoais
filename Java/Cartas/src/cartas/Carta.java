/*
 * Carta.java
 */
package cartas;

/**
 * @author PPROG
 */
public class Carta implements Comparable<Carta> {

    @Override
    public int compareTo(Carta c) {

        if (n != c.n) {
            return n.compareTo(c.n);
        } else {
            return t.compareTo(c.t);
        }

    }

    // Tipo enumerado de poss�veis cartas
    public static enum Tipo {

        Duque, Terno, Quadra, Quina, Sena,
        Oito, Nove, Dez, Dama, Valete, Rei, Manilha, As
    };

    // Tipo enumerado de poss�veis naipes
    public static enum Naipe {

        Copas, Ouros, Espadas, Paus
    };
    /* Tipo da inst�ncia de carta */
    private Tipo t;
    private Naipe n;

    /**
     * Cria uma nova inst�ncia de Carta
     *
     * @param t
     * @param n
     */
    public Carta(Tipo t, Naipe n) {
        this.t = t;
        this.n = n;
    }

    /**
     * Cria uma nova inst�ncia de Carta baseada numa string. ex: "As de Ouros"
     *
     * @param s
     */
    public Carta(String s) {
        String[] ls = s.split(" ");
        this.t = Tipo.valueOf(ls[0]);
        this.n = Naipe.valueOf(ls[2]);
    }

    /**
     * Devolve o tipo da carta
     *
     * @return
     */
    public Tipo getTipo() {
        return t;
    }

    /**
     * Devolve o naipe da carta
     *
     * @return
     */
    public Naipe getNaipe() {
        return n;
    }

    /**
     * Converte uma carta numa string. ex: "As de Ouros"
     *
     * @return
     */
    @Override
    public String toString() {
        return t.toString() + " de " + n.toString();
    }
}
