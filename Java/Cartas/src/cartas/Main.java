/*
 * Main.java
 */
package cartas;

import java.util.ArrayList;

/**
 * @author PPROG
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        for (int x = 0; x < 10; x++) {

            // Baralhar as cartas
            Baralho b = new Baralho(Baralho.Tipo.BAR_40);
            b.baralhar();

            // Retirar um conjunto de 10 cartas
            MaoDeJogo jogo = new MaoDeJogo(b.primeirasCartas(10));
            System.out.println("Jogo: " + jogo);

            // Quantos tipos de cartas h�?
            ArrayList<Carta.Tipo> at = jogo.tiposDeCarta();
            System.out.println("Neste jogo h� os tipos: " + at);

            // Quantas cartas por cada tipo?
            for (Carta.Tipo t : at) {
                System.out.println("Neste jogo h� " + jogo.contarCartasDoTipo(t) + " cartas do tipo " + t);
            }

            // Ordenar novamente o Jogo de fora da classe
            jogo.ordenar();
            System.out.println("Jogo ordenado: " + jogo);
        }
    }
}
