/*
 * MaoDeJogo.java
 */
package cartas;

import java.util.ArrayList;
import java.util.Collections;

/**
 * @author PPROG
 */
public class MaoDeJogo extends ArrayList<Carta> {

    /**
     * Cria uma nova inst�ncia de MaoDeJogo
     *
     * @param l
     */
    public MaoDeJogo(ArrayList<Carta> l) {
        super(l);
    }

    public ArrayList<Carta.Tipo> tiposDeCarta() {
        //TODO: Devolver um conjunto que contenha os v�rios tipos (duque, terno, etc.) que existem na mao de jogo.
        ArrayList<Carta.Tipo> t = new ArrayList<Carta.Tipo>();
        for (Carta c : this) {
            if (!t.contains(c.getTipo())) {
                t.add(c.getTipo());
            } else {
                System.out.println("J� tem " + c.getTipo());
            }
        }
        return t;
    }

    public int contarCartasDoTipo(Carta.Tipo t) {
        int n = 0;
        for (Carta c : this) {
            if (t == c.getTipo()) {
                n++;
            }
        }
        return n;
    }

    public void ordenar() {
        //TODO: Ordenar o jogo, primeiro por naipe e, no mesmo naipe, ordenar por tipo
        // A "Carta" deve implementar a interface Comparable, devendo implementar expecificamente Comparable <Carta>
        Collections.sort(this);

    }
}
