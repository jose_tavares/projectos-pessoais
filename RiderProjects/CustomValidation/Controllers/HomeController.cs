﻿using CustomValidation.Models;
using Microsoft.AspNetCore.Mvc;

namespace CustomValidation.Controllers;

public class HomeController : Controller
{
    public IActionResult Index()
    {
        return View();
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public IActionResult Index(Aluno aluno)
    {
        return ModelState.IsValid ? Ok("Valid") : View();
    }
}