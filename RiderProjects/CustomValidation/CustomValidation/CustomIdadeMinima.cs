﻿using System.ComponentModel.DataAnnotations;
using CustomValidation.Models;

namespace CustomValidation.CustomValidation;

public class CustomIdadeMinima : ValidationAttribute
{
    protected override ValidationResult IsValid(object value, ValidationContext validationContext)
    {
        var aluno = (Aluno)validationContext.ObjectInstance;

        if (aluno.DataNascimento is null)
            return new ValidationResult("Informe a data de nascimento.");

        var idade = DateTime.Today.Year - aluno.DataNascimento.Value.Year;

        return (idade >= 18)
            ? ValidationResult.Success
            : new ValidationResult("O aluno deve ter no mínimo 18 anos.");
    }
}