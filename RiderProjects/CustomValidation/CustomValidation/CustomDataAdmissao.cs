﻿using System.ComponentModel.DataAnnotations;

namespace CustomValidation.CustomValidation;

public class CustomDataAdmissao : ValidationAttribute
{
    public override bool IsValid(object value)
    {
        var dateTime = Convert.ToDateTime(value);
        return dateTime <= DateTime.Now;
    }
}