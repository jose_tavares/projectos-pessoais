﻿using System.ComponentModel.DataAnnotations;
using CustomValidation.CustomValidation;

namespace CustomValidation.Models;

public class Aluno
{
    [Key]
    public int Id { get; set; }
    
    [Required(ErrorMessage = "Informe o nome")]
    public string? Nome { get; set; }

    [Required(ErrorMessage = "Escolha a data de admissão.")]
    [Display(Name = "Data de Admissão")]
    [DataType(DataType.Date)]
    [CustomDataAdmissao(ErrorMessage = "A data de admissão deve ser menor ou igual a hoje.")]
    public DateTime? DataAdmissao { get; set; }

    [Display(Name = "Date de Nascimento")]
    [DataType(DataType.Date)]
    [CustomIdadeMinima]
    public DateTime? DataNascimento { get; set; }
}