﻿using Newtonsoft.Json;
using System.Diagnostics;
using Aspc_Session1.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace Aspc_Session1.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            HttpContext.Session.SetString("SessionNome", "Macoratti");
            HttpContext.Session.SetInt32("SessionIdade", 54);

            var userInfo = new User()
            {
                Id = 1,
                Nome = "Macoratti"
            };

            HttpContext.Session.SetString("SessionUser", JsonConvert.SerializeObject(userInfo));

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
