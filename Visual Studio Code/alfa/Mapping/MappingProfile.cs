﻿using AutoMapper;
using alfa.Models;
using alfa.Resources;

namespace alfa.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Marca, MarcaResource>();
            CreateMap<Modelo, ModeloResource>();
            CreateMap<Acessorio, AcessorioResource>();
        }
    }
}
