import { Marca } from "../models/marca";
import { Component, OnInit } from "@angular/core";
import { VeiculoService } from "../services/veiculo.service";
import { Modelo } from "../models/modelo";

@Component({
  selector: "app-veiculo-form",
  templateUrl: "./veiculo-form.component.html",
  styleUrls: ["./veiculo-form.component.css"],
})
export class VeiculoFormComponent implements OnInit {
  marcas: Marca[];
  modelos: Modelo[];

  constructor(private veiculoService: VeiculoService) {}

  ngOnInit() {
    this.veiculoService
      .getMarcas()
      .subscribe((marcas) => (this.marcas = marcas));
  }

  changeModelos(id: number) {
    if (id.toString() === "") {
      this.modelos = [];
      return;
    }

    this.veiculoService
      .getModelos(id)
      .subscribe((modelos) => (this.modelos = modelos));
  }
}
