import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { AppComponent } from "./app.component";
import { RouterModule } from "@angular/router";
import { HomeComponent } from "./home/home.component";
import { HttpClientModule } from "@angular/common/http";
import { BrowserModule } from "@angular/platform-browser";
import { VeiculoService } from "./services/veiculo.service";
import { CounterComponent } from "./counter/counter.component";
import { NavMenuComponent } from "./nav-menu/nav-menu.component";
import { FetchDataComponent } from "./fetch-data/fetch-data.component";
import { VeiculoFormComponent } from "./veiculo-form/veiculo-form.component";

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    CounterComponent,
    FetchDataComponent,
    VeiculoFormComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: "ng-cli-universal" }),
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot([
      { path: "", component: HomeComponent, pathMatch: "full" },
      { path: "veiculos/novo", component: VeiculoFormComponent },
      { path: "counter", component: CounterComponent },
      { path: "fetch-data", component: FetchDataComponent },
    ]),
  ],
  providers: [VeiculoService],
  bootstrap: [AppComponent],
})
export class AppModule {}
