import { VeiculoService } from './veiculo.service';
import { TestBed, inject } from '@angular/core/testing';

describe('VeiculoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [VeiculoService]
    });
  });

  it('should be created', inject([VeiculoService], (service: VeiculoService) => {
    expect(service).toBeTruthy();
  }));
});
