import "rxjs/add/operator/map";
import { Observable } from "rxjs";
import { Marca } from "../models/marca";
import { Modelo } from "../models/modelo";
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable()
export class VeiculoService {
  constructor(private http: HttpClient) {}

  getMarcas(): Observable<Marca[]> {
    return this.http.get<Marca[]>("api/marcas");
  }

  getModelos(id: number): Observable<Modelo[]> {
    return this.http.get<Modelo[]>("api/modelos/" + id);
  }
}
