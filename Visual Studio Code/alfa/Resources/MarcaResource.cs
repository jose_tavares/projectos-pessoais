﻿using alfa.Models;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace alfa.Resources
{
    public class MarcaResource
    {
        public int Id { get; set; }

        public string Nome { get; set; }

        public ICollection<Modelo> Modelos { get; private set; } = new Collection<Modelo>();
    }
}
