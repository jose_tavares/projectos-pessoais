﻿using AutoMapper;
using alfa.Models;
using System.Linq;
using alfa.Resources;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace alfa.Controllers
{
    public class MarcasController : Controller
    {
        private readonly IMapper _mapper;
        private readonly AlfaDbContext _context;

        public MarcasController
            (IMapper mapper,
            AlfaDbContext context)
        {
            _mapper = mapper;
            _context = context;
        }

        [HttpGet("/api/marcas")]
        public async Task<IEnumerable<MarcaResource>> GetMarcas()
        {
            var marcas = await _context.Marcas.ToListAsync();

            return _mapper.Map<IEnumerable<Marca>, IEnumerable<MarcaResource>>(marcas);
        }

        [HttpGet("/api/modelos/{id}")]
        public async Task<IEnumerable<ModeloResource>> GetModelos(int ID)
        {
            var modelos = await _context.Modelos.ToListAsync();

            return _mapper.Map<IEnumerable<Modelo>, IEnumerable<ModeloResource>>(modelos.Where(where => where.MarcaId == ID));
        }
    }
}