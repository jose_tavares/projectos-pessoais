﻿using AutoMapper;
using alfa.Models;
using alfa.Resources;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace alfa.Controllers
{
    public class AcessoriosController : Controller
    {
        private readonly IMapper _mapper;
        private readonly AlfaDbContext _context;

        public AcessoriosController
            (IMapper mapper,
            AlfaDbContext context)
        {
            _mapper = mapper;
            _context = context;
        }

        [HttpGet("/api/acessorios")]
        public async Task<IEnumerable<AcessorioResource>> GetAcessorios()
        {
            var acessorios = await _context.Acessorios.ToListAsync();

            return _mapper.Map<IEnumerable<Acessorio>, IEnumerable<AcessorioResource>>(acessorios);
        }
    }
}