﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace alfa.Models
{
    [Table("Modelos")]
    public class Modelo
    {
        public int Id { get; set; }

        [Required]
        [StringLength(255)]
        public string Nome { get; set; }

        public Marca Marca { get; set; }

        public int MarcaId { get; set; }
    }
}
