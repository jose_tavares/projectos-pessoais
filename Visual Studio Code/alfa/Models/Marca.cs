﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace alfa.Models
{
    [Table("Marcas")]
    public class Marca
    {
        public int Id { get; set; }

        [Required]
        [StringLength(255)]
        public string Nome { get; set; }

        public ICollection<Modelo> Modelos { get; private set; } = new Collection<Modelo>();
    }
}
