﻿using Microsoft.EntityFrameworkCore;

namespace alfa.Models
{
    public class AlfaDbContext : DbContext
    {
        public AlfaDbContext(DbContextOptions<AlfaDbContext> options) : base(options) { }

        public DbSet<Marca> Marcas { get; set; }

        public DbSet<Modelo> Modelos { get; set; }

        public DbSet<Acessorio> Acessorios { get; set; }
    }
}
