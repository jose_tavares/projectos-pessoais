var http = require("http");
http.createServer(function(request, response){
	// Enviar cabeçalho HTTP
	// HTTP Status: 200: OK
	// Content Type: text/plan
	
	response.writeHead(200, {'Content-Type': 'text/plan'});
	//Enviar a String "Hello, World!"
	response.end('Hello, World!\n');
}).listen(8081);
// Consola imprime mensagem
console.log('Servidor a correr em http://127.0.0.1/');