export class Cliente {
  Id: string;
  Nome: string;
  Email: string;
  Telefone: string;
  Endereco: string;
  Cidade: string;
}
