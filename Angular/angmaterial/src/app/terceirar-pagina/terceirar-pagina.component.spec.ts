import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TerceirarPaginaComponent } from './terceirar-pagina.component';

describe('TerceirarPaginaComponent', () => {
  let component: TerceirarPaginaComponent;
  let fixture: ComponentFixture<TerceirarPaginaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TerceirarPaginaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TerceirarPaginaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
